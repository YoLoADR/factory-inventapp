import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbAskQuestionProvider } from '../providers/database/db-ask-question';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';


@Component({
    selector: 'app-ask-question-session-moderate',
    templateUrl: './ask-question-session-moderate.component.html',
    styleUrls: ['./ask-question-session-moderate.component.scss']
})
export class AskQuestionSessionModerateComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    eventId: string = null;
    moduleId: string = null;
    sessionId: string = null;
    questions: Array<any> = [];

    refQuestions: any = null;
    refQuestionsCollection: AngularFirestoreCollection<any> = null;
    loader: boolean = true;

    constructor(
        private dbAskQuestion: DbAskQuestionProvider,
        private route: ActivatedRoute,
        private afs: AngularFirestore

    ) {
        this.eventId = this.route.snapshot.params['eventId'];
        this.moduleId = this.route.snapshot.params['moduleId'];
        this.sessionId = this.route.snapshot.params['sessionId'];

        this.getQuestionsSession();
    }

    ngOnInit() {

    }

    /**
     * Unsubscribe subscriptions on destroy
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    getQuestionsSession() {
        this.refQuestionsCollection = this.afs
            .collection('modules')
            .doc(this.moduleId)
            .collection('sessions')
            .doc(this.sessionId)
            .collection('questions', ref => ref.orderBy('createdAt'));


        this.subscriptions.push(this.refQuestionsCollection.valueChanges().subscribe((questions: any) => {
            let listQuestions = [];

            if (questions && questions.length > 0) {
                listQuestions = questions.sort(function (a, b) {
                    if (a.visibility == true && b.visibility == false) {
                        return 1;
                    } else if (a.visibility == false && b.visibility == true) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            }

            // questions.forEach(element => {
            //     let question = element;
            //     listQuestions.push(question);

            //     listQuestions = listQuestions.sort(function (a, b) {
            //         if (a.visibility == true && b.visibility == false) {
            //             return 1;
            //         }
            //         if (a.visibility == false && b.visibility == true) {
            //             return -1;
            //         }
            //         // a must be equal to b
            //         return 0;
            //     });
            // });

            this.questions = listQuestions;
            this.loader = false;
        }))
    }

    /**
     * Check if index exist
     * @param array 
     * @param item 
     */
    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    /**
     * Change visibility of question
     * @param question 
     */
    changeVisibility(question) {
        this.dbAskQuestion.updateVisibilityAskQuestion(this.eventId, this.moduleId, this.sessionId, question.uid, !question.visibility);
    }

    /**
     * Enable all questions
     */
    enableAll() {
        this.questions.forEach(element => {
            this.dbAskQuestion.updateVisibilityAskQuestion(this.eventId, this.moduleId, this.sessionId, element.uid, true);
        });
    }

    /**
     * Disable all questions
     */
    disableAll() {
        this.questions.forEach(element => {
            this.dbAskQuestion.updateVisibilityAskQuestion(this.eventId, this.moduleId, this.sessionId, element.uid, false);
        });
    }

}

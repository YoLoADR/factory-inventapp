import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SafeHtmlPipe } from '../pipes/safe-html.pipe';
import { ChangeLangComponent } from './change-lang/change-lang.component';
import { FilterPipe } from '../pipes/filter.pipe';
import { GetNameModule } from '../pipes/get-name-module';
import { GetNameTrack } from '../pipes/get-name-track';

// translate module config
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  declarations: [
    SafeHtmlPipe,
    ChangeLangComponent,
    FilterPipe,
    GetNameModule,
    GetNameTrack
  ],
  exports: [
    SafeHtmlPipe,
    ChangeLangComponent,
    FilterPipe,
    GetNameModule,
    GetNameTrack
  ]
})
export class SharedModule { }

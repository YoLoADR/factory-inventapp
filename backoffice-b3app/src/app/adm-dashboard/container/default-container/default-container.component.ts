import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { TypeLogin } from 'src/app/enums/type-login';
import { Container } from 'src/app/models/container';
import { DbAdminUserProvider } from 'src/app/providers/database/db-admin-user';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Event } from 'src/app/models/event';

@Component({
  selector: 'app-default-container',
  templateUrl: './default-container.component.html',
  styleUrls: ['./default-container.component.scss']
})
export class DefaultContainerComponent implements OnInit {
  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;

  containerId: string = 'defaultContainer';
  loginType: TypeLogin = 0;
  clients: Array<any> = [];
  container: Container = {
    uid: 'defaultContainer',
    clientId: null,
    appName: null,
    loginPage: {
      type: TypeLogin.WITH_EMAIL_CONFIRM,
      eventId: null,
    },
    loginOptions: {
      fbLoginBtn: false,
      gLoginBtn: false,
      registeBtn: false,
      shortcodeBtn: true,
    },
    logo: null,
    logoSize: 'invent-logo',
    color: null,
    termsOfUse: '',
    privacyTerms: ''
  };
  displaySelectEvent: boolean = false;
  keyword = 'name';
  eventLoader: boolean = false;
  saveLoader: boolean = false;
  events: Array<Event> = [];
  allowPublicOptions: boolean = false
  blankField: boolean = false;
  nameFile: string = null;
  displayImage: any = null;
  selectedEvent: Event = null;
  noEvents: boolean = false;
  blockBtnSave: boolean = false;

  constructor(
    private dbAdmin: DbAdminUserProvider,
    private dbEvents: DbEventsProvider,
    private zone: NgZone
  ) {
    ;
  }

  ngOnInit() {
    this.getContainer();
  }

  getContainer() {
    this.dbAdmin.getContainer(this.containerId, (container: Container) => {
      this.zone.run(() => {
        this.container = container;
      })
    });
  }
  // get image from input file
  getPicture($ev) {
    this.nameFile = $ev.target.value;
    this.displayImage = $ev.srcElement.files[0]
  }

  changeLoginType(ev) {
    this.loginType = ev.target.value;
  }

  loginTypeEventChange() {
    this.events = [];
    this.eventLoader = false;
    this.blockBtnSave = false;
    this.displaySelectEvent = false;
    this.container.loginPage.eventId = null;
    this.allowPublicOptions = false;
    this.container.loginOptions.fbLoginBtn = false;
    this.container.loginOptions.gLoginBtn = false;
  }

  updateContainer() {
    this.blankField = false;
    if (this.container.appName !== null
      && this.container.loginPage.type !== null
      && (this.container.logo !== null || this.displayImage !== null)
      && this.container.logoSize !== null
    ) {
      this.saveLoader = true;
      this.dbAdmin.updateContainer(this.container, this.displayImage, (status) => {
        if (status) {
          this.successSwal.fire();
          this.saveLoader = false;
        } else {
          this.errorSwal.fire();
          this.saveLoader = false;
        }
      })
    } else {
      this.blankField = true;
    }
  }

  reload() {
    this.getContainer();
    this.displayImage = null;
    this.nameFile = null;
  }
}

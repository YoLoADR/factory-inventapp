import { Component, OnInit, NgModule } from '@angular/core';
import { DbAdminUserProvider } from '../../providers/database/db-admin-user';
import { GlobalService } from 'src/app/providers/global/global.service';
import { FilterPipe } from 'src/app/pipes/filter.pipe';


@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.scss']
})

@NgModule({
    declarations: [FilterPipe],
})

export class ClientComponent implements OnInit {

    public listUsers = [];
    public p: number = 1;
    public deleteUserId: string;
    deleteUserIndex: number;
    loader: boolean = true;
    userId: string = null;
    typeOrder: string = 'asc';
    term: string = '';

    constructor(private dbAdminUser: DbAdminUserProvider, private global: GlobalService) {

        this.global.loadService(() => {
            this.userId = this.global.userId;
            this.loadOrder().then(() => {
                this.getClients();
            });
        })
    }

    ngOnInit() {
        // this.dbAdminUser.setClaims();
    }

    loadOrder() {
        return new Promise((resolve) => {
            this.userId = this.global.userId;

            this.dbAdminUser.getUser(this.userId, (data) => {
                let user = data.result;

                if (user.clientsOrder !== undefined && user.clientsOrder !== null && user.clientsOrder !== '') {
                    this.typeOrder = user.clientsOrder;
                }

                resolve(true);
            })
        })
    }

    getClients() {
        this.dbAdminUser.getClients(this.typeOrder, (users) => {
            this.listUsers = users['result'];
            this.loader = false;
        })
    }

    getUserRemove(userId, index) {
        this.deleteUserId = userId;
        this.deleteUserIndex = index;
    }

    changeOrder() {
        this.dbAdminUser.changeOrderClients(this.userId, this.typeOrder, (data) => {
            if (data == true) {
                this.getClients();
            }
        })
    }

    removeUser() {
        this.loader = true;
        this.dbAdminUser.removeClient(this.deleteUserId, (data) => {
            console.log(data)
            if (data == true) {
                this.listUsers.splice(this.deleteUserIndex, 1);
                this.loader = false;
            } else {
                this.loader = false;
            }
        })
    }

}

// modules
import { NgxPaginationModule } from 'ngx-pagination/dist/ngx-pagination';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// components
import { PathComponents } from '../paths/path-components';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CKEditorModule } from 'ng2-ckeditor';
import { AvatarModule } from 'ngx-avatar';
import { DbLogsProvider } from '../providers/database/db-logs';
import { DbEventsProvider } from '../providers/database/db.events';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ImageCropperModule } from "ngx-image-cropper";
import { SharedModule } from '../shared/shared.module';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

// translate module config
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, '../../assets/i18n/', '.json');
}

const routes: Routes = [
    {
        // dashboard main
        path: '', component: PathComponents.adm_dashboard, children: [
            { path: '', redirectTo: 'client', pathMatch: 'full' },
            // resume is default page
            { path: 'resume', component: PathComponents.adm_resume },
            // supergods components
            { path: 'supergod', component: PathComponents.adm_supergods },
            { path: 'supergod/create', component: PathComponents.adm_supergod_create },
            { path: 'supergod/edit/:uid', component: PathComponents.adm_supergod_edit },
            //gods components
            { path: 'god', component: PathComponents.adm_gods },
            { path: 'god/create', component: PathComponents.adm_god_create },
            { path: 'god/edit/:uid', component: PathComponents.adm_god_edit },
            //clients components
            { path: 'client', component: PathComponents.adm_clients },
            { path: 'client/create', component: PathComponents.adm_client_create },
            { path: 'client/edit/:uid', component: PathComponents.adm_client_edit },
            //client container components
            { path: 'containers', component: PathComponents.adm_client_container },
            { path: 'containers/create', component: PathComponents.adm_client_container_create },
            { path: 'containers/edit/:uid', component: PathComponents.adm_client_container_edit },
            // events b3 components
            { path: 'events-b3', component: PathComponents.adm_events_b3 },
            { path: 'events-b3/create', component: PathComponents.adm_events_b3_create },
            { path: 'events-b3/edit/:uid', component: PathComponents.adm_events_b3_edit },
            // events client components
            { path: 'events-client', component: PathComponents.adm_events_client },
            { path: 'events-client/create', component: PathComponents.adm_events_client_create },
            { path: 'events-client/edit/:uid', component: PathComponents.adm_events_client_edit },
            // default container app
            { path: 'app-container', component: PathComponents.adm_default_container },
            // terms of use and privacy terms
            { path: 'terms-of-use', component: PathComponents.adm_terms_of_use },
            { path: 'privacy', component: PathComponents.adm_privacy_terms },
            { path: 'logs', component: PathComponents.adm_logs },
            // profile
            { path: 'profile/:uid', component: PathComponents.adm_profile }
        ]
    }

]

@NgModule({
    imports: [
        ReactiveFormsModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        SweetAlert2Module,
        CKEditorModule,
        NgxPaginationModule,
        AvatarModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
        ImageCropperModule,
        AutocompleteLibModule,
        SharedModule
    ],
    declarations: [
        PathComponents.adm_dashboard,
        PathComponents.adm_resume,
        PathComponents.adm_supergods,
        PathComponents.adm_supergod_create,
        PathComponents.adm_supergod_edit,
        PathComponents.adm_gods,
        PathComponents.adm_god_create,
        PathComponents.adm_god_edit,
        PathComponents.adm_clients,
        PathComponents.adm_client_create,
        PathComponents.adm_client_edit,
        PathComponents.adm_events_b3,
        PathComponents.adm_events_client,
        PathComponents.adm_events_b3_create,
        PathComponents.adm_events_b3_edit,
        PathComponents.adm_events_client_create,
        PathComponents.adm_events_client_edit,
        PathComponents.adm_terms_of_use,
        PathComponents.adm_privacy_terms,
        PathComponents.adm_logs,
        PathComponents.adm_profile,
        PathComponents.adm_client_container,
        PathComponents.adm_client_container_create,
        PathComponents.adm_client_container_edit,
        PathComponents.adm_default_container
    ],
    providers: [
        DbLogsProvider,
        DbEventsProvider
    ]
})

export class AdmDashboardModule { }

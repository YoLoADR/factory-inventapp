export class OrderType {
    static recentFirst = "recent";
    static oldFirst = "oldest";
    static A_Z = "asc";
    static Z_A = "desc";
}
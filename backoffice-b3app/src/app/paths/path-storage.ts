import { Injectable } from "@angular/core";

@Injectable()

export class PathStorage {
    static profile_pictures = '/profile-pictures';
    static profile_pictures_attendees = '/profile-pictures-attendees';
    static profile_pictures_speakers = '/profile-pictures-speakers';
    static widgets = '/widgets';
    static locations = '/locations';
    static gallery = '/gallery';
    static documents = '/documents';
    static event_visual = '/event/visual';
    static notifications = '/notifications';
    static news_feed = '/news_feed';
    static maps = '/maps';
    static quiz = '/quiz';
    static training = '/training';
    static containers = '/containers';
    static widget_bgs: '/widgets-bgs'
}
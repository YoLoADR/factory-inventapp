import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { DbScheduleProvider } from '../providers/database/db-schedule';
import { GlobalService } from '../providers/global/global.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-ask-question-session-result',
    templateUrl: './ask-question-session-result.component.html',
    styleUrls: ['./ask-question-session-result.component.scss']
})
export class AskQuestionSessionResultComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    eventId: string = null;
    moduleId: string = null;
    sessionId: string = null;
    questions: Array<any> = [];

    refQuestions: any = null;
    refQuestionsCollection: AngularFirestoreCollection<any> = null;

    refVotes: any = null;
    refVotesCollection: AngularFirestoreCollection<any> = null;
    sessionName: string = null;
    loader: boolean = true;

    constructor(
        private route: ActivatedRoute,
        private afs: AngularFirestore,
        private dbSchedule: DbScheduleProvider,
        private global: GlobalService
    ) {
        this.eventId = this.route.snapshot.params['eventId'];
        this.moduleId = this.route.snapshot.params['moduleId'];
        this.sessionId = this.route.snapshot.params['sessionId'];

        this.getUserLanguage();
        this.getQuestionsSession();
    }

    ngOnInit() {
        this.dbSchedule.getSession(this.sessionId, this.moduleId, (session) => {
            this.sessionName = session.name[this.userLanguageFormat];
        })
    }

    /**
     * Unsubscribe subscriptions on destroy
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    userLanguageFormat: string = 'PtBR';
    /**
     * Get user language
     */
    getUserLanguage() {
        this.global.getLanguage((language) => {
            this.userLanguageFormat = this.convertLangFormat(language);
        })
    }

    /**
     * Get questions for the session
     */
    getQuestionsSession() {
        this.refQuestionsCollection = this.afs
            .collection('modules')
            .doc(this.moduleId)
            .collection('sessions')
            .doc(this.sessionId)
            .collection('questions', ref => ref.orderBy('createdAt').where('visibility', "==", true));


        this.subscriptions.push(this.refQuestionsCollection.valueChanges().subscribe((data: any) => {
            let listQuestions = [];

            if (data.length <= 0) {
                this.questions = [];
            }

            data.forEach(element => {
                let question = element;

                this.refVotesCollection = this.refQuestionsCollection
                    .doc(question.uid)
                    .collection('votes');

                this.subscriptions.push(this.refVotesCollection.valueChanges().subscribe((data: any) => {
                    let totalVotes = data.length;

                    question.totalVotes = totalVotes;

                    let index = this.checkIndexExists(listQuestions, question);

                    if (index >= 0) {
                        listQuestions[index] = question;
                    } else {
                        listQuestions.push(question);
                    }

                    listQuestions = listQuestions.sort(function (a, b) {
                        if (a.totalVotes < b.totalVotes) {
                            return 1;
                        }
                        if (a.totalVotes > b.totalVotes) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    });

                    this.questions = listQuestions;
                    this.loader = false;
                }))
            });
        }))
    }

    /**
     * Check if index exist
     * @param array 
     * @param item 
     */
    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    /**
     * Convert language format
     * @param lang 
     */
    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/providers/global/global.service';
import { DbClientEmployeeProvider } from 'src/app/providers/database/db-client-employee';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { User } from 'src/app/models/user';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { DbAdminUserProvider } from 'src/app/providers/database/db-admin-user';
import { TypeUser } from 'src/app/enums/typeUser';
import { DbAttendeesProvider } from 'src/app/providers/database/db-attendees';
import { DbSpeakersProvider } from 'src/app/providers/database/db-speakers';
import { IfStmt } from '@angular/compiler';
import { TranslateService } from '@ngx-translate/core';

declare let $: any;

@Component({
    selector: 'app-create-employee',
    templateUrl: './create-employee.component.html',
    styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {
    imageChangedEvent: any = '';
    croppedImage: any = '';
    formValidation: FormGroup;
    public eventId: string = null;
    public moduleId: string = null;
    loader: boolean = false;
    photoBase64: string = null;
    cropperShow: boolean = false;
    data: any = {};
    sizeImgMax: number = 2097152;
    invalidSizeImg: boolean = false;
    finalFile: any = null;
    fileName: string = null;
    oldUser = null;
    newUser = null;

    validPassword: boolean = null;
    showErrorPassword: boolean = false;

    @ViewChild('successSwal') public successSwal: SwalComponent;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;
    @ViewChild('confirmChangeToClientSwal') public confirmChangeToClientSwal: SwalComponent;
    @ViewChild('errorPermissionSwal') public errorPermissionSwal: SwalComponent;
    @ViewChild('confirmChangeUserTypeSwal') public confirmChangeUserTypeSwal: SwalComponent;
    @ViewChild('alreadyExistsSwal') public alreadyExistsSwal: SwalComponent;

    userRemoveId: string = null;
    userRemoveType: number = null;
    userRemoveTypeStr: string = null;

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private global: GlobalService,
        private dbClientEmployee: DbClientEmployeeProvider,
        private auth: AuthService,
        private dbAdminUser: DbAdminUserProvider,
        private dbAttendee: DbAttendeesProvider,
        private dbSpeaker: DbSpeakersProvider,
        private translateService: TranslateService
    ) {

        this.formValidation = fb.group({
            'photoUrl': [null],
            'name': [null, Validators.compose([Validators.required, Validators.maxLength(35)])],
            'email': [null, Validators.compose([Validators.required, Validators.email, Validators.maxLength(200)])],
            'password': [null, Validators.compose([Validators.required, Validators.maxLength(30)])],
            'title': [null, Validators.compose([Validators.maxLength(50)])],
            'company': [null, Validators.compose([Validators.maxLength(50)])],
            'language': ['pt_BR', Validators.compose([Validators.required])],
            'description': [null],
        });
    }

    ngOnInit() {

    }

    ngDoCheck() {
        this.validatePassword();
    }

    validatePassword() {
        let password = this.formValidation.get('password').value;
        let regex = /(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^a-zA-Z]).{8,50}/;
        let regexSymbol = /[-!$%^&*#@()_+|~=`{}\[\]:";'<>?,.\/]/;

        let found;
        let foundSymbol;
        if (password !== null && password !== undefined && password !== '') {
            found = password.match(regex);
            foundSymbol = password.match(regexSymbol);


            if (found !== null && foundSymbol !== null) {
                this.validPassword = true;
            } else {
                this.validPassword = false;
                this.showErrorPassword = true;
            }
        } else {
            this.validPassword = false;
            this.showErrorPassword = false;
        }
    }



    checkEmailCreateEmployee() {
        let data = this.formValidation.value;

        this.dbAdminUser.getUserByEmail(data.email, (user) => {
            if (user === null) {
                this.userRemoveId = null;
                this.userRemoveType = null;
                this.userRemoveTypeStr = null;
                this.createEmployee();

            } else {

                if (user.type === TypeUser.SUPERGOD) {
                    if (this.global.userType === TypeUser.SUPERGOD) {
                        this.userRemoveId = user.uid;
                        this.userRemoveType = user.type;
                        this.userRemoveTypeStr = this.translateService.instant('global.type_user_supergod');
                        this.confirmChangeUserTypeSwal.fire();
                    } else {
                        this.errorPermissionSwal.fire();
                    }

                } else if (user.type === TypeUser.GOD) {
                    if (this.global.userType === TypeUser.SUPERGOD || this.global.userType === TypeUser.GOD) {
                        // Pergunta se o usuário deseja transformar o GOD em CLIENT
                        this.userRemoveId = user.uid;
                        this.userRemoveType = user.type;
                        this.userRemoveTypeStr = this.translateService.instant('global.type_user_god');
                        this.confirmChangeUserTypeSwal.fire();
                    } else {
                        //Informa que o usuário não tem permissão para mudar o tipo de usuário.
                        this.errorPermissionSwal.fire();
                    }

                } else if (user.type === TypeUser.CLIENT) {
                    if (this.global.userType === TypeUser.SUPERGOD || this.global.userType === TypeUser.GOD) {
                        // Pergunta se o usuário deseja transformar o CLIENT em EMPLOYE
                        this.userRemoveId = user.uid;
                        this.userRemoveType = user.type;
                        this.userRemoveTypeStr = this.translateService.instant('global.type_user_client');
                        this.confirmChangeUserTypeSwal.fire();
                    } else {
                        //Informa que o usuário não tem permissão para mudar o tipo de usuário.
                        this.errorPermissionSwal.fire();
                    }

                } else if (user.type === TypeUser.EMPLOYEE) {
                    //Mensagem informando que o usuário já é um EMPLOYE
                    this.alreadyExistsSwal.fire();

                } else if (user.type === TypeUser.SPEAKER) {
                    this.userRemoveId = user.uid;
                    this.userRemoveType = user.type;
                    this.userRemoveTypeStr = this.translateService.instant('global.type_user_speaker');
                    this.confirmChangeUserTypeSwal.fire();

                } else if (user.type === TypeUser.ATTENDEE) {
                    this.userRemoveId = user.uid;
                    this.userRemoveType = user.type;
                    this.userRemoveTypeStr = this.translateService.instant('global.type_user_attendee');
                    this.confirmChangeUserTypeSwal.fire();

                }
            }
        })
    }

    confirmChangeUserType() {
        if (this.userRemoveType == TypeUser.SUPERGOD || this.userRemoveType == TypeUser.GOD
            || this.userRemoveType == TypeUser.CLIENT || this.userRemoveType == TypeUser.EMPLOYEE) {

            this.auth.removeUserAuth(this.userRemoveId, (data) => {
                if (data.result === true) {
                    this.createEmployee();
                }
            });

        } else if (this.userRemoveType == TypeUser.ATTENDEE) {

            this.dbAttendee.removeAttendeeAllEvents(this.userRemoveId, (data) => {
                if (data == true) {
                    this.createEmployee();
                }
            })

        } else if (this.userRemoveType == TypeUser.SPEAKER) {

            this.dbSpeaker.removeSpeakerAllEvents(this.userRemoveId, (data) => {
                if (data == true) {
                    this.createEmployee();
                }
            })
        }

    }


    createEmployee() {
        let data = this.formValidation.value;

        if (this.validPassword) {
            this.loader = true;
            let user = new User(data.name, data.email);
            user.$photoUrl = "";
            user.$password = data.password;
            user.$language = data.language;
            user.$company = data.company;
            user.$title = data.title;
            console.log(data.$description)
            user.$description = typeof data.$description !== 'undefined' && data.$description !== null ? data.description.replace(/href="/g, 'class="wysiwyg-link" href="') : ""

            user.$createdAt = Date.now() / 1000 | 0;
            user.$clientId = this.global.userId;
            this.auth.verifyEmailDb(data.email)
                .then((response) => {
                    if (response['result'] == "email-not-found") {
                        this.dbClientEmployee.createEmployee(user, this.finalFile, (newUser) => {
                            if (newUser.uid !== null && newUser.uid !== undefined) {
                                // display success modal
                                this.successSwal.fire();
                                this.loader = false;

                            } else {
                                // display error
                                this.errorSwal.fire();
                                this.loader = false;
                            }
                        });
                    } else if (response['result']['uid'] !== null && response['result']['uid'] !== undefined) {
                        this.oldUser = response['result'];
                        this.newUser = user;
                        this.confirmChangeToClientSwal.fire();
                    } else if (response['message'] == 'error') { // caso o retorno seja um erro
                        // display error
                        this.errorSwal.fire();
                        this.loader = false;
                    }
                })
        }
    }

    changeUserToClient() {
        this.dbAdminUser.deleteAndRemakeUser(this.oldUser, this.newUser, (status) => {
            if (status['message'] == 'success') {
                this.successSwal.fire();
                this.loader = false;
            } else {
                this.errorSwal.fire();
                this.loader = false;
            }
        });
    }

    /**
     * On upload image file
     * @param $event 
     */
    onUploadChange($event: any) {
        this.invalidSizeImg = false;
        const file: File = $event.target.files[0];
        this.fileName = $event.target.files[0].name;

        if (file.size < this.sizeImgMax) {
            this.cropperShow = true;
            this.imageChangedEvent = event;
        } else {
            this.invalidSizeImg = true;
        }
    }

    /**
     * On cropped image event
     * @param event 
     */
    imageCropped(event: ImageCroppedEvent) {

        // Get base 64 image
        this.croppedImage = event.base64;
    }

    /**
     * Save profile picture
     */
    saveProfilePicture() {
        if (!this.invalidSizeImg && this.croppedImage) {
            const aux = this.croppedImage.split(',');
            this.finalFile = aux[1];
            this.clearCropper();
            $('#selectPicture').modal('toggle')
        }
    }

    /**
     * Clear cropper data
     */
    clearCropper() {
        this.invalidSizeImg = false;
        this.cropperShow = false;
        this.fileName = null;
    }

    redirectList() {
        this.router.navigate([`/dash/employees/${this.global.userId}`]);
    }

}

/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Event } from '../../../models/event';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { TranslateService } from '@ngx-translate/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { MapsAPILoader } from '@agm/core';

declare let google: any;
declare let $: any;
import { FormGroup, Validators, FormBuilder, FormsModule } from '@angular/forms';
import { DateTime } from 'luxon';
import { environment } from 'src/environments/environment';
import { EventFieldsVisibility } from 'src/app/models/event-fields-visibility';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { NameGroup } from 'src/app/enums/name-group';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DbManagerModuleProvider } from 'src/app/providers/database/db-manager-module';

@Component({
  selector: 'app-client-event-info',
  templateUrl: './client-event-info.component.html',
  styleUrls: ['./client-event-info.component.scss']
})

export class ClientEventInfoComponent implements OnInit {
  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;
  visibility;
  eventId: string;
  eventEdit: Event = null;
  formValidation: FormGroup;
  testComplete;
  eventFields: EventFieldsVisibility;
  // timezones
  timezones: Array<string> = [];
  visibilities: Array<boolean> = [];
  languages = [];
  loader: boolean = true;
  loaderBtn: boolean = false;
  avaiableDescriptionsToSelect;
  // maps
  @ViewChild("search") public searchElement: ElementRef;
  addressGoogle = '';
  // Variable Forms Error
  dateError: boolean;
  startDatePastError: boolean
  endDatePastError: boolean
  updateEventError: boolean

  newSelectedLanguage: string = '-1';

  defaultAppUrl: string = environment.platform.defaultAppUrl;

  allow_language: boolean = false; //multi language 

  interactivityModuleId: string = null;
  answerOffline: boolean = null;
  attendeeModules: Array<any> = [];
  principalEventLanguage: string = '';
  constructor(
    private route: ActivatedRoute,
    private dbEvents: DbEventsProvider,
    private dbModules: DbManagerModuleProvider,
    private dbGroup: DbGroupsProvider,
    private luxon: LuxonService,
    private translateService: TranslateService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private fb: FormBuilder,
  ) {
    this.eventId = this.route.parent.params['_value']['uid']
  }

  ngOnInit() {
    //timezones
    this.timezones = [
      '(UTC-10) Honolulu, Hawaii',
      '(UTC-7) Seattle, Washington, Phoenix, Arizona, Los Angeles, Las Vegas, Portland',
      '(UTC-6) Costa Rica',
      '(UTC-6) El Salvador',
      '(UTC-6) Guatemala',
      '(UTC-6) México City, Chiapas, Guanajuato, Nuevo León, San Luis Potosí, Tamaulipas',
      '(UTC-6) Colombia',
      '(UTC-6) Ecuador',
      '(UTC-5) Quintana Roo',
      '(UTC-5) Panama',
      '(UTC-5) Peru',
      '(UTC-5) Birmingham, Chicago, New Orleans, Minneapolis, Oklahoma City',
      '(UTC-4) Bahamas',
      '(UTC-4) Bolivia',
      '(UTC-4) Dominican Republic',
      '(UTC-4) Washington, Atlanta, Indianapolis, Boston, Detroit,  New York, Philadelphia',
      '(UTC-4) Venezuela',
      '(UTC-3) Argentina',
      '(UTC-3) Brasilia, Salvador, Fortaleza, Belo Horizonte, Rio de Janeiro, Porto Alegre, São Paulo',
      '(UTC-3) Paraguay',
      '(UTC-3) Uruguay',
      '(UTC-1) Cape Verde',
      '(UTC-1) Azores',
      '(UTC+0) Ghana',
      '(UTC+0) Iceland',
      '(UTC+0) Ireland',
      '(UTC+0) Morocco',
      '(UTC+0) Lisboa, Braga, Coimbra, Madeira, Porto, Vila Real',
      '(UTC+0) Las Palmas, Santa Cruz de Tenerife',
      '(UTC+0) Togo',
      '(UTC+0) United Kingdom',
      '(UTC+1) Albania',
      '(UTC+1) Algeria',
      '(UTC+1) Andorra',
      '(UTC+1) Angola',
      '(UTC+1) Austria',
      '(UTC+1) Belgium',
      '(UTC+1) Cameroon',
      '(UTC+1) Croatia',
      '(UTC+1) Czech Republic',
      '(UTC+1) Denmark',
      '(UTC+1) France',
      '(UTC+1) Germany',
      '(UTC+1) Hungary',
      '(UTC+1) Italy',
      '(UTC+1) Liechtenstein',
      '(UTC+1) Luxembourg',
      '(UTC+1) Nigeria',
      '(UTC+1) Norway',
      '(UTC+1) Poland',
      '(UTC+1) Serbia',
      '(UTC+1) Slovakia',
      '(UTC+1) Slovenia',
      '(UTC+1) Madrid, Barcelona, Córdoba, Girona, Granada, Navarra, València',
      '(UTC+1) Sweden',
      '(UTC+1) Switzerland',
      '(UTC+1) Tunisia',
      '(UTC+2) Finland',
      '(UTC+2) Israel',
      '(UTC+2) Romania',
      '(UTC+2) South Africa',
      '(UTC+2) Finland',
      '(UTC+2) Israel',
      '(UTC+2) Romania',
      '(UTC+2) Ukraine',
      '(UTC+2) Egypt',
      '(UTC+3) Iraq',
      '(UTC+3) Kuwait',
      '(UTC+3) Saudi Arabia',
      '(UTC+3) Qatar',
      '(UTC+3) Moscow, Adygeya, Volgograd',
      '(UTC+4) Samara',
      '(UTC+4) United Arab Emirates',
      '(UTC+5.5) India',
      '(UTC+6) Tianjin, Xinjiang, Xizang',
      '(UTC+7) Jakarta, Pontianak',
      '(UTC+7) Thailand',
      '(UTC+8) Western Australia',
      '(UTC+8) Beijing, Guizhou, Hong Kong, Shaanxi, Shanghai',
      '(UTC+8) Hong Kong',
      '(UTC+8) Singapore',
      '(UTC+8.5) North Korea',
      '(UTC+9) Japan',
      '(UTC+9) South Korea',
    ];


    // languages
    this.languages = [
      { value: "pt_BR", name: this.translateService.instant('comp.event_info.portuguese') },
      { value: "en_US", name: this.translateService.instant('comp.event_info.english') },
      { value: "es_ES", name: this.translateService.instant('comp.event_info.spanish') },
      { value: "fr_FR", name: this.translateService.instant('comp.event_info.french') },
      { value: "de_DE", name: this.translateService.instant('comp.event_info.german') }
    ]

    // form
    this.formValidation = this.fb.group({
      'title': [this.translateService.instant('comp.new_event.event_name'), Validators.compose([Validators.required])],
      'language': [{ value: '', name: '' }, Validators.compose([Validators.required])],
      'shortcode': [this.translateService.instant('comp.new_event.event_shortcode'), Validators.compose([Validators.required])],
      'site': [''],
      'startDate': ['dd/mm/aaaa', Validators.compose([Validators.required])],
      'endDate': ['dd/mm/aaaa', Validators.compose([Validators.required])],
      'timezone': ['', Validators.compose([Validators.required])],
      'visibility': ['', Validators.compose([Validators.required])],
      'default_attendee_module': [''],
      'google_btn': [false],
      'facebook_btn': [false],
      'twitter_btn': [false],
      'microsoft_btn': [false],
      'yahoo_btn': [false],
      'placeAddress': [''],
      'placeName': [''],
      'emailSupport': [''],
      'pt_BR': [null],
      'en_US': [null],
      'es_ES': [null],
      'fr_FR': [null],
      'de_DE': [null]
    });
  }

  ngAfterViewInit() {
    this.loadEvents()
    this.loadConfigInteractivity();
  }

  loadConfigInteractivity() {
    this.dbEvents.getModuleInteractivity(this.eventId, (module) => {
      this.interactivityModuleId = module.uid;
      if (module.answerOffline === undefined) {
        this.answerOffline = false;
      } else {
        this.answerOffline = module.answerOffline;
      }
    })
  }

  changeInteractivityConfig() {
    setTimeout(() => {
      this.dbEvents.changeInteractivityAnswerConfig(this.eventId, this.interactivityModuleId, this.answerOffline, (data) => {
        if (!data) {
          this.answerOffline = !this.answerOffline;
        }
      })
    }, 1000);
  }

  getAttendeesModules() {
    this.dbModules.getAttendeeModulesByEvent(this.eventId, (modules) => {
      this.attendeeModules = modules;
    })
  }

  // load the event
  loadEvents() {
    this.dbEvents.getEvent(this.eventId, (event: Event) => {
      this.ngZone.run(() => {
        if (event) {
          this.principalEventLanguage = this.convertLangFormat(event.language);
          this.getAttendeesModules()

          // get event
          this.eventEdit = event;
          if (this.eventEdit.publicOptions == undefined) {
            this.eventEdit.publicOptions = {
              google_btn: false,
              facebook_btn: false,
              twitter_btn: false,
              microsoft_btn: false,
              yahoo_btn: false,
            }
          }

          if (this.eventEdit.allowProfileQR == undefined || this.eventEdit.allowProfileQR == null) {
            this.eventEdit.allowProfileQR = true;
          }
      
          //multi language 
          this.allow_language = this.eventEdit.allow_language

          // sort array of languages by placing the main language at the top.
          this.sortArrayLanguages()

          if (event.eventFields === undefined) {
            event['eventFields'] = Object.assign({}, new EventFieldsVisibility());
          }

          this.eventFields = event.eventFields;

          // get infos event
          this.formValidation.patchValue({
            title: event.title,
            language: this.getLanguage(event.language),
            shortcode: event.shortcode,
            site: event.site,
            startDate: this.luxon.convertDateToStringYearMonthDay(this.luxon.convertTimestampToDate(event.startDate)),
            endDate: this.luxon.convertDateToStringYearMonthDay(this.luxon.convertTimestampToDate(event.endDate)),
            timezone: event.timezone,
            visibility: event.visibility,
            placeAddress: event.placeAddress,
            placeName: event.placeName,
            emailSupport: event.emailSupport,
            default_attendee_module: event.default_attendee_module,
            pt_BR: event.description.pt_BR,
            en_US: event.description.en_US,
            es_ES: event.description.es_ES,
            fr_FR: event.description.fr_FR,
            de_DE: event.description.de_DE
          });

          this.loadPlacesAutocomplete();
        }

        this.loader = false;
      });
    });
  }

  async updateEvent() {
    this.loaderBtn = true;
    let validation = true;
    this.dateError = false;
    this.startDatePastError = false;
    this.endDatePastError = false;
    this.updateEventError = false;

    this.eventEdit.title = this.formValidation.value.title
    this.eventEdit.site = this.formValidation.value.site
    this.eventEdit.timezone = this.formValidation.value.timezone;
    this.eventEdit.visibility = this.formValidation.value.visibility;
    this.eventEdit.emailSupport = this.formValidation.value.emailSupport;
    this.eventEdit.eventFields = this.eventFields;
    this.eventEdit.default_attendee_module = this.formValidation.value.default_attendee_module;

    if (this.addressGoogle !== '') {
      this.eventEdit.placeAddress = this.addressGoogle;
    } else {
      this.eventEdit.placeAddress = this.formValidation.value.placeAddress;
    }
    this.eventEdit.placeName = this.formValidation.value.placeName;


    // descriptions
    // Checks if the language is enabled.
    if (this.eventEdit.languages['PtBR']) {
      this.eventEdit.description['pt_BR'] = this.formValidation.value.pt_BR !== null ? this.formValidation.value.pt_BR.replace(/href="/g, 'class="wysiwyg-link" href="') : ""
    } else {
      this.eventEdit.description['pt_BR'] = null
    }

    if (this.eventEdit.languages['EnUS']) {
      this.eventEdit.description['en_US'] = this.formValidation.value.en_US !== null ? this.formValidation.value.en_US.replace(/href="/g, 'class="wysiwyg-link" href="') : ""
    } else {
      this.eventEdit.description['en_US'] = null
    }

    if (this.eventEdit.languages['EsES']) {
      this.eventEdit.description['es_ES'] = this.formValidation.value.es_ES !== null ? this.formValidation.value.es_ES.replace(/href="/g, 'class="wysiwyg-link" href="') : ""
    } else {
      this.eventEdit.description['es_ES'] = null
    }

    if (this.eventEdit.languages['FrFR']) {
      this.eventEdit.description['fr_FR'] = this.formValidation.value.fr_FR !== null ? this.formValidation.value.fr_FR.replace(/href="/g, 'class="wysiwyg-link" href="') : ""
    } else {
      this.eventEdit.description['fr_FR'] = null
    }

    if (this.eventEdit.languages['DeDE']) {
      this.eventEdit.description['de_DE'] = this.formValidation.value.de_DE !== null ? this.formValidation.value.de_DE.replace(/href="/g, 'class="wysiwyg-link" href="') : ""
    } else {
      this.eventEdit.description['de_DE'] = null
    }


    // start date
    const arrayStartDate = this.formValidation.value.startDate.split('-') // pega o valor do input startDate

    const year1 = arrayStartDate[0];
    const month1 = arrayStartDate[1];
    const day1 = arrayStartDate[2];

    const sTimestamp = this.luxon.createTimeStamp(this.luxon.createDate(year1, month1, day1, '00', '00', '00'));

    // end date
    const arrayEndDate = this.formValidation.value.endDate.split('-') // pega o valor do input startDate

    const year2 = arrayEndDate[0];
    const month2 = arrayEndDate[1];
    const day2 = arrayEndDate[2];

    const eTimestamp = this.luxon.createTimeStamp(this.luxon.createDate(year2, month2, day2, '00', '00', '00'));

    // start date after closure date    
    if (sTimestamp > eTimestamp) {
      validation = false;
      this.dateError = true;
      this.loader = false;
    }

    // manufactures today's date
    const today = this.luxon.convertDateToStringYearMonthDay(DateTime.local());
    const arrayToday = today.split('-');

    const year = arrayToday[0];
    const month = arrayToday[1];
    const day = arrayToday[2];

    const todayTimestamp = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, '00', '00', '00'))

    // If the end date is in the past
    if (sTimestamp < todayTimestamp) {
      validation = false;
      this.startDatePastError = true;
      this.loader = false;
    }

    // If the end date is in the past
    if (eTimestamp < todayTimestamp) {
      validation = false;
      this.endDatePastError = true;
      this.loader = false;
    }

    this.eventEdit.startDate = sTimestamp
    this.eventEdit.endDate = eTimestamp

    // update event
    this.dbEvents.updateEvent(this.eventEdit, (data) => {
      if (data == true) {
        this.successSwal.fire();
        this.loaderBtn = false;
        this.loadEvents();
      } else {
        this.errorSwal.fire();
        this.loaderBtn = false;
      }
    })
  }

  // sort array of languages by placing the main language at the top.
  sortArrayLanguages() {
    const index = this.languages.map(function (e) { return e['value']; }).indexOf(this.eventEdit['language']);

    // put the main language at index zero of the array.
    if (index > 0) {
      const aux = this.languages[0]
      this.languages[0] = this.languages[index]
      this.languages[index] = aux;
    }
  }

  // change the main language of the event
  changePrincipalLanguage() {
    let aux = this.formValidation.value.language['value'];

    // set the primary language
    switch (aux) {
      case 'pt_BR': {
        this.eventEdit['language'] = 'pt_BR'
        this.eventEdit['languages']['PtBR'] = true

        if (!this.formValidation.value.pt_BR) {
          this.formValidation.value.pt_BR = ''
        }

        break;
      }

      case 'en_US': {
        this.eventEdit['language'] = 'en_US'
        this.eventEdit['languages']['EnUS'] = true

        if (!this.formValidation.value.en_US) {
          this.formValidation.value.en_US = ''
        }

        break;
      }

      case 'es_ES': {
        this.eventEdit['language'] = 'es_ES'
        this.eventEdit['languages']['EsES'] = true

        if (!this.formValidation.value.es_ES) {
          this.formValidation.value.es_ES = ''
        }

        break;
      }

      case 'fr_FR': {
        this.eventEdit['language'] = 'fr_FR'
        this.eventEdit['languages']['FrFR'] = true

        if (!this.formValidation.value.fr_FR) {
          this.formValidation.value.fr_FR = ''
        }

        break;
      }

      case 'de_DE': {
        this.eventEdit['language'] = 'de_DE'
        this.eventEdit['languages']['DeDE'] = true

        if (!this.formValidation.value.de_DE) {
          this.formValidation.value.de_DE = ''
        }

        break;
      }
    }


    // sort array of languages by placing the main language at the top.
    this.sortArrayLanguages()

    // Checks if multi-language is enabled.
    // If multi-language has been disabled, set all secondary languages to false.
    if (!this.allow_language) {
      if (this.eventEdit['language'] != 'pt_BR') {
        this.eventEdit['languages']['PtBR'] = false // disable the language
      }

      if (this.eventEdit['language'] != 'en_US') {
        this.eventEdit['languages']['EnUS'] = false // disable the language
      }

      if (this.eventEdit['language'] != 'es_ES') {
        this.eventEdit['languages']['EsES'] = false
      }

      if (this.eventEdit['language'] != 'fr_FR') {
        this.eventEdit['languages']['FrFR'] = false
      }

      if (this.eventEdit['language'] != 'de_DE') {
        this.eventEdit['languages']['DeDE'] = false
      }
    }
  }

  // get a language and enable it on the eventEdit.languages attribute
  addLanguageSecondary(language: string) {
    // check language
    switch (language) {
      case 'pt_BR': {
        this.eventEdit.languages.PtBR = true  // enable the language
        this.formValidation.value.pt_BR = '' // enable language description
        break;
      }

      case 'en_US': {
        this.eventEdit.languages.EnUS = true  // enable the language
        this.formValidation.value.en_US = '' // enable language description
        break;
      }

      case 'es_ES': {
        this.eventEdit.languages.EsES = true  // enable the language
        this.formValidation.value.es_ES = ''  // enable language description
        break;
      }

      case 'fr_FR': {
        this.eventEdit.languages.FrFR = true // enable the language
        this.formValidation.value.fr_FR = '' // enable language description
        break;
      }

      case 'de_DE': {
        this.eventEdit.languages.DeDE = true // enable the language
        this.formValidation.value.de_DE = '' // enable language description
        break;
      }
    }
  }

  // get a language and disable it in the eventEdit.languages  attribute
  removeLanguageSecondary(language: string) {
    // check language
    switch (language) {
      case 'pt_BR': {
        this.eventEdit.languages.PtBR = false // disable the language

        // disable language description
        this.formValidation.patchValue({
          'pt_BR': null
        })

        break;
      }

      case 'en_US': {
        this.eventEdit.languages.EnUS = false // disable the language

        // disable language description
        this.formValidation.patchValue({
          'en_US': null
        })


        break;
      }

      case 'es_ES': {
        this.eventEdit.languages.EsES = false // disable the language

        // disable language description
        this.formValidation.patchValue({
          'es_ES': null
        })

        break;
      }

      case 'fr_FR': {
        this.eventEdit.languages.FrFR = false  // disable the language

        // disable language description
        this.formValidation.patchValue({
          'fr_FR': null
        })

        break;
      }

      case 'de_DE': {
        this.eventEdit.languages.DeDE = false // disable the language

        // disable language description
        this.formValidation.patchValue({
          'de_DE': null
        })

        break;
      }
    }

  }

  addDescription;
  clearAddDescription() {
    this.newSelectedLanguage = '-1';
  }

  loadPlacesAutocomplete() {
    this.mapsAPILoader.load().then(
      () => {
        setTimeout(() => {
          let autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, { types: ["address"] });
          autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();
              if (place.geometry === undefined || place.geometry === null) {
                return;
              }
              this.addressGoogle = place.formatted_address;
            })
          })
        }, 1200);
      }
    )
  }

  getLanguage(language) {
    if (language === "pt_BR") {
      return { value: "pt_BR", name: this.translateService.instant('comp.event_info.portuguese') }
    } else if (language === 'en_US') {
      return { value: "en_US", name: this.translateService.instant('comp.event_info.english') }
    } else if (language === "es_ES") {
      return { value: "es_ES", name: this.translateService.instant('comp.event_info.spanish') }
    } else if (language === "fr_FR") {
      return { value: "fr_FR", name: this.translateService.instant('comp.event_info.french') }
    } else if (language === "de_DE") {
      return { valeu: "de_DE", name: this.translateService.instant('comp.event_info.german') }
    }
  }

  convertLangFormat(lang) {
    let formatedLang;
    switch (lang) {
      case 'pt_BR': {
        formatedLang = 'PtBR'
        break;
      }
      case 'en_US': {
        formatedLang = 'EnUS';
        break;
      }
      case 'es_ES': {
        formatedLang = 'EsES';
        break;
      }
      case 'fr_FR': {
        formatedLang = 'FrFR';
        break;
      }
      case 'de_DE': {
        formatedLang = 'DeDE';
        break;
      }
    }
    return formatedLang;
  }
}

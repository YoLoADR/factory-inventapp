import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.scss']
})
export class ColorsComponent implements OnInit {
  eventId: string;

  menu_color: string = '#2973A2';
  menu_color2: string = '#2973A2';
  menu_text_color: string = '#FFFFFF';
  title_color: string = '#000000';
  text_content_color: string = '#3F3F3F';
  link_color: string = '#4BA4D8';
  bg_content_color: string = '#FFFFFF';
  bg_general_color: string = '#F6F6F6';
  loader: boolean = true;
  saveLoader: boolean = false;
  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;
  constructor(
    private route: ActivatedRoute,
    private dbEvents: DbEventsProvider
  ) {
    this.eventId = this.route.parent.params['_value']['uid']
  }

  ngOnInit() {
    this.startEvent();
  }

  allowDegrade: boolean = false;
  startEvent() {
    this.dbEvents.getEvent(this.eventId, (event) => {
      if (event.colors.menu_color.includes('linear')) {
        this.allowDegrade = true;
        let colors = this.separeGradientColors(event.colors.menu_color);
        this.menu_color = colors[0];
        this.menu_color2 = colors[1];
      } else {
        this.menu_color = event.colors.menu_color;
      }
      this.menu_text_color = event.colors.menu_text_color;
      this.title_color = event.colors.title_color;
      this.text_content_color = event.colors.text_content_color;
      this.link_color = event.colors.link_color;
      this.bg_content_color = event.colors.bg_content_color;
      this.bg_general_color = event.colors.bg_general_color;
      this.loader = false;
    });
  }

  updateColors() {
    this.saveLoader = true;
    let menu_color;
    if (this.allowDegrade) {
      menu_color = `linear-gradient(to right, ${this.menu_color}, ${this.menu_color2})`
    } else {
      menu_color = this.menu_color;
    }
    let colors = {
      menu_color: menu_color,
      menu_text_color: this.menu_text_color,
      title_color: this.title_color,
      text_content_color: this.text_content_color,
      link_color: this.link_color,
      bg_content_color: this.bg_content_color,
      bg_general_color: this.bg_general_color
    }

    this.dbEvents.updateEventColors(this.eventId, colors, (status) => {
      if (status == true) {
        this.successSwal.fire();
        this.saveLoader = false;
      } else {
        this.errorSwal.fire();
        this.saveLoader = false;
      }
    });
  }


  separeGradientColors(str) {
    let hex = [];
    let index = str.indexOf('#');

    while (index > -1) {
      hex.push(this.getHexColors(str))
      str = str.replace('#', '');
      index = str.indexOf('#');
    }

    return hex;
  }

  getHexColors(str) {
    let position = str.indexOf('#');
    let result = str.substring(position, position + 7);

    return result;
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { DbBroadcastProvider } from 'src/app/providers/database/db-broadcast';
import { GlobalService } from 'src/app/providers/global/global.service';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.scss']
})
export class BroadcastComponent implements OnInit {

  public module = null

  // get the language of the user.
  public userLanguage: string

  listBroadcast = []
  liveSelected: string = null; 
  broadcastRemoveId: string = null;
  broadcastRemoveIndex: number = null;

  eventId: string;
  moduleId: string;

  loader: boolean

  pageRemove = null
  moduleName: string = null;

  loaderGeneral: boolean = false;

  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;
  @ViewChild('deleteSwal') public deleteSwal: SwalComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbBroadcast: DbBroadcastProvider,
    private global: GlobalService,
    private dbEvent: DbEventsProvider
  ) {
    
  }

  ngOnInit() {
    this.loader = true

    this.eventId = this.route.parent.params['_value']['uid'];
    this.moduleId = this.route.snapshot.params['moduleId'];

    this.getUserLanguage()
    this.getModule();
    this.getEvent();
    this.loadBroadcasts()
  }

  event: any = null;
  getEvent() {
    this.dbEvent.getEvent(this.eventId, (event: Event) => {
      this.event = event;
    });
  }

  getModule() {
    this.dbBroadcast.getModule(this.moduleId, (module) => {
      this.module = module;
    });
  }

  ngDoCheck() { // verifica se houve mudança no parâmetro do idModule
    this.eventId = this.route.parent.params['_value']['uid'];

    if (this.moduleId !== this.route.snapshot.params['moduleId']) {
      this.loader = true

      this.moduleId = this.route.snapshot.params['moduleId'];
      this.getModule()
      this.getUserLanguage()
      this.loadBroadcasts()
    }
  }

  // get the language of the user.
  getUserLanguage() {
    this.global.getLanguage((language) => {
      this.userLanguage = language
    })
  }

  loadBroadcasts() {
    this.dbBroadcast.getBroadcasts(this.moduleId, (data) => {
      this.listBroadcast = data.results;
    })
  }

  getBroascastRemove(broadcastId, index) {
    console.log(this.moduleId, broadcastId, index) 
    this.broadcastRemoveId = broadcastId;
    this.broadcastRemoveIndex = index;
    this.deleteSwal.fire();
  }
  
  broadcastRemove() {
    this.dbBroadcast.removeBroadcast(this.moduleId, this.broadcastRemoveId).then((data) => {
      if(true) {
        this.successSwal.fire();
        this.listBroadcast.splice(this.broadcastRemoveIndex, 1);
      }
    });
  }

  showBroadcast(broadcast) {
    console.log(broadcast.resourceUri)
    //https://dist.bambuser.net/player/?resourceUri=https%3A//cdn.bambuser.net/broadcasts/0b9860dd-359a-67c4-51d9-d87402770319%3Fda_signature_method%3DHMAC-SHA256%26da_id%3D9e1b1e83-657d-7c83-b8e7-0b782ac9543a%26da_timestamp%3D1482921565%26da_static%3D1%26da_ttl%3D0%26da_signature%3Dcacf92c6737bb60beb1ee1320ad85c0ae48b91f9c1fbcb3032f54d5cfedc7afe
    this.liveSelected = broadcast.resourceUri;
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { DbCustomPagesProvider } from 'src/app/providers/database/db-custom-pages';
import { GlobalService } from 'src/app/providers/global/global.service';
import { Languages } from 'src/app/models/languages';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Location } from '@angular/common';


@Component({
  selector: 'app-custom-page-edit',
  templateUrl: './custom-page-edit.component.html',
  styleUrls: ['./custom-page-edit.component.scss']
})

export class CustomPageEditComponent implements OnInit {
  public module = null

  // get the language of the user.
  public userLanguage: string

  public eventId: string = this.route.parent.params['_value']['uid'];;
  public moduleId: string = this.route.snapshot.params['moduleId'];
  public pageId: string = this.route.snapshot.params['pageId']

  pageEdit = null
  htmlContent = {
    PtBR: '',
    EnUS: '',
    EsES: '',
    FrFR: '',
    DeDE: ''
  };
  title = {
    PtBR: '',
    EnUS: '',
    EsES: '',
    FrFR: '',
    DeDE: ''
  };
  languages: Languages = null //event languages
  event: any;
  principalEventLanguageFormated: string = 'PtBR';
  activeCreateLanguage: string = 'PtBR';
  loader: boolean

  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbCustomPage: DbCustomPagesProvider,
    private global: GlobalService,
    private dbEvent: DbEventsProvider,
    public location: Location
  ) { }

  ngOnInit() {
    this.loadPage();
    this.getModule();
    this.getUserLanguage();
    this.startEvent();
  }

  // get the language of the user.
  getUserLanguage() {
    this.global.getLanguage((language) => {
      this.userLanguage = language
    })
  }

  startEvent() {
    this.dbEvent.getEvent(this.eventId, (event) => {
      this.event = event;
      this.principalEventLanguageFormated = this.convertLangFormat(this.event.language);
      this.activeCreateLanguage = this.principalEventLanguageFormated;
      this.languages = event.languages;
    });
  }

  getModule() {
    this.dbCustomPage.getModule(this.moduleId, (module) => {
      this.module = module;
    });
  }

  loadPage() {
    this.loader = true

    this.dbCustomPage.getPage(this.moduleId, this.pageId, (page) => {
      if (typeof page !== 'undefined' && page !== null) {
        this.pageEdit = page
        this.htmlContent = page.htmlContent
        this.title = page.title
      }

      this.loader = false
    })
  }

  errorPrincipalLanguageBlank: boolean = false;
  updatePage() {
    this.errorPrincipalLanguageBlank = false;
    this.pageEdit.title = this.title
    const contentHtml = {
      PtBR: this.htmlContent.PtBR.replace(/href="/g, 'class="wysiwyg-link" href="'),
      EnUS: this.htmlContent.EnUS.replace(/href="/g, 'class="wysiwyg-link" href="'),
      EsES: this.htmlContent.EsES.replace(/href="/g, 'class="wysiwyg-link" href="'),
      FrFR: this.htmlContent.FrFR.replace(/href="/g, 'class="wysiwyg-link" href="'),
      DeDE: this.htmlContent.DeDE.replace(/href="/g, 'class="wysiwyg-link" href="')
    }
    this.pageEdit.htmlContent = contentHtml
    if (this.pageEdit.title[this.principalEventLanguageFormated] !== '' && this.pageEdit.htmlContent[this.principalEventLanguageFormated] !== '') {
      this.loader = true;
      for (let lang in this.pageEdit.title) {
        if (this.pageEdit.title[lang] == '') {
          this.pageEdit.title[lang] = this.pageEdit.title[this.principalEventLanguageFormated];
        }

        if (this.pageEdit.htmlContent[lang] == '') {
          this.pageEdit.htmlContent[lang] = this.pageEdit.htmlContent[this.principalEventLanguageFormated];
        }
      }
      this.dbCustomPage.updatePage(this.pageEdit).then((success) => {
        this.successSwal.fire();
        this.loader = false;
        // this.location.back();
      }).catch((error) => {
        this.errorSwal.fire();
        this.loader = false;
      })
    } else {
      this.errorPrincipalLanguageBlank = true;
    }
  }

  redirectList() {
    this.router.navigate(['/event/' + this.eventId + '/custom-pages/' + this.moduleId]);
  }

  convertLangFormat(lang) {
    let formatedLang;
    switch (lang) {
      case 'pt_BR': {
        formatedLang = 'PtBR'
        break;
      }
      case 'en_US': {
        formatedLang = 'EnUS';
        break;
      }
      case 'es_ES': {
        formatedLang = 'EsES';
        break;
      }
      case 'fr_FR': {
        formatedLang = 'FrFR';
        break;
      }
      case 'de_DE': {
        formatedLang = 'DeDE';
        break;
      }
    }
    return formatedLang;
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { DbCustomPagesProvider } from 'src/app/providers/database/db-custom-pages';
import { PageInfobooth } from 'src/app/models/page-infobooth'
import { GlobalService } from 'src/app/providers/global/global.service';
import { Languages } from 'src/app/models/languages';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Location } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-custom-page-create',
  templateUrl: './custom-page-create.component.html',
  styleUrls: ['./custom-page-create.component.scss']
})

export class CustomPageCreateComponent implements OnInit {
  public module = null

  // get the language of the user.
  public userLanguage: string

  htmlContent = {
    PtBR: '',
    EnUS: '',
    EsES: '',
    FrFR: '',
    DeDE: ''
  };
  title = {
    PtBR: '',
    EnUS: '',
    EsES: '',
    FrFR: '',
    DeDE: ''
  };

  eventId = this.route.parent.params['_value']['uid']
  moduleId = this.route.snapshot.params['moduleId']

  public formGroup: FormGroup;

  loader: boolean
  moduleName: string = null;
  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;
  languages: Languages = null //event languages
  event: any;
  principalEventLanguageFormated: string = 'PtBR';
  activeCreateLanguage: string = 'PtBR';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbCustomPage: DbCustomPagesProvider,
    private global: GlobalService,
    private dbEvent: DbEventsProvider,
    public location: Location
  ) { }

  ngOnInit() {
    this.getModule();
    this.getUserLanguage();
    this.startEvent();
  }

  startEvent() {
    this.dbEvent.getEvent(this.eventId, (event) => {
      this.event = event;
      this.principalEventLanguageFormated = this.convertLangFormat(this.event.language);
      console.log(this.principalEventLanguageFormated);
      this.activeCreateLanguage = this.principalEventLanguageFormated;
      this.languages = event.languages;
    });
  }

  // get the language of the user.
  getUserLanguage() {
    this.global.getLanguage((language) => {
      this.userLanguage = language
    })
  }

  getModule() {
    this.dbCustomPage.getModule(this.moduleId, (module) => {
      this.module = module;
    });
  }

  errorPrincipalLanguageBlank: boolean = false;
  createPage() {

    const page = new PageInfobooth()
    this.errorPrincipalLanguageBlank = false;
    page.moduleId = this.moduleId
    page.eventId = this.eventId
    page.title = this.title
    const contentHtml = {
      PtBR: this.htmlContent.PtBR.replace(/href="/g, 'class="wysiwyg-link" href="'),
      EnUS: this.htmlContent.EnUS.replace(/href="/g, 'class="wysiwyg-link" href="'),
      EsES: this.htmlContent.EsES.replace(/href="/g, 'class="wysiwyg-link" href="'),
      FrFR: this.htmlContent.FrFR.replace(/href="/g, 'class="wysiwyg-link" href="'),
      DeDE: this.htmlContent.DeDE.replace(/href="/g, 'class="wysiwyg-link" href="')
    }
    page.htmlContent = contentHtml;

    if (page.title[this.principalEventLanguageFormated] !== '' && page.htmlContent[this.principalEventLanguageFormated] !== '') {
      this.loader = true
      for (let lang in page.title) {
        if (page.title[lang] == '') {
          page.title[lang] = page.title[this.principalEventLanguageFormated];
        }

        if (page.htmlContent[lang] == '') {
          page.htmlContent[lang] = page.htmlContent[this.principalEventLanguageFormated];
        }
      }
      this.dbCustomPage.getTotalPagesModule(this.moduleId, (total) => {
        page.order = total;

        this.dbCustomPage.createPage(page).then(() => {
          this.successSwal.fire();
          this.loader = false;
          // this.location.back();
        }).catch((error) => {
          this.errorSwal.fire()
          this.loader = false
        })
      })
    } else {
      this.errorPrincipalLanguageBlank = true;
    }
  }

  redirectList() {
    this.router.navigate(['/event/' + this.eventId + '/custom-pages/' + this.moduleId]);
  }

  convertLangFormat(lang) {
    let formatedLang;
    switch (lang) {
      case 'pt_BR': {
        formatedLang = 'PtBR'
        break;
      }
      case 'en_US': {
        formatedLang = 'EnUS';
        break;
      }
      case 'es_ES': {
        formatedLang = 'EsES';
        break;
      }
      case 'fr_FR': {
        formatedLang = 'FrFR';
        break;
      }
      case 'de_DE': {
        formatedLang = 'DeDE';
        break;
      }
    }
    return formatedLang;
  }

}

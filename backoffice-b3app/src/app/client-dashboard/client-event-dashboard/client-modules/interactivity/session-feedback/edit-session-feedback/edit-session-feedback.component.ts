import { Component, OnInit, ViewChild, NgModule } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from 'src/app/models/session-feedback-question';
import { Answer } from 'src/app/models/session-feedback-answer';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Track } from 'src/app/models/track';
import { Session } from 'src/app/models/session';
import { Group } from 'src/app/models/group';
import { TypeSessionFeedback } from 'src/app/models/type-session-feedback';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { DbScheduleProvider } from 'src/app/providers/database/db-schedule';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { DbSessionFeedbackProvider } from 'src/app/providers/database/db-session-feedback';
import { SessionFeedback } from 'src/app/models/session-feedback';
import { MenuIcons, icons } from '../../../../../../models/menu-icons';
import { PathIcons } from 'src/app/paths/path-icons';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { GlobalService } from 'src/app/providers/global/global.service';
import { GetNameSession } from 'src/app/pipes/get-name-session';
import { GetNameTrack } from 'src/app/pipes/get-name-track';
import { NameModule } from 'src/app/models/name-module';
import { Languages } from 'src/app/models/languages';
import { FormatedMultiIdiomaService } from 'src/app/providers/formated-multi-idioma/formated-multi-idioma.service';

declare let $: any;

@NgModule({
  declarations: [
    GetNameSession
  ],
})

@Component({
  selector: 'app-edit-session-feedback',
  templateUrl: './edit-session-feedback.component.html',
  styleUrls: ['./edit-session-feedback.component.scss']
})

export class EditSessionFeedbackComponent implements OnInit {
  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorDeleteLastQuestion') public errorDeleteLastQuestion: SwalComponent;
  
  // get the language of the user.
  public userLanguage: string
  // get the language of the event.
  public eventLanguage: string
  languages: Languages = null //event languages
  activeLanguage: string = null;

  eventId: string = null;
  eventTimezone: string = null;
  moduleId: string = null;
  feedbackId: string = null;

  public questions: Array<Question> = [];
  public answers: Array<Answer> = [];

  public listAuxInput: Array<any> = [];
  loaderQuestions: Array<boolean> = [];

  questionId: string = null;
  iconsImg: MenuIcons[] = icons;
  selectedIcons;
  selectedIcon: string = null;
  selectedIconFamily: string = null;
  formGroup: FormGroup;
  formEditQuestion: FormGroup;
  formNewQuestion: FormGroup;
  addQuestionShow: boolean = false;
  editQuestionShow: boolean = false;
  allowShowAddQuestion: boolean = true;

  editQuestionIndex: number = null;

  // blockGraphicTypevisibilityEdit = true;
  blockOptionsAnswersVisibilityEdit = true;
  blockQuestionPointsVisibilityEdit = true;
  // blockGraphicTypevisibilityCreate = true;
  blockOptionsAnswersVisibilityCreate = true;
  blockQuestionPointsVisibilityCreate = true;
  activeMarker: boolean = false;
  activeWeight: boolean = false;

  public errorTitleEdit = false;
  public errorSelectQuestionEdit = false;
  public errorOptionAnswerEdit = false;
  public errorTitleCreate = false;
  public errorSelectQuestionCreate = false;
  public errorOptionAnswerCreate = false;

  feedback;
  feedbackTitle;
  feedbackIcon;
  // feedbackMaxResponses;
  feedbackChangeAnswer = false;
  // feedbackMaxResponsesVisibility = false;

  SelectedSessionVisibility: boolean = false;
  tracksAttendeeVisibility: boolean = false;
  specificTrackVisibility: boolean = false;
  scheduleModuleVisibility: boolean = false;
  scheduleModuleTrackVisibility: boolean = false;
  scheduleModuleSessionVisibility: boolean = false;
  specificGroupVisibility: boolean = false;

  // tracks
  tracks: Array<Track>;
  public group = []; // array de ids
  public query = ''; // string digitada pelo usuário
  public filteredList = []; // lista de sugestões exibidas
  public selectedTracks = []; // array com os tracks selecionados

  // sessions
  sessions: Array<Session>;
  sessionsNames: Array<any>;
  public session = [];
  public querySession = ''; // string digitada pelo usuário
  public filteredSession = []; // lista de sugestões exibidas
  public selectedSessions = []; // array com os tracks selecionados

  // groups
  groups: Array<Group>;
  groupsNames: Array<any>;
  public ManagerGroup = []; // array de ids
  public queryGroup = ''; // string digitada pelo usuário
  public filteredGroup = []; // lista de sugestões exibidas
  public selectedGroups = []; // array com os tracks selecionados

  scheduleModules: Array<any> = [];
  feedbackType: string = TypeSessionFeedback.AllSessions;
  scheduleModuleFeedbackEdit: string = null;
  scheduleModuleTrackFeedbackEdit: string = null;
  scheduleModuleSessionFeedbackEdit: string = null;


  listAnswers: Array<any> = [];

  listNewQuestion: Array<any> = [];

  listDeleteInputAnswers: Array<any> = [];


  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private dbEvent: DbEventsProvider,
    private dbSession: DbScheduleProvider,
    private dbGroup: DbGroupsProvider,
    private dbFeedback: DbSessionFeedbackProvider,
    private fb: FormBuilder,
    private router: Router,
    private global: GlobalService,
    private formatIdioma: FormatedMultiIdiomaService
  ) {
    this.eventId = this.eventId = this.route.pathFromRoot[1]['params']['_value']['uid'];
    this.moduleId = this.route.snapshot.params['moduleId'];
    this.feedbackId = this.route.snapshot.params['sessionFeedbackId']

    this.getEvent();
    this.getUserLanguage()

    this.load();

    this.formGroup = new FormGroup({
      feedback_name_pt_br: new FormControl(''),
      feedback_name_en_us: new FormControl(''),
      feedback_name_es_es: new FormControl(''),
      feedback_name_fr_fr: new FormControl(''),
      feedback_name_de_de: new FormControl(''),
      changeAnswer: new FormControl('')
    });


    /**
     * Formulario editQuestion
     */
    this.formEditQuestion = this.fb.group({
      'title_pt_br': [''],
      'title_en_us': [''],
      'title_es_es': [''],
      'title_fr_fr': [''],
      'title_de_de': [''],
      'infobooth_pt_br': [''],
      'infobooth_en_us': [''],
      'infobooth_es_es': [''],
      'infobooth_fr_fr': [''],
      'infobooth_de_de': [''],
      'radioTypeQuestion': [null],
      'points': [null]
      // 'radioTypeGraphic': [null]
    });

    /**
     * Formulario create New Question
     */
    this.formNewQuestion = this.fb.group({
      'title_pt_br': [''],
      'title_en_us': [''],
      'title_es_es': [''],
      'title_fr_fr': [''],
      'title_de_de': [''],
      'infobooth_pt_br': [''],
      'infobooth_en_us': [''],
      'infobooth_es_es': [''],
      'infobooth_fr_fr': [''],
      'infobooth_de_de': [''], 
      'radioTypeQuestion': [null],
      'points': [null]
      // 'radioTypeGraphic': [null]
    });

    this.listNewQuestion.push(new Answer());

    // //Seta o icone inicial
    let initialIcon = {
      icon: PathIcons.icon_session_feedback,
      family: 'material-icons'
    }
    this.setIcon(initialIcon);

    this.getFeedbackOptions();

    this.dbFeedback.getQuestions(this.moduleId, this.feedbackId, (list) => {
      this.questions = list;
      this.loaderQuestions = Array(this.questions.length).fill(false);
    })
  }

  ngOnInit() {
    $("label.icon-selector").click(function () {
      $("#dropdownMenuButton").dropdown("toggle");
    });
  }

  // get the language of the user.
  getUserLanguage() {
    this.global.getLanguage((language) => {
      this.userLanguage = language
    })
  }

  getEvent() {
    this.dbEvent.getEvent(this.eventId, (event) => {
      this.eventTimezone = event.timezone
      this.eventLanguage = event.language // get the language of the event.
      this.languages = event.languages;
      this.activeLanguage = this.formatIdioma.convertLangFormat(event.language);
    })
  }

  setIcon(item) {
    this.selectedIcon = item.icon;
    this.selectedIconFamily = item.family;
    $('.dropdown-menu').removeClass('show');
  }

  getFeedbackOptions() {
    this.dbFeedback.getFeedback(this.moduleId, this.feedbackId, (retorno) => {
      this.feedback = retorno;
      this.feedback.weight !== undefined ? this.activeWeight = this.feedback.weight : this.feedback.weight = false;
      this.feedback.marker !== undefined ? this.activeMarker = this.feedback.marker : this.feedback.marker = false;
      this.setValuesFeedback();
    })
  }

  setValuesFeedback() {
    if (this.feedback.type != TypeSessionFeedback.AllSessions) {
      this.feedbackType = this.feedback.type;
      this.changeSessionType();

      switch (this.feedbackType) {
        case TypeSessionFeedback.ScheduleModule:
          this.scheduleModuleFeedbackEdit = this.feedback.module_id;
          break;

        case TypeSessionFeedback.SessionTrack:
          this.scheduleModuleTrackFeedbackEdit = this.feedback.module_id;
          this.specificTrackVisibility = true;
          this.loadTracks(this.scheduleModuleTrackFeedbackEdit);

          for (let trackId of this.feedback.references) {
            this.dbSession.getTrackModule(this.scheduleModuleTrackFeedbackEdit, trackId, (track) => {
              let notFound = true;
              for (let aux of this.selectedTracks) {
                if (aux.uid == track.uid) {
                  notFound = false;
                }
              }

              if (notFound == true) {
                this.selectedTracks.push(track);
              }

            })
          }

          break;

        case TypeSessionFeedback.SpecificGroup:
          this.specificGroupVisibility = true;
          this.loadGroups();

          for (let groupId of this.feedback.references) {
            //marcador
            this.dbGroup.searchModulesAndGroups(this.eventId, (data) => {
              let groups = data['groups'];

              let notFound = true;
              for (let group of groups) {
                if (group.uid == groupId) {

                  for (let aux of this.selectedGroups) {
                    if (aux.uid == group.uid) {
                      notFound = false;
                    }
                  }

                  if (notFound) {
                    this.selectedGroups.push(group);
                  }

                  break;
                }
              }
            })
          }
          break;

        case TypeSessionFeedback.SpecificSession:
          this.scheduleModuleSessionFeedbackEdit = this.feedback.module_id;
          this.SelectedSessionVisibility = true;
          this.loadSessions(this.scheduleModuleSessionFeedbackEdit);

          for (let sessionId of this.feedback.references) {
            this.dbSession.getSession(sessionId, this.scheduleModuleSessionFeedbackEdit, (session: Session) => {

              let notFound = true;
              for (let aux of this.selectedSessions) {
                if (aux.uid == session.uid) {
                  notFound = false;
                }
              }

              if (notFound == true) {
                this.selectedSessions.push(session)
              }
            })
          }

          break;
      }
    }

    this.formGroup.patchValue({
      feedback_name_pt_br: this.feedback.title.PtBR,
      feedback_name_en_us: this.feedback.title.EnUS,
      feedback_name_es_es: this.feedback.title.EsES,
      feedback_name_fr_fr: this.feedback.title.FrFR,
      feedback_name_de_de: this.feedback.title.DeDE,
      // maxResponses: this.feedback.max_responses,
      changeAnswer: this.feedback.change_answer
    })
  }

  updateFeedback(data) {
    let feedback = new SessionFeedback();
    feedback.icon = this.selectedIcon;
    feedback.iconFamily = this.selectedIconFamily;
    let aux = new NameModule(data.feedback_name_pt_br, data.feedback_name_en_us, data.feedback_name_es_es, data.feedback_name_fr_fr, data.feedback_name_de_de);
    feedback.title = this.formatIdioma.updateObjectMultiLanguages(this.feedback.title, aux, this.eventLanguage);
    feedback.change_answer = data.changeAnswer;
    feedback.type = this.feedbackType;
    feedback.weight = this.activeWeight;
    feedback.marker = this.activeMarker;

    switch (feedback.type) {
      case TypeSessionFeedback.AllSessions:
        feedback.module_id = null;
        feedback.references = null;
        break;

      case TypeSessionFeedback.ScheduleModule:
        feedback.module_id = this.scheduleModuleFeedbackEdit;
        feedback.references = null;
        break;

      case TypeSessionFeedback.SessionTrack:
        feedback.module_id = this.scheduleModuleTrackFeedbackEdit;
        this.dbFeedback.forFeedbackReferences(this.selectedTracks, this.feedbackId, this.moduleId);
        // this.daoFeedback.forFeedbackTracksUids(this.selectedTracks, this.feedbackId, this.eventId, this.scheduleModuleTrackFeedbackEdit);
        break;

      case TypeSessionFeedback.SpecificSession:
        feedback.module_id = this.scheduleModuleSessionFeedbackEdit;
        this.dbFeedback.forFeedbackReferences(this.selectedSessions, this.feedbackId, this.moduleId);
        // this.daoFeedback.forFeedbackSessionsUids(this.selectedSessions, this.feedbackId, this.eventId);
        break;

      case TypeSessionFeedback.SpecificGroup:
        feedback.module_id = null;
        this.dbFeedback.forFeedbackReferences(this.selectedGroups, this.feedbackId, this.moduleId);
        // this.daoFeedback.forFeedbackGroupsUids(this.selectedGroups, this.feedbackId, this.eventId);
        break;
    }

    this.dbFeedback.updateFeedback(this.moduleId, this.feedbackId, feedback, (data) => {
      if (data == true) {
        this.successSwal.fire();

        setTimeout(() => {
          this.router.navigate([`/event/${this.eventId}/interactivity/session-feedback/${this.moduleId}`]);
        }, 500);
      }
    });

  }

  /**
   * Abre Collapse da questao por index
   */
  openQuestion(index) {
    /**
     * get info answers for index
     */
    this.editQuestionIndex = index;
    this.editQuestionShow = true;
    this.allowShowAddQuestion = false;
    for (const i in this.questions) {
      if (i == index) {
        this.questionInfo(this.questions[i]);
        this.loadAnswer(this.questions[i].uid);
        window.scroll({
          top: 160,
          behavior: 'smooth'
        })
        break;
      }
    }
  }

  resetEditQuestion() {
    this.editQuestionShow = false;
    this.allowShowAddQuestion = true;
    this.formEditQuestion.reset();
  }

  /***
   * Delete question
   */
  deleteQuestion(index) {
    this.loaderQuestions[index] = true;

    if(this.questions.length > 1) {
      const deleteId = this.questions[index].uid;
      this.dbFeedback.removeQuestion(this.moduleId, this.feedbackId, deleteId, (status) => {
        if (status.code == 200) {
          this.questions.splice(index, 1);
          this.loaderQuestions[index] = false;
          this.loaderQuestions.splice(index, 1);
          this.successSwal.fire();
        }
      })
    } else {
      this.errorDeleteLastQuestion.fire();
      this.loaderQuestions[index] = false;
    }
  }

  /**
   * Pega os valores da Questions
   */
  questionInfo(data) {
    switch (data.type) {
      case 'oneselect':
        // this.blockGraphicTypevisibilityEdit = true;
        this.blockOptionsAnswersVisibilityEdit = true;
        this.blockQuestionPointsVisibilityEdit = false;

        if (this.listAnswers.length <= 0) {
          this.listAnswers.push(new Answer());
        }
        break;

      case 'multipleSelect':
        // this.blockGraphicTypevisibilityEdit = true;
        this.blockOptionsAnswersVisibilityEdit = true;
        this.blockQuestionPointsVisibilityEdit = false;

        if (this.listAnswers.length <= 0) {
          this.listAnswers.push(new Answer());
        }
        break;

      case 'evaluation':
        // this.blockGraphicTypevisibilityEdit = true;
        this.blockOptionsAnswersVisibilityEdit = false;
        this.blockQuestionPointsVisibilityEdit = true;
        break;

      case 'dissertative':
        // this.blockGraphicTypevisibilityEdit = false;
        this.blockOptionsAnswersVisibilityEdit = false;
        this.blockQuestionPointsVisibilityEdit = true;
        break;

      case 'yesOrNo':
        // this.blockGraphicTypevisibilityEdit = false;
        this.blockOptionsAnswersVisibilityEdit = false;
        this.blockQuestionPointsVisibilityEdit = true;
        break;
    }
    this.questionId = data.uid;
    this.formEditQuestion.reset();
    this.formEditQuestion.patchValue({
      title_pt_br: data.title.PtBR,
      title_en_us: data.title.EnUS,
      title_es_es: data.title.EsES,
      title_fr_fr: data.title.FrFR,
      title_de_de: data.title.DeDE,
      infobooth_pt_br: data.infobooth.PtBR,
      infobooth_en_us: data.infobooth.EnUS,
      infobooth_es_es: data.infobooth.EsES,
      infobooth_fr_fr: data.infobooth.FrFR,
      infobooth_de_de: data.infobooth.DeDE,
      radioTypeQuestion: data.type,
      points: data.points
      // radioTypeGraphic: data.graphic,
    });


  }

  changeTypeQuestionEdit() {
    let value = this.formEditQuestion.get('radioTypeQuestion').value;

    switch (value) {
      case 'oneSelect':
        // this.blockGraphicTypevisibilityEdit = true;
        this.blockOptionsAnswersVisibilityEdit = true;
        this.blockQuestionPointsVisibilityEdit = false;

        if (this.listAnswers.length <= 0) {
          this.listAnswers.push(new Answer());
        }
        break;

      case 'multipleSelect':
        // this.blockGraphicTypevisibilityEdit = true;
        this.blockOptionsAnswersVisibilityEdit = true;
        this.blockQuestionPointsVisibilityEdit = false;

        if (this.listAnswers.length <= 0) {
          this.listAnswers.push(new Answer());
        }
        break;

      case 'evaluation':
        // this.blockGraphicTypevisibilityEdit = true;
        this.blockOptionsAnswersVisibilityEdit = false;
        this.blockQuestionPointsVisibilityEdit = true;
        break;

      case 'dissertative':
        // this.blockGraphicTypevisibilityEdit = false;
        this.blockOptionsAnswersVisibilityEdit = false;
        this.blockQuestionPointsVisibilityEdit = true;
        break;

      case 'yesOrNo':
        // this.blockGraphicTypevisibilityEdit = false;
        this.blockOptionsAnswersVisibilityEdit = false;
        this.blockQuestionPointsVisibilityEdit = true;
        break;
    }
  }

  changeTypeQuestionCreate() {
    let value = this.formNewQuestion.get('radioTypeQuestion').value;

    switch (value) {
      case 'oneSelect':
        // this.blockGraphicTypevisibilityCreate = true;
        this.blockOptionsAnswersVisibilityCreate = true;
        this.blockQuestionPointsVisibilityCreate = false;

        if (this.listAnswers.length <= 0) {
          this.listAnswers.push(new Answer());
        }
        break;

      case 'multipleSelect':
        // this.blockGraphicTypevisibilityCreate = true;
        this.blockOptionsAnswersVisibilityCreate = true;
        this.blockQuestionPointsVisibilityCreate = false;

        if (this.listAnswers.length <= 0) {
          this.listAnswers.push(new Answer());
        }
        break;

      case 'evaluation':
        // this.blockGraphicTypevisibilityCreate = true;
        this.blockOptionsAnswersVisibilityCreate = false;
        this.blockQuestionPointsVisibilityCreate = true;
        break;

      case 'dissertative':
        // this.blockGraphicTypevisibilityCreate = false;
        this.blockOptionsAnswersVisibilityCreate = false;
        this.blockQuestionPointsVisibilityCreate = true;
        break;

      case 'yesOrNo':
        // this.blockGraphicTypevisibilityCreate = false;
        this.blockOptionsAnswersVisibilityCreate = false;
        this.blockQuestionPointsVisibilityCreate = true;
        break;
    }
  }

  /**
   * Pega os valores das Answers na DAO
   */
  loadAnswer(questionId) {
    this.dbFeedback.getAnswers(this.moduleId, this.feedbackId, questionId, (list) => {
      this.answers = [];
      this.listAuxInput = [];
      for (const data of list) {
        this.listAuxInput.push(data);
        this.answers.push(data);
      }

      this.carryInputsAnswer(this.answers);
      this.inputValueAnswer(this.listAuxInput);
    })
    // this.daoFeedback.getAnswers(questionId).then((list: Array<any>) => {

    //   this.answers = [];
    //   this.listAuxInput = [];
    //   for (const data of list) {
    //     this.listAuxInput.push(data);
    //     this.answers.push(data);
    //   }
    //   this.carryInputsAnswer(this.answers);
    //   this.inputValueAnswer(this.listAuxInput);
    // });
  }

  /**
   * Carrega os valores das answers no input
   */
  inputValueAnswer(data) {
    let index = 0;
    this.listAnswers = [];

    for (const aux of data) {
      this.listAnswers[index] = aux;
      index++;
    }
  }

  /**
   * Cria os clones de inputs do formulario Answer
   */
  cloneInputAnswers() {
    this.listAnswers.push(new Answer());
  }

  /**
   * Delete input answers
   */
  getDeleteInputAnswers(index) {
    const deleteId = this.listAnswers[index].uid;

    if (deleteId !== undefined) {
      var questionId = this.questionId;
      var deleteAnswer = [questionId, deleteId];
      this.listDeleteInputAnswers.push(deleteAnswer);
      this.listAnswers.splice(index, 1);
    }
  }

  deleteInputAnswers() {
    for (let aux of this.listDeleteInputAnswers) {
      var questionId = aux[0];
      var deleteId = aux[1];

      this.dbFeedback.deleteAnswer(this.moduleId, this.feedbackId, questionId, deleteId, (status) => {

      });
    }
  }

  /**
   *  Carrega os clones dos inputs de acordo com o total de respostas que o formulario possui
   */
  carryInputsAnswer(data) {
    let cont = data.length;
    let print = 1;
    while (cont >= 1) {
      if (print < data.length) {
        this.cloneInputAnswers();
        print++;
      }
      cont--;
    }
  }

  /**
   * Submit formulario Edit Question
   */
  editQuestion(data) {
    let indexQuestion = this.getIndexQuestion(this.questionId);

    this.errorTitleEdit = false;
    this.errorSelectQuestionEdit = false;
    this.errorOptionAnswerEdit = false;

    const newQuestion = new Question();
    let auxTitle = new NameModule(data.title_pt_br, data.title_en_us, data.title_es_es, data.title_fr_fr, data.title_de_de);
    let auxInfobooth = new NameModule(data.infobooth_pt_br, data.infobooth_en_us, data.infobooth_es_es, data.infobooth_fr_fr, data.infobooth_de_de);
    newQuestion.title = this.formatIdioma.updateObjectMultiLanguages(this.questions[this.editQuestionIndex].title, auxTitle, this.eventLanguage) 
    newQuestion.infobooth = this.formatIdioma.updateObjectMultiLanguages(this.questions[this.editQuestionIndex].infobooth, auxInfobooth, this.eventLanguage)
    newQuestion.type = data.radioTypeQuestion;
    newQuestion.points = data.points;
    newQuestion.uid = this.questionId;
    newQuestion.createdAt = this.questions[indexQuestion].createdAt;

    if (newQuestion.type == 'oneSelect' || newQuestion.type == 'multipleSelect') {
      for (const answer of this.listAnswers) {
        newQuestion.answers.push(answer);
      }
    }

    newQuestion.title = this.formatIdioma.fillEmptyLanguage(newQuestion.title, this.eventLanguage);

    //Caso o usuário não tenha inserido o titulo da pergunta
    if (newQuestion.title == undefined || newQuestion.title == null || (newQuestion.title.PtBR == "" &&
      newQuestion.title.EnUS == "" && newQuestion.title.EsES == "" && newQuestion.title.FrFR == "" &&
      newQuestion.title.DeDE == "")) {
      this.errorTitleEdit = true;
    }

    if (newQuestion.type == undefined || newQuestion.type == null || newQuestion.type == "") {
      this.errorSelectQuestionEdit = true;
    }

    if (newQuestion.type == 'oneSelect' || newQuestion.type == 'multipleSelect') {
      //Caso não tenha adicionado nenhuma resposta possível
      let find = false;

      for (let answer of this.listAnswers) {
        if (!(answer.answer == undefined) && !(answer.answer == null) && (answer.answer.PtBR != ""
          || answer.answer.EnUS != "" || answer.answer.EsES != "" || answer.answer.FrFR != ""
          || answer.answer.DeDE != "")) {
          find = true;
        }
      }

      if (find == false) {
        this.errorOptionAnswerEdit = true;
      }
    }

    if (!(this.errorTitleEdit || this.errorSelectQuestionEdit || this.errorOptionAnswerEdit)) {
      this.dbFeedback.updateQuestion(this.moduleId, this.feedbackId, this.questionId, newQuestion, this.eventTimezone, (status) => {
        this.questions[this.editQuestionIndex].title = newQuestion.title;
        newQuestion.infobooth = new NameModule(data.infobooth_pt_br, data.infobooth_en_us, data.infobooth_es_es, data.infobooth_fr_fr, data.infobooth_de_de);
        this.questions[this.editQuestionIndex].type = newQuestion.type;
        this.questions[this.editQuestionIndex].points = newQuestion.points;

        this.editQuestionShow = false;
        this.addQuestionShow = false;
        this.allowShowAddQuestion = true;
      });

      this.deleteInputAnswers();

      this.allowShowAddQuestion = true;
    }

  }

  getIndexQuestion(questionId) {
    return this.questions.map(function (e) { return e.uid; }).indexOf(questionId);
  }


  //----------------------------------
  //         Create New Question
  //----------------------------------

  /**
   * Cria os clones de inputs do formulario Answer
   */
  cloneInputNewQuestion() {
    this.listNewQuestion.push(new Answer());
  }

  /**
   * Delete input answers
   */
  deleteInputNewQuestion(index) {
    this.listNewQuestion.splice(index, 1);
  }

  /**
   * Create new Question
   */
  newQuestion(data) {
    this.errorTitleCreate = false;
    this.errorSelectQuestionCreate = false;
    this.errorOptionAnswerCreate = false;

    let newQuestion = new Question();
    newQuestion.title = new NameModule(data.title_pt_br, data.title_en_us, data.title_es_es, data.title_fr_fr, data.title_de_de);
    newQuestion.infobooth = new NameModule(data.infobooth_pt_br, data.infobooth_en_us, data.infobooth_es_es, data.infobooth_fr_fr, data.infobooth_de_de);
    newQuestion.type = data.radioTypeQuestion;
    newQuestion.points = data.points;

    newQuestion.title = this.formatIdioma.fillEmptyLanguage(newQuestion.title, this.eventLanguage);
    newQuestion.infobooth = this.formatIdioma.fillEmptyLanguage(newQuestion.infobooth, this.eventLanguage);
    newQuestion.infobooth = this.replaceLinkInfooboth(newQuestion.infobooth);

    //Caso o usuário não tenha inserido o titulo da pergunta
    if (newQuestion.title == undefined || newQuestion.title == null || (newQuestion.title.PtBR == ""
      && newQuestion.title.EnUS == "" && newQuestion.title.EsES == "" && newQuestion.title.FrFR == ""
      && newQuestion.title.DeDE == "")) {
      this.errorTitleCreate = true;
    }

    if (newQuestion.type == undefined || newQuestion.type == null || newQuestion.type == "") {
      this.errorSelectQuestionCreate = true;
    }

    if (newQuestion.type == 'oneSelect' || newQuestion.type == 'multipleSelect') {
      //Caso não tenha adicionado nenhuma resposta possível
      let find = false;

      for (let answer of this.listNewQuestion) {
        if (answer.answer != undefined && answer.answer != null && (answer.answer.PtBR != ""
          || answer.answer.EnUS != "" || answer.answer.EsES != "" || answer.answer.FrFR != ""
          || answer.answer.DeDE != "")) {

          find = true;
        }
      }

      if (find == false) {
        this.errorOptionAnswerCreate = true;
      }
    }



    if (!(this.errorTitleCreate || this.errorSelectQuestionCreate || this.errorOptionAnswerCreate)) {
      if (newQuestion.type == 'oneSelect' || newQuestion.type == 'multipleSelect') {
        let cont = 0;
        for (let aux of this.listNewQuestion) {
          let answer = new Answer();

          answer.answer = aux.answer;
          aux.weight == undefined ? answer.weight = null : answer.weight = aux.weight;
          aux.marker == undefined ? answer.marker = null : answer.marker = aux.marker;

          if (answer.answer !== undefined && answer.answer !== null && (answer.answer.PtBR != ""
            || answer.answer.EnUS != "" || answer.answer.EsES != "" || answer.answer.FrFR != ""
            || answer.answer.DeDE != "")) {

            newQuestion.answers.push(answer)
          }

          if (cont == this.listNewQuestion.length - 1) {
            this.dbFeedback.createOneQuestions(this.moduleId, this.feedbackId, newQuestion, this.eventTimezone, (data) => {
              if (data.status == true) {
                newQuestion.uid = data.questionId;
                this.questions.push(newQuestion);
                this.loaderQuestions.push(false);
              }

              this.editQuestionShow = false;
              this.addQuestionShow = false;
              this.allowShowAddQuestion = true;
            });
          }
          
          cont++;
        }
      } else {
        this.dbFeedback.createOneQuestions(this.moduleId, this.feedbackId, newQuestion, this.eventTimezone, (data) => {
          if (data.status == true) {
            newQuestion.uid = data.questionId;
            this.questions.push(newQuestion);
            this.loaderQuestions.push(false);
          }

          this.editQuestionShow = false;
          this.addQuestionShow = false;
          this.allowShowAddQuestion = true;
        });
      }


      //Clear form new question, close modal and show/hide blocks
      this.formNewQuestion.reset();
      // this.blockGraphicTypevisibilityCreate = true;
      this.blockOptionsAnswersVisibilityCreate = true;
      $('#modalFeedback').modal('toggle');

      this.listNewQuestion = [];
      this.listNewQuestion.push(new Answer());
      this.addQuestionShow = false;

    }

  }

  cancelNewQuestion() {

    //Clear form new question, close modal and show/hide blocks
    $(function () {
      $('#form-questions').each(function () {
        this.reset();
        $("input[name=newRadioTypeQuestion]").prop('checked', false);
        // $("input[name=radioTypeGraphic]").prop('checked', false);
        // $('#answer-options').hide('slow');
        // $('#chart-options').hide('slow');
      });
    })

    this.listNewQuestion = [];
    this.listNewQuestion.push(new Answer());
    this.addQuestionShow = false;
  }




  load() {
    this.dbEvent.getModulesSchedule(this.eventId, (data) => {
      this.scheduleModules = data;
    });
  }

  // LOAD ALL TRACKS OF SCHEDULE MODULE 
  private loadTracks(moduleId) {
    this.dbSession.getModuleTracks(moduleId, (tracks) => {
      // this.dao.tracksSessionEvent(moduleId, (tracks) => {
      this.tracks = [];
      this.group = [];
      this.group = tracks;
      for (const track of tracks) {
        this.tracks.push(track);
      }
    });
  }

  // receive group name and return index of the group  to get id group
  findIdTrack(track: Track) {
    const index = this.tracks.map(function (e) {
      return new GetNameTrack().transform(e, [this.userLanguage, this.eventLanguage]); 
    }).indexOf(new GetNameTrack().transform(track, [this.userLanguage, this.eventLanguage]));

    return index;
  }

  // autocomplete
  filter() { //  função usa a query variável para filtrar tracks, então ela armazena o resultado no filteredList.
    if (this.query !== "") {
      this.filteredList = this.tracks.filter(function (el) {
        let elementStr = new GetNameTrack().transform(el, [this.userLanguage, this.eventLanguage]);
        return elementStr.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
      }.bind(this));
    } else {
      this.filteredList = [];
    }
  }

  // atribuir o item selecionado para a variável de consulta, a fim de fazê-lo aparecer na entrada, e para fazer a lista de sugestões de desaparecer
  select(item) {
    if (this.selectedTracks.length > 0) {
      const index = this.selectedTracks.indexOf(item);
      if (index == -1) {
        this.selectedTracks.push(item);
      }
    } else {
      this.selectedTracks.push(item);
    }

    this.query = '';
    this.filteredList = [];
  }

  remove(item) { // remove group from display list
    this.selectedTracks.splice(this.selectedTracks.indexOf(item), 1);
  }



  loadSessions(moduleId) {
    this.dbSession.getSessionsModule(moduleId, (list: Array<Session>) => {
      this.sessions = [];
      this.session = list;

      this.sessionsNames = [];
      for (const session of list) {
        this.sessionsNames.push(new GetNameSession().transform(session, [this.userLanguage, this.eventLanguage]))
        // this.sessions.push(session);
      }
    })
    // this.dao.sessionsModule(eventId, moduleId, (list: Array<Session>) => {
    //   this.sessions = [];
    //   this.session = list;

    //   this.sessionsNames = [];
    //   for (const session of list) {
    //     this.sessionsNames.push(session.name);
    //   }
    // });
  }

  // autocomplete
  filterSession() { //  função usa a query variável para filtrar tracks, então ela armazena o resultado no filteredList.
    if (this.querySession !== "") {
      this.filteredSession = this.session.filter(function (el) {
         // search session using user language
         switch (this.userLanguage) {
          case ('pt_BR'):
            return el.name.PtBR.toLowerCase().indexOf(this.querySession.toLowerCase()) > -1;

          case ('en_US'):
            return el.name.EnUS.toLowerCase().indexOf(this.querySession.toLowerCase()) > -1;

          case ('es_ES'):
            return el.name.EsES.toLowerCase().indexOf(this.querySession.toLowerCase()) > -1;


          case ('fr_FR'):
            return el.name.FrFR.toLowerCase().indexOf(this.querySession.toLowerCase()) > -1;

          case ('de_DE'):
            return el.name.DeDE.toLowerCase().indexOf(this.querySession.toLowerCase()) > -1;
        }
      }.bind(this));
    } else {
      this.filteredSession = [];
    }
  }

  // receive session name and return index of the session to get id session
  findIdSession(session: Session) {
    const index = this.session.map(function (e) { return e.name; }).indexOf(session.name);
    return index;
  }

  // atribuir o item selecionado para a variável de consulta, a fim de fazê-lo aparecer na entrada, e para fazer a lista de sugestões de desaparecer
  selectSession(item) {
    if (this.selectedSessions.length > 0) {
      const index = this.selectedSessions.map(function (e) { return e.uid; }).indexOf(item.uid);
      
      if (index == -1) {
        this.selectedSessions.push(item);
      }
    } else {
      this.selectedSessions.push(item);
    }

    this.querySession = '';
    this.filteredSession = [];
  }

  removeSession(item) { // remove group from display list
    this.selectedSessions.splice(this.selectedSessions.indexOf(item), 1);
    // this.dao.notAllowGroupCheckin(this.eventId, this.groupId);
  }

  loadGroups() {
    this.dbGroup.searchModulesAndGroups(this.eventId, (response) => {
      this.groups = [];
      this.groups = response['groups'];
    });
  }

  // autocomplete
  filterGroup() { //  função usa a query variável para filtrar tracks, então ela armazena o resultado no filteredList.
    if (this.queryGroup !== "") {
      this.filteredGroup = this.groups.filter(function (el) {
        return el.name.toLowerCase().indexOf(this.queryGroup.toLowerCase()) > -1;
      }.bind(this));
    } else {
      this.filteredGroup = [];
    }
  }

  // receive session name and return index of the session to get id session
  findIdGroup(group: Group) {
    const index = this.groups.map(function (e) { return e.name; }).indexOf(group.name);
    return index;
  }

  // atribuir o item selecionado para a variável de consulta, a fim de fazê-lo aparecer na entrada, e para fazer a lista de sugestões de desaparecer
  selectGroup(item) {
    if (this.selectedGroups.length > 0) {
      const index = this.selectedGroups.indexOf(item);
      if (index == -1) {
        this.selectedGroups.push(item);
      }
    } else {
      this.selectedGroups.push(item);
    }

    this.queryGroup = '';
    this.filteredGroup = [];
  }

  removeGroup(item) { // remove group from display list
    this.selectedGroups.splice(this.selectedGroups.indexOf(item), 1);
    // this.dao.notAllowGroupCheckin(this.eventId, this.groupId);
  }

  //verifica se o feedback será vinculado ou não a uma sessão
  // changeTypeSessionFeedback() {
  //   if (this.feedbackLinkedToSession) {
  //     //caso o feedback sejá vinculado a uma sessão, automaticamente o valor inicial se torna todas as sessões
  //     this.feedbackType = TypeSessionFeedback.AllSessions;
  //   } else {
  //     this.feedbackLinkedToSession = false;
  //     this.selectedSessions = [];
  //     this.feedbackType = TypeSessionFeedback.Pattern;

  //     this.SelectedSessionVisibility = false;
  //     this.tracksAttendeeVisibility = false;
  //     this.scheduleModuleTrackVisibility = false;
  //     this.scheduleModuleSessionVisibility = false;
  //     this.specificTrackVisibility = false;
  //     this.specificGroupVisibility = false;
  //   }
  // }

  //caso o feedback seja vinculado as sessões, verifica se será em todas ou em uma especifica e altera o feedbackType
  changeSessionType() {
    switch (this.feedbackType) {
      case 'AllSessions':
        this.feedbackType = TypeSessionFeedback.AllSessions;

        this.SelectedSessionVisibility = false;
        this.tracksAttendeeVisibility = false;
        this.scheduleModuleTrackVisibility = false;
        this.scheduleModuleSessionVisibility = false;
        this.specificTrackVisibility = false;
        this.specificGroupVisibility = false;

        //Esvazia o array de sessões selecionadas posteriormente.
        this.selectedSessions = [];
        break;

      case 'ScheduleModule':
        this.feedbackType = TypeSessionFeedback.ScheduleModule;

        this.SelectedSessionVisibility = false;
        this.tracksAttendeeVisibility = false;
        this.scheduleModuleVisibility = true;
        this.scheduleModuleTrackVisibility = false;
        this.scheduleModuleSessionVisibility = false;
        this.specificTrackVisibility = false;
        this.specificGroupVisibility = false;
        break;

      case 'SessionTrack':
        this.feedbackType = TypeSessionFeedback.SessionTrack;

        this.SelectedSessionVisibility = false;
        this.tracksAttendeeVisibility = false;
        this.scheduleModuleVisibility = false;
        this.scheduleModuleSessionVisibility = false;
        this.scheduleModuleTrackVisibility = true;
        this.specificGroupVisibility = false;
        break;

      case 'SpecificSession':
        this.feedbackType = TypeSessionFeedback.SpecificSession;

        this.SelectedSessionVisibility = false;
        this.tracksAttendeeVisibility = false;
        this.scheduleModuleVisibility = false;
        this.scheduleModuleTrackVisibility = false;
        this.scheduleModuleSessionVisibility = true;
        this.specificTrackVisibility = false;
        this.specificGroupVisibility = false;
        break;

      case 'SpecificGroup':
        this.feedbackType = TypeSessionFeedback.SpecificGroup;

        this.SelectedSessionVisibility = false;
        this.tracksAttendeeVisibility = false;
        this.scheduleModuleVisibility = false;
        this.scheduleModuleTrackVisibility = false;
        this.scheduleModuleSessionVisibility = false;
        this.specificTrackVisibility = false;
        this.specificGroupVisibility = true;
        this.loadGroups();
        break;
    }
  }

  moduleSelectedChange() {
    this.selectedGroups = [];
    this.selectedSessions = [];
    this.selectedTracks = [];

    var trackId = this.scheduleModuleTrackFeedbackEdit;
    var sessionId = this.scheduleModuleSessionFeedbackEdit;
    var specificModule = this.scheduleModuleFeedbackEdit;

    if (trackId == undefined || trackId == null || trackId == '') {
      this.specificTrackVisibility = false;
    } else {
      this.specificTrackVisibility = true;
      this.loadTracks(trackId);
    }

    if (sessionId == undefined || sessionId == null || sessionId == '') {
      this.SelectedSessionVisibility = false;
    } else {
      this.SelectedSessionVisibility = true;
      this.loadSessions(sessionId);
    }
  }

  resetValuesForm() {
    this.SelectedSessionVisibility = false;
    this.tracksAttendeeVisibility = false;
    this.specificTrackVisibility = false;
    this.scheduleModuleVisibility = false;
    this.scheduleModuleTrackVisibility = false;
    this.scheduleModuleSessionVisibility = false;
    this.specificGroupVisibility = false;

    // tracks
    this.tracks = [];
    this.group = []; // array de ids
    this.query = ''; // string digitada pelo usuário
    this.filteredList = []; // lista de sugestões exibidas
    this.selectedTracks = []; // array com os tracks selecionados

    // sessions
    this.sessions = [];
    this.sessionsNames = [];
    this.session = [];
    this.querySession = ''; // string digitada pelo usuário
    this.filteredSession = []; // lista de sugestões exibidas
    this.selectedSessions = []; // array com os tracks selecionados

    // groups
    this.groups = [];
    this.groupsNames = [];
    this.ManagerGroup = []; // array de ids
    this.queryGroup = ''; // string digitada pelo usuário
    this.filteredGroup = []; // lista de sugestões exibidas
    this.selectedGroups = []; // array com os tracks selecionados

    this.scheduleModules = [];
    this.feedbackType = TypeSessionFeedback.AllSessions;
    this.scheduleModuleFeedbackEdit = null;
    this.scheduleModuleTrackFeedbackEdit = null;
    this.scheduleModuleSessionFeedbackEdit = null;
  }

  cancel() {
    this.location.back();
  }

  replaceLinkInfooboth(data) {
    data['PtBR'] == null ? data['PtBR'] = '' : data['PtBR'] = data['PtBR'].replace(/href="/g, 'class="wysiwyg-link" href="');
    data['EnUS'] == null ? data['EnUS'] = '' : data['EnUS'] = data['EnUS'].replace(/href="/g, 'class="wysiwyg-link" href="');
    data['EsES'] == null ? data['EsES'] = '' : data['EsES'] = data['EsES'].replace(/href="/g, 'class="wysiwyg-link" href="');
    data['FrFR'] == null ? data['FrFR'] = '' : data['FrFR'] = data['FrFR'].replace(/href="/g, 'class="wysiwyg-link" href="');
    data['DeDE'] == null ? data['DeDE'] = '' : data['DeDE'] = data['DeDE'].replace(/href="/g, 'class="wysiwyg-link" href="');

    return data;
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DbTrainingProvider } from 'src/app/providers/database/db-training';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { GlobalService } from 'src/app/providers/global/global.service';
import { Languages } from 'src/app/models/languages';
import { DragulaService } from 'ng2-dragula';
import { DragulaOptions } from 'dragula';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';

type AOA = Array<Array<any>>;
declare let $: any;

@Component({
    selector: 'app-training',
    templateUrl: './training.component.html',
    styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

    @ViewChild('successSwal') public successSwal: SwalComponent;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;
    @ViewChild('clearTrainingSwal') public clearTrainingSwal: SwalComponent;
    @ViewChild('removeTrainingSwal') public removeTrainingSwal: SwalComponent;
    @ViewChild('clearQuestionSwal') public clearQuestionSwal: SwalComponent;

    dragulaOptions: DragulaOptions = {
        moves: () => true,
    }

    onReorderShow: boolean = false;
    loaderOrder: boolean = false;
    trainingView: boolean = true;
    moduleId: string = null
    eventId: string = null;
    eventTimezone: string = null;
    loader: boolean = true;
    public trainings: Array<any> = [];
    public trainingRemoveId;
    trainingRemoveIndex;
    public trainingClearId;
    public questionClearId;

    languages: Languages = null //event languages
    // get the language of the user.
    public userLanguage: string;
    public exportLanguage: string = null;
    public trainingExportId: string = null;
    public questionExportId: string = null;
    public typeExport: string = null;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private db: DbTrainingProvider,
        private dbEvent: DbEventsProvider,
        private global: GlobalService,
        private dragula: DragulaService,
        private luxon: LuxonService) {
        // get module id
        this.eventId = this.route.pathFromRoot[1]['params']['_value']['uid'];
        this.moduleId = this.route.snapshot.params['moduleId'];

        dragula.createGroup('bag-training', this.dragulaOptions);
    }

    ngAfterContentChecked() {
        let auxRoute = this.router.url;
        if (auxRoute.includes('create-training') || auxRoute.includes('edit-training')) {
            this.trainingView = false;
        } else {
            this.trainingView = true;
        }
    }

    ngOnInit() {
        // start dragula reorder bag
        this.dragula.dropModel('bag-training').subscribe((value: any) => {
            this.onReorder(value);
        });

        this.db.getTrainings(this.moduleId, (trainings: Array<any>) => {
            this.trainings = [];
            this.trainings = trainings;
            this.loader = false;
        })

        this.getEventTimezone();
        this.getUserLanguage();
        this.getEvent();
    }

    ngOnDestroy() {
        // remove dragula reorder bag case exit component
        this.dragula.destroy('bag-training');
    }


    // update order of modules
    onReorder(order: any): void {
        this.onReorderShow = true;
        this.trainings = order.targetModel;
        for (let i = 0; i < (this.trainings).length; ++i) {
            this.trainings[i].order = i;
        }
    }


    saveNewOrder() {
        this.loaderOrder = true;

        this.db.changeOrder(this.moduleId, this.trainings, (result) => {
            if (result == true) {
                this.successSwal.fire();
            } else {
                this.errorSwal.fire();
            }

            this.loaderOrder = false;
            this.onReorderShow = false;
        })
    }

    getEvent() {
        this.dbEvent.getEvent(this.eventId, (event) => {
            this.exportLanguage = this.convertLangFormat(event.language);
            this.languages = event.languages;
        })
    }

    convertLangFormat(lang) {
        let formatedLang;

        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    // get the language of the user.
    getUserLanguage() {
        this.global.getLanguage((language) => {
            this.userLanguage = language
        })
    }

    getEventTimezone() {
        this.dbEvent.getTimezone(this.eventId, (timezone) => {
            this.eventTimezone = timezone;
        })
    }

    changeVisibilityTraining(trainingId, visibility) {
        this.db.changeVisibility(this.moduleId, trainingId, !visibility);
    }

    changeVisibilityQuestion(trainingId, questionId, visibility, trainingIndex, questionIndex) {
        this.db.changeVisibilityQuestion(this.moduleId, trainingId, questionId, !visibility);


        if (visibility) {
            this.trainings[trainingIndex].questions[questionIndex].visibility = false;
        } else {
            this.trainings[trainingIndex].questions[questionIndex].visibility = true;
        }
    }

    enableAll() {
        for (let training of this.trainings) {
            this.db.changeVisibility(this.moduleId, training.uid, true);
        }
    }

    disableAll() {
        for (let training of this.trainings) {
            this.db.changeVisibility(this.moduleId, training.uid, false);
        }
    }

    expandirQuestions(index, training) {
        (training.expand) ? training.expand = false : training.expand = true;
        let aux = '.hidden';
        aux = aux.concat(index);

        if (this.trainings[index].questions.length > 0) {
            this.showAndHiddenCollapse(aux);
        } else {

            this.db.getQuestions(this.moduleId, this.trainings[index].uid, (questions) => {
                this.trainings[index].questions = questions;

                setTimeout(() => {
                    this.showAndHiddenCollapse(aux);
                }, 1000);
            })

        }
    }

    /**
     * Hide or show collapse based on a query
     * @param query 
     */
    showAndHiddenCollapse(query) {
        if ($(query).hasClass('hideAll')) {
            $(query).removeClass('hideAll');
            $(query).show();
        } else {
            $(query).addClass('hideAll');
            $(query).hide();
        }
    }

    getRemoveQuestion(trainingId, questionId, index) {

    }

    removeQuestion() {

    }

    getRemoveTraining(trainingId, index) {
        this.trainingRemoveId = trainingId;
        this.trainingRemoveIndex = index;
        this.removeTrainingSwal.fire();
    }

    removeTraining() {
        this.loader = true;
        this.db.removeTraining(this.moduleId, this.trainingRemoveId, (data) => {
            if (data.code == 200) {
                this.successSwal.fire();
                this.trainings.splice(this.trainingRemoveIndex, 1);
                this.loader = false;
            }
        })
    }

    clearAllResults() {
        let listTrainingsIds = [];
        this.loader = true;
        for (let training of this.trainings) {
            listTrainingsIds.push(training.uid);
        }

        this.db.clearAllTrainings(this.moduleId, listTrainingsIds, (data) => {
            if (data.code == 200) {
                this.successSwal.fire();
                this.loader = false;
            }
        })
    }

    getTrainingClear(training) {
        this.trainingClearId = training.uid;
        this.clearTrainingSwal.fire();
    }

    clearTraining() {
        this.db.clearTraining(this.moduleId, this.trainingClearId, (data) => {
            if (data.code == 200) {
                this.successSwal.fire();
            }
        })
    }

    getQuestionClear(trainingId, questionId) {
        this.trainingClearId = trainingId;
        this.questionClearId = questionId;
        this.clearQuestionSwal.fire();
    }

    clearQuestion() {
        this.db.clearQuestion(this.moduleId, this.trainingClearId, this.questionClearId, (data) => {
            if (data.code == 200) {
                this.successSwal.fire();
            }
        })
    }

    exportData() {
        $('#exportModal').modal('hide');
        switch (this.typeExport) {
            case 'training':
                this.exportOneTraining(this.trainingExportId, this.exportLanguage)
                break;

            case 'question':
                this.exportQuestion(this.trainingExportId, this.questionExportId, this.exportLanguage)
                break;

            case 'allTraining':
                this.exportTrainings(this.exportLanguage)
                break;
        }
    }

    exportTrainings(language) {
        $('#exportLoading').modal({ backdrop: 'static', keyboard: false })

        let dataExportTraining: Array<any> = [];
        let cont = 0;
        for (let training of this.trainings) {
            this.prepareExportTraining(training.uid, language, (data) => {

                if (data !== null) {
                    dataExportTraining.push(data)
                }

                if (cont == this.trainings.length - 1) {
                    $('#exportLoading').modal('toggle');
                    this.export(dataExportTraining, 'Trainings');
                }

                cont++;
            })

        }
    }

    exportOneTraining(trainingId, language) {
        $('#exportLoading').modal({ backdrop: 'static', keyboard: false })

        this.prepareExportTraining(trainingId, language, (data) => {
            let dataExportTraining: Array<any> = [];

            if (data !== null) {
                dataExportTraining.push(data)
                $('#exportLoading').modal('toggle');
                this.export(dataExportTraining, 'Training');
            } else {
                $('#exportLoading').modal('toggle');
            }
        })
    }

    exportQuestion(trainingId, questionId, language) {
        let dataExport = [];
        dataExport = [[
            'Question',
            'Name',
            'E-mail',
            'Answer',
            'Marker',
            'Question Check',
            'Date'
        ]];

        this.db.exportQuestion(this.moduleId, trainingId, questionId, language, (data) => {
            for (let result of data.result) {
                dataExport.push(this.prepareResult(result));
            }

            let dataExportQuestion = [dataExport];
            this.export(dataExportQuestion, 'Question')
        })
    }

    prepareExportTraining(trainingId, language, onResolve) {

        let dataExportTraining = [];
        dataExportTraining = [[
            'Question',
            'Name',
            'E-mail',
            'Answer',
            'Marker',
            'Question Check',
            'Date'
        ]];

        this.db.exportTrainings(this.moduleId, trainingId, language, (data) => {
            if (data.code == 200) {
                let listResult = data.result;

                if (listResult.length <= 0) {
                    onResolve(null);
                }
                let cont = 0;
                for (let result of listResult) {
                    dataExportTraining.push(this.prepareResult(result));

                    if (cont == listResult.length - 1) {
                        onResolve(dataExportTraining);
                    }
                    cont++;
                }
            } else {
                $('#exportLoading').modal('toggle');
            }
        })

    }

    prepareResult(result) {
        let array = [];
        array[0] = result.question;
        array[1] = result.name;
        array[2] = result.email;
        array[3] = result.answer;
        array[4] = result.marker;

        if (result.verifyQuestion) {
            array[5] = 'Yes';
        } else {
            array[5] = 'No';
        }


        let date = this.luxon.convertTimestampToDate(result.timestamp)
        let day = '0' + date.day;
        let month = '0' + date.month;
        let year = date.year;
        let hours = date.hour;
        let minutes = '0' + date.minute;
        let seconds = '0' + date.second;

        array[6] = day.substr(-2) + '/' + month.substr(-2) + '/' + year + ' - ' + hours + ':' +
            minutes.substr(-2) + ':' + seconds.substr(-2);

        return array;

    }

    export(listExport, fileName) {
        let totalFlaps = listExport.length;
        let result: Array<any> = [];

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();

        for (let i = 0; i < totalFlaps; i++) {

            const wscols: XLSX.ColInfo[] = [
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { wpx: 100 }, // "pixels"
                { hidden: false } // hide column
            ];

            /* At 96 PPI, 1 pt = 1 px */
            const wsrows: XLSX.RowInfo[] = [
                { hpx: 25 }, // "pixels"
            ];

            /* generate worksheet */
            const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(listExport[i]);

            /* TEST: column props */
            ws['!cols'] = wscols;

            /* TEST: row props */
            ws['!rows'] = wsrows;

            result[i] = ws;


            XLSX.utils.book_append_sheet(wb, result[i], 'Sheet' + (i + 1));
        }

        /* save to file */
        const wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
        saveAs(new Blob([this.s2ab(wbout)]), fileName + '.xlsx');
    }

    // AJUDA A GERAR O ARQUIVO EXECL
    private s2ab(s: string): ArrayBuffer {
        const buf: ArrayBuffer = new ArrayBuffer(s.length);
        const view: Uint8Array = new Uint8Array(buf);
        for (let i = 0; i !== s.length; ++i) {
            view[i] = s.charCodeAt(i) & 0xFF;
        }
        return buf;
    }

}

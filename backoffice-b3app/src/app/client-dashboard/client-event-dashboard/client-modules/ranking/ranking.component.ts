import { Component, OnInit } from '@angular/core';
import { DbAttendeesProvider } from 'src/app/providers/database/db-attendees';
import { Attendee } from 'src/app/models/attendees';
import { DbRankingProvider } from 'src/app/providers/database/db-ranking';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/providers/global/global.service';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
type AOA = Array<Array<any>>;

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
  providers: [DbRankingProvider]
})
export class RankingComponent implements OnInit {
  term;

  public module = null

  // get the language of the user.
  public userLanguage: string

  eventId: string;
  moduleId: string;
  attendees: Array<Attendee> = [];
  // EXPORT DATA
  data: any = null;
  attendeeExport: AOA = null;
  loader: boolean = true;
  constructor(
    private dbRanking: DbRankingProvider,
    private route: ActivatedRoute,
    private global: GlobalService,
    private dbEvent: DbEventsProvider
  ) {
    this.eventId = this.route.parent.params['_value']['uid'];
    this.moduleId = this.route.snapshot.params['moduleId'];
  }

  ngOnInit() {
    this.dbRanking.getRankingModule(this.moduleId, (module) => {
      this.module = module;
    });
    this.getEvent();
    this.start();
    this.getUserLanguage()
  }

  event: any = null;
  getEvent() {
    this.dbEvent.getEvent(this.eventId, (event: Event) => {
      this.event = event;
    });
  }

  start() {
    this.dbRanking.getAttendeeRanking(this.eventId, (attendees) => {
      this.attendees = [];

      attendees = attendees.sort(function (a, b) {
        if (a['points'] < b['points']) { return 1; }
        if (a['points'] > b['points']) { return -1; }
        return 0;
      });

      this.attendees = attendees;
      this.loader = false;
    });
  }

  exportRanking() {
    this.attendeeExport = [];
    this.attendeeExport = [[
      'Position',
      'Name',
      'Points'
    ]];
    let cont = 0;
    for (let i = 0; i < this.attendees.length; i++) {
      let row: any;
      let position = i + 1;
      row = this.instatiateAttendee(this.attendees[i], position);
      this.attendeeExport.push(row);
      if (cont == this.attendees.length - 1) {
        const wscols: XLSX.ColInfo[] = [
          { wpx: 100 }, // "pixels"
          { wpx: 100 }, // "pixels"
          { wpx: 100 }, // "pixels"
          // { hidden: false } // hide column
        ];

        /* At 96 PPI, 1 pt = 1 px */
        const wsrows: XLSX.RowInfo[] = [
          { hpx: 25 }, // "pixels"
        ];

        /* generate worksheet */
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.attendeeExport);

        /* TEST: column props */
        ws['!cols'] = wscols;

        /* TEST: row props */
        ws['!rows'] = wsrows;

        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        const wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
        saveAs(new Blob([this.s2ab(wbout)]), 'ranking.xlsx');
        this.data = null;
      }
      cont++;
    }
  }
  // AJUDA A GERAR O ARQUIVO EXECL
  private s2ab(s: string): ArrayBuffer {
    const buf: ArrayBuffer = new ArrayBuffer(s.length);
    const view: Uint8Array = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) {
      view[i] = s.charCodeAt(i) & 0xFF;
    }
    return buf;
  }

  instatiateAttendee(attendee: Attendee, position: number) {
    const row = [];
    let positionString = position.toString();
    row.push(positionString);
    row.push(attendee['name']);
    row.push(attendee['points']);

    return row;
  }

  // get the language of the user.
  getUserLanguage() {
    this.global.getLanguage((language) => {
      this.userLanguage = language
    })
  }
}

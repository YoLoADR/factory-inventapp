import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Session } from '../../../../../models/session';
import { Location } from '../../../../../models/location';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { DbScheduleProvider } from 'src/app/providers/database/db-schedule';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { DbLocationsProvider } from 'src/app/providers/database/db-locations';
import { Track } from 'src/app/models/track';
import { Group } from 'src/app/models/group';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { DbSpeakersProvider } from 'src/app/providers/database/db-speakers';
import { Speaker } from 'src/app/models/speakers';
import { Event } from 'src/app/models/event';
import { DbDocuments } from 'src/app/providers/database/db-documents';
import { GlobalService } from 'src/app/providers/global/global.service';
import { NameSession } from 'src/app/models/name-session';
import { TranslateService } from '@ngx-translate/core';
import { ModuleSchedule } from 'src/app/models/modules/module-schedule';
import { WherebyService } from 'src/app/providers/whereby/whereby.service';

@Component({
    selector: 'app-create-schedule',
    templateUrl: './create-schedule.component.html',
    styleUrls: ['./create-schedule.component.scss']
})

export class CreateScheduleComponent implements OnInit {
    formValidation: FormGroup;
    public eventId: string = this.route.parent.params['_value']['uid'];;
    public moduleId: string = this.route.snapshot.params['moduleId'];

    @ViewChild('successSwal') public successSwal: SwalComponent;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;

    loader: boolean = false;

    module: ModuleSchedule;

    public event: Event
    principalEventLangFormated: string = 'PtBR';
    public languages: Array<any> = [
        { value: "pt_BR", name: this.translateService.instant('comp.event_info.portuguese') },
        { value: "en_US", name: this.translateService.instant('comp.event_info.english') },
        { value: "es_ES", name: this.translateService.instant('comp.event_info.spanish') },
        { value: "fr_FR", name: this.translateService.instant('comp.event_info.french') },
        { value: "de_DE", name: this.translateService.instant('comp.event_info.german') }
    ];
    public userLanguage: string = null //user language is using

    createSessionError: boolean;
    errorFormLocation: boolean
    timeError: boolean

    // automcomplete locations
    listLocations: Array<Location>
    selectedLocations: Array<Location> = [];
    filteredListLocations = [];
    queryLocation = '';

    // tracks
    listTrack: Array<Track>
    selectedTrack: Array<Track> = [];
    filteredListTrack = [];
    queryTrack = '';
    errorFormTrack: boolean = false;

    // GROUPS FILTER

    listGroup: Array<Group>
    selectedGroup: Array<Group> = [];
    filteredListGroups = [];
    queryGroup = '';
    errorFormGroup: boolean = false;

    // SPEAKERS FILTER
    listSpeaker: Array<Speaker>
    selectedSpeaker = [];
    filteredListSpeakers = [];
    querySpeaker = '';
    errorFormSpeaker: boolean = false;

    // DOCUMENTS FILTER
    listDocument: Array<Speaker>
    selectedDocument = [];
    filteredListDocuments = [];
    queryDocument = '';
    errorFormDocument: boolean = false;

    datePast: boolean = false //Boolean variable that checks if the session date is in the past

    // WHEREBY
    wherebyForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private luxon: LuxonService,
        private dbEvent: DbEventsProvider,
        private dbLocation: DbLocationsProvider,
        private dbSchedule: DbScheduleProvider,
        private dbGroups: DbGroupsProvider,
        private dbSpeaker: DbSpeakersProvider,
        private dbDocuments: DbDocuments,
        private global: GlobalService,
        private translateService: TranslateService,
        private SWhereby: WherebyService
    ) {
        this.createSessionError = false;
        this.errorFormLocation = false;
        this.timeError = false
        this.datePast = false

        this.wherebyForm = this.fb.group({
            name: new FormControl(''),
            url: new FormControl(''),
            startDate: new FormControl(''),
            endDate: new FormControl(''),
            mode: new FormControl('normal'),
            allowChatOnVisio: new FormControl(true),
            allowScreenShareOnVisio: new FormControl(true),
            allowLeaveOnVisio: new FormControl(true),
            activated: new FormControl(true)
        })

        this.formValidation = fb.group({
            'date': [null, Validators.compose([Validators.required])],
            'locations': [null],
            'startTime': [null, Validators.compose([Validators.required])],
            'finalTime': [null],
            'speakers': [null],
            'tracks': [null],
            'groups': [null],
            'documents': [null],
            'descriptionlanguages_de_DE': [''],
            'descriptionlanguages_en_US': [''],
            'descriptionlanguages_es_ES': [''],
            'descriptionlanguages_fr_FR': [''],
            'descriptionlanguages_pt_BR': [''],
            'totalAttendees': [0]
        })

        // get the user's primary language.
        this.global.getLanguage((language: string) => {
            this.userLanguage = language
            this.principalEventLangFormated = this.convertLangFormat(this.userLanguage);
        })

        this.getScheduleModule();

        // get the event.
        this.dbEvent.getEvent(this.eventId, (event: Event) => {
            this.event = event;

            // sort array of languages by placing the main language at the top.
            this.sortArrayLanguages()


            // Initializes the formControl of session titles.
            if (this.event.languages.PtBR) {
                this.formValidation.addControl('title_pt_br', new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(1000)])))
            } else {
                this.formValidation.addControl('title_pt_br', new FormControl(''))
            }

            if (this.event.languages.EnUS) {
                this.formValidation.addControl('title_en_us', new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(1000)])))
            } else {
                this.formValidation.addControl('title_en_us', new FormControl(''))
            }

            if (this.event.languages.EsES) {
                this.formValidation.addControl('title_es_es', new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(1000)])))
            } else {
                this.formValidation.addControl('title_es_es', new FormControl(''))
            }

            if (this.event.languages.FrFR) {
                this.formValidation.addControl('title_fr_fr', new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(1000)])))
            } else {
                this.formValidation.addControl('title_fr_fr', new FormControl(''))
            }

            if (this.event.languages.DeDE) {
                this.formValidation.addControl('title_de_de', new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(1000)])))
            } else {
                this.formValidation.addControl('title_de_de', new FormControl(''))
            }
        })

        this.dbLocation.getLocationsEvent(this.eventId, 'asc', (locations) => {
            this.listLocations = [];
            this.listLocations = locations
        });

        this.dbSchedule.getModuleTracks(this.moduleId, (tracks: Array<Track>) => {
            this.listTrack = [];
            this.listTrack = tracks;
        });

        this.dbSpeaker.getSpeakersEvent(this.eventId, (speakers: Array<Speaker>) => {
            this.listSpeaker = []
            this.listSpeaker = speakers
        })

        this.dbGroups.searchModulesAndGroups(this.eventId, (result) => {
            this.listGroup = [];
            this.listGroup = result['groups'];
        });

        this.dbDocuments.getEventDocuments(this.eventId, (result) => {
            this.listDocument = []
            this.listDocument = result
        })
    }

    ngOnInit() { }

    /**
     * Getting schedule module
     */
    getScheduleModule() {
        this.dbSchedule.getScheduleModule(this.moduleId, (module: ModuleSchedule) => {
            this.module = module;
        });
    }

    async createSession(form) {
        let validation = true;
        this.loader = true

        this.createSessionError = false
        this.errorFormLocation = false
        this.errorFormTrack = false
        this.errorFormSpeaker = false
        this.errorFormDocument = false
        this.timeError = false
        this.datePast = false

        // dates
        const arrayDate = form.date.split('-');

        const year = arrayDate[0]
        const month = arrayDate[1];
        const day = arrayDate[2];

        const date = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, '00', '00', '00'));
        console.log("Date created: ", date, " - ", form.date, " - ", new Date(date));
        // start time
        let startTime1 = '00' //hours
        let startTime2 = '00' //minutes
        let startTime3 = '00' //seconds

        const arrayStartTime = form.startTime.split(':')

        // with seconds
        if (arrayStartTime.length === 3) {
            startTime1 = arrayStartTime[0]
            startTime2 = arrayStartTime[1]
            startTime3 = arrayStartTime[2]
        }
        //no seconds
        else {
            startTime1 = arrayStartTime[0]
            startTime2 = arrayStartTime[1]
        }

        const startTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, startTime1, startTime2, startTime3));


        // end-time
        let endTime: any = ""

        // if the end time is filled.
        if (form.finalTime !== null && form.finalTime !== "") {
            let endTime1 = '00' //hours
            let endTime2 = '00' //minutes
            let endTime3 = '00' //seconds

            const arrayEndTime = form.finalTime.split(':');

            // with seconds
            if (arrayEndTime.length === 3) {
                endTime1 = arrayEndTime[0]
                endTime2 = arrayEndTime[1]
                endTime3 = arrayEndTime[2]
            }
            //no seconds
            else {
                endTime1 = arrayEndTime[0]
                endTime2 = arrayEndTime[1]
            }

            endTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, endTime1, endTime2, endTime3));

            if (endTime < startTime) {
                validation = false
                this.loader = false
                this.timeError = true
            }
        }

        const session = new Session();

        // title
        session.name = new NameSession(form.title_pt_br, form.title_en_us, form.title_es_es, form.title_fr_fr, form.title_de_de)

        session.eventId = this.eventId
        session.moduleId = this.moduleId;
        session.date = date
        session.startTime = startTime
        session.endTime = endTime;

        if (form.locations.length > 0) {
            this.errorFormLocation = true;
            validation = false;
        } else {
            let contOrder = 0

            for (let index in this.selectedLocations) {
                const location = this.selectedLocations[index]
                location.order = contOrder
                session.locations[location.uid] = Object.assign({}, location)

                contOrder++
            }
        }

        if (form.speakers) {
            this.errorFormSpeaker = true
            validation = false
        } else {
            for (let index in this.selectedSpeaker) {
                const speaker = this.selectedSpeaker[index]
                session.speakers[speaker.uid] = Object.assign({}, speaker)
            }
        }

        if (form.documents) {
            this.errorFormDocument = true
            validation = false
        } else {
            for (let index in this.selectedDocument) {
                const document = this.selectedDocument[index]
                session.documents[document.uid] = Object.assign({}, document)
            }
        }


        if (this.selectedTrack.length > 0) {
            for (let index in this.selectedTrack) {
                const track = this.selectedTrack[index];
                session.tracks[track['uid']] = Object.assign({}, track)
            }
        }

        if (this.selectedGroup.length > 0) {
            for (let index in this.selectedGroup) {
                const group = this.selectedGroup[index];
                session.groups[group['uid']] = Object.assign({}, group)
            }
        }


        // total attendees
        if (form.totalAttendees !== null && form.totalAttendees !== '' && typeof form.totalAttendees !== 'undefined') {
            session.limitAttendees = form.totalAttendees
        }

        // descriptions
        if (typeof form.descriptionlanguages_pt_BR !== 'undefined') {
            session.descriptions.PtBR = form.descriptionlanguages_pt_BR.replace(/href="/g, 'class="wysiwyg-link" href="')
        } else {
            session.descriptions.PtBR = ''
        }

        if (typeof form.descriptionlanguages_en_US !== 'undefined') {
            session.descriptions.EnUS = form.descriptionlanguages_en_US.replace(/href="/g, 'class="wysiwyg-link" href="')
        } else {
            session.descriptions.EnUS = ''
        }

        if (typeof form.descriptionlanguages_de_DE !== 'undefined') {
            session.descriptions.DeDE = form.descriptionlanguages_de_DE.replace(/href="/g, 'class="wysiwyg-link" href="')
        } else {
            session.descriptions.DeDE = ''
        }

        if (typeof form.descriptionlanguages_es_ES !== 'undefined') {
            session.descriptions.EsES = form.descriptionlanguages_es_ES.replace(/href="/g, 'class="wysiwyg-link" href="')
        } else {
            session.descriptions.EsES = ''
        }

        if (typeof form.descriptionlanguages_fr_FR !== 'undefined') {
            session.descriptions.FrFR = form.descriptionlanguages_fr_FR.replace(/href="/g, 'class="wysiwyg-link" href="')
        } else {
            session.descriptions.FrFR = ''
        }

        // checks if the start date is in the past
        let now = new Date();
        let dayNow: any = now.getDate().toString()
        let monthNow: any = now.getMonth() + 1
        monthNow.toString()
        const yearNow: any = now.getFullYear().toString()
        let hourNow: any = now.getHours().toString()
        let minuteNow: any = now.getMinutes().toString()

        if (dayNow < 10) {
            dayNow = `0${dayNow}`
        }

        if (monthNow < 10) {
            monthNow = `0${monthNow}`
        }

        if (minuteNow < 10) {
            minuteNow = `0${minuteNow}`
        }

        if (hourNow < 10) {
            hourNow = `0${hourNow}`
        }

        const timeStampNow = this.luxon.createTimeStamp(this.luxon.createDate(yearNow, monthNow, dayNow, hourNow, minuteNow, '00'))


        if (session.startTime < timeStampNow) {
            validation = false
            this.datePast = true
        }


        if (validation) {
            try {
                // Create room whereby if needed
                let startDateArray = (this.wherebyForm.value.startDate) ? this.wherebyForm.value.startDate.split('-') : [];
                let startDate = (startDateArray.length > 0) ? this.luxon.createDate(startDateArray[0], startDateArray[1], startDateArray[2], '00', '00', '00').toISO() : null;
                let endDateArray = (this.wherebyForm.value.endDate) ? this.wherebyForm.value.endDate.split('-') : [];
                let endDate = (endDateArray.length > 0) ? this.luxon.createDate(endDateArray[0], endDateArray[1], endDateArray[2], '00', '00', '00').toISO() : null;

                if (this.event.allow_visio && startDate && endDate && startDate < endDate) {
                    if (session.visio && session.visio.meetingId) {
                        await this.SWhereby.deleteMeeting(session.visio.meetingId);
                    }

                    let meeting: any = {
                        "roomNamePrefix": "/" + ((this.wherebyForm.value.name) ? this.wherebyForm.value.name : (session.visio.roomName) ? session.visio.roomName : "default"),
                        "roomMode": (this.wherebyForm.value.mode) ? this.wherebyForm.value.mode : "normal",
                        "startDate": (startDate) ? startDate : null,
                        "endDate": (endDate) ? endDate : null,
                    }

                    let resp = await this.SWhereby.createMeeting(meeting)

                    if (resp.body && resp.body.meetingId && resp.body.roomUrl) {
                        session.visio = {
                            meetingId: (resp.body.meetingId) ? resp.body.meetingId : "",
                            roomUrl: (resp.body.roomUrl) ? resp.body.roomUrl : "",
                            startDate: (startDate) ? startDate : null,
                            endDate: (endDate) ? endDate : null,
                            roomMode: (this.wherebyForm.value.mode) ? this.wherebyForm.value.mode : "normal",
                            roomName: (this.wherebyForm.value.name) ? this.wherebyForm.value.name : (session.visio.roomName) ? session.visio.roomName : "default",
                            allowChatOnVisio: this.wherebyForm.value.allowChatOnVisio,
                            allowScreenShareOnVisio: this.wherebyForm.value.allowScreenShareOnVisio,
                            allowLeaveOnVisio: this.wherebyForm.value.allowLeaveOnVisio,
                            activated: this.wherebyForm.value.activated
                        }
                    }
                }

                this.dbSchedule.getIdentifiersSessions(this.eventId, (identifiers) => {
                    session.identifier = this.identifier(identifiers);

                    this.dbSchedule.createSession(session, (success) => {
                        if (success) {
                            this.loader = false;
                            this.successSwal.fire();
                        } else {
                            this.errorSwal.fire()
                            this.loader = false;
                        }
                    })
                })
            } catch (error) {
                this.createSessionError = true;
                this.loader = false;
            }
        } else {
            this.loader = false
        }
    }

    identifier(identifiers) {
        let id = 1;
        let flag = false;

        while (!flag) {
            const pos = identifiers.map(function (e) { return e; }).indexOf(id);

            if (pos <= -1) {
                flag = true;
                break;
            }

            id++;
        }
        return id;
    }

    // sort array of languages by placing the main language at the top.
    sortArrayLanguages() {
        const index = this.languages.map(function (e) { return e.value; }).indexOf(this.event.language);

        // put the main language at index zero of the array.
        if (index > 0) {
            const aux = this.languages[0]
            this.languages[0] = this.languages[index]
            this.languages[index] = aux;
        }
    }


    redirectList() {
        this.router.navigate(['/event/' + this.eventId + '/schedule/' + this.moduleId]);
    }

    // autocomplete locations

    filterLocations() { //  funÃ§Ã£o usa a query variÃ¡vel para filtrar tracks, entÃ£o ela armazena o resultado no filteredList.
        if (this.queryLocation !== "") {
            this.filteredListLocations = this.listLocations.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryLocation.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListLocations = [];
        }
    }

    selectLocation(item) { // atribuir o item selecionado para a variÃ¡vel de consulta, a fim de fazÃª-lo aparecer na entrada, e para fazer a lista de sugestÃµes de desaparecer
        if (this.selectedLocations.length > 0) {
            const index = this.selectedLocations.indexOf(item);
            if (index == -1) {
                this.selectedLocations.push(item);
            }
        } else {
            this.selectedLocations.push(item);
        }

        this.queryLocation = '';
        this.filteredListLocations = [];
    }

    removeLocation(item) { // remove valores selecionados
        this.selectedLocations.splice(this.selectedLocations.indexOf(item), 1);
    }

    // filter track
    filterTracks() {
        if (this.queryTrack !== "") {
            this.filteredListTrack = this.listTrack.filter(function (el) {
                return el.name[this.principalEventLangFormated].toLowerCase().indexOf(this.queryTrack.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListTrack = [];
        }
    }

    // select track from filtered list
    selectTrack(item) {
        if (this.selectedTrack.length > 0) {
            const index = this.selectedTrack.indexOf(item);
            if (index == -1) {
                this.selectedTrack.push(item);
            }
        } else {
            this.selectedTrack.push(item);
        }
        this.queryTrack = '';
        this.filteredListTrack = [];
    }

    // remove selected location
    removeTrack(item) {
        this.selectedTrack.splice(this.selectedTrack.indexOf(item), 1);
    }

    /******************* GROUPS **********************/

    // filter groups
    filterGroups() {
        if (this.queryGroup !== "") {
            this.filteredListGroups = this.listGroup.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroup.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroups = [];
        }
    }

    // select track from filtered list
    selectGroup(item) {
        if (this.selectedGroup.length > 0) {
            const index = this.selectedGroup.indexOf(item);
            if (index == -1) {
                this.selectedGroup.push(item);
            }
        } else {
            this.selectedGroup.push(item);
        }
        this.queryGroup = '';
        this.filteredListGroups = [];
    }

    // remove selected location
    removeGroup(item) {
        this.selectedGroup.splice(this.selectedGroup.indexOf(item), 1);
    }

    /******************* SPEAKERS **********************/

    // filter speakers
    filterSpeakers() {
        if (this.querySpeaker !== "") {
            this.filteredListSpeakers = this.listSpeaker.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.querySpeaker.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListSpeakers = [];
        }
    }

    // select speaker from filtered list
    selectSpeaker(item) {
        if (this.selectedSpeaker.length > 0) {
            const index = this.selectedSpeaker.indexOf(item);
            if (index == -1) {
                this.selectedSpeaker.push(item);
            }
        } else {
            this.selectedSpeaker.push(item);
        }
        this.querySpeaker = '';
        this.filteredListSpeakers = [];
    }

    // remove selected speaker
    removeSelectedSpeakers(item) {
        this.selectedSpeaker.splice(this.selectedSpeaker.indexOf(item), 1);
    }

    /******************* DOCUMENTS **********************/

    // filter documents
    filterDocuments() {
        if (this.queryDocument !== "") {
            this.filteredListDocuments = this.listDocument.filter(function (el) {
                return el.name[this.principalEventLangFormated].toLowerCase().indexOf(this.queryDocument.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListDocuments = [];
        }
    }

    // select document from filtered list
    selectDocument(item) {
        if (this.selectedDocument.length > 0) {
            const index = this.selectedDocument.indexOf(item);
            if (index == -1) {
                this.selectedDocument.push(item);
            }
        } else {
            this.selectedDocument.push(item);
        }
        this.queryDocument = '';
        this.filteredListDocuments = [];
    }

    // remove selected document
    removeSelectedDocuments(item) {
        this.selectedDocument.splice(this.selectedDocument.indexOf(item), 1);
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';


// models
import { Session } from 'src/app/models/session';
import { Location } from 'src/app/models/location';
import { Event } from 'src/app/models/event'

// providers
import { DbScheduleProvider } from 'src/app/providers/database/db-schedule';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { DbLocationsProvider } from 'src/app/providers/database/db-locations';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { Track } from 'src/app/models/track';
import { Group } from 'src/app/models/group';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { DbSpeakersProvider } from 'src/app/providers/database/db-speakers';
import { Speaker } from 'src/app/models/speakers';
import { DbDocuments } from 'src/app/providers/database/db-documents';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/providers/global/global.service';
import { WherebyService } from 'src/app/providers/whereby/whereby.service';
import { ModuleSchedule } from 'src/app/models/modules/module-schedule';
import { ModuleDocuments } from 'src/app/models/modules/module-documents';

import * as moment from 'moment';

@Component({
    selector: 'app-edit-schedule',
    templateUrl: './edit-schedule.component.html',
    styleUrls: ['./edit-schedule.component.scss']
})

export class EditScheduleComponent implements OnInit {
    public eventId: string = this.route.parent.params['_value']['uid'];;
    public moduleId: string = this.route.snapshot.params['moduleId'];
    public sessionId: string = this.route.snapshot.params['scheduleId']

    module: ModuleSchedule;

    formValidation: FormGroup;
    public languages = [];
    loader

    @ViewChild('successSwal') public successSwal: SwalComponent;


    // automcomplete locations
    listLocations: Array<Location>
    selectedLocations: Array<Location> = [];
    filteredListLocations = [];
    queryLocation = '';

    // automcomplete tracks
    listTracks: Array<Track>
    selectedTracks: Array<Track> = [];
    filteredListTracks = [];
    queryTrack = '';
    errorFormTrack: boolean = false;

    createSessionError
    errorFormLocation
    timeError

    identifier = 0

    // GROUPS FILTER
    listGroup: Array<Group>
    selectedGroup: Array<Group> = [];
    filteredListGroups = [];
    queryGroup = '';
    errorFormGroup: boolean = false;

    // SPEAKERS FILTER
    listSpeaker: Array<Speaker>
    selectedSpeaker = [];
    filteredListSpeakers = [];
    querySpeaker = '';
    errorFormSpeaker: boolean = false;

    // DOCUMENTS FILTER
    listDocument: Array<Speaker>
    selectedDocument = [];
    filteredListDocuments = [];
    queryDocument = '';
    errorFormDocument: boolean = false;

    sessionEdit: Session;
    public event: Event
    principalEventLangFormated: string = 'PtBR';
    public userLanguage: string     // get the language of the user.

    datePast: boolean = false //Boolean variable that checks if the session date is in the past

    // WHEREBY
    wherebyForm: FormGroup;
    wherebyError: string[] = [];
    activationVisioUpdated: boolean = false;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private dbSchedule: DbScheduleProvider,
        private dbLocation: DbLocationsProvider,
        private dbEvent: DbEventsProvider,
        private luxon: LuxonService,
        private dbGroups: DbGroupsProvider,
        private dbSpeaker: DbSpeakersProvider,
        private dbDocuments: DbDocuments,
        private translateService: TranslateService,
        private global: GlobalService,
        private SWhereby: WherebyService
    ) {
        this.loader = true

        this.createSessionError = false
        this.errorFormLocation = false
        this.errorFormSpeaker = false
        this.timeError = false
        this.datePast = false

        this.wherebyForm = this.fb.group({
            name: new FormControl(''),
            url: new FormControl(''),
            startDate: new FormControl(''),
            endDate: new FormControl(''),
            mode: new FormControl('normal'),
            allowChatOnVisio: new FormControl(true),
            allowScreenShareOnVisio: new FormControl(true),
            allowLeaveOnVisio: new FormControl(true),
            activated: new FormControl(true)
        })

        // languages
        this.languages = [
            { value: "pt_BR", name: this.translateService.instant('comp.event_info.portuguese') },
            { value: "en_US", name: this.translateService.instant('comp.event_info.english') },
            { value: "es_ES", name: this.translateService.instant('comp.event_info.spanish') },
            { value: "fr_FR", name: this.translateService.instant('comp.event_info.french') },
            { value: "de_DE", name: this.translateService.instant('comp.event_info.german') }
        ]

        this.formValidation = fb.group({
            'date': [null, Validators.compose([Validators.required])],
            'locations': [null],
            'startTime': [this.minDate('start'), Validators.compose([Validators.required])],
            'finalTime': [this.minDate('end')],
            'speakers': [null],
            'tracks': [null],
            'groups': [null],
            'documents': [null],
            'totalAttendees': [0],
            'descriptionlanguages_de_DE': [''],
            'descriptionlanguages_en_US': [''],
            'descriptionlanguages_es_ES': [''],
            'descriptionlanguages_fr_FR': [''],
            'descriptionlanguages_pt_BR': [''],
        })

        this.getScheduleModule();

        this.dbSchedule.getSession(this.sessionId, this.moduleId, (sessionEdit: Session) => {
            this.loader = false

            if (typeof sessionEdit !== 'undefined' && sessionEdit !== null) {
                this.sessionEdit = sessionEdit;
                this.identifier = sessionEdit.identifier;
                if (this.sessionEdit.visio) {
                    this.wherebyForm.patchValue({
                        url: (this.sessionEdit.visio.roomUrl) ? this.sessionEdit.visio.roomUrl : '',
                        mode: (this.sessionEdit.visio.roomMode) ? this.sessionEdit.visio.roomMode : 'normal',
                        startDate: (this.sessionEdit.visio.startDate) ? this.sessionEdit.visio.startDate.split('T')[0] : '',
                        endDate: (this.sessionEdit.visio.endDate) ? this.sessionEdit.visio.endDate.split('T')[0] : '',
                        allowChatOnVisio: (this.sessionEdit.visio) ? this.sessionEdit.visio.allowChatOnVisio : true,
                        allowScreenShareOnVisio: (this.sessionEdit.visio) ? this.sessionEdit.visio.allowScreenShareOnVisio : true,
                        allowLeaveOnVisio: (this.sessionEdit.visio) ? this.sessionEdit.visio.allowLeaveOnVisio : true,
                        activated: (this.sessionEdit.visio) ? this.sessionEdit.visio.activated : true,
                    })
                }

                this.sessionEdit.date = this.luxon.convertDateToStringYearMonthDay(this.luxon.convertTimestampToDate(this.sessionEdit.date));
                this.sessionEdit.startTime = this.luxon.dateTimeWithSeconds(this.luxon.convertTimestampToDate(this.sessionEdit.startTime))

                if (this.sessionEdit.endTime !== "" && this.sessionEdit.endTime !== null && typeof this.sessionEdit.endTime !== 'undefined') {
                    this.sessionEdit.endTime = this.luxon.dateTimeWithSeconds(this.luxon.convertTimestampToDate(this.sessionEdit.endTime))
                }

                // init formValidation
                this.formValidation.patchValue({
                    'date': this.sessionEdit.date,
                    'startTime': this.sessionEdit.startTime,
                    'finalTime': this.sessionEdit.endTime,
                    'totalAttendees': this.sessionEdit.limitAttendees,
                    'descriptionlanguages_de_DE': this.sessionEdit.descriptions.DeDE,
                    'descriptionlanguages_en_US': this.sessionEdit.descriptions.EnUS,
                    'descriptionlanguages_es_ES': this.sessionEdit.descriptions.EsES,
                    'descriptionlanguages_fr_FR': this.sessionEdit.descriptions.FrFR,
                    'descriptionlanguages_pt_BR': this.sessionEdit.descriptions.PtBR,
                })

                this.global.getLanguage((language) => {
                    this.userLanguage = language
                    this.principalEventLangFormated = this.convertLangFormat(this.userLanguage);
                })

                // get event
                this.dbEvent.getEvent(this.eventId, (event: Event) => {
                    this.event = event

                    // sort array of languages by placing the main language at the top.
                    this.sortArrayLanguages()

                    // Initializes the formControl of session titles.
                    if (this.event.languages.PtBR) {
                        this.formValidation.addControl('title_pt_br', new FormControl(this.sessionEdit.name.PtBR, Validators.compose([Validators.required, Validators.maxLength(1000)])))
                    } else {
                        this.formValidation.addControl('title_pt_br', new FormControl(this.sessionEdit.name.PtBR))
                    }

                    if (this.event.languages.EnUS) {
                        this.formValidation.addControl('title_en_us', new FormControl(this.sessionEdit.name.EnUS, Validators.compose([Validators.required, Validators.maxLength(1000)])))
                    } else {
                        this.formValidation.addControl('title_en_us', new FormControl(this.sessionEdit.name.EnUS))
                    }

                    if (this.event.languages.EsES) {
                        this.formValidation.addControl('title_es_es', new FormControl(this.sessionEdit.name.EsES, Validators.compose([Validators.required, Validators.maxLength(1000)])))
                    } else {
                        this.formValidation.addControl('title_es_es', new FormControl(this.sessionEdit.name.EsES))
                    }

                    if (this.event.languages.FrFR) {
                        this.formValidation.addControl('title_fr_fr', new FormControl(this.sessionEdit.name.FrFR, Validators.compose([Validators.required, Validators.maxLength(1000)])))
                    } else {
                        this.formValidation.addControl('title_fr_fr', new FormControl(this.sessionEdit.name.FrFR))
                    }

                    if (this.event.languages.DeDE) {
                        this.formValidation.addControl('title_de_de', new FormControl(this.sessionEdit.name.DeDE, Validators.compose([Validators.required, Validators.maxLength(1000)])))
                    } else {
                        this.formValidation.addControl('title_de_de', new FormControl(this.sessionEdit.name.DeDE))
                    }
                })

                // locations
                this.selectedLocations = []
                for (const uid in sessionEdit.locations) {
                    this.selectedLocations.push(sessionEdit.locations[uid])
                }

                // sort by the order field.
                this.selectedLocations.sort(function (a, b) {
                    return a.order - b.order;
                });

                // tracks
                this.selectedTracks = []
                for (const uid in sessionEdit.tracks) {
                    this.selectedTracks.push(sessionEdit.tracks[uid])
                }

                // groups
                this.selectedGroup = [];
                for (const uid in sessionEdit.groups) {
                    this.selectedGroup.push(sessionEdit.groups[uid]);
                }

                // speakers
                this.selectedSpeaker = []
                for (const uid in sessionEdit.speakers) {
                    this.selectedSpeaker.push(sessionEdit.speakers[uid]);
                }

                // documents
                this.selectedDocument = []
                for (const uid in this.sessionEdit.documents) {
                    this.selectedDocument.push(this.sessionEdit.documents[uid])
                }
            }
        })

        this.dbLocation.getLocationsEvent(this.eventId, 'asc', (locations) => {
            this.listLocations = [];
            this.listLocations = locations
        });

        this.dbGroups.searchModulesAndGroups(this.eventId, (result) => {
            this.listGroup = result['groups'];
        });

        this.dbSchedule.getModuleTracks(this.moduleId, (tracks: Array<Track>) => {
            this.listTracks = [];
            this.listTracks = tracks;
        });

        this.dbSpeaker.getSpeakersEvent(this.eventId, (speakers: Array<Speaker>) => {
            this.listSpeaker = []
            this.listSpeaker = speakers
        })

        this.dbDocuments.getEventDocuments(this.eventId, (documents) => {
            this.listDocument = []
            this.listDocument = documents
        })
    }

    ngOnInit() {

    }

    /**
     * Min date for datepicker
     */
    minDate(startEnd: string) {
        return ((startEnd == 'start') ? moment().format('YYYY-MM-DD') : moment().add(1, 'day').format('YYYY-MM-DD'));
    }

    /**
     * Getting schedule module
     */
    getScheduleModule() {
        this.dbSchedule.getScheduleModule(this.moduleId, (module: ModuleSchedule) => {
            this.module = module;
        });
    }

    // sort array of languages by placing the main language at the top.
    sortArrayLanguages() {
        const index = this.languages.map(function (e) { return e.value; }).indexOf(this.event.language);

        // put the main language at index zero of the array.
        if (index > 0) {
            const aux = this.languages[0]
            this.languages[0] = this.languages[index]
            this.languages[index] = aux;
        }
    }

    /**
     * Update session
     * @param form 
     */
    async updateSession(form) {
        let validation = true;

        this.createSessionError = false;
        this.errorFormLocation = false;
        this.errorFormTrack = false;
        this.errorFormSpeaker = false
        this.errorFormDocument = false
        this.timeError = false
        this.datePast = false

        this.loader = true;

        // dates
        const arrayDate = form.date.split('-');

        const year = arrayDate[0];
        const month = arrayDate[1];
        const day = arrayDate[2];

        const date = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, '00', '00', '00'));

        // start time
        // start time
        let startTime1 = '00' //hours
        let startTime2 = '00' //minutes
        let startTime3 = '00' //seconds

        const arrayStartTime = form.startTime.split(':')

        // with seconds
        if (arrayStartTime.length === 3) {
            startTime1 = arrayStartTime[0]
            startTime2 = arrayStartTime[1]
            startTime3 = arrayStartTime[2]
        }
        //no seconds
        else {
            startTime1 = arrayStartTime[0]
            startTime2 = arrayStartTime[1]
        }

        const startTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, startTime1, startTime2, startTime3));


        // end-time
        let endTime: any = ""

        // if the end time is filled.
        if (form.finalTime !== null && form.finalTime !== "") {
            let endTime1 = '00' //hours
            let endTime2 = '00' //minutes
            let endTime3 = '00' //seconds

            const arrayEndTime = form.finalTime.split(':');

            // with seconds
            if (arrayEndTime.length === 3) {
                endTime1 = arrayEndTime[0]
                endTime2 = arrayEndTime[1]
                endTime3 = arrayEndTime[2]
            }
            //no seconds
            else {
                endTime1 = arrayEndTime[0]
                endTime2 = arrayEndTime[1]
            }

            endTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, endTime1, endTime2, endTime3));

            if (endTime < startTime) {
                validation = false
                this.loader = false
                this.timeError = true
            }
        }

        // titles
        if (typeof form.title_pt_br !== 'undefined' && form.title_pt_br !== null) {
            this.sessionEdit.name.PtBR = form.title_pt_br
        } else {
            this.sessionEdit.name.PtBR = form.title_pt_br
        }

        if (typeof form.title_en_us !== 'undefined' && form.title_en_us !== null) {
            this.sessionEdit.name.EnUS = form.title_en_us
        } else {
            this.sessionEdit.name.EnUS = form.title_en_us
        }

        if (typeof form.title_es_es !== 'undefined' && form.title_es_es !== null) {
            this.sessionEdit.name.EsES = form.title_es_es
        } else {
            this.sessionEdit.name.EsES = form.title_es_es
        }

        if (typeof form.title_fr_fr !== 'undefined' && form.title_fr_fr !== null) {
            this.sessionEdit.name.FrFR = form.title_fr_fr
        } else {
            this.sessionEdit.name.FrFR = form.title_fr_fr
        }

        if (typeof form.title_de_de !== 'undefined' && form.title_de_de !== null) {
            this.sessionEdit.name.DeDE = form.title_de_de
        } else {
            this.sessionEdit.name.DeDE = form.title_de_de
        }


        this.sessionEdit.date = date
        this.sessionEdit.startTime = startTime
        this.sessionEdit.endTime = endTime;

        this.sessionEdit.locations = {}
        if (form.locations.length > 0) {
            this.errorFormLocation = true;
            validation = false;
            this.loader = false
        } else {
            let contOrder = 0

            for (let index in this.selectedLocations) {
                const location = this.selectedLocations[index]
                location.order = contOrder
                this.sessionEdit.locations[location.uid] = Object.assign({}, location)

                contOrder++
            }
        }

        this.sessionEdit.documents = {}
        if (form.documents) {
            this.errorFormDocument = true
            validation = false
        } else {
            for (let index in this.selectedDocument) {
                const document = this.selectedDocument[index]
                this.sessionEdit.documents[document.uid] = Object.assign({}, document)
            }
        }

        this.sessionEdit.tracks = {}
        if (this.selectedTracks.length > 0) {
            for (let index in this.selectedTracks) {
                const track = this.selectedTracks[index];
                this.sessionEdit.tracks[track['uid']] = Object.assign({}, track)
            }
        }

        this.sessionEdit.groups = {}
        if (this.selectedGroup.length > 0) {
            for (let index in this.selectedGroup) {
                const group = this.selectedGroup[index];
                this.sessionEdit.groups[group['uid']] = Object.assign({}, group)
            }
        }

        this.sessionEdit.speakers = {}
        if (form.speakers) {
            this.errorFormSpeaker = true
            validation = false
        } else {
            for (let index in this.selectedSpeaker) {
                const speaker = this.selectedSpeaker[index]
                this.sessionEdit.speakers[speaker.uid] = Object.assign({}, speaker)
            }
        }

        // total attendees
        if (form.totalAttendees !== null && form.totalAttendees !== '' && typeof form.totalAttendees !== 'undefined') {
            this.sessionEdit.limitAttendees = form.totalAttendees
        }

        // descriptions
        this.sessionEdit.descriptions.PtBR = form.descriptionlanguages_pt_BR.replace(/href="/g, 'class="wysiwyg-link" href="')
        this.sessionEdit.descriptions.EnUS = form.descriptionlanguages_en_US.replace(/href="/g, 'class="wysiwyg-link" href="')
        this.sessionEdit.descriptions.DeDE = form.descriptionlanguages_de_DE.replace(/href="/g, 'class="wysiwyg-link" href="')
        this.sessionEdit.descriptions.EsES = form.descriptionlanguages_es_ES.replace(/href="/g, 'class="wysiwyg-link" href="')
        this.sessionEdit.descriptions.FrFR = form.descriptionlanguages_fr_FR.replace(/href="/g, 'class="wysiwyg-link" href="')

        // checks if the start date is in the past
        let now = new Date();
        let dayNow: any = now.getDate().toString()
        let monthNow: any = now.getMonth() + 1
        monthNow.toString()
        const yearNow: any = now.getFullYear().toString()
        let hourNow: any = now.getHours().toString()
        let minuteNow: any = now.getMinutes().toString()

        if (dayNow < 10) {
            dayNow = `0${dayNow}`
        }

        if (monthNow < 10) {
            monthNow = `0${monthNow}`
        }

        if (minuteNow < 10) {
            minuteNow = `0${minuteNow}`
        }

        if (hourNow < 10) {
            hourNow = `0${hourNow}`
        }

        const timeStampNow = this.luxon.createTimeStamp(this.luxon.createDate(yearNow, monthNow, dayNow, hourNow, minuteNow, '00'))

        if (this.sessionEdit.startTime < timeStampNow) {
            validation = false
            this.datePast = true
        }

        this.wherebyError = [];

        if (validation) {
            try {
                console.log("Whereby form: ", this.wherebyForm.value);
                // Create room whereby if needed
                let startDateArray = (this.wherebyForm.value.startDate) ? this.wherebyForm.value.startDate.split('-') : [];
                let startDate = (startDateArray.length > 0) ? this.luxon.createDate(startDateArray[0], startDateArray[1], startDateArray[2], '00', '00', '00').toISO() : null;
                let endDateArray = (this.wherebyForm.value.endDate) ? this.wherebyForm.value.endDate.split('-') : [];
                let endDate = (endDateArray.length > 0) ? this.luxon.createDate(endDateArray[0], endDateArray[1], endDateArray[2], '00', '00', '00').toISO() : null;

                if (this.event.allow_visio && this.wherebyForm.value.name && this.wherebyForm.value.name != "") {

                    console.log("Start date; ", startDate, " - ", endDate, " - ", this.luxon.currentTime().toISO());
                    if (!startDate || startDate < this.luxon.currentTime().toISO()) {
                        this.wherebyError.push('start-date-required-or-invalid');
                    }
                    if (!endDate || endDate < this.luxon.currentTime().toISO()) {
                        this.wherebyError.push('end-date-required-or-invalid');
                    }

                    if (startDate > endDate) {
                        this.wherebyError.push('start-superior-end');
                    }

                    if (this.wherebyError.length > 0) {
                        this.createSessionError = true;
                        this.loader = false;
                        return;
                    }

                    if (this.sessionEdit.visio && this.sessionEdit.visio.meetingId) {
                        await this.SWhereby.deleteMeeting(this.sessionEdit.visio.meetingId);
                    }

                    let meeting: any = {
                        "roomNamePrefix": "/" + ((this.wherebyForm.value.name) ? this.wherebyForm.value.name : (this.sessionEdit.visio.roomName) ? this.sessionEdit.visio.roomName : "default"),
                        "roomMode": (this.wherebyForm.value.mode) ? this.wherebyForm.value.mode : "normal",
                        "startDate": (startDate) ? startDate : null,
                        "endDate": (endDate) ? endDate : null,
                    }

                    let resp = await this.SWhereby.createMeeting(meeting)

                    if (resp.body && resp.body.meetingId && resp.body.roomUrl) {
                        this.sessionEdit.visio = {
                            meetingId: (resp.body.meetingId) ? resp.body.meetingId : "",
                            roomUrl: (resp.body.roomUrl) ? resp.body.roomUrl : "",
                            startDate: (startDate) ? startDate : null,
                            endDate: (endDate) ? endDate : null,
                            roomMode: (this.wherebyForm.value.mode) ? this.wherebyForm.value.mode : "normal",
                            roomName: (this.wherebyForm.value.name) ? this.wherebyForm.value.name : (this.sessionEdit.visio.roomName) ? this.sessionEdit.visio.roomName : "default",
                            allowChatOnVisio: this.wherebyForm.value.allowChatOnVisio,
                            allowScreenShareOnVisio: this.wherebyForm.value.allowScreenShareOnVisio,
                            allowLeaveOnVisio: this.wherebyForm.value.allowLeaveOnVisio,
                            activated: this.wherebyForm.value.activated
                        }
                    }

                }

                this.dbSchedule.updateSession(this.sessionEdit, (success) => {
                    if (success) {
                        this.loader = false;
                        this.successSwal.fire();
                    } else {
                        this.createSessionError = true;
                        this.loader = false;
                    }
                })
            } catch (error) {
                this.createSessionError = true;
                this.loader = false;
            }
        } else {
            this.loader = false
        }
    }

    redirectList() {
        if (!this.activationVisioUpdated) {
            this.router.navigate(['/event/' + this.eventId + '/schedule/' + this.moduleId]);
        } else {
            this.activationVisioUpdated = false;
        }
    }

    // autocomplete locations
    filterLocations() { //  funÃ§Ã£o usa a query variÃ¡vel para filtrar tracks, entÃ£o ela armazena o resultado no filteredList.
        if (this.queryLocation !== "") {
            this.filteredListLocations = this.listLocations.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryLocation.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListLocations = [];
        }
    }

    selectLocation(item) { // atribuir o item selecionado para a variÃ¡vel de consulta, a fim de fazÃª-lo aparecer na entrada, e para fazer a lista de sugestÃµes de desaparecer
        const pos = this.selectedLocations.map(function (e) { return e.uid; }).indexOf(item.uid);

        if (pos < 0) {
            this.selectedLocations.push(item);
        }

        this.queryLocation = '';
        this.filteredListLocations = [];
    }

    removeLocation(item) { // remove valores selecionados
        this.selectedLocations.splice(this.selectedLocations.indexOf(item), 1);
    }

    // autocomplete tracks
    filterTracks() {
        if (this.queryTrack !== "") {
            this.filteredListTracks = this.listTracks.filter(function (el) {
                return el.name[this.principalEventLangFormated].toLowerCase().indexOf(this.queryTrack.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListTracks = [];
        }
    }

    selectTrack(item) {
        const pos = this.selectedTracks.map(function (e) { return e['uid']; }).indexOf(item.uid);

        if (pos < 0) {
            this.selectedTracks.push(item);
        }

        this.queryTrack = '';
        this.filteredListTracks = [];
    }

    removeTrack(item) {
        this.selectedTracks.splice(this.selectedTracks.indexOf(item), 1);
    }

    /******************* GROUPS **********************/

    // filter groups
    filterGroups() {
        if (this.queryGroup !== "") {
            this.filteredListGroups = this.listGroup.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroup.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroups = [];
        }
    }

    // select track from filtered list
    selectGroup(item) {
        if (this.selectedGroup.length > 0) {
            const index = this.selectedGroup.indexOf(item);
            if (index == -1) {
                this.selectedGroup.push(item);
            }
        } else {
            this.selectedGroup.push(item);
        }
        this.queryGroup = '';
        this.filteredListGroups = [];
    }

    // remove selected location
    removeGroup(item) {
        this.selectedGroup.splice(this.selectedGroup.indexOf(item), 1);
    }

    /******************* SPEAKERS **********************/

    // filter speakers
    filterSpeakers() {
        if (this.querySpeaker !== "") {
            this.filteredListSpeakers = this.listSpeaker.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.querySpeaker.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListSpeakers = [];
        }
    }

    // select speaker from filtered list
    selectSpeaker(item) {
        if (this.selectedSpeaker.length > 0) {
            const index = this.selectedSpeaker.map(function (e) { return e.uid; }).indexOf(item.uid);

            if (index == -1) {
                this.selectedSpeaker.push(item);
            }
        } else {
            this.selectedSpeaker.push(item);
        }
        this.querySpeaker = '';
        this.filteredListSpeakers = [];
    }

    // remove selected speaker
    removeSelectedSpeakers(item) {
        this.selectedSpeaker.splice(this.selectedSpeaker.indexOf(item), 1);
    }

    /******************* DOCUMENTS **********************/

    // filter documents
    filterDocuments() {
        if (this.queryDocument !== "") {
            this.filteredListDocuments = this.listDocument.filter(function (el) {
                return el.name[this.principalEventLangFormated].toLowerCase().indexOf(this.queryDocument.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListDocuments = [];
        }
    }

    // select document from filtered list
    selectDocument(item) {
        if (this.selectedDocument.length > 0) {
            const index = this.selectedDocument.map(function (e) { return e.uid; }).indexOf(item.uid);

            if (index == -1) {
                this.selectedDocument.push(item);
            }
        } else {
            this.selectedDocument.push(item);
        }

        this.queryDocument = '';
        this.filteredListDocuments = [];
    }

    // remove selected document
    removeSelectedDocuments(item) {
        this.selectedDocument.splice(this.selectedDocument.indexOf(item), 1);
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    /**
     * Change activation of visio
     */
    async changeActivationVisio() {
        this.createSessionError = false;
        this.errorFormLocation = false;
        this.errorFormTrack = false;
        this.errorFormSpeaker = false
        this.errorFormDocument = false
        this.timeError = false
        this.datePast = false

        this.loader = true;

        try {
            let newActivation = !this.sessionEdit.visio.activated;
            await this.dbSchedule.updateActivationVisioOnSession(this.sessionEdit, newActivation);
            this.activationVisioUpdated = true;
            this.successSwal.fire();
        } catch (error) {
            this.createSessionError = true;
            this.loader = false;
        }
    }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Event } from '../../../../../models/event';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { Group } from 'src/app/models/group';
import { Notification } from '../../../../../models/notifications';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NotifiicationDateService } from 'src/app/providers/luxon/notification-date.service';
import { DbNotificationsProvider } from 'src/app/providers/database/db-notifications';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { UUID } from 'angular2-uuid';
import { Attendee } from 'src/app/models/attendees';
import { DbAttendeesProvider } from 'src/app/providers/database/db-attendees';
declare let $: any;

@Component({
    selector: 'app-create-notification',
    templateUrl: './create-notification.component.html',
    styleUrls: ['./create-notification.component.scss']
})
export class CreateNotificationComponent implements OnInit {
    formValidation: FormGroup;
    eventId: string = null;
    moduleId: string = null;
    event: Event = null;
    requiredTitle: boolean = false;
    requiredTitleLanguage: boolean = false;
    requiredMessage: boolean = false;
    requiredMessageLanguage: boolean = false;
    requiredSendTo: boolean = false;
    requiredDate: boolean = false;
    requiredTime: boolean = false;
    requiredWhenSend: boolean = false;
    requiredGroups: boolean = false;
    notificationCheckError: boolean = false;
    // GROUPS FILTER
    listGroup: Array<Group> = [];
    selectedGroup: Array<Group> = [];
    filteredListGroups = [];
    queryGroup = '';

    whenSend = null;
    scheduledInputs: boolean = false;
    groupsInput: boolean = false;
    sendTo = null;
    fileUrl: string = null;
    file: any = null;
    customErrorModal: string = null;
    loader: boolean = false;
    showTotalUserReceive: boolean = false;
    numberAttendeesReceive: number = 0;
    allAttendees: Array<Attendee> = [];
    @ViewChild('successSwal') public successSwal: SwalComponent;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;
    @ViewChild('errorTitleSwal') public errorTitleSwal: SwalComponent;
    @ViewChild('errorMsgSwal') public errorMsgSwal: SwalComponent;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private dbEvent: DbEventsProvider,
        private dbGroups: DbGroupsProvider,
        private dbNotification: DbNotificationsProvider,
        private luxon: NotifiicationDateService,
        private router: Router,
        private storage: StorageService,
        private dbAttendees: DbAttendeesProvider
    ) {
        this.eventId = this.route.parent.params['_value']['uid'];
        this.moduleId = this.route.snapshot.params['moduleId'];
        this.formValidation = fb.group({
            'title_pt_BR': [''],
            'title_en_US': [''],
            'title_es_ES': [''],
            'title_fr_FR': [''],
            'title_de_DE': [''],
            'msg_pt_BR': [''],
            'msg_en_US': [''],
            'msg_es_ES': [''],
            'msg_fr_FR': [''],
            'msg_de_DE': [''],
            'url': [''],
            'send_to': [''],
            'when_send': [''],
            'date': [''],
            'time': [''],
            'groups': ['']
        });
        this.getEvent();
        this.getGroupsList();
        this.getAllAttendees();
    }

    ngOnInit() {
    }


    getEvent() {
        this.dbEvent.getEvent(this.eventId, (event: Event) => {
            this.event = event;
        });
    }

    getGroupsList() {
        this.dbGroups.searchModulesAndGroups(this.eventId, (result) => {
            this.listGroup = result['groups'];
        });
    }

    getAllAttendees() {
        this.dbAttendees.getAttendeesByEvent(this.eventId, (attendees: Array<Attendee>) => {
            this.allAttendees = [];
            this.allAttendees = attendees;
        });
    }

    /******************* GROUPS **********************/

    // filter groups
    filterGroups() {
        if (this.queryGroup !== "") {
            this.filteredListGroups = this.listGroup.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroup.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroups = [];
        }
    }

    // select track from filtered list
    selectGroup(item) {
        if (this.selectedGroup.length > 0) {
            const index = this.selectedGroup.indexOf(item);
            if (index == -1) {
                this.selectedGroup.push(item);
            }
        } else {
            this.selectedGroup.push(item);
        }
        this.getGroupPeopleReceiveNotification();
        this.queryGroup = '';
        this.filteredListGroups = [];
    }

    // remove selected location
    removeGroup(item) {
        this.selectedGroup.splice(this.selectedGroup.indexOf(item), 1);
        this.getGroupPeopleReceiveNotification();
    }

    getGroupPeopleReceiveNotification() {
        let auxAttendees = [];
        let contGroups = 0;
        this.showTotalUserReceive = false;
        this.selectedGroup.forEach(group => {
            let contAttendees = 0;
            for (let i in this.allAttendees) {
                let attendeeGroups = this.allAttendees[i]['groups'];
                if (attendeeGroups[group.uid] !== undefined && this.allAttendees[i]['notification'] !== undefined) {
                    auxAttendees.push(this.allAttendees[i]);
                }
                contAttendees++;
                if (contAttendees == this.allAttendees.length) {
                    contGroups++;
                }
            }
            if (contGroups == this.selectedGroup.length) {
                this.numberAttendeesReceive = this.removeDoubleUser(auxAttendees);
                this.showTotalUserReceive = true;
            }
        });
    }

    removeDoubleUser(array) {
        let finalArray = array.filter(function (el, i) {
            return array.indexOf(el) == i;
        });
        return finalArray.length;
    }

    whenSendRadios($event) {
        this.whenSend = $event.target.value;
        if (this.whenSend == 'now') {
            this.scheduledInputs = false;
            this.formValidation.patchValue({
                date: null,
                time: null
            });
        } else {
            this.scheduledInputs = true;
        }
    }

    // onChange funciton to get file from input file and handle image
    uploadPicture($event: any) {
        this.file = $event.srcElement.files[0];
        this.storage.notificationPicture(this.file, this.eventId, this.moduleId, UUID.UUID(), (url) => {
            this.fileUrl = url;
        });
    }

    sendToRadios($event) {
        this.sendTo = $event.target.value;
        let auxAttendees = [];
        this.showTotalUserReceive = false;
        this.numberAttendeesReceive = 0;
        if (this.sendTo == 'all') {
            this.groupsInput = false;
            this.selectedGroup = [];
            this.allAttendees.forEach(element => {
                if (element['notification'] !== undefined) {
                    auxAttendees.push(element);
                    this.showTotalUserReceive = true;
                    this.numberAttendeesReceive = auxAttendees.length;
                }
            });
        } else {
            this.groupsInput = true;
        }
    }

    async checkNotification(data) {
        this.requiredMessage = false;
        this.requiredSendTo = false;
        this.requiredTitle = false;
        this.requiredDate = false;
        this.requiredTime = false;
        this.requiredWhenSend = false;
        this.requiredGroups = false;
        this.requiredTitleLanguage = false;
        this.requiredMessageLanguage = false;
        this.notificationCheckError = false;
        // start notification object
        let notification = new Notification();

        // notification principal language, to set the principal lang on send notification
        notification.principal_language = this.event.language;
        let auxLang;
        switch (this.event.language) {
            case 'pt_BR':
                auxLang = 'pt';
                break;
            case 'en_US':
                auxLang = 'en';
                break;
            case 'es_ES':
                auxLang = 'es';
                break;
            case 'fr_FR':
                auxLang = 'fr';
                break;
            case 'de_DE':
                auxLang = 'de';
                break;
        }

        // min 1 title is required
        if (
            (data.title_pt_BR == '' || data.title_pt_BR == undefined) &&
            (data.title_en_US == '' || data.title_en_US == undefined) &&
            (data.title_es_ES == '' || data.title_es_ES == undefined) &&
            (data.title_fr_FR == '' || data.title_fr_FR == undefined) &&
            (data.title_de_DE == '' || data.title_de_DE == undefined)
        ) {
            // required to fill only title
            this.requiredTitle = true;
            this.notificationCheckError = true;
            $('#collapseOfHeadings').collapse('show');
            this.errorTitleSwal.fire();
        }

        // notification titles (5 languages)
        notification.headings = {
            pt: data.title_pt_BR,
            en: data.title_en_US,
            es: data.title_es_ES,
            fr: data.title_fr_FR,
            de: data.title_de_DE
        }
        if (notification.headings[auxLang] == '' && notification.headings[auxLang]) {
            this.notificationCheckError = true;
            this.requiredTitleLanguage = true;
        }

        if (data.title_en_US == '' || data.title_en_US == undefined) {
            notification.headings['en'] = notification.headings[auxLang];
        }

        // min 1 msg is required
        if (
            (data.msg_pt_BR == '' || data.msg_pt_BR == undefined) &&
            (data.msg_en_US == '' || data.msg_en_US == undefined) &&
            (data.msg_es_ES == '' || data.msg_es_ES == undefined) &&
            (data.msg_fr_FR == '' || data.msg_fr_FR == undefined) &&
            (data.msg_de_DE == '' || data.msg_de_DE == undefined)
        ) {
            // required to fill only message
            this.requiredMessage = true;
            this.notificationCheckError = true;
            this.customErrorModal = 'É necessário preencher o título';
            $('#collapseOfContents').collapse('show');
            this.errorMsgSwal.fire();
        }

        // notification messages (5 languages)
        notification.contents = {
            pt: data.msg_pt_BR,
            en: data.msg_en_US,
            es: data.msg_es_ES,
            fr: data.msg_fr_FR,
            de: data.msg_de_DE
        }
        if (notification.contents[auxLang] == '' && notification.contents[auxLang]) {
            this.notificationCheckError = true;
            this.requiredMessageLanguage = true;
        }
        if (data.msg_en_US == '' || data.msg_en_US == undefined) {
            notification.contents['en'] = notification.contents[auxLang];
        }

        // send to (all or group);
        if (this.sendTo == null) {
            this.requiredSendTo = true;
            this.notificationCheckError = true;
        }
        notification.send_to = this.sendTo;

        if (this.sendTo == 'group') {
            // case have selected groups, push uid of the groups
            notification.groups_ids = [];
            if (this.selectedGroup.length >= 1) {
                this.selectedGroup.forEach(element => {
                    notification.groups_ids.push(element.uid);
                });
            } else {
                this.requiredGroups = true;
                this.notificationCheckError = true;
            }
        } else if (this.sendTo == 'all') {
            this.selectedGroup = null;
            notification.groups_ids = [];
        }

        // case is scheduled notification
        if (this.whenSend == null) {
            this.requiredWhenSend = true;
            this.notificationCheckError = true;
        }
        notification.scheduled = this.scheduledInputs;
        if (this.scheduledInputs == true) {
            if (data.date !== '' && data.date !== null) {
                if (data.time !== '' && data.time !== null) {
                    notification.scheduled_date = this.luxon.convertToRFC(data.date, data.time, this.event.timezone);
                } else {
                    // required to set time
                    this.requiredTime = true;
                    this.notificationCheckError = true;
                }
            } else {
                // required to set date
                this.requiredDate = true;
                this.notificationCheckError = true;
            }
        } else {
            notification.scheduled_date = '';
        }

        if (data.date !== '' && data.date !== null && data.time !== '' && data.time !== null) {
            notification.delivery_date = this.luxon.getTimeStampFromString(data.date, data.time, this.event.timezone);
        } else {
            notification.delivery_date = this.luxon.getTimeStampFromDateNow(new Date(), this.event.timezone);
        }
        // notification highglight image (display in notification details on app)
        notification.highlight_picture = this.fileUrl;
        // url to open on click in notification
        notification.url = data.url;
        if (this.notificationCheckError == false) {
            this.createNotification(notification);
        }
    }

    createNotification(notification: Notification) {
        this.loader = true;
        this.dbNotification.createNotification(this.eventId, this.moduleId, notification, (status) => {
            if (status == true) {
                this.successSwal.fire();
                this.loader = false;
            } else {
                this.errorSwal.fire();
                this.loader = false;
            }
        });
    }

    redirectToList() {
        this.router.navigate([`/event/${this.eventId}/notifications/${this.moduleId}`]);
    }
}

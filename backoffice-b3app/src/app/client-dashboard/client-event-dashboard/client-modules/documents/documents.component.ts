import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DbDocuments } from 'src/app/providers/database/db-documents';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { GalleryFolder } from 'src/app/models/gallery-folder';
import { DocumentsFolder } from 'src/app/models/documents-folder';
import { DbManagerModuleProvider } from 'src/app/providers/database/db-manager-module';
import { Module } from 'src/app/models/modules/module';
import { GlobalService } from 'src/app/providers/global/global.service';
import { DragulaService } from 'ng2-dragula';
import { NameModule } from 'src/app/models/name-module';
import { Languages } from 'src/app/models/languages';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Group } from 'src/app/models/group';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { TypeVisionDocument } from 'src/app/enums/type-vision-document';
declare let $: any;

@Component({
    selector: 'app-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss'],
    providers: [DbDocuments]
})

export class DocumentsComponent implements OnInit {
    // get the language of the user.
    public userLanguage: string

    @ViewChild('successSwal') public successSwal: SwalComponent;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;
    eventId: string;
    moduleId: string;
    folders: Array<any> = [];
    typeOrder: string = null;
    term;
    folderName: NameModule = new NameModule('', '', '', '', '');
    folderNameEdit: NameModule = new NameModule('', '', '', '', '');

    folderNameRequired: boolean = false;
    folderIdDelete: string = null;
    folderEdit;
    selectedAllInput: boolean = false;
    foldersSelected: Array<boolean> = [];
    loader: boolean = true;
    loadingCreate: boolean = false;
    folderView: boolean = false;
    module: any;

    onReorderShow: boolean = false;
    loaderGeneral: boolean = false;
    eventLanguage: string   // get the language of the event.
    languages: Languages = null //event languages
    activeLanguage: string = null;

    // vision foolder
    folderCreateTypeVision: number = TypeVisionDocument.GLOBAL_VISION
    folderEditTypeVision = null
    vision_global: number = TypeVisionDocument.GLOBAL_VISION
    vision_group_vision: number = TypeVisionDocument.GROUP_VISION

    // GROUPS FILTER
    listGroups: Array<Group>
    selectedGroupsCreate: Array<Group> = [];
    filteredListGroupsCreate = [];
    queryGroupsCreate = '';

    selectedGroupsEdit: Array<Group> = [];
    filteredListGroupsEdit = [];
    queryGroupsEdit = '';


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private DbDocuments: DbDocuments,
        private dbModules: DbManagerModuleProvider,
        private dbEvent: DbEventsProvider,
        private global: GlobalService,
        private dragula: DragulaService,
        private dbGroups: DbGroupsProvider,
    ) {
        this.eventId = this.route.parent.params['_value']['uid'];
        this.moduleId = this.route.snapshot.params['moduleId'];
    }

    ngOnInit() {
        this.folderCreateTypeVision = TypeVisionDocument.GLOBAL_VISION

        this.getModule();
        this.getUserLanguage()
        this.getEvent();
        this.getGroupsList()

        // start dragula reorder bag
        this.dragula.dropModel('bag-folders-list').subscribe((value: any) => {
            this.onReorder(value);
        });
    }

    event: any = null;
    getEvent() {
        this.dbEvent.getEvent(this.eventId, (event) => {
            this.event = event;
            this.eventLanguage = event.language;
            this.activeLanguage = this.convertLangFormat(event.language);
            this.languages = event.languages;
        })
    }

    convertLangFormat(lang) {
        let formatedLang;

        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    // update order of modules
    onReorder(order: any): void {
        this.onReorderShow = true;
        this.typeOrder = 'custom';
        this.folders = order.targetModel;
        for (let i = 0; i < (this.folders).length; ++i) {
            this.folders[i].order = i;
        }
    }

    getModule() {
        this.DbDocuments.getModule(this.moduleId, (module) => {
            if (typeof module !== 'undefined' && module !== null) {
                this.module = module;
                this.typeOrder = module['typeOrder'];
                this.getFolders();
            }
        });
    }

    ngDoCheck() { // verifica se houve mudança no parâmetro do idModule
        if (this.moduleId !== this.route.params['_value']['moduleId']) {
            this.moduleId = this.route.params['_value']['moduleId']
            this.folders = [];
            this.getModule();
            this.getUserLanguage()
            this.getGroupsList()

            this.folderCreateTypeVision = TypeVisionDocument.GLOBAL_VISION
        }
    }

    // get the language of the user.
    getUserLanguage() {
        this.global.getLanguage((language) => {
            this.userLanguage = language
        })
    }

    ngAfterContentChecked() {
        let auxRoute = this.router.url.includes('folder');
        if (auxRoute == true) {
            this.folderView = true;
        } else {
            this.folderView = false;
        }
    }

    getFolders() {
        this.DbDocuments.getFolders(this.moduleId, this.typeOrder, (folders: Array<DocumentsFolder>) => {
            this.folders = folders;

            if (this.eventId && this.moduleId) {
                if (this.folders.length === 1 && this.folders[0].typeVision === this.vision_global && this.folders[0].uid) {
                    this.module['viewApp'] = `/event/${this.eventId}/documents/${this.moduleId}/folder/${this.folders[0].uid}`;
                    this.dbModules.updateModule(this.module);
                } else {
                    this.module['viewApp'] = `/event/${this.eventId}/documents/${this.moduleId}`;
                    this.dbModules.updateModule(this.module);
                }
            }
            this.loader = false;
        });
    }

    createFolder() {
        this.loadingCreate = true
        this.folderNameRequired = false;

        if (this.folderName !== null && this.folderName !== undefined && typeof this.folderName !== 'undefined' && this.mainLanguageIsFilled(this.folderName)) {
            let order = this.folders.length > 0 ? this.folders.length : 0
            this.folderName = this.fillEmptyLanguage(this.folderName);

            let folder = new DocumentsFolder(this.folderName, order, this.folderCreateTypeVision);

            // get groups
            if (this.folderCreateTypeVision === this.vision_group_vision) {
                for (const group of this.selectedGroupsCreate) {
                    const groupId = group.uid
                    folder.groups[groupId] = group
                }
            }

            // create folder
            this.DbDocuments.createFolder(this.moduleId, folder, (status) => {
                if (status == true) {
                    $('#addFolder').modal('toggle');
                    this.folderName.PtBR = ''
                    this.folderName.EnUS = ''
                    this.folderName.EsES = ''
                    this.folderName.FrFR = ''
                    this.folderName.DeDE = ''

                    this.folderCreateTypeVision = this.vision_global
                    this.selectedGroupsCreate = []
                    this.filteredListGroupsCreate = []
                    this.successSwal.fire();
                    this.loadingCreate = false;
                } else {
                    this.loadingCreate = false;
                    this.errorSwal.fire();
                }
            });
        } else {
            this.loadingCreate = false
            this.folderNameRequired = true;
        }
    }

    editFolder() {
        this.folderNameRequired = false;
        this.loadingCreate = true;

        if (this.folderNameEdit !== null && this.folderNameEdit !== undefined && typeof this.folderNameEdit !== 'undefined' && this.mainLanguageIsFilled(this.folderNameEdit)) {
            this.folderName = this.fillEmptyLanguage(this.folderNameEdit);
            this.folderEdit.name = this.folderNameEdit;
            this.folderEdit.typeVision = this.folderEditTypeVision
            this.folderEdit.groups = {}

            if (this.folderEdit.typeVision === this.vision_group_vision) {
                for (const group of this.selectedGroupsEdit) {
                    const groupId = group.uid
                    this.folderEdit.groups[groupId] = group
                }
            }

            this.DbDocuments.editFolder(this.moduleId, this.folderEdit, (status) => {
                if (status == true) {
                    this.loadingCreate = false;
                    this.folderNameEdit.PtBR = ''
                    this.folderNameEdit.EnUS = ''
                    this.folderNameEdit.EsES = ''
                    this.folderNameEdit.FrFR = ''
                    this.folderNameEdit.DeDE = ''
                    this.selectedGroupsEdit = []
                    this.filteredListGroupsEdit = []
                    $('#editFolder').modal('toggle');
                    this.successSwal.fire();
                } else {
                    this.loadingCreate = false;
                    this.errorSwal.fire();
                }
            });
        } else {
            this.loadingCreate = false;
            this.folderNameRequired = true;
        }
    }

    deleteFolder() {
        this.loader = true;
        this.DbDocuments.deleteFolder(this.moduleId, this.folderIdDelete, (status) => {
            if (status == true) {
                this.successSwal.fire();
                this.loader = false;
            } else {
                this.errorSwal.fire();
                this.loader = false;
            }
        });
    }

    selectedAll() {
        if (this.selectedAllInput) {
            for (let i = 0; i < this.folders.length; i++) {
                this.foldersSelected[this.folders[i]['uid']] = true;
            }
        } else {
            for (let i = 0; i < this.folders.length; i++) {
                this.foldersSelected[this.folders[i]['uid']] = false;
            }
        }
    }

    removeSelected() {
        this.loader = true;

        let listRemove = [];
        let listImgStorageRemove = [];
        // let listRemoveIndexes = [];

        for (let i = 0; i < this.folders.length; i++) {
            if (this.foldersSelected[this.folders[i].uid] == true) {
                listRemove.push(this.folders[i].uid);
                listImgStorageRemove.push(this.folders[i].storageId);
                // listRemoveIndexes.push(i);
            }
        }

        this.DbDocuments.deleteFolders(this.eventId, this.moduleId, listRemove, listImgStorageRemove, (data) => {
            if (data == true) {
                //remove all selected box
                for (let j = 0; j < this.foldersSelected.length; j++) {
                    this.foldersSelected[this.folders[j].uid] = false;
                }

                this.getFolders();
                this.selectedAllInput = false;
                this.successSwal.fire();
                // this.loader = false;
            } else {
                this.loader = false;
                this.errorSwal.fire();
            }
        })
    }

    changeOrder() {
        this.DbDocuments.changeOrderModule(this.moduleId, this.typeOrder, (status) => { });
    }

    getFolderEdit(folder) {
        this.folderNameEdit = folder.name;
        this.folderEdit = folder;

        this.selectedGroupsEdit = []
        this.folderEditTypeVision = typeof folder.typeVision !== 'undefined' && folder.typeVision !== null && typeof folder.typeVision === 'number' ? folder.typeVision : this.vision_global

        if (this.folderEditTypeVision === this.vision_group_vision) {
            for (const uid in this.folderEdit.groups) {
                const group = this.folderEdit.groups[uid]
                this.selectedGroupsEdit.push(group)
            }
        }

    }

    getFolderDelete(uid) {
        this.folderIdDelete = uid;
    }

    saveReorder() {
        this.loaderGeneral = true;
        this.DbDocuments.reorderFoldersList(this.moduleId, this.folders)
            .then((success) => {
                this.onReorderShow = false;
                this.loaderGeneral = false
                if (this.typeOrder == 'custom') this.changeOrder();
                /* case success remove display of btn to save new order and display modal success*/
                this.successSwal.fire();
            }).catch((error) => {
                this.onReorderShow = false;
                this.loaderGeneral = false
                // case error, display modal error
                this.errorSwal.fire();
            })
    }

    // checks if the primary language name has been filled in.
    mainLanguageIsFilled(languagesObj) {
        switch (this.eventLanguage) {
            case 'pt_BR':
                if (languagesObj['PtBR'] !== '') {
                    return true;
                }
                break;

            case 'en_US':
                if (languagesObj['EnUS'] !== '') {
                    return true;
                }
                break;

            case 'es_ES':
                if (languagesObj['EsES'] !== '') {
                    return true;
                }
                break;

            case 'fr_FR':
                if (languagesObj['FrFR'] !== '') {
                    return true;
                }
                break;

            case 'de_DE':
                if (languagesObj['DeDE'] !== '') {
                    return true;
                }
                break;
        }

        return false;
    }

    // fills in the empty folder names with the main language name.
    fillEmptyLanguage(languagesObj) {
        let mainLanguageStr = null;

        switch (this.eventLanguage) {
            case 'pt_BR':
                mainLanguageStr = languagesObj['PtBR'];
                break;

            case 'en_US':
                mainLanguageStr = languagesObj['EnUS'];
                break;

            case 'es_ES':
                mainLanguageStr = languagesObj['EsES'];
                break;

            case 'fr_FR':
                mainLanguageStr = languagesObj['FrFR'];
                break;

            case 'de_DE':
                mainLanguageStr = languagesObj['DeDE'];
                break;
        }

        if (languagesObj['PtBR'] == '') {
            languagesObj['PtBR'] = mainLanguageStr;
        }

        if (languagesObj['EnUS'] == '') {
            languagesObj['EnUS'] = mainLanguageStr;
        }

        if (languagesObj['EsES'] == '') {
            languagesObj['EsES'] = mainLanguageStr;
        }

        if (languagesObj['FrFR'] == '') {
            languagesObj['FrFR'] = mainLanguageStr;
        }

        if (languagesObj['DeDE'] == '') {
            languagesObj['DeDE'] = mainLanguageStr;
        }

        return languagesObj;
    }

    /******************* GROUPS **********************/

    getGroupsList() {
        this.dbGroups.searchModulesAndGroups(this.eventId, (result) => {
            this.listGroups
            this.listGroups = result['groups'];
        });
    }

    // filter groups
    filterGroupsCreate() {
        if (this.queryGroupsCreate !== "") {
            this.filteredListGroupsCreate = this.listGroups.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroupsCreate.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroupsCreate = [];
        }
    }

    // select track from filtered list
    selectGroupCreate(item) {
        if (this.selectedGroupsCreate.length > 0) {
            const index = this.selectedGroupsCreate.indexOf(item);
            if (index == -1) {
                this.selectedGroupsCreate.push(item);
            }
        } else {
            this.selectedGroupsCreate.push(item);
        }
        this.queryGroupsCreate = '';
        this.filteredListGroupsCreate = [];
    }

    // remove selected location
    removeGroupCreate(item) {
        this.selectedGroupsCreate.splice(this.selectedGroupsCreate.indexOf(item), 1);
    }


    // filter groups
    filterGroupsEdit() {
        if (this.queryGroupsEdit !== "") {
            this.filteredListGroupsEdit = this.listGroups.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroupsEdit.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroupsEdit = [];
        }
    }

    // select track from filtered list
    selectGroupEdit(item) {
        if (this.selectedGroupsEdit.length > 0) {
            const index = this.selectedGroupsEdit.map(function (e) { return e.uid; }).indexOf(item.uid);

            if (index == -1) {
                this.selectedGroupsEdit.push(item);
            }
        } else {
            this.selectedGroupsEdit.push(item);
        }
        this.queryGroupsEdit = '';
        this.filteredListGroupsEdit = [];
    }

    // remove selected location
    removeGroupEdit(item) {
        this.selectedGroupsEdit.splice(this.selectedGroupsEdit.indexOf(item), 1);
    }


    ngOnDestroy() {
        this.loader = false;
        this.folderCreateTypeVision = this.vision_global
    }
}

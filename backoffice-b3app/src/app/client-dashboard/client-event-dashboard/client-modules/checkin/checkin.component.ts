import { Component, OnInit, ViewChild } from '@angular/core';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Checkin } from 'src/app/models/checkin';
import { DbCheckinProvider } from 'src/app/providers/database/db-checkin';
import { ActivatedRoute, Router } from '@angular/router';
import { Group } from 'src/app/models/group';
import { DbGroupsProvider } from 'src/app/providers/database/db-groups';
import { Attendee } from 'src/app/models/attendees';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { TranslateService } from '@ngx-translate/core';
import * as jspdf from 'jspdf';
import { DragulaService } from 'ng2-dragula';
import { DragulaOptions } from 'dragula';
import { DbManagerModuleProvider } from 'src/app/providers/database/db-manager-module';
import { GlobalService } from 'src/app/providers/global/global.service';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { TypeVisionCheckin } from 'src/app/enums/type-vision-checkin';

declare let $: any;
type AOA = Array<Array<any>>;

@Component({
    selector: 'app-checkin',
    templateUrl: './checkin.component.html',
    styleUrls: ['./checkin.component.scss'],
    providers: [DbCheckinProvider]
})

export class CheckinComponent implements OnInit {
    // get the language of the user.
    public userLanguage: string

    @ViewChild('successSwal') public successSwal: SwalComponent;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;
    eventId: string = null;
    moduleId: string = null;
    module;
    checkins: Array<Checkin> = [];
    loader: boolean = true;
    loaderCreate: boolean = false;
    loaderEdit: boolean = false;
    p: number = 1;
    checkinName: string = null;
    checkinEditName: string = null;
    checkinEditTypeVision: number = null;
    requiredName: boolean = false;

    checkinEdit;
    checkinDeleteId: string = null;
    statusView: boolean = false;
    checkinExportId: string = null;
    checkinExportName: string = null;
    checkinExportTypeVision: number = null;
    checkinExportGroups = [];



    // GROUPS FILTER
    listGroups: Array<Group>
    selectedGroupsCreate: Array<Group> = [];
    filteredListGroupsCreate = [];
    queryGroupsCreate = '';

    selectedGroupsEdit: Array<Group> = [];
    filteredListGroupsEdit = [];
    queryGroupsEdit = '';

    attendees: Array<Attendee> = [];
    // export checkin
    dataExportAttendees: AOA = null;
    data: any = null;
    qrCodeValue: string = null;
    term;
    dragulaOptions: DragulaOptions = {
        moves: () => true,
    }
    onReorderShow: boolean = false;
    loaderGeneral: boolean = false;
    checkinConfigs = null;
    loaderConfigs: boolean = false;
    generalSettingsLoader: boolean = false;
    totalAttendeeTxtColor: string = '#000000';
    totalPresentTxtColor: string = '#000000';
    totalAttendeeBgColor: string = '#fafafa';
    totalPresentBgColor: string = '#1fc94f';
    selectedAllInput: boolean = false;
    checkinSelected: Array<boolean> = [];

    checkinTypeVision: number = TypeVisionCheckin.GLOBAL_VISION
    vision_global: number = TypeVisionCheckin.GLOBAL_VISION
    vision_group_vision: number = TypeVisionCheckin.GROUP_VISION

    constructor(
        private dbCheckin: DbCheckinProvider,
        private route: ActivatedRoute,
        private router: Router,
        private dbGroups: DbGroupsProvider,
        private translateService: TranslateService,
        private dragula: DragulaService,
        private dbModules: DbManagerModuleProvider,
        private global: GlobalService,
        private dbEvent: DbEventsProvider
    ) {
        this.eventId = this.route.parent.params['_value']['uid'];
        this.moduleId = this.route.snapshot.params['moduleId'];
        dragula.createGroup('bag-checkin-list', this.dragulaOptions);
    }

    ngOnInit() {
        this.getModule();
        this.getEvent();
        this.getGroupsList();
        this.getUserLanguage()

        // heats the function to perform faster.
        this.dbCheckin.getAttendeesWithCheckin(null, null, null, null, null, null, (a) => { });

        // start dragula reorder bag
        this.dragula.dropModel('bag-checkin-list').subscribe((value: any) => {
            this.onReorder(value);
        });
    }

    event: any = null;
    getEvent() {
        this.dbEvent.getEvent(this.eventId, (event: Event) => {
            this.event = event;
        });
    }

    // update order of modules
    onReorder(order: any): void {
        this.onReorderShow = true;
        this.checkins = order.targetModel;
        for (let i = 0; i < (this.checkins).length; ++i) {
            this.checkins[i].order = i;
        }
    }

    getModule() {
        this.dbCheckin.getCheckinModule(this.moduleId, (module) => {
            this.module = module;
            this.getCheckins();
        });
    }

    // get the language of the user.
    getUserLanguage() {
        this.global.getLanguage((language) => {
            this.userLanguage = language
        })
    }


    ngAfterContentChecked() {
        let auxRoute = this.router.url.includes('status');
        if (auxRoute == true) {
            this.statusView = true;
        } else {
            this.statusView = false;
        }
    }



    getCheckins() {
        this.dbCheckin.getCheckinList(this.moduleId, (checkins: Array<Checkin>) => {
            this.checkins = [];

            if (checkins.length === 1 && checkins[0].typeVision === this.vision_global && checkins[0].visibility) {
                if (this.eventId && this.moduleId && checkins[0].uid) {
                    this.module['viewApp'] = `/event/${this.eventId}/checkin-detail/${this.moduleId}/${checkins[0].uid}`
                    this.dbModules.updateModule(this.module)
                }
            } else {
                if (this.eventId && this.moduleId) {
                    this.module['viewApp'] = `/event/${this.eventId}/checkin/${this.moduleId}`;
                    this.dbModules.updateModule(this.module);
                }
            }

            for (let checkin of checkins) {
                checkin['qrcodeId'] = this.moduleId + '+' + checkin.uid;
                this.checkins.push(checkin);
            }

            this.loader = false;
        });
    }

    // create checkin
    createCheckin() {
        this.requiredName = false;

        if (this.checkinName !== '' && this.checkinName !== null) {
            this.loaderCreate = true;
            let order = 0;

            if (this.checkins.length == 0) {
                order = 0;
            } else {
                order = this.checkins.length;
            }

            let checkin = new Checkin(this.checkinName, order, this.checkinTypeVision);

            if (this.checkinTypeVision === this.vision_group_vision) {
                for (const group of this.selectedGroupsCreate) {
                    const groupId = group.uid
                    checkin.groups[groupId] = group
                }
            }

            checkin.eventId = this.eventId;
            checkin.moduleId = this.moduleId;

            this.dbCheckin.newCheckin(this.moduleId, this.eventId, checkin, (response) => {
                if (!response['status']) {
                    this.errorSwal.fire();
                    this.loaderCreate = false;
                } else {
                    this.selectedGroupsCreate = []
                    this.checkinTypeVision = this.vision_global
                    $('#newCheckin').modal('toggle');
                    this.loaderCreate = false;
                    this.checkinName = null;
                    this.successSwal.fire();
                }
            });
        } else {
            this.requiredName = true;
        }
    }

    getEditCheckin(checkin) {
        this.selectedGroupsEdit = []

        this.checkinEdit = checkin;
        this.checkinEditName = checkin.name;
        this.checkinEditTypeVision = checkin.typeVision

        if (this.checkinEditTypeVision === this.vision_group_vision) {
            for (const uid in checkin.groups) {
                const group = checkin.groups[uid]
                this.selectedGroupsEdit.push(group)
            }
        }


    }

    editCheckin() {
        this.requiredName = false;
        if (this.checkinEditName !== '' && this.checkinEditName !== null) {
            this.loaderEdit = true;


            if (this.checkinEditTypeVision === this.vision_global) {
                this.checkinEdit.groups = {}
            }


            if (this.checkinEditTypeVision === this.vision_group_vision) {
                this.checkinEdit.groups = {}
                for (const group of this.selectedGroupsEdit) {
                    const groupId = group.uid
                    this.checkinEdit.groups[groupId] = group
                }
            }

            this.dbCheckin.editCheckin(this.moduleId, this.checkinEdit.uid, this.checkinEditName, this.checkinEditTypeVision, this.checkinEdit.groups, (status) => {
                if (!status) {
                    this.errorSwal.fire();
                    this.loaderEdit = false;
                } else {
                    this.checkinEdit = null
                    this.checkinEditName = ''
                    this.checkinEditTypeVision = this.vision_global
                    this.selectedGroupsEdit = []
                    $('#editCheckin').modal('toggle');
                    this.loaderEdit = false;
                    this.successSwal.fire();
                }
            });
        } else {
            this.requiredName = true;
        }
    }

    /** 
      * get checkinDeleteId to remove check in.
      * @param uid Checkin uid.
    */
    getDeleteCheckin(uid: string) {
        this.checkinDeleteId = uid;
    }

    /** 
     *Delete checkin. Activate the trigger:dbCheckinDelete
     */
    deleteCheckin() {
        this.dbCheckin.deleteCheckin(this.moduleId, this.checkinDeleteId, (status) => {
            if (!status) {
                this.errorSwal.fire();
            } else {
                this.successSwal.fire();
            }
        });
    }

    changeVisibility(uid: string, status: boolean) {
        this.dbCheckin.changeVisibility(this.moduleId, uid, status);
    }

    clearForm() {
        this.checkinName = null;
    }

    startCheckinRef(checkinId: string, name: string) {
        this.checkinExportId = checkinId;
    }

    qrCodeStart() {
        if (this.checkinExportId == null) {
            this.checkinExportId = this.route.children[0]['params']['_value']['checkinId'];
        }
        this.qrCodeValue = this.moduleId + '+' + this.checkinExportId;
    }


    /******************* GROUPS **********************/

    getGroupsList() {
        this.dbGroups.searchModulesAndGroups(this.eventId, (result) => {
            this.listGroups
            this.listGroups = result['groups'];
        });
    }



    // filter groups
    filterGroupsCreate() {
        if (this.queryGroupsCreate !== "") {
            this.filteredListGroupsCreate = this.listGroups.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroupsCreate.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroupsCreate = [];
        }
    }

    // select track from filtered list
    selectGroupCreate(item) {
        if (this.selectedGroupsCreate.length > 0) {
            const index = this.selectedGroupsCreate.indexOf(item);
            if (index == -1) {
                this.selectedGroupsCreate.push(item);
            }
        } else {
            this.selectedGroupsCreate.push(item);
        }
        this.queryGroupsCreate = '';
        this.filteredListGroupsCreate = [];
    }

    // remove selected location
    removeGroupCreate(item) {
        this.selectedGroupsCreate.splice(this.selectedGroupsCreate.indexOf(item), 1);
    }


    // filter groups
    filterGroupsEdit() {
        if (this.queryGroupsEdit !== "") {
            this.filteredListGroupsEdit = this.listGroups.filter(function (el) {
                return el.name.toLowerCase().indexOf(this.queryGroupsEdit.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredListGroupsEdit = [];
        }
    }

    // select track from filtered list
    selectGroupEdit(item) {
        if (this.selectedGroupsEdit.length > 0) {
            const index = this.selectedGroupsEdit.map(function (e) { return e.uid; }).indexOf(item.uid);

            if (index == -1) {
                this.selectedGroupsEdit.push(item);
            }
        } else {
            this.selectedGroupsEdit.push(item);
        }
        this.queryGroupsEdit = '';
        this.filteredListGroupsEdit = [];
    }

    // remove selected location
    removeGroupEdit(item) {
        this.selectedGroupsEdit.splice(this.selectedGroupsEdit.indexOf(item), 1);
    }

    /********************************* EXPORT ALL ATTENDEES CHECKIN ************************/
    async exportCheckin() {
        if (this.checkinExportId == null) {
            this.checkinExportId = this.route.children[0]['params']['_value']['checkinId'];
        }

        const checkin: any = await this.dbCheckin.getCheckinPromise(this.moduleId, this.checkinExportId)
        this.checkinExportTypeVision = typeof checkin.typeVision !== 'undefined' && checkin.typeVision !== null ? checkin.typeVision : TypeVisionCheckin.GLOBAL_VISION
        const auxGroups = typeof checkin.groups !== 'undefined' && checkin.groups !== null ? checkin.groups : {}
        this.checkinExportGroups = []
        for (const uid in auxGroups) {
            const group = auxGroups[uid]
            this.checkinExportGroups.push(group)
        }

        this.dataExportAttendees = [];
        this.dataExportAttendees = [[
            'Identifier',
            'Name',
            'Status',
        ]];

        let cont = 0;
        await this.dbCheckin.exportCheckin(this.eventId, this.moduleId, this.checkinExportId, this.checkinExportTypeVision, this.checkinExportGroups, (attendees) => {
            if (attendees.length > 0) {
                for (let attendee of attendees) {

                    let row: any;
                    row = this.prepareCheckinExport(attendee);
                    this.dataExportAttendees.push(row);

                    if (cont == attendees.length - 1) {
                        const wscols: XLSX.ColInfo[] = [
                            { wpx: 100 }, // "pixels"
                            { wpx: 100 }, // "pixels"
                            { wpx: 100 }, // "pixels"
                            { hidden: false } // hide column
                        ];

                        /* At 96 PPI, 1 pt = 1 px */
                        const wsrows: XLSX.RowInfo[] = [
                            { hpx: 25 }, // "pixels"
                        ];

                        /* generate worksheet */
                        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.dataExportAttendees);

                        /* TEST: column props */
                        ws['!cols'] = wscols;

                        /* TEST: row props */
                        ws['!rows'] = wsrows;

                        /* generate workbook and add the worksheet */
                        const wb: XLSX.WorkBook = XLSX.utils.book_new();
                        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

                        /* save to file */
                        const wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
                        saveAs(new Blob([this.s2ab(wbout)]), 'checkin.xlsx');
                        this.data = null;

                        $('#exportLoading').modal('toggle');

                    } else {

                        $('#exportLoading').modal('toggle');

                        // this.errorSwal.fire();
                    }
                    cont++;
                }
            } else {
                setTimeout(() => {
                    $('#exportLoading').modal('toggle');
                    this.errorSwal.fire();
                }, 1000);
            }
        });
    }

    // PREPARA UM REGISTRO DE GRUPOS PARA EXPORTAÇÃO
    prepareCheckinExport(attendee: Attendee) {
        const row = [];

        row.push(attendee['identifier']);
        row.push(attendee['name']);
        if (attendee['checkinStatus'] == true) {
            row.push(this.translateService.instant('comp.checkin.showed'));
        } else {
            row.push(this.translateService.instant('comp.checkin.away'));
        }


        return row;
    }

    // AJUDA A GERAR O ARQUIVO EXECL
    private s2ab(s: string): ArrayBuffer {
        const buf: ArrayBuffer = new ArrayBuffer(s.length);
        const view: Uint8Array = new Uint8Array(buf);
        for (let i = 0; i !== s.length; ++i) {
            view[i] = s.charCodeAt(i) & 0xFF;
        }
        return buf;
    }

    printAllQrCodes() {
        $('#printAll').modal('show');
        let size = this.checkins.length;
        let array = []
        for (let i = 0; i < size; i++) {
            // get element canvas and make data url string
            let elem = document.getElementById("qrcodesAll" + i);
            const canvas = elem.childNodes[0].childNodes[0] as HTMLCanvasElement;
            const imageData = canvas.toDataURL("image/png").toString();
            // size and position of qrcodes
            let imgWidth = 150;
            let imgHeight = canvas.height * imgWidth / canvas.width;
            array.push({
                name: this.checkins[i].name,
                image: imageData,
                width: imgWidth,
                height: imgHeight,
                position: 30
            });

            if (array.length == size) {
                this.saveAllQrCodes(array);
            }
        }
    }

    saveAllQrCodes(qrcodes) {
        let size = qrcodes.length;
        // start jspdf a4 format in landscape mode using milimiters 
        let pdf = new jspdf('l', 'mm', 'a4'); // A4 size page of PDF
        let pageHeight = pdf.internal.pageSize.height || pdf.internal.pageSize.getHeight();
        let pageWidth = pdf.internal.pageSize.width || pdf.internal.pageSize.getWidth();
        for (let i = 0; i < size; i++) {
            // add title to top
            pdf.setFontType('bold');
            pdf.text(qrcodes[i].name, pageWidth / 2, pageHeight - 190, 'center');
            // add qrcode image after title
            pdf.addImage(qrcodes[i].image, 'PNG', 70, qrcodes[i].position, qrcodes[i].width, qrcodes[i].height);
            // generate new page
            if (i < size - 1) {
                pdf.addPage();
            }
            if (i == size - 1) {
                pdf.save('all-qrcodes.pdf'); // Generated PD
                setTimeout(() => {
                    $('#printAll').modal('toggle');
                }, 1000);
            }
        }
    }

    printQr() {
        this.dbCheckin.getCheckin(this.moduleId, this.checkinExportId, (checkin) => {
            this.checkinExportName = checkin['name'];
            const canvas = document.querySelector("canvas") as HTMLCanvasElement;
            const imageData = canvas.toDataURL("image/png").toString();
            let imgWidth = 150;
            let imgHeight = canvas.height * imgWidth / canvas.width;

            let pdf = new jspdf('l', 'mm', 'a4'); // A4 size page of PDF
            let pageHeight = pdf.internal.pageSize.height || pdf.internal.pageSize.getHeight();
            let pageWidth = pdf.internal.pageSize.width || pdf.internal.pageSize.getWidth();
            const position = 30;
            pdf.setFontType('bold');
            pdf.text(this.checkinExportName, pageWidth / 2, pageHeight - 190, 'center');
            pdf.addImage(imageData, 'PNG', 70, position, imgWidth, imgHeight);
            pdf.save('qrcode.pdf'); // Generated PD
        });
    }

    saveReorder() {
        this.dbCheckin.reorderCheckinList(this.moduleId, this.checkins)
            .then((success) => {
                this.onReorderShow = false;
                this.loaderGeneral = false
                /* case success remove display of btn to save new order and display modal success*/
                this.successSwal.fire();
            }).catch((error) => {
                this.onReorderShow = false;
                this.loaderGeneral = false
                // case error, display modal error
                this.errorSwal.fire();
            })
    }

    getCheckinConfigs() {
        this.generalSettingsLoader = true;
        this.checkinExportId = this.route.firstChild.snapshot.params['checkinId'];
        this.dbCheckin.getCheckin(this.moduleId, this.checkinExportId, (checkin) => {
            this.checkinConfigs = checkin;
            this.totalAttendeeBgColor = checkin['totalBgColor'];
            this.totalAttendeeTxtColor = checkin['totalTxtColor'];
            this.totalPresentBgColor = checkin['totalPresentBgColor'];
            this.totalPresentTxtColor = checkin['totalPresentTxtColor'];
            this.generalSettingsLoader = false;
        });
    }

    updateCheckinConfigs() {
        this.loaderConfigs = true;
        this.checkinConfigs['totalBgColor'] = this.totalAttendeeBgColor;
        this.checkinConfigs['totalTxtColor'] = this.totalAttendeeTxtColor;
        this.checkinConfigs['totalPresentBgColor'] = this.totalPresentBgColor;
        this.checkinConfigs['totalPresentTxtColor'] = this.totalPresentTxtColor;
        this.dbCheckin.editCheckinConfigs(this.moduleId, this.checkinExportId, this.checkinConfigs, (status) => {
            if (status == true) {
                this.loaderConfigs = false;
                $('#realtimeCheckinSettings').modal('toggle');
                this.successSwal.fire();
            } else {
                this.loaderConfigs = false;
                this.errorSwal.fire();
            }
        });
    }

    changeConfigAllowFooter($ev) {
        this.checkinConfigs['allowFooter'] = $ev;
    }
    changeConfigAllowUsername($ev) {
        this.checkinConfigs['allowUsername'] = $ev;
    }

    selectedAll() {
        if (this.selectedAllInput) {
            for (let i = 0; i < this.checkins.length; i++) {
                this.checkinSelected[this.checkins[i]['uid']] = true;
            }
        } else {
            for (let i = 0; i < this.checkins.length; i++) {
                this.checkinSelected[this.checkins[i]['uid']] = false;
            }
        }
    }


    /** 
        *Delete checkins. Activate the trigger:dbCheckinDelete
        *@param moduleId
        *@param listRemove
        *@returns onResolve
    */
    removeSelected() {
        this.loader = true
        let listRemove = []

        for (const uid in this.checkinSelected) {
            if (this.checkinSelected[uid])
                listRemove.push(uid)
        }

        if (listRemove.length > 0) {
            this.dbCheckin.deleteCheckins(this.moduleId, listRemove, (result) => {
                if (result) {
                    this.loader = false
                    this.successSwal.fire();
                    this.selectedAllInput = false
                    this.checkinSelected = []
                } else {
                    this.loader = false;
                    this.errorSwal.fire();
                }
            })
        } else {
            this.loader = false
        }
    }

    ngOnDestroy() {
        this.dragula.destroy('bag-checkin-list');
    }
}

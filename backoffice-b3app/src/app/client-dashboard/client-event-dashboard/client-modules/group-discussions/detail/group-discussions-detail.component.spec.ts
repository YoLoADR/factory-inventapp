import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupDiscussionsDetailComponent } from './group-discussions-detail.component';

describe('GroupDiscussionsDetailComponent', () => {
  let component: GroupDiscussionsDetailComponent;
  let fixture: ComponentFixture<GroupDiscussionsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupDiscussionsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDiscussionsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-terms-n-privacy',
  templateUrl: './terms-n-privacy.component.html',
  styleUrls: ['./terms-n-privacy.component.scss']
})
export class TermsNPrivacyComponent implements OnInit {
  @ViewChild('successSwal') public successSwal: SwalComponent;
  @ViewChild('errorSwal') public errorSwal: SwalComponent;
  eventId: string;
  termsOfUse: string = null;
  privacy: string = null;
  loader: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private dbEvent: DbEventsProvider
  ) {
    this.eventId = this.route.parent.params['_value']['uid'];
  }

  ngOnInit() {
    this.dbEvent.getEvent(this.eventId, (event) => {
      this.termsOfUse = event['terms_of_use'];
      this.privacy = event['privacy'];
    })
  }

  updateTermOfuse() {
    if(typeof this.termsOfUse === 'undefined' || this.termsOfUse === null){
      this.termsOfUse = ""
    }

    this.loader = true;
    
    this.dbEvent.updateEventTerms(this.eventId, { terms_of_use: this.termsOfUse }, (status) => {
      if (status == true) {
        this.successSwal.fire();
        this.loader = false;
      } else {
        this.errorSwal.fire();
        this.loader = false;
      }
    });
  }

  updatePrivacy() {
    if(typeof this.privacy === 'undefined' || this.privacy === null){
      this.privacy = ""
    }
    
    this.loader = true;
    
    this.dbEvent.updateEventTerms(this.eventId, { privacy: this.privacy }, (status) => {
      if (status == true) {
        this.successSwal.fire();
        this.loader = false;
      } else {
        this.errorSwal.fire();
        this.loader = false;
      }
    });
  }
}

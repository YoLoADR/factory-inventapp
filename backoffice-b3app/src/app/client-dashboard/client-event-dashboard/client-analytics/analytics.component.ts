import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { DbAttendeesProvider } from 'src/app/providers/database/db-attendees';
import { DbEventsProvider } from 'src/app/providers/database/db.events';
import { Event } from 'src/app/models/event';
import { DbAnalyticsProvider } from 'src/app/providers/database/db-analytics';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { TranslateService } from '@ngx-translate/core';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { GlobalService } from 'src/app/providers/global/global.service';
import { RegexProvider } from 'src/app/providers/regex/regex.service';
import { FormatedEventLanguageService } from 'src/app/providers/formated-event-language/formated-event-language.service';
import { WherebyService } from 'src/app/providers/whereby/whereby.service';
type AOA = Array<Array<any>>;
declare let $: any;

@Component({
    selector: 'app-analytics',
    templateUrl: './analytics.component.html',
    styleUrls: ['./analytics.component.scss'],
    providers: [DbAnalyticsProvider]
})

export class AnalyticsComponent implements OnInit {
    // get the language of the user.
    public userLanguage: string

    eventId: string;
    event: Event = null;
    @ViewChild('errorSwal') public errorSwal: SwalComponent;
    loader: boolean = true;

    // ATTENDEES INFOS
    totalAccess: number = 0;
    uniqueAccess: number = 0;
    profileEdited: number = 0;
    blockUserInfos: boolean = false;

    // MODULES INFOS
    loaderModules: boolean = true;
    modules: Array<any> = [];

    // FEED INFOS
    totalFeedPosts: number = 0;
    totalFeedLikes: number = 0;
    totalFeedComments: number = 0;
    blockFeedInfos: boolean = false;

    // CHAT INFOS
    totalChats: number = 0;
    totalChatMsgs: number = 0;
    totalChatMembers: number = 0;
    blockChatInfos: boolean = false;

    // DOCUMENTS INFOS
    bestDocuments: Array<any> = [];
    blockDocumentInfos: boolean = false;

    // GALLERY IMAGE INFOS
    bestImages: Array<any> = [];
    blockGalleryInfos: boolean = false;

    // VISIO INFOS
    totalPIM: number = 0;
    totalVisiosBetween2: number = 0;
    totalRooms: number = 0;
    blockVisioInfos: boolean = false;

    // EXPORT DATA
    data: any = null;
    exportUsersInfos: AOA = null;
    exportModulesInfos: AOA = null;
    exportChatInfos: AOA = null;
    exportFeedInfos: AOA = null;
    exportDocumentsInfos: AOA = null;
    exportGalleryImageInfos: AOA = null;

    constructor(
        private route: ActivatedRoute,
        private dbAttendees: DbAttendeesProvider,
        private dbEvent: DbEventsProvider,
        private dbAnalytics: DbAnalyticsProvider,
        private translateService: TranslateService,
        private luxon: LuxonService,
        private global: GlobalService,
        private regex: RegexProvider,
        public formatedLanguage: FormatedEventLanguageService,
        private SWhereby: WherebyService
    ) {
        this.eventId = this.route.parent.params['_value']['uid']
    }

    ngOnInit() {
        this.getUserLanguage()
        this.getEvent();
    }

    getEvent() {
        this.dbEvent.getEvent(this.eventId, (event) => {
            this.event = event;
            this.formatedLanguage.convertLangFormat(event.language);
            this.start();
        });
    }

    // get the language of the user.
    getUserLanguage() {
        this.global.getLanguage((language) => {
            this.userLanguage = this.convertLangFormat(language);
        })
    }

    // convert user language to data format
    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    // get all data
    start() {
        Promise.all([
            this.getAttendeesInfos(),
            this.getModulesInfos(),
            this.getFeedInfos(),
            this.getChatInfos(),
            this.getDocumentsInfos(),
            this.getGalleryImageInfos(),
            this.getVisioInfos()
        ])
            .then((responses) => {
                if (responses[0] !== true) {
                    this.blockUserInfos = true;
                }

                if (responses[2] !== true) {
                    this.blockFeedInfos = true;
                }

                if (responses[3] !== true) {
                    this.blockChatInfos = true;
                }

                if (responses[2] !== true) {
                    this.blockDocumentInfos = true;
                }

                if (responses[2] !== true) {
                    this.blockGalleryInfos = true;
                }

                if (responses[6] !== true) {
                    this.blockVisioInfos = true;
                }

                this.loader = false;
            })
            .catch((e) => { console.log() })
    }

    // get all attendees infos
    getAttendeesInfos() {
        return new Promise((resolve) => {
            if (this.event.availableModules.module_attendees == true) {
                this.dbAnalytics.getAttendeeInfos(this.eventId, (data) => {
                    if (data !== null && data !== undefined) {
                        this.totalAccess = data['totalAccess'];
                        this.uniqueAccess = data['uniqueAccess'];
                        this.profileEdited = data['editedProfile'];
                    }
                    resolve(true)
                });
            } else {
                resolve(false);
            }
        });
    }

    // get all modules infos
    getModulesInfos() {
        return new Promise((resolve) => {
            this.dbAnalytics.getModulesAccess(this.eventId, (data) => {
                this.modules = data;
                resolve(true);
            });
        });
    }

    // get all feed news infos
    getFeedInfos() {
        return new Promise((resolve) => {
            if (this.event.availableModules.module_feed_news == true) {
                this.dbAnalytics.getFeedInfos(this.eventId, (data) => {
                    this.totalFeedPosts = data['totalPosts'];
                    this.totalFeedLikes = data['totalLikes'];
                    this.totalFeedComments = data['totalComments'];
                    resolve(true);
                });
            } else {
                this.blockFeedInfos = true;
                resolve(false);
            }
        });
    }

    // get all chat infos
    getChatInfos() {
        return new Promise((resolve) => {
            if (this.event.allow_chat == true) {
                this.dbAnalytics.getChatInfos(this.eventId, (chat) => {
                    this.totalChats = chat['totalChats'];
                    this.totalChatMsgs = chat['totalMsgs'];
                    this.totalChatMembers = chat['totalMembers'];
                    resolve(true);
                });
            } else {
                this.blockChatInfos = true;
                resolve(false);
            }
        });
    }

    // get all documents infos
    getDocumentsInfos() {
        return new Promise((resolve) => {
            if (this.event.availableModules.module_document == true) {
                this.dbAnalytics.getDocumentsAccess(this.eventId, (data) => {
                    this.bestDocuments = data;
                    resolve(true);
                });
            } else {
                this.blockDocumentInfos = true;
                resolve(false);
            }
        });
    }

    // get all gallery images infos
    getGalleryImageInfos() {
        return new Promise((resolve) => {
            if (this.event.availableModules.module_gallery == true) {
                this.dbAnalytics.getGalleryImageAccess(this.eventId, (data) => {
                    this.bestImages = data;
                    resolve(true);
                });
            } else {
                this.blockGalleryInfos = true;
                resolve(false);
            }
        });
    }

    /**
     * Get visio analytics
     */
    getVisioInfos() {
        return (new Promise(async (resolve) => {
            if (this.event.allow_visio) {
                this.totalRooms = await this.dbAnalytics.getTotalRoomsForEvent(this.eventId);
                this.totalPIM = await this.dbAnalytics.getTotalAccessForEvent(this.eventId);
                this.totalVisiosBetween2 = await this.dbAnalytics.getTotalRoomsFor2(this.eventId);
                console.log("Session visios: ", this.totalRooms);
                resolve(true);
            } else {
                this.blockVisioInfos = true;
                resolve(false);
            }
        }))
    }

    // export all data
    async exportAll() {
        $('#exportLoading').modal('show');
        Promise.all([
            this.getAllUserInfos(),
            this.getAllModulesInfos(),
            this.getAllFeedInfos(),
            this.getAllDocumentsInfos(),
            this.getAllGalleryImagesInfos()
        ])
            .then((response) => {
                let result: Array<any> = [];
                /* generate workbook and add the worksheet */
                const wb: XLSX.WorkBook = XLSX.utils.book_new();

                for (let i = 0; i < response.length; i++) {
                    let item: any = response[i];
                    const wscols: XLSX.ColInfo[] = [
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        { wpx: 100 }, // "pixels"
                        // { hidden: false } // hide column
                    ];

                    /* At 96 PPI, 1 pt = 1 px */
                    const wsrows: XLSX.RowInfo[] = [
                        { hpx: 25 }, // "pixels"
                    ];

                    /* generate worksheet */
                    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(item);

                    /* TEST: column props */
                    ws['!cols'] = wscols;

                    /* TEST: row props */
                    ws['!rows'] = wsrows;

                    result[i] = ws;

                    let workSheetName = this.getWorkSheetName(i);
                    XLSX.utils.book_append_sheet(wb, result[i], workSheetName);

                    if (i == Object.keys(response).length - 1) {
                        /* save to file */
                        const wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
                        saveAs(new Blob([this.s2ab(wbout)]), 'data_analytics.xlsx');
                        this.data = null;
                        setTimeout(() => {
                            $('#exportLoading').modal('toggle');
                        }, 250);
                    }
                }
            })
            .catch((e) => {
                console.log(e);
            });
    }

    // get name of worksheet (excel)
    getWorkSheetName(index) {
        let name = '';
        switch (index) {
            case 0:
                name = 'Users';
                break;
            case 1:
                name = 'Modules';
                break;
            case 2:
                name = 'Feed';
                break;
            case 3:
                name = 'Documents';
                break;
            case 4:
                name = 'Gallery';
                break;
        }

        return name;
    }

    // convert boolean to string 'yes' or 'no'
    getYesOrNot(boolean: boolean) {
        let string = '';
        if (boolean == true) {
            string = this.translateService.instant('global.yes_btn');
        } else {
            string = this.translateService.instant('global.no_btn');
        }
        return string;
    }

    // get all user infos to export data
    getAllUserInfos() {
        return new Promise((resolve, reject) => {
            this.exportUsersInfos = [];
            this.exportUsersInfos = [[
                'Identifier',
                'Name',
                'E-mail',
                'Edit profile',
                'User accessed',
                'Total access'
            ]];

            this.dbAnalytics.exportUserInfos(this.eventId, (data) => {
                let row: any;
                row = [];
                if (data.length >= 1) {
                    for (let i = 0; i < data.length; i++) {
                        row.push(data[i].identifier);
                        row.push(data[i].name);
                        row.push(data[i].email);
                        row.push(this.getYesOrNot(data[i].edited_profile));
                        let accessed;
                        if (data[i].total_access >= 1) {
                            accessed = 'Yes';
                        } else {
                            accessed = 'No'
                        }
                        // let accessed;
                        // if (data[i].firstAccess) {
                        //   accessed = 'No';
                        // } else {
                        //   accessed = 'Yes'
                        // }
                        row.push(accessed);
                        if (data[i].total_access !== undefined) {
                            row.push(data[i].total_access);
                        } else {
                            row.push('0');
                        }
                        this.exportUsersInfos.push(row);
                        row = [];
                        if (i == data.length - 1) {
                            resolve(this.exportUsersInfos);
                        }
                    }
                } else {
                    resolve(this.exportUsersInfos);
                }
            });
        });
    }

    // get all modules infos to export data
    getAllModulesInfos() {
        return new Promise((resolve, reject) => {
            this.exportModulesInfos = [];
            this.exportModulesInfos = [[
                'Module',
                'Total access'
            ]];
            this.dbAnalytics.exportModulesInfos(this.eventId, (data) => {
                let row: any;
                row = [];
                if (data.length >= 1) {
                    for (let i = 0; i < data.length; i++) {
                        row.push(data[i].name[this.formatedLanguage.language]);
                        row.push(data[i].total_access);
                        this.exportModulesInfos.push(row);
                        row = [];
                        if (i == data.length - 1) {
                            resolve(this.exportModulesInfos);
                        }
                    }
                } else {
                    resolve(this.exportModulesInfos);
                }
            });
        });
    }

    // get all feed infos to export data
    getAllFeedInfos() {
        return new Promise((resolve, reject) => {
            this.exportFeedInfos = [];
            this.exportFeedInfos = [[
                'Module',
                'Author',
                'Post',
                'Post image',
                'Date',
                'Likes',
                'Comments',
            ]];
            this.dbAnalytics.exportFeedInfos(this.eventId, (data) => {
                let row: any;
                row = [];
                if (data.length >= 1) {
                    for (let i = 0; i < data.length; i++) {
                        row.push(data[i].moduleName[this.userLanguage]);
                        row.push(data[i].creator.name);
                        row.push(this.regex.removeHtml(data[i].description));
                        row.push(data[i].img.url);
                        row.push(this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(data[i].date)));
                        row.push(data[i].totalLikes);
                        row.push(data[i].totalComments);
                        this.exportFeedInfos.push(row);
                        row = [];
                        if (i == data.length - 1) {
                            resolve(this.exportFeedInfos);
                        }
                    }
                } else {
                    resolve(this.exportFeedInfos);
                }
            });
        });
    }

    // get all documents infos to export data
    getAllDocumentsInfos() {
        return new Promise((resolve, reject) => {
            this.exportDocumentsInfos = [];
            this.exportDocumentsInfos = [[
                'Document name',
                'URL',
                'Total access',
            ]];
            this.dbAnalytics.exportDocumentsInfos(this.eventId, (data) => {
                let row: any;
                row = [];
                if (data.length >= 1) {
                    for (let i = 0; i < data.length; i++) {
                        row.push(data[i].name[this.userLanguage]);
                        row.push(data[i].url);
                        row.push(data[i].total_access);
                        this.exportDocumentsInfos.push(row);
                        row = [];
                        if (i == data.length - 1) {
                            resolve(this.exportDocumentsInfos);
                        }
                    }
                } else {
                    resolve(this.exportDocumentsInfos);
                }
            });
        });
    }

    // get all gallery image infos to export data
    getAllGalleryImagesInfos() {
        return new Promise((resolve, reject) => {
            this.exportGalleryImageInfos = [];
            this.exportGalleryImageInfos = [[
                'Image URL',
                'Total access',
            ]];
            this.dbAnalytics.exportGalleryImagesInfos(this.eventId, (data) => {
                let row: any;
                row = [];
                if (data.length >= 1) {
                    for (let i = 0; i < data.length; i++) {
                        row.push(data[i].name);
                        row.push(data[i].url);
                        row.push(data[i].total_access);
                        this.exportGalleryImageInfos.push(row);
                        row = [];
                        if (i == data.length - 1) {
                            resolve(this.exportGalleryImageInfos);
                        }
                    }
                } else {
                    resolve(this.exportGalleryImageInfos);
                }
            });
        });
    }

    // AJUDA A GERAR O ARQUIVO EXECL
    private s2ab(s: string): ArrayBuffer {
        const buf: ArrayBuffer = new ArrayBuffer(s.length);
        const view: Uint8Array = new Uint8Array(buf);
        for (let i = 0; i !== s.length; ++i) {
            view[i] = s.charCodeAt(i) & 0xFF;
        }
        return buf;
    }
}

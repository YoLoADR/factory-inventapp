import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { Routes, RouterModule } from "@angular/router";
import { PathComponents } from "../../paths/path-components";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
import { CKEditorModule } from "ng2-ckeditor";
import { AvatarModule } from "ngx-avatar";
import { NgxPaginationModule } from "ngx-pagination";
import { UiSwitchModule } from "ngx-ui-switch";
import { ColorPickerModule } from "ngx-color-picker";
import { StorageService } from "src/app/providers/storage/storage.service";
import { DbWidgetsProvider } from "src/app/providers/database/db-widgets";
import { DragulaModule } from "ng2-dragula";
import { ImageCropperModule } from "ngx-image-cropper";
import { SharedModule } from "../../shared/shared.module";
import { FilterPiperSessions } from "../../pipes/filter-sessions.pipe";
import { GetNameSession } from "../../pipes/get-name-session";

import { AngularSvgIconModule } from "angular-svg-icon";
import { DropzoneModule } from "ngx-dropzone-wrapper";
import { DROPZONE_CONFIG } from "ngx-dropzone-wrapper";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { FileDropDirective } from "src/app/directives/file-drop.directive";
import { AskQuestionComponent } from "./client-modules/interactivity/ask-question/ask-question.component";
import { NgxQRCodeModule } from "ngx-qrcode2";
import { CustomPagesComponent } from "./client-modules/custom-pages/custom-pages.component";
import { CustomPageCreateComponent } from "./client-modules/custom-pages/custom-page-create/custom-page-create.component";
import { CustomPageEditComponent } from "./client-modules/custom-pages/custom-page-edit/custom-page-edit.component";
import { QuizComponent } from "./client-modules/interactivity/quiz/quiz.component";
import { CreateQuizComponent } from "./client-modules/interactivity/quiz/create-quiz/create-quiz.component";
import { EditQuizComponent } from "./client-modules/interactivity/quiz/edit-quiz/edit-quiz.component";
import { TrainingComponent } from "./client-modules/interactivity/training/training.component";
import { CreateTrainingComponent } from "./client-modules/interactivity/training/create-training/create-training.component";
import { EditTrainingComponent } from "./client-modules/interactivity/training/edit-training/edit-training.component";
import { WordCloudComponent } from "./client-modules/interactivity/word-cloud/word-cloud.component";
import { GamificationComponent } from "./client-modules/gamification/gamification.component";
import { BroadcastComponent } from "./client-modules/broadcast/broadcast.component";
import { TransformVisibilityPipe } from "src/app/pipes/transform-visibility.pipe";
import { GetNameParticipantPipe } from "src/app/pipes/get-name-participant.pipe";
import { GroupDiscussionsDetailComponent } from "./client-modules/group-discussions/detail/group-discussions-detail.component";
import { SortGroupDiscussionsPipe } from "src/app/pipes/sort-group-discussions.pipe";

// translate module config
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, "../assets/i18n/", ".json");
}

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
    // Change this to your upload POST address:
    url: 'https://us-central1-b3app-master.cloudfunctions.net/utilitiesDropzoneUpload',
    maxFilesize: 50
};

const routes: Routes = [
    {
        // dashboard main
        path: "",
        component: PathComponents.client_event_dashboard,
        children: [
            { path: "", redirectTo: "info", pathMatch: "full" },
            // resume is default page
            { path: "info", component: PathComponents.client_event_info },
            { path: "legal", component: PathComponents.client_terms_privacy },

            // manage module
            {
                path: "manage-modules/:moduleId",
                component: PathComponents.client_manage_module,
            },

            // attendees
            {
                path: "attendees/:moduleId",
                component: PathComponents.client_attendee,
            },
            {
                path: "create-attendee/:moduleId",
                component: PathComponents.client_attendee_create,
            },
            {
                path: "edit-attendee/:moduleId/:attendeeId",
                component: PathComponents.client_attendee_edit,
            },

            // speakers
            { path: "speakers/:moduleId", component: PathComponents.client_speaker },
            {
                path: "create-speaker/:moduleId",
                component: PathComponents.client_speaker_create,
            },
            {
                path: "edit-speaker/:moduleId/:speakerId",
                component: PathComponents.client_speaker_edit,
            },

            //custom pages
            {
                path: "custom-pages/:moduleId",
                component: PathComponents.custom_pages,
            },
            {
                path: "custom-pages-create/:moduleId",
                component: PathComponents.custom_pages_create,
            },
            {
                path: "custom-pages-edit/:moduleId/:pageId",
                component: PathComponents.custom_pages_edit,
            },

            // schedule
            { path: "schedule/:moduleId", component: PathComponents.client_schedule },
            {
                path: "create-schedule/:moduleId",
                component: PathComponents.client_schedule_create,
            },
            {
                path: "edit-schedule/:moduleId/:scheduleId",
                component: PathComponents.client_schedule_edit,
            },
            {
                path: "personal-schedule/:moduleId",
                component: PathComponents.client_schedule_personal,
            },

            {
                path: "group-discussions/:moduleId",
                component: PathComponents.client_group_discussion,
            },
            {
                path: "group-discussions/:moduleId/create",
                component: PathComponents.client_group_discussion_detail,
            },
            {
                path: "group-discussions/:moduleId/edit/:groupId",
                component: PathComponents.client_group_discussion_detail,
            },

            // widgets
            { path: "widgets/:moduleId", component: PathComponents.client_widgets },
            // locations
            {
                path: "locations/:moduleId",
                component: PathComponents.client_locations,
            },
            { path: "maps/:moduleId", component: PathComponents.client_maps },
            { path: "ranking/:moduleId", component: PathComponents.client_ranking },
            // groups
            { path: "groups/:moduleId", component: PathComponents.client_groups },
            // interacitivty
            {
                path: "interactivity",
                component: PathComponents.client_interactivity,
                children: [
                    { path: "", redirectTo: "survey", pathMatch: "full" },
                    {
                        path: "survey/:moduleId",
                        component: PathComponents.client_survey,
                        children: [
                            {
                                path: "create-survey",
                                component: PathComponents.client_survey_new,
                            },
                            {
                                path: "edit-survey/:moduleId/:surveyId",
                                component: PathComponents.client_survey_edit,
                            },
                        ],
                    },
                    {
                        path: "quiz/:moduleId",
                        component: PathComponents.client_quiz,
                        children: [
                            {
                                path: "create-quiz",
                                component: PathComponents.client_quiz_new,
                            },
                            {
                                path: "edit-quiz/:moduleId/:quizId",
                                component: PathComponents.client_quiz_edit,
                            },
                        ],
                    },
                    {
                        path: "training/:moduleId",
                        component: PathComponents.client_training,
                        children: [
                            {
                                path: "create-training",
                                component: PathComponents.client_training_new,
                            },
                            {
                                path: "edit-training/:moduleId/:trainingId",
                                component: PathComponents.client_training_edit,
                            },
                        ],
                    },
                    {
                        path: "session-feedback/:moduleId",
                        component: PathComponents.client_session_feedback,
                        children: [
                            {
                                path: "create-session-feedback",
                                component: PathComponents.client_session_feedback_new,
                            },
                            {
                                path: "edit-session-feedback/:moduleId/:sessionFeedbackId",
                                component: PathComponents.client_session_feedback_edit,
                            },
                        ],
                    },

                    {
                        path: "ask-question/:moduleId",
                        component: PathComponents.client_ask_question,
                    },
                    {
                        path: "word-cloud/:moduleId",
                        component: PathComponents.client_word_cloud,
                    },
                ],
            },
            {
                path: "documents/:moduleId",
                component: PathComponents.client_documents,
                children: [
                    {
                        path: "folder/:folderId",
                        component: PathComponents.client_documents_folder,
                    },
                ],
            },
            {
                path: "gallery/:moduleId",
                component: PathComponents.client_gallery,
                children: [
                    {
                        path: "folder/:folderId",
                        component: PathComponents.client_gallery_folder,
                    },
                ],
            },
            {
                path: "checkin/:moduleId",
                component: PathComponents.client_checkin,
                children: [
                    {
                        path: "status/:checkinId",
                        component: PathComponents.client_checkin_status,
                    },
                ],
            },
            { path: "design/colors", component: PathComponents.client_design_colors },
            {
                path: "design/identity",
                component: PathComponents.client_design_bannerlogo,
            },
            {
                path: "notifications/:moduleId",
                component: PathComponents.client_notifications,
            },
            {
                path: "create-notification/:moduleId",
                component: PathComponents.client_create_notifications,
            },
            {
                path: "feed-news/:moduleId",
                component: PathComponents.client_feed_news,
            },
            { path: "analytics", component: PathComponents.client_analytics },
            // infobooth
            {
                path: "page-booth/:moduleId",
                component: PathComponents.client_infobooth,
            },
            { path: "gaming/:moduleId", component: PathComponents.gamification },

            // attendees
            { path: "broadcast/:moduleId", component: PathComponents.broadcast },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient],
            },
        }),
        AvatarModule,
        SweetAlert2Module,
        CKEditorModule,
        NgxPaginationModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: "never" }),
        UiSwitchModule,
        ColorPickerModule,
        DragulaModule,
        ImageCropperModule,
        AngularSvgIconModule,
        DropzoneModule,
        NgxQRCodeModule,
        SharedModule,
    ],
    declarations: [
        PathComponents.client_event_dashboard,
        PathComponents.client_event_info,
        PathComponents.client_attendee,
        PathComponents.client_attendee_create,
        PathComponents.client_attendee_edit,
        PathComponents.client_speaker,
        PathComponents.client_speaker_create,
        PathComponents.client_speaker_edit,
        PathComponents.client_schedule,
        PathComponents.client_schedule_create,
        PathComponents.client_schedule_edit,
        PathComponents.client_widgets,
        PathComponents.client_manage_module,
        PathComponents.client_locations,
        PathComponents.client_locations,
        PathComponents.client_groups,
        PathComponents.client_schedule_personal,
        PathComponents.client_interactivity,
        PathComponents.client_survey,
        PathComponents.client_survey_new,
        PathComponents.client_survey_edit,
        PathComponents.client_quiz,
        PathComponents.client_quiz_new,
        PathComponents.client_quiz_edit,
        PathComponents.client_training,
        PathComponents.client_training_new,
        PathComponents.client_training_edit,
        PathComponents.client_session_feedback,
        PathComponents.client_session_feedback_new,
        PathComponents.client_session_feedback_edit,
        PathComponents.client_documents,
        PathComponents.client_documents_folder,
        PathComponents.client_gallery,
        PathComponents.client_gallery_folder,
        PathComponents.client_checkin,
        PathComponents.client_checkin_status,
        PathComponents.client_design_colors,
        PathComponents.client_design_bannerlogo,
        PathComponents.client_notifications,
        PathComponents.client_create_notifications,
        PathComponents.client_feed_news,
        PathComponents.client_analytics,
        PathComponents.client_maps,
        PathComponents.client_ranking,
        PathComponents.client_terms_privacy,
        PathComponents.client_infobooth,
        PathComponents.client_group_discussion,
        FileDropDirective,
        CustomPagesComponent,
        AskQuestionComponent,
        CustomPageCreateComponent,
        CustomPageEditComponent,
        WordCloudComponent,
        FilterPiperSessions,
        GetNameSession,
        PathComponents.gamification,
        TrainingComponent,
        CreateTrainingComponent,
        EditTrainingComponent,
        BroadcastComponent,
        GroupDiscussionsDetailComponent,
        TransformVisibilityPipe,
        GetNameParticipantPipe,
        SortGroupDiscussionsPipe,
    ],
    providers: [
        StorageService,
        DbWidgetsProvider,
        {
            provide: DROPZONE_CONFIG,
            useValue: DEFAULT_DROPZONE_CONFIG,
        },
    ],
})
export class ClientEventDashboardModule { }

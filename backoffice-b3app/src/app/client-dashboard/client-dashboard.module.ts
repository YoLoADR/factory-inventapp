import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Routes, RouterModule } from '@angular/router';
import { PathComponents } from '../paths/path-components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AvatarModule } from 'ngx-avatar';
import { NgxPaginationModule } from 'ngx-pagination';
import { CKEditorModule } from 'ng2-ckeditor';
import { GlobalService } from '../providers/global/global.service';
import { AuthService } from '../providers/auth/auth.service';
import { DbClientEmployeeProvider } from '../providers/database/db-client-employee';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SharedModule } from '../shared/shared.module';

// translate module config
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

const routes: Routes = [
    {
        // dashboard main
        path: '', component: PathComponents.client_dashboard, children: [
            { path: '', redirectTo: 'resume', pathMatch: 'full' },
            { path: 'resume', component: PathComponents.client_resume },
            { path: 'employees/:userId', component: PathComponents.client_dashboard_employees },
            { path: 'create-employee', component: PathComponents.client_dashboard_create_employee },
            { path: 'edit-employee/:employeeId', component: PathComponents.client_dashboard_edit_employee },
            { path: 'my-profile/:uid', component: PathComponents.client_dashboard_edit_profile },
            { path: 'containers/:userId', component: PathComponents.client_containers },
            { path: 'edit-container/:containerId', component: PathComponents.client_edit_container },
            { path: 'create-container/:clientId', component: PathComponents.client_create_container }
        ]
    }

]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        SweetAlert2Module,
        AvatarModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        CKEditorModule,
        ImageCropperModule,
        SharedModule
    ],
    declarations: [
        PathComponents.client_dashboard,
        PathComponents.client_dashboard_employees,
        PathComponents.client_dashboard_create_employee,
        PathComponents.client_dashboard_edit_employee,
        PathComponents.client_resume,
        PathComponents.client_dashboard_edit_profile,
        PathComponents.client_containers,
        PathComponents.client_edit_container,
        PathComponents.client_create_container
    ],
    providers: [GlobalService, AuthService, DbClientEmployeeProvider]
})
export class ClientDashboardModule { }

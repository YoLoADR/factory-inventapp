import { DescriptionsSession } from './description-session';
import { NameSession } from './name-session';

export class Session {
    uid: string;
    identifier: number; // id usado para importar e exportar.
    name: NameSession;
    date: any;
    startTime: any;
    endTime: any;
    anonymous: boolean;

    moduleId: string;
    eventId: string;
    descriptions: DescriptionsSession;

    locations;
    tracks;
    groups;
    attendees;
    speakers;
    documents;

    // personal schedule control
    limitAttendees: number; //limit of participants who will be able to have the session on the personal agenda

    askQuestion = false;
    askModerate = false;

    visio: any;

    constructor() {
        this.uid = '';
        this.identifier = 0;
        this.name = new NameSession('', '', '', '', '');
        this.date = '';
        this.startTime = '';
        this.endTime = '';
        this.moduleId = '';
        this.eventId = '';
        this.anonymous = true;
        this.descriptions = new DescriptionsSession();
        this.limitAttendees = 0;

        this.locations = {};
        this.tracks = {};
        this.groups = {};
        this.attendees = {};
        this.speakers = {};
        this.documents = {};
    }
}

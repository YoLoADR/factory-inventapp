import { DateTime } from "luxon";
import { NameModule } from "./name-module";

export class PageInfobooth {
    uid: string
    moduleId: string
    eventId: string
    title: NameModule;
    htmlContent: NameModule;
    creationDate: number
    order: number

    constructor() {
        this.uid = ""
        this.moduleId = ""
        this.eventId = ""
        this.title = new NameModule('', '', '', '', '');
        this.htmlContent = new NameModule('', '', '', '', '');

        this.creationDate = DateTime.local().valueOf() / 1000;
    }
}
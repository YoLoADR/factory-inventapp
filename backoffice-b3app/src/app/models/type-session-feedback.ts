export enum TypeSessionFeedback {
    AllSessions = "AllSessions",
    ScheduleModule = "ScheduleModule",
    SessionTrack = "SessionTrack",
    SpecificSession = "SpecificSession",
    SpecificGroup = "SpecificGroup"

}
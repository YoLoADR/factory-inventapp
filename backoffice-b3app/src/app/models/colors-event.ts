export class ColorsEvent {
    menu_color: string
    menu_text_color: string
    title_color: string
    text_content_color: string
    link_color: string
    bg_content_color: string
    bg_general_color: string

    constructor() {
        // sets the color attributes.
        this.menu_color = '#2973a2'
        this.menu_text_color = '#ffffff'
        this.title_color = '#000000'
        this.text_content_color = '#3f3f3f'
        this.link_color = '#4ba4d8'
        this.bg_content_color = '#ffffff'
        this.bg_general_color = '#f6f6f6'
    }
}
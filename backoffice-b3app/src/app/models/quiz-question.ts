import { Answer } from './quiz-answer';
import { NameModule } from './name-module';

export class Question {
    uid: string;
    title: NameModule;
    infobooth: NameModule;
    type: string;
    graphic: string;
    answers: Array<Answer> = [];
    createdAt: number;
    image: any;
    visibility: boolean;
    graphicDifusion: boolean;
    // maxResponses: number;

}

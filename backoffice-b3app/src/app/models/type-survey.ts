export enum TypeSurvey {
    Pattern = "Pattern",
    AllSessions = "AllSessions",
    ScheduleModule = "ScheduleModule",
    SessionTrack = "SessionTrack",
    SpecificSession = "SpecificSession",
    SpecificGroup = "SpecificGroup"

}
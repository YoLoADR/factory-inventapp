export class EventColors {
    menu_color: string;
    menu_text_color: string;
    title_color: string;
    text_content_color: string;
    link_color: string;
    bg_content_color: string;
    bg_general_color: string;

    constructor() {
        this.menu_color = '#2973A2';
        this.menu_text_color = '#FFFFFF';
        this.title_color = '#000000';
        this.text_content_color = '#3F3F3F';
        this.link_color = '#4BA4D8';
        this.bg_content_color = '#FFFFFF';
        this.bg_general_color = '#F6F6F6';
    }
}
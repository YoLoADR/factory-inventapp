import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';
import { GalleryImage } from '../gallery';
import { NameModule } from '../name-module';

export class ModuleGallery extends Module {
    gallerys: Array<GalleryImage>;
    selected: boolean;
    access_groups: any
    typeVision: number;
    typeOrder: string;

    constructor(name: NameModule, icon: string, eventId: string, typeVision:number, order: number) {
        super(name, icon, TypeModule.GALLERY, eventId, order, true, true, true);
        this.gallerys = [];
        this.selected = false;
        this.access_groups = {};
        this.typeVision = typeVision;
        this.typeOrder = 'recent';
    }

}
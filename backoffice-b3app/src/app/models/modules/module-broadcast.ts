import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';
import { NameModule } from '../name-module';

export class ModuleBroadcast extends Module {
    typeVision: number;
    access_groups: any; // uid of the group that is allowed access to the module
    selected: boolean;


    constructor(name: NameModule, icon: string, typeVision: number, eventId: string, order: number) {
        super(name, icon, TypeModule.BROADCAST, eventId, order, false, false, false)
        this.typeVision = typeVision;
        this.access_groups = {};
        this.selected = false;
    }
}
import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';
import { NameModule } from '../name-module';

export class ModuleCheckin extends Module {
    typeVision: number;
    access_group: Array<string>; // uid of the group that is allowed access to the module
    selected: boolean;

    constructor(name: NameModule, icon: string, typeVision: number, eventId: string, order: number) {
        super(name, icon, TypeModule.CHECKIN, eventId, order, false, true, true)
        this.typeVision = typeVision;
        this.access_group = null;
        this.selected = false;
    }
}
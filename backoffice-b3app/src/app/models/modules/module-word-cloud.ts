import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';

export class ModuleWordCloud extends Module {
    typeVision: number;
    access_groups: any; // uid of the group that is allowed access to the module
    selected: boolean;


    constructor(name: any, icon: string, typeVision: number, eventId: string, order: number) {
        super(name, icon, TypeModule.WORDCLOUD, eventId, order, false, false, false)
        this.typeVision = typeVision;
        this.access_groups = {};
        this.selected = false;
    }
}
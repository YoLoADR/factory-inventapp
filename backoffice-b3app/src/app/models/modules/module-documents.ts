import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';
import { NameModule } from '../name-module';

export class ModuleDocuments extends Module {
    documents: Array<Document>;
    selected: boolean;
    access_groups: any
    typeVision: number;
    typeOrder: string;

    constructor(name: NameModule, icon: string, eventId: string, typeVision:number ,order: number) {
        super(name, icon, TypeModule.DOCUMENT, eventId, order, true, true, true);
        this.documents = [];
        this.selected = false;
        this.access_groups = {};
        this.typeVision = typeVision
        this.typeOrder = 'recent';
    }

}
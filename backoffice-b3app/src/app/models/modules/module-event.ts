import { Module } from "./module";
import { TypeModule } from '../../enums/type-module';
import { NameModule } from "../name-module";

export class ModuleEvent extends Module {
    constructor(name: NameModule, icon: string, eventId: string, order: number) {
        super(name, icon, TypeModule.EVENT, eventId, order, false, true, true);
        this.viewBackOffice = null;
    }
}
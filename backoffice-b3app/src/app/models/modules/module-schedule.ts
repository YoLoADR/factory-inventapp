import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';
import { NameModule } from '../name-module';

export class ModuleSchedule extends Module {
    sessions: Array<any>;
    typeVision: number;
    habiliedPersonal: boolean; //controls whether module is enabled in the personal calendar
    habilitedLimit: boolean; //enable limit of participan for session
    allowVisio: boolean;
    allowNextPrevBtn: boolean;
    selected: boolean;
    trackName: any;
    access_groups: any; // uid of the group that is allowed access to the module
    oratorsField: string;

    constructor(name: NameModule, icon: string, typeVision: number, eventId: string, order: number) {
        super(name, icon, TypeModule.SCHEDULE, eventId, order, true, true, true)
        this.typeVision = typeVision;
        this.habiliedPersonal = true;
        this.habilitedLimit = false
        this.access_groups = {};
        this.selected = false;
        this.trackName = {
            PtBR: 'Tracks',
            EnUS: 'Tracks',
            EsES: 'Tracks',
            FrFR: 'Tracks',
            DeDE: 'Tracks'
        }
        this.sessions = [];
    }

}
import { Module } from "./module";
import { TypeModule } from '../../enums/type-module';
import { NameModule } from "../name-module";


export class ModuleMaps extends Module {
    selected: boolean;
    orderMaps: string;
    access_groups: any; // uid of the group that is allowed access to the module
    typeVision: number;

    constructor(name: NameModule, icon: string, typeVision, eventId: string) {
        super(name, icon, TypeModule.MAPS, eventId, null, null, true, true);
        this.typeVision = typeVision;
        this.access_groups = {};
        this.selected = false;
        this.orderMaps = 'asc';
    }
}
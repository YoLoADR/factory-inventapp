import { Module } from "./module";
import { Session } from "../session";
import { TypeModule } from '../../enums/type-module';
import { NameModule } from "../name-module";


export class ModulePersonalAgenda extends Module{
    sessions: Array<Session>    

    constructor(name: NameModule, icon: string, eventId: string, order: number){
        super(name, icon, TypeModule.PERSONALSCHEDULE, eventId, order, false, true, true)
        this.sessions = []
    }
}
import { Module } from "./module";
import { TypeModule } from '../../enums/type-module';
import { NameModule } from "../name-module";


export class ModuleSelfCheckin extends Module {
    selected: boolean;
    constructor(name: NameModule, icon: string, eventId: string, order: number) {
        super(name, icon, TypeModule.SELF_CHECKIN, eventId, order, false, true, true)
        this.selected = false;
    }
}
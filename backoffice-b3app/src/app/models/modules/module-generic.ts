import { Module } from "./module";
import { TypeModule } from '../../enums/type-module';


export class ModuleGeneric extends Module {

    constructor(name, icon, eventId, order, viewApp) {
        super(name, icon, TypeModule.GENERIC, eventId, order, true, true, true)
        this.viewApp = viewApp;
    }
}
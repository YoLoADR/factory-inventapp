import { Module } from "./module";
import { TypeModule } from '../../enums/type-module';
import { Notification } from '../notifications';
import { NameModule } from "../name-module";

export class ModuleNotification extends Module {
    notifications: Array<Notification>;
    orderNotifications: string;

    constructor(name: NameModule, icon: string, eventId: string) {
        // name, icon, module, eventid, order, allow remove, habilited on app, habilited on backoffice
        super(name, icon, TypeModule.NOTIFY, eventId, null, false, null, true)
        this.notifications = [];
        this.orderNotifications = 'desc';
    }

}
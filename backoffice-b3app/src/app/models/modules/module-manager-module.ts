import { Module } from '../../models/modules/module'
import { TypeModule } from '../../enums/type-module';
import { NameModule } from '../name-module';


export class ManagerModules extends Module{
    constructor (name: NameModule, icon: string, eventId: string){
        super(name, icon, TypeModule.MANAGER_MODULES, eventId, null, false, null, true)
    }
}
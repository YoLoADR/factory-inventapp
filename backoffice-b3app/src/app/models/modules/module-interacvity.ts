import { Module } from "./module";
import { TypeModule } from '../../enums/type-module';


export class ModuleInteractivity extends Module {
    access_groups: any; // uid of the group that is allowed access to the module
    typeVision: number; 
    constructor(name, icon, typeVision, eventId, order) {
        super(name, icon, TypeModule.INTERACTIVITY, eventId, order, false, true, true)
        this.typeVision = typeVision;
        this.access_groups = {};
    }
}
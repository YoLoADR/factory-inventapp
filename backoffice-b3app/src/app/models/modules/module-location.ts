import { Module } from "./module";
import { Location } from "./../location";
import { TypeModule } from '../../enums/type-module';
import { NameModule } from "../name-module";


export class ModuleLocation extends Module{
    locations: Array<Location>;
    orderLocations: string;
    
    constructor(name: NameModule, icon: string, eventId: string){
        super(name, icon, TypeModule.LOCATION, eventId, null, false, null, true)
        this.locations = [];
        this.orderLocations = 'asc';
    }

}
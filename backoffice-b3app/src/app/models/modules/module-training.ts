import { Module } from '../modules/module';
import { TypeModule } from '../../enums/type-module';
import { NameModule } from '../name-module';

export class ModuleTraining extends Module {
    typeVision: number;
    access_group: string; // uid of the group that is allowed access to the module
    selected: boolean;


    constructor(name: NameModule, icon: string, typeVision: number, eventId: string, order: number) {
        super(name, icon, TypeModule.TRAINING, eventId, order, false, false, false)
        this.typeVision = typeVision;
        this.access_group = null;
        this.selected = false;
    }
}
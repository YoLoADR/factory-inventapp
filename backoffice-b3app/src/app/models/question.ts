import { Answer } from './answer';
import { NameModule } from './name-module';

export class Question {
    uid: string;
    mainTitle: NameModule;
    title: NameModule;
    infobooth: NameModule;
    type: string;
    points: number;
    graphic: string;
    answers: Array<Answer> = [];
    createdAt: number;
    visibility: boolean;
    anonymous: boolean;
    // maxResponses: number;
}

import { NameModule } from './name-module';

export class Question {
    uid: string;
    name: NameModule;
    visibility: boolean;
    moderate: boolean;
    order: number;
    anonymous: boolean;
}

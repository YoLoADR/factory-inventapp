export enum TypeTraining {
    Pattern = "Pattern",
    AllSessions = "AllSessions",
    ScheduleModule = "ScheduleModule",
    SessionTrack = "SessionTrack",
    SpecificSession = "SpecificSession",
    SpecificGroup = "SpecificGroup"
}
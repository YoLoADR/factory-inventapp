export class Checkin {
    uid: string;
    eventId: string;
    moduleId: string
    name: string;
    visibility: boolean;
    order: number;
    typeOrder: string;
    title: string;
    text: string;
    totalBgColor: string;
    totalTxtColor: string;
    totalPresentBgColor: string;
    totalPresentTxtColor: string;
    allowFooter: boolean;
    allowUsername: boolean;
    typeVision: number
     groups: any

    constructor(name: string, order: number, typeVision: number) {
        this.uid = null;
        this.eventId = null;
        this.moduleId
        this.name = name;
        this.visibility = true;
        this.typeOrder = 'asc';
        this.order = order;
        this.title = '';
        this.text = '';
        this.totalBgColor = '#fafafa';
        this.totalTxtColor = '#000000';
        this.totalPresentBgColor = '#1fc94f';
        this.totalPresentTxtColor = '#000000';
        this.allowFooter = true;
        this.allowUsername = true;
        this.typeVision = typeVision
        this.groups = {}
    }
}
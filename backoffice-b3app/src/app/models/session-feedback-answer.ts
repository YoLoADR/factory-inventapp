import { NameModule } from "./name-module";

export class Answer {
    uid: string;
    answer: NameModule = new NameModule('', '', '', '', '');
    weight: number;
    marker: string;
    createdAt: number;
}
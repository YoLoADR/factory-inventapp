import { NameModule } from "./name-module";

export class GalleryFolder {
    uid: string;
    name: NameModule;
    orderGallery: string;
    createdAt: number;
    order: number;
    grid: boolean;
    allowGrid: boolean;

    constructor(name: NameModule, order: number) {
        this.uid = null;
        this.name = name;
        this.orderGallery = 'recent';
        this.order = order;
        this.createdAt = null;
        this.grid = true;
        this.allowGrid = true;
    }
}
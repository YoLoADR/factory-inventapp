import { TypeLogin } from "../enums/type-login";

export interface Container {
    uid?: string;
    clientId?: string;
    clientName?: string;
    appName?: string;
    loginPage?: {
        type?: TypeLogin;
        eventId?: string;
    };
    loginOptions?: {
        fbLoginBtn?: boolean;
        gLoginBtn?: boolean;
        registeBtn?: boolean;
        shortcodeBtn?: boolean;
    },
    logo?: string;
    logoSize?: string;
    color?: string;
    termsOfUse?: string;
    privacyTerms?: string;
}
import { NameModule } from "./name-module";

export class eventCustomFieldAnswerOption {
    uid: string;
    answer: NameModule;

    constructor() {
        this.answer = new NameModule('', '', '', '', '');
    }
}
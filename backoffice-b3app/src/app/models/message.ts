import { Participant } from "./participant";

export class Message {
  id: string;
  content: string;
  type: string;
  groupId: string;
  moduleId: string;
  sender: Participant;
  sentAt: firebase.firestore.Timestamp

  constructor(
    id: string,
    content: string,
    type: string,
    groupId: string,
    moduleId: string,
    sender: Participant,
    sentAt: firebase.firestore.Timestamp = null
  ) {
    this.id = id;
    this.content = content;
    this.type = type;
    this.groupId = groupId;
    this.moduleId = moduleId;
    this.sender = sender;
    this.sentAt = sentAt;
  }
}

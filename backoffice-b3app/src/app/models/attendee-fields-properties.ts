export class AttendeeFieldsProperties {
    required: boolean;
    unique_edit: boolean;

    constructor(required: boolean, unique_edit: boolean) {
        this.required = required;
        this.unique_edit = unique_edit;
    }

}
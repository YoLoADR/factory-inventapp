import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../providers/global/global.service';
import { DbEventsProvider } from '../providers/database/db.events';
import { DbRankingProvider } from '../providers/database/db-ranking';

@Component({
  selector: 'app-realtime-ranking',
  templateUrl: './realtime-ranking.component.html',
  styleUrls: ['./realtime-ranking.component.scss']
})
export class RealtimeRankingComponent implements OnInit {
  public eventId: string = null;
  public event: any = null;
  public attendees: Array<any> = [];
  public loader: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private dbEvent: DbEventsProvider,
    private dbRanking: DbRankingProvider,
    private zone: NgZone
  ) {
    this.eventId = this.route.snapshot.params['eventId'];
  }

  ngOnInit() {
    this.startEvent();
    this.startRanking();
  }

  startEvent() {
    this.dbEvent.getEvent(this.eventId, (event) => {
      this.event = event;
    });
  }

  startRanking() {
    this.dbRanking.getAttendeeRanking(this.eventId, (attendees) => {
      this.zone.run(() => {
        this.attendees = [];
        let attendeeesOrdered = [];
        attendeeesOrdered = attendees.sort(function (a, b) {
          if (a['points'] < b['points']) { return 1; }
          if (a['points'] > b['points']) { return -1; }
          return 0;
        });

        this.attendees = attendeeesOrdered;
        this.loader = false;
      })
    });
  }

}

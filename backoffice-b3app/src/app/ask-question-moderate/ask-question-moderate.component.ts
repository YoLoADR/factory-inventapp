import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbAskQuestionProvider } from '../providers/database/db-ask-question';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-ask-question-moderate',
    templateUrl: './ask-question-moderate.component.html',
    styleUrls: ['./ask-question-moderate.component.scss']
})
export class AskQuestionModerateComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    eventId: string = null;
    moduleId: string = null;
    itemId: string = null;
    questions: Array<any> = [];

    refQuestions: any = null;
    refQuestionsCollection: AngularFirestoreCollection<any> = null;
    loader: boolean = true;

    constructor(
        private dbAskQuestion: DbAskQuestionProvider,
        private route: ActivatedRoute,
        private afs: AngularFirestore

    ) {
        this.eventId = this.route.snapshot.params['eventId'];
        this.moduleId = this.route.snapshot.params['moduleId'];
        this.itemId = this.route.snapshot.params['itemId'];

        this.getQuestions();
    }

    ngOnInit() {
    }

    /**
     * Unsubscribe subscriptions on destroy
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Get all questions for a module and item
     */
    getQuestions() {
        this.refQuestionsCollection = this.afs
            .collection('modules')
            .doc(this.moduleId)
            .collection('items')
            .doc(this.itemId)
            .collection('questions', ref => ref.orderBy('createdAt'));


        this.subscriptions.push(this.refQuestionsCollection.valueChanges().subscribe((questions: any) => {
            let listQuestions = [];

            if (questions && questions.length > 0) {
                listQuestions = questions.sort(function (a, b) {
                    if (a.visibility == true && b.visibility == false) {
                        return 1;
                    } else if (a.visibility == false && b.visibility == true) {
                        return -1;
                    } else {
                        return 0;
                    }
                });
            }

            // questions.forEach((element) => {
            //     let question = element;
            //     listQuestions.push(question);

            //     listQuestions = listQuestions.sort(function (a, b) {
            //         if (a.visibility == true && b.visibility == false) {
            //             return 1;
            //         } else if (a.visibility == false && b.visibility == true) {
            //             return -1;
            //         } else {
            //             return 0;
            //         }
            //     });
            // });

            this.questions = listQuestions;
            this.loader = false;
        }))
    }

    /**
     * Check if index exist
     * @param array 
     * @param item 
     */
    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    /**
     * Change visibility of a question
     * @param question 
     */
    changeVisibility(question) {
        this.dbAskQuestion.updateVisibilityAskQuestionGeral(this.moduleId, this.itemId, question.uid, !question.visibility);
    }

    /**
     * Enable all questions
     */
    enableAll() {
        this.questions.forEach(element => {
            this.dbAskQuestion.updateVisibilityAskQuestionGeral(this.moduleId, this.itemId, element.uid, true);
        });
    }

    /**
     * Disable all questions
     */
    disableAll() {
        this.questions.forEach(element => {
            this.dbAskQuestion.updateVisibilityAskQuestionGeral(this.moduleId, this.itemId, element.uid, false);
        });
    }

}

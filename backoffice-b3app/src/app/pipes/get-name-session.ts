import { Pipe, PipeTransform } from '@angular/core';
import { Session } from '../models/session';

@Pipe({
    name: 'getNameSession',
    pure: true
})

export class GetNameSession implements PipeTransform {
    private userLanguage: string //user language.
    private eventLanguage: string //main language of the event.

    transform(value: Session, args:string[]): any {
        this.userLanguage = args[0]
        this.eventLanguage = args[1]
        return this.getNameSession(value);
    }

    getNameSession(session: Session): string {
        let name: string = ""

        switch (this.userLanguage) {
            case 'pt_BR':
                name = session.name.PtBR;
                break;
            case 'en_US':
                name = session.name.EnUS;
                break;
            case 'es_ES':
                name = session.name.EsES;
                break;
            case 'fr_FR':
                name = session.name.FrFR;
                break;
            case 'de_DE':
                name = session.name.DeDE;
                break;
        }

        if (name === '') {
            switch (this.eventLanguage) {
                case 'pt_BR':
                    name = session.name.PtBR;
                    break;
                case 'en_US':
                    name = session.name.EnUS;
                    break;
                case 'es_ES':
                    name = session.name.EsES;
                    break;
                case 'fr_FR':
                    name = session.name.FrFR;
                    break;
                case 'de_DE':
                    name = session.name.DeDE;
                    break;
            }
        }

        return name
    }
}
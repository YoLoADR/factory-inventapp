import { Pipe, PipeTransform } from '@angular/core';
import { Track } from '../models/track';

@Pipe({
    name: 'getNameTrack',
    pure: true
})

export class GetNameTrack implements PipeTransform {
    private userLanguage: string //user language.
    private eventLanguage: string //main language of the event.

    transform(value: Track, args:string[]): any {
        this.userLanguage = args[0]
        this.eventLanguage = args[1]
        return this.getNameTrack(value);
    }

    getNameTrack(track): string {
        let name: string = ""

        switch (this.userLanguage) {
            case 'pt_BR':
                name = track.name.PtBR;
                break;
            case 'en_US':
                name = track.name.EnUS;
                break;
            case 'es_ES':
                name = track.name.EsES;
                break;
            case 'fr_FR':
                name = track.name.FrFR;
                break;
            case 'de_DE':
                name = track.name.DeDE;
                break;
        }

        if (name === '') {
            switch (this.eventLanguage) {
                case 'pt_BR':
                    name = track.name.PtBR;
                    break;
                case 'en_US':
                    name = track.name.EnUS;
                    break;
                case 'es_ES':
                    name = track.name.EsES;
                    break;
                case 'fr_FR':
                    name = track.name.FrFR;
                    break;
                case 'de_DE':
                    name = track.name.DeDE;
                    break;
            }
        }

        return name
    }
}
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { PathStorage } from 'src/app/paths/path-storage';

@Injectable()

export class StorageService {
    storage;
    constructor() {
        this.storage = firebase.storage().ref();
    }

    profilePicture(file: any, userId: string, onResolve) {
        const ref = this.storage.child(PathStorage.profile_pictures).child(userId);
        ref.putString(file, 'base64').then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        });
    }

    profilePictureAttendees(file: any, eventId: string, userId: string, onResolve) {
        let metadata = {
            contentType: 'image/jpeg',
        };

        const ref = this.storage.child(PathStorage.profile_pictures_attendees).child(eventId).child(userId);
        ref.putString(file, 'base64', metadata).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        });
    }

    profilePictureSpeakers(file: any, eventId: string, userId: string, onResolve) {
        let metadata = {
            contentType: 'image/jpeg',
        };

        const ref = this.storage.child(PathStorage.profile_pictures_speakers).child(eventId).child(userId);
        ref.putString(file, 'base64', metadata).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        });
    }

    delete(userId: string) {
        var deleteRef = this.storage.child(PathStorage.profile_pictures).child(userId);
        deleteRef.delete();
    }

    widgetPicture(file: any, eventId: string, moduleId: string, widgetUID: string) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.widgets).child(eventId).child(moduleId).child(widgetUID);
            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(null);
                })
            }).catch((error) => {
                resolve(null);
            })
        })
    }

    deleteWidget(eventId: string, moduleId: string, widgetUID: string) {
        const deleteRef = this.storage.child(PathStorage.widgets).child(eventId).child(moduleId).child(widgetUID);
        deleteRef.delete();
    }

    locationPicture(file: any, eventId: string, moduleId: string, locationId: string, onResolve) {
        const ref = this.storage.child(PathStorage.locations).child(eventId).child(moduleId).child(locationId);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        })
    }

    deleteLocation(eventId: string, moduleId: string, locationId: string) {
        var deleteRef = this.storage.child(PathStorage.locations).child(eventId).child(moduleId).child(locationId);
        deleteRef.delete();
    }

    galleryPicture(file: any, eventId: string, moduleId: string, imageId: string, onResolve) {
        const ref = this.storage.child(PathStorage.gallery).child(eventId).child(moduleId).child(imageId);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        })
    }

    deleteGalleryPic(eventId: string, moduleId: string, imageId: string) {
        var deleteRef = this.storage.child(PathStorage.gallery).child(eventId).child(moduleId).child(imageId);
        deleteRef.delete();
    }

    documentFile(file: any, eventId: string, moduleId: string, docId: string, type, onResolve) {
        const ref = this.storage.child(PathStorage.documents).child(eventId).child(moduleId).child(file.name);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        })
    }

    deleteDocument(eventId: string, moduleId: string, docId: string) {
        var deleteRef = this.storage.child(PathStorage.documents).child(eventId).child(moduleId).child(docId);
        deleteRef.delete();
    }

    logoPicture(file: any, fileId: string, eventId: string, onResolve) {
        const ref = this.storage.child(PathStorage.event_visual).child(eventId).child(fileId);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        })
    }

    deleteLogo(eventId: string, logoId: string) {
        var deleteRef = this.storage.child(PathStorage.event_visual).child(eventId).child(logoId);
        deleteRef.delete();
    }

    touchIconPicture(file: any, fileId: string, eventId: string, onResolve) {
        const ref = this.storage.child(PathStorage.event_visual).child(eventId).child(fileId);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            })
            // .catch((error) => {
            //     onResolve(null);
            // })
        })
        // .catch((error) => {
        //     onResolve(null);
        // })
    }

    deleteTouchIcon(eventId: string, iconId: string) {
        var deleteRef = this.storage.child(PathStorage.event_visual).child(eventId).child(iconId);
        deleteRef.delete();
    }

    bannerPicture(file: any, fileId: string, eventId: string, onResolve) {
        const ref = this.storage.child(PathStorage.event_visual).child(eventId).child(fileId);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        })
    }

    deleteBanner(eventId: string, bannerId: string) {
        var deleteRef = this.storage.child(PathStorage.locations).child(eventId).child(bannerId);
        deleteRef.delete();
    }

    notificationPicture(file: any, eventId: string, moduleId: string, notificationUID: string, onResolve) {
        const ref = this.storage.child(PathStorage.notifications).child(eventId).child(moduleId).child(notificationUID);
        ref.put(file).then((snapshot) => {
            ref.getDownloadURL().then((url: string) => {
                onResolve(url);
            }).catch((error) => {
                onResolve(null);
            })
        }).catch((error) => {
            onResolve(null);
        })
    }

    deleteNotification(eventId: string, moduleId: string, notificationUID: string) {
        const deleteRef = this.storage.child(PathStorage.notifications).child(eventId).child(moduleId).child(notificationUID);
        deleteRef.delete();
    }

    /********************************************* NEWS FEED ************************************************************************************** */

    uploadImgNewsFeed(file: any, postId, eventId: string, moduleId) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.news_feed).child(eventId).child(moduleId).child(postId);

            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(error);
                })
            }).catch((error) => {
                resolve(error);
            })
        })
    }

    deletePost(postId: string, eventId: string, moduleId: string) {
        const deleteRef = this.storage.child(PathStorage.news_feed).child(eventId).child(moduleId).child(postId)
        deleteRef.delete();
    }


    uploadMapImage(file: any, storageId: string, moduleId: string, eventId: string) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.maps).child(eventId).child(moduleId).child(storageId);

            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(error);
                })
            }).catch((error) => {
                resolve(error);
            })
        })
    }

    deleteMap(storageId: string, moduleId: string, eventId: string) {
        const deleteRef = this.storage.child(PathStorage.maps).child(eventId).child(moduleId).child(storageId)
        deleteRef.delete();
    }

    uploadQuestionQuizImage(file: any, moduleId: string, quizId: string, questionId: string, eventId: string) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.quiz).child(eventId).child(moduleId).child(quizId).child(questionId);

            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(error);
                })
            }).catch((error) => {
                resolve(error);
            })
        })
    }

    uploadQuestionTrainingImage(file: any, moduleId: string, trainingId: string, questionId: string, eventId: string) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.training).child(eventId).child(moduleId).child(trainingId).child(questionId);

            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(error);
                })
            }).catch((error) => {
                resolve(error);
            })
        })
    }

    containerLogo(file: any, containerId: string) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.containers).child(containerId);
            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(null);
                })
            }).catch((error) => {
                resolve(null);
            })
        })
    }

    wdigetModuleBackground(file: any, eventId: string, moduleId: string) {
        return new Promise((resolve, reject) => {
            const ref = this.storage.child(PathStorage.widget_bgs).child(eventId).child(moduleId);
            ref.put(file).then((snapshot) => {
                ref.getDownloadURL().then((url: string) => {
                    resolve(url);
                }).catch((error) => {
                    resolve(null);
                })
            }).catch((error) => {
                resolve(null);
            })
        })
    }

}
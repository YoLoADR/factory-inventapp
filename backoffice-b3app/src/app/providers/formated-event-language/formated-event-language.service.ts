/**
 * service responsible for formatting the module name in the main language of the event.
 */


import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})

export class FormatedEventLanguageService {
    public language: string
    
    constructor (){
       this.language = environment.platform.defaultLanguage;
    }

    /**
     * @param lang
     * @returns converts the lang parameter to the event's main event event.
     */
    convertLangFormat(lang) {
        switch (lang) {
            case 'en_US' || 'en-US': {
            this.language = 'EnUS';
            break;
          }
          case 'es_ES' || 'es-ES': {
            this.language= 'EsES';
            break;
          }
          case 'fr_FR' || 'fr-FR': {
            this.language  = 'FrFR';
            break;
          }
          case 'de_DE' || 'de-DE': {
            this.language = 'DeDE';
            break;
          }
          
          default:{
            this.language = 'PtBR'
          }
        }
      }
}
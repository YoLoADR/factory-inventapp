import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../../providers/auth/auth.service';
import { PathApi } from '../../paths/path-api';
import { AngularFirestore } from '@angular/fire/firestore';
import { Container } from 'src/app/models/container';
import { StorageService } from '../storage/storage.service';

@Injectable({
    providedIn: 'root'
})
export class DbAdminUserProvider {
    public headers;
    public requestOptions;
    private db;


    constructor(private auth: AuthService, private aFirestore: AngularFirestore, private http: HttpClient, private storageService: StorageService) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };

        this.db = aFirestore.firestore;
    }

    setClaims() {
        this.http.get('http://localhost:9000/b3app-develop/us-central1/dbUserSetClaimsForExistingUsers')
            .subscribe((a) => console.log(a), (e) => { console.log(e) });
    }

    createUser(user, onResolve) {
        this.auth.createUser(user).then((newUser) => {
            onResolve(newUser)
        })
            .catch((error) => {
                onResolve(error)
            })
    }

    getSuperGods(typeOrder, onResolve) {

        let ref;

        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.db
                    .collection('users')
                    .where('type', '==', 0)
                    .orderBy('name', 'asc');
                break;

            case 'desc': //z-a
                ref = this.db
                    .collection('users')
                    .where('type', '==', 0)
                    .orderBy('name', 'desc');
                break;

            case 'recent'://distante-próximo
                ref = this.db
                    .collection('users')
                    .where('type', '==', 0)
                    .orderBy('createdAt', 'desc');
                break;

            case 'oldest': //próximo-distante
                ref = this.db
                    .collection('users')
                    .where('type', '==', 0)
                    .orderBy('createdAt', 'asc');
                break;
        }

        ref.get()
            .then((users) => {
                let listUsers = [];
                users.forEach(
                    (doc) => {
                        let user = doc.data();
                        listUsers.push(user);
                    }
                )
                onResolve({
                    code: 200,
                    message: 'success',
                    result: listUsers
                });
            })
            .catch((error) => {
                onResolve({
                    code: 400,
                    message: 'error',
                    result: error
                });
            });
    }

    getGods(typeOrder, onResolve) {
        let ref;

        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.db
                    .collection('users')
                    .where('type', '==', 1)
                    .orderBy('name', 'asc');
                break;

            case 'desc': //z-a
                ref = this.db
                    .collection('users')
                    .where('type', '==', 1)
                    .orderBy('name', 'desc');
                break;

            case 'recent'://distante-próximo
                ref = this.db
                    .collection('users')
                    .where('type', '==', 1)
                    .orderBy('createdAt', 'desc');
                break;

            case 'oldest': //próximo-distante
                ref = this.db
                    .collection('users')
                    .where('type', '==', 1)
                    .orderBy('createdAt', 'asc');
                break;
        }

        ref.get()
            .then((users) => {
                let listUsers = [];
                users.forEach(
                    (doc) => {
                        let user = doc.data();
                        listUsers.push(user);
                    }
                )

                onResolve({
                    code: 200,
                    message: 'success',
                    result: listUsers
                });
            })
            .catch((error) => {
                onResolve({
                    code: 404,
                    message: 'error',
                    result: error
                });
            });
    }

    getClients(typeOrder, onResolve) {
        let ref;

        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.db
                    .collection('users')
                    .where('type', '==', 2)
                    .orderBy('name', 'asc');
                break;

            case 'desc': //z-a
                ref = this.db
                    .collection('users')
                    .where('type', '==', 2)
                    .orderBy('name', 'desc');
                break;

            case 'recent'://distante-próximo
                ref = this.db
                    .collection('users')
                    .where('type', '==', 2)
                    .orderBy('createdAt', 'desc');
                break;

            case 'oldest': //próximo-distante
                ref = this.db
                    .collection('users')
                    .where('type', '==', 2)
                    .orderBy('createdAt', 'asc');
                break;
        }

        ref.get()
            .then((users) => {
                let listUsers = [];
                users.forEach(
                    (doc) => {
                        let user = doc.data();
                        listUsers.push(user);
                    }
                )
                onResolve({
                    code: 200,
                    message: 'success',
                    result: listUsers
                });
            })
            .catch((error) => {
                onResolve({
                    code: 404,
                    message: 'error',
                    result: error
                });
            });
    }

    checkClientWithEmail(email, onResolve) {
        this.db.collection("users")
            .where("email", "==", email)
            .where("type", "==", 2)
            .get()
            .then((snapshot) => {
                let client = null;
                snapshot.forEach((element) => {
                    client = element.data();
                })

                onResolve(client)
            }).catch((error) => {
                onResolve(error)
            })

    }

    getUser(userId, onResolve) {
        this.db
            .collection('users').doc(userId).get()
            .then((user) => {
                onResolve({
                    result: user.data()
                });
            })
            .catch((error) => {

            })
    }

    getUserByEmail(email, onResolve) {
        this.db
            .collection('users')
            .where('email', '==', email)
            .get()
            .then((data) => {

                if (data.size <= 0) {
                    onResolve(null);
                }

                let user;
                data.forEach(element => {
                    user = element.data();
                });

                onResolve(user);
            })
            .catch((error) => {
                console.log(error)
            })
    }

    removeUser(userId, onResolve) {
        this.requestOptions.params.userId = userId;

        this.http.delete(PathApi.baseUrl + PathApi.authDeleteUser, this.requestOptions).subscribe((data) => {
            onResolve(data['result']);
        }), err => {
            onResolve(err);
        }
    }

    removeClient(clientId, onResolve) {
        this.requestOptions.params.clientId = clientId;

        this.http.delete(PathApi.baseUrl + PathApi.authClientDelete, this.requestOptions).subscribe((data) => {
            onResolve(data['result']);
        }), err => {
            onResolve(err);
        }
    }

    updateUser(userId, user, onResolve) {
        this.auth.updateUserAuth(userId, user, (status) => {
            if (status['result'] == true) {
                this.checkHaveContainerAndUpdate(userId, user.name);
                this.db
                    .collection("users").doc(userId).update({
                        name: user.name,
                        email: user.email,
                        language: user.language,
                        description: user.description,
                        type: user.type,
                        uid: userId,
                        photoUrl: user.photoUrl,
                        title: user.title,
                        company: user.company,
                    })
                    .then((data) => {
                        onResolve({
                            code: 200,
                            message: 'success',
                            result: data
                        });
                    })
                    .catch((error) => {
                        onResolve({
                            code: 500,
                            message: 'error',
                            result: error
                        });
                    });
            } else {
                onResolve({
                    code: 500,
                    message: 'error-change-password',
                    result: null
                })
            }
        })
    }

    checkHaveContainerAndUpdate(clientId, name) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers');

        ref
            .where('clientId', '==', clientId)
            .get()
            .then((snapshot) => {
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let container = element.data();
                        ref.doc(container.uid).update({ clientName: name });
                    });
                }
            })
    }

    getUserProfile(uid, onResolve) {
        this.db
            .collection('users').doc(uid).get().then((data) => {
                onResolve(data.data());
            }).catch((err) => {

            });
    }

    changeOrderInternalEvents(userId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('users').doc(userId).update({ internalEventsOrder: typeOrder }).then(() => {
            onResolve(true);
        })
    }

    changeOrderClientsEvents(userId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('users').doc(userId).update({ clientsEventsOrder: typeOrder }).then(() => {
            onResolve(true);
        })
    }

    changeOrderSupergods(userId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('users').doc(userId).update({ superGodsOrder: typeOrder }).then(() => {
            onResolve(true);
        })
    }

    changeOrderGods(userId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('users').doc(userId).update({ godsOrder: typeOrder }).then(() => {
            onResolve(true);
        })
    }

    changeOrderClients(userId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('users').doc(userId).update({ clientsOrder: typeOrder }).then(() => {
            onResolve(true);
        })
    }

    changeOrderEmployees(userId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('users').doc(userId).update({ employeesOrder: typeOrder }).then(() => {
            onResolve(true);
        })
    }

    deleteAndRemakeUser(oldUser, newUser, onResolve) {
        let body = {
            oldUser: oldUser,
            newUser: newUser,
        }

        this.http.post(PathApi.baseUrl + PathApi.dbUserDeleteAndRemakeUser, body, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            },

            err => {
                onResolve(err)
            }
        )
    }

    getContainers(onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers');
        ref
            .onSnapshot((snapshot) => {
                let containers = [];

                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        containers.push(element.data());
                    });
                }
                onResolve(containers);
            })
    }

    getContainer(containerId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers').doc(containerId);
        ref
            .get()
            .then((snapshot) => {
                onResolve(snapshot.data());
            })
    }

    getContainersClient(clientId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers').where('clientId', '==', clientId);
        ref
            .onSnapshot((snapshot) => {
                let containers = [];

                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        containers.push(element.data());
                    });
                }
                onResolve(containers);
            })
    }

    createContainer(container: Container, image: any, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers').doc();
        container.uid = ref.id;
        this.storageService.containerLogo(image, container.uid)
            .then((url: string) => {
                container.logo = url;
                container = Object.assign({}, container);
                ref.set(container)
                    .then(_ => onResolve(true))
                    .catch((e) => {
                        console.error(e);
                        onResolve(false);
                    });
            })
    }

    updateContainer(container: Container, image: any, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers').doc(container.uid);
        if (image !== null && image !== undefined) {
            this.storageService.containerLogo(image, container.uid)
                .then((url: string) => {
                    container.logo = url;
                    container = Object.assign({}, container);
                    ref.update(container)
                        .then(_ => onResolve(true))
                        .catch((e) => {
                            console.error(e);
                            onResolve(false);
                        });
                })
        } else {
            container = Object.assign({}, container);
            ref.update(container)
                .then(_ => onResolve(true))
                .catch((e) => {
                    console.error(e);
                    onResolve(false);
                });
        }
    }

    deleteContainer(container: Container, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers').doc(container.uid);

        ref.delete()
            .then(_ => onResolve(true))
            .catch((e) => {
                console.error(e);
                onResolve(false);
            });
    }
}
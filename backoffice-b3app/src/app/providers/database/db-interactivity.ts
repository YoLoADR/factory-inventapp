import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { TypeModule } from 'src/app/enums/type-module';

@Injectable({
    providedIn: 'root'
})

export class DbInteractivityProvider {
    private db: firebase.firestore.Firestore

    constructor() {
        this.db = firebase.firestore()
    }

    getModule(moduleId: string, onResolve) {
        const ref = this.db.collection('modules').doc(moduleId);

        ref
        .get()
        .then((result) => {
            onResolve(result.data());
        });

    }

    getInteractivityModuleByEventId(eventId, onResolve) {
        let ref = this.db
        .collection('modules')
        .where('eventId', '==', eventId)
        .where('type', '==', TypeModule.INTERACTIVITY)
        
        ref.get()
        .then((data) => {
            let module = null;

            data.forEach(element => {
                module = element.data();
            });
            
            onResolve(module)
        })
        .catch((err) => {
            console.log(err)
        })
    }

    getListColorsCharts(moduleId, onResolve) {
        let ref = this.db
        .collection('modules')
        .doc(moduleId)
        .collection('colorsCharts')
        .orderBy('order', 'asc');

        ref.get()
        .then((data) => {

            if(data.size <= 0) {
                onResolve(null);
            }
            let listColors: Array<any> = [];

            data.forEach(element => {
                let color = element.data();
                if(color !== undefined) {
                    listColors.push(color);
                }
            });

            onResolve(listColors);
        })
    }

    addNewColorCharts(moduleId, color, onResolve) {
        let ref = this.db.collection('modules').doc(moduleId).collection('colorsCharts');
        let order = 0;

        ref.get()
        .then((data) => {
            if(data.size > 0) {
                order = data.size;
            }
            
            let doc = ref.doc();
            let colorId = doc.id;

            let aux = {
                color: color,
                order: order,
                uid: colorId
            }

            ref.doc(colorId).set(aux).then(() => {
                onResolve(aux);
            })
        })
    }

    removeColorChart(moduleId: string, colorId: string, onResolve) {
        let ref = this.db
        .collection('modules')
        .doc(moduleId)
        .collection('colorsCharts')
        .doc(colorId);
        
        ref.get()
        .then((data) => {
            let color = data.data();
            let order = color.order;
            this.reorderColors(moduleId, order, (data) => {
                if(data) {
                    ref.delete()
                    .then(() => {
                        onResolve(true)
                    })
                    .catch((err) => {
                        onResolve(err)
                    })
                }
            })
        })

    }


    reorderColors(moduleId, removeOrder, onResolve) {
        
        let db = this.db;
        let batch = db.batch()
    
        let ref = db
        .collection('modules')
        .doc(moduleId)
        .collection('colorsCharts');
    
        ref.get().then((data) => {
            if(data.size > 0) {
                let listColors = []
                data.forEach(element => {
                    let color = element.data()
                    listColors.push(color)
                });
    
                for(let color of listColors) {
                    if(color.order > removeOrder) {
                        let newOrder = color.order -1;
    
                        let refColor = ref.doc(color.uid);
                        batch.update(refColor, { order: newOrder })
                    }
                }
    
                batch.commit()
                .then(() => {
                    onResolve(true)
                })
                .catch((err) => {
                    onResolve(false)
                })
    
            } else {
                onResolve(true)
            }
        })
    }

    changeOrderColors(moduleId: string, listColors: any, onResolve) {
        let batch = this.db.batch();

        for(let color of listColors) {
            let ref = this.db.collection('modules').doc(moduleId).collection('colorsCharts').doc(color.uid)
            batch.update(ref, { order: color.order })
        }

        batch.commit()
        .then(() => {
            onResolve(true)
        })
        .catch((e) => {
            onResolve(false)
        })
    }


    updateChartOptions(eventId: string, options: any, onResolve) {
        let batch = this.db.batch();

        this.getInteractivityModuleByEventId(eventId, (module) => {
            let moduleId = module.uid;
            
            let refModule = this.db
            .collection('modules')
            .doc(moduleId)
            
            let refEvent = this.db
            .collection('events')
            .doc(eventId)
            .collection('modules')
            .doc(moduleId)
            
            let optionsObj = {};
            optionsObj['optionsChart'] = options;
    
            batch.update(refModule, optionsObj);
            batch.update(refEvent, optionsObj);
    
            batch.commit()
            .then((data) => {
                onResolve(true);
            })
            .catch((err) => {
                onResolve(false)
            })
        })

    }

    getOptionsChart(eventId: string, onResolve) {
        this.getInteractivityModuleByEventId(eventId, (module) => {
            let moduleId = module.uid;
            
            let refQuestion = this.db
            .collection('modules')
            .doc(moduleId)
    
            refQuestion.get()
            .then((data) => {
                let doc = data.data();
    
                if(doc['optionsChart'] == undefined) {
                    onResolve(null)
                }
    
                onResolve(doc['optionsChart'])
            })
            .catch((err) => {
                console.log(err)
            })
        })
        
    }
}
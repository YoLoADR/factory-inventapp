import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PathApi } from '../../paths/path-api';
import { Session } from '../../models/session';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { TypeModule } from 'src/app/enums/type-module';


@Injectable({
    providedIn: 'root'
})

export class DbPersonalScheduleProvider {
    public headers;
    public requestOptions;
    private db: firebase.firestore.Firestore

    constructor(
        private http: HttpClient,
        private aFirestore: AngularFirestore
    ) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };

        this.db = firebase.firestore()
    }

    getModule(moduleId, onResolve){
        const ref = this.db.collection('modules').doc(moduleId)

        ref.get().then((snapshot) => {
            onResolve(snapshot.data())
        })
    }

    contModulesPersonalAgendaEvent(eventId) {
        return new Promise((resolve) => {
            const ref = this.db.collection('events').doc(eventId).collection('modules').where('type', '==', TypeModule.PERSONALSCHEDULE)

            ref.get().then((snapshot) => {
                resolve(snapshot.size)
            })

        })
    }

    // oads the sessions with the number of participants registered in the weight calendar.
    getSessions(eventId: string, onResolve) {
       this.http.get(PathApi.baseUrl + PathApi.dbPersonalScheduleGetAllSessions + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data['result']['sessions'])
            }
        )
    }

}
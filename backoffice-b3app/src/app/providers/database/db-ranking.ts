import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})

export class DbRankingProvider {
    constructor(private afs: AngularFirestore) { }

    getRankingModule(moduleId: string, onResolve) {
        let db = this.afs.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((snapshot) => {
                onResolve(snapshot.data());
            });
    }

    getAttendeeRanking(eventId: string, onResolve) {
        let db = this.afs.firestore;

        db
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .orderBy('queryName', 'asc')
            // .orderBy('points', 'desc')
            .onSnapshot((snapshot) => {
                let attendees = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        attendees.push(element.data());
                    });
                }
                onResolve(attendees);
            })
    }
}
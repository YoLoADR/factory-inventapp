import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as firebase from 'firebase/app';
import { DbAttendeesProvider } from './db-attendees';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class DbBroadcastProvider {
    private APPLICATION_ID: string = environment.platform.bambuserApplicationId;
    private READONLY_API_KEY: string = environment.platform.bambuserApiKey;
    private BaseUrl: string = 'https://api.bambuser.com/';
    
    private db: firebase.firestore.Firestore
    public headers;
    public requestOptions;

    constructor(
        private aFirestore: AngularFirestore,
        private http: HttpClient,
        private dbAttendee: DbAttendeesProvider,
    ) {
        this.db = firebase.firestore()
        
        this.headers = { headers: 
            { 
                Accept: 'application/vnd.bambuser.v1+json', 
                Authorization: 'Bearer ' + this.READONLY_API_KEY 
            } 
        };

    }

    
    getModule(moduleId: string, onResolve) {
        const ref = this.db.collection('modules').doc(moduleId);

        ref
            .get()
            .then((result) => {
                onResolve(result.data());
            });
    }
    

    getBroadcasts(moduleId: string, onResolve) {
        this.http.get(this.BaseUrl + 'broadcasts?byAuthors=' + moduleId, this.headers)
            .subscribe(
                (data) => {
                    onResolve(data)
                },
    
                (err) => {
                    onResolve(err)
                }
            )

    }

    getBroadcast(broadcastId) {
        return new Promise((resolve, reject) => {
            this.http.get(this.BaseUrl + 'broadcasts/' + broadcastId, this.headers)
            .subscribe(
                (data) => {
                    resolve(data)
                },
    
                (err) => {
                    reject(err)
                }
            )
        })
    }
  
    removeBroadcast(moduleId, broadcastId) {
        let ref = this.db.collection('modules').doc(moduleId).collection('broadcasts').doc(broadcastId);

        return new Promise((resolve, reject) => {
            this.http.delete(this.BaseUrl + 'broadcasts/' + broadcastId, this.headers)
            .subscribe(
                () => {
                    ref.delete()
                    .then(() => {
                        resolve(true);
                    })
                    .catch((err) => {
                        reject(err);
                    })
                },
                (err) => {
                    reject(err);
                }
            )
        })
    }

}
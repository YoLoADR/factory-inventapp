import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PathApi } from 'src/app/paths/path-api';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DbAnalyticsProvider {
    public headers;
    public requestOptions;

    constructor(
        private http: HttpClient,
        private afs: AngularFirestore
    ) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
    }

    getAttendeeInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsGetAtteendesInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    getChatInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsGetChatInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    getFeedInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsGetFeedInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    getModulesAccess(eventId: string, onResolve) {
        let db = this.afs.firestore;

        db
            .collection('events')
            .doc(eventId)
            .collection('modules')
            // .where('eventId', '==', eventId)
            .orderBy('total_access', 'desc')
            .limit(5)
            .get()
            .then((snapshot) => {
                let modules = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        modules.push(element.data());
                    });
                }
                onResolve(modules);
            })
        // .orderBy('total_access', 'desc')
        // .limit(5)
        // .get()
        // .then((snapshot) => {
        //     let modules = [];
        //     if (snapshot.size >= 1) {
        //         snapshot.forEach(element => {
        //             modules.push(element.data());
        //         });
        //     }
        //     onResolve(modules);
        // });
    }

    getDocumentsAccess(eventId: string, onResolve) {
        let db = this.afs.firestore;

        db
            .collection('events')
            .doc(eventId)
            .collection('documents')
            .orderBy('total_access', 'desc')
            .limit(5)
            .get()
            .then((snapshot) => {
                let documents = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        documents.push(element.data());
                    });
                }
                onResolve(documents);
            });
    }

    getGalleryImageAccess(eventId: string, onResolve) {
        let db = this.afs.firestore;

        db
            .collection('events')
            .doc(eventId)
            .collection('gallery-images')
            .orderBy('total_access', 'desc')
            .limit(5)
            .get()
            .then((snapshot) => {
                let images = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        images.push(element.data());
                    });
                }
                onResolve(images);
            });
    }

    /**
     * Get totalRoomsForEvent
     * @param eventId 
     */
    getTotalRoomsForEvent(eventId: string) {
        return (this.afs.collection('events').doc(eventId).collection('sessions', (ref) => ref.where('visio.meetingId', '>', '')).get().pipe(
            switchMap((docs) => {
                return (of(docs.size));
            })
        ).toPromise());
    }

    /**
     * Get total access of visio for event
     * @param eventId 
     */
    getTotalAccessForEvent(eventId: string) {
        return (this.afs.collection('analytics').doc(eventId).collection('total-visio-access').get().pipe(
            switchMap((docs) => {
                return (of(docs.size));
            })
        ).toPromise());
    }

    /**
     * Get total rooms for 2
     * @param eventId 
     */
    getTotalRoomsFor2(eventId: string) {
        return (this.afs.collection('analytics').doc(eventId).collection('total-visio-for-2').get().pipe(
            switchMap((docs) => {
                return (of(docs.size));
            })
        ).toPromise());
    }

    exportUserInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsExportAllUserInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    exportModulesInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsExportAllModulesInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    exportFeedInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsExportAllFeedInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    exportDocumentsInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsExportAllDocumentsInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

    exportGalleryImagesInfos(eventId: string, onResolve) {
        this.http.get(PathApi.baseUrl + PathApi.analyticsExportAllGalleryInfos + '?eventId=' + eventId, this.requestOptions).subscribe(
            data => {
                onResolve(data)
            }
        )
    }

}
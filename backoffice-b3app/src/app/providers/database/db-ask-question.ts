import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PathApi } from '../../paths/path-api';
import { AngularFirestore } from '@angular/fire/firestore';
import { TypeModule } from 'src/app/enums/type-module';
import { Question } from '../../../app/models/ask-question';
import { map, switchMap, tap, catchError } from 'rxjs/operators';
import { Observable, of, forkJoin } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DbAskQuestionProvider {
    headers: HttpHeaders;
    requestOptions: any;
    db: firebase.firestore.Firestore;

    constructor(
        private http: HttpClient,
        private aFirestore: AngularFirestore
    ) {
        this.headers = new HttpHeaders();
        this.db = aFirestore.firestore;
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.requestOptions = { headers: this.headers };
    }

    /**
     *
     * @param eventId
     * @param onResolve
     */
    getQuestionModuleAndAskItems(eventId: string, onResolve) {
        let moduleId;
        let ref = this.db
            .collection('events')
            .doc(eventId)
            .collection('modules');
        let listAskQuestions = [];
        ref.where('type', '==', TypeModule.ASK_QUESTION)
            .get()
            .then((snapshot) => {
                if (snapshot.size >= 1) {
                    snapshot.forEach((element) => {
                        moduleId = element.data().uid;
                    });

                    let refQuestion = this.db
                        .collection('modules')
                        .doc(moduleId)
                        .collection('items');

                    refQuestion.get().then((data) => {
                        data.forEach((element) => {
                            let question = element.data();
                            this.getAskItem(
                                moduleId,
                                question.uid,
                                (questions) => {
                                    question.questions = questions;
                                    listAskQuestions.push(question);
                                    if (listAskQuestions.length == data.size) {
                                        onResolve({
                                            questions: listAskQuestions,
                                            moduleId: moduleId
                                        });
                                    }
                                }
                            );
                        });
                    });
                }
            })
            .catch((e) => {
                onResolve(e);
            });
    }

    getAskQuestionModule(eventId: string, onResolve) {
        let ref = this.db
            .collection('events')
            .doc(eventId)
            .collection('modules');
        ref.where('type', '==', TypeModule.ASK_QUESTION)
            .get()
            .then((snapshot) => {
                let auxModule = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach((element) => {
                        auxModule.push(element.data());
                    });
                }
                onResolve(auxModule);
            })
            .catch((e) => {
                console.log();
                onResolve(e);
            });
    }

    getAskItem(moduleId, itemId, onResolve) {
        let ref = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId);

        ref.get().then((data) => {
            let item = data.data();

            onResolve(item);
        });
    }

    getQuestions(moduleId, onResolve) {
        let ref = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .orderBy('order', 'asc');

        ref.get().then((data) => {
            let listQuestions = [];
            data.forEach((element) => {
                listQuestions.push(element.data());
            });
            onResolve(listQuestions);
        });
    }

    changeOrder(moduleId, questions, onResolve) {
        let batch = this.db.batch();

        for (let question of questions) {
            let ref = this.db
                .collection('modules')
                .doc(moduleId)
                .collection('items')
                .doc(question.uid);
            batch.update(ref, { order: question.order });
        }

        batch
            .commit()
            .then(() => {
                console.log('success');
                onResolve(true);
            })
            .catch((e) => {
                console.log(e);
                onResolve(false);
            });
    }

    updateVisibilitySession(eventId, moduleId, sessionId, status, onResolve) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId);
        const refEvent = this.db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId);

        let batch = this.db.batch();

        batch.update(refModule, { askQuestion: status });
        batch.update(refEvent, { askQuestion: status });

        batch
            .commit()
            .then(() => {
                onResolve(true);
            })
            .catch((err) => {
                onResolve(false);
            });
    }

    updateAnonymous(moduleId: string, questionId: string, anonymous: boolean, onResolve?) {
        const moduleRef = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(questionId);

        moduleRef.update({
            anonymous
        }).then(() => {
            onResolve(true);
        })
        .catch((err) => {
            onResolve(false);
        });
    }

    updateVisibility(moduleId, questionId, visibility, onResolve) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(questionId);
        let batch = this.db.batch();

        batch.update(refModule, { visibility: visibility });

        batch
            .commit()
            .then(() => {
                onResolve(true);
            })
            .catch((err) => {
                onResolve(false);
            });
    }

    updateModerateSession(eventId, moduleId, sessionId, status, onResolve) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId);
        const refEvent = this.db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId);

        let batch = this.db.batch();

        batch.update(refModule, { askModerate: status });
        batch.update(refEvent, { askModerate: status });

        batch
            .commit()
            .then(() => {
                onResolve(true);
            })
            .catch((err) => {
                onResolve(false);
            });
    }

    updateAnonymousSession(
        eventId: string,
        moduleId: string,
        sessionId: string,
        anonymous: boolean,
        onResolve
    ) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId);
        const refEvent = this.db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId);
        const batch = this.db.batch();

        batch.update(refModule, { anonymous: anonymous });
        batch.update(refEvent, { anonymous: anonymous });

        batch
            .commit()
            .then(() => {
                onResolve(true);
            })
            .catch((err) => {
                onResolve(false);
            });
    }

    updateModerate(moduleId, questionId, status, onResolve) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(questionId);

        let batch = this.db.batch();

        batch.update(refModule, { moderate: status });

        batch
            .commit()
            .then(() => {
                onResolve(true);
            })
            .catch((err) => {
                onResolve(false);
            });
    }

    getQuestionsSession(moduleId, sessionId, onResolve) {
        let db = this.aFirestore.firestore;

        const refModule = db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions');

        //busca as questões ordenadas da mais antiga pra mais nova
        refModule.orderBy('createdAt').onSnapshot((snapshot) => {
            let totalQuestions = snapshot.size;
            let listQuestions = [];

            //passa por todas as questões
            let cont = 0;
            snapshot.forEach((element) => {
                let question = element.data();
                //marca que o usuário que solicitou as questões ainda não curtiu
                question.userLiked = false;

                refModule
                    .doc(question.uid)
                    .collection('votes')
                    .onSnapshot((snapshotVotes) => {
                        //caso a questão tenha votos
                        if (snapshotVotes.size >= 1) {
                            question.totalVotes = snapshotVotes.size;

                            let index = this.checkIndexExists(
                                listQuestions,
                                question
                            );

                            if (index >= 0) {
                                listQuestions[index] = question;
                            } else {
                                listQuestions.push(question);
                            }

                            if (cont == totalQuestions - 1) {
                                // console.log('sort')
                                // listQuestions.sort(function (a, b) {
                                //     if (a.totalVotes < b.totalVotes) {
                                //         return 1;
                                //     }
                                //     if (a.totalVotes > b.totalVotes) {
                                //         return -1;
                                //     }
                                //     // a must be equal to b
                                //     return 0;
                                // });

                                onResolve(listQuestions);
                            }

                            cont++;
                        } else {
                            // caso a questão não tenha votos
                            question.totalVotes = 0;

                            let index = this.checkIndexExists(
                                listQuestions,
                                question
                            );

                            if (index >= 0) {
                                listQuestions[index] = question;
                            } else {
                                listQuestions.push(question);
                            }

                            if (cont == totalQuestions - 1) {
                                // console.log('sort')
                                // listQuestions.sort(function (a, b) {
                                //     if (a.totalVotes < b.totalVotes) {
                                //         return 1;
                                //     }
                                //     if (a.totalVotes > b.totalVotes) {
                                //         return -1;
                                //     }
                                //     // a must be equal to b
                                //     return 0;
                                // });

                                onResolve(listQuestions);
                            }

                            cont++;
                        }
                    });
            });
        });
    }

    createQuestion(moduleId, name, visibility, moderate, order, onResolve) {
        let ref = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc();

        let questionId = ref.id;

        let question: Question = new Question();
        question.uid = questionId;
        question.name = name;
        question.visibility = visibility;
        question.moderate = moderate;
        question.order = order;

        question = Object.assign({}, question);
        question.name = Object.assign({}, question.name);

        ref.set(question).then(() => {
            onResolve(question);
        });
    }

    editQuestion(moduleId, question, onResolve) {
        let ref = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(question.uid);

        let newQuestion: Question = new Question();
        newQuestion.uid = question.uid;
        newQuestion.name = question.name;
        newQuestion.visibility = question.visibility;
        newQuestion.moderate = question.moderate;

        newQuestion = Object.assign({}, newQuestion);
        newQuestion.name = Object.assign({}, newQuestion.name);

        ref.update(newQuestion)
            .then(() => {
                onResolve(true);
            })
            .catch(() => {
                onResolve(false);
            });
    }

    /**
     * Delete an item for ask a question
     * @param moduleId 
     * @param itemId 
     */
    deleteQuestion(moduleId, itemId) {
        let refItem = this.aFirestore.collection('modules').doc(moduleId).collection('items').doc(itemId).ref;
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .collection('questions')
            .get().pipe(
                switchMap(async (docs) => {
                    if (docs.size > 0) {

                        let obsArray$ = [];

                        for (let question of docs.docs) {

                            obsArray$.push(this.aFirestore
                                .collection("moduleId")
                                .doc(moduleId)
                                .collection('items')
                                .doc(itemId)
                                .collection('questions')
                                .doc(question.id)
                                .collection('votes')
                                .get().pipe(
                                    switchMap(async (docs) => {
                                        let batch = this.aFirestore.firestore.batch();
                                        if (docs.size > 0) {
                                            for (let iV = 0; iV < docs.docs.length; iV++) {
                                                batch.delete(docs.docs[iV].ref);
                                            }
                                        }

                                        batch.delete(question.ref);
                                        batch.delete(refItem);

                                        await batch.commit();
                                        return (of(true).toPromise());
                                    })
                                )
                            )
                        }

                        return (forkJoin(obsArray$).toPromise());
                    } else {
                        await refItem.delete();
                        return (of(true).toPromise());
                    }
                })
            ))
    }

    /**
     * Clear result for a question of an item
     * @param moduleId 
     * @param itemId 
     */
    clearResultQuestion(moduleId, itemId) {
        return (this.aFirestore
            .collection("moduleId")
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .collection('questions')
            .get().pipe(
                switchMap((docs) => {
                    if (docs.size > 0) {

                        let obsArray$ = [];

                        for (let question of docs.docs) {
                            obsArray$.push(this.aFirestore
                                .collection("moduleId")
                                .doc(moduleId)
                                .collection('items')
                                .doc(itemId)
                                .collection('questions')
                                .doc(question.id)
                                .collection('votes')
                                .get().pipe(
                                    switchMap(async (docs) => {
                                        let batch = this.aFirestore.firestore.batch();

                                        if (docs.size > 0) {
                                            for (let iV = 0; iV < docs.docs.length; iV++) {
                                                batch.delete(docs.docs[iV].ref);
                                            }
                                        }

                                        batch.delete(question.ref);
                                        await batch.commit();
                                        return (of(true).toPromise());
                                    })
                                )
                            )
                        }

                        return (forkJoin(obsArray$));
                    } else {
                        return (of(true));
                    }
                })
            ))
    }

    /**
     * Clear questions and votes of an item for a session
     * @param eventId 
     * @param moduleId 
     * @param sessionId 
     */
    clearResultQuestionSession(eventId, moduleId, sessionId) {
        let eventObs = this.aFirestore
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .get().pipe(
                switchMap((docs) => {
                    if (docs.size > 0) {

                        let obsArray$ = [];

                        for (let question of docs.docs) {
                            obsArray$.push(this.aFirestore
                                .collection("events")
                                .doc(eventId)
                                .collection('sessions')
                                .doc(sessionId)
                                .collection('questions')
                                .doc(question.id)
                                .collection('votes')
                                .get().pipe(
                                    switchMap(async (docs) => {
                                        let batch = this.aFirestore.firestore.batch();
                                        if (docs.size > 0) {
                                            for (let iV = 0; iV < docs.docs.length; iV++) {
                                                batch.delete(docs.docs[iV].ref);
                                            }
                                        }

                                        batch.delete(question.ref);
                                        await batch.commit();

                                        return (of(true).toPromise());
                                    })
                                ));
                        }

                        return (forkJoin(obsArray$));
                    } else {
                        return (of(true));
                    }
                })
            )

        let moduleObs = this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .get().pipe(
                switchMap((docs) => {
                    if (docs.size > 0) {
                        let obsArray$ = [];

                        for (let question of docs.docs) {
                            obsArray$.push(this.aFirestore
                                .collection("modules")
                                .doc(moduleId)
                                .collection('sessions')
                                .doc(sessionId)
                                .collection('questions')
                                .doc(question.id)
                                .collection('votes')
                                .get().pipe(
                                    switchMap(async (docs) => {
                                        let batch = this.aFirestore.firestore.batch();
                                        if (docs.size > 0) {
                                            for (let iV = 0; iV < docs.docs.length; iV++) {
                                                batch.delete(docs.docs[iV].ref);
                                            }
                                        }

                                        batch.delete(question.ref);
                                        await batch.commit();
                                        return (of(true).toPromise());
                                    })
                                )
                            )
                        }

                        return (forkJoin(obsArray$));
                    } else {
                        return (of(true));
                    }
                })
            )


        return (forkJoin([eventObs, moduleObs]));
    }

    /**
     * Clear all sessions questions results
     * @param moduleId 
     * @param questionsIds 
     */
    clearAllSessionsQuestionsResults(eventId: string, sessions: Array<any>) {
        let clearSessionsResults$ = [];

        for (let session of sessions) {

            clearSessionsResults$.push(this.aFirestore
                .collection('events')
                .doc(eventId)
                .collection('sessions')
                .doc(session.uid)
                .collection('questions')
                .get().pipe(
                    switchMap((docs) => {
                        if (docs.size > 0) {
                            let obsArray$ = [];

                            for (let question of docs.docs) {
                                obsArray$.push(this.aFirestore
                                    .collection("events")
                                    .doc(eventId)
                                    .collection('sessions')
                                    .doc(session.uid)
                                    .collection('questions')
                                    .doc(question.id)
                                    .collection('votes')
                                    .get().pipe(
                                        switchMap(async (docs) => {
                                            let batch = this.aFirestore.firestore.batch();
                                            if (docs.size > 0) {
                                                for (let iV = 0; iV < docs.docs.length; iV++) {
                                                    batch.delete(docs.docs[iV].ref);
                                                }
                                            }

                                            batch.delete(question.ref);
                                            await batch.commit();

                                            return (of(true).toPromise());
                                        })
                                    ));
                            }

                            return (forkJoin(obsArray$));
                        } else {
                            return (of(true));
                        }
                    })
                ))

            clearSessionsResults$.push(this.aFirestore
                .collection('modules')
                .doc(session.moduleId)
                .collection('sessions')
                .doc(session.uid)
                .collection('questions')
                .get().pipe(
                    switchMap((docs) => {
                        if (docs.size > 0) {

                            let obsArray$ = [];

                            for (let question of docs.docs) {
                                obsArray$.push(this.aFirestore
                                    .collection("modules")
                                    .doc(session.moduleId)
                                    .collection('sessions')
                                    .doc(session.uid)
                                    .collection('questions')
                                    .doc(question.id)
                                    .collection('votes')
                                    .get().pipe(
                                        switchMap(async (docs) => {
                                            let batch = this.aFirestore.firestore.batch();
                                            if (docs.size > 0) {
                                                for (let iV = 0; iV < docs.docs.length; iV++) {
                                                    batch.delete(docs.docs[iV].ref);
                                                }
                                            }

                                            batch.delete(question.ref);
                                            await batch.commit();
                                            return (of(true).toPromise());
                                        })
                                    )
                                )
                            }

                            return (forkJoin(obsArray$));
                        } else {
                            return (of(true));
                        }
                    })
                ))
        }
        return (forkJoin(clearSessionsResults$));
    }

    /**
     * Clear all questions results
     * @param moduleId 
     * @param questionsIds 
     */
    clearAllQuestionsResults(moduleId: string, questionsIds: Array<string>) {
        let questionsArray$ = [];

        for (let questionId of questionsIds) {
            questionsArray$.push(this.aFirestore.collection('modules')
                .doc(moduleId)
                .collection('items')
                .doc(questionId)
                .collection('questions').get().pipe(
                    switchMap((items) => {
                        if (items.size > 0) {
                            let votesArrays$ = [];

                            for (let item of items.docs) {
                                votesArrays$.push(this.aFirestore.collection('modules')
                                    .doc(moduleId)
                                    .collection('items')
                                    .doc(questionId)
                                    .collection('questions')
                                    .doc(item.id)
                                    .collection('votes').get().pipe(
                                        switchMap(async (votes) => {
                                            if (votes.size > 0) {
                                                for (let vote of votes.docs) {
                                                    await vote.ref.delete();
                                                }
                                            }

                                            await item.ref.delete();
                                            return (of(true).toPromise());
                                        })
                                    ))
                            }
                            return (forkJoin(votesArrays$));
                        } else {
                            return (of(true));
                        }
                    })
                ))
        }

        return (forkJoin(questionsArray$));
    }

    checkIndexExists(array, item) {
        return array
            .map(function (e) {
                return e.uid;
            })
            .indexOf(item.uid);
    }

    updateVisibilityAskQuestion(
        eventId,
        moduleId,
        sessionId,
        questionId,
        visibility
    ) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(questionId);

        const refEvent = this.db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(questionId);

        let batch = this.db.batch();
        batch.update(refModule, { visibility: visibility });
        batch.update(refEvent, { visibility: visibility });

        batch.commit().then(() => {
            console.log('Update visibility');
        });
    }

    updateVisibilityAskQuestionGeral(moduleId, itemId, questionId, visibility) {
        const refModule = this.db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .collection('questions')
            .doc(questionId);

        refModule.update({ visibility: visibility }).then(() => {
            console.log('Update visibility');
        });
    }

    exportAskQuestion(moduleId, sessionId, onResolve) {
        this.http
            .get(
                PathApi.baseUrl +
                    PathApi.dbAskQuestionExport +
                    '?moduleId=' +
                    moduleId +
                    '&sessionId=' +
                    sessionId,
                this.requestOptions
            )
            .subscribe(
                (data) => {
                    onResolve(data);
                },

                (err) => {
                    onResolve(err);
                }
            );
    }

    exportAskQuestionGeral(moduleId, itemId, onResolve) {
        this.http
            .get(
                PathApi.baseUrl +
                    PathApi.dbAskQuestionExportGeral +
                    '?moduleId=' +
                    moduleId +
                    '&itemId=' +
                    itemId,
                this.requestOptions
            )
            .subscribe(
                (data) => {
                    onResolve(data);
                },

                (err) => {
                    onResolve(err);
                }
            );
    }
}

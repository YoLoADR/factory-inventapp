import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class BambuserService {
    
    private APPLICATION_ID: string = environment.platform.bambuserApplicationId;
    private READONLY_API_KEY: string = environment.platform.bambuserApiKey;
    private BaseUrl: string = 'https://api.bambuser.com/';
    
    public headers;
    public requestOptions;
    
    constructor(private http: HttpClient) {
        this.headers = new Headers();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
        
    }

    getBroadcast(broadcastId) {
        this.http.get(this.BaseUrl + 'broadcasts/' + broadcastId, this.requestOptions)
        .subscribe(
            (data) => {
                return data;
            },

            (err) => {
                return err;
            }
        )
    } 
}
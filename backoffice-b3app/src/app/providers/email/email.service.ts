import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PathApi } from '../../paths/path-api';

@Injectable({
    providedIn: 'root'
})
export class EmailProvider {
    public headers;
    public requestOptions;

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
    }

    sendEmailToUser(email, onResolve) {
        this.http.post(PathApi.baseUrl + PathApi.emailsSendToUser, email, this.requestOptions)
            .subscribe((status) => {
                onResolve(status)
            });
    }

    sendEmailResetPassword(emails, titleEmail, passTitle, uidUser, onResolve) {
        let body = {
            emails: emails,
            title: titleEmail,
            uid: uidUser,
            password_title: passTitle
        }
        this.http.post(PathApi.baseUrl + PathApi.emailsResetPasswordUser, body, this.requestOptions)
            .subscribe((status) => {
                onResolve(status)
            });
    }
    sendEmailRoomPairInvite(email, onResolve) { }
    sendEmailRoomPairAccept(email, onResolve) { }
    sendEmailRoomPairDecline(email, onResolve) { }
}
import { TestBed } from "@angular/core/testing";

import { GroupDiscussionsService } from "./group-discussions.service";

describe("groupDiscussionsService", () => {
  let service: GroupDiscussionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupDiscussionsService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});

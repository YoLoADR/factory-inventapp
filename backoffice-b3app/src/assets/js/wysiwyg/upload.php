<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, origin");

if(isset($_FILES['upload'])) {
    $info = pathinfo($_FILES['upload']['name']);
    $ext = $info['extension'];
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $newname = substr(str_shuffle(str_repeat($pool, 5)), 0, 16) . "." . $ext;
    $target = 'assets/images/wysiwyg/'.$newname;
    $url = 'https://teste.b3app.com/'.$target;
    move_uploaded_file($_FILES['upload']['tmp_name'], '../../images/wysiwyg/'.$newname);

    $response = [
        'uploaded' => 1,
        'fileName' => $newname,
        'url' => $url
    ];
    echo json_encode($response);
} else {
    $response = [
        'uploaded' => 0,
        'error' => [
            'message' => 'Nenhum arquivo enviado.',
        ]
    ];
    echo json_encode($response);
}
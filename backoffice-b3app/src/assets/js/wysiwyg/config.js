/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
  // Define changes to default configuration here. For example:
  // config.language = 'fr';
  // config.uiColor = '#AADC6E';
  config.extraPlugins =
    "youtube,divarea,filetools,embedbase,html5video,widget,widgetselection,clipboard,lineutils,pbckcode,videodetector,oembed,autolink,textmatch";
  config.filebrowserUploadUrl =
    "https://teste.b3app.com/assets/js/wysiwyg/upload.php";
  config.skin = "minimalist";
  config.resize_enabled = false;
  config.basicEntities = false;
  // CONFIG EN PLUS EN CAS DE BUG //
  config.enterMode = CKEDITOR.ENTER_BR;
  config.forceEnterMode;
  // config.fillEmptyBlocks = false;
  // config.entities = false;
  // config.entities_latin = false;
  // config.fillEmptyBlocks;
  // config.ignoreEmptyParagraph;
  config.toolbarGroups = [
    {
      name: "document",
      groups: ["mode", "document", "doctools"],
    },
    {
      name: "clipboard",
      groups: ["clipboard", "undo"],
    },
    {
      name: "editing",
      groups: ["find", "selection", "spellchecker", "editing"],
    },
    {
      name: "forms",
      groups: ["forms"],
    },
    {
      name: "basicstyles",
      groups: ["basicstyles", "cleanup"],
    },
    {
      name: "links",
      groups: ["links"],
    },
    {
      name: "insert",
      groups: ["insert"],
    },
    "/",
    {
      name: "styles",
      groups: ["styles"],
    },
    {
      name: "colors",
      groups: ["colors"],
    },
    {
      name: "paragraph",
      groups: ["list", "indent", "blocks", "align", "bidi", "paragraph"],
    },
    {
      name: "tools",
      groups: ["tools"],
    },
    {
      name: "others",
      groups: ["others"],
    },
  ];
  config.removeButtons =
    "Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Superscript,Subscript,Flash,SpecialChar,Smiley,HorizontalRule,Maximize,ShowBlocks,Preview,Print,Replace,Find,SelectAll,Scayt,NewPage,Templates,Copy,Outdent,Indent,BulletedList,NumberedList,RemoveFormat,CopyFormatting,Blockquote,CreateDiv,Language,BidiRtl,BidiLtr,Anchor,PageBreak";
};

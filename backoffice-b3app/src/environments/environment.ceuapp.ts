export const environment = {
    production: true,

    firebase: {
        apiKey: "AIzaSyBOY3g2pteaC9dOQ4k5MhN9dGGpFtmh5Bw",
        authDomain: "ceuapp-prod.firebaseapp.com",
        databaseURL: "https://ceuapp-prod.firebaseio.com",
        projectId: "ceuapp-prod",
        storageBucket: "ceuapp-prod.appspot.com",
        messagingSenderId: "1040918180944",
        appId: "1:1040918180944:web:f2fec8651a17bb0c",
    },

    platform: {
        name: "CéuApp - Backoffice",
        apiBaseUrl: "https://us-central1-ceuapp-prod.cloudfunctions.net/",
        loginLogo: "logo-ceuapp-login.svg",
        navLogo: "logo-nav-ceuapp.svg",
        license: "CéuApp Brasil - Todos os Direitos Reservados © 2020.",
        defaultLanguage: "pt_BR",
        defaultTimezone: "America/Sao_Paulo",
        defaultAppUrl: "https://app.ceuapp.com/#/",
        oneSignalApiId: "MDFlZTk0MjAtMjRhOS00MWEwLWIxNDgtZjUzMWE1ZjNiOTQw",
        oneSignalAppId: "a3c19564-340b-46e8-8a41-468ef87de9a0",
        defaultEventIcon:
            "https://firebasestorage.googleapis.com/v0/b/ceuapp-prod.appspot.com/o/icon-android.png?alt=media&token=2a34e2ea-a2a5-494f-91ca-c50f0a641fc5",
        bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
        bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
    },
};

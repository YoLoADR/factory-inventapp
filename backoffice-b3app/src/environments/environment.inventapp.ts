export const environment = {
    production: true,

    firebase: {
        apiKey: "AIzaSyA3NQSX1-436v5WByKK3hS9dNue44CH75U",
        authDomain: "invent-app-prod.firebaseapp.com",
        databaseURL: "https://invent-app-prod.firebaseio.com",
        projectId: "invent-app-prod",
        storageBucket: "invent-app-prod.appspot.com",
        messagingSenderId: "394492317094",
    },

    platform: {
        name: "Invent-App - Backoffice",
        apiBaseUrl: "https://us-central1-invent-app-prod.cloudfunctions.net/",
        loginLogo: "logo-vertical-inventapp.png",
        navLogo: "logo-vertical-inventapp.png",
        license: "Invent-App - Tous droits réservés © 2020.",
        defaultLanguage: "fr_FR",
        defaultTimezone: "Europe/Paris",
        defaultAppUrl: "https://app.invent-app.com/#/",
        oneSignalApiId: "YjI0NDMwNTEtZTUzZS00N2MwLTk4YjQtYWVmZDhiNzJhZTY5",
        oneSignalAppId: "fb407285-3d6d-467a-8be7-4ddf7a373873",
        defaultEventIcon:
            "https://firebasestorage.googleapis.com/v0/b/invent-app-prod.appspot.com/o/icon.png?alt=media&token=3fc0debf-25b5-4678-8fb2-2df40de3b2df",
        bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
        bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
    },
};

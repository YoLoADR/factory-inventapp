export const environment = {
    production: true,

    firebase: {
        // test
        apiKey: "AIzaSyD4CDW53IBQhrqkIFHAGZr6mC_FnDPae7E",
        authDomain: "b3app-master.firebaseapp.com",
        databaseURL: "https://b3app-master.firebaseio.com",
        projectId: "b3app-master",
        storageBucket: "b3app-master.appspot.com",
        messagingSenderId: "882162877679",
    },

    platform: {
        name: "B3App Staging- Backoffice",
        apiBaseUrl: "https://us-central1-b3app-master.cloudfunctions.net/",
        loginLogo: "logo-horizontal-b3-login.svg",
        navLogo: "logo-horizontal-b3.svg",
        license: "B3APP Corp - All Rights Reserved © 2020.",
        defaultLanguage: "en_US",
        defaultTimezone: "Europe/Paris",
        defaultAppUrl: "https://app-staging.b3app.com/#/",
        oneSignalApiId: "ZGFiY2YxYTEtYzU2ZC00ZWM1LWE2NjItY2E2OWFjN2M3NmJl",
        oneSignalAppId: "4632b112-ea3d-4866-918a-343650b2d24a",
        defaultEventIcon:
            "https://firebasestorage.googleapis.com/v0/b/b3app-master.appspot.com/o/icon.png?alt=media&token=55a86bb1-1374-4d3d-a3f2-4edec6ec4a91",
        bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
        bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
    },
};

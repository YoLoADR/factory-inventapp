export const environment = {
    production: true,

    firebase: {
        apiKey: "AIzaSyClL8A0hChJwmFPCZoHrpE56xobP5Dkgc4",
        authDomain: "b3app-prod.firebaseapp.com",
        databaseURL: "https://b3app-prod.firebaseio.com",
        projectId: "b3app-prod",
        storageBucket: "b3app-prod.appspot.com",
        messagingSenderId: "663376630834",
    },

    platform: {
        name: "B3App Prod - Backoffice",
        apiBaseUrl: "https://us-central1-b3app-prod.cloudfunctions.net/",
        loginLogo: "logo-horizontal-b3-login.svg",
        navLogo: "logo-horizontal-b3.svg",
        license: "B3APP Corp - All Rights Reserved © 2020.",
        defaultLanguage: "en_US",
        defaultTimezone: "Europe/Paris",
        defaultAppUrl: "https://app.b3app.com/#/",
        oneSignalApiId: "YjA2YzY4YTgtYzFhYi00ZTg4LTk3NTctZThjYjhhN2ZkOTY2",
        oneSignalAppId: "a8d79900-d281-4b89-bd09-27c6454a6a75",
        defaultEventIcon:
            "https://firebasestorage.googleapis.com/v0/b/b3app-master.appspot.com/o/icon.png?alt=media&token=55a86bb1-1374-4d3d-a3f2-4edec6ec4a91",
        bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
        bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
    },
};

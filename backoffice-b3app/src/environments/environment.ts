export const environment = {
    production: false,
    // firebase: {
    //     apiKey: "AIzaSyBeX_P4lo33bo_bS-xrvzXS0D4G4RPOINU",
    //     authDomain: "b3app-develop.firebaseapp.com",
    //     databaseURL: "https://b3app-develop.firebaseio.com",
    //     projectId: "b3app-develop",
    //     storageBucket: "b3app-develop.appspot.com",
    //     messagingSenderId: "613044995870"
    // },
    // platform: {
    //     name: 'B3App - Backoffice',
    //     // apiBaseUrl: 'http://localhost:9000/b3app-develop/us-central1/',
    //     apiBaseUrl: 'https://us-central1-b3app-develop.cloudfunctions.net/',
    //     loginLogo: 'logo-horizontal-b3-login.svg',
    //     navLogo: 'logo-horizontal-b3.svg',
    //     license: 'B3APP - All Rights Reserved © 2020.',
    //     defaultLanguage: 'en_US',
    //     defaultTimezone: 'Europe/Paris',
    //     defaultAppUrl: 'https://app.b3app.com/#/',
    //     oneSignalApiId: 'NDgyM2RkNDctZTYxZi00YTBiLTlhYzItZjIxMWUwN2ZmMGFi',
    //     oneSignalAppId: 'f7b1610c-09b2-4588-af3d-f2a07a60534b',
    //     defaultEventIcon: "https://firebasestorage.googleapis.com/v0/b/b3app-develop.appspot.com/o/icon.png?alt=media&token=4abf701b-5e5a-449e-ac04-db9bf8c957bd",
    //     bambuserApplicationId: '3u9sc8akbtohsISc6XbDnw',
    //     bambuserApiKey: 'EHPZTFHfSbTuoxA3CDnc2a'
    // }
    firebase: {
        // test
        apiKey: "AIzaSyD4CDW53IBQhrqkIFHAGZr6mC_FnDPae7E",
        authDomain: "b3app-master.firebaseapp.com",
        databaseURL: "https://b3app-master.firebaseio.com",
        projectId: "b3app-master",
        storageBucket: "b3app-master.appspot.com",
        messagingSenderId: "882162877679",
    },
    platform: {
        name: "B3App - Backoffice",
        apiBaseUrl: "https://us-central1-b3app-master.cloudfunctions.net/",
        loginLogo: "logo-horizontal-b3-login.svg",
        navLogo: "logo-horizontal-b3.svg",
        license: "B3APP - All Rights Reserved © 2020.",
        defaultLanguage: "en_US",
        defaultTimezone: "Europe/Paris",
        defaultAppUrl: "https://app-staging.b3app.com/#/",
        oneSignalApiId: "ZGFiY2YxYTEtYzU2ZC00ZWM1LWE2NjItY2E2OWFjN2M3NmJl",
        oneSignalAppId: "4632b112-ea3d-4866-918a-343650b2d24a",
        defaultEventIcon:
            "https://firebasestorage.googleapis.com/v0/b/b3app-master.appspot.com/o/icon.png?alt=media&token=55a86bb1-1374-4d3d-a3f2-4edec6ec4a91",
        bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
        bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
    },
};

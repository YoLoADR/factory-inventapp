const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

//
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.query.eventId;
        let db = admin.firestore();

        db
            .collection('events')
            .doc(eventId)
            .collection('feed-posts')
            .get()
            .then((snapshot) => {
                let posts = [];
                if (snapshot.size >= 1) {
                    let cont = 0;
                    snapshot.forEach(async(element) => {
                        let post = element.data();
                        let comments = 0;
                        let likes = 0;
                        post.totalComments = comments + Object.keys(post.comments).length;
                        post.totalLikes = likes + Object.keys(post.likes).length;
                        post.moduleName = await getModuleName(post.moduleId, db);
                        posts.push(post);

                        cont++;
                        if (cont == snapshot.size) {
                            res.json(posts);
                        }
                    });

                } else {
                    res.json(posts);
                }
            })
            .catch((e) => {
                let array = [];
                res.json(array);
            })
    });
});

const getModuleName = (moduleId, db) => {
    return new Promise((resolve) => {
        let ref = db.collection('modules').doc(moduleId);

        ref
            .get()
            .then((snapshot) => {
                resolve(snapshot.data().name);
            });
    })
}
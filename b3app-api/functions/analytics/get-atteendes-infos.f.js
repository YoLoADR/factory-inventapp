const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// GET ATTENDEES INFOS FOR ANALYTICS
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.query.eventId;
        let db = admin.firestore();
        // console.log('FB URL: ', configServer.fb_url);
        Promise.all([
                getEditedProfileAndUniqueAccess(eventId, db),
                getTotalAccess(eventId, db)
            ])
            .then((response) => {
                res.json({
                    editedProfile: response[0].profileEdited,
                    uniqueAccess: response[0].uniqueAccess,
                    totalAccess: response[1]
                });
            })
            .catch((e) => {
                res.send(e);
            });
    });
});


const getEditedProfileAndUniqueAccess = (eventId, db) => {
    return new Promise((resolve, reject) => {
        db
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .get()
            .then((snapshot) => {
                let uniqueAccess = [];
                let editedProfiled = [];
                let size = snapshot.size;
                let contProfile = 0;
                let contAccess = 0;
                if (size >= 1) {
                    snapshot.forEach(element => {
                        let attendee = element.data();

                        if (attendee.edited_profile == true) {
                            editedProfiled.push(attendee);
                            contProfile++;
                        } else {
                            contProfile++;
                        }

                        if (attendee.last_access !== null && attendee.last_access !== undefined) {
                            uniqueAccess.push(attendee);
                            contAccess++;
                        } else {
                            contAccess++;
                        }

                        if (contProfile == size && contAccess == size) {
                            resolve({
                                uniqueAccess: uniqueAccess.length,
                                profileEdited: editedProfiled.length
                            })
                        }
                    });
                } else {
                    resolve({
                        uniqueAccess: 0,
                        profileEdited: 0
                    })
                }
            });
    });
}

const getTotalAccess = (eventId, db) => {
    return new Promise((resolve, reject) => {
        let ref = db.collection('analytics').doc(eventId).collection('total-user-access');
        ref
            .get()
            .then((snapshot) => {
                let size = snapshot.size;
                let totalAccess = [];
                let cont = 0;
                if (size >= 1) {
                    snapshot.forEach(element => {
                        totalAccess.push(element.data());
                        cont++;
                    });
                } else {
                    resolve(0);
                }
                if (cont == size) {
                    resolve(totalAccess.length);
                }
            });
    })
}
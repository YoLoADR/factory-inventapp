const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

//
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.query.eventId;
        let db = admin.firestore();
        let ref = db.collection('events').doc(eventId).collection('modules').orderBy('total_access', 'desc');
        ref
            .get()
            .then((snapshot) => {
                let modules = [];
                let cont = 0;
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        modules.push(element.data());
                        cont++;
                    });
                    if (cont == snapshot.size) {
                        res.json(modules);
                    }
                } else {
                    res.json(modules);
                }
            })

    });
});
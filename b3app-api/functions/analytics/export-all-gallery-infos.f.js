const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

//
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.query.eventId;
        let db = admin.firestore();
        let ref = db.collection('events').doc(eventId).collection('gallery-images').orderBy('total_access', 'desc');

        ref
            .get()
            .then((snapshot) => {
                let images = [];
                let cont = 0;
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        images.push(element.data());
                        cont++;
                    });
                    if (cont == snapshot.size) {
                        res.json(images);
                    }
                } else {
                    res.json(images);
                }
            })

    });
});
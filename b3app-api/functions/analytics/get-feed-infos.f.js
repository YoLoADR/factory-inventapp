const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// GET FEED INFOS FOR ANALYTICS
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.query.eventId;
        let db = admin.firestore();

        Promise.all([
                getTotalPosts(eventId, db),
                getTotalLikes(eventId, db),
                getTotalComments(eventId, db)
            ])
            .then((response) => {
                res.json({
                    totalPosts: response[0],
                    totalLikes: response[1],
                    totalComments: response[2]
                })
            })
            .catch((e) => {
                res.send(e);
            })
    });
});

const getTotalPosts = (eventId, db) => {
    return new Promise((resolve) => {
        db
            .collection('events')
            .doc(eventId)
            .collection('feed-posts')
            .get()
            .then((posts) => {
                resolve(posts.size);
            })
            .catch((e) => {
                resolve(0);
            });
    });
}

const getTotalLikes = (eventId, db) => {
    return new Promise((resolve) => {

        db
            .collection('events')
            .doc(eventId)
            .collection('feed-posts')
            .get()
            .then((posts) => {
                let totalLikes = 0;
                let contPosts = 0;
                if (posts.size >= 1) {
                    posts.forEach(element => {
                        let post = element.data();
                        totalLikes = totalLikes + Object.keys(post.likes).length;
                        contPosts++;
                    });
                    if (contPosts == posts.size) {
                        resolve(totalLikes);
                    }
                } else {
                    resolve(0);
                }
            })
            .catch((e) => {
                resolve(0);
            });
    });
}

const getTotalComments = (eventId, db) => {
    return new Promise((resolve) => {
        db
            .collection('events')
            .doc(eventId)
            .collection('feed-posts')
            .get()
            .then((posts) => {
                let totalComments = 0;
                let contPosts = 0;
                if (posts.size >= 1) {
                    posts.forEach(element => {
                        let post = element.data();
                        totalComments = totalComments + Object.keys(post.comments).length;
                        contPosts++;
                    });
                    if (contPosts == posts.size) {
                        resolve(totalComments);
                    }
                } else {
                    resolve(0);
                }
            })
            .catch((e) => {
                resolve(0);
            });
    });
}
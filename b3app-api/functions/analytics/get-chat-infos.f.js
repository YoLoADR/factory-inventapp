const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({
        timestampsInSnapshots: true
    });
} catch (e) {
    console.log(e)
}
const cors = require("cors")({
    origin: true
});

// GET ATTENDEES INFOS FOR ANALYTICS
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.query.eventId;
        let db = admin.firestore();

        Promise.all([
                getChats(eventId, db),
                getTotalMsgs(eventId, db),
                getChatMembers(eventId, db)
            ])
            .then((response) => {
                res.json({
                    totalChats: response[0],
                    totalMsgs: response[1],
                    totalMembers: response[2]
                });
            })
            .catch((e) => {
                functions.logger.log("Error: ", e);
                res.send(e);
            });
    });
});

const getChats = (eventId, db) => {
    return new Promise((resolve) => {
        db.collection('events').doc(eventId).collection('chats').get()
            .then((chats) => {
                let totalChats = chats.size;
                resolve(totalChats);
            })
            .catch((e) => {
                resolve(0);
            });
    });
}

const getTotalMsgs = (eventId, db) => {
    return new Promise((resolve) => {
        db.collection('events').doc(eventId).collection('chats').get()
            .then((snapshot) => {
                let size = snapshot.size;
                let totalMsgs = 0;
                let cont = 0;
                if (size >= 1) {
                    snapshot.forEach(async (element) => {
                        let chat = element.data();
                        await auxGetMsgs(chat, refChat).then((msgs) => {
                            totalMsgs = totalMsgs + msgs;
                            cont++;
                            if (cont == size) {
                                resolve(totalMsgs);
                            }
                        });
                    });
                } else {
                    resolve(0);
                }
            });
    });
}

const auxGetMsgs = (chat, refChat) => {
    return new Promise((resolve) => {
        refChat
            .doc(chat.uid)
            .collection('messages')
            .get()
            .then((msgs) => {
                resolve(msgs.size);
            })
            .catch((e) => {
                resolve(0)
            });
    })
}

const getChatMembers = (eventId, db) => {
    return new Promise((resolve) => {
        let ref = db.collection('events').doc(eventId).collection('chats');

        ref
            .get()
            .then((snapshot) => {
                let cont = 0;
                let totalMembers = 0;
                let auxMembers = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let chatMembers = element.data().members;
                        let contMember = 0;
                        for (let i in chatMembers) {
                            auxMembers.push(chatMembers[i].uid);
                            if (contMember == Object.keys(chatMembers).length - 1) {
                                if (cont == snapshot.size - 1) {
                                    totalMembers = getMembersAndRemoveDouble(auxMembers);
                                    resolve(totalMembers);
                                }
                                cont++;
                            }
                            contMember++;
                        }

                    });
                } else {
                    resolve(0);
                }
            })
            .catch((e) => {
                resolve(0);
            })
    });
}


function getMembersAndRemoveDouble(array) {
    let members = array.filter(function (el, i) {
        return array.indexOf(el) == i;
    });

    return members.length;
}
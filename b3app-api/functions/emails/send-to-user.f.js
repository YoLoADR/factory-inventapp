const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });
const api_key = configServer.mailgun_api_key; //ceuapp
const domain = configServer.mailgun_domain; // ceuapp
const mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        console.log(req.body.email);
        const data = {
            //Specify email data
            from: configServer.mailgun_noreply_mail,
            //The email to contact
            to: req.body.email,
            //Subject and text data  
            subject: req.body.title,
            html: `${req.body.body}`
        };
        //Invokes the method to send emails given the above data with the helper library
        mailgun.messages().send(data, function (err, body) {
            console.log('err ', err);
            console.log('body ', body)
            if (err) {
                console.log(err);
                res.json({
                    code: 404,
                    message: 'error',
                    result: false
                });
            }
            //Else we can greet    and leave
            else {
                res.json({
                    code: 200,
                    message: 'success',
                    result: true
                });
            }
        });

    })
})
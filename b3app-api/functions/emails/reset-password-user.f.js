const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });
const api_key = configServer.mailgun_api_key; //ceuapp
const domain = configServer.mailgun_domain; // ceuapp
const mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let randomPass = Math.random().toString(36).slice(-8); // senha aleatória
        admin.auth().updateUser(req.body.uid, ({
            password: randomPass
        })).then((success) => {
            const data = {
                //Specify email data
                from: configServer.mailgun_noreply_mail,
                //The email to contact
                to: req.body.emails,
                //Subject and text data  
                subject: req.body.title,
                html: `
                <html>
    
                <head>
                    <style>
                        body {
                            background: #eeeeee;
                            font-family: Arial, Helvetica, sans-serif;
                        }
                        .card {
                            margin: 0 auto;
                            width: 50rem;
                            background-color: #ffffff !important;
                            /* color: #ffff; */
                            margin-top: 30px;
                        }
                        .logo {
                            padding-top: 40px;
                            width: 35%;
                            margin: 0 auto;
                            display: block;
                        }
                        .card-title {
                            text-align: center;
                            font-size: 1.2em;
                            margin-top: 45px;
                            text-transform: uppercase;
                            font-weight: bold;
                        }
                        .txt-aux {
                            text-align: center;
                            font-size: 1.1em;
                        }
                        .access-date {
                            text-align: center;
                            font-size: 1em;
                        }
                        .center {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                    </style>
                </head>
                
                <body>
                    <div class="card">
                        <div class="card-body">
                            <p class="card-title">${req.body.title}</p>
                            <br>
                            <p class="access-date"><strong>${req.body.password_title}</strong><br> ${randomPass} </p>
                            <br>
                        </div>
                    </div>
                </body>
                
                </html>`
            };
            //Invokes the method to send emails given the above data with the helper library
            mailgun.messages().send(data, function (err, body) {
                if (err) {
                    console.log(err);
                    res.json({
                        code: 404,
                        message: 'error',
                        result: false
                    });
                }
                //Else we can greet and leave
                else {
                    res.json({
                        code: 200,
                        message: 'success',
                        result: true
                    });
                }
            });
        })
            .catch((err) => {
                res.json({
                    code: 404,
                    message: 'error',
                    result: false
                });
            })
    })
})
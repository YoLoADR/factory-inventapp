const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let feedbackId = req.query.feedbackId;
        let questionId = req.query.questionId;

        let refQuestions = db
            .collection('modules')
            .doc(moduleId)
            .collection('session-feedbacks')
            .doc(feedbackId)
            .collection('questions')
            .doc(questionId)
            .collection('result');

        refQuestions.get()
            .then((data) => {
                if (data.size >= 1) {
                    let listUsers = [];
                    data.forEach(element => {
                        listUsers.push(element.data());
                    });

                    contUser = 0;
                    for (let user of listUsers) {
                        let userId = user.uid;

                        refQuestions
                            .doc(userId)
                            .collection('sessions')
                            .get()
                            .then(async(data) => {
                                let listResult = [];
                                data.forEach(element => {
                                    listResult.push(element.data());
                                });


                                let contResult = 0;
                                for (let result of listResult) {
                                    await refQuestions
                                        .doc(result.user)
                                        .collection('sessions')
                                        .doc(result.session)
                                        .delete()
                                        .then(async() => {
                                            await refQuestions.doc(result.user).delete().then(() => {
                                                if (contResult == listResult.length - 1) {
                                                    if (contUser == listUsers.length - 1) {
                                                        res.status(200).json({
                                                            code: 200,
                                                            message: 'OK',
                                                            result: true
                                                        })
                                                    }

                                                    contUser++;
                                                }

                                                contResult++;
                                            });

                                        })
                                }
                            })
                    }

                }
            })
    });
});
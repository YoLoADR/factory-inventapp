const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let feedbackId = req.query.feedbackId;

        getQuestions(moduleId, feedbackId, (questions) => {
            ref = db
                .collection('modules')
                .doc(moduleId)
                .collection('session-feedbacks')
                .doc(feedbackId);

            if (questions !== null) {
                Promise.all([
                    removeResults(moduleId, feedbackId, questions),
                    removeQuestions(moduleId, feedbackId, questions)
                ]).then(() => {
                    ref.get().then((data) => {
                        let feedback = data.data();
                        console.log(feedback)
                        let order = feedback.order;

                        reorderFeedbacks(moduleId, order, (value) => {
                            if (value) {
                                ref.delete()
                                    .then(() => {
                                        res.status(200).json({
                                            code: 200,
                                            message: 'OK',
                                            result: true
                                        })
                                    })
                            }
                        })
                    })
                })
            } else {
                ref.get().then((data) => {
                    let feedback = data.data();
                    console.log(feedback)
                    let order = feedback.order;

                    reorderFeedbacks(moduleId, order, (value) => {
                        if (value) {
                            ref.delete()
                                .then(() => {
                                    res.status(200).json({
                                        code: 200,
                                        message: 'OK',
                                        result: true
                                    })
                                })
                        }
                    })
                })
            }
        });
    });
});


function getQuestions(moduleId, feedbackId, onResolve) {
    let db = admin.firestore();

    let listQuestions = [];

    let refQuestions = db.collection('modules').doc(moduleId).collection('session-feedbacks').doc(feedbackId).collection('questions');

    refQuestions.get()
        .then((data) => {
            if (data.size > 0) {
                data.forEach(element => {
                    let question = element.data();
                    listQuestions.push(question);
                });

                onResolve(listQuestions);
            } else {
                onResolve(null);
            }
        })
}

function removeResults(moduleId, feedbackId, questions) {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        let cont = 0;
        for (let question of questions) {

            let refQuestions = db
                .collection('modules')
                .doc(moduleId)
                .collection('session-feedbacks')
                .doc(feedbackId)
                .collection('questions')
                .doc(question.uid)
                .collection('result');

            refQuestions.get()
                .then((data) => {
                    if (data.size >= 1) {
                        let listUsers = [];
                        data.forEach(element => {
                            listUsers.push(element.data());
                        });

                        contUser = 0;
                        for (let user of listUsers) {
                            let userId = user.uid;

                            refQuestions
                                .doc(userId)
                                .collection('sessions')
                                .get()
                                .then(async(data) => {
                                    let listResult = [];
                                    data.forEach(element => {
                                        listResult.push(element.data());
                                    });


                                    let contResult = 0;
                                    for (let result of listResult) {
                                        await refQuestions
                                            .doc(result.user)
                                            .collection('sessions')
                                            .doc(result.session)
                                            .delete()
                                            .then(async() => {
                                                await refQuestions.doc(result.user).delete().then(() => {
                                                    if (contResult == listResult.length - 1) {

                                                        if (contUser == listUsers.length - 1) {

                                                            if (cont == Object.keys(questions).length - 1) {
                                                                resolve(true)
                                                            }

                                                            cont++;
                                                        }

                                                        contUser++;
                                                    }

                                                    contResult++;
                                                });

                                            })
                                    }
                                })
                        }

                    } else {

                        if (cont == Object.keys(questions).length - 1) {
                            resolve(true);
                        }

                        cont++;
                    }
                })

        }
    })
}

function removeQuestions(moduleId, feedbackId, questions) {
    let db = admin.firestore();

    return new Promise((resolve, reject) => {
        let cont = 0;
        for (let question of questions) {
            refQuestion = db
                .collection('modules')
                .doc(moduleId)
                .collection('session-feedbacks')
                .doc(feedbackId)
                .collection('questions')
                .doc(question.uid);

            if (question.type == 'oneSelect' || question.type == 'multipleSelect') {
                let refAnswers = refQuestion
                    .collection('answers');

                refAnswers.get()
                    .then((dataAnswers) => {
                        let listAnswers = [];
                        dataAnswers.forEach(element => {
                            let answer = element.data();
                            listAnswers.push(answer);
                        });

                        let contAnswer = 0;
                        for (let answer of listAnswers) {
                            refAnswers.doc(answer.uid).delete()
                                .then(async() => {
                                    if (contAnswer == listAnswers.length - 1) {

                                        await refQuestion.delete()
                                            .then(() => {
                                                if (cont == Object.keys(questions).length - 1) {
                                                    resolve(true);
                                                }

                                                cont++;
                                            })
                                    }

                                    contAnswer++;
                                })
                        }
                    })

            } else {
                refQuestion.delete()
                    .then(() => {
                        if (cont == Object.keys(questions).length - 1) {
                            resolve(true);
                        }

                        cont++;
                    })
            }
        }
    })
}

function reorderFeedbacks(moduleId, removeOrder, onResolve) {
    console.log('reorder function:', moduleId, removeOrder)
    let db = admin.firestore()
    let batch = db.batch()

    let ref = db
        .collection('modules')
        .doc(moduleId)
        .collection('session-feedbacks');

    ref.get().then((data) => {
        if (data.size > 0) {
            let listFeedbacks = []
            data.forEach(element => {
                let feedback = element.data()
                listFeedbacks.push(feedback)
            });

            for (let feedback of listFeedbacks) {
                if (feedback.order > removeOrder) {
                    console.log(feedback.order)
                    newOrder = feedback.order - 1;
                    console.log(newOrder)

                    refFeedback = ref.doc(feedback.uid);
                    batch.update(refFeedback, { order: newOrder })
                }
            }

            batch.commit()
                .then(() => {
                    console.log('commit true')
                    onResolve(true)
                })
                .catch((err) => {
                    onResolve(false)
                })

        } else {
            onResolve(true)
        }
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.firestore
    .document('users/{userId}').onUpdate((change, context) => {
        return new Promise((resolve) => {
            let userGetted = change.after.data();

            if (!userGetted.firstAccess && userGetted.type !== 4) admin.auth().updateUser(userGetted.uid, { email: userGetted.email });

            Promise.all([
                updateInEvents(userGetted),
                updateInModules(userGetted)
            ]).then(() => {
                resolve(true);
            })
                .catch((e) => {
                    console.error(e);
                    resolve(false);
                })

        })
    });

function updateInEvents(user) {
    return new Promise((resolve) => {
        if (typeof user.events !== 'undefined') {
            let cont = 0;
            for (let eventId of user.events) {
                let refEventAttendee = admin.firestore().collection('events').doc(eventId).collection('attendees').doc(user.uid);
                refEventAttendee.update({ firstAccess: user.firstAccess, email: user.email, emailRecovery: user.emailRecovery }).then(() => {
                    if (cont == user.events.length - 1) {
                        resolve(true);
                    }

                    cont++;
                });
            }
        } else {
            resolve(true);
        }
    })
}

function updateInModules(user) {
    return new Promise((resolve) => {
        if (typeof user.attendeeModules !== 'undefined') {
            let cont = 0;
            for (let moduleId of user.attendeeModules) {
                let refModuleAttendee = admin.firestore().collection('modules').doc(moduleId).collection('attendees').doc(user.uid);
                refModuleAttendee.update({ firstAccess: user.firstAccess, email: user.email, emailRecovery: user.emailRecovery }).then(() => {
                    if (cont == user.attendeeModules.length - 1) {
                        resolve(true);
                    }

                    cont++;
                });
            }
        } else {
            resolve(true);
        }
    })
}
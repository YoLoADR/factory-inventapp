const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });


exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        console.log('*** STARTED ****')
        let db = admin.firestore();
        let auth = admin.auth();

        auth.listUsers(1000)
            .then((userRecord) => {
                let cont = 0;
                let size = userRecord.users.length;
                userRecord.users.forEach(element => {
                    let user = element.toJSON();
                    db
                        .collection('users')
                        .doc(user.uid)
                        .get()
                        .then((snapshot) => {
                            let dbUser = snapshot.data();
                            if (dbUser.type !== null && dbUser.type !== undefined) {
                                if (dbUser.type >= 4) {
                                    auth.setCustomUserClaims(user.uid, { type: dbUser.type, events: dbUser.events });
                                    console.log('CONT ', cont, size - 1);
                                    if (cont == size - 1) {
                                        res.json(true);
                                    }
                                    cont++;
                                } else {
                                    auth.setCustomUserClaims(user.uid, { type: dbUser.type });
                                    console.log('CONT ', cont, size - 1);
                                    if (cont == size - 1) {
                                        res.json(true);
                                    }
                                    cont++;
                                }
                            } else {
                                console.log('CONT ', cont, size - 1);
                                if (cont == size - 1) {
                                    res.json(true);
                                }
                                cont++;
                            }
                        });
                });
            })
            .catch((e) => { console.log(e) })
        // db
        //     .collection('users')
        //     .where('firstAccess', '==', false)
        //     .get()
        //     .then((snapshot) => {
        //         if (snapshot.size >= 1) {
        //             let cont = 0;
        //             snapshot.forEach(element => {
        //                 let user = element.data();
        //                 auth.setCustomUserClaims(user.uid, { type: user.type, events: user.events });
        //                 console.log('** CONT: ', cont, snapshot.size);
        //                 if (cont == snapshot.size - 1) {
        //                     res.json(true);
        //                 }
        //                 cont++;
        //             });
        //         } else {
        //             res.json(true);
        //         }
        //     })
    });
});


const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// send e-mail
const mailgun = require('mailgun-js')({ apiKey: configServer.mailgun_api_key, domain: configServer.mailgun_domain });

// create random code, send to user by e-mail and add to Firestore User collection to verify last
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleRef = db.collection('modules');

        let code = '';
        for (let i = 0; i < 6; i++) {
            let aux = Math.floor(Math.random() * 10);
            code += aux.toString();
        }

        db.collection("users").doc(req.body.uid).update({
            codeNumber: code
        }).then((_) => {
            let emails = [req.body.email, req.body.emailRecovery];
   
            let lastIndexModule;
            let lastIndexEvent = req.body.events.length - 1;
            let documentReference;
            if (req.body.type == 5) {
                if (req.body.attendeeModules !== undefined) {
                    lastIndexModule = req.body.attendeeModules.length - 1;
                    documentReference = req.body.attendeeModules[lastIndexModule];
                }
            } else if (req.body.type == 4) {
                if (req.body.speakerModules !== undefined) {
                    lastIndexModule = req.body.speakerModules.length - 1;
                    documentReference = req.body.speakerModules[lastIndexModule];
                }
            }

            if (typeof lastIndexModule == 'number') {
                moduleRef
                    .doc(documentReference)
                    .get()
                    .then((snapshotModule) => {
                        let module = snapshotModule.data();

                        if (!module.firstAccessCodeLanguage) {
                            module.firstAccessCodeLanguage = configServer.default_language;
                        }

                        getEvent(req.body.events[lastIndexEvent])
                            .then((eventTitle) => {
                                let translatedData = getTranslatedData(module.firstAccessCodeLanguage);

                                let data = {
                                    from: configServer.mailgun_noreply_mail,
                                    to: emails,
                                    subject: translatedData.title + ' ' + eventTitle,
                                    html: `
                            <html>
                            <head>
                                <style>
                                    body {
                                        background: #eeeeee;
                                        font-family: Arial, Helvetica, sans-serif;
                                    }
                                    .card {
                                        margin: 0 auto;
                                        width: 50rem;
                                        background-color: #ffffff !important;
                                        / color: #ffff; /
                                        margin-top: 30px;
                                    }
                                    .card-title {
                                        text-align: center;
                                        font-size: 1.2em;
                                        margin-top: 45px;
                                        text-transform: uppercase;
                                        font-weight: bold;
                                    }
                                    .txt-aux {
                                        text-align: center;
                                        font-size: 1.1em;
                                    }
                                    .code-number {
                                        font-weight: bold;
                                        font-size: 1.2em;
                                    }
                                </style>
                            </head>
                            <body>
                                <div class="card">
                                    <div class="card-body">
                                        <br>
                                        <p class="card-title">${translatedData.subtitle}</p>
                                        <br>
                                        <p class="txt-aux">${translatedData.message}</p>
                                        <p class="txt-aux code-number">${code}</p>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </body>
                            </html>
                            `
                                };
                                //Invokes the method to send emails given the above data with the helper library
                                mailgun.messages().send(data, function (err, body) {
                                    if (err) {
                                        res.status(res.statusCode).json({
                                            code: res.statusCode,
                                            message: 'email-not-send',
                                            result: false
                                        });
                                    }
                                    //Else we can greet    and leave
                                    else {
                                        res.status(res.statusCode).json({
                                            code: res.statusCode,
                                            message: 'success',
                                            result: _
                                        });

                                    }
                                })
                            });

                    })
            }


        }).catch((err) => {
            res.status(res.statusCode).json({
                code: res.statusCode,
                message: 'error',
                result: err
            });
        })
    });
});

getEvent = (eventId) => {
    return new Promise((resolve) => {
        admin
            .firestore()
            .collection('events')
            .doc(eventId)
            .get()
            .then((snapshot) => {
                let event = snapshot.data();
                resolve(event.title);
            });
    })
}

function getTranslatedData(language) {
    let data = {
        title: '',
        subtitle: '',
        message: ''
    }

    if (language == 'pt_BR') {
        data.title = 'Código de verificação de conta no app';
        data.subtitle = 'Código de verificação';
        data.message = 'Para confirmar sua identidade utilize o código abaixo no aplicativo:';
    } else if (language == 'en_US') {
        data.title = 'In-app verification code';
        data.subtitle = 'Verification code';
        data.message = 'To verify your identity use the code below in the app:';
    } else if (language == 'es_ES') {
        data.title = 'Código de verificación en la aplicación';
        data.subtitle = 'Código de verificación';
        data.message = 'Para verificar su identidad, use el siguiente código en la aplicación:';
    } else if (language == 'fr_FR') {
        data.title = 'Code de vérification de connexion à l’app';
        data.subtitle = 'Code de vérification';
        data.message = 'Afin de confirmer votre identité, veuillez saisir dans l’app le code ci-dessous:';
    } else if (language == 'de_DE') {
        data.title = 'Bestätigungscode des Kontos';
        data.subtitle = 'Verifizierungsschlüssel';
        data.message = 'Um Ihre Identität zu überprüfen, verwenden Sie den folgenden Code in der App';
    }

    return data;
}
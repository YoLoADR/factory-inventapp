const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const FieldValue = require('firebase-admin').firestore.FieldValue;
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.https.onCall((data, context) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        let eventId = data.eventId;
        let name = data.name;
        let email = data.email;
        let password = data.password;
        let language = data.language;
        let refEvent = db.collection('events').doc(eventId);
        let attendee = {
            firstAccess: false,
            uid: "",
            identifier: "",
            name: "",
            email: "",
            company: "",
            title: {
                PtBR: '',
                EnUS: '',
                EsES: '',
                FrFR: '',
                DeDE: ''
            },
            description: {
                PtBR: '',
                EnUS: '',
                EsES: '',
                FrFR: '',
                DeDE: ''
            },
            facebook: "",
            instagram: "",
            twitter: "",
            linkedin: "",
            website: "",
            picture: "",
            selfEditLink: "",
            aboutMe: "",
            eventId: "",
            language: "",
            points: 0,
            type: 5,
            chatStatus: null,
            createdAt: null,
            photoUrl: '',
            nation: "",
            city: "",
            state: "",
            phone: "",
            letter: "",
            emailRecovery: "",
            moduleId: "",
            invitationSent: null,
            invitationReceived: null,
            edited_profile: false,
            checkins: [],
            queryName: "",
        }
        let user = {
            name: "",
            email: "",
            uid: "",
            identifier: "",
            emailRecovery: "",
            company: "",
            title: "",
            description: "",
            type: 5,
            facebook: "",
            instagram: "",
            twitter: "",
            linkedin: "",
            website: "",
            picture: "",
            selfEditLink: "",
            aboutMe: "",
            phone: "",
            photoUrl: "",
            language: "pt_BR",
            token: "",
            groups: null,
            customFields: null,
            terms_use: "",
            privacity: "",
            createdAt: null,
            clientId: null,
        }

        const uuidv4 = require('uuid/v4');

        attendee.name = name;
        attendee.email = email;
        attendee.identifier = uuidv4();
        attendee.createdAt = Date.now() / 1000 | 0;
        attendee.firstAccess = false;
        attendee.language = language;
        attendee.eventId = eventId;
        attendee.queryName = name.toUpperCase();
        attendee.groups = {};
        user.name = name;
        user.firstAccess = false;
        user.email = email;
        user.type = 5;
        user.language = language;

        refEvent
            .get()
            .then((snapshot) => {
                let event = snapshot.data();
                let attendeeModuleId = event['default_attendee_module'];
                if (attendeeModuleId !== '') {

                    verifyEmailDb(user.email)
                        .then((snapshot) => {
                            if (snapshot.code == 200 && snapshot.result == 'email-not-found') {
                                // create user
                                admin.auth().createUser({ email: email, password: password })
                                    .then(async (newUser) => {
                                        if (newUser.uid !== null) {
                                            let blockAdd = false;
                                            if (blockAdd == false) {

                                                attendee.events = FieldValue.arrayUnion(eventId);
                                                attendee.uid = newUser.uid;
                                                attendee.moduleId = attendeeModuleId;
                                                user.uid = newUser.uid;
                                                user.events = FieldValue.arrayUnion(eventId);
                                                user.attendeeModules = FieldValue.arrayUnion(attendeeModuleId);
                                                user = Object.assign({}, user);
                                                attendee = Object.assign({}, attendee);
                                                await makeUsers(attendee, user, eventId).then((status) => {
                                                    blockAdd = true;
                                                    resolve({
                                                        status: true,
                                                        uid: newUser.uid
                                                    });
                                                })
                                                    .catch((e) => {
                                                        resolve({
                                                            status: false,
                                                            uid: null
                                                        });
                                                    });
                                            }
                                            // })
                                        } else {
                                            resolve({
                                                status: false,
                                                uid: null
                                            });
                                        }
                                    })
                                    .catch((e) => {
                                        resolve(e.code);
                                    });
                            } else if (snapshot.code == 200 && snapshot.result !== 'email-not-found') {
                                // account already exists
                                resolve({
                                    status: 'account-already-exist',
                                    uid: null
                                });
                            }
                        })

                } else {
                    // não tem módulo de participante configurado
                    resolve({
                        status: 'account-already-exist',
                        uid: null
                    });
                }

            });
    })
});

makeUsers = (attendee, user, eventId) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        let batch = db.batch();
        let refModule = db.collection('modules').doc(attendee.moduleId).collection('attendees').doc(user.uid);
        let refEvent = db.collection('events').doc(eventId).collection('attendees').doc(user.uid);
        let userRef = db.collection('users').doc(user.uid);

        batch.set(refModule, attendee);
        batch.set(refEvent, attendee);
        batch.set(userRef, user);

        batch
            .commit()
            .then((result) => {
                resolve(true);
            })
            .catch((e) => {
                resolve(false);
            });
    });
}

verifyEmailDb = (email) => {
    return new Promise((resolve) => {
        let db = admin.firestore();

        db
            .collection('users') // users collection
            .where('email', '==', email) // find DOC using e-mail (index)
            .get()
            .then((snapshot) => {
                let aux;
                snapshot.forEach(doc => {
                    aux = doc.data();
                });
                if (aux !== undefined) {
                    resolve({
                        code: 200,
                        message: 'success',
                        result: aux
                    });
                } else {
                    resolve({
                        code: 200,
                        message: 'success',
                        result: 'email-not-found'
                    });
                }
            })
            .catch((error) => {
                resolve({
                    code: 404,
                    message: 'error',
                    result: error
                })
            })
    })
}
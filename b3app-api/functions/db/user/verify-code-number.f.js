const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.https.onCall((data, context) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        let code = data.code;
        let userId = data.uid;

        db
            .collection("users")
            .doc(userId)
            .get()
            .then((user) => {
                let aux = user.data();
                if (aux.codeNumber == code) {
                    db.collection("users").doc(userId).update({
                        codeNumber: null
                    });
                    resolve({
                        code: 200,
                        message: 'success',
                        result: true
                    });
                } else {
                    resolve({
                        code: 404,
                        message: 'error',
                        result: false
                    });
                }
            })
            .catch((error) => {
                resolve({
                    code: 404,
                    message: 'error',
                    result: error
                });
            });
    })
});
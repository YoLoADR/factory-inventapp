const configServer = require('../../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin.firestore().collection('users').where("clientId", "==", req.query.clientId).get()
            .then((users) => {
                let listUsers = [];
                users.forEach(
                    (doc) => {
                        let user = doc.data();
                        listUsers.push(user);
                    }
                )
                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'success',
                    result: listUsers
                });
            })
            .catch((error) => {
                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'error',
                    result: error
                });
            });
    });
});
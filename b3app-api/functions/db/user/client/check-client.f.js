const configServer = require('../../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

/**
 * check email client
    return client
 */

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        const email = req.body.email;
        let db = admin.firestore();

        db.collection("users")
            .where("email", "==", email)
            .where("type", "==", 2)
            .get()
            .then((snapshot) => {
                let client = null;
                snapshot.forEach((element) => {
                    client = element.data();
                })

                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'success',
                    result: client
                });
            }).catch((error) => {
                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'sortcode search error',
                    result: error
                });
            })
    })
})
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

const cors = require("cors")({
    origin: true
});

exports = module.exports = functions.firestore.document('users/{userId}').onWrite((change, context) => {
    return new Promise((resolve) => {
        let userId = context.params.userId;
        let newUser = change.after.exists ? change.after.data() : null;
        let oldUser = change.before.exists ? change.before.data() : null;
        let type = null;
        let events = [];
        let firstAccess = true;

        if (newUser !== null) {
            type = newUser.type;
            events = newUser.events ? newUser.events : [];
            firstAccess = newUser.firstAccess;
        } else if (oldUser !== null) {
            type = oldUser.type;
            events = oldUser.events ? oldUser.events : [];
            firstAccess = oldUser.firstAccess;
        }

        if (!firstAccess) {
            admin.auth().getUser(userId)
                .then((user) => {
                    if (user) {
                        if (type >= 4) {
                            admin.auth().setCustomUserClaims(userId, {
                                type: type,
                                events: events
                            });
                        } else {
                            admin.auth().setCustomUserClaims(userId, {
                                type: type
                            });
                        }
                    } else {
                        resolve(true);
                    }
                })
        }

        resolve(true);
    });
});
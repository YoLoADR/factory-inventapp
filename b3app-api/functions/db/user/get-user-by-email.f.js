const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.https.onCall((data, context) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        let email = data.email;

        db
            .collection('users') // users collection
            .where('email', '==', email) // find DOC using e-mail (index)
            .get()
            .then((snapshot) => {
                let aux;
                snapshot.forEach(doc => {
                    aux = doc.data();
                });
                if (aux !== undefined) {
                    resolve({
                        code: 200,
                        message: 'success',
                        result: aux
                    });
                } else {
                    resolve({
                        code: 200,
                        message: 'success',
                        result: 'email-not-found'
                    });
                }
            })
            .catch((error) => {
                reject({
                    code: 404,
                    message: 'error',
                    result: error
                })
            })
    })
});
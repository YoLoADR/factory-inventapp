const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// create user in Firebase Auth e Firestore DB
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        db.collection("users").doc(req.body.oldUid).get().then((user) => {
            let aux = user.data();
            admin.auth().setCustomUserClaims(req.body.newUid, { type: aux.type }).then(() => {
                let eventsId = aux.events;
                let data = {
                    name: {
                        first: aux.name.first,
                        last: aux.name.last
                    },
                    type: aux.type,
                    email: aux.email,
                    photoUrl: aux.photoUrl,
                    createdAt: aux.createdAt,
                    company: aux.company,
                    title: aux.title,
                    description: aux.description,
                    recoveryEmail: aux.recoveryEmail,
                    uid: req.body.newUid,
                    events: aux.events,
                    firstAccess: false,
                    language: aux.language
                }

                let userObj = userModelData(data);

                for (let idEvent of eventsId) {
                    let batch = null;
                    batch = db.batch();

                    // adicionar no path users o novo
                    let newUserRef = db.collection("users").doc(req.body.newUid);
                    batch.set(newUserRef, userObj);

                    // apgar no path users antigo
                    
                    let oldUserRef = db.collection("users").doc(req.body.oldUid);
                    batch.delete(oldUserRef);

                    let oldAttendeeRef = db.collection("events").doc(idEvent).collection("attendees").doc(req.body.oldUid);
                    let newAttendeeRef = db.collection("events").doc(idEvent).collection("attendees").doc(req.body.newUid);

                    let listCustomField = [];
                    oldAttendeeRef.collection('customFields').get().then((data) => {
                        data.forEach(doc => {
                            let custom = doc.data();
                            listCustomField.push(custom);
                        });
                        // console.log(listCustomField)
                    })

                    oldAttendeeRef.get().then((data) => {
                        attendee = data.data();
                        attendee.uid = req.body.newUid;
                        let oldModulesAttendeeRef = db.collection("modules").doc(attendee.moduleId).collection('attendees')
                        .doc(req.body.oldUid);
                        let newModulesAttendeeRef = db.collection("modules").doc(attendee.moduleId).collection('attendees')
                        .doc(req.body.newUid);

                        
                        // adicionar no path events -> attendees
                        batch.set(newAttendeeRef, attendee);
                        // adicionar no path modules -> attendees novo
                        batch.set(newModulesAttendeeRef, attendee);

                        // apagar no path events -> attendes
                        batch.delete(oldAttendeeRef);
                        // apagar no path modules -> attendees antigo
                        batch.delete(oldModulesAttendeeRef);

                        batch.commit().then((batchOk) => {
                            let batchCustom = db.batch();
                            for(let custom of listCustomField) {
                                console.log(custom)
                                refCustomEventAttendee = newAttendeeRef.collection('customFields').doc(custom.uid);
                                refCustomModuleAttendee = newModulesAttendeeRef.collection('customFields').doc(custom.uid);
                                
                                batchCustom.set(refCustomEventAttendee, custom);
                                batchCustom.set(refCustomModuleAttendee, custom);
                            }

                            batchCustom.commit().then(() => {
                                console.log('add custom')
                            })

                            res.status(res.statusCode).json({
                                code: res.statusCode,
                                message: 'success',
                                result: batchOk
                            });
                        }).catch((batchError) => {
                            //remove da autenticação caso dê erro no banco
                            console.log("Catchhhh")
                            console.log(batchError)
                            admin.auth().deleteUser(newUid);

                            res.status(res.statusCode).json({
                                code: res.statusCode,
                                message: 'error',
                                result: batchError
                            });
                        })
                    });
                }
            }).catch((errorSetClaim) => {
                //remove da autenticação caso de erro no banco
                admin.auth().deleteUser(req.body.newUid);

                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'error',
                    result: errorSetClaim
                });
            })
        }).catch((error) => {
            res.status(res.statusCode).json({
                code: res.statusCode,
                message: 'error',
                result: error
            });
        });

    });
});

function userModelData(user) {
    let userFormat = {
        name: {
            first: "",
            last: ""
        },
        type: null,
        email: "",
        language: "",
        description: "",
        photoUrl: "",
        company: "",
        title: "",
        recoveryEmail: "",
        events: null,
        firstAccess: "",
        uid: ""
    };

    if (user.name.first != undefined) {
        userFormat.name.first = user.name.first;
    }

    if (user.name.last != undefined) {
        userFormat.name.last = user.name.last;
    }

    if (user.type != undefined) {
        userFormat.type = user.type;
    }

    if (user.email != undefined) {
        userFormat.email = user.email;
    }

    if (user.language != undefined) {
        userFormat.language = user.language;
    }

    if (user.description != undefined) {
        userFormat.description = user.description;
    }

    if (user.photoUrl != undefined) {
        userFormat.photoUrl = user.photoUrl;
    }

    if (user.company != undefined) {
        userFormat.company = user.company;
    }

    if (user.title != undefined) {
        userFormat.title = user.title;
    }

    if (user.recoveryEmail != undefined) {
        userFormat.recoveryEmail = user.recoveryEmail;
    }

    if (user.events != undefined) {
        userFormat.events = user.events;
    }

    if (user.firstAccess != undefined) {
        userFormat.firstAccess = user.firstAccess;
    }

    if (user.uid != undefined) {
        userFormat.uid = user.uid;
    }

    return userFormat;
}
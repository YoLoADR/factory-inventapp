const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const TypeUser = require('../../utilities/enums/type-user');
const CHECKIN = require('../../utilities/enums/type-module').CHECKIN //the module types file is imported.


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

exports = module.exports = functions.https.onCall(async (data, context) => {

    // Getting data passed from front
    let email = data.email;
    let password = data.password;
    let userUid = data.userId;
    let type = data.userType;

    // If user uid exist
    if (userUid !== null) {
        try {

            // Check if user already exist
            let userRecord = await checkUserExist(email);

            // If exist delete it because it's first access
            if (userRecord && userRecord.uid) {
                await admin.auth().deleteUser(userRecord.uid);
            }

            // Create firebase auth account with email and password
            let accCreated = await admin.auth().createUser({
                email: email,
                password: password
            })

            if (type == TypeUser.ATTENDEE) {
                // Create new attendee user
                let result = await createAttendeeFirstAccess(userUid, accCreated.uid);

                return ({
                    message: 'success',
                    result: result
                })
            } else if (type == TypeUser.SPEAKER) {
                // Create new speaker user
                let result = await createSpeakerFirstAccess(userUid, accCreated.uid);

                return ({
                    message: 'success',
                    result: result
                })
            }
        } catch (err) {
            // If firebase error code exist
            if (err && err.errorInfo && err.errorInfo['code']) {
                return ({
                    message: 'error',
                    code: err.errorInfo['code']
                })
            } else {
                return ({
                    message: 'error',
                    result: err
                })
            }
        }
    } else {
        // If user uid don't exist return error code 400 wrong data
        return ({
            message: 'error',
            code: 400
        });
    }
})

/**
 * Check if user with email exist
 * @param {*} email 
 */
const checkUserExist = async (email) => {
    try {
        let userRecord = await admin.auth().getUserByEmail(email);
        return (userRecord);
    } catch (error) {
        return (null);
    }
}

/**
 * Create attendee for his first access
 * @param {*} oldUid 
 * @param {*} newUid 
 */
const createAttendeeFirstAccess = async (oldUid, newUid) => {
    // Get reference firestore
    let db = admin.firestore();

    try {
        // Get data of old uid user document
        let userDoc = await db.collection("users").doc(oldUid).get();

        let userData = userDoc.data();

        // If user language don't exist make to default
        if (userData.language == '' || userData.language == null || userData.language == undefined) {
            userData.language = environment.platform.defaultLanguage;
        }

        // Create new user payload
        let newUserData = userModelData({
            name: userData.name,
            type: userData.type,
            email: userData.email,
            photoUrl: userData.photoUrl,
            createdAt: userData.createdAt,
            company: userData.company,
            title: userData.title,
            description: userData.description,
            emailRecovery: userData.emailRecovery,
            phone: null,
            uid: newUid,
            events: userData.events,
            attendeeModules: userData.attendeeModules,
            firstAccess: false,
            language: userData.language
        });

        // Get events linked to user
        let eventsId = userData.events;

        for (let idEvent of eventsId) {

            // Begin attendee part
            let attendee = null;
            let dataAttendee = await db.collection("events").doc(idEvent).collection("attendees").doc(oldUid).get();

            if (dataAttendee.exists) {
                attendee = dataAttendee.data();
                attendee.uid = newUid;
                attendee.firstAccess = false;

                // If attendee language don't exist make default
                if (attendee.language == '' || attendee.language == null || attendee.language == undefined) {
                    attendee.language = environment.platform.defaultLanguage;
                }

                // If attendee identifier don't exist create random one
                if (attendee.identifier == '' || attendee.identifier == null || attendee.identifier == undefined) {
                    const uuidv4 = require('uuid/v4');
                    attendee.identifier = uuidv4();
                }
            }

            // End attendee part



            // Begin checkin modules

            let moduleCheckinId = null;
            let listCheckins = [];

            // Get snapshot for checkin modules
            let snapshotModules = await db.collection("events").doc(idEvent).collection("modules").where('type', '==', CHECKIN).get();

            if (snapshotModules.size > 0) {
                for (let iM = 0; iM < snapshotModules.length; iM++) {
                    let module = snapshotModules[iM].data();
                    moduleCheckinId = module.uid;

                    // Get snapshot for each checkin for a module
                    let snapshotCheckin = await db.collection('modules').doc(moduleCheckinId).collection('checkins').get();
                    if (snapshotCheckin.size > 0) {

                        for (let iC = 0; iC < snapshotCheckin.length; iC++) {
                            let checkin = snapshotCheckin[iC].data();
                            let checkinId = checkin.uid;

                            // Get data for the attendee on the checkin of a module
                            let attendeeCheckin = await db.collection('modules')
                                .doc(moduleCheckinId)
                                .collection('checkins')
                                .doc(checkinId)
                                .collection('attendees')
                                .doc(oldUid)
                                .get();

                            if (attendeeCheckin.exists) {
                                let attendeeCheckinData = attendeeCheckin.data();
                                let checkinStatus = attendeeCheckinData.checkinStatus ? attendeeCheckinData.checkinStatus : false;
                                listCheckins.push({
                                    moduleCheckinId,
                                    checkinId,
                                    checkinStatus
                                });
                            }
                        }
                    }
                }
            }

            // End checkins modules



            // Begin custom field part

            let listCustomFields = [];

            let snapshotCustomFields = await db.collection('events')
                .doc(idEvent)
                .collection('attendees')
                .doc(oldUid)
                .collection('customFields')
                .get();

            if (snapshotCustomFields.size > 0) {
                for (let iS = 0; iS < snapshotCustomFields.length; iS++) {
                    let custom = snapshotCustomFields[iS].data();
                    listCustomFields.push(custom);
                }
            }

            // End of custom field part

            // Process update batch of documents for each event
            let batchResult = await processBatch(idEvent, oldUid, newUserData, attendee, listCheckins, listCustomFields);
        }

        return (true);
    } catch (error) {
        throw error;
    }
}

const processBatch = (eventId, oldUid, newUser, newAttendee, listCheckins, listCustomFields) => {
    let db = admin.firestore()

    // Create firestore batch reference
    let batch = db.batch();

    if (newAttendee) {
        const newUid = newAttendee.uid

        // Create new user data
        let newUserRef = db.collection("users").doc(newUid)
        batch.set(newUserRef, newUser)

        // Delete old user data
        let oldUserRef = db.collection("users").doc(oldUid)
        batch.delete(oldUserRef)

        // Create reference to old and new attendee doc on modules and event collection
        let oldAttendeeRef = db.collection("events").doc(eventId).collection("attendees").doc(oldUid)
        let newAttendeeRef = db.collection("events").doc(eventId).collection("attendees").doc(newUid)
        let oldModulesAttendeeRef = db.collection("modules").doc(newAttendee.moduleId).collection('attendees').doc(oldUid)
        let newModulesAttendeeRef = db.collection("modules").doc(newAttendee.moduleId).collection('attendees').doc(newUid)

        // Create new attendee data on modules and event collection
        batch.set(newAttendeeRef, newAttendee);
        batch.set(newModulesAttendeeRef, newAttendee);

        // Delete old attendee data on modules and event collection
        batch.delete(oldAttendeeRef);
        batch.delete(oldModulesAttendeeRef);

        // For each checkins found
        for (const checkin of listCheckins) {
            const checkinId = checkin.checkinId;
            const moduleCheckinId = checkin.moduleCheckinId;
            newAttendee.checkinStatus = checkin.checkinStatus;

            // Create new checkin data
            const refCheckin = db.collection('modules').doc(moduleCheckinId).collection('checkins').doc(checkinId).collection('attendees').doc(newUid)
            batch.set(refCheckin, newAttendee)
        }

        // For each custom field found
        for (let custom of listCustomFields) {
            let refCustomEventAttendee = newAttendeeRef.collection('customFields').doc(custom.uid);
            let refCustomModuleAttendee = newModulesAttendeeRef.collection('customFields').doc(custom.uid);

            // Create new custom field data on necessary collections
            batch.set(refCustomEventAttendee, custom);
            batch.set(refCustomModuleAttendee, custom);
        }
    }

    return (batch.commit());
}

/**
 * 
 * OLD FUNCTION
 * 
 */
// exports = module.exports = functions.https.onCall((data, context) => {
//     return new Promise(async (resolve, reject) => {
//         let email = data.email;
//         let password = data.password;
//         let userUid = data.userId;
//         let type = data.userType;

//         if (userUid !== null) {
//             admin.auth().createUser({
//                 email: email,
//                 password: password
//             }).then(async (accCreated) => {
//                 // post data with uids of user to update in Firestore Database
//                 if (type == TypeUser.ATTENDEE) {
//                     await createAttendeeFirstAccess(userUid, accCreated.uid)
//                         .then((_) => {
//                             resolve({
//                                 message: 'success',
//                                 result: _
//                             });
//                         })
//                         .catch((e) => {
//                             resolve({
//                                 message: 'error',
//                                 result: e
//                             });
//                         })
//                 } else if (type == TypeUser.SPEAKER) {
//                     await createSpeakerFirstAccess(userUid, accCreated.uid)
//                         .then((_) => {
//                             resolve({
//                                 message: 'success',
//                                 result: _
//                             });
//                         })
//                         .catch((e) => {
//                             resolve({
//                                 message: 'error',
//                                 result: e
//                             });
//                         });
//                 }
//             }).catch((err) => {
//                 console.error(err);
//                 resolve({
//                     message: 'error',
//                     code: err['code']
//                 });
//             });
//         } else {
//             resolve(true);
//         }
//     });
// });

/**
 * 
 * OLD FUNCTION
 * 
 */
// const createAttendeeFirstAccess = async (oldUid, newUid) => {
//     return new Promise(async (resolve, reject) => {
//         let db = admin.firestore();

//         // resgata user/oldUid
//         db.collection("users")
//             .doc(oldUid)
//             .get()
//             .then(async (user) => {
//                 let aux = user.data();

//                 // se o user não tiver o campo language, recebe o valor default.
//                 if (aux.language == '' || aux.language == null || aux.language == undefined)
//                     aux.language = environment.platform.defaultLanguage;

//                 // cria novo user.
//                 let data = {
//                     name: aux.name,
//                     type: aux.type,
//                     email: aux.email,
//                     photoUrl: aux.photoUrl,
//                     createdAt: aux.createdAt,
//                     company: aux.company,
//                     title: aux.title,
//                     description: aux.description,
//                     emailRecovery: aux.emailRecovery,
//                     phone: null,
//                     uid: newUid,
//                     events: aux.events,
//                     attendeeModules: aux.attendeeModules,
//                     firstAccess: false,
//                     language: aux.language
//                 }

//                 let userObj = userModelData(data);

//                 // pega todos os eventos do user.
//                 let eventsId = aux.events;
//                 let contEvent = 0 // conta o número de eventos processados.

//                 // percorre  a lista de eventos do user.
//                 let batch = db.batch();
//                 for (let idEvent of eventsId) {
//                     // carregamento do attendee
//                     let flagAttendee = false // Monitora o carregamento do attendee.
//                     let attendee = null

//                     // carrega o paricipante antigo
//                     db.collection("events")
//                         .doc(idEvent)
//                         .collection("attendees")
//                         .doc(oldUid)
//                         .get().then(async (data) => {
//                             if (data.exists) {
//                                 attendee = data.data()
//                                 attendee.uid = newUid;
//                                 attendee.firstAccess = false;

//                                 // se o attendee não tiver o campo language, recebe o valor default.
//                                 if (attendee.language == '' || attendee.language == null || attendee.language == undefined)
//                                     attendee.language = environment.platform.defaultLanguage;

//                                 // se o attendee não tiver  identifier, gera um automaticamente.
//                                 if (attendee.identifier == '' || attendee.identifier == null || attendee.identifier == undefined) {
//                                     const uuidv4 = require('uuid/v4');
//                                     attendee.identifier = uuidv4();
//                                 }
//                             }


//                             flagAttendee = true

//                             if (await processEvent(flagAttendee, flagCheckin, flagCustomField, batch, idEvent, contEvent, eventsId.length, oldUid, userObj, attendee, listCheckins, listCustomField)) {
//                                 resolve(true)
//                             } else {
//                                 resolve(false)
//                             }
//                         })


//                     //carregamento dos checkins
//                     let flagCheckin = false // Monitora o carregamento do checkin.
//                     let moduleCheckinId = null
//                     let listCheckins = []

//                     // pega o id do módulo checkin, seus checkins e o status do attendee antigo.
//                     db.collection('events')
//                         .doc(idEvent)
//                         .collection('modules')
//                         .where('type', '==', CHECKIN)
//                         .get()
//                         .then(async (snapshot) => {
//                             if (snapshot.size <= 0) {
//                                 flagCheckin = true

//                                 if (await processEvent(flagAttendee, flagCheckin, flagCustomField, batch, idEvent, contEvent, eventsId.length, oldUid, userObj, attendee, listCheckins, listCustomField)) {
//                                     resolve(true)
//                                 } else {
//                                     resolve(false)
//                                 }
//                             }

//                             // Todo evento tem no máximo um módullo checkin.
//                             snapshot.forEach((element) => {
//                                 const module = element.data()
//                                 moduleCheckinId = module.uid

//                                 db.collection('modules')
//                                     .doc(moduleCheckinId)
//                                     .collection('checkins')
//                                     .get()
//                                     .then(async (childSnapshot) => {
//                                         if (childSnapshot.size <= 0) {
//                                             flagCheckin = true

//                                             if (await processEvent(flagAttendee, flagCheckin, flagCustomField, batch, idEvent, contEvent, eventsId.length, oldUid, userObj, attendee, listCheckins, listCustomField)) {
//                                                 resolve(true)
//                                             } else {
//                                                 resolve(false)
//                                             }
//                                         }

//                                         let totalCheckins = childSnapshot.size
//                                         let contCheckin = 0
//                                         childSnapshot.forEach((element) => {
//                                             const checkin = element.data()
//                                             const checkinId = checkin.uid

//                                             // pega o statusCheckin do user antigo no checkin.
//                                             db.collection('modules')
//                                                 .doc(moduleCheckinId)
//                                                 .collection('checkins')
//                                                 .doc(checkinId)
//                                                 .collection('attendees')
//                                                 .doc(oldUid)
//                                                 .get()
//                                                 .then(async (grandChild) => {
//                                                     contCheckin++
//                                                     let aux = grandChild.data()
//                                                     let checkinStatus = aux.checkinStatus ? aux.checkinStatus : false
//                                                     listCheckins.push({
//                                                         moduleCheckinId,
//                                                         checkinId,
//                                                         checkinStatus
//                                                     })

//                                                     if (contCheckin >= totalCheckins) {
//                                                         flagCheckin = true

//                                                         if (await processEvent(flagAttendee, flagCheckin, flagCustomField, batch, idEvent, contEvent, eventsId.length, oldUid, userObj, attendee, listCheckins, listCustomField)) {
//                                                             resolve(true)
//                                                         } else {
//                                                             resolve(false)
//                                                         }
//                                                     }

//                                                 })
//                                         })

//                                     })
//                             })
//                         })


//                     //carregamento dos customFields
//                     let listCustomField = [];
//                     let flagCustomField = false
//                     db.collection('events')
//                         .doc(idEvent)
//                         .collection('attendees')
//                         .doc(oldUid)
//                         .collection('customFields')
//                         .get()
//                         .then(async (snapshot) => {
//                             snapshot.forEach(doc => {
//                                 let custom = doc.data();
//                                 listCustomField.push(custom);
//                             });

//                             flagCustomField = true

//                             if (await processEvent(flagAttendee, flagCheckin, flagCustomField, batch, idEvent, contEvent, eventsId.length, oldUid, userObj, attendee, listCheckins, listCustomField)) {
//                                 resolve(true)
//                             } else {
//                                 resolve(false)
//                             }
//                         })

//                 }
//             })
//     })
// }

/**
 * OLD FUNCTION
 */
// Executa os dados do user no evento.
// const processEvent = (flagAttendee, flagCheckin, flagCustomField, batch, eventId, contEvent, totalEvent, oldUid, newUser, newAttendee, listCheckin, listCustomField) => {
//     return new Promise((resolve) => {
//         // se executa a função se todos os dados foram carregados.
//         if (flagAttendee && flagCheckin && flagCustomField) {
//             let db = admin.firestore()

//             if (newAttendee) {
//                 const newUid = newAttendee.uid

//                 // adicionar no path users o novo
//                 let newUserRef = db.collection("users").doc(newUid)
//                 batch.set(newUserRef, newUser)

//                 // apgar no path users antigo
//                 let oldUserRef = db.collection("users").doc(oldUid)
//                 batch.delete(oldUserRef)

//                 // Cria referências para os paths do antigo attendee e novo attendee.
//                 let oldAttendeeRef = db.collection("events").doc(eventId).collection("attendees").doc(oldUid)
//                 let newAttendeeRef = db.collection("events").doc(eventId).collection("attendees").doc(newUid)
//                 let oldModulesAttendeeRef = db.collection("modules").doc(newAttendee.moduleId).collection('attendees').doc(oldUid)
//                 let newModulesAttendeeRef = db.collection("modules").doc(newAttendee.moduleId).collection('attendees').doc(newUid)

//                 // adicionar no path events -> attendees e  adicionar no path modules -> attendees novo
//                 batch.set(newAttendeeRef, newAttendee);
//                 batch.set(newModulesAttendeeRef, newAttendee);

//                 // apagar no path events -> attendes e apagar no path modules -> attendees antigo
//                 // ativa trigger de remover attendee
//                 batch.delete(oldAttendeeRef);
//                 batch.delete(oldModulesAttendeeRef);

//                 // adiciona o attendee nos checkins.
//                 for (const checkin of listCheckin) {
//                     const checkinId = checkin.checkinId
//                     const moduleCheckinId = checkin.moduleCheckinId
//                     newAttendee.checkinStatus = checkin.checkinStatus

//                     const refCheckin = db.collection('modules').doc(moduleCheckinId).collection('checkins').doc(checkinId).collection('attendees').doc(newUid)
//                     batch.set(refCheckin, newAttendee)
//                 }

//                 // adiciona os customfiields do attendee
//                 for (let custom of listCustomField) {
//                     let refCustomEventAttendee = newAttendeeRef.collection('customFields').doc(custom.uid);
//                     let refCustomModuleAttendee = newModulesAttendeeRef.collection('customFields').doc(custom.uid);

//                     batch.set(refCustomEventAttendee, custom);
//                     batch.set(refCustomModuleAttendee, custom);
//                 }
//             }

//             // computa mais 1 evento.
//             contEvent++

//             // se todos os eventos foram computados, faz o commit do batch.
//             if (contEvent >= totalEvent) {
//                 batch.commit()
//                     .then((_) => {
//                         resolve(true)
//                     })
//                     .catch((err) => {
//                         resolve(false)
//                     })
//             }
//         }
//     })
// }

createSpeakerFirstAccess = (oldUid, newUid) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        db
            .collection("users")
            .doc(oldUid)
            .get()
            .then((user) => {
                let aux = user.data();

                let body = {
                    uid: newUid,
                    type: aux.type
                }

                let eventsId = aux.events;
                if (aux.language == '' || aux.language == null || aux.language == undefined) {
                    aux.language = environment.platform.defaultLanguage;
                }
                let data = {
                    name: aux.name,
                    type: aux.type,
                    email: aux.email,
                    photoUrl: aux.photoUrl,
                    createdAt: aux.createdAt,
                    company: aux.company,
                    title: aux.title,
                    description: aux.description,
                    emailRecovery: aux.emailRecovery,
                    uid: newUid,
                    events: aux.events,
                    speakerModules: aux.speakerModules,
                    firstAccess: false,
                    language: aux.language,
                    edited_profile: false
                }

                let userObj = userModelDataSpeaker(data);

                for (let idEvent of eventsId) {
                    let batch = null;
                    batch = db.batch();

                    // adicionar no path users o novo
                    let newUserRef = db.collection("users").doc(newUid);
                    batch.set(newUserRef, userObj);

                    // apgar no path users antigo

                    let oldUserRef = db.collection("users").doc(oldUid);
                    batch.delete(oldUserRef);

                    let oldSpeakerRef = db.collection("events").doc(idEvent).collection("speakers").doc(oldUid);
                    let newSpeakerRef = db.collection("events").doc(idEvent).collection("speakers").doc(newUid);

                    let listCustomField = [];
                    oldSpeakerRef.collection('customFields').get().then((data) => {
                        data.forEach(doc => {
                            let custom = doc.data();
                            listCustomField.push(custom);
                        });
                        // console.log(listCustomField)
                    })

                    oldSpeakerRef.get().then((data) => {
                        let speaker = data.data();
                        if (speaker.language == '' || speaker.language == null || speaker.language == undefined) {
                            speaker.language = environment.platform.defaultLanguage;
                        }
                        if (speaker.language == '' || speaker.language == null || speaker.language == undefined) {
                            environment.platform.defaultLanguage;
                        }
                        if (speaker.identifier == '' || speaker.identifier == null || speaker.identifier == undefined) {
                            const uuidv4 = require('uuid/v4');
                            speaker.identifier = uuidv4();
                        }
                        speaker.uid = newUid;
                        speaker.firstAccess = false;
                        let oldModulesSpeakerRef = db.collection("modules").doc(speaker.moduleId).collection('speakers').doc(oldUid);
                        let newModulesSpeakerRef = db.collection("modules").doc(speaker.moduleId).collection('speakers').doc(newUid);


                        // adicionar no path events -> speakers
                        batch.set(newSpeakerRef, speaker);
                        // adicionar no path modules -> speakers novo
                        batch.set(newModulesSpeakerRef, speaker);

                        // apagar no path events -> speaker
                        batch.delete(oldSpeakerRef);
                        // apagar no path modules -> speakers antigo
                        batch.delete(oldModulesSpeakerRef);

                        batch.commit().then((batchOk) => {
                            let batchCustom = db.batch();
                            for (let custom of listCustomField) {
                                let refCustomEventSpeaker = newSpeakerRef.collection('customFields').doc(custom.uid);
                                let refCustomModuleSpeaker = newModulesSpeakerRef.collection('customFields').doc(custom.uid);

                                batchCustom.set(refCustomEventSpeaker, custom);
                                batchCustom.set(refCustomModuleSpeaker, custom);
                            }

                            batchCustom.commit().then(() => {})

                            resolve({
                                code: 201,
                                message: 'success',
                                result: batchOk
                            });
                        }).catch((batchError) => {
                            //remove da autenticação caso dê erro no banco
                            console.log(batchError)
                            // admin.auth().deleteUser(newUid);

                            reject({
                                code: 404,
                                message: 'error',
                                result: batchError
                            });
                        })
                    });
                }
            });
    })
}

/**
 * Build user model data
 * @param {*} user 
 */
const userModelData = (user) => {
    let userFormat = {
        name: null,
        type: null,
        email: "",
        language: "",
        description: "",
        photoUrl: "",
        company: "",
        title: "",
        emailRecovery: "",
        events: null,
        attendeeModules: null,
        firstAccess: null,
        uid: "",
        edited_profile: false
    };

    if (user.name != undefined) {
        userFormat.name = user.name;
    }

    if (user.type != undefined) {
        userFormat.type = user.type;
    }

    if (user.email != undefined) {
        userFormat.email = user.email;
    }

    if (user.language != undefined) {
        userFormat.language = user.language;
    }

    if (user.description != undefined) {
        userFormat.description = user.description;
    }

    if (user.photoUrl != undefined) {
        userFormat.photoUrl = user.photoUrl;
    }

    if (user.company != undefined) {
        userFormat.company = user.company;
    }

    if (user.title != undefined) {
        userFormat.title = user.title;
    }

    if (user.emailRecovery != undefined) {
        userFormat.emailRecovery = user.emailRecovery;
    }

    if (user.events != undefined) {
        userFormat.events = user.events;
    }

    userFormat.attendeeModules = user.attendeeModules

    if (user.firstAccess != undefined) {
        userFormat.firstAccess = user.firstAccess;
    }

    if (user.uid != undefined) {
        userFormat.uid = user.uid;
    }

    return userFormat;
}

/**
 * Build user model data speaker
 */
userModelDataSpeaker = (user) => {
    let userFormat = {
        name: null,
        type: null,
        email: "",
        language: "",
        description: "",
        photoUrl: "",
        company: "",
        title: "",
        emailRecovery: "",
        events: null,
        speakerModules: null,
        firstAccess: null,
        uid: "",
        edited_profile: false
    };

    if (user.name != undefined) {
        userFormat.name = user.name;
    }

    if (user.type != undefined) {
        userFormat.type = user.type;
    }

    if (user.email != undefined) {
        userFormat.email = user.email;
    }

    if (user.language != undefined) {
        userFormat.language = user.language;
    }

    if (user.description != undefined) {
        userFormat.description = user.description;
    }

    if (user.photoUrl != undefined) {
        userFormat.photoUrl = user.photoUrl;
    }

    if (user.company != undefined) {
        userFormat.company = user.company;
    }

    if (user.title != undefined) {
        userFormat.title = user.title;
    }

    if (user.emailRecovery != undefined) {
        userFormat.emailRecovery = user.emailRecovery;
    }

    if (user.events != undefined) {
        userFormat.events = user.events;
    }

    userFormat.speakerModules = user.speakerModules

    if (user.firstAccess != undefined) {
        userFormat.firstAccess = user.firstAccess;
    }

    if (user.uid != undefined) {
        userFormat.uid = user.uid;
    }

    return userFormat;
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// delete attendee/speaker and remake user with new type
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let oldUser = req.body.oldUser;
        let newUser = req.body.newUser;

        if (oldUser.type == 4) {
            removeSpeaker(oldUser)
                .then((status) => {
                    remakeUser(newUser)
                        .then(() => {
                            res.json({
                                message: 'success',
                            })
                        })
                })
        } else if (oldUser.type == 5) {
            removeAttendee(oldUser)
                .then((status) => {
                    remakeUser(newUser)
                        .then(() => {
                            res.json({
                                message: 'success',
                            })
                        })
                })
        } else {
            // caso seja outro tipo, apenas faz alterações no user
            updateUser(newUser)
                .then(() => {
                    res.json({
                        message: 'success'
                    })
                })
        }
    });
});

removeSpeaker = (user) => {
    return new Promise((resolve) => {
        let contEvents = 0;
        let contModules = 0;

        for (let event of user.events) {
            admin.firestore().collection('events').doc(event).collection('speakers').doc(user.uid).delete();
            contEvents++;
            if (contModules == user.speakerModules.length && contEvents == user.events.length) {
                admin.firestore().collection('users').doc(user.uid).delete();
                admin.auth().deleteUser(user.uid).then(() => {
                    resolve(true);
                })
                    .catch((e) => {
                        resolve(false);
                    })
            }
        }

        for (let module of user.speakerModules) {
            admin.firestore().collection('modules').doc(module).collection('speakers').doc(user.uid).delete();
            contModules++;
            if (contModules == user.speakerModules.length && contEvents == user.events.length) {
                admin.firestore().collection('users').doc(user.uid).delete();
                admin.auth().deleteUser(user.uid).then(() => {
                    resolve(true);
                })
                    .catch((e) => {
                        resolve(false);
                    })
            }
        }
    });
}

removeAttendee = (user) => {
    return new Promise((resolve) => {
        let contEvents = 0;
        let contModules = 0;

        for (let event of user.events) {
            admin.firestore().collection('events').doc(event).collection('attendees').doc(user.uid).delete();
            contEvents++;
            if (contModules == user.attendeeModules.length && contEvents == user.events.length) {
                admin.firestore().collection('users').doc(user.uid).delete();
                admin.auth().deleteUser(user.uid).then(() => {
                    resolve(true);
                })
                    .catch((e) => {
                        resolve(false);
                    })
            }
        }

        for (let module of user.attendeeModules) {
            admin.firestore().collection('modules').doc(module).collection('attendees').doc(user.uid).delete();
            contModules++;
            if (contModules == user.attendeeModules.length && contEvents == user.events.length) {
                admin.firestore().collection('users').doc(user.uid).delete();
                admin.auth().deleteUser(user.uid).then(() => {
                    resolve(true);
                })
                    .catch((e) => {
                        resolve(false);
                    })
            }
        }
    });
}

remakeUser = (user) => {
    return new Promise((resolve) => {
        admin.auth().createUser({
            email: user.email,
            password: user.password,
            displayName: user.name,
        })
            .then((authUser) => {
                admin.auth().setCustomUserClaims(authUser.uid, { type: user.type }).then(() => { });
                user.password = null;
                // user = Object.keys({}, user);
                admin.firestore().collection('users').doc(authUser.uid)
                    .set(user)
                    .then(() => {
                        resolve(true);
                    })
                    .catch((e) => {
                        console.log(e);
                        resolve(false);
                    })
            })
            .catch((e) => {
                console.log('**** error: ', e);
            })
    });
}

updateUser = (user, oldUid) => {
    return new Promise((resolve) => {
        admin.auth().updateUser(oldUid, { email: user.email, password: user.password })
            .then(() => {
                admin.auth().setCustomUserClaims(oldUid, { type: user.type }).then(() => { });
                user.password = null;
                user.uid = oldUid;
                user = Object.keys({}, user);
                admin.firestore().collection('users').doc(oldUid).update(user)
                    .then(() => {
                        resolve(true);
                    });
            })
            .catch(() => {
                admin.auth().createUser({
                    email: user.email,
                    password: user.password,
                    displayName: user.name,
                })
                    .then((authUser) => {
                        admin.auth().setCustomUserClaims(authUser.uid, { type: user.type }).then(() => { });
                        user.password = null;
                        user.uid = authUser.uid;
                        user = Object.keys({}, user);
                        admin.firestore().collection('users').doc(authUser.uid).update(user)
                            .then(() => {
                                resolve(true);
                            });
                    });
            })
    });
}
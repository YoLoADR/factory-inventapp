const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let surveyId = req.query.surveyId;
        let questionId = req.query.questionId;
        let listAnswers = [];
        let language = req.query.language;

        let refQuestion = db
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(questionId);

        let refQuestionResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(questionId)
            .collection('result');

        refQuestion.get()
            .then((dataQuestion) => {
                let question = dataQuestion.data();

                refQuestionResult.get()
                    .then(async(data) => {
                        if (data.size >= 1) {
                            let listResult = [];
                            data.forEach(element => {
                                listResult.push(element.data());
                            });

                            let listResultQuestion = [];
                            let contResult = 0;
                            for (let result of listResult) {
                                if (question.type == 'oneSelect' || question.type == 'multipleSelect') {

                                    let user = await getUser(result.user);

                                    let contAnswer = 0;
                                    for (let answerId of result.answer) {

                                        let index = searchAnswer(answerId, listAnswers);
                                        let answer = null;

                                        if (index < 0) {
                                            answer = await getAnswer(moduleId, surveyId, question.uid, answerId);
                                            listAnswers.push(answer);
                                        } else {
                                            answer = listAnswers[index];
                                        }

                                        let obj = {};

                                        if (user !== null) {
                                            obj.name = user.name;
                                            obj.email = user.email;
                                        } else {
                                            obj.name = "";
                                            obj.email = "";
                                        }

                                        // obj.question = question.title;
                                        // obj.answer = answer.answer;
                                        obj.question = question.title[language]
                                        obj.answer = answer.answer[language]

                                        if (answer.marker !== null && answer.marker != undefined) {
                                            obj.marker = answer.marker;
                                        } else {
                                            obj.marker = '';
                                        }

                                        obj.timestamp = result.timestamp;

                                        listResultQuestion.push(obj);

                                        if (contAnswer == result.answer.length - 1) {
                                            if (contResult == listResult.length - 1) {
                                                res.status(200).json({
                                                    code: 200,
                                                    message: 'OK',
                                                    result: listResultQuestion
                                                })
                                            }
                                            contResult++;
                                        }

                                        contAnswer++;

                                    }
                                } else {
                                    let user = await getUser(result.user);

                                    let obj = {};
                                    if (user !== null) {
                                        obj.name = user.name;
                                        obj.email = user.email;
                                    } else {
                                        obj.name = "";
                                        obj.email = "";
                                    }

                                    // obj.question = question.title;
                                    obj.question = question.title[language]

                                    if (question.type == 'date') {

                                        let date = new Date(result.answer * 1000);
                                        let day = date.getDate();
                                        let month = date.getMonth() + 1;
                                        let year = date.getFullYear();

                                        obj.answer = day + '/' + month + '/' + year;

                                    } else {
                                        obj.answer = result.answer;
                                    }

                                    obj.marker = '';
                                    obj.timestamp = result.timestamp;

                                    listResultQuestion.push(obj);

                                    if (contResult == listResult.length - 1) {
                                        res.status(200).json({
                                            code: 200,
                                            message: 'OK',
                                            result: listResultQuestion
                                        })
                                    }
                                    contResult++;

                                }
                            }
                        } else {
                            let listResultQuestion = [];
                            res.status(200).json({
                                code: 200,
                                message: 'OK',
                                result: listResultQuestion
                            })

                            cont++;
                        }
                    })
            })
    });
});


function getAnswer(moduleId, surveyId, questionId, answerId) {
    return new Promise((resolve) => {
        let db = admin.firestore();

        let ref = db
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(questionId)
            .collection('answers')
            .doc(answerId);

        ref.get()
            .then((data) => {
                if (!data.exists) {
                    resolve(null);
                }

                resolve(data.data())
            })
    })
}

function getUser(userId) {
    let db = admin.firestore();

    return new Promise((resolve) => {
        let refUsers = db.collection('users').doc(userId);
        refUsers.get()
            .then((data) => {
                if (!data.exists) {
                    resolve(null);
                }

                resolve(data.data())
            })
    })
}

function searchAnswer(itemId, list) {
    return list.map(function(e) { return e.uid; }).indexOf(itemId);
}
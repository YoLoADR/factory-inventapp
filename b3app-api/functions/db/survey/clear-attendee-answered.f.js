const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.firestore
    .document('modules/{moduleId}/surveys/{surveyId}/questions/{questionId}/result/{userId}').onDelete((snap, context) => {
        return new Promise((resolve) => {
            let surveyId = context.params.surveyId;
            let userId = context.params.userId;
            let moduleId = context.params.moduleId;

            let refModule = admin.firestore().collection('modules').doc(moduleId);
            refModule
                .get()
                .then((snapshot) => {
                    let module = snapshot.data();
                    let ref = admin.firestore().collection('events').doc(module.eventId).collection('attendees').doc(userId);

                    ref.update({ answeredSurveys: admin.firestore.FieldValue.arrayRemove(surveyId) });
                    resolve(true);
                });
        });
    });
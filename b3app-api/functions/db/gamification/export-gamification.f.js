const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.https.onCall((data, context) => {
    return new Promise(async(resolve) => {
        let eventId = data.body.eventId;
        let moduleId = data.body.moduleId;

        await getData(eventId, moduleId)
            .then((qrcodes) => {
                resolve(qrcodes);
            });
    })
});

getData = (eventId, moduleId) => {
    return new Promise((resolve, reject) => {
        let ref = admin.firestore().collection('modules').doc(moduleId).collection('gamification-qrcodes');
        let qrcodes = [];
        ref
            .get()
            .then((snapshot) => {
                let cont = 0;
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let qrcode = element.data();
                        getAttendees(eventId, qrcode.uid)
                            .then((data) => {
                                qrcode.attendees = data;
                                qrcodes.push(qrcode);
                                cont++;
                                if (cont == snapshot.size) {
                                    resolve(qrcodes)
                                }
                            });
                    });
                } else {
                    resolve(qrcodes);
                }
            })
    })
}

getAttendees = (eventId, qrId) => {
    return new Promise((resolve) => {
        let refAttendees = admin.firestore().collection('events').doc(eventId).collection('attendees');
        let attendees = [];
        let cont = 0;
        refAttendees
            .where(`gaming.${qrId}.uid`, '==', qrId)
            .get()
            .then((snapshot) => {
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let attendee = element.data();
                        attendees.push(attendee);
                        cont++;
                        if (cont == snapshot.size) {
                            resolve(attendees);
                        }
                    });
                } else {
                    resolve(attendees);
                }
            })
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const cors = require("cors")({ origin: true });

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        const db = admin.firestore()
        let moduleId = req.query.moduleId;
        let cloudId = req.query.cloudId;

        deleteWords(moduleId, cloudId)
            .then((data) => {
                if (data === true) {
                    res.status(200).json({
                        code: 200,
                        message: 'OK',
                        result: true
                    })
                }
            })
            .catch((err) => {
                console.log('CATCH 001')
                res.status(500).json({
                    code: 500,
                    message: 'Error',
                    result: err
                })
            })
    })
})

function deleteWords(moduleId, cloudId) {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        console.log(moduleId, cloudId)
        let ref = db
            .collection('modules')
            .doc(moduleId)
            .collection('wordClouds')
            .doc(cloudId)
            .collection('words')

        ref.get()
            .then((data) => {

                if (data.size > 0) {
                    let listWords = [];
                    data.forEach(element => {
                        let doc = element.data();
                        doc.uid = element.id;
                        listWords.push(doc);
                    });

                    let cont = 0;
                    for (let word of listWords) {
                        console.log(word)
                        ref.doc(word.uid).delete().then(() => {
                            if (cont == listWords.length - 1) {
                                resolve(true);
                            }

                            cont++;
                        })
                    }
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                console.log('CATCH 002')
                reject(err);
            })
    })
}
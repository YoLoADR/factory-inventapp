const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const cors = require("cors")({ origin: true });

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        const db = admin.firestore()
        let moduleId = req.query.moduleId;
        let cloudId = req.query.cloudId;

        let ref = db
            .collection('modules')
            .doc(moduleId)
            .collection('wordClouds')
            .doc(cloudId)

        deleteWords(moduleId, cloudId)
            .then((data) => {
                console.log(data)
                if (data === true) {
                    ref.delete()
                        .then(() => {
                            res.status(200).json({
                                code: 200,
                                message: 'OK',
                                result: true
                            })
                        })
                        .catch((error) => {
                            res.status(500).json({
                                code: 500,
                                message: 'Error',
                                result: error
                            })
                        })
                }
            })
            .catch((err) => {
                res.status(500).json({
                    code: 500,
                    message: 'Error',
                    result: err
                })
            })
    })
})

function deleteWords(moduleId, cloudId) {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        console.log(moduleId, cloudId)
        let ref = db
            .collection('modules')
            .doc(moduleId)
            .collection('wordClouds')
            .doc(cloudId)
            .collection('words')

        ref.get()
            .then((data) => {
                let listWords = [];

                if (data.size > 0) {
                    data.forEach(element => {
                        let doc = element.data();
                        doc.uid = element.id;
                        listWords.push(doc);
                    });

                    console.log(listWords)
                    let cont = 0;
                    for (let word of listWords) {
                        ref.doc(word.uid).delete().then(() => {
                            if (cont == listWords.length - 1) {
                                resolve(true);
                            }

                            cont++;
                        })
                    }
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                reject(err);
            })
    })
}
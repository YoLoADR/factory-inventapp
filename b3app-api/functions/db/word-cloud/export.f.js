const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const cors = require("cors")({ origin: true });

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        const db = admin.firestore()
        let moduleId = req.query.moduleId;
        let cloudId = req.query.cloudId;
        let listResults = [];

        let ref = admin.firestore()
            .collection('modules')
            .doc(moduleId)
            .collection('wordClouds')
            .doc(cloudId)
            .collection('words');

        ref.get().then((data) => {

            if (data.size > 0) {
                let listWords = [];

                data.forEach(element => {
                    listWords.push(element.data());
                });

                let cont = 0;
                for (let aux of listWords) {
                    generateResult(aux).then((result) => {
                        listResults.push(result);

                        if (cont == listWords.length - 1) {
                            res.status(200).json({
                                code: 200,
                                message: 'OK',
                                result: listResults
                            })
                        }

                        cont++;
                    })
                }
            } else {
                res.status(200).json({
                    code: 200,
                    message: 'OK',
                    result: listResults
                })
            }
        })
    })
})

function generateResult(data) {
    return new Promise((resolve, reject) => {
        let result = [];
        getUser(data.userId, (user) => {
            result[0] = user.name;
            result[1] = user.email;
            result[2] = data.answer;

            resolve(result);
        })

    })
}

function getUser(userId, onResolve) {
    let db = admin.firestore();

    let refUsers = db.collection('users').doc(userId);
    refUsers.get()
        .then((data) => {
            if (!data.exists) {
                onResolve(null);
            }

            onResolve(data.data())
        })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.firestore
    .document('modules/{moduleId}/customFields/{customId}').onCreate((snap, context) => {
        return new Promise((resolve, reject) => {
            let moduleId = context.params.moduleId;
            let customField = snap.data();

            admin.firestore().collection('modules')
                .doc(moduleId)
                .collection('attendees')
                .get().then((data) => {
                    if (data.size >= 1) {

                        let listAttendee = [];
                        //pega todos os documentos
                        data.forEach(
                            (doc) => {
                                listAttendee.push(doc.data());
                            }
                        )

                        let listErrorAttendee = [];
                        let cont = 0;
                        for (let i = 0; i < listAttendee.length; i++) {
                            let attendee = listAttendee[i];
                            let batch = admin.firestore().batch();

                            let refAttendeeEvent = admin.firestore().collection("events")
                                .doc(attendee.eventId)
                                .collection("attendees")
                                .doc(attendee.uid)
                                .collection('customFields')
                                .doc(customField.uid)

                            let refAttendeeModule = admin.firestore().collection('modules')
                                .doc(moduleId)
                                .collection('attendees')
                                .doc(attendee.uid)
                                .collection('customFields')
                                .doc(customField.uid)

                            batch.set(refAttendeeEvent, customField);
                            batch.set(refAttendeeModule, customField);

                            batch.commit()
                                .then((data) => {
                                    if (cont == listAttendee.length - 1) {
                                        resolve({
                                            code: 200,
                                            message: 'success',
                                            result: customField
                                        })
                                    }

                                    cont++;
                                })
                                .catch((error) => {
                                    listErrorAttendee.push(attendee);

                                    if (cont == listAttendee.length - 1) {
                                        reject({
                                            code: 500,
                                            message: 'error',
                                            result: listErrorAttendee
                                        })
                                    }

                                    cont++;
                                })
                        }
                    } else {
                        resolve({
                            code: 200,
                            message: 'success',
                            result: customField
                        })
                    }
                })
        })

    });
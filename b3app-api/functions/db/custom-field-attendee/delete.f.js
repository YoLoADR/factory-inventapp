const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.body.eventId;
        let moduleId = req.body.moduleId;
        let customId = req.body.customId;

        let refModule = admin.firestore().collection('modules').doc(moduleId);
        let refCustom = admin.firestore().collection('modules').doc(moduleId).collection('customFields').doc(customId);

        refCustom.get()
            .then((doc) => {
                let custom = doc.data();

                if (custom.type == 'select') {
                    let refCustomOptions = admin.firestore()
                        .collection('modules')
                        .doc(moduleId)
                        .collection('customFields')
                        .doc(customId).collection('options');

                    refCustomOptions.get()
                        .then((data) => {
                            data.forEach(
                                doc => {
                                    let option = doc.data();
                                    refCustomOptions.doc(option.uid).delete();
                                }
                            );
                        })

                    refModule.get().then((data) => {
                        let aux = data.data();
                        aux = aux['fieldsCustom'];
                        let orderItemDelete = aux[customId].order;

                        let updateFields = {};
                        for (let itemId in aux) {
                            if (aux[itemId].order > orderItemDelete) {
                                updateFields[`fieldsCustom.${itemId}.order`] = aux[itemId].order - 1;
                            }
                        }

                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                        updateFields[`fieldsCustom.${customId}`] = FieldValue.delete();

                        refModule.update(updateFields);
                        refCustom.delete();
                    })
                } else {
                    refModule.get().then((data) => {
                        let aux = data.data();
                        aux = aux['fieldsCustom'];
                        let orderItemDelete = aux[customId].order;

                        let updateFields = {};
                        for (let itemId in aux) {
                            if (aux[itemId].order > orderItemDelete) {
                                updateFields[`fieldsCustom.${itemId}.order`] = aux[itemId].order - 1;
                            }
                        }

                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                        updateFields[`fieldsCustom.${customId}`] = FieldValue.delete();

                        refModule.update(updateFields);
                        refCustom.delete();
                    })
                }
            })

        admin.firestore().collection('modules')
            .doc(moduleId)
            .collection('attendees')
            .get().then((data) => {
                if (data.size >= 1) {

                    //passa por todos os documentos
                    let listAttendees = [];
                    data.forEach(
                        (doc) => {
                            let attendee = doc.data();
                            listAttendees.push(attendee);
                        }
                    )

                    let listAttendeesError = [];
                    let totalAttendees = listAttendees.length;
                    let cont = 0;
                    for (let i = 0; i < totalAttendees; i++) {
                        let batch = admin.firestore().batch();
                        let attendee = listAttendees[i];

                        let refAttendeeEvent = admin.firestore().collection('events').doc(eventId).collection("attendees").doc(attendee.uid)
                            .collection('customFields').doc(customId);

                        let refAttendeeModule = admin.firestore().collection('modules').doc(moduleId).collection('attendees').doc(attendee.uid)
                            .collection('customFields').doc(customId);

                        batch.delete(refAttendeeEvent);
                        batch.delete(refAttendeeModule);

                        batch.commit()
                            .then(() => {
                                if (cont == totalAttendees - 1) {
                                    if (listAttendeesError.length == 0) {
                                        res.status(200).json({
                                            code: 200,
                                            message: 'success',
                                            result: 'removed'
                                        });
                                    } else {
                                        res.status(500).json({
                                            code: 500,
                                            message: 'error',
                                            result: listAttendeesError
                                        });
                                    }
                                }

                                cont++;
                            })
                            .catch(() => {
                                listAttendeesError.push(attendee)

                                if (cont == totalAttendees - 1) {
                                    res.status(500).json({
                                        code: 500,
                                        message: 'error',
                                        result: listAttendeesError
                                    });
                                }

                                cont++;
                            })
                    }
                } else {
                    res.status(200).json({
                        code: 200,
                        message: 'success',
                        result: 'removed'
                    });
                }
            });
    });
});
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.body.eventId;
        let moduleId = req.body.moduleId;
        let customField = req.body.customField;
        let oldCustom = req.body.oldCustomField;
        let listOptionsCustomEdit = req.body.listOptionsCustomEdit;
        let listOptionsCustomRemove = req.body.listOptionsCustomRemove;

        if (customField.type == 'select' && oldCustom.type == 'select') {
            for (let i = 0; i < listOptionsCustomEdit.length; i++) {
                //caso o opção tenha uid é um update em uma opção já existente, caso não tenha uid quer dizer que é uma nova opção e é um create
                if (listOptionsCustomEdit[i].uid != undefined) {
                    updateOptionCustomField(moduleId, oldCustom.uid, listOptionsCustomEdit[i]);
                } else {
                    createOptionCustomField(moduleId, oldCustom.uid, listOptionsCustomEdit[i]);
                }
            }

            for (let optionId of listOptionsCustomRemove) {
                removeOptionOfAttendees(eventId, moduleId, oldCustom.uid, optionId);
                removeOptionCustomField(moduleId, oldCustom.uid, optionId);
            }
        }
        // else if(customField.type == 'text' && oldCustom.type == 'text') {
            
        // }
        else if (customField.type == 'select' && oldCustom.type == 'text') {
            clearUsersResultCustom(eventId, moduleId, oldCustom.uid);

            for (let option of listOptionsCustomEdit) {
                createOptionCustomField(moduleId, oldCustom.uid, option);
            }
        } else if (customField.type == 'text' && oldCustom.type == 'select') {
            clearUsersResultCustom(eventId, moduleId, oldCustom.uid);

            for (let option of listOptionsCustomEdit) {
                let optionId = option.uid;
                removeOptionCustomField(moduleId, oldCustom.uid, optionId)
            }
        }


        //remove a propriedade value do objeto customField
        delete customField.value;

        admin.firestore()
            .collection('modules')
            .doc(moduleId)
            .collection('customFields')
            .doc(customField.uid)
            .update(customField)
            .then((data) => {
                admin.firestore().
                collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .get().then((data) => {

                        if (data.size >= 1) {
                            let listAttendees = [];
                            data.forEach(
                                doc => {
                                    let attendee = doc.data();
                                    listAttendees.push(attendee)
                                }
                            );

                            let listAttendeesError = [];
                            let sizeAttendees = listAttendees.length;
                            for (let i = 0; i < sizeAttendees; i++) {
                                let attendee = listAttendees[i];

                                let refAttendeesEvent = admin.firestore().collection("events")
                                    .doc(eventId)
                                    .collection("attendees")
                                    .doc(attendee.uid)
                                    .collection('customFields')
                                    .doc(customField.uid);

                                let refAttendeesModule = admin.firestore().collection('modules')
                                    .doc(moduleId)
                                    .collection('attendees')
                                    .doc(attendee.uid)
                                    .collection('customFields')
                                    .doc(customField.uid);

                                refAttendeesModule.get().then((data) => {
                                    customTextValue = data.data().textValue;
                                    customValue = data.data().value;

                                    customField.textValue = customTextValue;
                                    customField.value = customValue;
                                    
                                    let batch = admin.firestore().batch();
    
                                    batch.update(refAttendeesEvent, customField);
                                    batch.update(refAttendeesModule, customField);
    
                                    batch.commit()
                                    .then((data) => {
                                        if (i == sizeAttendees - 1) {
                                            res.status(200).json({
                                                code: 200,
                                                message: 'success',
                                                result: data
                                            })
                                        }
                                    })
                                    .catch((error) => {
                                        listAttendeesError.push(attendee);
    
                                        if (i == sizeAttendees - 1) {
                                            res.status(500).json({
                                                code: 500,
                                                message: 'error',
                                                result: listAttendeesError
                                            })
                                        }
                                    })
                                })
                                .catch((err) => {
                                    listAttendeesError.push(attendee);
    
                                    if (i == sizeAttendees - 1) {
                                        res.status(500).json({
                                            code: 500,
                                            message: 'error',
                                            result: listAttendeesError
                                        })
                                    }
                                })
                            }
                        } else {
                            res.status(200).json({
                                code: 200,
                                message: 'success',
                                result: data
                            })
                        }
                    })
            })
            .catch((error) => {
                res.status(500).json({
                    code: 500,
                    message: 'error',
                    result: error
                })
            })


    });
});


function createOptionCustomField(moduleId, customId, option) {
    // this.dbAttendee.createOptionCustomField(this.moduleId, customId, option);
    let ref = admin.firestore().collection('modules').doc(moduleId).collection('customFields').doc(customId).collection("options").doc();
    option.uid = ref.id;

    ref.set(option);
}

function removeOptionOfAttendees(eventId, moduleId, customId, optionId) {
    // this.dbAttendee.removeOptionOfAttendees(this.eventId, this.moduleId, customId, optionId);
    admin.firestore().collection('modules')
        .doc(moduleId)
        .collection('attendees')
        .get().then((data) => {
            //passa por todos os documentos
            data.forEach(
                (doc) => {
                    let attendee = doc.data();

                    let refAttendeeEvent = admin.firestore().collection('events').doc(eventId).collection("attendees").doc(attendee.uid)
                        .collection('customFields').doc(customId);

                    let refAttendeeModule = admin.firestore().collection('modules').doc(moduleId).collection('attendees').doc(attendee.uid)
                        .collection('customFields').doc(customId);


                    refAttendeeModule.get().
                    then((data) => {
                        let custom = data.data();

                        if (custom.value == optionId) {
                            let batch = admin.firestore().batch();

                            batch.update(refAttendeeEvent, { value: "" });
                            batch.update(refAttendeeModule, { value: "" });

                            batch.commit()
                                .then(() => {})
                                .catch(() => {})
                        }
                    })
                }
            )
        })
}

function removeOptionCustomField(moduleId, customId, optionId) {
    let ref = admin.firestore().collection('modules').doc(moduleId).collection('customFields').doc(customId).collection("options").doc(optionId);

    ref.delete();
}

function clearUsersResultCustom(eventId, moduleId, customId) {

    admin.firestore().collection('modules')
        .doc(moduleId)
        .collection('attendees')
        .get().then((data) => {
            //passa por todos os documentos
            data.forEach(
                (doc) => {
                    let attendee = doc.data();

                    let refAttendeeEvent = admin.firestore().collection('events').doc(eventId).collection("attendees").doc(attendee.uid)
                        .collection('customFields').doc(customId);

                    let refAttendeeModule = admin.firestore().collection('modules').doc(moduleId).collection('attendees').doc(attendee.uid)
                        .collection('customFields').doc(customId);

                    let batch = admin.firestore().batch();

                    batch.update(refAttendeeEvent, { value: '' });
                    batch.update(refAttendeeModule, { value: '' });

                    batch.commit()
                        .then(() => {})
                        .catch(() => {})

                }
            )
        })
}

function updateOptionCustomField(moduleId, customId, option) {
    let ref = admin.firestore().collection('modules').doc(moduleId).collection('customFields').doc(customId).collection("options").doc(option.uid);

    ref.update(option);
}
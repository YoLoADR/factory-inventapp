const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

exports = module.exports = functions.firestore.document('events/{eventId}').onUpdate((change, context) => {
    return new Promise((resolve) => {
        const event = change.after.data();
        let eventRef = admin.firestore().collection('events').doc(event.uid).collection('modules');
        let moduleRef = admin.firestore().collection('modules');

        Promise.all([
                checkAttendees(event, eventRef, moduleRef),
                checkSpeakers(event, eventRef, moduleRef),
                checkSchedules(event, eventRef, moduleRef),
                checkPersonalSchedule(event, eventRef, moduleRef),
                checkWidgets(event, eventRef, moduleRef),
                checkDocument(event, eventRef, moduleRef),
                checkGallery(event, eventRef, moduleRef),
                checkNewsFeed(event, eventRef, moduleRef),
                checkInfobooth(event, eventRef, moduleRef),
                checkCustomPage(event, eventRef, moduleRef),
                checkCheckin(event, eventRef, moduleRef),
                checkSelfCheckin(event, eventRef, moduleRef),
                checkGamification(event, eventRef, moduleRef),
                checkRanking(event, eventRef, moduleRef),
                checkMaps(event, eventRef, moduleRef),
                checkSurvey(event, eventRef, moduleRef),
                checkQuiz(event, eventRef, moduleRef),
                checkAskQuestion(event, eventRef, moduleRef),
                checkSessionFeedback(event, eventRef, moduleRef),
                checkWordCloud(event, eventRef, moduleRef),
            ])
            .then((_) => {
                console.info('*** Event module deleted successfully!!! ****');
                resolve(true);
            })
            .catch((e) => {
                console.error('**** An error ocurred on deleting modules ****');
                console.error(e);
                resolve(false);
            });
    });
});

checkAttendees = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;

        if (!event.availableModules.module_attendees) {
            eventRef.where('type', '==', typeModules.ATTENDEE)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkSpeakers = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_speaker) {
            eventRef.where('type', '==', typeModules.SPEAKER)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkSchedules = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_schedule) {
            eventRef.where('type', '==', typeModules.SCHEDULE)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkPersonalSchedule = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_schedule) {
            eventRef.where('type', '==', typeModules.PERSONALSCHEDULE)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkWidgets = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_widget) {
            eventRef.where('type', '==', typeModules.WIDGETS)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkDocument = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_document) {
            eventRef.where('type', '==', typeModules.DOCUMENT)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkGallery = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_gallery) {
            eventRef.where('type', '==', typeModules.GALLERY)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkNewsFeed = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_feed_news) {
            eventRef.where('type', '==', typeModules.NEWS_FEED)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkInfobooth = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_infobooth) {
            eventRef.where('type', '==', typeModules.INFOBOOTH)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkCustomPage = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_custom_page) {
            eventRef.where('type', '==', typeModules.CUSTOM_PAGE)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkCheckin = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_checkin) {
            eventRef.where('type', '==', typeModules.CHECKIN)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                });
        } else {
            resolve(true);
        }
    });
}

checkSelfCheckin = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_checkin) {
            eventRef.where('type', '==', typeModules.SELF_CHECKIN)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkGamification = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_gamification) {
            eventRef.where('type', '==', typeModules.GAMING)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkRanking = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_ranking) {
            eventRef.where('type', '==', typeModules.RANKING)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkMaps = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_maps) {
            eventRef.where('type', '==', typeModules.MAPS)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkSurvey = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_survey) {
            eventRef.where('type', '==', typeModules.SURVEY_LIST)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkQuiz = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_quiz) {
            eventRef.where('type', '==', typeModules.QUIZLIST)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkAskQuestion = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_ask_question) {
            eventRef.where('type', '==', typeModules.ASK_QUESTION)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkSessionFeedback = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;
        if (!event.availableModules.module_session_feedback) {
            eventRef.where('type', '==', typeModules.SESSION_FEEDBACK)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}

checkWordCloud = (event, eventRef, moduleRef) => {
    return new Promise((resolve) => {
        let cont = 0;

        if (!event.availableModules.module_word_cloud) {
            eventRef.where('type', '==', typeModules.WORDCLOUD)
                .get()
                .then((snapshot) => {
                    let size = snapshot.size;
                    if (size >= 1) {
                        snapshot.forEach(element => {
                            let module = element.data();
                            eventRef.doc(module.uid).delete();
                            moduleRef.doc(module.uid).delete();
                            if (cont == size) {
                                resolve(true);
                            }
                            cont++;
                        });
                    } else {
                        resolve(true);
                    }
                })
        } else {
            resolve(true);
        }
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });


exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin.firestore().collection('events')
            .get()
            .then((events) => {
                const list = [];

                events.forEach(
                    (doc) => {
                        let event = doc.data();
                        list.push(event);
                    }
                )

                list.sort(function(a, b) {
                    return a.startDate < b.startDate ? -1 : a.startDate > b.startDate ? 1 : 0;
                });

                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'success',
                    result: list
                });

            })
            .catch((error) => {
                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'error',
                    result: error
                });
            })


    })
})
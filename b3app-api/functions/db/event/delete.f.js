/**
 * Trigger responsible for monitoring and removing events and their modules.
 */

// imports
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.
const funcDeleteCollection = require('../../utilities/delete-collection'); //function that deletes collections in a generic way.
const funcCheckCollectionExist = require('../../utilities/check-collection') //function that checks only the collection has data.
const firebase_tools = require('firebase-tools');


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }


// monitors the deletion of events.
exports = module.exports = functions.firestore.document('events/{eventId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        const event = snap.data()
        const eventId = event.uid

        let db = admin.firestore();

        /*remove data within analytics paths*/
        const ref = db.collection('analytics').doc(eventId).collection('total-user-access')

        // check if the path analytics/{eventId}/total-user-access exists.
        if (await funcCheckCollectionExist(ref)) {
            funcDeleteCollection(ref.path)
        }

        /* delete data within the event path. */
        const ref1 = db.collection('events').doc(eventId).collection('sessions')
        const ref2 = db.collection('events').doc(eventId).collection('locations')
        const ref3 = db.collection('events').doc(eventId).collection('modules')
        const ref4 = db.collection('events').doc(eventId).collection('notifications')
        const ref5 = db.collection('events').doc(eventId).collection('attendees')
        const ref6 = db.collection('events').doc(eventId).collection('speakers')
        const ref7 = db.collection('events').doc(eventId).collection('chats')
        const ref8 = db.collection('events').doc(eventId).collection('documents')
        const ref9 = db.collection('events').doc(eventId).collection('feed-posts')
        const ref10 = db.collection('events').doc(eventId).collection('gallery-images')

        // check if the path events/{eventId}/sessions exists.
        if (await funcCheckCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        // check if the path events/{eventId}/locations exists.
        if (await funcCheckCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }

        // check if the path events/{eventId}/modules exists.
        if (await funcCheckCollectionExist(ref3)) {
            funcDeleteCollection(ref3.path)
        }


        // check if the path events/{eventId}/notifications exists.
        if (await funcCheckCollectionExist(ref4)) {
            funcDeleteCollection(ref4.path)
        }


        // check if the path events/{eventId}/attendees exists.
        if (await funcCheckCollectionExist(ref5)) {
            funcDeleteCollection(ref5.path)
        }

        // check if the path events/{eventId}/speakers exists.
        if (await funcCheckCollectionExist(ref6)) {
            funcDeleteCollection(ref6.path)
        }

        // check if the path events/{eventId}/chats exists.
        if (await funcCheckCollectionExist(ref7)) {
            funcDeleteCollection(ref7.path)
        }

        // check if the path events/{eventId}/documents exists.
        if (await funcCheckCollectionExist(ref8)) {
            funcDeleteCollection(ref8.path)
        }

        // check if the path events/{eventId}/feed-posts.
        if (await funcCheckCollectionExist(ref9)) {
            funcDeleteCollection(ref9.path)
        }

        // check if the path events/{eventId}/gallery-images.
        if (await funcCheckCollectionExist(ref10)) {
            funcDeleteCollection(ref10.path)
        }


        /*remove data within module paths*/

        // get the event modules.
        const modules = await getModulesEvents(eventId)

        for (const m of modules) {
            const moduleId = m.uid

            const ref1 = db.collection('modules').doc(moduleId).collection('attendees')
            const ref2 = db.collection('modules').doc(moduleId).collection('sessions')
            const ref3 = db.collection('modules').doc(moduleId).collection('tracks')
            const ref4 = db.collection('modules').doc(moduleId).collection('locations')
            const ref5 = db.collection('modules').doc(moduleId).collection('documents')
            const ref6 = db.collection('modules').doc(moduleId).collection('speakers')
            const ref7 = db.collection('modules').doc(moduleId).collection('widgets')
            const ref8 = db.collection('modules').doc(moduleId).collection('checkins')
            const ref9 = db.collection('modules').doc(moduleId).collection('surveys')
            const ref10 = db.collection('modules').doc(moduleId).collection('questions')
            const ref11 = db.collection('modules').doc(moduleId).collection('items')
            const ref12 = db.collection('modules').doc(moduleId).collection('groups')
            const ref13 = db.collection('modules').doc(moduleId).collection('pages')
            const ref14 = db.collection('modules').doc(moduleId).collection('posts')
            const ref15 = db.collection('modules').doc(moduleId).collection('maps')
            const ref16 = db.collection('modules').doc(moduleId).collection('folders')
            const ref17 = db.collection('modules').doc(moduleId).collection('customFields')

            // check if the path modules/{moduleId}/attendees exists.
            if (await funcCheckCollectionExist(ref1)) {
                funcDeleteCollection(ref1.path)
            }

            // check if the path modules/{moduleId}/sessions exists.
            if (await funcCheckCollectionExist(ref2)) {
                funcDeleteCollection(ref2.path)
            }

            // check if the path modules/{moduleId}/tracks exists.
            if (await funcCheckCollectionExist(ref3)) {
                funcDeleteCollection(ref3.path)
            }


            // check if the path modules/{moduleId}/locations exists.
            if (await funcCheckCollectionExist(ref4)) {
                funcDeleteCollection(ref4.path)
            }


            // check if the path modules/{moduleId}/documents exists.
            if (await funcCheckCollectionExist(ref5)) {
                funcDeleteCollection(ref5.path)
            }

            // check if the path modules/{moduleId}/speakers exists.
            if (await funcCheckCollectionExist(ref6)) {
                funcDeleteCollection(ref6.path)
            }

            // check if the path modules/{moduleId}/widgets exists.
            if (await funcCheckCollectionExist(ref7)) {
                funcDeleteCollection(ref7.path)
            }

            // check if the path modules/{moduleId}/checkins exists.
            if (await funcCheckCollectionExist(ref8)) {
                funcDeleteCollection(ref8.path)
            }

            // check if the path modules/{moduleId}/surveys exists.
            if (await funcCheckCollectionExist(ref9)) {
                funcDeleteCollection(ref9.path)
            }

            // check if the path modules/{moduleId}/questions exists.
            if (await funcCheckCollectionExist(ref10)) {
                funcDeleteCollection(ref10.path)
            }

            // check if the path modules/{moduleId}/items exists.
            if (await funcCheckCollectionExist(ref11)) {
                funcDeleteCollection(ref11.path)
            }

            // check if the path modules/{moduleId}/groups exists.
            if (await funcCheckCollectionExist(ref12)) {
                funcDeleteCollection(ref12.path)
            }

            // check if the path modules/{moduleId}/pages exists.
            if (await funcCheckCollectionExist(ref13)) {
                funcDeleteCollection(ref13.path)
            }

            // check if the path modules/{moduleId}/posts exists.
            if (await funcCheckCollectionExist(ref14)) {
                funcDeleteCollection(ref14.path)
            }

            // check if the path modules/{moduleId}/maps exists.
            if (await funcCheckCollectionExist(ref15)) {
                funcDeleteCollection(ref15.path)
            }

            // check if the path modules/{moduleId}/folders exists.
            if (await funcCheckCollectionExist(ref16)) {
                funcDeleteCollection(ref16.path)
            }

            // check if the path modules/{moduleId}/customFields exists.
            if (await funcCheckCollectionExist(ref17)) {
                funcDeleteCollection(ref17.path)
            }

            //remove modules from paths. Enable trigger dbModulesDelte that removes data from modules.
            db.collection('modules').doc(moduleId).delete()
        }


        resolve(true)
    })
})

// get the event modules.
const getModulesEvents = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore();

        db.collection('modules').where('eventId', '==', eventId).get().then((snapshot) => {
            const modules = []

            snapshot.forEach((element) => {
                modules.push(element.data())
            })

            resolve(modules)
        })
    })
}
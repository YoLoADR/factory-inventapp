const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let eventId = req.query.eventId;

        db
            .collection('events')
            .doc(eventId)
            .get()
            .then((snapshot) => {
                let event = snapshot.data();
                res.json({
                    touchIcon: event.webApp.touchIcon.url,
                    // splashScreen: event.webApp.splashScreen,
                    // favicon: event.webApp.favicon,
                    title: event.title
                });
            });
    });
});
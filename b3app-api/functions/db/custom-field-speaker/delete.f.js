const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.body.eventId;
        let moduleId = req.body.moduleId;
        let customId = req.body.customId;

        let refModule = admin.firestore().collection('modules').doc(moduleId);
        let refCustom = admin.firestore().collection('modules').doc(moduleId).collection('customFields').doc(customId);
        refCustom.get()
            .then((doc) => {
                let custom = doc.data();

                if (custom.type == 'select') {
                    let refCustomOptions = admin.firestore()
                        .collection('modules')
                        .doc(moduleId)
                        .collection('customFields')
                        .doc(customId).collection('options');

                    refCustomOptions.get()
                        .then((data) => {
                            data.forEach(
                                doc => {
                                    let option = doc.data();
                                    refCustomOptions.doc(option.uid).delete();
                                }
                            );
                        })

                    refModule.get().then((data) => {
                        let aux = data.data();
                        aux = aux['fieldsCustom'];
                        let orderItemDelete = aux[customId].order;

                        let updateFields = {};
                        for (let itemId in aux) {
                            if (aux[itemId].order > orderItemDelete) {
                                updateFields[`fieldsCustom.${itemId}.order`] = aux[itemId].order - 1;
                            }
                        }

                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                        updateFields[`fieldsCustom.${customId}`] = FieldValue.delete();

                        refModule.update(updateFields);
                        refCustom.delete();
                    })
                } else {
                    refModule.get().then((data) => {
                        let aux = data.data();
                        aux = aux['fieldsCustom'];
                        let orderItemDelete = aux[customId].order;

                        let updateFields = {};
                        for (let itemId in aux) {
                            if (aux[itemId].order > orderItemDelete) {
                                updateFields[`fieldsCustom.${itemId}.order`] = aux[itemId].order - 1;
                            }
                        }

                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                        updateFields[`fieldsCustom.${customId}`] = FieldValue.delete();

                        refModule.update(updateFields);
                        refCustom.delete();
                    })
                }
            })

        admin.firestore().collection('modules')
            .doc(moduleId)
            .collection('speakers')
            .get().then((data) => {

                if (data.size >= 1) {
                    //passa por todos os documentos
                    let listSpeakers = [];
                    data.forEach(
                        (doc) => {
                            let speaker = doc.data();
                            listSpeakers.push(speaker);
                        }
                    )

                    let listSpeakersError = [];
                    let totalSpeakers = listSpeakers.length;
                    let cont = 0;
                    for (let i = 0; i < totalSpeakers; i++) {
                        let batch = admin.firestore().batch();
                        let speaker = listSpeakers[i];

                        let refSpeakerEvent = admin.firestore().collection('events').doc(eventId).collection("speakers").doc(speaker.uid)
                            .collection('customFields').doc(customId);

                        let refSpeakerModule = admin.firestore().collection('modules').doc(moduleId).collection('speakers').doc(speaker.uid)
                            .collection('customFields').doc(customId);

                        batch.delete(refSpeakerEvent);
                        batch.delete(refSpeakerModule);

                        batch.commit()
                            .then(() => {
                                if (cont == totalSpeakers - 1) {
                                    if (listSpeakersError.length == 0) {
                                        res.status(200).json({
                                            code: 200,
                                            message: 'success',
                                            result: 'removed'
                                        });
                                    } else {
                                        res.status(500).json({
                                            code: 500,
                                            message: 'error',
                                            result: listSpeakersError
                                        });
                                    }
                                }

                                cont++;
                            })
                            .catch(() => {
                                listSpeakersError.push(speaker)

                                if (cont == totalSpeakers - 1) {
                                    res.status(500).json({
                                        code: 500,
                                        message: 'error',
                                        result: listSpeakersError
                                    });
                                }

                                cont++;
                            })
                    }
                } else {
                    res.status(200).json({
                        code: 200,
                        message: 'success',
                        result: 'removed'
                    });
                }

            });
    });
});
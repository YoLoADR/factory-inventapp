const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.firestore
    .document('modules/{moduleId}/customFields/{customId}').onCreate((snap, context) => {
        return new Promise((resolve, reject) => {
            let moduleId = context.params.moduleId;
            let customField = snap.data();

            admin.firestore().collection('modules')
                .doc(moduleId)
                .collection('speakers')
                .get().then((data) => {
                    if (data.size >= 1) {

                        let listSpeaker = [];
                        //pega todos os documentos
                        data.forEach(
                            (doc) => {
                                listSpeaker.push(doc.data());
                            }
                        )

                        let listErrorSpeaker = [];
                        let cont = 0;
                        for (let i = 0; i < listSpeaker.length; i++) {
                            let speaker = listSpeaker[i];
                            let batch = admin.firestore().batch();

                            let refSpeakerEvent = admin.firestore().collection("events")
                                .doc(speaker.eventId)
                                .collection("speakers")
                                .doc(speaker.uid)
                                .collection('customFields')
                                .doc(customField.uid)

                            let refSpeakerModule = admin.firestore().collection('modules')
                                .doc(moduleId)
                                .collection('speakers')
                                .doc(speaker.uid)
                                .collection('customFields')
                                .doc(customField.uid)

                            batch.set(refSpeakerEvent, customField);
                            batch.set(refSpeakerModule, customField);

                            batch.commit()
                                .then((data) => {
                                    if (cont == listSpeaker.length - 1) {
                                        resolve({
                                            code: 200,
                                            message: 'success',
                                            result: customField
                                        })
                                    }

                                    cont++;
                                })
                                .catch((error) => {
                                    listErrorSpeaker.push(speaker);

                                    if (cont == listSpeaker.length - 1) {
                                        reject({
                                            code: 500,
                                            message: 'error',
                                            result: listErrorSpeaker
                                        })
                                    }

                                    cont++;
                                })
                        }
                    } else {
                        resolve({
                            code: 200,
                            message: 'success',
                            result: customField
                        })
                    }
                })
        })

    });
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let quizId = req.query.quizId;
        let questionId = req.query.questionId;

        let refQuestionResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(questionId)
            .collection('result');

        refQuestionResult.get()
            .then(async(data) => {
                if (data.size >= 1) {
                    let listResult = [];
                    data.forEach(element => {
                        listResult.push(element.data());
                    });

                    let contResult = 0;
                    for (let result of listResult) {
                        await refQuestionResult.doc(result.user).delete()
                            .then(() => {
                                if (contResult == listResult.length - 1) {
                                    res.status(200).json({
                                        code: 200,
                                        message: 'OK',
                                        result: true
                                    })
                                }

                                contResult++;
                            })
                    }
                } else {
                    res.status(200).json({
                        code: 200,
                        message: 'OK',
                        result: true
                    })
                }
            })
    });
});
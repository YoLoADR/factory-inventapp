const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let quizId = req.query.quizId;
        let questionId = req.query.questionId;

        getQuestion(moduleId, quizId, questionId, (question) => {
            Promise.all([
                removeResult(moduleId, quizId, question),
                removeQuestion(moduleId, quizId, question)
            ]).then(() => {
                res.status(200).json({
                    code: 200,
                    message: 'OK',
                    result: true
                })
            })
        });
    });
});


function getQuestion(moduleId, quizId, questionId, onResolve) {
    let db = admin.firestore();

    let refQuestions = db
        .collection('modules')
        .doc(moduleId)
        .collection('quizs')
        .doc(quizId)
        .collection('questions')
        .doc(questionId);

    refQuestions.get()
        .then((data) => {
            let question = data.data();
            onResolve(question);
        })
}

function removeResult(moduleId, quizId, question) {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        let refQuestionResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(question.uid)
            .collection('result');

        refQuestionResult.get()
            .then(async(data) => {
                if (data.size >= 1) {
                    let listResult = [];
                    data.forEach(element => {
                        listResult.push(element.data());
                    });

                    let contResult = 0;
                    for (let result of listResult) {
                        await refQuestionResult.doc(result.user).delete()
                            .then(() => {

                                if (contResult == listResult.length - 1) {
                                    resolve(true);
                                }

                                contResult++;
                            })
                    }
                } else {
                    resolve(true);
                }
            })
    })
}

function removeQuestion(moduleId, quizId, question) {
    let db = admin.firestore();

    return new Promise((resolve, reject) => {
        refQuestion = db
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(question.uid);

        if (question.type == 'oneSelect' || question.type == 'multipleSelect') {
            let refAnswers = refQuestion
                .collection('answers');

            refAnswers.get()
                .then((dataAnswers) => {
                    let listAnswers = [];
                    dataAnswers.forEach(element => {
                        let answer = element.data();
                        listAnswers.push(answer);
                    });

                    let contAnswer = 0;
                    for (let answer of listAnswers) {
                        refAnswers.doc(answer.uid).delete()
                            .then(() => {
                                if (contAnswer == listAnswers.length - 1) {

                                    refQuestion.delete()
                                        .then(() => {
                                            resolve(true);
                                        })
                                }

                                contAnswer++;
                            })
                    }
                })

        } else {
            refQuestion.delete()
                .then(() => {
                    resolve(true);
                })
        }
    })
}
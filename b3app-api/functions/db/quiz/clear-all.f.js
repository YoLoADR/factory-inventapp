const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let moduleId = req.query.moduleId;
        let quizsIds = req.body.quizsIds;

        let contQuiz = 0;
        for (let quizId of quizsIds) {
            deleteQuizResults(moduleId, quizId).then(() => {

                if (contQuiz >= quizsIds.length - 1) {
                    res.status(200).json({
                        code: 200,
                        message: 'OK',
                        result: true
                    })
                }

                contQuiz++;
            })
        }

    });
});

function deleteQuizResults(moduleId, quizId) {
    return new Promise((resolve, reject) => {

        let db = admin.firestore();

        getQuestions(moduleId, quizId, (questions) => {

            let cont = 0;
            for (let question of questions) {

                let refQuestions = db
                    .collection('modules')
                    .doc(moduleId)
                    .collection('quizs')
                    .doc(quizId)
                    .collection('questions')
                    .doc(question.uid)
                    .collection('result');

                refQuestions.get()
                    .then(async(data) => {
                        if (data.size >= 1) {
                            let listResult = [];
                            data.forEach(element => {
                                listResult.push(element.data());
                            });

                            let contResult = 0;
                            for (let result of listResult) {
                                await refQuestions.doc(result.user).delete()
                                    .then(() => {
                                        if (contResult == listResult.length - 1) {
                                            if (cont == Object.keys(questions).length - 1) {
                                                resolve(true);
                                            }

                                            cont++;
                                        }

                                        contResult++;
                                    })
                            }
                        } else {

                            if (cont == Object.keys(questions).length - 1) {
                                resolve(true);
                            }

                            cont++;
                        }
                    })

            }
        });
    })

}

function getQuestions(moduleId, quizId, onResolve) {
    let db = admin.firestore();

    let listQuestions = [];

    let refQuestions = db.collection('modules').doc(moduleId).collection('quizs').doc(quizId).collection('questions');

    refQuestions.get()
        .then((data) => {
            data.forEach(element => {
                let question = element.data();
                listQuestions.push(question);
            });

            onResolve(listQuestions);
        })
}
/**
 * Trigger responsible for monitoring and removing  modules.
 * 
 */

// imports
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.
const funcDeleteCollection = require('../../utilities/delete-collection'); //function that deletes collections in a generic way.


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

// monitors the deletion of modules.
exports = module.exports = functions.firestore.document('modules/{moduleId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        const moduleRemove = snap.data()
        const moduleId = moduleRemove.uid
        const eventId = moduleRemove.eventId
        const type = moduleRemove.type

        // module schedule
        if (type === typeModules.SCHEDULE) {
            console.info(`delete module schedule: ${moduleId}`)
            await deleteModuleSchedule(moduleId, eventId)
        }

        // module widget
        if (type === typeModules.WIDGETS) {
            console.info(`delete module widget: ${moduleId}`)
            await deleteModuleWidget(moduleId)
        }

        // module infobooth
        if (type === typeModules.INFOBOOTH) {
            console.info(`delete module infobooth: ${moduleId}`)
            await deleteModuleInfobooth(moduleId, eventId)
        }


        // module custom-pages
        if (type === typeModules.CUSTOM_PAGE) {
            console.info(`delete module custom pages: ${moduleId}`)
            await deleteModuleCustomPages(moduleId, eventId)
        }

        // module news feed
        if (type === typeModules.NEWS_FEED) {
            console.info(`delete module news feed: ${moduleId}`)
            await deleteModuleNewsFeed(moduleId, eventId)
        }

        // module gallery
        if (type === typeModules.GALLERY) {
            console.info(`delete module gallery: ${moduleId}`)
            await deleteModuleGallery(moduleRemove)
        }

        // module document
        if (type === typeModules.DOCUMENT) {
            console.info(`delete module document: ${moduleId}`)
            await deleteModuleDocument(moduleRemove)
        }

        // module attendee
        if (type === typeModules.ATTENDEE) {
            console.info(`delete module attendee: ${moduleId}`)
            await deleteModuleAttendee(moduleId)
        }

        // module speaker
        if (type === typeModules.SPEAKER) {
            console.info(`delete module speaker: ${moduleId}`)
            await deleteModuleSpeaker(moduleId, eventId)
        }

        if (type === typeModules.GAMING) {
            console.info(`delete module gamification: ${moduleId}`)
            await deleteModuleGamification(moduleId, eventId)
        }

        if (type === typeModules.RANKING) {
            console.info(`delete module ranking: ${moduleId}`)
            await deleteModuleRanking(moduleId, eventId)
        }

        if (type === typeModules.CHECKIN) {
            console.info(`delete module checkin: ${moduleId}`)
            await deleteModuleCheckin(moduleId, eventId)
        }

        if (type === typeModules.GROUPS) {
            console.info(`delete module groups: ${moduleId}`)
            await deleteModuleGroups(moduleId, eventId)
        }

        if (type === typeModules.LOCATION) {
            console.info(`delete module locations: ${moduleId}`)
            await deleteModuleLocation(moduleId, eventId)
        }

        if (type === typeModules.MAPS) {
            console.info(`delete module maps: ${moduleId}`)
            await deleteModuleMaps(moduleId, eventId)
        }

        if (type === typeModules.NOTIFICATIONS) {
            console.info(`delete module notifications: ${moduleId}`)
            await deleteModuleNotifications(moduleId, eventId)
        }

        if (type === typeModules.INTERACTIVITY) {
            console.info(`delete module interactivity: ${moduleId}`)
            await deleteModuleInteractivity(moduleId, eventId)
        }

        resolve(true)
    })
})

// delete module schedule
const deleteModuleSchedule = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // delete path tracks
        const ref1 = db.collection('modules').doc(moduleId).collection('tracks')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        // delete paths sessions
        const ref2 = db.collection('modules').doc(moduleId).collection('sessions')

        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }


        resolve(true)
    })
}

// delete module widget
const deleteModuleWidget = (moduleId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // delete path widgets
        const ref = db.collection('modules').doc(moduleId).collection('widgets')

        // check if the path exists.
        if (await checkCollectionExist(ref)) {
            funcDeleteCollection(ref.path)
        }

        resolve(true)
    })
}


// delete module infobooth
const deleteModuleInfobooth = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        const ref1 = db.collection('modules').doc(moduleId).collection('pages')
        const ref2 = db.collection('events').doc(eventId).collection('modules').doc(moduleId).collection('pages')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }

        resolve(true)
    })
}

// delete module custom pages
const deleteModuleCustomPages = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        const ref1 = db.collection('modules').doc(moduleId).collection('pages')
        const ref2 = db.collection('events').doc(eventId).collection('modules').doc(moduleId).collection('pages')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }

        resolve(true)
    })
}



// delete module news feed
const deleteModuleNewsFeed = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // list the module posts that are in the event path.
        const posts = await getPostsModule(moduleId)

        // delete post from the event path.
        for (const post of posts) {
            const postId = post.uid
            db.collection('events').doc(eventId).collection('feed-posts').doc(postId).delete()
        }


        // delete posts in module path.
        const ref1 = db.collection('modules').doc(moduleId).collection('posts')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        resolve(true)
    })
}

// list the module posts that are in the event path.
const getPostsModule = (moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const posts = []

        db.collection('modules').doc(moduleId).collection('posts').get().then((snapshot) => {
            snapshot.forEach((element) => {
                posts.push(element.data())
            })

            resolve(posts)
        })
    })
}


// delete module gallery
const deleteModuleGallery = (module) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();
        const moduleId = module.uid
        const eventId = module.eventId

        // list the module images.
        const images = await getImagesModuleGallery(moduleId)

        // deletes the image of the event path.
        for (const image of images) {
            const imageId = image.uid
            db.collection('events').doc(eventId).collection('gallery-images').doc(imageId).delete()
        }

        // deletes the image of the module path.
        const ref1 = db.collection('modules').doc(moduleId).collection('folders')
            // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        resolve(true)
    })
}

// list the module gallery that are in the event path.
const getImagesModuleGallery = (moduleId) => {
    return new Promise(async(resolve) => {
        const list = []
        const folders = await getFoldersModuleGallery(moduleId)

        for (const folder of folders) {
            const folderId = folder.uid
            const images = await getImagesOfFolders(folderId, moduleId)

            for (const image of images) {
                list.push(image)
            }
        }

        resolve(list)
    })
}

// list module folders
const getFoldersModuleGallery = (moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        db.collection('modules').doc(moduleId).collection('folders').get().then((snapshot) => {
            snapshot.forEach((element) => {
                const folder = element.data()
                list.push(folder)
            })

            resolve(list)
        })
    })
}

// list the folder images.
const getImagesOfFolders = (folderId, moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        db.collection('modules').doc(moduleId).collection('folders').doc(folderId).collection('images').get().then((snapshot) => {
            snapshot.forEach((element) => {
                const image = element.data()
                list.push(image)
            })

            resolve(list)
        })
    })
}


// delete module documents
const deleteModuleDocument = (module) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();
        const moduleId = module.uid
        const eventId = module.eventId

        // list the module images.
        const documents = await getDocumentsModule(moduleId)

        // deletes the document of the event path.
        for (const document of documents) {
            const documentId = document.uid
            db.collection('events').doc(eventId).collection('documents').doc(documentId).delete()
        }

        // deletes the documents of the module path.
        const ref1 = db.collection('modules').doc(moduleId).collection('folders')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        resolve(true)
    })
}


// list the module documents that are in the event path.
const getDocumentsModule = (moduleId) => {
    return new Promise(async(resolve) => {
        const list = []
        const folders = await getFoldersModuleDocument(moduleId)

        for (const folder of folders) {
            const folderId = folder.uid
            const documents = await getDocumentsOfFolders(folderId, moduleId)

            for (const document of documents) {
                list.push(document)
            }
        }

        resolve(list)
    })
}

// list module folders
const getFoldersModuleDocument = (moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        db.collection('modules').doc(moduleId).collection('folders').get().then((snapshot) => {
            snapshot.forEach((element) => {
                const folder = element.data()
                list.push(folder)
            })

            resolve(list)
        })
    })
}

// list the folder documents.
const getDocumentsOfFolders = (folderId, moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        db.collection('modules').doc(moduleId).collection('folders').doc(folderId).collection('documents').get().then((snapshot) => {
            snapshot.forEach((element) => {
                const image = element.data()
                list.push(image)
            })

            resolve(list)
        })
    })
}



//delete module attendee
const deleteModuleAttendee = (moduleAttendeeId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        const ref1 = db.collection('modules').doc(moduleAttendeeId).collection('customFields')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        const ref2 = db.collection('modules').doc(moduleAttendeeId).collection('attendees')

        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }

        resolve(true)
    })
}

//delete module speaker
const deleteModuleSpeaker = (moduleSpeakerId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // delete custom fields from module
        const ref1 = db.collection('modules').doc(moduleSpeakerId).collection('customFields')

        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        // delete speakers from module
        const ref2 = db.collection('modules').doc(moduleSpeakerId).collection('speakers')

        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }


        resolve(true)
    })
}

// delete module gamification
const deleteModuleGamification = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();
        // delete path gaming qrcodes
        let ref1 = db.collection('modules').doc(moduleId).collection('gamification-qrcodes')
            // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }
        // delete path modules
        db.collection('modules').doc(moduleId).delete();

        // delete paths events
        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// delete module checkin
const deleteModuleCheckin = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        let ref1 = db.collection('modules').doc(moduleId).collection('checkins');
        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }
        // delete path modules
        db.collection('modules').doc(moduleId).delete();

        // delete paths events
        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// delete module groups
const deleteModuleGroups = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        let ref1 = db.collection('modules').doc(moduleId).collection('groups');
        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }
        // delete path modules
        db.collection('modules').doc(moduleId).delete();

        // delete paths events
        let ref2 = db.collection('events').doc(eventId).collection('modules').doc(moduleId).collection('groups');

        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }

        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// delete module location
const deleteModuleLocation = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        let ref1 = db.collection('modules').doc(moduleId).collection('locations');
        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }
        // delete path modules
        db.collection('modules').doc(moduleId).delete();

        // delete paths events
        let ref2 = db.collection('events').doc(eventId).collection('modules').doc(moduleId).collection('locations');
        // check if the path exists.
        if (await checkCollectionExist(ref2)) {
            funcDeleteCollection(ref2.path)
        }
        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// delete module maps
const deleteModuleMaps = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        let ref1 = db.collection('modules').doc(moduleId).collection('maps');
        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }
        // delete path modules
        db.collection('modules').doc(moduleId).delete();
        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// delete module notifications
const deleteModuleNotifications = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        let ref1 = db.collection('events').doc(eventId).collection('notifications');
        // check if the path exists.
        if (await checkCollectionExist(ref1)) {
            funcDeleteCollection(ref1.path)
        }

        // delete path modules
        db.collection('modules').doc(moduleId).delete();
        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// delete module interactivity
const deleteModuleInteractivity = (moduleId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // delete path modules
        db.collection('modules').doc(moduleId).delete();
        db.collection('events').doc(eventId).collection('modules').doc(moduleId).delete();

        resolve(true)
    })
}

// check for elements within the collection
const checkCollectionExist = (path) => {
    return new Promise((resolve) => {
        path.get().then((snapshot) => {
            const size = snapshot.size > 0 ? true : false
            resolve(size)
        })
    })
}
/**
 * Trigger responsible for monitoring and updating the participant module.
   Changes the module field of participants within the checkin.
 * 
 */

// imports
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }


exports = module.exports = functions.firestore.document('modules/{moduleId}').onUpdate((change, context) => {
    return new Promise(async(resolve) => {
        const module = change.after.data()
        const eventId = module.eventId
        let db = admin.firestore()

        if (module.type === typeModules.ATTENDEE) {
            const checkins = await getCheckinsEvent(eventId)

            for (const checkin of checkins) {
                const moduleAttendeeId = module.uid
                const checkinId = checkin.uid
                const checkinModuleId = checkin.moduleId

                const attendees = await getAttendeesModuleAttendees(moduleAttendeeId, checkinId, checkinModuleId)
                for (const attendee of attendees) {
                    const newModule = { uid: module.uid, name: module.name }

                    db.collection('modules')
                        .doc(checkinModuleId)
                        .collection('checkins')
                        .doc(checkinId)
                        .collection('attendees')
                        .doc(attendee.uid)
                        .update({
                            module: newModule
                        })
                }
            }
        }

        resolve(true)
    })
})


/**
 * get all checks in of the event.  
 * @param eventId  
 * @returns checkin[]
 */

const getCheckinsEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let checkins = []

        // get checkins module
        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', typeModules.CHECKIN)
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve([])

                // get all module checkins.
                snapshot.forEach((element) => {
                    const module = element.data()
                    const moduleId = module.uid

                    db.collection('modules')
                        .doc(moduleId)
                        .collection('checkins')
                        .get()
                        .then((childSnapshot) => {
                            childSnapshot.forEach((element) => {
                                const checkin = element.data()
                                checkins.push(checkin)
                            })

                            resolve(checkins)
                        })
                })
            })
    })
}


/**
 * get attendees module attendees 
 * @param {string} attendeeId  
 * @param {string} checkinId  
 * @param {string} checkinModuleId  
 * @returns string
 */

const getAttendeesModuleAttendees = (moduleAttendeeId, checkinId, checkinModuleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const attendees = []


        db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .where('module.uid', '==', moduleAttendeeId)
            .get()
            .then((snapshot) => {
                snapshot.forEach((element) => {
                    const attendee = element.data()
                    attendees.push(attendee)
                })

                resolve(attendees)
            })
    })
}
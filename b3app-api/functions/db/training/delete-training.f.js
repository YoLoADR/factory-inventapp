const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let trainingId = req.query.trainingId;

        getQuestions(moduleId, trainingId, (questions) => {

            ref = db
                .collection('modules')
                .doc(moduleId)
                .collection('trainings')
                .doc(trainingId);

            if (questions !== null) {
                Promise.all([
                    removeResults(moduleId, trainingId, questions),
                    removeQuestions(moduleId, trainingId, questions)
                ]).then(() => {


                    ref.get().then((data) => {
                        let training = data.data();
                        let order = training.order;

                        reorderTrainings(moduleId, order, (value) => {
                            if (value) {
                                ref.delete()
                                    .then(() => {
                                        res.status(200).json({
                                            code: 200,
                                            message: 'OK',
                                            result: true
                                        })
                                    })
                            }
                        })
                    })


                })
            } else {
                ref.get().then((data) => {
                    let training = data.data();
                    let order = training.order;

                    reorderTrainings(moduleId, order, (value) => {
                        if (value) {
                            ref.delete()
                                .then(() => {
                                    res.status(200).json({
                                        code: 200,
                                        message: 'OK',
                                        result: true
                                    })
                                })
                        }
                    })
                })
            }
        });
    });
});


function getQuestions(moduleId, trainingId, onResolve) {
    let db = admin.firestore();

    let listQuestions = [];

    let refQuestions = db.collection('modules').doc(moduleId).collection('trainings').doc(trainingId).collection('questions');

    refQuestions.get()
        .then((data) => {
            if (data.size > 0) {
                data.forEach(element => {
                    let question = element.data();
                    listQuestions.push(question);
                });

                onResolve(listQuestions);
            } else {
                onResolve(null);
            }
        })
}

function removeResults(moduleId, trainingId, questions) {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        let cont = 0;
        for (let question of questions) {

            let refQuestionResult = db
                .collection('modules')
                .doc(moduleId)
                .collection('trainings')
                .doc(trainingId)
                .collection('questions')
                .doc(question.uid)
                .collection('result');

            refQuestionResult.get()
                .then(async(data) => {
                    if (data.size >= 1) {
                        let listResult = [];
                        data.forEach(element => {
                            listResult.push(element.data());
                        });

                        let contResult = 0;
                        for (let result of listResult) {
                            await refQuestionResult.doc(result.user).delete()
                                .then(() => {

                                    if (contResult == listResult.length - 1) {
                                        if ('contQuestion:', cont == Object.keys(questions).length - 1) {
                                            resolve(true);
                                        }

                                        cont++;
                                    }

                                    contResult++;
                                })
                        }
                    } else {

                        if (cont == Object.keys(questions).length - 1) {
                            resolve(true);
                        }

                        cont++;
                    }
                })
        }
    })
}

function removeQuestions(moduleId, trainingId, questions) {
    let db = admin.firestore();

    return new Promise((resolve, reject) => {
        let cont = 0;
        for (let question of questions) {
            refQuestion = db
                .collection('modules')
                .doc(moduleId)
                .collection('trainings')
                .doc(trainingId)
                .collection('questions')
                .doc(question.uid);

            if (question.type == 'oneSelect' || question.type == 'multipleSelect') {
                let refAnswers = refQuestion
                    .collection('answers');

                refAnswers.get()
                    .then((dataAnswers) => {
                        let listAnswers = [];
                        dataAnswers.forEach(element => {
                            let answer = element.data();
                            listAnswers.push(answer);
                        });

                        let contAnswer = 0;
                        for (let answer of listAnswers) {
                            refAnswers.doc(answer.uid).delete()
                                .then(async() => {
                                    if (contAnswer == listAnswers.length - 1) {

                                        await refQuestion.delete()
                                            .then(() => {
                                                if (cont == Object.keys(questions).length - 1) {
                                                    resolve(true);
                                                }

                                                cont++;
                                            })
                                    }

                                    contAnswer++;
                                })
                        }
                    })

            } else {
                refQuestion.delete()
                    .then(() => {
                        if (cont == Object.keys(questions).length - 1) {
                            resolve(true);
                        }

                        cont++;
                    })
            }
        }
    })
}

function reorderTrainings(moduleId, removeOrder, onResolve) {
    let db = admin.firestore()
    let batch = db.batch()

    let ref = db
        .collection('modules')
        .doc(moduleId)
        .collection('trainings');

    ref.get().then((data) => {
        if (data.size > 0) {
            let listTrainings = []
            data.forEach(element => {
                let training = element.data()
                listTrainings.push(training)
            });

            for (let training of listTrainings) {
                if (training.order > removeOrder) {
                    newOrder = training.order - 1;

                    refTraining = ref.doc(training.uid);
                    batch.update(refTraining, { order: newOrder })
                }
            }

            batch.commit()
                .then(() => {
                    onResolve(true)
                })
                .catch((err) => {
                    onResolve(false)
                })

        } else {
            onResolve(true)
        }
    })
}
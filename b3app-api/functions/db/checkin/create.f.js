const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const cors = require("cors")({ origin: true });
// /// types modules
const ATTENDEE = require('../../utilities/enums/type-module').ATTENDEE


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }


exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        main(req, res)
    })
})

/**
 * main function.
 * @param req
 * @param res
 * @returns res.ponse
 */
const main = async(req, res) => {
    // get params
    const eventId = req.body.eventId
    const moduleId = req.body.moduleId
    const checkin = req.body.checkin

    const attendees = await getAttendeesEvent(eventId)

    let db = admin.firestore()

    let batchesArray = [];
    batchesArray.push(db.batch());
    let batchIndex = 0
    let batchCounter = 0

    let ref = db.collection('modules').doc(moduleId).collection('checkins').doc()
    checkin.uid = ref.id
    batchesArray[0].set(ref, checkin)

    for (const attendee of attendees) {
        batchCounter++

        let ref = db.collection('modules')
            .doc(moduleId)
            .collection('checkins')
            .doc(checkin.uid)
            .collection('attendees')
            .doc(attendee.uid)


        attendee.checkinStatus = false
        batchesArray[batchIndex].set(ref, attendee)


        if (batchCounter === 450) {
            batchesArray.push(db.batch())
            batchIndex++;
            batchCounter = 0;
        }
    }

    let batchSize = batchesArray.length
    let contBatchCommit = 0;

    batchesArray.forEach((batch) => {
        batch.commit()
            .then((_) => {
                if (contBatchCommit == batchSize - 1) {
                    response(res, true, null)
                }
                contBatchCommit++;
            })

    })

}


/**
 * verify that all sessions have been processed and return a response to the application
 * @param res
 * @param status
 * @param messageErrors
 * @returns res.json  
 */
const response = (res, status, messageErrors) => {
    res.json({
        code: 200,
        message: 'success',
        result: {
            status: status,
            'messageErrors': messageErrors
        }
    })
}


/**
 * search all event participants.
 * @param eventId
 * @returns attendees  
 */

const getAttendeesEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const attendees = []

        db.collection('events')
            .doc(eventId)
            .collection('attendees')
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve(attendees)

                snapshot.forEach((element) => {
                    const attendee = element.data()
                    attendees.push(attendee)
                })

                resolve(attendees)
            })
    })
}

/**
 * get all attendee modules.
 * @param eventId
 * @returns modules  
 */

const getAttendeesModules = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const modules = []

        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', ATTENDEE)
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve(modules)

                snapshot.forEach((element) => {
                    const module = element.data()
                    modules.push(module)
                })

                resolve(modules)
            })
    })
}
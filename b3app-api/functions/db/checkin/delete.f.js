/**
 * Trigger responsible for monitoring and removing checkin participants.
 */

// imports
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const funcDeleteCollection = require('../../utilities/delete-collection'); //function that deletes collections in a generic way.
const funcCheckCollectionExist = require('../../utilities/check-collection') //function that checks only the collection has data.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }


exports = module.exports = functions.firestore.document('modules/{moduleId}/checkins/{checkinId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        const moduleCheckinId = context.params.moduleId
        const checkinId = context.params.checkinId

        const ref = db.collection('modules').doc(moduleCheckinId).collection('checkins').doc(checkinId).collection('attendees')

        // check if the path modules/{moduleId}/checkins/{checkinId}/attendees exists.
        if (await funcCheckCollectionExist(ref)) {
            funcDeleteCollection(ref.path)
        }

        resolve(true)
    })
})
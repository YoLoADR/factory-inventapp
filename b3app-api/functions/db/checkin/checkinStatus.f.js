const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const GLOBAL_VISION = require("../../utilities/enums/type-vision-checkin").GLOBAL_VISION
const GROUP_VISION = require("../../utilities/enums/type-vision-checkin").GROUP_VISION

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.body.eventId !== null) {
            let eventId = req.body.eventId;
            let moduleId = req.body.moduleId;
            let checkinId = req.body.checkinId;
            let order = req.body.order;
            let typeVision = req.body.typeVision;
            let groups = req.body.groups;

            // Checks the type of view of the checkin.
            if (typeVision === GLOBAL_VISION || typeof typeVision === 'undefined' || typeVision === null) {
                loadCheckinGlobal(res, checkinId, moduleId, order)
            } else {
                loadCheckinByGroup(res, groups, checkinId, moduleId, eventId, order)
            }
        } else {
            res.json('fired!');
        }
    });
});


// loads check in with overview.
const loadCheckinGlobal = (res, checkinId, moduleId, order) => {
    let db = admin.firestore();
    let attendees = [];

    console.log('load checkin global.')
        //  Loads event attendees.
    const ref = db
        .collection('modules')
        .doc(moduleId)
        .collection('checkins')
        .doc(checkinId)
        .collection('attendees')
        .orderBy('queryName', 'asc')
        .get()

    ref.then((snapshot) => {
        if (snapshot.size) {
            snapshot.forEach((element) => {
                const attendee = element.data()
                attendees.push(attendee)
            })

            response(res, attendees, order)
        } else {
            response(res, [], order)
        }
    })
}

//loads group view check-in.
const loadCheckinByGroup = (res, groups, checkinId, moduleId, eventId, order) => {
    let db = admin.firestore();
    const auxGroups = []

    for (const uid in groups) {
        auxGroups.push(groups[uid])
    }

    if (auxGroups.length > 0) {
        let contGroup = 0
        const attendees = []


        // get the attendees of each group
        for (const group of auxGroups) {
            const groupId = `groups.${group.uid}`
            let cont = 0

            // load group atttendees.
            const ref = db
                .collection('modules')
                .doc(moduleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees')
                .orderBy(groupId, 'asc')
                .get()

            ref.then((snapshot) => {
                if (snapshot.size > 0) {
                    // withdraws replay of attendees
                    snapshot.forEach((element) => {
                        const attendee = element.data()

                        // Checks if the participant is already in the array.
                        const pos = attendees.map(function(e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1) {
                            attendees.push(attendee)
                        }

                        cont++

                        // the group participants were sued.
                        if (cont >= snapshot.size) {
                            contGroup++
                        }

                        // participants from all groups were sued.
                        if (contGroup >= auxGroups.length) {
                            response(res, attendees, order) // Run the check in data.
                        }
                    })
                } else {
                    contGroup++

                    // participants from all groups were sued.
                    if (contGroup >= auxGroups.length) {
                        response(res, attendees, order) // Run the check in data.
                    }
                }
            })
        }
    } else {
        response(res, [], 0, 0, order)
    }
}

// send response from server.
function response(res, attendees, order) {
    let auxOrder;
    let aux = attendees;
    attendees = null;

    switch (order) {
        case 'asc':
            attendees = aux.sort(function(a, b) {
                if (a['queryName'] < b['queryName']) { return -1; }
                if (a['queryName'] > b['queryName']) { return 1; }
                return 0;
            });

            break;

        case 'desc':
            attendees = aux.sort(function(a, b) {
                if (a['queryName'] < b['queryName']) { return 1; }
                if (a['queryName'] > b['queryName']) { return -1; }
                return 0;
            });

            break;

        case 'present':
            attendees = aux.sort(function(a, b) {
                if (a['queryName'] < b['queryName']) { return -1; }
                if (a['queryName'] > b['queryName']) { return 1; }
                return 0;
            });

            auxOrder = attendees;
            attendees = [];
            attendees = auxOrder.sort(function(a, b) {
                if (a['checkinStatus'] == true && b['checkinStatus'] == false) { return -1; }
                if (a['checkinStatus'] == false && b['checkinStatus'] == true) { return 1; }
                return 0;
            });

            break;
        case 'away':
            attendees = aux.sort(function(a, b) {
                if (a['queryName'] < b['queryName']) { return -1; }
                if (a['queryName'] > b['queryName']) { return 1; }
                return 0;
            });

            auxOrder = attendees;
            attendees = [];
            attendees = auxOrder.sort(function(a, b) {
                if (a['checkinStatus'] == false && b['checkinStatus'] == true) { return -1; }
                if (a['checkinStatus'] == true && b['checkinStatus'] == false) { return 1; }
                return 0;
            });

            break;
    }

    res.json({
        code: 200,
        message: 'success',
        result: attendees,
    });
}
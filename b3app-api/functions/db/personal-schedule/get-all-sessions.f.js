/**
 * 
 */

const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const cors = require("cors")({ origin: true });

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        const db = admin.firestore()
        let eventId = req.query.eventId;

        // get all the sessions of the event.
        const ref1 = db.collection('events').doc(eventId).collection('sessions')

        ref1.get().then((snapshot) => {
            const aux = []

            // get all the sessions of the event.
            snapshot.forEach((childSnapshot) => {
                const session = childSnapshot.data()
                aux.push(session)
            })

            if (aux.length === 0) { response(res, []) }

            // browse through the sessions and search for participants in each session.
            const sessions = []
            for (const session of aux) {
                const sessionId = session.uid

                // obtain the number of participants of each session.
                const ref = db.collection('events').doc(eventId).collection('sessions').doc(sessionId).collection('attendees')

                // get the session participants.
                ref.get().then((snapshot) => {
                    session.qtdAttendees = snapshot.size
                    session.attendees = []

                    snapshot.forEach((childSnapshot) => {
                        session.attendees.push(childSnapshot.data())
                    })

                    sessions.push(session)

                    // if array sessions tiiver all sessions of the aux array, all sessions took its participants.
                    if (sessions.length === aux.length) {
                        // Does the qtdAttendees Field Arrangement
                        sessions.sort((a, b) => (a.qtdAttendees < b.qtdAttendees) ? 1 : ((b.qtdAttendees < a.qtdAttendees) ? -1 : 0));
                        response(res, sessions)
                    }
                })
            }
        })
    })
})

// // verify that all sessions have been processed and return a response to the application
const response = (res, data) => {
    res.json({
        code: 200,
        message: 'success',
        result: {
            sessions: data,
        }
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

const cors = require("cors")({
    origin: true
});

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let moduleId = req.body.moduleId;
        let questionId = req.body.questionId;
        console.log('Ids:', moduleId, questionId)

        let db = admin.firestore();

        let refItem = db
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(questionId);

        refItem.collection('questions')
            .get()
            .then((data) => {
                if (data.size > 0) { // caso o item tenha questões pega cada uma delas para excluir
                    let listElementsIds = [];
                    data.forEach(element => {
                        let question = element.data();
                        listElementsIds.push(question.uid);
                    });

                    let cont = 0;
                    for (let item of listElementsIds) {
                        refItem
                            .collection('questions')
                            .doc(item)
                            .collection('votes')
                            .get()
                            .then((snapshot) => {
                                if (snapshot.size > 0) { //caso a questão tenha votos exclui cada um dos votos antes de excluir a questão
                                    let listVotes = [];
                                    snapshot.forEach(element => {
                                        let vote = element.data();
                                        listVotes.push(vote.uid);
                                    });

                                    let contVote = 0;
                                    for (let voteId of listVotes) {
                                        refItem
                                            .collection('questions')
                                            .doc(item)
                                            .collection('votes')
                                            .doc(voteId)
                                            .delete()
                                            .then(async () => {

                                                if (contVote == listVotes.length - 1) {

                                                    await refItem.collection('questions')
                                                        .doc(item)
                                                        .delete()
                                                        .then(() => {
                                                            if (cont == listElementsIds.length - 1) {
                                                                console.log('response 200')
                                                                res.status(200).json({
                                                                    code: 200,
                                                                    message: 'Resultados excluidos com sucesso.',
                                                                    result: true
                                                                })
                                                            }

                                                            cont++;
                                                        })

                                                }

                                                contVote++;
                                            })
                                    }
                                } else { // caso a questão não tenha votos apaga diretamente
                                    refItem.collection('questions')
                                        .doc(item)
                                        .delete()
                                        .then(() => {
                                            if (cont == listElementsIds.length - 1) {
                                                console.log('response 200')
                                                res.status(200).json({
                                                    code: 200,
                                                    message: 'Resultados excluidos com sucesso.',
                                                    result: true
                                                })
                                            }

                                            cont++;
                                        })
                                }
                            })
                    }
                } else { // caso o item não tenha questões, apaga direto
                    console.log('response 204')
                    res.status(200).json({
                        code: 200,
                        message: 'Não há resultados para excluir.',
                        result: true
                    })
                }
            })
    });
});
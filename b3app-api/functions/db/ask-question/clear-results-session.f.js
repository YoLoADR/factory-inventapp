const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

const cors = require("cors")({
    origin: true
});

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let eventId = req.body.eventId;
        let moduleId = req.body.moduleId;
        let sessionId = req.body.sessionId;

        let refEvent = db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')

        let refModule = db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')


        refModule.get()
            .then((data) => {
                let listQuestions = [];

                data.forEach(element => {
                    let question = element.data();
                    listQuestions.push(question);
                });

                let cont = 0;
                for (let question of listQuestions) {
                    deleteQuestion(eventId, moduleId, sessionId, question.uid)
                        .then((data) => {
                            if (data === true) {
                                if (cont == listQuestions.length - 1) {
                                    res.status(200).json({
                                        code: 200,
                                        message: 'Resultados excluidos com sucesso.',
                                        result: true
                                    })
                                }

                                cont++;
                            }
                        })
                }
            })
    });
});


function deleteQuestion(eventId, moduleId, sessionId, questionId) {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        let refEvent = db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(questionId)

        let refModule = db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(questionId)

        refModule
            .collection('votes')
            .get()
            .then((data) => {
                if (data.size > 0) {
                    let listVotes = [];

                    data.forEach(element => {
                        let vote = element.data();
                        listVotes.push(vote);
                    });

                    let batch = db.batch();

                    for (let vote of listVotes) {
                        refVoteEvent = refEvent.collection('votes').doc(vote.uid);
                        refVoteModule = refModule.collection('votes').doc(vote.uid);

                        batch.delete(refVoteEvent);
                        batch.delete(refVoteModule);
                    }

                    batch.delete(refEvent);
                    batch.delete(refModule);

                    batch.commit()
                        .then(() => {
                            resolve(true);
                        })
                        .catch((err) => {
                            reject(err);
                        })

                } else {
                    let batch = db.batch();

                    batch.delete(refEvent);
                    batch.delete(refModule);

                    batch.commit()
                        .then(() => {
                            resolve(true);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                }
            })
            .catch((err) => {
                reject(err);
            })
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let sessionId = req.query.sessionId;

        let refModule = db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')

        refModule
            .orderBy("createdAt")
            .get()
            .then((data) => {

                if (data.size <= 0) {
                    res.status(404).json({
                        code: 404,
                        message: 'Not Found',
                    })
                }

                let totalQuestions = data.size;
                let listResults = [];

                let cont = 0;
                data.forEach(element => {
                    let question = element.data();

                    refModule
                        .doc(question.uid)
                        .collection('votes')
                        .get()
                        .then((dataVotes) => {
                            question.totalLikes = dataVotes.size;

                            if (question.userId !== null && question.userId !== undefined) {
                                getUser(question.userId, (user) => {
                                    let obj = {};

                                    if (user !== null && user !== undefined) {
                                        obj.userName = user.name;
                                        obj.userEmail = user.email;
                                    } else {
                                        obj.userName = '';
                                        obj.userEmail = '';
                                    }

                                    obj.question = question.question;
                                    obj.totalLikes = question.totalLikes;
                                    obj.timestamp = question.createdAt;

                                    listResults.push(obj);

                                    if (cont == totalQuestions - 1) {
                                        res.status(200).json({
                                            code: 200,
                                            message: 'OK',
                                            result: listResults
                                        })
                                    }

                                    cont++;
                                })
                            } else {
                                let obj = {};

                                obj.userName = '';
                                obj.userEmail = '';
                                obj.question = question.question;
                                obj.totalLikes = question.totalLikes;
                                obj.timestamp = question.createdAt;

                                listResults.push(obj);

                                if (cont == totalQuestions - 1) {
                                    res.status(200).json({
                                        code: 200,
                                        message: 'OK',
                                        result: listResults
                                    })
                                }

                                cont++;
                            }
                        })
                });
            })
    });
});

function getUser(userId, onResolve) {
    let db = admin.firestore();

    let refUsers = db.collection('users').doc(userId);
    refUsers.get()
        .then((data) => {
            if (!data.exists) {
                onResolve(null);
            }

            onResolve(data.data())
        })
}
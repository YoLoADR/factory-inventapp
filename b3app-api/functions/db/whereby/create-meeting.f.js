const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const axios = require('axios');

const cors = require("cors")({
    origin: true
});

// Create meeting on whereby
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, async () => {
        try {
            let resp = await axios.default.post("https://api.whereby.dev/v1/meetings", req.body, {
                headers: {
                    "Authorization": 'Bearer ' + configServer.wherebyApiKey
                }
            })
            if (resp.status == 201 && resp.data) {
                res.status(201);
                res.send(resp.data);
            } else {
                res.status(500);
                res.send("An error has occured");
            }
        } catch (error) {
            console.log("Error: ", error);
            res.status(500);
            res.send(error);
        }
    });
});
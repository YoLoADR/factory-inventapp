const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const axios = require('axios');

const cors = require("cors")({
    origin: true
});

// delete attendee/speaker and remake user with new type
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, async () => {
        try {
            if (req.body && req.body.meetingId) {
                let resp = await axios.default.delete("https://api.whereby.dev/v1/meetings/" + req.body.meetingId, {
                    headers: {
                        "Authorization": 'Bearer ' + configServer.wherebyApiKey
                    }
                })
                if (resp.status == 204) {
                    res.status(204);
                    res.send("meeting-deleted");
                } else {
                    res.status(500);
                    res.send("Meeting-not-deleted");
                }
            } else {
                res.status(500);
                res.send("no-meeting-id");
            }
        } catch (error) {
            res.status(500);
            res.send(error);
        }
    });
});
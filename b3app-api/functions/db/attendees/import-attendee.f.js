const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const NameGroup = require('../../utilities/enums/name-group')
const TypeModule = require('../../utilities/enums/type-module')

try {
    !admin.apps.length ? admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    }) : admin.app();

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

const cors = require("cors")({
    origin: true
});
let principalLanguage = ""
let groupsLanguage = []

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        main(req, res)
    });
});

const main = async (req, res) => {
    let arrayAttendees = [];
    let arrayUpdateds = [];
    let arrayIdentifiersDouble = [];
    let arrayEmailInSpeaker = [];
    let arrayEmailsDouble = [];
    let arrayFail = [];
    let failGroups = [];
    let failDocuments = [];
    let failCustomField = [];
    let cont = 0;
    let totalAttendees = req.body.attendees.length;
    let groupModuleId = req.body.groupModuleId;
    let eventId = req.body.eventId
    let nameModule = req.body.nameModule
    const event = await getEvent(eventId)
    principalLanguage = event.language;
    // groupsLanguage = await getGroupsLanguages(groupModuleId, eventId)


    for (let i = 0; i < totalAttendees; i++) {
        let attendee = req.body.attendees[i];
        checkEmailExistInSpeakers(attendee.email, req.body.eventId).then((statusSpeakerEmail) => {
            if (statusSpeakerEmail == false) {
                // check double identifier 
                checkIdentifier(req.body.eventId, attendee.identifier).then((checkStatus) => {
                    if (checkStatus == undefined) {
                        // check e-mail is double
                        checkEmailExistInAttendees(req.body.eventId, attendee.email).then((checkEmailAttendee) => {
                                if (checkEmailAttendee == undefined) {
                                    // check if e-mail exist in users
                                    checkEmailExistInUsers(attendee.email).then((checkEmailUser) => {

                                            if (checkEmailUser == undefined) {
                                                Promise.all(
                                                        [
                                                            validateGroups(groupModuleId, attendee.groups),
                                                            addCustomField(req.body.moduleId, attendee),
                                                            validateDocuments(req.body.eventId, attendee.documents, principalLanguage)
                                                        ]
                                                    ).then((result) => {
                                                        /* case e-mail doesn't exist in users path, 
                                                            create in users, 
                                                            events=>attendes, 
                                                            and modules=>attendees*/
                                                        if (result[0].invalid !== undefined && result[0].invalid !== null) {
                                                            for (let groupInvalid of result[0].invalid) {
                                                                if (!(failGroups.indexOf(groupInvalid) > -1)) {
                                                                    failGroups.push(groupInvalid);
                                                                }
                                                            }
                                                        }

                                                        if (result[1].invalid !== undefined && result[1].invalid !== null) {
                                                            for (let invalid of result[1].invalid) {
                                                                if (!verifyExistCustomField(failCustomField, invalid)) {
                                                                    failCustomField.push(invalid);
                                                                }
                                                            }
                                                        }

                                                        if (result[2].invalid !== undefined && result[2].invalid !== null) {
                                                            for (let documentInvalid of result[2].invalid) {
                                                                if (!(failDocuments.indexOf(documentInvalid) > -1)) {
                                                                    failDocuments.push(documentInvalid);
                                                                }
                                                            }
                                                        }

                                                        createAttendeeAll(req.body.eventId, req.body.moduleId, nameModule, attendee, result[0].data, result[1].data, result[2].data)
                                                            .then((item) => {
                                                                cont++;
                                                                arrayAttendees.push(item);
                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                            })
                                                            .catch((err) => {
                                                                console.log(err);
                                                                cont++;
                                                                arrayFail.push(attendee);
                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                            })

                                                    })
                                                    .catch((errorPromiseAll) => {
                                                        console.log('=== errorPromiseAll === ', errorPromiseAll);
                                                    });

                                            } else {
                                                if (checkEmailUser.type <= 3) {
                                                    attendee.type = checkEmailUser.type;
                                                    arrayEmailsDouble.push(attendee);
                                                    cont++;
                                                } else {
                                                    // check identifier is double
                                                    checkIdentifier(req.body.eventId, attendee.identifier).then((checkStatus) => {
                                                            if (checkStatus == undefined) {
                                                                Promise.all(
                                                                        [
                                                                            validateGroups(groupModuleId, attendee.groups),
                                                                            addCustomField(req.body.moduleId, attendee),
                                                                            validateDocuments(req.body.eventId, attendee.documents, principalLanguage)
                                                                        ]
                                                                    ).then((result) => {
                                                                        /*case e-mail exist in users path and identifier do not exist, 
                                                                        only add in events=>attendees and modules=>attendees*/
                                                                        if (result[0].invalid !== undefined && result[0].invalid !== null) {
                                                                            for (let groupInvalid of result[0].invalid) {
                                                                                if (!(failGroups.indexOf(groupInvalid) > -1)) {
                                                                                    failGroups.push(groupInvalid);
                                                                                }
                                                                            }
                                                                        }

                                                                        if (result[1].invalid !== undefined && result[1].invalid !== null) {
                                                                            for (let invalid of result[1].invalid) {
                                                                                if (!verifyExistCustomField(failCustomField, invalid)) {
                                                                                    failCustomField.push(invalid);
                                                                                }
                                                                            }
                                                                        }

                                                                        if (result[2].invalid !== undefined && result[2].invalid !== null) {
                                                                            for (let documentInvalid of result[2].invalid) {
                                                                                if (!(failDocuments.indexOf(documentInvalid) > -1)) {
                                                                                    failDocuments.push(documentInvalid);
                                                                                }
                                                                            }
                                                                        }

                                                                        createAttendeeInModuleAndEvent(req.body.eventId, req.body.moduleId, nameModule, checkEmailUser, attendee, result[0].data, result[1].data, result[2].data)
                                                                            .then((item) => {
                                                                                cont++;
                                                                                arrayAttendees.push(item);
                                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                                            })
                                                                            .catch((err) => {
                                                                                console.log(err);
                                                                                cont++;
                                                                                arrayFail.push(attendee);
                                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                                            })
                                                                    })
                                                                    .catch((errorPromiseAll) => {
                                                                        console.log('=== errorPromiseAll === ', errorPromiseAll);
                                                                    });

                                                            } else if (checkStatus.moduleId == req.body.moduleId) {
                                                                Promise.all(
                                                                        [
                                                                            validateGroups(groupModuleId, attendee.groups),
                                                                            addCustomField(req.body.moduleId, attendee),
                                                                            validateDocuments(req.body.eventId, attendee.documents, principalLanguage)

                                                                        ]
                                                                    ).then((result) => {
                                                                        // update attendee infos in events=>attendes and modules=>attendees
                                                                        if (result[0].invalid !== undefined && result[0].invalid !== null) {
                                                                            for (let groupInvalid of result[0].invalid) {
                                                                                if (!(failGroups.indexOf(groupInvalid) > -1)) {
                                                                                    failGroups.push(groupInvalid);
                                                                                }
                                                                            }
                                                                        }

                                                                        if (result[1].invalid !== undefined && result[1].invalid !== null) {
                                                                            for (let invalid of result[1].invalid) {
                                                                                if (!verifyExistCustomField(failCustomField, invalid)) {
                                                                                    failCustomField.push(invalid);
                                                                                }
                                                                            }
                                                                        }

                                                                        if (result[2].invalid !== undefined && result[2].invalid !== null) {
                                                                            for (let documentInvalid of result[2].invalid) {
                                                                                if (!(failDocuments.indexOf(documentInvalid) > -1)) {
                                                                                    failDocuments.push(documentInvalid);
                                                                                }
                                                                            }
                                                                        }

                                                                        updateAttendee(req.body.eventId, req.body.moduleId, nameModule, attendee, checkStatus, result[0].data, result[1].data, result[2].data)
                                                                            .then((item) => {
                                                                                cont++;
                                                                                arrayUpdateds.push(item);
                                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                                            })
                                                                            .catch((err) => {
                                                                                console.log(err);
                                                                                cont++;
                                                                                arrayFail.push(attendee);
                                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                                            })
                                                                    })
                                                                    .catch((errorPromiseAll) => {
                                                                        console.log('=== errorPromiseAll === ', errorPromiseAll);
                                                                    });

                                                            } else if (checkStatus.moduleId !== req.body.moduleId) {
                                                                // case identifier exist, push to return to front the identifier errors
                                                                cont++;
                                                                arrayIdentifiersDouble.push(attendee);
                                                                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                            }
                                                        })
                                                        .catch((errCheckIdentifier) => {
                                                            console.log('CheckIdError: ', errCheckIdentifier);
                                                        });
                                                }


                                            }
                                        })
                                        .catch((checkUserEmailError) => {
                                            console.log('userEmailError: ', checkUserEmailError);
                                        })
                                } else if (req.body.moduleId == checkEmailAttendee.moduleId) {
                                    if (checkEmailAttendee.identifier == attendee.identifier) {
                                        Promise.all(
                                                [
                                                    validateGroups(groupModuleId, attendee.groups),
                                                    addCustomField(req.body.moduleId, attendee),
                                                    validateDocuments(req.body.eventId, attendee.documents, principalLanguage)
                                                ]
                                            ).then((result) => {
                                                // user exists in the event and is of the same module
                                                // update attendee infos in events=>attendes and modules=>attendees
                                                if (result[0].invalid !== undefined && result[0].invalid !== null) {
                                                    for (let groupInvalid of result[0].invalid) {
                                                        if (!(failGroups.indexOf(groupInvalid) > -1)) {
                                                            failGroups.push(groupInvalid);
                                                        }
                                                    }
                                                }

                                                if (result[1].invalid !== undefined && result[1].invalid !== null) {
                                                    for (let invalid of result[1].invalid) {
                                                        if (!verifyExistCustomField(failCustomField, invalid)) {
                                                            failCustomField.push(invalid);
                                                        }
                                                    }
                                                }

                                                if (result[2].invalid !== undefined && result[2].invalid !== null) {
                                                    for (let documentInvalid of result[2].invalid) {
                                                        if (!(failDocuments.indexOf(documentInvalid) > -1)) {
                                                            failDocuments.push(documentInvalid);
                                                        }
                                                    }
                                                }

                                                updateAttendee(req.body.eventId, req.body.moduleId, nameModule, attendee, checkEmailAttendee, result[0].data, result[1].data, result[2].data)
                                                    .then((item) => {
                                                        cont++;
                                                        arrayUpdateds.push(item);
                                                        response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                    })
                                                    .catch((err) => {
                                                        console.log(err);
                                                        cont++;
                                                        arrayFail.push(attendee);
                                                        response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                                    })
                                            })
                                            .catch((errorPromiseAll) => {
                                                console.log('=== errorPromiseAll === ', errorPromiseAll);
                                            });
                                    } else {
                                        arrayEmailsDouble.push(attendee);
                                        if (cont == totalAttendees - 1) {
                                            res.json({
                                                code: 200,
                                                message: 'success',
                                                result: {
                                                    success: arrayAttendees,
                                                    emailDouble: arrayEmailsDouble,
                                                    idDouble: arrayIdentifiersDouble
                                                }
                                            })
                                        }
                                        cont++;
                                    }
                                } else if (attendee.email == checkEmailAttendee.email && req.body.moduleId !== checkEmailAttendee.moduleId) {
                                    // attendee exists but is not of the same module, can not be edited
                                    arrayEmailsDouble.push(attendee);
                                    if (cont == totalAttendees - 1) {
                                        res.json({
                                            code: 200,
                                            message: 'success',
                                            result: {
                                                success: arrayAttendees,
                                                emailDouble: arrayEmailsDouble,
                                                idDouble: arrayIdentifiersDouble
                                            }
                                        })
                                    }
                                    cont++;
                                } else if (attendee.identifier == checkEmailAttendee.identifier && req.body.moduleId !== checkEmailAttendee.module) {
                                    // case identifier exist, push to return to front the identifier errors
                                    arrayIdentifiersDouble.push(attendee);
                                    response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                    cont++;
                                }
                            })
                            .catch((checkEmailAttendeeError) => {
                                console.log('checkAttendeeEmailError: ', checkEmailAttendeeError);
                            })


                    } else {
                        // identifier exist

                        if (checkStatus.moduleId == req.body.moduleId) {
                            Promise.all(
                                    [
                                        validateGroups(groupModuleId, attendee.groups),
                                        addCustomField(req.body.moduleId, attendee),
                                        validateDocuments(req.body.eventId, attendee.documents, principalLanguage)

                                    ]
                                ).then((result) => {
                                    // update attendee infos in events=>attendes and modules=>attendees
                                    if (result[0].invalid !== undefined && result[0].invalid !== null) {
                                        for (let groupInvalid of result[0].invalid) {
                                            if (!(failGroups.indexOf(groupInvalid) > -1)) {
                                                failGroups.push(groupInvalid);
                                            }
                                        }
                                    }

                                    if (result[1].invalid !== undefined && result[1].invalid !== null) {
                                        for (let invalid of result[1].invalid) {
                                            if (!verifyExistCustomField(failCustomField, invalid)) {
                                                failCustomField.push(invalid);
                                            }
                                        }
                                    }

                                    if (result[2].invalid !== undefined && result[2].invalid !== null) {
                                        for (let documentInvalid of result[2].invalid) {
                                            if (!(failDocuments.indexOf(documentInvalid) > -1)) {
                                                failDocuments.push(documentInvalid);
                                            }
                                        }
                                    }

                                    updateAttendee(req.body.eventId, req.body.moduleId, nameModule, attendee, checkStatus, result[0].data, result[1].data, result[2].data)
                                        .then((item) => {
                                            cont++;
                                            arrayUpdateds.push(item);
                                            response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                            cont++;
                                            arrayFail.push(attendee);
                                            response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                                        })
                                })
                                .catch((errorPromiseAll) => {
                                    console.log('=== errorPromiseAll === ', errorPromiseAll);
                                });

                        } else if (checkStatus.moduleId !== req.body.moduleId) {
                            // case identifier exist, push to return to front the identifier errors
                            cont++;
                            arrayIdentifiersDouble.push(attendee);
                            response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
                        }


                    }
                });
            } else {
                // e-mail existe em speakers
                // case identifier exist, push to return to front the identifier errors
                cont++;
                arrayEmailInSpeaker.push(attendee);
                response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustomField, arrayEmailInSpeaker, failDocuments);
            }
        })
    }
}

const checkIdentifier = (eventId, identifier) => {
    return new Promise((resolve, reject) => {
        admin.firestore()
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .where('identifier', '==', identifier)
            .get()
            .then((status) => {
                let find = false;
                let aux;
                status.forEach(element => {
                    aux = element.data();
                    find = true;
                });

                if (find == true) {
                    resolve(aux);
                } else {
                    resolve(undefined);
                }
            })
            .catch((err) => {
                console.log(err);
                reject(undefined);
            });
    })
}

const getGroupsLanguages = (moduleGroupId, eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let list = []

        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .doc(moduleGroupId)
            .collection('groups')
            .where('multiLanguage', '==', true)
            .get()
            .then((value) => {
                value.forEach(element => {
                    list.push(element.data())
                });

                resolve(list)
            })
    })
}

//load event
const getEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        db.collection('events')
            .doc(eventId)
            .get()
            .then((snapshot) => {
                const event = snapshot.data()
                resolve(event)
            })
    })
}

// Gets the participant's language. path events
const getLanguageAttendee = (attendeeId, eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const ref = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId)
        let language = null

        ref.get().then((snapshot) => {

            if (typeof snapshot.data() !== 'undefined') {
                language = snapshot.data().language
            }

            resolve(language)
        })
    })
}

const checkEmailExistInAttendees = (eventId, email) => {
    return new Promise((resolve, reject) => {
        admin.firestore()
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .where('email', '==', email)
            .get()
            .then((status) => {
                let find = false;
                let aux;
                status.forEach(element => {
                    aux = element.data();
                    find = true;
                });

                if (find == true) {
                    resolve(aux);
                } else {
                    resolve(undefined);
                }
            })
            .catch((err) => {
                console.log(err);
                reject(undefined);
            });
    })
}

const checkEmailExistInUsers = (email) => {
    return new Promise((resolve, reject) => {
        admin.firestore()
            .collection('users')
            .where('email', '==', email)
            .get()
            .then((status) => {
                let find = false;
                let aux;
                status.forEach(element => {
                    aux = element.data();
                    find = true;
                });

                if (find == true) {
                    resolve(aux);
                } else {
                    resolve(undefined);
                }
            })
            .catch((err) => {
                console.log(err);
                reject(undefined);
            });
    })
}

const checkEmailExistInSpeakers = (email, eventId) => {
    return new Promise((resolve, reject) => {
        admin.firestore()
            .collection('events')
            .doc(eventId)
            .collection('speakers')
            .where('email', '==', email)
            .get()
            .then((data) => {
                let docNotExist = true;
                data.forEach(doc => {
                    if (doc.exists) {
                        docNotExist = false;
                        resolve(true);
                    }
                });

                if (docNotExist == true) {
                    resolve(false);
                }
            });
    });
}

const createAttendeeAll = (eventId, moduleId, nameModule, attendeeObj, groups, customField, documents) => {
    return new Promise(async (resolve, reject) => {
        let db = admin.firestore();
        let usersRef = db.collection('users').doc();
        let eventRef = db.collection('events').doc(eventId).collection('attendees').doc(usersRef.id);
        let moduleRef = db.collection('modules').doc(moduleId).collection('attendees').doc(usersRef.id);
        let attendee = await instantiateAttendee(attendeeObj, usersRef.id, moduleId, eventId, groups, documents, nameModule);
        let user = instantiateUser(attendeeObj, usersRef.id, eventId, moduleId, true);
        attendee.checkinStatus = false

        let batch = admin.firestore().batch();

        batch.set(usersRef, user);
        batch.set(eventRef, attendee);
        batch.set(moduleRef, attendee);

        if (customField !== null) {
            for (let custom of customField) {
                if (custom !== null) {
                    let refAttendeeCustom = eventRef.collection('customFields').doc(custom.uid);
                    let refAttendeeModuleCustom = moduleRef.collection('customFields').doc(custom.uid);
                    batch.set(refAttendeeCustom, custom);
                    batch.set(refAttendeeModuleCustom, custom);
                }
            }
        }


        //get event checkins
        const checkins = await getCheckinsEvent(eventId)
        for (const checkin of checkins) {
            const checkinId = checkin.uid
            const moduleCheckinId = checkin.moduleId

            let ref = db.collection('modules')
                .doc(moduleCheckinId)
                .collection('checkins')
                .doc(checkinId).collection('attendees')
                .doc(attendee.uid)

            batch.set(ref, attendee)
        }


        batch.commit().
        then((_) => {
                resolve(attendee);
            })
            .catch((err) => {
                console.log(err);
                reject(false);
            });
    })

}

const createAttendeeInModuleAndEvent = (eventId, moduleId, nameModule, userObj, attendeeObj, groups, customField, documents) => {
    return new Promise(async (resolve, reject) => {
        let db = admin.firestore();
        let eventRef = db.collection('events').doc(eventId).collection('attendees').doc(userObj.uid);
        let moduleRef = db.collection('modules').doc(moduleId).collection('attendees').doc(userObj.uid);
        let attendee = await instantiateAttendee(attendeeObj, eventRef.id, moduleId, eventId, groups, documents, nameModule);
        attendee.checkinStatus = false

        let userRef = db.collection('users').doc(userObj.uid);
        let userAttr = instantiateUser(userObj, userObj.uid, eventId, moduleId, userObj.firstAccess);
        let batch = admin.firestore().batch();

        batch.set(eventRef, attendee);
        batch.set(moduleRef, attendee);
        if (customField !== null) {
            for (let custom of customField) {
                if (custom !== null) {
                    let refAttendeeCustom = eventRef.collection('customFields').doc(custom.uid);
                    let refAttendeeModuleCustom = moduleRef.collection('customFields').doc(custom.uid);
                    batch.set(refAttendeeCustom, custom);
                    batch.set(refAttendeeModuleCustom, custom);
                }
            }
        }

        batch.update(userRef, userAttr);


        //get event checkins
        const checkins = await getCheckinsEvent(eventId)

        for (const checkin of checkins) {
            const checkinId = checkin.uid
            const moduleCheckinId = checkin.moduleId

            let ref = db.collection('modules')
                .doc(moduleCheckinId)
                .collection('checkins')
                .doc(checkinId).collection('attendees')
                .doc(attendee.uid)

            batch.set(ref, attendee)
        }


        batch.commit().
        then((_) => {

                resolve(attendee);
            })
            .catch((err) => {
                console.log(err);
                reject(false);
            });

    })

}

const updateAttendee = async (eventId, moduleId, nameModule, attendeeObj, user, groups, customField, documents) => {
    return new Promise(async (resolve, reject) => {
        let db = admin.firestore();
        let eventRef = db.collection('events').doc(eventId).collection('attendees').doc(user.uid);
        eventRef.get().then(async (snapshot) => {
            let attendeeGetted = snapshot.data();
            let moduleRef = db.collection('modules').doc(moduleId).collection('attendees').doc(user.uid);
            attendeeObj.points = attendeeGetted.points;
            let attendee = await instantiateAttendee(attendeeObj, user.uid, moduleId, eventId, groups, documents, nameModule);
            let userRef = db.collection('users').doc(user.uid);
            let userObj = instantiateUser(user, user.uid, eventId, moduleId, (user.firstAccess) ? true : false);

            let batch = admin.firestore().batch();
            batch.update(eventRef, attendee);
            batch.update(moduleRef, attendee);
            batch.update(userRef, userObj);

            if (customField !== null) {
                for (let custom of customField) {
                    if (custom !== null) {
                        let refAttendeeCustom = eventRef.collection('customFields').doc(custom.uid);
                        let refAttendeeModuleCustom = moduleRef.collection('customFields').doc(custom.uid);
                        batch.update(refAttendeeCustom, custom);
                        batch.update(refAttendeeModuleCustom, custom);
                    }
                }
            }

            // get event checkins
            const checkins = await getCheckinsEvent(eventId)

            for (const checkin of checkins) {
                const checkinId = checkin.uid
                const moduleCheckinId = checkin.moduleId

                attendee['checkinStatus'] = await getCheckinStatus(attendee.uid, checkinId, moduleCheckinId)
                let ref = db.collection('modules').doc(moduleCheckinId).collection('checkins').doc(checkinId).collection('attendees').doc(attendee.uid)
                batch.update(ref, attendee)
            }


            batch.commit().
            then((_) => {
                    if (user.firstAccess == false) {
                        admin.auth().updateUser(user.uid, {
                            email: attendeeObj.email
                        });
                    }
                    resolve(attendee);
                })
                .catch((err) => {
                    console.log(err);
                    reject(false);
                });
        });
    })
}

const validateGroups = (groupModuleId, arrayGroupName) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []
        const invalidGroup = [];
        if (isEmpty(arrayGroupName)) {
            resolve({
                msg: 'empty',
                data: list,
                invalid: []
            })
        } else {
            if (arrayGroupName.indexOf(";") != -1) {
                arrayGroupName = arrayGroupName.split(';');
            } else {
                let aux = arrayGroupName;
                arrayGroupName = [aux];
            }
            // removes position from the array if it is an empty string ''
            let indexs = []
            for (let i = 0; i < arrayGroupName.length; i++) {
                if (arrayGroupName[i].length <= 0) {
                    indexs.push(i)
                }
            }

            // removes position 
            for (index of indexs) {
                arrayGroupName.splice(index, 1)
            }

            const total = arrayGroupName.length
            let cont = 0

            for (const id of arrayGroupName) {
                const ref = db.collection('modules').doc(groupModuleId).collection('groups').where('identifier', '==', id)

                ref.get().then((snapshot) => {

                        // atual, adicionando por indice
                        if (snapshot.size > 0) {
                            cont++
                            snapshot.forEach((childSnapshot) => {
                                const tracks = childSnapshot.data()
                                list.push(tracks)
                            })
                        } else {
                            cont++
                            invalidGroup.push(id);
                        }

                        if (cont >= total) {
                            resolve({
                                msg: 'ok',
                                data: list,
                                invalid: invalidGroup
                            })
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            }
        }
    });
}


const validateDocuments = (eventId, arrayDocuments, language) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = {}
        const invalidDocument = [];
        if (isEmpty(arrayDocuments)) {
            resolve({
                msg: 'empty',
                data: list,
                invalid: []
            })
        } else {
            if (arrayDocuments.indexOf(";") != -1) {
                arrayDocuments = arrayDocuments.split(';');
            } else {
                let aux = arrayDocuments;
                arrayDocuments = [aux];
            }
            // removes position from the array if it is an empty string ''
            let indexs = []
            for (let i = 0; i < arrayDocuments.length; i++) {
                if (arrayDocuments[i].length <= 0) {
                    indexs.push(i)
                }
            }

            // removes position 
            for (index of indexs) {
                arrayDocuments.splice(index, 1)
            }

            const total = arrayDocuments.length
            let cont = 0


            for (const id of arrayDocuments) {
                const ref = db.collection('events').doc(eventId).collection('documents').where('identifier', '==', id)

                ref.get().then((snapshot) => {
                        if (snapshot.size > 0) {
                            cont++
                            snapshot.forEach((childSnapshot) => {
                                const document = childSnapshot.data()
                                const documentId = document.uid
                                list[documentId] = document;
                            })

                        } else {
                            cont++
                            invalidDocument.push(id);
                        }

                        if (cont >= total) {
                            resolve({
                                msg: 'ok',
                                data: list,
                                invalid: invalidDocument
                            })
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    })
            }
        }
    });
}

const updateCustomField = (moduleId, attendeeObj) => {
    return new Promise((resolve) => {
        //Adiciona os custom Fields ao novo attendee
        let db = admin.firestore();
        db.collection('modules').doc(moduleId).collection('customFields').get().then(async (data) => {
            let size = data.size;
            let cont = 0;
            let customArray = [];
            let invalidOption = [];
            let attendeeCustomSize = attendeeObj.customField.length;
            if (size >= 1) {
                data.forEach(async (doc) => {
                    let custom = doc.data();

                    custom = Object.assign({}, custom);

                    for (let customFieldAttendee of attendeeObj.customField) {
                        if (customFieldAttendee !== null) {
                            if (customFieldAttendee.uid == custom.uid && customFieldAttendee.value !== undefined) {
                                if (customFieldAttendee.type == 'text') {
                                    custom.value = customFieldAttendee.value;
                                    customArray.push(custom);
                                    if (cont == attendeeCustomSize - 1) {
                                        resolve({
                                            msg: 'ok',
                                            data: customArray,
                                            invalid: []
                                        });
                                    }
                                    cont++;
                                } else if (customFieldAttendee.type == 'select') {
                                    await db.collection('modules').doc(moduleId).collection('customFields').doc(customFieldAttendee.uid).collection('options')
                                        .get()
                                        .then((dataOpt) => {
                                            let optionsSize = dataOpt.size;
                                            let contOptions = 0;
                                            let find = false;
                                            custom.value = null;
                                            dataOpt.forEach(elOpt => {
                                                let customFieldOptions = elOpt.data();

                                                if (customFieldOptions.answer == customFieldAttendee.value) {
                                                    custom.value = customFieldOptions.uid;
                                                    customArray.push(custom);
                                                    find = true;
                                                }
                                                if (contOptions == optionsSize - 1) {
                                                    if (find == false) {
                                                        if (customFieldAttendee.value !== undefined && customFieldAttendee.value !== null) {
                                                            invalidOption.push({
                                                                custom: custom.name,
                                                                value: customFieldAttendee.value
                                                            });
                                                        }
                                                        customArray.push(custom);
                                                    }
                                                    if (cont == attendeeCustomSize - 1) {
                                                        resolve({
                                                            msg: 'ok',
                                                            data: customArray,
                                                            invalid: invalidOption
                                                        });
                                                    }
                                                    cont++;
                                                }
                                                contOptions++;
                                            });
                                        })
                                        .catch((errOpt) => {
                                            console.log('== DEU ERRO ==', errOpt);
                                        });
                                }

                            }
                        } else {
                            if (cont == attendeeCustomSize - 1) {
                                resolve({
                                    msg: 'ok',
                                    data: customArray,
                                    invalid: invalidOption
                                });
                            }
                            cont++;
                        }

                    }

                });
            } else {
                resolve({
                    msg: 'error',
                    data: [],
                    invalid: []
                });
            }

        })
    })
}

const addCustomField = (moduleId, attendeeObj) => {
    return new Promise(async (resolve) => {
        //Adiciona os custom Fields ao novo attendee
        let db = admin.firestore();

        let cont = 0;
        let customArray = [];
        let invalidOption = [];
        let attendeeCustomSize = attendeeObj.customField.length;

        if (attendeeObj.customField.length >= 1) {
            for (let customFieldAttendee of attendeeObj.customField) {
                if (customFieldAttendee !== null) {
                    // if (customFieldAttendee.uid == custom.uid && customFieldAttendee.value !== undefined) {
                    if (customFieldAttendee.type == 'text') {
                        // custom.value = customFieldAttendee.value;
                        customArray.push(customFieldAttendee);
                        cont++;
                        if (cont == attendeeCustomSize) {
                            resolve({
                                msg: 'ok',
                                data: customArray,
                                invalid: []
                            });
                        }
                    } else {
                        await db.collection('modules').doc(moduleId).collection('customFields').doc(customFieldAttendee.uid).collection('options')
                            .get()
                            .then((dataOpt) => {
                                let optionsSize = dataOpt.size;
                                let contOptions = 0;
                                let find = false;
                                dataOpt.forEach(elOpt => {
                                    let customFieldOptions = elOpt.data();
                                    if (customFieldOptions.answer == customFieldAttendee.value) {
                                        // custom.value = customFieldOptions.uid;
                                        customFieldAttendee.value = customFieldOptions.uid;
                                        customArray.push(customFieldAttendee);
                                        find = true;
                                    }
                                    if (contOptions == optionsSize - 1) {
                                        if (find == false) {

                                            if (customFieldAttendee.value !== undefined && customFieldAttendee.value !== null && customFieldAttendee.value !== '') {
                                                invalidOption.push({
                                                    custom: customFieldAttendee.name,
                                                    value: customFieldAttendee.value
                                                });
                                            }
                                            customArray.push(customFieldAttendee);
                                        }
                                        cont++;
                                        if (cont == attendeeCustomSize) {
                                            resolve({
                                                msg: 'ok',
                                                data: customArray,
                                                invalid: invalidOption
                                            });
                                        }
                                    }
                                    contOptions++;
                                });
                            })
                            .catch((err) => {
                                console.log(err);
                            })
                    }
                } else {
                    cont++;
                    if (cont == attendeeCustomSize) {
                        resolve({
                            msg: 'ok',
                            data: customArray,
                            invalid: invalidOption
                        });
                    }
                }
            }
        } else {
            resolve({
                msg: 'empty',
                data: customArray,
                invalid: invalidOption
            });
        }
    })

}

function instantiateAttendee(data, userId, moduleId, eventId, attendeeGroupArray, attendeeDocumentArray, nameModule) {
    return new Promise(async (resolve) => {
        let dataFormat = {
            uid: userId,
            name: data.name,
            queryName: "",
            type: data.type,
            email: "",
            language: principalLanguage,
            description: {
                PtBR: "",
                EnUS: "",
                EsES: "",
                FrFR: "",
                DeDE: ""
            },
            photoUrl: "",
            company: "",
            title: {
                PtBR: "",
                EnUS: "",
                EsES: "",
                FrFR: "",
                DeDE: ""
            },
            emailRecovery: "",
            moduleId: moduleId,
            module: {
                uid: moduleId,
                name: nameModule
            },
            eventId: eventId,
            groups: {},
            documents: {},
            identifier: "",
            createdAt: Date.now() / 1000 | 0,
            points: 0,
            website: "",
            facebook: "",
            instagram: "",
            linkedin: "",
            twitter: "",
            edited_profile: false
        };

        if (data.name != undefined) {
            dataFormat.name = data.name;
            dataFormat.queryName = data.name.toUpperCase();
        }

        if (data.email != undefined) {
            dataFormat.email = data.email;
        }

        if (data.description.PtBR != undefined) {
            dataFormat.description.PtBR = data.description.PtBR;
        }

        if (data.description.EnUS != undefined) {
            dataFormat.description.EnUS = data.description.EnUS;
        }
        if (data.description.EsES != undefined) {
            dataFormat.description.EsES = data.description.EsES;
        }
        if (data.description.FrFR != undefined) {
            dataFormat.description.FrFR = data.description.FrFR;
        }
        if (data.description.DeDE != undefined) {
            dataFormat.description.DeDE = data.description.DeDE;
        }

        if (data.photoUrl != undefined) {
            dataFormat.photoUrl = data.photoUrl;
        }

        if (data.company != undefined) {
            dataFormat.company = data.company;
        }

        if (data.title.PtBR != undefined) {
            dataFormat.title.PtBR = data.title.PtBR;
        }
        if (data.title.EnUS != undefined) {
            dataFormat.title.EnUS = data.title.EnUS;
        }
        if (data.title.EsES != undefined) {
            dataFormat.title.EsES = data.title.EsES;
        }
        if (data.title.FrFR != undefined) {
            dataFormat.title.FrFR = data.title.FrFR;
        }
        if (data.title.DeDE != undefined) {
            dataFormat.title.DeDE = data.title.DeDE;
        }

        if (data.facebook != undefined) {
            dataFormat.facebook = data.facebook;
        }

        if (data.linkedin != undefined) {
            dataFormat.linkedin = data.linkedin;
        }

        if (data.twitter != undefined) {
            dataFormat.twitter = data.twitter;
        }

        if (data.instagram != undefined) {
            dataFormat.instagram = data.instagram;
        }

        if (data.website != undefined) {
            dataFormat.website = data.website;
        }

        if (data.emailRecovery != undefined) {
            dataFormat.emailRecovery = data.emailRecovery;
        }

        if (data.identifier != undefined) {
            dataFormat.identifier = data.identifier;
        }

        if (data.points != undefined) {
            dataFormat.points = data.points;
        }

        if (data.groups != undefined) {
            for (let index in attendeeGroupArray) {
                const group = attendeeGroupArray[index];
                dataFormat.groups[group['uid']] = Object.assign({}, group);
            }
        }

        if (attendeeDocumentArray != undefined) {
            dataFormat.documents = attendeeDocumentArray
        }

        if (data.edited_profile != undefined) {
            dataFormat.edited_profile = data.edited_profile;
        }

        // language attendee
        const language = await getLanguageAttendee(userId, eventId)

        if (language) {
            dataFormat.language = language
        }

        resolve(dataFormat);
    })
}

function instantiateUser(data, userId, eventId, moduleId, firstAccess) {
    let dataFormat = {
        uid: userId,
        name: data.name,
        queryName: "",
        type: data.type,
        email: "",
        language: "",
        description: "",
        photoUrl: "",
        company: "",
        title: "",
        emailRecovery: "",
        events: data.events,
        attendeeModules: data.attendeeModules,
        firstAccess: firstAccess,
        language: data.language
    };

    dataFormat.events = admin.firestore.FieldValue.arrayUnion(eventId);

    dataFormat.attendeeModules = admin.firestore.FieldValue.arrayUnion(moduleId);

    if (data.name != undefined) {
        dataFormat.name = data.name;
        dataFormat.queryName = data.name.toUpperCase();
    }

    if (data.email != undefined) {
        dataFormat.email = data.email;
    }

    if (data.language != undefined) {
        dataFormat.language = data.language;
    }

    if (data.description != undefined) {
        dataFormat.description = data.description;
    }

    if (data.photoUrl != undefined) {
        dataFormat.photoUrl = data.photoUrl;
    }

    if (data.company != undefined) {
        dataFormat.company = data.company;
    }

    if (data.title != undefined) {
        dataFormat.title = data.title;
    }

    if (data.emailRecovery != undefined) {
        dataFormat.emailRecovery = data.emailRecovery;
    }

    if (data.language != undefined) {
        dataFormat.language = data.language;
    }

    return dataFormat;
}

function isEmpty(obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function verifyExistCustomField(array, obj) {
    let indexValue;
    let indexName;
    for (let lang in obj) {
        if (obj[lang] !== '') {
            indexValue = array.findIndex(val => val.value == obj.value);
            indexName = array.findIndex(val => val.custom[lang] == obj.custom[lang]);
        }
    }

    if (indexValue < 0 || indexName < 0) {
        //não existe
        return false
    } else {
        // existe
        return true
    }
}

function response(cont, totalAttendees, res, arrayAttendees, arrayEmailsDouble, arrayIdentifiersDouble, arrayUpdateds, arrayFail, failGroups, failCustom, arrayEmailSpeaker, failDocuments) {
    if (cont == totalAttendees) {
        res.json({
            code: 200,
            message: 'success',
            result: {
                success: arrayAttendees,
                emailDouble: arrayEmailsDouble,
                idDouble: arrayIdentifiersDouble,
                updateds: arrayUpdateds,
                fail: arrayFail,
                failGroups: failGroups,
                failCustomField: failCustom,
                emailInSpeaker: arrayEmailSpeaker,
                failDocuments: failDocuments
            }
        });
    }
}

/**
 * get participant checkin status.  
 * @param {string} attendeeId  
 * @param {string} checkinId  
 * @param {string} checkinModuleId  
 * @returns string
 */

const getCheckinStatus = (attendeeId, checkinId, checkinModuleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .doc(attendeeId)
            .get()
            .then((snapshot) => {
                if (snapshot.exists && typeof snapshot.data().checkinStatus !== 'undefined' || snapshot.data().checkinStatus !== null)
                    resolve(snapshot.data().checkinStatus)

                resolve(false)
            })
    })
}

/**
 * get all checks in of the event.  
 * @param {string} eventId  
 * @returns Promise<Checkin[]>
 */

const getCheckinsEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let checkins = []

        // get checkins module
        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', TypeModule.CHECKIN)
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve([])

                // get all module checkins.
                snapshot.forEach((element) => {
                    const module = element.data()
                    const moduleId = module.uid

                    db.collection('modules')
                        .doc(moduleId)
                        .collection('checkins')
                        .get()
                        .then((childSnapshot) => {
                            childSnapshot.forEach((element) => {
                                const checkin = element.data()
                                checkins.push(checkin)
                            })

                            resolve(checkins)
                        })
                })
            })
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.firestore
    .document('events/{eventId}/attendees/{attendeeId}').onCreate((snap, context) => {
        return new Promise((resolve) => {
            let attendee = snap.data();
            // let eventId = context.params.eventId;
            admin.firestore()
                .collection('users')
                .doc(attendee.uid)
                .get()
                .then((snapshot) => {
                    let user = snapshot.data();

                    if (typeof user.events !== 'undefined') {
                        for (let eventId of user.events) {
                            let refEventAttendee = admin.firestore().collection('events').doc(eventId).collection('attendees').doc(user.uid);
                            refEventAttendee.update({ firstAccess: user.firstAccess });
                        }
                    }

                    if (typeof user.attendeeModules !== 'undefined') {
                        for (let moduleId of user.attendeeModules) {
                            let refModuleAttendee = admin.firestore().collection('modules').doc(moduleId);
                            refModuleAttendee.update({ firstAccess: user.firstAccess });
                        }
                    }
                    resolve(true);
                });
        })
    });
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
// const JSONC = require('jsoncomp');
// const XLSX  =  require ( ' xlsx ' );
// const lzString = require('../../utilities/lz-string');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

const cors = require("cors")({
    origin: true
});


exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {

        // let listCustomField = [];
        // let principalEventLang = req.query.principalEventLang; //falta passar
        // let languages = req.body.languages; //falta passar
        // let eventLanguage = req.body.eventLanguage;
        console.log('##################################################################################')
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let typeOrder = req.query.typeOrder;
        let refAttendees;
        let attendeesSize;
        let firstGet = req.body.firstGet;
        let lastDocument = req.body.lastDocument;
        let refCustomField = db.collection('modules').doc(moduleId).collection('customFields');
        let refFieldsCustom = db.collection('modules').doc(moduleId);

        switch (typeOrder) {
            case 'asc': //a-z
                if (firstGet) {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('queryName', 'asc')
                        .limit(500)
                } else {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('queryName', 'asc')
                        .startAfter(lastDocument.queryName)
                        .limit(500)
                }

                break;

            case 'desc': //z-a
                if (firstGet) {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('queryName', 'desc')
                        .limit(500)
                } else {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('queryName', 'desc')
                        .startAfter(lastDocument.queryName)
                        .limit(500)
                }
                break;

            case 'oldest': //antiho-recente

                if (firstGet) {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('createdAt', 'asc')
                        .limit(500)
                } else {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('createdAt', 'asc')
                        .startAfter(lastDocument.queryName)
                        .limit(500)
                }

                break;

            case 'recent': //recente-antigo
                if (firstGet) {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('createdAt', 'desc')
                        .limit(500)
                } else {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('createdAt', 'desc')
                        .startAfter(lastDocument.queryName)
                        .limit(500)
                }

                break;

            case 'id': //ID
                if (firstGet) {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('identifier')
                        .limit(500)
                } else {
                    refAttendees = db.collection('modules').doc(moduleId).collection('attendees')
                        .orderBy('identifier')
                        .startAfter(lastDocument.queryName)
                        .limit(500)
                }

                break;
        }

        refFieldsCustom
            .get()
            .then((fieldsCustomValue) => {
                let aux = fieldsCustomValue.data();
                let fieldsCustom = aux['fieldsCustom'];

                refAttendees
                    .get()
                    .then(
                        (dataAttendee) => {
                            let listAttendees = [];
                            let contAttendee = 0;
                            attendeesSize = dataAttendee.size;
                            lastDocument = dataAttendee.docs[dataAttendee.docs.length - 1].data();
                            if (attendeesSize >= 1) {
                                dataAttendee
                                    .forEach(
                                        (doc) => {
                                            let attendee = doc.data();
                                            attendee.customField = [];
                                            db
                                                .collection('modules')
                                                .doc(moduleId)
                                                .collection('attendees')
                                                .doc(attendee.uid)
                                                .collection('customFields')
                                                .get()
                                                .then(async (dataAttendeeFields) => {
                                                    let customArray = [];
                                                    dataAttendeeFields.forEach(element => {
                                                        customArray.push(element.data());
                                                    });

                                                    if (customArray.length > 0) {
                                                        let listCustomField = [];

                                                        for (let i = 0; i < customArray.length; i++) {
                                                            //pega o id do custom field
                                                            let id = customArray[i].uid;
                                                            //com base no id, verifica a posição do custom field que está armazenada no fieldOptionsCustom
                                                            let position = fieldsCustom[id].order;
                                                            //insere o custom field na sua posição
                                                            listCustomField[position] = customArray[i];
                                                        }
                                                        let contCustomField = 0;
                                                        for (let attendeeField of listCustomField) {
                                                            if (attendeeField.type == 'text') {
                                                                attendee.customField[contCustomField] = attendeeField;

                                                                if (contCustomField == listCustomField.length - 1) {
                                                                    listAttendees.push(attendee);
                                                                    contAttendee++;
                                                                }
                                                                contCustomField++;

                                                                if (contAttendee == attendeesSize && contCustomField == listCustomField.length) {
                                                                    response(listAttendees, typeOrder, lastDocument, res)
                                                                }

                                                            } else if (attendeeField.type == 'select') {
                                                                await refCustomField
                                                                    .doc(attendeeField.uid)
                                                                    .collection('options')
                                                                    .get()
                                                                    .then((dataCustomFieldOptions) => {
                                                                        let contOptions = 0;
                                                                        dataCustomFieldOptions.forEach((elementCustomFieldOptions) => {
                                                                            let options = elementCustomFieldOptions.data();
                                                                            if (attendeeField.value == '' || attendeeField.value == null || attendeeField == undefined) {
                                                                                attendeeField.value = '';
                                                                                attendee.customField[contCustomField] = attendeeField;
                                                                            } else if (options.uid == attendeeField.value) {
                                                                                attendeeField.value = options.answer;
                                                                                attendee.customField[contCustomField] = attendeeField;
                                                                            }
                                                                            if (contOptions == dataCustomFieldOptions.size - 1) {
                                                                                contCustomField++;
                                                                            }
                                                                            contOptions++;

                                                                            if (contCustomField == listCustomField.length - 1 && contOptions == dataCustomFieldOptions.size - 1) {
                                                                                listAttendees.push(attendee);
                                                                                contAttendee++;
                                                                            }
                                                                            if (contAttendee == attendeesSize && contCustomField == listCustomField.length && contOptions == dataCustomFieldOptions.size) {
                                                                                response(listAttendees, typeOrder, lastDocument, res);
                                                                            }


                                                                        });

                                                                    })
                                                                    .catch((e) => {
                                                                        console.error('error => ', e);
                                                                    })
                                                            }
                                                        }
                                                    } else {
                                                        listAttendees.push(attendee);
                                                        if (contAttendee == attendeesSize - 1) {
                                                            response(listAttendees, typeOrder, lastDocument, res);
                                                        }
                                                        contAttendee++;
                                                    }

                                                    // if (dataAttendeeFields.size > 0) {
                                                    //     let contCustomField = 0;
                                                    //     dataAttendeeFields.forEach((elementAttendeeField) => {
                                                    //         let attendeeField = elementAttendeeField.data();


                                                    //     });
                                                    // }
                                                })
                                                .catch((error) => {
                                                    console.log('error => ', error);
                                                    res.json({
                                                        code: 404,
                                                        message: 'error',
                                                        result: null
                                                    });
                                                })
                                        }
                                    )
                            } else {
                                response(listAttendees, typeOrder, lastDocument, res);
                            }
                        })
                    .catch((e) => {
                        console.error('error => ', e);
                    })
            })
            .catch((e) => {
                console.error('error => ', e);
            })
    });
});

reorderAttendees = (order, attendees) => {
    return new Promise((resolve) => {
        let aux = attendees;
        attendees = [];
        switch (order) {
            case 'asc':
                attendees = aux.sort(function (a, b) {
                    if (a['queryName'] < b['queryName']) {
                        return -1;
                    }
                    if (a['queryName'] > b['queryName']) {
                        return 1;
                    }
                    return 0;
                });
                break;

            case 'desc':
                attendees = aux.sort(function (a, b) {
                    if (a['queryName'] < b['queryName']) {
                        return 1;
                    }
                    if (a['queryName'] > b['queryName']) {
                        return -1;
                    }
                    return 0;
                })
                break;

            case 'recent':
                attendees = aux.sort((a, b) => b.createdAt - a.createdAt);
                break;

            case 'oldest':
                attendees = aux.sort((a, b) => a.createdAt - b.createdAt);
                break;

            case 'id':
                attendees = aux.sort(function (a, b) {
                    if (a['identifier'] < b['identifier']) {
                        return -1;
                    }
                    if (a['identifier'] > b['identifier']) {
                        return 1;
                    }
                    return 0;
                });
                break;
        }
        resolve(attendees);
    });
}

function response(arrayAttendees, order, lastDocument, res) {
    reorderAttendees(order, arrayAttendees)
        .then((attendees) => {
            // let compressData = JSONC.compress(JSON.stringify(attendees))                                                              
            // exportAllAttendees(attendees, listCustomField, principalEventLang, languages, eventLanguage)
            console.log('==== RESPONSE ====')
            res.json({
                code: 200,
                message: 'success',
                result: attendees,
                lastDocument: lastDocument
            });
        })
        .catch((e) => {
            console.error('error => ', e);
        })
}
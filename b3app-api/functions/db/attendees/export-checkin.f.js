const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const GLOBAL_VISION = require("../../utilities/enums/type-vision-checkin").GLOBAL_VISION

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.body.eventId !== null) {
            let db = admin.firestore();

            let eventId = req.body.eventId;
            let moduleId = req.body.moduleId;
            let checkinId = req.body.checkinId;
            let typeVision = req.body.typeVision;
            let groups = req.body.groups;

            // Checks the type of view of the checkin.

            if (typeVision === GLOBAL_VISION) {
                loadCheckinGlobal(res, checkinId, moduleId)
            } else {
                loadCheckinByGroup(res, groups, checkinId, moduleId)
            }
        } else {
            res.json('fired!');
        }

    });
});

const loadCheckinGlobal = (res, checkinId, moduleId) => {
    let db = admin.firestore();
    let attendees = [];

    //  Loads event attendees.
    const ref = db
        .collection('modules')
        .doc(moduleId)
        .collection('checkins')
        .doc(checkinId)
        .collection('attendees')
        .orderBy('queryName', 'asc')
        .get()

    ref.then((snapshot) => {
        snapshot.forEach((element) => {
            const attendee = element.data()
            attendees.push(attendee)
        })

        response(res, 'success', 200, attendees);
    }).catch((err) => {
        response(res, 'error', 404, attendees);
    })
}

//loads group view check-in.
const loadCheckinByGroup = (res, groups, checkinId, moduleId) => {
    let db = admin.firestore();

    if (groups.length > 0) {
        let contGroup = 0
        const attendees = []


        // get the attendees of each group
        for (const group of groups) {
            const groupId = `groups.${group.uid}`
            let cont = 0

            // load group atttendees.
            const ref = db
                .collection('modules')
                .doc(moduleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees')
                .orderBy(groupId, 'asc')
                .get()

            ref.then((snapshot) => {
                if (snapshot.size > 0) {
                    // withdraws replay of attendees
                    snapshot.forEach((element) => {
                        const attendee = element.data()

                        // Checks if the participant is already in the array.
                        const pos = attendees.map(function(e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1) {
                            attendees.push(attendee)
                        }

                        cont++

                        // the group participants were sued.
                        if (cont >= snapshot.size) {
                            contGroup++
                        }

                        // participants from all groups were sued.
                        if (contGroup >= groups.length) {
                            response(res, 'success', 200, attendees);
                        }
                    })
                } else {
                    contGroup++

                    // participants from all groups were sued.
                    if (contGroup >= groups.length) {
                        response(res, 'success', 200, attendees);
                    }
                }
            })
        }
    } else {
        response(res, 'success', 200, attendees);
    }
}


function response(res, msg, code, attendees) {
    res.json({
        code: code,
        message: msg,
        result: attendees
    });
}
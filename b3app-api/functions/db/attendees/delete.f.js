/**
 * Trigger responsible for monitoring and removing  attendees.
 * 
 */

// imports
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

// monitors the deletion of modules.
exports = module.exports = functions.firestore.document('modules/{moduleId}/attendees/{attendeeId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        console.info('dbAttendeesDelete')

        const db = admin.firestore();
        const attendeeRemove = snap.data()
            // get params
        const attendeeId = attendeeRemove.uid
        const moduleId = attendeeRemove.moduleId
        const eventId = attendeeRemove.eventId

        // delete the participant from the event path.
        db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).delete()

        await removeSessions(attendeeId, moduleId, eventId) // remove sessions from attendee
        await removeCustomFields(attendeeId, moduleId, eventId) // remove custom-fields from attendee

        // get attendee user account
        const user = await getUser(attendeeId)
        console.info(user)

        // get all event checkins.
        const checkins = await getCheckinsEvent(eventId)
        console.info(checkins)
        for (const checkin of checkins) {
            const moduleCheckinId = checkin.moduleId
            const checkinId = checkin.uid

            // deleting participant at checkin.
            db.collection('modules').doc(moduleCheckinId).collection('checkins').doc(checkinId).collection('attendees').doc(attendeeId).delete()
        }


        // pulls the event uid from the user account.
        let totalEvents = 0

        if (typeof user.events !== 'undefined') {
            const indexEventUid = user.events.indexOf(eventId)
            user.events.splice(indexEventUid, 1);
            totalEvents = user.events.length
        }

        if (typeof user.attendeeModules !== 'undefined') {
            const indexModuleUid = user.attendeeModules.indexOf(moduleId)
            user.attendeeModules.splice(indexModuleUid, 1)
        }

        // checks the number of attendee events. 
        // if >0 => update account or if <=0 => remove account

        if (totalEvents > 0) {
            db.collection('users').doc(attendeeId).update(user)
        } else {
            //remove o usuário da autenticação
            admin.auth().deleteUser(attendeeId).then(() => {
                db.collection('users').doc(attendeeId).delete()
            }).catch((err) => {
                // caso a conta não exista na autenticação.
                if (err.code === 'auth/user-not-found') {
                    db.collection('users').doc(attendeeId).delete()
                } else {
                    db.collection('users').doc(attendeeId).update(user)
                }
            })
        }

        resolve(true)
    })
})

// remove sessions from attendee
const removeSessions = (attendeeId, moduleAttendeeId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // get the personal agenda module of the event.
        const modulePersonal = await eventCalendarModule(eventId)

        if (modulePersonal) {
            const modulePersonalAgendaId = modulePersonal.uid

            // get sessions
            db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).collection('sessions').get().then((snapshot) => {
                snapshot.forEach((element) => {
                    const session = element.data()
                    const sessionId = session.uid
                    const moduleScheduleId = session.moduleId


                    // removes session from the personal calendar module.
                    db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId).delete()

                    // remove participant session
                    db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId).delete()
                    db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId).delete()

                    // remove session participant
                    db.collection('events').doc(eventId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId).delete()
                    db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId).delete()
                })

                // removes the document with the personal calendar participant information
                db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).delete()

                resolve(true)
            })
        } else {
            resolve(true)
        }
    })
}


// receives the event uid and returns the event's agenda module
eventCalendarModule = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const PERSONALSCHEDULE = typeModules.PERSONALSCHEDULE

        const ref = db.collection('events').doc(eventId).collection('modules');

        ref.where('type', '==', PERSONALSCHEDULE).get().then((snapshot) => {
            if (snapshot.size > 0) {
                snapshot.forEach((childSnapshot) => {
                    const module = childSnapshot.data()
                    resolve(module)
                })
            } else {
                resolve(null)
            }
        })

    })
}

// remove custom-fields from attendee
const removeCustomFields = (attendeeId, moduleAttendeeId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore();

        // list custom fields
        db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('customFields').get().then((snapshot) => {
            snapshot.forEach((element) => {
                const custom = element.data()
                const customId = custom.uid

                db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('customFields').doc(customId).delete()
                db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).collection('customFields').doc(customId).delete()
            })

            resolve(true)
        })
    })
}

// get attendee user account
const getUser = (userId) => {
    return new Promise((resolve) => {
        const db = admin.firestore();

        db.collection('users').doc(userId).get().then((snapshot) => {
            const user = snapshot.data()
            resolve(user)
        })

    })
}

/**
 * get all checks in of the event.  
 * @param eventId  
 * @returns Checkins[]
 */

const getCheckinsEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let checkins = []

        // get checkins module
        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', typeModules.CHECKIN)
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve([])

                // get all module checkins.
                snapshot.forEach((element) => {
                    const module = element.data()
                    const moduleId = module.uid

                    db.collection('modules')
                        .doc(moduleId)
                        .collection('checkins')
                        .get()
                        .then((childSnapshot) => {
                            childSnapshot.forEach((element) => {
                                const checkin = element.data()
                                checkins.push(checkin)
                            })

                            resolve(checkins)
                        })
                })
            })
    })
}
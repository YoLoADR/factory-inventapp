const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.firestore
    .document('events/{eventId}/notifications/{notificationId}').onDelete((snap, context) => {
        return new Promise((resolve, reject) => {
            let notification = snap.data();
            let eventId = context.params.eventId;
            admin.firestore()
                .collection('events')
                .doc(eventId)
                .get()
                .then((response) => {
                    let event = response.data();
                    let apiId;
                    let appId;
                    if (event.notification_api_id && event.notification_app_id) {
                        apiId = event.notification_api_id;
                        appId = event.notification_app_id;
                    } else {
                        apiId = configServer.notification_api_id;
                        appId = configServer.notification_app_id;
                    }

                    const headers = {
                        "Content-Type": "application/json; charset=utf-8",
                        "Authorization": "Basic " + apiId
                    }

                    // declare options, with host, port, path, and use headers
                    const options = {
                        host: "onesignal.com",
                        port: 443,
                        path: `/api/v1/notifications/${notification.notificationId}?app_id=${appId}`,
                        method: "DELETE",
                        headers: headers
                    }

                    let https = require('https');
                    let requisition = https.request(options, function(res) {
                        console.info('*** returned: ', res);
                        res.on('data', (chunck) => {
                            console.log('data chucnk: ', chunck);
                            resolve(true);
                        });
                        res.on('end', () => {
                            console.log('no more data in response');
                        });
                    });
                    requisition.on('error', function(e) {
                        console.error("*** ERROR: ", e);
                        reject(false);
                    });
                    requisition.end();
                });
        })
    });
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

exports = module.exports = functions.firestore
    .document('events/{eventId}/notifications/{notificationId}').onCreate((snap, context) => {
        // observe notifications path
        let data = snap.data();
        let db = admin.firestore();
        let eventId = context.params.eventId;

        // case send to all
        if (data.send_to == 'all') {
            Promise.all([
                    getEventApiId(eventId).catch((e) => {
                        console.error('error get event', e);
                        return e
                    }),
                    getAllAttendeesPlayerIds(db, data.eventId).catch((e) => {
                        console.error('error attendee: ', e);
                        return e
                    }),
                    getAllSpeakersPlayerIds(db, data.eventId).catch((e) => {
                        console.error('error speaker: ', e);
                        return e
                    }),
                ])
                .then(async (response) => {
                    console.log("Response: ", response);
                    let api_id = response[0][0];
                    let app_id = response[0][1];
                    let attendeesPlayerIds = response[1];
                    let speakersPlayerIds = response[2];
                    let player_ids = attendeesPlayerIds.concat(speakersPlayerIds);
                    let notification = instatiateNotification(data, player_ids, api_id, app_id);
                    await sendNotification(db, data, notification);
                })
                .catch((e) => {
                    console.error(e);
                });
        } else {
            // send to groups
            Promise.all([
                    getEventApiId(eventId).catch((e) => {
                        console.error('error get event', e);
                        return e
                    }),
                    getAttendeesGroupsPlayerIds(db, data.eventId, data.groups_ids),
                    getSpeakersGroupsPlayerIds(db, data.eventId, data.groups_ids)
                ])
                .then(async (response) => {
                    let api_id = response[0][0];
                    let app_id = response[0][1];
                    let attendeesPlayerIds = response[1];
                    let speakersPlayerIds = response[2];
                    let player_ids = attendeesPlayerIds.concat(speakersPlayerIds);

                    let notification = instatiateNotification(data, player_ids, api_id, app_id);
                    await sendNotification(db, data, notification);
                })
                .catch((e) => {
                    console.error(e);
                });
        }
        return false;
    });


const getEventApiId = (eventId) => {
    return new Promise((resolve, reject) => {
        admin.firestore()
            .collection('events')
            .doc(eventId)
            .get()
            .then((response) => {
                let event = response.data();
                if (event.notification_api_id && event.notification_app_id) {
                    let aux = [event.notification_api_id, event.notification_app_id]
                    resolve(aux);
                } else {
                    let auxElse = [configServer.notification_api_id, configServer.notification_app_id]
                    resolve(auxElse);
                }
            });
    });
}

const getAllAttendeesPlayerIds = (db, eventId) => {
    return new Promise((resolve, reject) => {
        let playerIds = [];

        admin.firestore()
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .where('havePushId', '==', true)
            .get()
            .then((response) => {
                let size = response.size;
                let contAttendee = 0;
                if (size >= 1) {
                    response.forEach(element => {
                        let attendee = element.data();
                        playerIds.push(attendee['notification']['userId']);
                        contAttendee++;
                    });
                    if (contAttendee == size) {
                        resolve(playerIds);
                    }
                } else {
                    resolve(playerIds)
                }
            })
    });
}

const getAttendeesGroupsPlayerIds = (db, eventId, groups) => {
    return new Promise((resolve, reject) => {
        let playerIds = [];
        let totalGroups = groups.length;
        let contGroups = 0;
        for (let group of groups) {
            db
                .collection('events')
                .doc(eventId)
                .collection('attendees')
                .where('havePushId', '==', true)
                .where(`groups.${group}.uid`, '==', group)
                .get()
                .then((response) => {
                    let size = response.size;
                    if (size >= 1) {
                        response.forEach(element => {
                            let attendee = element.data();
                            playerIds.push(attendee['notification']['userId']);
                        });

                        if (contGroups == totalGroups - 1) {
                            resolve(playerIds);
                        }
                        contGroups++;

                    } else {
                        // not have attendees with this group
                        if (contGroups == totalGroups - 1) {
                            resolve(playerIds);
                        }
                        contGroups++;
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        }
    });
}

const getAllSpeakersPlayerIds = (db, eventId) => {
    return new Promise((resolve, reject) => {
        let playerIds = [];
        admin.firestore()
            .collection('events')
            .doc(eventId)
            .collection('speakers')
            .where('havePushId', '==', true)
            .get()
            .then((response) => {
                let size = response.size;
                let contSpeakers = 0;
                if (size >= 1) {
                    response.forEach(element => {
                        let speaker = element.data();
                        playerIds.push(speaker['notification']['userId']);
                        contSpeakers++;
                    });
                    if (contSpeakers == size) {
                        resolve(playerIds);
                    }
                } else {
                    resolve(playerIds)
                }
            })
            .catch((e) => {
                reject(e);
            });
    });
}

const getSpeakersGroupsPlayerIds = (db, eventId, groups) => {
    return new Promise((resolve, reject) => {
        let playerIds = [];
        let totalGroups = groups.length;
        let contGroups = 0;
        for (let group of groups) {
            db
                .collection('events')
                .doc(eventId)
                .collection('speakers')
                .where('havePushId', '==', true)
                .where(`groups.${group}.uid`, '==', group)
                .get()
                .then((response) => {
                    let size = response.size;
                    if (size >= 1) {
                        response.forEach(element => {
                            let speaker = element.data();
                            playerIds.push(speaker['notification']['userId']);
                        });

                        if (contGroups == totalGroups - 1) {
                            resolve(playerIds);
                        }
                        contGroups++;

                    } else {
                        // not have speakers with this group
                        if (contGroups == totalGroups - 1) {
                            resolve(playerIds);
                        }
                        contGroups++;
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        }
    });
}

const instatiateNotification = (notification, playerIds, apiId, appId) => {
    let postNotification = {
        app_id: appId,
        headings: {
            en: '',
            pt: '',
            es: '',
            fr: '',
            de: ''
        },
        contents: {
            en: '',
            pt: '',
            es: '',
            fr: '',
            de: ''
        },
        include_player_ids: [],
        send_after: '',
        priority: 10,
        url: '',
        api_id: apiId
        // data: {}
    }

    // titles (headings)
    if (notification.headings['pt'] != undefined) {
        postNotification.headings['pt'] = notification.headings['pt'];
    }

    if (notification.headings['en'] != undefined) {
        postNotification.headings['en'] = notification.headings['en'];
    }

    if (notification.headings['es'] != undefined) {
        postNotification.headings['es'] = notification.headings['es'];
    }

    if (notification.headings['fr'] != undefined) {
        postNotification.headings['fr'] = notification.headings['fr'];
    }

    if (notification.headings['de'] != undefined) {
        postNotification.headings['de'] = notification.headings['de'];
    }

    // message (contents)
    if (notification.contents['pt'] != undefined) {
        postNotification.contents['pt'] = notification.contents['pt'];
    }

    if (notification.contents['en'] != undefined) {
        postNotification.contents['en'] = notification.contents['en'];
    }

    if (notification.contents['es'] != undefined) {
        postNotification.contents['es'] = notification.contents['es'];
    }

    if (notification.contents['fr'] != undefined) {
        postNotification.contents['fr'] = notification.contents['fr'];
    }

    if (notification.contents['de'] != undefined) {
        postNotification.contents['de'] = notification.contents['de'];
    }

    // player ids (send to correct users, in correct event)
    if (playerIds != undefined) {
        postNotification.include_player_ids = removeDoublePlayerIds(playerIds);
    }

    // schedule notification (date with GMT)
    if (notification.scheduled_date != undefined) {
        postNotification.send_after = notification.scheduled_date;
    }

    // url (to start in browser when open notification)
    if (notification.url != undefined) {
        postNotification.url = notification.url;
    }

    return postNotification;
}

function removeDoublePlayerIds(array) {
    let playerIds = array.filter(function (el, i) {
        return array.indexOf(el) == i;
    });

    return playerIds;
}

const sendNotification = (db, data, notification) => {
    console.log("Send notif: ", notification);
    return new Promise((resolve, reject) => {
        // declare header with auth api key to one signal
        const headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Basic " + notification.api_id
        }

        // declare options, with host, port, path, and use headers
        const options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
        }
        let https = require('https');
        // send notification
        let req = https.request(options, function (res) {
            res.on('data', function (notification) {
                console.log('Response: ', JSON.parse(notification));
                let notificationReturnData = JSON.parse(notification);
                db.collection('events')
                    .doc(data.eventId)
                    .collection('notifications')
                    .doc(data.uid)
                    .update({
                        notificationId: notificationReturnData.id,
                        delivery_date: data.delivery_date
                    });
                resolve(true);
            });
        });

        req.on('error', function (e) {
            console.log("ERROR: ", e);
            reject(false);
        });
        req.write(JSON.stringify(notification));
        req.end();
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });

} catch (e) {
    console.log(e)
}

exports = module.exports = functions.firestore
    // observe notifications path
    .document('events/{eventId}/chats/{chatId}/messages/{messageId}').onCreate((snap, context) => {
        let data = snap.data();
        let db = admin.firestore();
        let eventId = context.params.eventId;

        admin.firestore()
            .collection('events')
            .doc(eventId)
            .get()
            .then((response) => {
                let event = response.data();
                let apiId;
                let appId;
                if (event.notification_api_id && event.notification_app_id) {
                    apiId = event.notification_api_id;
                    appId = event.notification_app_id;
                } else {
                    apiId = configServer.notification_api_id;
                    appId = configServer.notification_app_id;
                }

                Promise.all([
                        getAttendeeReceiveNotification(data.eventId, data.to_user),
                        getAttendeeSendNotification(data.eventId, data.from_user)
                    ])
                    .then((response) => {
                        let attendee = response[0];
                        let sender_attendee = response[1]; // to verify userId notification token

                        if (attendee.notification.userId !== sender_attendee.notification.userId) {
                            let player_ids = [];
                            if (attendee.notification.userId !== null && attendee.notification.userId !== undefined && attendee.notification.userId !== '') {
                                player_ids.push(attendee.notification.userId);
                                let notification = {
                                    headings: {
                                        en: ''
                                    },
                                    contents: {
                                        en: ''
                                    }
                                };
                                notification.headings['en'] = data.send_from_user_name;
                                notification.contents['en'] = data.message;
                                let finalNotification = instatiateNotification(notification, player_ids, apiId, appId);
                                sendNotification(finalNotification);
                                return true;
                            } else {
                                return true
                            }
                        } else {
                            return true;
                        }
                    })
            });
    });

const instatiateNotification = (notification, playerIds, apiId, appId) => {
    let postNotification = {
        app_id: appId,
        headings: {
            en: '',
        },
        contents: {
            en: '',
        },
        include_player_ids: [],
        priority: 10,
        api_id: apiId
    }

    if (notification.headings['en'] != undefined) {
        postNotification.headings['en'] = notification.headings['en'];
    }

    if (notification.contents['en'] != undefined) {
        postNotification.contents['en'] = notification.contents['en'];
    }

    // player ids (send to correct users, in correct event)
    if (playerIds != undefined) {
        postNotification.include_player_ids = playerIds;
    }

    return postNotification;
}

getAttendeeReceiveNotification = (eventId, userId) => {
    return new Promise((resolve) => {
        admin
            .firestore()
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .doc(userId)
            .get()
            .then((snapshot) => {
                resolve(snapshot.data());
            });
    })
}
getAttendeeSendNotification = (eventId, userId) => {
    return new Promise((resolve) => {
        admin
            .firestore()
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .doc(userId)
            .get()
            .then((snapshot) => {
                resolve(snapshot.data());
            });
    })
}

function sendNotification(notification) {
    // declare header with auth api key to one signal
    const headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic " + notification.api_id
    }

    // declare options, with host, port, path, and use headers
    const options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    }
    let https = require('https');
    // send notification
    let req = https.request(options, function (res) {
        res.on('data', function (notification) {
            console.log('Response: ', JSON.stringify(notification));
        });
    });

    req.on('error', function (e) {
        console.log("ERROR:");
        console.log(e);
    });
    req.write(JSON.stringify(notification));
    req.end();
}
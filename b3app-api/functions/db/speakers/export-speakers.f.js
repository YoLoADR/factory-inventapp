const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });


exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.query.moduleId;
        let typeOrder = req.query.typeOrder;
        let refSpeakers;
        let speakersSize;
        let refCustomField = db.collection('modules').doc(moduleId).collection('customFields');
        let refFieldsCustom = db.collection('modules').doc(moduleId);

        switch (typeOrder) {
            case 'asc': //a-z
                refSpeakers = db.collection('modules').doc(moduleId).collection('speakers')
                    .orderBy('queryName', 'asc');
                break;

            case 'desc': //z-a
                refSpeakers = db.collection('modules').doc(moduleId).collection('speakers')
                    .orderBy('queryName', 'desc');
                break;

            case 'oldest': //antiho-recente
                refSpeakers = db.collection('modules').doc(moduleId).collection('speakers')
                    .orderBy('createdAt', 'asc');
                break;

            case 'recent': //recente-antigo
                refSpeakers = db.collection('modules').doc(moduleId).collection('speakers')
                    .orderBy('createdAt', 'desc');
                break;

            case 'id': //ID
                refSpeakers = db.collection('modules').doc(moduleId).collection('speakers')
                    .orderBy('identifier');
                break;
        }

        refFieldsCustom
            .get()
            .then((fieldsCustomValue) => {
                let aux = fieldsCustomValue.data();
                let fieldsCustom = aux['fieldsCustom'];

                refSpeakers
                    .get()
                    .then(
                        (dataAttendee) => {
                            let listSpeakers = [];
                            let contAttendee = 0;
                            speakersSize = dataAttendee.size;
                            if (speakersSize >= 1) {
                                dataAttendee
                                    .forEach(
                                        (doc) => {
                                            let attendee = doc.data();
                                            attendee.customField = [];
                                            db
                                                .collection('modules')
                                                .doc(moduleId)
                                                .collection('speakers')
                                                .doc(attendee.uid)
                                                .collection('customFields')
                                                .get()
                                                .then(async (dataAttendeeFields) => {
                                                    let customArray = [];
                                                    dataAttendeeFields.forEach(element => {
                                                        customArray.push(element.data());
                                                    });

                                                    if (customArray.length > 0) {
                                                        let listCustomField = [];
                                                        for (let i = 0; i < customArray.length; i++) {
                                                            //pega o id do custom field
                                                            let id = customArray[i].uid;
                                                            //com base no id, verifica a posição do custom field que está armazenada no fieldOptionsCustom
                                                            let position = fieldsCustom[id].order;
                                                            //insere o custom field na sua posição
                                                            listCustomField[position] = customArray[i];
                                                        }
                                                        let contCustomField = 0;
                                                        for (let attendeeField of listCustomField) {
                                                            if (attendeeField.type == 'text') {
                                                                attendee.customField[contCustomField] = attendeeField;

                                                                if (contCustomField == listCustomField.length - 1) {
                                                                    listSpeakers.push(attendee);
                                                                    contAttendee++;
                                                                }
                                                                contCustomField++;

                                                                if (contAttendee == speakersSize && contCustomField == listCustomField.length) {
                                                                    response(listSpeakers, res, typeOrder);
                                                                }

                                                            } else if (attendeeField.type == 'select') {
                                                                await refCustomField
                                                                    .doc(attendeeField.uid)
                                                                    .collection('options')
                                                                    .get()
                                                                    .then((dataCustomFieldOptions) => {
                                                                        let contOptions = 0;
                                                                        dataCustomFieldOptions.forEach((elementCustomFieldOptions) => {
                                                                            let options = elementCustomFieldOptions.data();
                                                                            if (attendeeField.value == '' || attendeeField.value == null || attendeeField == undefined) {
                                                                                attendeeField.value = '';
                                                                                attendee.customField[contCustomField] = attendeeField;
                                                                            } else if (options.uid == attendeeField.value) {
                                                                                attendeeField.value = options.answer;
                                                                                attendee.customField[contCustomField] = attendeeField;
                                                                            }
                                                                            if (contOptions == dataCustomFieldOptions.size - 1) {
                                                                                contCustomField++;
                                                                            }
                                                                            contOptions++;

                                                                            if (contCustomField == listCustomField.length - 1 && contOptions == dataCustomFieldOptions.size - 1) {
                                                                                listSpeakers.push(attendee);
                                                                                contAttendee++;
                                                                            }
                                                                            if (contAttendee == speakersSize && contCustomField == listCustomField.length && contOptions == dataCustomFieldOptions.size) {
                                                                                response(listSpeakers, res, typeOrder);
                                                                            }


                                                                        });

                                                                    })
                                                            }
                                                        }
                                                    } else {
                                                        listSpeakers.push(attendee);
                                                        if (contAttendee == speakersSize - 1) {
                                                            response(listSpeakers, res, typeOrder);
                                                        }
                                                        contAttendee++;
                                                    }


                                                })
                                                .catch((error) => {
                                                    console.log('error => ', error);
                                                    res.json({
                                                        code: 404,
                                                        message: 'error',
                                                        result: null
                                                    });
                                                })
                                        }
                                    )
                            } else {
                                response(listSpeakers, res, typeOrder);
                            }
                        })
            });


    });
});

reorderSpeakers = (order, speakers) => {
    return new Promise((resolve) => {
        let aux = speakers;
        speakers = [];
        switch (order) {
            case 'asc':
                speakers = aux.sort(function (a, b) {
                    if (a['queryName'] < b['queryName']) { return -1; }
                    if (a['queryName'] > b['queryName']) { return 1; }
                    return 0;
                });
                break;

            case 'desc':
                speakers = aux.sort(function (a, b) {
                    if (a['queryName'] < b['queryName']) { return 1; }
                    if (a['queryName'] > b['queryName']) { return -1; }
                    return 0;
                })
                break;
            case 'recent':
                speakers = aux.sort((a, b) => b.createdAt - a.createdAt);
                break;
            case 'oldest':
                speakers = aux.sort((a, b) => a.createdAt - b.createdAt);
                break;

            case 'id':
                speakers = aux.sort(function (a, b) {
                    if (a['identifier'] < b['identifier']) { return -1; }
                    if (a['identifier'] > b['identifier']) { return 1; }
                    return 0;
                });
                break;
        }
        resolve(speakers);
    });
}

function response(arraySpeakers, res, order) {
    reorderSpeakers(order, arraySpeakers)
        .then((speakers) => {
            res.json({
                code: 200,
                message: 'success',
                result: speakers
            });
        })
}
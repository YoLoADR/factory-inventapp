const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.firestore
    .document('events/{eventId}/speakers/{speakerId}').onCreate((snap, context) => {
        return new Promise((resolve) => {
            let speaker = snap.data();
            // let eventId = context.params.eventId;
            admin.firestore()
                .collection('users')
                .doc(speaker.uid)
                .get()
                .then((snapshot) => {
                    let user = snapshot.data();

                    if (typeof user.events !== 'undefined') {
                        for (let eventId of user.events) {
                            let refEventSpeaker = admin.firestore().collection('events').doc(eventId).collection('speakers').doc(user.uid);
                            refEventSpeaker.update({ firstAccess: user.firstAccess });
                        }
                    }
                    if (typeof user.speakerModules !== 'undefined') {
                        for (let moduleId of user.speakerModules) {
                            let refModuleSpeaker = admin.firestore().collection('modules').doc(moduleId);
                            refModuleSpeaker.update({ firstAccess: user.firstAccess });
                        }
                    }
                    resolve(true);
                });
        })
    });
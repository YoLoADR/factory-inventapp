/**
 * Trigger responsible for monitoring and removing  speakers.
 * 
 */


// imports
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

// monitors the deletion of modules.
exports = module.exports = functions.firestore.document('modules/{moduleId}/speakers/{speakerId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()

        const speakerRemove = snap.data()
        const moduleSpeakerId = speakerRemove.moduleId
        const speakerId = speakerRemove.uid
        const eventId = speakerRemove.eventId

        db.collection('events').doc(eventId).collection('speakers').doc(speakerId).delete()

        await removeSessions(speakerId, moduleSpeakerId, eventId)
        await removeCustomFieldsOfSpeaker(speakerId, moduleSpeakerId, eventId)

        // Checks if the speaker has email.
        if (typeof speakerRemove.email !== 'undefined' && speakerRemove.email !== null && speakerRemove.email !== '') {
            const user = await getUser(speakerId)

            if (user) {
                // pulls the event uid from the user account.
                let totalEvents = 0

                if (typeof user.events !== 'undefined') {
                    const indexEventUid = user.events.indexOf(eventId)
                    user.events.splice(indexEventUid, 1);
                    totalEvents = user.events.length
                }

                if (typeof user.speakerModules !== 'undefined') {
                    const indexModuleUid = user.speakerModules.indexOf(moduleSpeakerId)
                    user.speakerModules.splice(indexModuleUid, 1)
                }

                // checks the number of attendee events. 
                // if >0 => update account or if <=0 => remove account
                let db = admin.firestore()

                if (totalEvents > 0) {
                    db.collection('users').doc(speakerId).update(user)
                } else {
                    //remove o usuário da autenticação
                    admin.auth().deleteUser(speakerId).then(() => {
                        db.collection('users').doc(speakerId).delete()
                    }).catch((err) => {
                        // caso a conta não exista na autenticação.
                        if (err.code === 'auth/user-not-found') {
                            db.collection('users').doc(speakerId).delete()
                        } else {
                            db.collection('users').doc(speakerId).update(user)
                        }
                    })
                }

            }
        }


        resolve(true)
    })
})

// remove sessions from speaker
const removeSessions = (speakerId, moduleSpeakerId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore()

        // Lists all schedule modules of the event.
        const modules = await getModulesSheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the speaker.
            const sessions = await getSessionsOfSpeaker(speakerId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                // delete speaker.
                delete session.speakers[speakerId]

                // check if other session speakers exist.
                for (const index in session.speakers) {
                    const speaker = session.speakers[index]
                    const speakerId = speaker.uid

                    // If the speaker does not exist, remove it from the session.
                    if (!await checkSpeakerExist(speaker)) {
                        delete session.speakers[speakerId]
                    }
                }

                // update session
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(session)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(session)
            }
        }

        resolve(true)
    })
}

// Lists all schedule modules of the event.
const getModulesSheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the sessions of the module that has the speaker.
const getSessionsOfSpeaker = (speakerId, moduleScheduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const speaker = `speakers.${speakerId}`


        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(speaker)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

// check if the speaker exists.
const checkSpeakerExist = (speaker) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const moduleSpeakerId = speaker.moduleId
        const speakerId = speaker.uid

        const ref = db.collection('modules').doc(moduleSpeakerId).collection('speakers').doc(speakerId)

        ref.get().then((snapshot) => {
            const exist = typeof snapshot.data() !== 'undefined' ? true : false
            resolve(exist)
        })
    })
}

// remove custom fields from speaker
const removeCustomFieldsOfSpeaker = (speakerId, speakerModuleId, eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        // list the speaker custom fields.
        db.collection('modules').doc(speakerModuleId).collection('speakers').doc(speakerId).collection('customFields').get().then((snapshot) => {
            snapshot.forEach((element) => {
                const custom = element.data()
                const customId = custom.uid

                // remove custom field
                db.collection('events').doc(eventId).collection('speakers').doc(speakerId).collection('customFields').doc(customId).delete()
                db.collection('modules').doc(speakerModuleId).collection('speakers').doc(speakerId).collection('customFields').doc(customId).delete()
            })

            resolve(true)
        })
    })
}


// get attendee user account
const getUser = (userId) => {
    return new Promise((resolve) => {
        const db = admin.firestore();

        db.collection('users')
            .doc(userId)
            .get().then((snapshot) => {
                let user = null

                if (typeof snapshot.data() !== 'undefined' && snapshot.data() !== null) {
                    user = snapshot.data()
                }

                resolve(user)
            })

    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }


exports = module.exports = functions.firestore
    .document('events/{eventId}/documents/{documentId}').onDelete((snap, context) => {
        return new Promise(async(resolve) => {
            const document = snap.data();
            const eventId = context.params.eventId

            await removeAttendees(document, eventId)
            await removeSpeakers(document, eventId)
            await removeSessions(document, eventId)

            resolve(true)
        })

    });

/**************************************************************************** SPEAKERS ******************************************************************************* */

const removeSpeakers = (document, eventId) => {
    return new Promise(async(resolve, reject) => {
        let db = admin.firestore();
        const documentId = document.uid

        const modules = await getModulesSpeakerEvent(eventId)

        for (const module of modules) {
            const moduleSpeakerId = module.uid

            // lists the speakers of the module that has the document.
            const speakers = await getSpeakersOfDocument(documentId, moduleSpeakerId)

            // update speaker
            for (const speaker of speakers) {
                const speakerId = speaker.uid

                let fieldValue = admin.firestore.FieldValue
                let deleteDocument = {}
                deleteDocument[`documents.${documentId}`] = fieldValue.delete()

                // update speaker
                db.collection('events').doc(eventId).collection('speakers').doc(speakerId).update(deleteDocument)
                db.collection('modules').doc(moduleSpeakerId).collection('speakers').doc(speakerId).update(deleteDocument)
            }
        }

        resolve(true)
    });
}

// Lists all speaker modules of the event.
const getModulesSpeakerEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SPEAKER)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the attendees of the module that has the document.
const getSpeakersOfDocument = (documentId, moduleSpeakerId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const document = `documents.${documentId}`


        const ref = db.collection('modules').doc(moduleSpeakerId).collection('speakers').orderBy(document)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}



/**************************************************************************** ATTENDEES ******************************************************************************* */


const removeAttendees = (document, eventId) => {
    return new Promise(async(resolve, reject) => {
        let db = admin.firestore();
        const documentId = document.uid

        const modules = await getModulesAttendeesEvent(eventId)

        for (const module of modules) {
            const moduleAttendeeId = module.uid

            // lists the attendees of the module that has the document.
            const attendees = await getAttendeesOfDocument(documentId, moduleAttendeeId)

            // update attendee
            for (const attendee of attendees) {
                const attendeeId = attendee.uid

                let fieldValue = admin.firestore.FieldValue
                let deleteDocument = {}
                deleteDocument[`documents.${documentId}`] = fieldValue.delete()


                // update attendee
                db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).update(deleteDocument)
                db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).update(deleteDocument)
            }
        }

        resolve(true)
    });
}

// Lists all attendees modules of the event.
const getModulesAttendeesEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.ATTENDEE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the atendees of the module that has the document.
const getAttendeesOfDocument = (documentId, moduleAttendeeId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const document = `documents.${documentId}`


        const ref = db.collection('modules').doc(moduleAttendeeId).collection('attendees').orderBy(document)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}




/***************************************************** sessions ****************************************************************************** */

const removeSessions = (document, eventId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        const documentId = document.uid

        const modules = await getModulesSheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the document.
            const sessions = await getSessionsOfDocument(documentId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                let fieldValue = admin.firestore.FieldValue
                let deleteDocument = {}
                deleteDocument[`documents.${documentId}`] = fieldValue.delete()

                // update session
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(deleteDocument)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(deleteDocument)
            }
        }

        resolve(true)
    })
}

// Lists all schedule modules of the event.
const getModulesSheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the sessions of the module that has the document.
const getSessionsOfDocument = (documentId, moduleScheduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const document = `documents.${documentId}`


        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(document)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

// check if the document exists.
const checkDocumentExist = (document) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const moduleDocumentId = document.moduleId
        const folderId = document.folderId
        const documentId = document.uid

        const ref = db.collection('modules').doc(moduleDocumentId).collection('folders').doc(folderId).collection('documents').doc(documentId)

        ref.get().then((snapshot) => {
            const exist = typeof snapshot.data() !== 'undefined' ? true : false
            resolve(exist)
        })
    })
}
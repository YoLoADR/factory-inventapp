const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }


exports = module.exports = functions.firestore
    .document('events/{eventId}/documents/{documentId}').onUpdate((change, context) => {
        const updatedDocument = change.after.data();
        const eventId = context.params.eventId

        return Promise.all([
            updateSpeaker(updatedDocument, eventId),
            updateSessions(updatedDocument, eventId)
        ]).catch((err) => {
            console.info(err);
        })
    });

/*********************************************************************** SPEAKER *********************************************************************** */
const updateSpeaker = (document, eventId) => {
    return new Promise(async(resolve, reject) => {
        let db = admin.firestore();
        const documentId = document.uid

        // Lists all speaker modules of the event.
        const modules = await getModulesSpeakerEvent(eventId)
        for (const module of modules) {
            const moduleSpeakerId = module.uid

            // lists the speakers of the module that has the document.
            const speakers = await getSpeakersOfDocument(documentId, moduleSpeakerId)
                // update speakers
            for (const speaker of speakers) {
                const speakerId = speaker.uid

                // update session.
                speaker.documents[documentId] = document
                db.collection('events').doc(eventId).collection('speakers').doc(speakerId).update(speaker)
                db.collection('modules').doc(moduleSpeakerId).collection('speakers').doc(speakerId).update(speaker)
            }
        }

        resolve(true)
    });
}

// Lists all speaker modules of the event.
const getModulesSpeakerEvent = (eventId) => {
    return new Promise((resolve) => {
        const list = []
        let db = admin.firestore()
        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SPEAKER)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the speakers of the module that has the document.
const getSpeakersOfDocument = (documentId, moduleSpeakerId) => {
    return new Promise((resolve) => {
        const list = []
        const document = `documents.${documentId}`

        let db = admin.firestore()
        const ref = db.collection('modules').doc(moduleSpeakerId).collection('speakers').orderBy(document)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

/*********************************************************************** SCHEDULE *********************************************************************** */

const updateSessions = (document, eventId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore();
        const documentId = document.uid

        // Lists all schedule modules of the event.
        const modules = await getModulesScheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the document.
            const sessions = await getSessionsOfDocument(documentId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                // update session.
                session.documents[documentId] = document
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(session)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(session)
            }
        }

        resolve(true)
    })
}



// Lists all schedule modules of the event.
const getModulesScheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        const list = []
        let db = admin.firestore()
        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the sessions of the module that has the document.
const getSessionsOfDocument = (documentId, moduleScheduleId) => {
    return new Promise((resolve) => {
        const list = []
        const document = `documents.${documentId}`

        let db = admin.firestore()
        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(document)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}
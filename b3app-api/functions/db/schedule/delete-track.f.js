const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

/*
    create event
*/

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.body.eventId;
        let moduleId = req.body.moduleId;
        let trackId = req.body.trackId;
        let db = admin.firestore();
        let refModule = db.collection('modules').doc(moduleId).collection('sessions');
        let refEvent = db.collection('events').doc(eventId).collection('sessions');
        let refTrack = db.collection('modules').doc(moduleId).collection('tracks').doc(trackId);

        refTrack.delete();

        refEvent
            .get()
            .then((valueE) => {
                let contSessionsEv = 0;
                let sessionEventSize = valueE.size;
                valueE.forEach(session => {

                    // const batch = db.batch();
                    let FieldValue = require('firebase-admin').firestore.FieldValue;

                    const updates = {};

                    updates[`tracks.${trackId}`] = FieldValue.delete();

                    refEvent.doc(session.data().uid).update(updates);
                    refModule.doc(session.data().uid).update(updates);

                    if (contSessionsEv == sessionEventSize - 1) {
                        responseOk(res);
                    }
                    contSessionsEv++;

                });
            })
            .catch((err) => {
                // error
                console.log(err);
            });
    });
})

function responseOk(res) {
    console.log('=== entrou no response === ');
    res.json({
        code: 200,
        message: 'success',
        result: true
    })
}
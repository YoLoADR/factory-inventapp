const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

exports = module.exports = functions.firestore.document('modules/{moduleId}/sessions/{sessionId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()

        const session = snap.data()
        const sessionId = session.uid
        const sessionModuleId = session.moduleId
        const eventId = session.eventId

        // deletes session from the event path.
        db.collection('events').doc(eventId).collection('sessions').doc(sessionId).delete()

        resolve(await deleteSessionFromAttendees(sessionId, eventId, sessionModuleId))
    })
})


// deletes session from attendee-sessions collections of attendees module personal agenda.
const deleteSessionFromAttendees = (sessionId, eventId, sessionModuleId) => {
    return new Promise((resolve, reject) => {
        const db = admin.firestore()
        const batch = db.batch()

        // personal agenda
        eventCalendarModule(eventId).then((modulePersonalAgenda) => {
            if (modulePersonalAgenda !== null) {
                const modulePersonalAgendaId = modulePersonalAgenda.uid

                // lists the participants who have the session on the personal agenda.
                getListOfSessionAttendees(sessionModuleId, sessionId).then((attendees) => {
                    for (const attendee of attendees) {
                        const attendeeId = attendee.uid
                        const moduleAttendeeId = attendee.moduleId

                        // deletes the session from the list of attendee sessions within the personal schedule module.
                        let ref1 = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)

                        // deletes the session participant.
                        let ref2 = db.collection('events').doc(eventId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId)
                        let ref3 = db.collection('modules').doc(sessionModuleId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId)

                        // removes the session from within the participant collection.
                        let ref4 = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)
                        let ref5 = db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)

                        batch.delete(ref1)
                        batch.delete(ref2)
                        batch.delete(ref3)
                        batch.delete(ref4)
                        batch.delete(ref5)
                    }


                    // Commit the batch
                    batch.commit().then(() => {
                        resolve(true)
                    }).catch((error) => {
                        reject(error)
                    })

                })
            } else {
                resolve(true)
            }
        })

    })
}

// receives the event uid and returns the event's agenda module
const eventCalendarModule = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const PERSONALSCHEDULE = typeModules.PERSONALSCHEDULE //agenda pessoal

        const ref = db.collection('events').doc(eventId).collection('modules');
        ref.where('type', '==', PERSONALSCHEDULE).get().then((snapshot) => {
            let module = null

            if (snapshot.size > 0) {
                snapshot.forEach((childSnapshot) => {
                    if (typeof childSnapshot.data() !== 'undefined' && childSnapshot.data() !== null) {
                        module = childSnapshot.data()
                    }

                    resolve(module)
                })
            } else {
                resolve(module)
            }
        })
    })
}

// lists the participants who have the session on the personal agenda.
const getListOfSessionAttendees = (moduleScheduleId, sessionId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).collection('attendees')

        ref
            .get()
            .then((snapshot) => {
                const list = []

                snapshot.forEach((childSnapshot) => {
                    list.push(childSnapshot.data())
                })

                resolve(list)
            })
    })
}
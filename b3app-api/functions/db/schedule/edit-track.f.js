const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        main(req, res)
    })
})

const main = async(req, res) => {
    const moduleId = req.body.moduleId
    const eventId = req.body.eventId
    const track = req.body.track
    let trackId = track.uid
    let queryTrack = `tracks.${trackId}`

    let db = admin.firestore()

    db.collection('modules').doc(moduleId).collection('tracks').doc(track.uid).update(track)

    db.collection('modules')
        .doc(moduleId)
        .collection('sessions')
        .orderBy(queryTrack)
        .get()
        .then((snapshot) => {
            console.log(snapshot.size)
            snapshot.forEach((element) => {
                const session = element.data()
                const sessionId = session.uid
                session.tracks[trackId] = track

                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(session)
                db.collection('modules').doc(moduleId).collection('sessions').doc(sessionId).update(session)
            })

            res.json({
                code: 200,
                message: 'success',
                result: true
            })
        })



}
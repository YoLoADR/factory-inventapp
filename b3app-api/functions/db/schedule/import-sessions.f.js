const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const cors = require("cors")({
    origin: true
});
const Session = require("../../utilities/model/session")
const DateTime = require('luxon').DateTime
const createTimeStamp = require('../../utilities/luxon').createTimeStamp
const createDate = require('../../utilities/luxon').createDate
const currentDate = require('../../utilities/luxon').currentDate
const convertTimestampToDate = require('../../utilities/luxon').convertTimestampToDate
const defaultZoneName = require('../../utilities/luxon').defaultZoneName


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({
        timestampsInSnapshots: true
    });
} catch (e) {
    console.log(e)
}

defaultZoneName() // define zone default

// column EXCEL
const cell_identifier = 0;
const cell_name = 1
const cell_startDate = 2;
const cell_startTime = 3;
const cell_endTime = 4;
const cell_description = 5
const cell_location = 6;
const cell_track = 7;
const cell_group = 8;
const cell_speaker = 9;
const cell_document = 10;
const cell_limitAttendees = 11
const cell_description_language = 2

let sessions = []
let messageErrors = []
let arraySessions = []; //save imported sessions successfully
let arrayIdentifiersDouble = []; //save sessions with identifiers that belongs to other modules
let arrayGeneralErrors = []; //stores sessions that have error in batch recording (general errors)


/// types modules
const MANAGER_GROUP = require('../../utilities/enums/type-module').MANAGER_GROUP

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        main(req, res)
    })
})

const main = async (req, res) => {
    // get params
    const eventId = req.body.eventId
    const moduleId = req.body.moduleId
    let principalSessions = req.body.dataPrincipalSessions
    let sessions_de_DE = req.body.sessions_de_DE
    let sessions_en_US = req.body.sessions_en_US
    let sessions_es_ES = req.body.sessions_es_ES
    let sessions_fr_FR = req.body.sessions_fr_FR
    let sessions_pt_BR = req.body.sessions_pt_BR
    const groupModuleId = await getIdManagerGroupEvent(eventId)
    const locations = await getLocationsEvent(eventId)
    const tracks = await getTracksModule(moduleId)
    const groups = await getGroupsModule(groupModuleId)
    const speakers = await getSpeakersEvent(eventId)
    const documents = await getDocumentsEvent(eventId)
    const event = await getEvent(eventId)
    const languagePrincipal = event.language
    const languageSecondary = event.languages

    sessions = []
    messageErrors = []
    arraySessions = []; //save imported sessions successfully
    arrayIdentifiersDouble = []; //save sessions with identifiers that belongs to other modules
    arrayGeneralErrors = []; //stores sessions that have error in batch recording (general errors)


    // remove to empty rows from excel
    principalSessions = removeEmptyRows(principalSessions)
    sessions_de_DE = removeEmptyRowsMoultiLanguage(sessions_de_DE)
    sessions_en_US = removeEmptyRowsMoultiLanguage(sessions_en_US)
    sessions_fr_FR = removeEmptyRowsMoultiLanguage(sessions_fr_FR)
    sessions_es_ES = removeEmptyRowsMoultiLanguage(sessions_es_ES)
    sessions_pt_BR = removeEmptyRowsMoultiLanguage(sessions_pt_BR)


    // scroll through the data do principal Sessions
    for (let i = 1; i < principalSessions.length; i++) {
        const row = principalSessions[i]
        const newSession = Session();
        newSession.moduleId = moduleId;
        newSession.eventId = eventId;

        // verification of identifier
        try {
            // verifies that the identifier field has been filled            
            if (typeof row[cell_identifier] === 'undefined' || row[cell_identifier] === '' || row[cell_identifier] === null) throw 'exceptionIdentifierRequired'

            // check if the identifier field is a number
            const regex = /^\d{1,}$/

            if (!regex.test(row[cell_identifier])) throw 'exceptionNotNumber'

            // checks for duplicate identifiers
            if (!checkEqualIdentifiers(row[cell_identifier], i, principalSessions)) throw 'exceptionRepeatedIdentifiers'

            newSession.identifier = parseInt(row[cell_identifier])
        } catch (e) {
            const column = checkLetterCols(cell_identifier); //LETTER A

            if (e === 'exceptionIdentifierRequired') {
                const error = []
                error.push(`Principal ${column}${i + 1}:`)
                error.push('global.required_id')
                messageErrors.push(error)
            }

            if (e === 'exceptionNotNumber') {
                const error = []
                error.push(`Principal ${column}${i + 1}:`)
                error.push('comp.schedule.id_invalid_number')
                messageErrors.push(error)
            }

            if (e === 'exceptionRepeatedIdentifiers') {
                const cellRepeats = getEqualIdentifiers(row[cell_identifier], i, principalSessions)
                for (const cell of cellRepeats) {
                    const error = []
                    error.push(`Principal ${column}${i + 1}-${column}${cell + 1}:`)
                    error.push('global.equal_id')
                    messageErrors.push(error)
                }
            }
        }

        //   //verification of name
        try {
            // verifies that the name field has been filled            
            if (typeof row[cell_name] === 'undefined' || row[cell_name] === null || row[cell_name] === '') throw 'exceptionNameRequired'

            if (languagePrincipal == 'de_DE') {
                newSession.name.DeDE = row[cell_name]
            } else if (languagePrincipal == 'en_US') {
                newSession.name.EnUS = row[cell_name]
            } else if (languagePrincipal == 'es_ES') {
                newSession.name.EsES = row[cell_name]
            } else if (languagePrincipal == 'fr_FR') {
                newSession.name.FrFR = row[cell_name]
            } else if (languagePrincipal == 'pt_BR') {
                newSession.name.PtBR = row[cell_name]
            }
        } catch (e) {
            if (e === 'exceptionNameRequired') {
                const column = checkLetterCols(cell_name);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('global.required_name')
                messageErrors.push(error)
            }
        }

        //verification of date
        try {
            // verifies that the name field has been filled            
            if (typeof row[cell_startDate] === 'undefined' || row[cell_startDate] === '' || row[cell_startDate] === null) throw 'exceptionStartDateRequired'

            // checks whether the date is a Date object or if is string object dd/mm/yyyy
            const regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/.test(row[cell_startDate])

            // if the date is not in a valid format.
            if (!DateTime.fromISO(row[cell_startDate]).isValid && !regex) throw 'exceptionStartDateNotObjectDate'

            // object string
            if (regex) {
                const aux = row[cell_startDate].split('/');

                const day = aux[0];
                const month = aux[1];
                const year = aux[2];

                let hour = '00';
                let minute = '00';

                if (!validateDate(month, day, year)) throw 'exceptionValidateDate'

                newSession.date = createTimeStamp(createDate(year, month, day, hour, minute, '00')) //note: The session date will be updated in the start time portion.
            }
            // object date   
            else {
                const dateTime = DateTime.fromISO(row[cell_startDate])
                //  date mount using luxon library with event's event spindle
                newSession.date = createTimeStamp(dateTime) ////note: The session date will be updated in the start time portion.
            }

            //  creation of the current date
            let auxToday = currentDate()

            const year = auxToday.year
            const month = auxToday.month
            const day = auxToday.day

            // handling the dates to integrate with the luxon library
            let auxDay1 = null
            if (day < 10) {
                auxDay1 = `0${day}`
            } else {
                auxDay1 = day.toString()
            }

            let auxMonth1 = null
            if (month < 10) {
                auxMonth1 = `0${month}`
            } else {
                auxMonth1 = month.toString() // 10,11 or 12
            }

            auxYear1 = year.toString()

            const today = createTimeStamp(createDate(auxYear1, auxMonth1, auxDay1, '00', '00', '00'))

            // checks if the session date is in the past
            // if (newSession.date < today) throw 'exceptionStartDatePast'

        } catch (e) {
            if (e === 'exceptionStartDateRequired') {
                const column = checkLetterCols(cell_startDate);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('global.required_date')

                messageErrors.push(error)
            }

            if (e === 'exceptionStartDateNotObjectDate') {
                const column = checkLetterCols(cell_startDate);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('global.incorrect_date_format')

                messageErrors.push(error)
            }

            if (e === 'exceptionValidateDate') {
                const column = checkLetterCols(cell_startDate);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('global.date_does_not_exist')

                messageErrors.push(error)
            }


            if (e === 'exceptionStartDatePast') {
                const column = checkLetterCols(cell_startDate);
                const error = []

                error.push(`Pricipal ${column}${i + 1}:`)
                error.push('global.invalid_date_session')

                messageErrors.push(error)
            }
        }

        //  verification of start time
        try {
            // verifies that the start time field has been filled            
            if (typeof row[cell_startTime] === 'undefined' || row[cell_startTime] === '' || row[cell_startTime] === null) throw 'exceptionStartTimeRequired'

            // // check the time format

            // check the time format with hh:mm
            const regex1 = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$/.test(row[cell_startTime])

            // checks the time format with hh:mm:ss
            const regex2 = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(row[cell_startTime])

            // if the time is not in a valid format.
            if (!regex1 && !regex2) throw 'exceptionInvalidFormat'


            if (regex1) {
                if (newSession.date !== '' && typeof newSession.date === 'number') {
                    const aux1 = row[cell_startTime].split(':');
                    const hour = aux1[0];
                    const minute = aux1[1];


                    // takes the values from the session date
                    const aux2 = convertTimestampToDate(newSession.date)

                    // take the date values
                    const year = aux2.year
                    const month = aux2.month
                    const day = aux2.day

                    // // converts the data to string
                    let auxDay = null
                    if (day < 10) {
                        auxDay = `0${day}`
                    } else {
                        auxDay = day.toString()
                    }

                    // let auxMonth = null
                    if (month < 10) {
                        auxMonth = `0${month}`
                    } else {
                        auxMonth = month.toString() // 10,11 or 12
                    }

                    let auxYear = year.toString()


                    newSession.startTime = createTimeStamp(createDate(auxYear, auxMonth, auxDay, hour, minute, '00'))
                }
            }

            if (regex2) {
                if (newSession.date !== '' && typeof newSession.date === 'number') {
                    // startTime treatment

                    // get startTime values
                    const aux1 = row[cell_startTime].split(':');
                    const hour = parseInt(aux1[0])
                    const minute = parseInt(aux1[1])
                    const second = parseInt(aux1[2])

                    // hour 
                    let auxHour = null
                    if (hour < 10) {
                        auxHour = `0${hour}`
                    } else {
                        auxHour = hour.toString()
                    }

                    // minute
                    let auxMinute = null
                    if (minute < 10) {
                        auxMinute = `0${minute}`
                    } else {
                        auxMinute = minute.toString()
                    }

                    // second
                    let auxSecond = null
                    if (second < 10) {
                        auxSecond = `0${second}`
                    } else {
                        auxSecond = second.toString()
                    }


                    // date treatment

                    const aux2 = convertTimestampToDate(newSession.date)

                    // take the date values
                    const year = aux2.year
                    const month = aux2.month
                    const day = aux2.day

                    let auxYear = year.toString()


                    // // converts the data to string
                    let auxDay = null
                    if (day < 10) {
                        auxDay = `0${day}`
                    } else {
                        auxDay = day.toString()
                    }

                    let auxMonth = null
                    if (month < 10) {
                        auxMonth = `0${month}`
                    } else {
                        auxMonth = month.toString() // 10,11 or 12
                    }



                    newSession.startTime = createTimeStamp(createDate(auxYear, auxMonth, auxDay, auxHour, auxMinute, auxSecond))
                }
            }
        } catch (e) {
            if (e === 'exceptionStartTimeRequired') {
                const column = checkLetterCols(cell_startTime);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('global.requerid_startTime')

                messageErrors.push(error)
            }

            if (e === 'exceptionInvalidFormat') {
                const column = checkLetterCols(cell_startTime);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('comp.schedule.invalid_format_hours')

                messageErrors.push(error)
            }
        }


        //verification of end time
        try {
            // correct the value of endTime if it comes undefined or null.
            if (typeof row[cell_endTime] === 'undefined' || row[cell_endTime] === null) {
                row[cell_endTime] = ""
            }

            // if the endTime field is filled
            if (row[cell_endTime].length > 0) {
                // check the time format

                // check the time format with hh:mm
                const regex1 = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])$/.test(row[cell_endTime])

                // checks the time format with hh:mm:ss
                const regex2 = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/.test(row[cell_endTime])

                // if the time is not in a valid format.
                if (!regex1 && !regex2) throw 'exceptionInvalidFormat'


                if (regex1) {
                    if (newSession.date !== '' && typeof newSession.date === 'number') {
                        const aux1 = row[cell_endTime].split(':');
                        const hour = aux1[0];
                        const minute = aux1[1];

                        // takes the values from the session date
                        const aux2 = convertTimestampToDate(newSession.date)

                        // take the date values
                        const year = aux2.year
                        const month = aux2.month
                        const day = aux2.day

                        // converts the data to string
                        let auxDay = null
                        if (day < 10) {
                            auxDay = `0${day}`
                        } else {
                            auxDay = day.toString()
                        }

                        let auxMonth = null
                        if (month < 10) {
                            auxMonth = `0${month}`
                        } else {
                            auxMonth = month.toString() // 10,11 or 12
                        }

                        let auxYear = year.toString()

                        newSession.endTime = createTimeStamp(createDate(auxYear, auxMonth, auxDay, hour, minute, '00'))
                    }
                }

                if (regex2) {
                    if (newSession.date !== '' && typeof newSession.date === 'number') {
                        // endTime treatment

                        // get endTime values
                        const aux1 = row[cell_endTime].split(':');
                        const hour = parseInt(aux1[0])
                        const minute = parseInt(aux1[1])
                        const second = parseInt(aux1[2])

                        // hour 
                        let auxHour = null
                        if (hour < 10) {
                            auxHour = `0${hour}`
                        } else {
                            auxHour = hour.toString()
                        }

                        // minute
                        let auxMinute = null
                        if (minute < 10) {
                            auxMinute = `0${minute}`
                        } else {
                            auxMinute = minute.toString()
                        }

                        // second
                        let auxSecond = null
                        if (second < 10) {
                            auxSecond = `0${second}`
                        } else {
                            auxSecond = second.toString()
                        }


                        // date treatment

                        // takes the values from the session date
                        const aux2 = convertTimestampToDate(newSession.date)

                        // take the date values
                        const year = aux2.year
                        const month = aux2.month
                        const day = aux2.day

                        // // converts the data to string
                        let auxDay = null
                        if (day < 10) {
                            auxDay = `0${day}`
                        } else {
                            auxDay = day.toString()
                        }

                        let auxMonth = null
                        if (month < 10) {
                            auxMonth = `0${month}`
                        } else {
                            auxMonth = month.toString() // 10,11 or 12
                        }

                        let auxYear = year.toString()


                        newSession.endTime = createTimeStamp(createDate(auxYear, auxMonth, auxDay, auxHour, auxMinute, auxSecond))
                    }
                }

                // checks if the end time is less than the start time.
                if ((newSession.startTime !== '' && newSession.endTime !== '') && (newSession.endTime < newSession.startTime)) throw 'ExceptionLess'

            } else {
                newSession.endTime = row[cell_endTime]
            }
        } catch (e) {
            if (e === 'exceptionInvalidFormat') {
                const column = checkLetterCols(cell_endTime);
                const error = []

                error.push(`Principal ${column}${i + 1}:`)
                error.push('comp.schedule.invalid_format_hours')

                messageErrors.push(error)
            }

            if (e === 'ExceptionLess') {
                const column1 = checkLetterCols(cell_startTime);
                const column2 = checkLetterCols(cell_endTime);

                const error = []

                error.push(`Principal ${column1}${i + 1}-${column2}${i + 1}:`)
                error.push('comp.schedule.invalid_hours')

                messageErrors.push(error)
            }
        }

        // validate descriptions
        if (typeof row[cell_description] === 'undefined' || row[cell_description] === null) {
            row[cell_description] = ''
        }

        if (languagePrincipal === 'de_DE') {
            newSession.descriptions.DeDE = row[cell_description]
        } else if (languagePrincipal === 'en_US') {
            newSession.descriptions.EnUS = row[cell_description]
        } else if (languagePrincipal === 'es_ES') {
            newSession.descriptions.EsES = row[cell_description]
        } else if (languagePrincipal === 'fr_FR') {
            newSession.descriptions.FrFR = row[cell_description]
        } else if (languagePrincipal === 'pt_BR') {
            newSession.descriptions.PtBR = row[cell_description]
        }



        // validate locations
        if (typeof row[cell_location] === 'number') {
            row[cell_location] = row[cell_location].toString()
        }

        if (typeof row[cell_location] !== 'undefined' && row[cell_location] !== '' && row[cell_location] !== null) {
            const locationIdentifiers = row[cell_location].split(';');

            let order = 0
            for (const name of locationIdentifiers) {
                const index = locations.map(function (e) {
                    return e.identifier;
                }).indexOf(name);

                if (index > -1) {
                    // create location
                    const location = {
                        uid: locations[index].uid,
                        identifier: locations[index].identifier,
                        name: locations[index].name,
                        image: locations[index].image,
                        description: locations[index].description,
                        moduleId: locations[index].moduleId,
                        eventId: locations[index].eventId,
                        order: order
                    }
                    newSession.locations[location.uid] = location

                    order++
                } else {
                    const column = checkLetterCols(cell_location);
                    const error = []

                    error.push(`Principal ${column}${i + 1}: ${name},`)
                    error.push('global.does_not_belong_to_the_module')
                    messageErrors.push(error)
                }
            }
        }

        // validate tracks
        if (typeof row[cell_track] === 'number') {
            row[cell_track] = row[cell_track].toString()
        }

        if (typeof row[cell_track] !== 'undefined' && row[cell_track] !== '' && row[cell_track] !== null) {
            const identifiers = row[cell_track].split(';');
            for (const identifier of identifiers) {
                const index = tracks.map(function (e) {
                    return e.identifier;
                }).indexOf(identifier);
                if (index > -1) {
                    const track = tracks[index]
                    newSession.tracks[track.uid] = track
                } else {
                    const column = checkLetterCols(cell_track);
                    const error = []

                    error.push(`Principal ${column}${i + 1}: ${identifier},`)
                    error.push('global.does_not_belong_to_the_module')
                    messageErrors.push(error)
                }
            }
        }

        // validate groups
        if (typeof row[cell_group] === 'number') {
            row[cell_group] = row[cell_group].toString()
        }

        if (typeof row[cell_group] !== 'undefined' && row[cell_group] !== '' && row[cell_group] !== null) {
            const identifiers = row[cell_group].split(';');
            for (const identifier of identifiers) {
                const index = groups.map(function (e) {
                    return e.identifier;
                }).indexOf(identifier);
                if (index > -1) {
                    const group = groups[index]
                    newSession.groups[group.uid] = group
                } else {
                    const column = checkLetterCols(cell_group);
                    const error = []

                    error.push(`Principal ${column}${i + 1}: ${identifier},`)
                    error.push('global.does_not_belong_to_the_module')
                    messageErrors.push(error)
                }
            }
        }

        // validate speakers
        if (typeof row[cell_speaker] === 'number') {
            row[cell_speaker] = row[cell_speaker].toString()
        }

        if (typeof row[cell_speaker] !== 'undefined' && row[cell_speaker] !== '' && row[cell_speaker] !== null) {
            const identifiers = row[cell_speaker].split(';');
            for (const identifier of identifiers) {
                const index = speakers.map(function (e) {
                    return e.identifier;
                }).indexOf(identifier);
                if (index > -1) {
                    const speaker = speakers[index]
                    newSession.speakers[speaker.uid] = speaker
                } else {
                    const column = checkLetterCols(cell_speaker);
                    const error = []

                    error.push(`Principal ${column}${i + 1}: ${identifier},`)
                    error.push('global.does_not_belong_to_the_module')
                    messageErrors.push(error)
                }
            }
        }


        // validate documents
        if (typeof row[cell_document] === 'number') {
            row[cell_document] = row[cell_document].toString()
        }

        if (typeof row[cell_document] !== 'undefined' && row[cell_document] !== '' && row[cell_document] !== null) {
            const identifiers = row[cell_document].split(';');
            for (const identifier of identifiers) {
                const index = documents.map(function (e) {
                    return e.identifier;
                }).indexOf(identifier);
                if (index > -1) {
                    const document = documents[index]
                    newSession.documents[document.uid] = document
                } else {
                    const column = checkLetterCols(cell_document);
                    const error = []

                    error.push(`Principal ${column}${i + 1}: ${identifier},`)
                    error.push('global.does_not_belong_to_the_module')
                    messageErrors.push(error)
                }
            }
        }


        //limitAttendees 
        try {
            if (row[cell_limitAttendees] === '' || row[cell_limitAttendees] === null || typeof row[cell_limitAttendees] === 'undefined') throw 'ExceptionError'

            if (isNaN(parseInt(row[cell_limitAttendees]))) throw 'ExceptionIsNaN'

            newSession.limitAttendees = parseInt(row[cell_limitAttendees])
        } catch (e) {
            newSession.limitAttendees = 0
        }

        sessions.push(newSession)
    }

    // scroll through the data do sessions_de_DE
    if (languagePrincipal !== 'de_DE' && languageSecondary.DeDE) {
        for (let i = 1; i < sessions_de_DE.length; i++) {
            const row = sessions_de_DE[i]
            // verification of identifier
            try {
                const regex = /^\d{1,}$/
                // verifies that the identifier field has been filled            
                if (typeof row[cell_identifier] === 'undefined' || row[cell_identifier] === '' || row[cell_identifier] === null) throw 'exceptionIdentifierRequired'
                // check if the identifier field is a number
                if (!regex.test(row[cell_identifier])) throw 'exceptionNotNumber'
                // checks for duplicate identifiers
                if (!checkEqualIdentifiers(row[cell_identifier], i, sessions_de_DE)) throw 'exceptionRepeatedIdentifiers'

                // Checks if identier exists in the session array.
                if (!checkIdentiferInSession(row[cell_identifier])) throw 'exceptionIdentifierNotFound'

            } catch (e) {
                const column = checkLetterCols(cell_identifier); //LETTER A

                if (e === 'exceptionIdentifierRequired') {
                    const error = []
                    error.push(`de-DE ${column}${i + 1}:`)
                    error.push('global.required_id')
                    messageErrors.push(error)
                }

                if (e === 'exceptionNotNumber') {
                    const error = []
                    error.push(`de-DE ${column}${i + 1}:`)
                    error.push('comp.schedule.id_invalid_number')
                    messageErrors.push(error)
                }

                if (e === 'exceptionRepeatedIdentifiers') {
                    const cellRepeats = getEqualIdentifiers(row[cell_identifier], i, sessions_de_DE)
                    for (const cell of cellRepeats) {
                        const error = []
                        error.push(`de-DE ${column}${i + 1}-${column}${cell + 1}:`)
                        error.push('global.equal_id')
                        messageErrors.push(error)
                    }
                }

                if (e === 'exceptionIdentifierNotFound') {
                    const error = []
                    error.push(`de-DE ${column}${i + 1}:`)
                    error.push('global.identifier_not_found')
                    messageErrors.push(error)
                }
            }

            //   //verification of name

            // verifies that the name field has been filled            
            if (typeof row[cell_name] === 'undefined' || row[cell_name] === null) {
                row[cell_name] = ''
            }

            if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                const index = sessions.map(function (e) {
                    return e.identifier;
                }).indexOf(row[cell_identifier]);

                if (index > -1) {
                    const session = sessions[index]
                    session.name.DeDE = row[cell_name].toString()
                }
            }

            // descriptions
            if (typeof row[cell_description_language] !== 'undefined' && row[cell_description_language] !== null && row[cell_description_language] !== '') {
                if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                    const index = sessions.map(function (e) {
                        return e.identifier;
                    }).indexOf(row[cell_identifier]);

                    if (index > -1) {
                        const session = sessions[index]
                        session.descriptions.DeDE = row[cell_description_language]
                    }
                }

            }
        }
    }



    // scroll through the data do sessions_en_US
    if (languagePrincipal != 'en_US' && languageSecondary.EnUS) {
        for (let i = 1; i < sessions_en_US.length; i++) {
            const row = sessions_en_US[i]

            // verification of identifier
            try {
                const regex = /^\d{1,}$/
                // verifies that the identifier field has been filled            
                if (typeof row[cell_identifier] === 'undefined' || row[cell_identifier] === '' || row[cell_identifier] === null) throw 'exceptionIdentifierRequired'
                // check if the identifier field is a number
                if (!regex.test(row[cell_identifier])) throw 'exceptionNotNumber'
                // checks for duplicate identifiers
                if (!checkEqualIdentifiers(row[cell_identifier], i, sessions_en_US)) throw 'exceptionRepeatedIdentifiers'

                // Checks if identier exists in the session array.
                if (!checkIdentiferInSession(row[cell_identifier])) throw 'exceptionIdentifierNotFound'
            } catch (e) {
                const column = checkLetterCols(cell_identifier); //LETTER A

                if (e === 'exceptionIdentifierRequired') {
                    const error = []
                    error.push(`en-US ${column}${i + 1}:`)
                    error.push('global.required_id')
                    messageErrors.push(error)
                }

                if (e === 'exceptionNotNumber') {
                    const error = []
                    error.push(`en-US ${column}${i + 1}:`)
                    error.push('comp.schedule.id_invalid_number')
                    messageErrors.push(error)
                }

                if (e === 'exceptionRepeatedIdentifiers') {
                    const cellRepeats = getEqualIdentifiers(row[cell_identifier], i, sessions_en_US)
                    for (const cell of cellRepeats) {
                        const error = []
                        error.push(`en-US ${column}${i + 1}-${column}${cell + 1}:`)
                        error.push('global.equal_id')
                        messageErrors.push(error)
                    }
                }

                if (e === 'exceptionIdentifierNotFound') {
                    const error = []
                    error.push(`en-US ${column}${i + 1}:`)
                    error.push('global.identifier_not_found')
                    messageErrors.push(error)
                }
            }

            //   //verification of name

            // verifies that the name field has been filled            
            if (typeof row[cell_name] === 'undefined' || row[cell_name] === null) {
                row[cell_name] = ''
            }

            if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                const index = sessions.map(function (e) {
                    return e.identifier;
                }).indexOf(row[cell_identifier]);

                if (index > -1) {
                    const session = sessions[index]
                    session.name.EnUS = row[cell_name].toString()
                }
            }


            // descriptions
            if (typeof row[cell_description_language] !== 'undefined' && typeof row[cell_description_language] !== '') {
                if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                    const index = sessions.map(function (e) {
                        return e.identifier;
                    }).indexOf(row[cell_identifier]);
                    if (index > -1) {
                        const session = sessions[index]
                        session.descriptions.EnUS = row[cell_description_language]
                        console.log(session.descriptions.EnUS)
                    }
                }

            }
        }
    }


    // scroll through the data do sessions_es_ES
    if (languagePrincipal != 'es_ES' && languageSecondary.EsES) {
        for (let i = 1; i < sessions_es_ES.length; i++) {
            const row = sessions_es_ES[i]

            // verification of identifier
            try {
                const regex = /^\d{1,}$/
                // verifies that the identifier field has been filled            
                if (typeof row[cell_identifier] === 'undefined' || row[cell_identifier] === '' || row[cell_identifier] === null) throw 'exceptionIdentifierRequired'
                // check if the identifier field is a number
                if (!regex.test(row[cell_identifier])) throw 'exceptionNotNumber'
                // checks for duplicate identifiers
                if (!checkEqualIdentifiers(row[cell_identifier], i, sessions_es_ES)) throw 'exceptionRepeatedIdentifiers'


                // Checks if identier exists in the session array.
                if (!checkIdentiferInSession(row[cell_identifier])) throw 'exceptionIdentifierNotFound'

            } catch (e) {
                const column = checkLetterCols(cell_identifier); //LETTER A

                if (e === 'exceptionIdentifierRequired') {
                    const error = []
                    error.push(`es-ES ${column}${i + 1}:`)
                    error.push('global.required_id')
                    messageErrors.push(error)
                }

                if (e === 'exceptionNotNumber') {
                    const error = []
                    error.push(`es-ES ${column}${i + 1}:`)
                    error.push('comp.schedule.id_invalid_number')
                    messageErrors.push(error)
                }

                if (e === 'exceptionRepeatedIdentifiers') {
                    const cellRepeats = getEqualIdentifiers(row[cell_identifier], i, sessions_es_ES)
                    for (const cell of cellRepeats) {
                        const error = []
                        error.push(`es-ES ${column}${i + 1}-${column}${cell + 1}:`)
                        error.push('global.equal_id')
                        messageErrors.push(error)
                    }
                }

                if (e === 'exceptionIdentifierNotFound') {
                    const error = []
                    error.push(`es-ES ${column}${i + 1}:`)
                    error.push('global.identifier_not_found')
                    messageErrors.push(error)
                }
            }

            //   //verification of name

            // verifies that the name field has been filled            
            if (typeof row[cell_name] === 'undefined' || row[cell_name] === null) {
                row[cell_name] = ''
            }

            if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                const index = sessions.map(function (e) {
                    return e.identifier;
                }).indexOf(row[cell_identifier]);

                if (index > -1) {
                    const session = sessions[index]
                    session.name.EsES = row[cell_name].toString()
                }
            }


            // descriptions
            if (typeof row[cell_description_language] !== 'undefined' && typeof row[cell_description_language] !== '') {
                if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                    const index = sessions.map(function (e) {
                        return e.identifier;
                    }).indexOf(row[cell_identifier]);

                    if (index > -1) {
                        const session = sessions[index]
                        session.descriptions.EsES = row[cell_description_language]
                    }
                }

            }
        }
    }


    // // scroll through the data do sessions_fr_FR
    if (languagePrincipal != 'fr_FR' && languageSecondary.FrFR) {
        for (let i = 1; i < sessions_fr_FR.length; i++) {
            const row = sessions_fr_FR[i]

            // verification of identifier
            try {
                const regex = /^\d{1,}$/
                // verifies that the identifier field has been filled            
                if (typeof row[cell_identifier] === 'undefined' || row[cell_identifier] === '' || row[cell_identifier] === null) throw 'exceptionIdentifierRequired'
                // check if the identifier field is a number
                if (!regex.test(row[cell_identifier])) throw 'exceptionNotNumber'
                // checks for duplicate identifiers
                if (!checkEqualIdentifiers(row[cell_identifier], i, sessions_fr_FR)) throw 'exceptionRepeatedIdentifiers'

                // Checks if identier exists in the session array.
                if (!checkIdentiferInSession(row[cell_identifier])) throw 'exceptionIdentifierNotFound'
            } catch (e) {
                const column = checkLetterCols(cell_identifier); //LETTER A

                if (e === 'exceptionIdentifierRequired') {
                    const error = []
                    error.push(`fr-FR ${column}${i + 1}:`)
                    error.push('global.required_id')
                    messageErrors.push(error)
                }

                if (e === 'exceptionNotNumber') {
                    const error = []
                    error.push(`fr-FR ${column}${i + 1}:`)
                    error.push('comp.schedule.id_invalid_number')
                    messageErrors.push(error)
                }

                if (e === 'exceptionRepeatedIdentifiers') {
                    const cellRepeats = getEqualIdentifiers(row[cell_identifier], i, sessions_fr_FR)
                    for (const cell of cellRepeats) {
                        const error = []
                        error.push(`fr-FR ${column}${i + 1}-${column}${cell + 1}:`)
                        error.push('global.equal_id')
                        messageErrors.push(error)
                    }
                }

                if (e === 'exceptionIdentifierNotFound') {
                    const error = []
                    error.push(`fr-FR ${column}${i + 1}:`)
                    error.push('global.identifier_not_found')
                    messageErrors.push(error)
                }
            }

            //   //verification of name

            // verifies that the name field has been filled            
            if (typeof row[cell_name] === 'undefined' || row[cell_name] === null) {
                row[cell_name] = ''
            }


            if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                const index = sessions.map(function (e) {
                    return e.identifier;
                }).indexOf(row[cell_identifier]);

                if (index > -1) {
                    const session = sessions[index]
                    session.name.FrFR = row[cell_name].toString()
                }
            }


            // descriptions
            if (typeof row[cell_description_language] !== 'undefined' && typeof row[cell_description_language] !== '') {
                if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                    const index = sessions.map(function (e) {
                        return e.identifier;
                    }).indexOf(row[cell_identifier]);

                    if (index > -1) {
                        const session = sessions[index]
                        session.descriptions.FrFR = row[cell_description_language]
                    }
                }

            }
        }
    }




    // scroll through the data do sessions_pt_BR
    if (languagePrincipal != 'pt_BR' && languageSecondary.PtBR) {
        for (let i = 1; i < sessions_pt_BR.length; i++) {
            const row = sessions_pt_BR[i]

            // verification of identifier
            try {
                const regex = /^\d{1,}$/
                // verifies that the identifier field has been filled            
                if (typeof row[cell_identifier] === 'undefined' || row[cell_identifier] === '' || row[cell_identifier] === null) throw 'exceptionIdentifierRequired'
                // check if the identifier field is a number
                if (!regex.test(row[cell_identifier])) throw 'exceptionNotNumber'
                // checks for duplicate identifiers
                if (!checkEqualIdentifiers(row[cell_identifier], i, sessions_pt_BR)) throw 'exceptionRepeatedIdentifiers'

                // Checks if identier exists in the session array.
                if (!checkIdentiferInSession(row[cell_identifier])) throw 'exceptionIdentifierNotFound'
            } catch (e) {
                const column = checkLetterCols(cell_identifier); //LETTER A

                if (e === 'exceptionIdentifierRequired') {
                    const error = []
                    error.push(`pt-BR ${column}${i + 1}:`)
                    error.push('global.required_id')
                    messageErrors.push(error)
                }

                if (e === 'exceptionNotNumber') {
                    const error = []
                    error.push(`pt-BR ${column}${i + 1}:`)
                    error.push('comp.schedule.id_invalid_number')
                    messageErrors.push(error)
                }

                if (e === 'exceptionRepeatedIdentifiers') {
                    const cellRepeats = getEqualIdentifiers(row[cell_identifier], i, sessions_pt_BR)
                    for (const cell of cellRepeats) {
                        const error = []
                        error.push(`pt-BR ${column}${i + 1}-${column}${cell + 1}:`)
                        error.push('global.equal_id')
                        messageErrors.push(error)
                    }
                }

                if (e === 'exceptionIdentifierNotFound') {
                    const error = []
                    error.push(`pt-BR ${column}${i + 1}:`)
                    error.push('global.identifier_not_found')
                    messageErrors.push(error)
                }
            }

            //   //verification of name pt_BR

            // verifies that the name field has been filled            
            if (typeof row[cell_name] === 'undefined' || row[cell_name] === null) {
                row[cell_name] = ''
            }


            if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                const index = sessions.map(function (e) {
                    return e.identifier;
                }).indexOf(row[cell_identifier]);

                if (index > -1) {
                    const session = sessions[index]
                    session.name.PtBR = row[cell_name].toString()
                }
            }


            // descriptions
            if (typeof row[cell_description_language] !== 'undefined' && typeof row[cell_description_language] !== '') {
                if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] !== '' && row[cell_identifier] !== null) {
                    const index = sessions.map(function (e) {
                        return e.identifier;
                    }).indexOf(row[cell_identifier]);

                    if (index > -1) {
                        const session = sessions[index]
                        session.descriptions.PtBR = row[cell_description_language]
                    }
                }

            }
        }
    }


    if (messageErrors.length <= 0 && sessions.length > 0) {
        const total = sessions.length
        let cont = 0

        for (const session of sessions) {
            const myIdentifier = session.identifier
            const eventId = session.eventId
            const moduleId = session.moduleId

            getSession(myIdentifier, eventId).then((oldSession) => {
                // picks up session information that is not imported.
                session.uid = oldSession.uid
                session.attendees = oldSession.attendees
                session.askQuestion = oldSession.askQuestion
                session.askModerate = oldSession.askModerate

                // // checks if the session is in the same module
                if (moduleId === oldSession.moduleId) {
                    // update session
                    updateSession(session).then((success) => {
                        cont++
                        arraySessions.push(session)
                        response(cont, total, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors)
                    }).catch(() => {
                        cont++
                        arrayGeneralErrors.push(session)
                        response(cont, total, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors)
                    })
                } else {
                    cont++;
                    arrayIdentifiersDouble.push(session)
                    response(cont, total, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors)
                }

            }).catch(() => {
                // create session
                createSession(session).then((success) => {
                    cont++
                    arraySessions.push(session)
                    response(cont, total, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors)
                }).catch(() => {
                    cont++;
                    arrayGeneralErrors.push(session)
                    response(cont, total, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors)
                })
            })
        }
    } else {
        response(0, 0, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors)
    }
}

// load the event group module uid.
const getIdManagerGroupEvent = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()

        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', MANAGER_GROUP)
            .get()
            .then((value) => {
                value.forEach(element => {
                    const groupModuleId = element.data().uid
                    resolve(groupModuleId)
                });
            })
    })
}

// load all event locations
const getLocationsEvent = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        let list = []

        db.collection('events')
            .doc(eventId)
            .collection('locations')
            .get()
            .then((snapshot) => {
                snapshot.forEach((location) => {
                    list.push(location.data())
                })
                resolve(list)
            })
    })
}

// load all modules tracks
const getTracksModule = (moduleId) => {
    return new Promise((resolve) => {
        let tracks = [];
        const db = admin.firestore()

        db.collection('modules')
            .doc(moduleId)
            .collection('tracks')
            .get()
            .then((snapshot) => {
                snapshot.forEach(element => {
                    tracks.push(element.data());
                });

                resolve(tracks);
            });
    })
}

// load all modules groups
const getGroupsModule = (moduleId) => {
    return new Promise((resolve) => {
        let groups = [];
        const db = admin.firestore()

        db.collection('modules')
            .doc(moduleId)
            .collection('groups')
            .get()
            .then((values) => {
                if (values.size >= 1) {
                    values.forEach(element => {
                        groups.push(element.data());
                    });
                }

                resolve(groups);
            });
    })
}

// load all events documents
const getDocumentsEvent = (eventId) => {
    return new Promise((resolve) => {
        let documents = [];
        const db = admin.firestore()

        db.collection('events')
            .doc(eventId)
            .collection('documents')
            .get()
            .then((snapshot) => {

                snapshot.forEach((childSnanapshot) => {
                    const document = childSnanapshot.data()
                    documents.push(document)
                })

                resolve(documents)
            })

    })
}

// load all events speakers
const getSpeakersEvent = (eventId) => {
    return new Promise((resolve) => {
        let speakers = [];
        const db = admin.firestore()

        db.collection('events')
            .doc(eventId)
            .collection('speakers')
            .get()
            .then((snapshot) => {

                snapshot.forEach((childSnanapshot) => {
                    const speaker = childSnanapshot.data()
                    speakers.push(speaker)
                })

                resolve(speakers)
            })

    })
}

//load event
const getEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        db.collection('events')
            .doc(eventId)
            .get()
            .then((snapshot) => {
                const event = snapshot.data()
                resolve(event)
            })
    })
}

// remove to empty rows from excel
const removeEmptyRows = (data) => {
    const indexs = []

    // get the indices of the empty lines
    for (let i = 0; i < data.length; i++) {
        const row = data[i]
        if (row[cell_identifier] == '' && row[cell_startDate] == '' && row[cell_startTime] == '' && row[cell_endTime] == '' && row[cell_location] == '' &&
            row[cell_track] == '' && row[cell_group] == '' && row[cell_speaker] == '' && row[cell_limitAttendees] == '') {
            indexs.push(i)
        }
    }
    // remove the lines
    for (const index of indexs) {
        data.splice(index, 1)
    }

    return data;
}

// remove to empty rows from excel
const removeEmptyRowsMoultiLanguage = (data) => {
    const indexs = []

    // get the indices of the empty lines
    for (let i = 0; i < data.length; i++) {
        const row = data[i]
        if (row[cell_identifier] == '' && row[cell_name] == '' && row[cell_description_language]) {
            indexs.push(i)
        }
    }
    // remove the lines
    for (const index of indexs) {
        data.splice(index, 1)
    }

    return data;
}

//retorna a letra da coluna correspondente ao parâmetro.
const checkLetterCols = (column) => {
    let letter = "";

    switch (column) {
        case cell_identifier:
            letter = "A";
            break;
        case cell_name:
            letter = "B";
            break;
        case cell_startDate || cell_description_language:
            letter = "C";
            break;
        case cell_startTime:
            letter = "D";
            break;
        case cell_endTime:
            letter = "E";
            break;
        case cell_description:
            letter = "F";
            break;
        case cell_location:
            letter = "G";
            break;
        case cell_track:
            letter = "H";
            break;
        case cell_group:
            letter = "I";
            break;
        case cell_speaker:
            letter = "J"
            break
        case cell_document:
            letter = "K"
            break
        case cell_limitAttendees:
            letter = "L"
            break
    }

    return letter;
}

/* check if the id passed in the parameter has already been entered in another line of excel.*/
const getEqualIdentifiers = (value, pos, data) => {
    const repeatedLines = []

    for (let i = pos + 1; i < data.length; i++) {
        const row = data[i]

        if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] === value) {
            repeatedLines.push(i)
        }
    }

    return repeatedLines
}

// Checks if identier exists in the session array.
const checkIdentiferInSession = (identifier) => {
    return sessions.map(function (e) {
        return e.identifier;
    }).indexOf(identifier) > -1 ? true : false
}

// checks if there is more than 1 record with same identifier
const checkEqualIdentifiers = (value, pos, data) => {
    let contRepeat = 0

    for (let i = pos + 1; i < data.length; i++) {
        const row = data[i]

        if (typeof row[cell_identifier] !== 'undefined' && row[cell_identifier] === value) {
            contRepeat++
        }
    }

    if (contRepeat > 0) {
        return false
    }

    return true
}

// Checks if the date is valid
const validateDate = (month, day, year) => {
    const dtMonth = month;
    const dtDay = day;
    const dtYear = year;
    if (dtMonth < 1 || dtMonth > 12) {
        return false;
    } else if (dtDay < 1 || dtDay > 31) {
        return false;
    } else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
        return false;
    } else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap)) {
            return false;
        }
    }
    return true;
}

// checks if the session exists by the identifier (yes -> Object and no -> null)
const getSession = (myIdentifier, eventId) => {
    return new Promise((resolve, reject) => {
        const db = admin.firestore()

        const ref = db.collection('events').doc(eventId).collection('sessions')

        ref
            .where('identifier', '==', myIdentifier)
            .get()
            .then((snapshot) => {
                if (snapshot.size > 0) {
                    snapshot.forEach((element) => {
                        const session = element.data()
                        resolve(session)
                    })
                } else {
                    reject(null)
                }
            })
    })
};


// verify that all sessions have been processed and return a response to the application
const response = (cont, total, res, arraySessions, arrayIdentifiersDouble, arrayGeneralErrors, messageErrors) => {
    if (cont >= total) {
        res.json({
            code: 200,
            message: 'success',
            result: {
                success: arraySessions,
                generalErrors: arrayGeneralErrors,
                idDouble: arrayIdentifiersDouble,
                'messageErrors': messageErrors
            }
        })
    }
}

// create new session
const createSession = (session) => {
    return new Promise((resolve, reject) => {
        const sessionModuleId = session.moduleId
        const eventId = session.eventId

        // start batch
        const db = admin.firestore()
        let batch = db.batch()

        // create document event
        let refEvent = db.collection("events").doc(eventId).collection("sessions").doc();
        const sessionId = refEvent.id;
        session.uid = refEvent.id;
        batch.set(refEvent, session)

        // create document module session
        let refModule = db.collection('modules').doc(sessionModuleId).collection('sessions').doc(sessionId)
        batch.set(refModule, session)


        //  Commit the batch
        batch.commit().then((batchOk) => {
            console.log("commit! :)");
            resolve(true)

        }).catch((error) => {
            console.log("Error! :( " + error)
            resolve(error)
        })
    })
}

// // update session
// // sessionEdit --> object with the new session information

const updateSession = (sessionEdit) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore()
        const sessionModuleId = sessionEdit.moduleId
        const eventId = sessionEdit.eventId
        const sessionId = sessionEdit.uid

        // start batch
        let batch = db.batch();

        // update the session on the event and module
        const ref1 = db.collection('events').doc(eventId).collection('sessions').doc(sessionId)
        const ref2 = db.collection('modules').doc(sessionModuleId).collection('sessions').doc(sessionId)

        batch.set(ref1, sessionEdit)
        batch.set(ref2, sessionEdit)

        // Commit the batch
        batch.commit().then((batchOk) => {
            console.log("commit! :)");
            resolve(true)

        }).catch((error) => {
            console.log("Error! :( " + error)
            reject(error)
        })
    })
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }


exports = module.exports = functions.firestore.document('events/{eventId}/sessions/{sessionId}').onUpdate((change, context) => {
    return new Promise(async(resolve) => {
        const updateSession = change.after.data()
        await updatePersonalAgenda(updateSession)
        resolve(true)
    })
})

// updates the session within the participant module.
const updatePersonalAgenda = (session) => {
    return new Promise((resolve, reject) => {
        const batch = admin.firestore().batch()
        const db = admin.firestore()

        const sessionId = session.uid
        const sessionModuleId = session.moduleId
        const eventId = session.eventId

        // personal agenda
        eventCalendarModule(eventId).then((modulePersonalAgenda) => {
            if (modulePersonalAgenda !== null) {
                const modulePersonalAgendaId = modulePersonalAgenda.uid

                // lists the participants who have the session on the personal agenda.
                getListOfSessionAttendees(sessionModuleId, sessionId).then((attendees) => {
                    for (const attendee of attendees) {
                        const attendeeId = attendee.uid

                        let ref = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)
                        batch.set(ref, session)
                    }


                    // Commit the batch
                    batch.commit().then(() => {
                        resolve(true)

                    }).catch((error) => {
                        reject(error)
                    })

                })

            } else {
                resolve(true)
            }
        })
    })
}

// receives the event uid and returns the event's agenda module
const eventCalendarModule = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const PERSONALSCHEDULE = 13 //agenda pessoal

        const ref = db.collection('events').doc(eventId).collection('modules');

        ref.where('type', '==', PERSONALSCHEDULE).get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                let module = null

                if (typeof childSnapshot.data() !== 'undefined' && childSnapshot.data() !== null) {
                    module = childSnapshot.data()
                }

                resolve(module)
            })
        })

    })
}

// lists the participants who have the session on the personal agenda.
const getListOfSessionAttendees = (moduleScheduleId, sessionId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).collection('attendees')

        ref
            .get()
            .then((snapshot) => {
                const list = []

                snapshot.forEach((childSnapshot) => {
                    list.push(childSnapshot.data())
                })

                resolve(list)
            })
    })
}
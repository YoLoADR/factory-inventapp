const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);


try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {

        let db = admin.firestore();
        let moduleId = req.body.moduleId;
        let refSessions = db.collection('modules').doc(moduleId).collection('sessions').orderBy('startTime');

        refSessions.onSnapshot((snapshot) => {
            const listSessions = []
            const dates = [];

            snapshot.forEach((snapshoted) => {
                let session = snapshoted.data();
                listSessions.push(setTheSession(session));

                // pick up all different dates of sessions
                const date = session.date;
                const index = dates.indexOf(date);

                if (index === -1) {
                    dates.push(date);
                }
            })

            res.json({
                sessions: listSessions,
                date: dates
            })

        })
    });
});

// clear the session fields for front view
setTheSession = (session) => {
    // import luxon functions for dates
    const defaultZoneName = require('../../utilities/luxon').defaultZoneName
    const dateTime = require('../../utilities/luxon').dateTime;
    const convertDateToStringIsNotUSA = require('../../utilities/luxon').convertDateToStringIsNotUSA;
    const convertTimestampToDate = require('../../utilities/luxon').convertTimestampToDate;

    // define zone default
    defaultZoneName()

    // dates
    session.date = convertDateToStringIsNotUSA(convertTimestampToDate(session.date))
    session.startTime = dateTime(convertTimestampToDate(session.startTime))

    if (session.endTime !== "" && session.endTime !== null && typeof session.endTime !== 'undefined') {
        session.endTime = dateTime(convertTimestampToDate(session.endTime))
    }

    // locations of session
    const locations = []

    for (const uid in session.locations) {
        locations.push(session.locations[uid])
    }

    session.locations = locations

    // tracks of session
    const tracks = []

    for (const uid in session.tracks) {
        tracks.push(session.tracks[uid])
    }

    session.tracks = tracks

    // groups of session
    const groups = []

    for (const uid in session.groups) {
        groups.push(session.groups[uid])
    }

    session.groups = groups


    return session
}
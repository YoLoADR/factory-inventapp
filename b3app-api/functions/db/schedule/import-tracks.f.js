const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest(async(req, res) => {
    cors(req, res, () => {
        main(req, res)
    });
});


/** 
 *main function
 *@param req
 *@param res
 */

const main = async(req, res) => {
    // get parameters
    let moduleId = req.body.moduleId;
    let eventId = req.body.eventId;
    let tracks = req.body.tracks;

    let db = admin.firestore();

    let arrayTracksCreated = [];
    let arrayTracksUpdated = [];
    let arraySessionsUpdate = []


    // checks if the track exists.
    for (const track of tracks) {
        const identifier = track.identifier
        let uid = await checkIdentifier(identifier, moduleId)

        // Updates the track.
        if (uid) {
            track.uid = uid
            arrayTracksUpdated.push(track);
            db.collection('modules').doc(moduleId).collection('tracks').doc(uid).update(track)
        }
        // Creates the track.
        else {
            arrayTracksCreated.push(track);
            let refTrack = db.collection('modules').doc(moduleId).collection('tracks').doc();
            track.uid = refTrack.id;
            refTrack.set(track);
        }
    }

    // For each updated track, search your sessions for updates.
    for (const track of arrayTracksUpdated) {
        const trackId = track.uid

        // get the sessions from the track.
        const sessions = await getTrackSessions(trackId, moduleId)
        for (const session of sessions) {
            const sessionId = session.uid
            const index = arraySessionsUpdate.map(function(e) { return e.uid; }).indexOf(sessionId);

            // takes the repetition of sessions.
            if (index > -1) {
                arraySessionsUpdate[index].tracks[trackId] = track
            } else {
                session.tracks[trackId] = track
                arraySessionsUpdate.push(session)
            }
        }
    }

    // update sessions
    for (const session of arraySessionsUpdate) {
        const sessionId = session.uid


        // activates the trigger: dbScheduuleEditSession
        db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(session)
        db.collection('modules').doc(moduleId).collection('sessions').doc(sessionId).update(session)
    }

    // returns the response from the api.
    response(res, arrayTracksUpdated, arrayTracksCreated)
}


/** 
 *Check if the track exists.
    @param identifier
    @param moduleId
    @return uid or null
*/
const checkIdentifier = (identifier, moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore();

        db
            .collection('modules')
            .doc(moduleId)
            .collection('tracks')
            .where('identifier', '==', identifier)
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve(null)

                snapshot.forEach((element) => {
                    resolve(element.data().uid)
                })
            })
    })
}


/** 
 *get the sessions from the track.
    @param trackId
    @param moduleId
    @return sessions
*/

const getTrackSessions = (trackId, moduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let sessions = []
        let queryTrack = `tracks.${trackId}`

        db.collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .orderBy(queryTrack)
            .get()
            .then((snapshot) => {
                snapshot.forEach((element) => {
                    const session = element.data()
                    sessions.push(session)
                })

                resolve(sessions)
            })
    })
}

/** 
 *response to api.
    @param res
    @param arrayUpdated
    @param arrayCreated
*/

const response = (res, arrayUpdated, arrayCreated) => {
    res.json({
        code: 200,
        message: 'success',
        result: {
            arrayCreated: arrayCreated,
            arrayUpdated: arrayUpdated
        }
    });
}
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let eventId = req.body.eventId;
        let moduleId = req.body.moduleId;
        let locations = req.body.locations;
        let cont = 0;
        let createdArray = [];
        let updatedArray = [];
        let failArray = [];

        for (let i = 0; i < locations.length; i++) {
            verifyIdentifier(moduleId, locations[i].identifier)
                .then((result) => {
                    if (result['status'] == true) {
                        // exists => update location
                        locations[i].uid = result['uid'];
                        updateLocation(moduleId, eventId, locations[i])
                            .then((__) => {
                                cont++;
                                updatedArray.push(locations[i]);
                                response(res, cont, locations.length, createdArray, updatedArray, failArray);
                            })
                            .catch((e) => {
                                cont++;
                                failArray.push(locations[i]);
                                response(res, cont, locations.length, createdArray, updatedArray, failArray);
                            })
                    } else {
                        // do not exist => create location
                        createLocation(moduleId, eventId, locations[i])
                            .then((_) => {
                                cont++;
                                createdArray.push(locations[i]);
                                response(res, cont, locations.length, createdArray, updatedArray, failArray);
                            })
                            .catch((err) => {
                                cont++;
                                failArray.push(locations[i]);
                                response(res, cont, locations.length, createdArray, updatedArray, failArray);
                            })
                    }
                });
        }
    });
});

const verifyIdentifier = (moduleId, identifier) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        db
            .collection('modules')
            .doc(moduleId)
            .collection('locations')
            .where('identifier', '==', identifier)
            .get()
            .then((values) => {
                let size = values.size;
                if (size >= 1) {
                    values.forEach(element => {
                        resolve({
                            status: true,
                            uid: element.data().uid
                        });
                    });

                } else {
                    resolve({
                        status: false
                    });
                }
            })
    });
}

const updateLocation = (moduleId, eventId, location) => {
    return new Promise(async(resolve, reject) => {
        let db = admin.firestore();
        let batch = db.batch();
        location = Object.assign({}, location);
        location.eventId = eventId;
        location.moduleId = moduleId;
        let refEvent = db.collection('events').doc(eventId).collection('locations').doc(location.uid);
        let refModule = db.collection('modules').doc(moduleId).collection('locations').doc(location.uid);

        // calls the trigger dbLocationsEditLocation from the API.  
        batch.update(refModule, location);
        batch.update(refEvent, location);

        batch.commit()
            .then((_) => {
                resolve(true);
            })
            .catch((error) => {
                console.log(error)
                reject(false);
            });
    });
}

const createLocation = (moduleId, eventId, location) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        let batch = db.batch();
        location = Object.assign({}, location);
        location.eventId = eventId;
        location.moduleId = moduleId;
        let refEvent = db.collection('events').doc(eventId).collection('locations').doc();
        location.uid = refEvent.id;
        let refModule = db.collection('modules').doc(moduleId).collection('locations').doc(location.uid);

        batch.set(refModule, location);
        batch.set(refEvent, location);

        batch.commit()
            .then((_) => {
                resolve(true);
            })
            .catch((error) => {
                reject(false);
            });

    });
}

// get sessions of location
const getSessionsOfLocation = (reference) => {
    return new Promise((resolve) => {
        const sessions = []
        reference
            .get()
            .then((snapshot) => {
                snapshot.forEach((childSnapshot) => {
                    const session = childSnapshot.data()
                    sessions.push(session)
                })

                resolve(sessions)
            })
    })
}

function response(res, cont, totalSize, createdArray, updatedArray, failArray) {
    if (cont == totalSize) {
        res.json({
            code: 200,
            message: 'success',
            result: {
                created: createdArray,
                updated: updatedArray,
                fail: failArray
            }
        });
    }
}
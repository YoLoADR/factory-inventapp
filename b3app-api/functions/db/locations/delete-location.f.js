const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }



exports = module.exports = functions.firestore.document('events/{eventId}/locations/{locationId}').onDelete(async(snap, context) => {
    // get location data
    const location = snap.data()
    const locationId = location.uid
    const moduleId = location.moduleId
    const eventId = context.params.eventId

    //  excludes location from within sessions.
    return await removeSessions(locationId, moduleId, eventId)
})

//remove sessions from location
const removeSessions = (locationId, moduleLocationId, eventId) => {
    return new Promise(async(resolve) => {
        const db = admin.firestore()

        // Lists all schedule modules of the event.
        const modules = await getModulesSheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the location.
            const sessions = await getSessionsOfLocation(locationId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                // delete location.
                delete session.locations[locationId]

                // check if other session locations exist.
                for (const index in session.locations) {
                    const location = session.locations[index]
                    const locationId = location.uid

                    // If the location does not exist, remove it from the session.
                    if (!await checkLocationExist(location)) {
                        delete session.locations[locationId]
                    }
                }

                // update session
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(session)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(session)
            }
        }

        resolve(true)
    })
}

// Lists all schedule modules of the event.
const getModulesSheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the sessions of the module that has the location.
const getSessionsOfLocation = (locationId, moduleScheduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const location = `locations.${locationId}`

        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(location)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

// check if the location exists.
const checkLocationExist = (location) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const moduleLocationId = location.moduleId
        const locationId = location.uid

        const ref = db.collection('modules').doc(moduleLocationId).collection('locations').doc(locationId)

        ref.get().then((snapshot) => {
            const exist = typeof snapshot.data() !== 'undefined' ? true : false
            resolve(exist)
        })
    })
}
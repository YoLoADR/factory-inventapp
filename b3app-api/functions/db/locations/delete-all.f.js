const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });
const getCollection = require('./../../utilities/get-collection');

// get Sessions of locations
const getSessions = (path1, path2) => {
    return new Promise((resolve) => {
        path1
            .get()
            .then((snapshot) => {
                const list = []
                const total = snapshot.size

                if (total > 0) {
                    snapshot.forEach((childSnapshot) => {
                        const sessionId = childSnapshot.data().uid

                        path2.doc(sessionId)
                            .get()
                            .then((snapshot) => {
                                const session = snapshot.data()
                                list.push(session)

                                if (list.length >= total) {
                                    resolve(list)
                                }
                            })
                    })
                } else {
                    resolve(list)
                }
            })
    })
}


exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        const listRemoveUids = req.body.listRemoveUids;
        const moduleId = req.body.moduleId;
        const eventId = req.body.eventId;

        let db = admin.firestore()

        let size = listRemoveUids.length;
        let cont = 0;
        for (let locationId of listRemoveUids) {
            // paths
            const pathLocationsEvent = db.collection('events').doc(eventId).collection('locations').doc(locationId).collection('sessions')
            const pathSessionsEvent = db.collection('events').doc(eventId).collection('sessions')

            Promise.all(
                [
                    getSessions(pathLocationsEvent, pathSessionsEvent),
                ]
            ).then((results) => {
                const sessions = results[0]


                let batch = admin.firestore().batch();

                // update sessions
                for (const session of sessions) {
                    const sessionId = session.uid
                    const moduleScheduleId = session.moduleId

                    delete session.locations[locationId]

                    // path event
                    const ref1 = db.collection('events').doc(eventId).collection('locations').doc(locationId).collection('sessions').doc(sessionId)
                    const ref2 = db.collection('events').doc(eventId).collection('sessions').doc(sessionId)

                    batch.delete(ref1)
                    batch.set(ref2, session)

                    // path modules
                    const ref3 = db.collection('modules').doc(moduleId).collection('locations').doc(locationId).collection('sessions').doc(sessionId)
                    const ref4 = db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId)

                    batch.delete(ref3)
                    batch.set(ref4, session)
                }


                // remove location
                let refModule = db.collection('modules').doc(moduleId).collection('locations').doc(locationId);
                let refEvent = db.collection('events').doc(eventId).collection('locations').doc(locationId);

                batch.delete(refModule);
                batch.delete(refEvent);

                batch.commit().then(() => {
                        if (cont == size - 1) {
                            res.json({
                                code: 200,
                                message: 'success',
                                result: true
                            });
                        }

                        cont++;
                    })
                    .catch((err) => {
                        if (cont == size - 1) {
                            res.json({
                                code: error.code,
                                message: 'error',
                                result: false
                            });
                        }

                        cont++;
                    })
            })
        }
    });
});
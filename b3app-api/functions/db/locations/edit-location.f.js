const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }

// const cors = require("cors")({ origin: true });
// const getCollection = require('./../../utilities/get-collection');

exports = module.exports = functions.firestore.document('events/{eventId}/locations/{locationId}').onUpdate((change, context) => {
    return new Promise(async(resolve) => {
        const updatedLocation = change.after.data();
        const eventId = context.params.eventId

        await updateSessions(updatedLocation, eventId)

        resolve(true)
    })
})

const updateSessions = (location, eventId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore();
        const locationId = location.uid

        // Lists all schedule modules of the event.
        const modules = await getModulesScheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the location.
            const sessions = await getSessionsOfLocation(locationId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                // update session.
                location.order = session.locations[locationId].order
                session.locations[locationId] = location
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(session)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(session)
            }
        }

        resolve(true)
    })
}


// Lists all schedule modules of the event.
const getModulesScheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        const list = []
        let db = admin.firestore()
        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

// lists the sessions of the module that has the location.
const getSessionsOfLocation = (locationId, moduleScheduleId) => {
    return new Promise((resolve) => {
        const list = []
        const location = `locations.${locationId}`

        let db = admin.firestore()
        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(location)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}
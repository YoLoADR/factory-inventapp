const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

const TypeModule = require("../../utilities/enums/type-module")

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let db = admin.firestore();
        let moduleId = req.body.moduleId;
        let eventId = req.body.eventId;
        let group = req.body.group;

        // UPDATE GROUP
        let refGroupModule = db.collection('modules').doc(moduleId).collection('groups').doc(group.uid);
        let refGroupEvent = db.collection('events').doc(eventId).collection('modules').doc(moduleId).collection('groups').doc(group.uid);
        refGroupModule.update(group);
        refGroupEvent.update(group);

        Promise.all(
                [
                    updateSessions(eventId, group),
                    updateAttendees(eventId, group),
                    updateSpeakers(eventId, group),
                    updateModules(eventId, group),
                    updateWidgets(eventId, group),
                    updateCheckins(eventId, group),
                    updateFoldersDocuments(eventId, group)
                ]
            )
            .then((result) => {
                console.log(result)
                responseOk(res);
            })
            .catch((err) => {
                console.log(err)
                responseFail(res);
            });
    })
});

// /***************************************************** sessions ****************************************************************************** */

/**
 * update the sessions that own the group.
 * @param eventId  
 * @param group 
 * @returns Promise boolean
 */

const updateSessions = (eventId, group) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        let groupId = group.uid
        const modules = await getModulesSheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the group.
            const sessions = await getSessionsOfGroup(groupId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                let updateGroup = {}
                updateGroup[`groups.${groupId}`] = group

                // update session
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(updateGroup)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(updateGroup)
            }
        }

        resolve(true)
    });
}

/**
 * Lists all schedule modules of the event.
 * @param eventId  
 * @returns list
 */
const getModulesSheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

/**
 * lists the sessions of the module that has the group.
 * @param groupId
 * @param moduleScheduleId  
 * @returns list
 */
const getSessionsOfGroup = (groupId, moduleScheduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const group = `groups.${groupId}`


        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(group)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

// /***************************************************** attendees ****************************************************************************** */
/**
 * update the attendees that own the group.
 * @param eventId  
 * @param group 
 * @returns Promise boolean
 */

const updateAttendees = (eventId, group) => {
    return new Promise((resolve) => {
        // UPDATE GROUP IN ATTENDEES (MODULES/EVENTS)
        let db = admin.firestore();
        let refEventModules = db.collection('events').doc(eventId).collection('modules');
        let refAttendeesEvent = db.collection('events').doc(eventId).collection('attendees');
        let refAttendeesModule = db.collection('modules');

        refEventModules
            .where('type', '==', 2)
            .get()
            .then((values) => {
                let contModule = 0;
                if (values.size >= 1) {
                    values.forEach(el => {
                        let module = el.data();

                        refAttendeesModule
                            .doc(module.uid)
                            .collection('attendees')
                            .get()
                            .then((valueAttendees) => {
                                let contAttendees = 0;
                                if (valueAttendees.size >= 1) {
                                    valueAttendees.forEach(element => {
                                        let attendee = element.data();
                                        let updateAttendees = {};

                                        updateAttendees[`groups.${group.uid}`] = group;
                                        if (attendee.groups[group.uid] !== undefined) {
                                            refAttendeesModule.doc(module.uid).collection('attendees').doc(attendee.uid).update(updateAttendees);
                                            refAttendeesEvent.doc(attendee.uid).update(updateAttendees);
                                        }

                                        if (contModule == values.size - 1 && contAttendees == valueAttendees.size - 1) {
                                            resolve(true);
                                        }
                                        if (contAttendees == valueAttendees.size - 1) {
                                            contModule++;
                                        }
                                        contAttendees++;
                                    });
                                } else {
                                    if (contModule == values.size - 1) {
                                        resolve(true);
                                    }
                                    contModule++;
                                }
                            })
                    });
                } else {
                    resolve(true);
                }
            });
    })
}

// /***************************************************** speakers ****************************************************************************** */
/**
 * update the speakers that own the group.
 * @param eventId  
 * @param group 
 * @returns Promise boolean
 */

const updateSpeakers = (eventId, group) => {
    return new Promise((resolve) => {
        // UPDATE GROUP IN SPEAKERS (MODULES/EVENTS)
        let db = admin.firestore();
        let refEventModules = db.collection('events').doc(eventId).collection('modules');
        let refSpeakersEvent = db.collection('events').doc(eventId).collection('speakers');
        let refSpeakersModule = db.collection('modules');

        refEventModules
            .where('type', '==', 1)
            .get()
            .then((values) => {
                let contModule = 0;
                if (values.size >= 1) {
                    values.forEach(el => {
                        let module = el.data();

                        refSpeakersModule
                            .doc(module.uid)
                            .collection('speakers')
                            .get()
                            .then((valueSpeakers) => {
                                let contSpeakers = 0;
                                if (valueSpeakers.size >= 1) {
                                    valueSpeakers.forEach(element => {
                                        let speaker = element.data();
                                        let updateSpeakers = {};

                                        updateSpeakers[`groups.${group.uid}`] = group;
                                        if (speaker.groups[group.uid] !== undefined) {
                                            refSpeakersModule.doc(module.uid).collection('speakers').doc(speaker.uid).update(updateSpeakers);
                                            refSpeakersEvent.doc(speaker.uid).update(updateSpeakers);
                                        }

                                        if (contModule == values.size - 1 && contSpeakers == valueSpeakers.size - 1) {
                                            resolve(true);
                                        }
                                        if (contSpeakers == valueSpeakers.size - 1) {
                                            contModule++;
                                        }
                                        contSpeakers++;
                                    });
                                } else {
                                    if (contModule == values.size - 1) {
                                        resolve(true);
                                    }
                                    contModule++;
                                }
                            })
                    });
                } else {
                    resolve(true);
                }
            });
    })
}

// /***************************************************** modules ****************************************************************************** */

/**
 * update the modules that own the group.
 * @param eventId  
 * @param group
 * @returns Promise boolean
 */
const updateModules = (eventId, group) => {
    return new Promise((resolve) => {
        let db = admin.firestore();
        const groupId = group.uid

        const access_groups = `access_groups.${groupId}`
        const ref = db.collection('events')
            .doc(eventId)
            .collection('modules')
            .orderBy(access_groups)
            .get()

        ref.then((snapshot) => {
            if (snapshot.size <= 0)
                resolve(true)

            snapshot.forEach((element) => {
                const module = element.data()
                const moduleId = module.uid

                let updateGroup = {}
                updateGroup[`access_groups.${groupId}`] = group

                db.collection('events').doc(eventId).collection('modules').doc(moduleId).update(updateGroup)
                db.collection('modules').doc(moduleId).update(updateGroup)
            })

            resolve(true)
        })
    })
}

/********************************************************************************* WIDGETS **************************************************************/

/**
 * update the widgets that have the group.
 * @param eventId  
 * @param group
 * @returns Promise boolean
 */
const updateWidgets = (eventId, group) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        const groupId = group.uid

        // get the event widget modules.
        const modules = await getModulesWidgetsEvent(eventId)

        for (const module of modules) {
            const moduleId = module.uid

            // Get the widgets of the module that has the group.
            const widgets = await getWidgetsModule(groupId, moduleId)

            // update widgets
            for (const widget of widgets) {
                const widgetId = widget.uid
                let updateGroup = {}
                updateGroup[`groups.${groupId}`] = group

                db.collection('modules').doc(moduleId).collection('widgets').doc(widgetId)
                    .update(updateGroup)
            }
        }

        resolve(true)
    })
}

/**
 * get the event widget modules.
 * @param eventId  
 * @returns list
 */
const getModulesWidgetsEvent = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const WIDGETS = typeModules.WIDGETS
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', WIDGETS)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

/**
 * Get the widgets of the module that has the group.
 * @param groupId 
 * @param moduleId   
 * @returns list
 */
const getWidgetsModule = (groupId, moduleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const list = []
        const groups = `groups.${groupId}`

        const ref = db.collection('modules').doc(moduleId).collection('widgets').orderBy(groups)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

/********************************************************************************* CHECKIN **************************************************************/
/**
 * update the checkin that have the group.
 * @param eventId  
 * @param group  
 * @returns Promise boolean
 */

const updateCheckins = (eventId, group) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        const groupId = group.uid
            // get the event checkin modules.
        const module = await getCheckinModule(eventId)

        if (module) {
            const checkins = await getCheckinsEvent(eventId)

            for (const checkin of checkins) {
                const checkinId = checkin.uid
                const checkinModuleId = checkin.moduleId

                // updates the checkins that own the group.
                if (checkin.groups[groupId]) {
                    let updateGroup = {}
                    updateGroup[`groups.${groupId}`] = group

                    // update checkin
                    db.collection('modules').doc(checkinModuleId).collection('checkins').doc(checkinId)
                        .update(updateGroup)
                }

                // get attendees group attendees 
                const attendees = await getAttendeesOfGroup(group, checkinId, checkinModuleId)

                for (const attendee of attendees) {
                    const attendeeId = attendee.uid
                    let updateGroup = {}
                    updateGroup[`groups.${groupId}`] = group
                        // update attendee
                    db.collection('modules').doc(checkinModuleId).collection('checkins').doc(checkinId).collection('attendees').doc(attendeeId)
                        .update(updateGroup)
                }
            }
        }

        resolve(true)
    })
}


/**
 * get the wind check in module.
 * @param {string} eventId  
 * @returns module
 */

const getCheckinModule = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const CHECKIN = TypeModule.CHECKIN
        let module = null

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', CHECKIN)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                module = element.data()
            })

            resolve(module)
        })
    })
}

/**
 * Search for participants who belong to the group at check-in.
 * @param {string} group
 * @param {string} checkinId  
 * @param {string} checkinModuleId  
 * @returns attendees[]
 */

const getAttendeesOfGroup = (group, checkinId, checkinModuleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const attendees = []
        const groupId = group.uid
        const groups = `groups.${groupId}`

        const ref = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .orderBy(groups)

        ref.get().then((snapshot) => {
            if (snapshot.size <= 0)
                resolve([])

            snapshot.forEach((element) => {
                const attendee = element.data()
                attendees.push(attendee)
            })

            resolve(attendees)
        })
    })
}


/**
 * get all checks in of the event.  
 * @param eventId  
 * @returns checkin[]
 */

const getCheckinsEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let checkins = []

        // get checkins module
        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', typeModules.CHECKIN)
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve([])

                // get all module checkins.
                snapshot.forEach((element) => {
                    const module = element.data()
                    const moduleId = module.uid

                    db.collection('modules')
                        .doc(moduleId)
                        .collection('checkins')
                        .get()
                        .then((childSnapshot) => {
                            childSnapshot.forEach((element) => {
                                const checkin = element.data()
                                checkins.push(checkin)
                            })

                            resolve(checkins)
                        })
                })
            })
    })
}

/********************************************************************************* DOCUMENTS **************************************************************/

/**
 * updates the document module folders. 
 * @param eventId 
 * @param group  
 * @returns Promise boolean
 */

const updateFoldersDocuments = (eventId, group) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        const groupId = group.uid

        const modules = await getDocumentModules(eventId)

        for (const module of modules) {
            const moduleId = module.uid
            const folders = await getFoldersOfModule(group, moduleId)

            // update checkins
            for (const folder of folders) {
                const folderId = folder.uid
                const updateGroup = {}
                updateGroup[`groups.${groupId}`] = group

                db.collection('modules').doc(moduleId).collection('folders').doc(folderId).update(updateGroup)
            }
        }

        resolve(true)
    })
}

/**
 * get the document modules. 
 * @param eventId 
 * @returns modules
 */
const getDocumentModules = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const DOCUMENT = typeModules.DOCUMENT
        let modules = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', DOCUMENT)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                let module = element.data()
                modules.push(module)
            })

            resolve(modules)
        })
    })
}


/**
 * Get the folders of the module that has the group. 
 * @param group 
 * @param moduleId 
 * @returns list
 */
const getFoldersOfModule = (group, moduleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const list = []
        const groupId = group.uid
        const groups = `groups.${groupId}`

        const ref = db.collection('modules').doc(moduleId).collection('folders').orderBy(groups)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                const document = element.data()
                list.push(document)
            })

            resolve(list)
        })

    })
}




function responseOk(res) {
    res.json({
        code: 200,
        message: 'success',
        result: true
    });
}

function responseFail(res) {
    res.json({
        code: 404,
        message: 'error',
        result: false
    });
}
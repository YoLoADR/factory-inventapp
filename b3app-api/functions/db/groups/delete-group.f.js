const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const typeModules = require('../../utilities/enums/type-module') //the module types file is imported.

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }


exports = module.exports = functions.firestore.document('modules/{moduleId}/groups/{groupId}').onDelete((snap, context) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()

        const group = snap.data()
        const groupId = group.uid
        const moduleId = group.moduleId
        const eventId = group.eventId

        // deletes session from the event path.
        db.collection('events')
            .doc(eventId)
            .collection('modules')
            .doc(moduleId)
            .collection('groups')
            .doc(groupId)
            .delete()

        await updateSessions(groupId, eventId)
        await updateAttendees(eventId, groupId)
        await updateSpeakers(eventId, groupId)
        await updateModules(eventId, groupId)
        await updateWidgets(eventId, groupId)
        await updateCheckins(eventId, groupId)
        await updateFoldersDocuments(eventId, group)

        resolve(true)
    })
})



// /***************************************************** sessions ****************************************************************************** */

/**
 * update the sessions that own the group.
 * @param eventId  
 * @param groupId 
 * @returns Promise boolean
 */
const updateSessions = (groupId, eventId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()

        const modules = await getModulesSheduleEvent(eventId)

        for (const module of modules) {
            const moduleScheduleId = module.uid

            // lists the sessions of the module that has the group.
            const sessions = await getSessionsOfGroup(groupId, moduleScheduleId)

            // update sessions
            for (const session of sessions) {
                const sessionId = session.uid

                let fieldValue = admin.firestore.FieldValue
                let deleteGroup = {}
                deleteGroup[`groups.${groupId}`] = fieldValue.delete()

                // update session
                db.collection('events').doc(eventId).collection('sessions').doc(sessionId).update(deleteGroup)
                db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).update(deleteGroup)
            }
        }

        resolve(true)
    })
}


/**
 * Lists all schedule modules of the event.
 * @param eventId  
 * @returns list
 */
const getModulesSheduleEvent = (eventId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', typeModules.SCHEDULE)

        ref.get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                list.push(childSnapshot.data())
            })

            resolve(list)
        })

    })
}

/**
 * lists the sessions of the module that has the group.
 * @param groupId
 * @param moduleScheduleId  
 * @returns list
 */
const getSessionsOfGroup = (groupId, moduleScheduleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()

        const list = []
        const group = `groups.${groupId}`


        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').orderBy(group)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

// /***************************************************** attendees ****************************************************************************** */

/**
 * update the attendees that own the group.
 * @param eventId  
 * @param uid 
 * @returns Promise boolean
 */

const updateAttendees = (eventId, uid) => {
    return new Promise((resolve, reject) => {
        // UPDATE GROUP IN ATTENDEES (MODULES/EVENTS)
        let db = admin.firestore();
        let refEventModules = db.collection('events').doc(eventId).collection('modules');
        let refAttendeesEvent = db.collection('events').doc(eventId).collection('attendees');
        let refAttendeesModule = db.collection('modules');

        refEventModules
            .where('type', '==', 2)
            .get()
            .then((values) => {
                let contModule = 0;
                if (values.size >= 1) {
                    values.forEach(el => {
                        let module = el.data();

                        refAttendeesModule
                            .doc(module.uid)
                            .collection('attendees')
                            .get()
                            .then((valueAttendees) => {
                                let contAttendees = 0;
                                if (valueAttendees.size >= 1) {
                                    valueAttendees.forEach(element => {
                                        let attendee = element.data();
                                        let updateAttendees = {};
                                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                                        updateAttendees[`groups.${uid}`] = FieldValue.delete();
                                        if (attendee.groups[uid] !== undefined) {
                                            refAttendeesModule.doc(module.uid).collection('attendees').doc(attendee.uid).update(updateAttendees);
                                            refAttendeesEvent.doc(attendee.uid).update(updateAttendees);
                                        }

                                        if (contModule == values.size - 1 && contAttendees == valueAttendees.size - 1) {
                                            resolve(true);
                                        }
                                        if (contAttendees == valueAttendees.size - 1) {
                                            contModule++;
                                        }
                                        contAttendees++;
                                    });
                                } else {
                                    if (contModule == values.size - 1) {
                                        resolve(true);
                                    }
                                    contModule++;
                                }
                            })
                    });
                } else {
                    resolve(true);
                }
            });
    })
}

// /***************************************************** speakers ****************************************************************************** */

/**
 * update the speakers that own the group.
 * @param eventId  
 * @param uid 
 * @returns Promise boolean
 */
const updateSpeakers = (eventId, uid) => {
    return new Promise((resolve, reject) => {
        // UPDATE GROUP IN SPEAKERS (MODULES/EVENTS)
        let db = admin.firestore();
        let refEventModules = db.collection('events').doc(eventId).collection('modules');
        let refSpeakersEvent = db.collection('events').doc(eventId).collection('speakers');
        let refSpeakersModule = db.collection('modules');

        refEventModules
            .where('type', '==', 1)
            .get()
            .then((values) => {
                let contModule = 0;
                if (values.size >= 1) {
                    values.forEach(el => {
                        let module = el.data();

                        refSpeakersModule
                            .doc(module.uid)
                            .collection('speakers')
                            .get()
                            .then((valueSpeakers) => {
                                let contSpeakers = 0;
                                if (valueSpeakers.size >= 1) {
                                    valueSpeakers.forEach(element => {
                                        let speaker = element.data();
                                        let updateSpeakers = {};
                                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                                        updateSpeakers[`groups.${uid}`] = FieldValue.delete();
                                        if (speaker.groups[uid] !== undefined) {
                                            refSpeakersModule.doc(module.uid).collection('speakers').doc(speaker.uid).update(updateSpeakers);
                                            refSpeakersEvent.doc(speaker.uid).update(updateSpeakers);
                                        }

                                        if (contModule == values.size - 1 && contSpeakers == valueSpeakers.size - 1) {
                                            resolve(true);
                                        }
                                        if (contSpeakers == valueSpeakers.size - 1) {
                                            contModule++;
                                        }
                                        contSpeakers++;
                                    });
                                } else {
                                    if (contModule == values.size - 1) {
                                        resolve(true);
                                    }
                                    contModule++;
                                }
                            })
                    });
                } else {
                    resolve(true);
                }
            });
    })
}

// /***************************************************** modules ****************************************************************************** */

/**
 * update the modules that own the group.
 * @param eventId  
 * @param groupId
 * @returns Promise boolean
 */
const updateModules = (eventId, groupId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore();

        const group = `access_groups.${groupId}`
        const ref = db.collection('events')
            .doc(eventId)
            .collection('modules')
            .orderBy(group)
            .get()

        ref.then((snapshot) => {
            if (snapshot.size <= 0)
                resolve(true)

            snapshot.forEach((element) => {
                const module = element.data()
                const moduleId = module.uid

                let fieldValue = admin.firestore.FieldValue
                let deleteGroup = {}
                deleteGroup[`access_groups.${groupId}`] = fieldValue.delete()

                db.collection('events').doc(eventId).collection('modules').doc(moduleId).update(deleteGroup)
                db.collection('modules').doc(moduleId).update(deleteGroup)
            })

            resolve(true)
        })
    })
}


// /********************************************************************************* WIDGETS **************************************************************/

/**
 * update the widgets that have the group
 * @param eventId  
 * @param groupId
 * @returns Promise boolean
 */

const updateWidgets = (eventId, groupId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()

        // get the event widget modules.
        const modules = await getModulesWidgetsEvent(eventId)

        for (const module of modules) {
            const moduleId = module.uid

            // Get the widgets of the module that has the group.
            const widgets = await getWidgetsModule(groupId, moduleId)

            // update widgets
            for (const widget of widgets) {
                const widgetId = widget.uid
                let fieldValue = admin.firestore.FieldValue
                let deleteGroup = {}
                deleteGroup[`groups.${groupId}`] = fieldValue.delete()

                db.collection('modules').doc(moduleId).collection('widgets').doc(widgetId).update(deleteGroup)
            }
        }

        resolve(true)
    })
}

/**
 * get the event widget modules.
 * @param eventId  
 * @returns list
 */

const getModulesWidgetsEvent = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const WIDGETS = typeModules.WIDGETS
        const list = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', WIDGETS)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

/**
 * Get the widgets of the module that has the group.
 * @param groupId 
 * @param moduleId   
 * @returns list
 */
const getWidgetsModule = (groupId, moduleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const list = []
        const groups = `groups.${groupId}`

        const ref = db.collection('modules').doc(moduleId).collection('widgets').orderBy(groups)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                list.push(element.data())
            })

            resolve(list)
        })
    })
}

/********************************************************************************* CHECKIN **************************************************************/

/**
 * update the checkin that have the group.
 * @param eventId  
 * @param group  
 * @returns Promise boolean
 */
const updateCheckins = (eventId, groupId) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()

        // get the wind check in module.
        const module = await getCheckinModule(eventId)

        if (module) {
            const checkinModuleId = module.uid

            // Fetch all the module event check ins. 
            const checkins = await getCheckinListModule(checkinModuleId)
            for (const checkin of checkins) {
                const checkinId = checkin.uid

                // updates the checkins that own the group.
                let fieldValue = admin.firestore.FieldValue
                let deleteGroup = {}
                deleteGroup[`groups.${groupId}`] = fieldValue.delete()


                // update groups
                db.collection('modules').doc(checkinModuleId).collection('checkins').doc(checkinId).update(deleteGroup)


                // get attendees group attendees 
                const attendees = await getAttendeesOfGroup(groupId, checkinId, checkinModuleId)
                for (const attendee of attendees) {
                    const attendeeId = attendee.uid
                    let fieldValue = admin.firestore.FieldValue
                    let deleteGroup = {}
                    deleteGroup[`groups.${groupId}`] = fieldValue.delete()

                    // update attendee
                    db.collection('modules').doc(checkinModuleId).collection('checkins').doc(checkinId).collection('attendees').doc(attendeeId)
                        .update(deleteGroup)
                }
            }
        }

        resolve(true)
    })
}

/**
 * get the wind check in module.
 * @param {string} eventId  
 * @returns module
 */

const getCheckinModule = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const CHECKIN = typeModules.CHECKIN
        let module = null

        const ref = db.collection('events')
            .doc(eventId).collection('modules')
            .where('type', '==', CHECKIN)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                module = element.data()
            })

            resolve(module)
        })
    })
}

/**
 * Search for participants who belong to the group at check-in.
 * @param {string} group
 * @param {string} checkinId  
 * @param {string} checkinModuleId  
 * @returns attendees[]
 */

const getAttendeesOfGroup = (groupId, checkinId, checkinModuleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const attendees = []
        const groups = `groups.${groupId}`

        const ref = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .orderBy(groups)

        ref.get().then((snapshot) => {
            if (snapshot.size <= 0)
                resolve([])

            snapshot.forEach((element) => {
                const attendee = element.data()
                attendees.push(attendee)
            })

            resolve(attendees)
        })
    })
}



/**
 * Fetch all the module event check ins.  
 * @param checkinModuleId  
 * @returns checkin[]
 */

const getCheckinListModule = (checkinModuleId) => {
    return new Promise((resolve) => {
        let db = admin.firestore()
        let checkins = []

        // get checkins module
        db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .get()
            .then((snapshot) => {
                if (snapshot.size <= 0)
                    resolve([])

                snapshot.forEach((element) => {
                    const checkin = element.data()
                    checkins.push(checkin)
                })

                resolve(checkins)
            })
    })
}


/********************************************************************************* DOCUMENTS **************************************************************/
/**
 * updates the document module folders. 
 * @param eventId 
 * @param group  
 * @returns Promise boolean
 */

const updateFoldersDocuments = (eventId, group) => {
    return new Promise(async(resolve) => {
        let db = admin.firestore()
        const groupId = group.uid

        const modules = await getDocumentModules(eventId)

        for (const module of modules) {
            const moduleId = module.uid
            const folders = await getFoldersOfModule(group, moduleId)

            // update checkins
            for (const folder of folders) {
                const folderId = folder.uid

                let fieldValue = admin.firestore.FieldValue
                let deleteGroup = {}
                deleteGroup[`groups.${groupId}`] = fieldValue.delete()

                db.collection('modules').doc(moduleId).collection('folders').doc(folderId).update(deleteGroup)
            }
        }

        resolve(true)
    })
}


/**
 * get the document modules. 
 * @param eventId 
 * @returns modules
 */
const getDocumentModules = (eventId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const DOCUMENT = typeModules.DOCUMENT
        let modules = []

        const ref = db.collection('events').doc(eventId).collection('modules').where('type', '==', DOCUMENT)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                let module = element.data()
                modules.push(module)
            })

            resolve(modules)
        })
    })
}


/**
 * Get the folders of the module that has the group. 
 * @param group 
 * @param moduleId 
 * @returns list
 */
const getFoldersOfModule = (group, moduleId) => {
    return new Promise((resolve) => {
        const db = admin.firestore()
        const list = []
        const groupId = group.uid
        const groups = `groups.${groupId}`

        const ref = db.collection('modules').doc(moduleId).collection('folders').orderBy(groups)

        ref.get().then((snapshot) => {
            snapshot.forEach((element) => {
                const document = element.data()
                list.push(document)
            })

            resolve(list)
        })

    })
}
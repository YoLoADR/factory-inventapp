/**
 *  CONFIG B3APP DEVELOP (b3app-develop project)
 *  TO DEPLOY: COMMENT OTHER EXPORTS AND DESCOMING THIS.
 *  USE THE COMMAND BELOW TO DIRECT TO THE CORRECT PROJECT
 
 *  firebase deploy --only functions --project b3app-develop
 
 */

// module.exports = {
//     master_key: "b3app-develop-firebase-adminsdk-aphcz-fef6fc5aa8.json",
//     firebase_url: "https://b3app-develop.firebaseio.com",
//     onesignal_appid: 'f7b1610c-09b2-4588-af3d-f2a07a60534b',
//     notification_api_id: 'NDgyM2RkNDctZTYxZi00YTBiLTlhYzItZjIxMWUwN2ZmMGFi',
//     notification_app_id: 'f7b1610c-09b2-4588-af3d-f2a07a60534b',
//     mailgun_api_key: '9adf7819405335f249997315deb49e18-b3780ee5-c6600f6e',
//     mailgun_domain: 'mg.ceuapp.net',
//     mailgun_noreply_mail: 'noreply@ceuapp.net',
//     default_language: 'pt_BR',
//     wherebyApiKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmFwcGVhci5pbiIsImF1ZCI6Imh0dHBzOi8vYXBpLmFwcGVhci5pbi92MSIsImV4cCI6OTAwNzE5OTI1NDc0MDk5MSwiaWF0IjoxNTg4MDA2ODgxLCJvcmdhbml6YXRpb25JZCI6MjI5NTksImp0aSI6IjZmYjAzMGMzLWZiYTgtNGM5Ny04MTM4LTExMDQ2OWNlYWE0MyJ9.Zs784pC-EGZ5usX35DDsINHFZi039oflstPYfdKc_bQ"
// }

/**
 *  CONFIG B3APP PRODUCTION (b3app-prod project)
 *  TO DEPLOY: COMMENT OTHER EXPORTS AND DESCOMING THIS.
 *  USE THE COMMAND BELOW TO DIRECT TO THE CORRECT PROJECT

 *  firebase deploy --only functions --project b3app-prod

 */

// module.exports = {
//     master_key: "b3app-prod-firebase-adminsdk-60e5v-0eeba75f8c.json",
//     firebase_url: "https://b3app-prod.firebaseio.com",
//     onesignal_appid: '0d6840c7-d45e-475c-add0-62e3c1deed5f',
//     notification_api_id: 'ZDk4ZjMxM2YtNzY5NC00ZWYxLThiYTItYjMxYzdjNTFjMDBj',
//     notification_app_id: '0d6840c7-d45e-475c-add0-62e3c1deed5f',
//     mailgun_api_key: '9adf7819405335f249997315deb49e18-b3780ee5-c6600f6e',
//     mailgun_domain: 'mg.ceuapp.net',
//     mailgun_noreply_mail: 'noreply@ceuapp.net',
//     default_language: 'en_US'
// }


/**
 *  CONFIG B3APP TEST (b3app-master project)
 *  TO DEPLOY: COMMENT OTHER EXPORTS AND DESCOMING THIS.
 *  USE THE COMMAND BELOW TO DIRECT TO THE CORRECT PROJECT

 *  firebase deploy --only functions --project b3app-master

 */

module.exports = {
    master_key: "b3app-master-firebase-adminsdk-8s5v3-7aea723b07.json",
    firebase_url: "https://b3app-master.firebaseio.com",
    onesignal_appid: '4632b112-ea3d-4866-918a-343650b2d24a',
    notification_api_id: 'ZGFiY2YxYTEtYzU2ZC00ZWM1LWE2NjItY2E2OWFjN2M3NmJl',
    notification_app_id: 'f7b1610c-09b2-4588-af3d-f2a07a60534b',
    mailgun_api_key: '9adf7819405335f249997315deb49e18-b3780ee5-c6600f6e',
    mailgun_domain: 'mg.ceuapp.net',
    mailgun_noreply_mail: 'noreply@ceuapp.net',
    default_language: 'en_US',
    wherebyApiKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmFwcGVhci5pbiIsImF1ZCI6Imh0dHBzOi8vYXBpLmFwcGVhci5pbi92MSIsImV4cCI6OTAwNzE5OTI1NDc0MDk5MSwiaWF0IjoxNTg4MDA2ODgxLCJvcmdhbml6YXRpb25JZCI6MjI5NTksImp0aSI6IjZmYjAzMGMzLWZiYTgtNGM5Ny04MTM4LTExMDQ2OWNlYWE0MyJ9.Zs784pC-EGZ5usX35DDsINHFZi039oflstPYfdKc_bQ"
}

/**
 *  CONFIG CEUAPP PRODUCTION (ceuapp-prod project)
 *  TO DEPLOY: COMMENT OTHER EXPORTS AND DESCOMING THIS.
 *  USE THE COMMAND BELOW TO DIRECT TO THE CORRECT PROJECT

 *  firebase deploy --only functions --project ceuapp-prod

 */

// module.exports = {
//     master_key: "ceuapp-prod-firebase-adminsdk-y8ueu-7582373b1e.json",
//     firebase_url: "https://ceuapp-prod.firebaseio.com",
//     onesignal_appid: 'a3c19564-340b-46e8-8a41-468ef87de9a0',
//     notification_api_id: 'MDFlZTk0MjAtMjRhOS00MWEwLWIxNDgtZjUzMWE1ZjNiOTQw',
//     notification_app_id: 'a3c19564-340b-46e8-8a41-468ef87de9a0',
//     mailgun_api_key: '9adf7819405335f249997315deb49e18-b3780ee5-c6600f6e',
//     mailgun_domain: 'mg.ceuapp.net',
//     mailgun_noreply_mail: 'noreply@ceuapp.net',
//     default_language: 'pt_BR',
//     wherebyApiKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmFwcGVhci5pbiIsImF1ZCI6Imh0dHBzOi8vYXBpLmFwcGVhci5pbi92MSIsImV4cCI6OTAwNzE5OTI1NDc0MDk5MSwiaWF0IjoxNTg4MDA2ODgxLCJvcmdhbml6YXRpb25JZCI6MjI5NTksImp0aSI6IjZmYjAzMGMzLWZiYTgtNGM5Ny04MTM4LTExMDQ2OWNlYWE0MyJ9.Zs784pC-EGZ5usX35DDsINHFZi039oflstPYfdKc_bQ"
// }

/**
 *  CONFIG inventapp-prod PRODUCTION (invent-app-prod project)
 *  TO DEPLOY: COMMENT OTHER EXPORTS AND DESCOMING THIS.
 *  USE THE COMMAND BELOW TO DIRECT TO THE CORRECT PROJECT

 *  firebase deploy --only functions --project invent-app-prod

 */

// module.exports = {
//     master_key: "inventapp-prod-firebase-adminsdk-m1px0-f049461e13.json",
//     firebase_url: "https://invent-app-prod.firebaseio.com",
//     onesignal_appid: 'fb407285-3d6d-467a-8be7-4ddf7a373873',
//     notification_api_id: 'YjI0NDMwNTEtZTUzZS00N2MwLTk4YjQtYWVmZDhiNzJhZTY5',
//     notification_app_id: 'fb407285-3d6d-467a-8be7-4ddf7a373873',
//     mailgun_api_key: '953f3b9e54b1df27dcf5cdb603fd16b9-1b65790d-a361deaf',
//     mailgun_domain: 'mg.invent-app.fr',
//     mailgun_noreply_mail: 'password@invent-app.fr',
//     default_language: 'fr_FR',
//     wherebyApiKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmFwcGVhci5pbiIsImF1ZCI6Imh0dHBzOi8vYXBpLmFwcGVhci5pbi92MSIsImV4cCI6OTAwNzE5OTI1NDc0MDk5MSwiaWF0IjoxNTg4MDA2ODgxLCJvcmdhbml6YXRpb25JZCI6MjI5NTksImp0aSI6IjZmYjAzMGMzLWZiYTgtNGM5Ny04MTM4LTExMDQ2OWNlYWE0MyJ9.Zs784pC-EGZ5usX35DDsINHFZi039oflstPYfdKc_bQ"
// }
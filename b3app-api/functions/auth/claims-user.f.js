const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        const idToken = req.body.idToken

        admin
            .auth()
            .verifyIdToken(idToken)
            .then((claims) => {
                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'success',
                    result: claims
                });
            });
    })
})
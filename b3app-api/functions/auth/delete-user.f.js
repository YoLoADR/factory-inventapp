const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    memory: '256MB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        let userId = req.body.userId;

        admin.firestore().collection('users').doc(userId).get()
        .then((data) => {
            let user = data;
            if(user.firstAccess == undefined || user.firstAccess == false) {
                admin
                .auth()
                .deleteUser(userId)
                .then(() => {
                    admin.firestore().collection("users").doc(userId).delete()
                    .then((data) => {
                        res.status(res.statusCode).json({
                            code: res.statusCode,
                            message: 'success',
                            result: true
                        });
                    })
                    .catch((error) => {
                        res.status(res.statusCode).json({
                            code: res.statusCode,
                            message: 'error',
                            result: false
                        });
                    })
                })
    
                .catch((erro) => {
                    res.status(res.statusCode).json({
                        code: res.statusCode,
                        message: 'success',
                        result: erro
                    })
                });
            } else {
                admin.firestore().collection("users").doc(userId).delete()
                .then((data) => {
                    res.status(res.statusCode).json({
                        code: res.statusCode,
                        message: 'success',
                        result: true
                    });
                })
                .catch((error) => {
                    res.status(res.statusCode).json({
                        code: res.statusCode,
                        message: 'error',
                        result: false
                    });
                })
            }
        })

        
    });
});
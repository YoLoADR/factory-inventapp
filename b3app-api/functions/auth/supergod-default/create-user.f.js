/*
    creates a standard supergod
*/
const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin
            .auth()
            .createUser({
                email: "admin@ceuapp.com",
                emailVerified: false,
                password: "CeuApp_Admin19$$",
                displayName: "Administrador",
                disabled: false

            }).then((user) => {
                const uid = user.uid;
                // add claims type 0
                admin.auth().setCustomUserClaims(uid, { type: 0 }).then(() => {
                    admin.firestore().collection("users").doc(user.uid).set({
                        name: 'Administrador',
                        email: user.email,
                        type: 0,
                        uid: user.uid,
                        photoUrl: '',
                        language: 'pt_BR'
                    }).then((res) => {
                        let obj = {
                            auth: user,
                            db: res
                        }
                        res.send(obj);
                    }).catch((err) => {
                        let obj2 = {
                            auth: user,
                            db: err
                        }
                        res.send(obj2)
                    })
                }).catch((error) => {
                    res.send(error);
                })

            }).catch((error) => {
                res.send(error);
            })
    });
});


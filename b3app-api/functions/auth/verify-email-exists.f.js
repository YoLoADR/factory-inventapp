const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// verify in Firebase Authentcation if user exists using e-mail to find
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin
            .auth()
            .getUserByEmail(req.body.email).then((user) => {
                res.json({
                    code: res.statusCode,
                    message: 'success',
                    result: user
                });
            }).catch((err) => {
                res.json({
                    code: res.statusCode,
                    message: 'error',
                    result: err
                });
            });
    });
});
/*
    create user
*/
const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin
            .auth()
            .createUser({
                email: req.body.email,
                password: req.body.password,
                displayName: req.body.firstName + ' ' + req.body.lastName,
                claims: {
                    type: req.body.type
                }
            }).then((user) => {
                const uid = user.uid;
                admin.auth().setCustomUserClaims(uid, { type: req.body.type }).then(() => {
                    admin.firestore().collection("users").doc(user.uid).set({
                        name:
                        {
                            first: req.body.name.first,
                            last: req.body.name.last
                        },
                        email: user.email,
                        language: req.body.language,
                        description: req.body.description,
                        type: req.body.type,
                        uid: user.uid,
                        urlPhoto: req.body.urlPhoto,
                        createdAt: user.metadata.creationTime,
                        emailRecovery: req.body.emailRecovery,
                        title: req.body.title,
                        company: req.body.company,
                        firstAccess: false
                    }).then((data) => {
                        res.status(201).json({
                            code: 201,
                            message: 'success',
                            result: user
                        });
                    }).catch((err) => {
                        //remove da autenticação caso dê erro no banco
                        admin.auth().deleteUser(uid);

                        res.status(500).json({
                            code: 500,
                            message: 'error',
                            result: err
                        });
                    });
                })
                    .catch((errorSetClaim) => {
                        //remove da autenticação caso de erro no banco
                        admin.auth().deleteUser(uid);

                        res.status(500).json({
                            code: 500,
                            message: 'error',
                            result: errorSetClaim
                        });
                    })
            }).catch((error) => {
                res.status(500).json({
                    code: 500,
                    message: 'error',
                    result: error
                });
            })
    });
});

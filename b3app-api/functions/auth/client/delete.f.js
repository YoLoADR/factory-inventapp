const configServer = require('../../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../../${configServer.master_key}`);
const deleteEvent = require("../../utilities/delete-event");

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let clientId = req.body.clientId;
        const db = admin.firestore();

        admin
            .auth()
            .deleteUser(clientId)
            .then(() => {
                db.collection("users").doc(clientId).delete()
                    .then(() => {
                        db.collection("events")
                            .where("client.uid", "==", clientId)
                            .get()
                            .then((events) => {
                                const size = events.size;
                                let cont = 0;

                                if (size <= 0) {
                                    res.status(res.statusCode).json({
                                        code: res.statusCode,
                                        message: 'success',
                                        result: true
                                    });
                                }

                                events.forEach(
                                    (doc) => {
                                        let eventId = doc.data().uid;
                                        deleteEvent(eventId, admin)
                                            .then(() => {
                                                cont++;

                                                if (cont >= size) {
                                                    res.status(res.statusCode).json({
                                                        code: res.statusCode,
                                                        message: 'success',
                                                        result: true
                                                    });
                                                }
                                            })
                                            .catch((error) => {
                                                res.status(res.statusCode).json({
                                                    code: res.statusCode,
                                                    message: 'error',
                                                    result: error
                                                });
                                            })
                                    }
                                )
                            })
                            .catch((error) => {
                                res.status(res.statusCode).json({
                                    code: res.statusCode,
                                    message: 'error',
                                    result: error
                                });
                            })
                    })
                    .catch((error) => {
                        res.status(res.statusCode).json({
                            code: res.statusCode,
                            message: 'error',
                            result: error
                        });
                    })
            })
            .catch((error) => {
                res.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'error',
                    result: error
                });
            })
    })

});

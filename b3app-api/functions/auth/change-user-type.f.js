const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

// change user type in Firebase Authentication, needs User UID and Type (number)
exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin
            .auth()
            .updateUser(req.body.uid, {
                claims: {
                    type: req.body.type
                }
            }).then((updated) => {
                send.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'success',
                    result: updated
                });
            }).catch((err) => {
                send.status(res.statusCode).json({
                    code: res.statusCode,
                    message: 'error',
                    result: err
                });
            })
    });
});
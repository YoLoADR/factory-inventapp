const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });
    admin.firestore().settings({ timestampsInSnapshots: true });
} catch (e) { console.log(e) }
const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        admin.auth().setCustomUserClaims(req.body.uid, { type: req.body.type }).then((_) => {
            res.json({
                code: 200,
                message: 'success',
                result: true
            });
        })
            .catch((__) => {
                res.json({
                    code: 400,
                    message: 'error',
                    result: false
                });
            });
    });
});
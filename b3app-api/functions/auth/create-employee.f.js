/*
    create user
*/
const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);
try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        console.log(req.body);
        admin
            .auth()
            .createUser({
                email: req.body.email,
                password: req.body.password,
                displayName: req.body.firstName + '' + req.body.lastName,
                claims: {
                    type: req.body.type
                }
            }).then((user) => {
                const uid = user.uid;
                admin.auth().setCustomUserClaims(uid, { type: req.body.type }).then(() => {
                    let data = {
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                        email: user.email,
                        language: req.body.language,
                        description: req.body.description,
                        type: req.body.type,
                        uid: user.uid,
                        urlPhoto: req.body.urlPhoto,
                        createdAt: user.metadata.creationTime,
                        emailRecovery: req.body.emailRecovery,
                        title: req.body.title,
                        company: req.body.company,
                        firstAccess: false,
                        clientId: req.body.clientId,
                        uid: uid
                    }
                    let userObj = userModelData(data);
                    admin.firestore().collection("users").doc(user.uid).set(userObj).then((data) => {
                        res.status(200).json({
                            code: res.statusCode,
                            message: 'success',
                            result: user
                        });
                    }).catch((err) => {
                        //remove da autenticação caso dê erro no banco
                        admin.auth().deleteUser(uid);
                        console.log(err);
                        res.status(res.statusCode).json({
                            code: res.statusCode,
                            message: 'error',
                            result: err
                        });
                    });
                })
                    .catch((errorSetClaim) => {
                        //remove da autenticação caso de erro no banco
                        admin.auth().deleteUser(uid);
                        console.log(errorSetClaim);
                        res.status(res.statusCode).json({
                            code: res.statusCode,
                            message: 'error',
                            result: errorSetClaim
                        });
                    })
            }).catch((error) => {
                if (error.code == 'auth/email-already-exists') {
                    admin.firestore().collection("users")
                        .where('email', '==', req.body.email)
                        .get()
                        .then((users) => {
                            users.forEach(doc => {
                                let user = doc.data();
                                admin.auth().updateUser(user.uid, ({
                                    displayName: req.body.firstName + '' + req.body.lastName,
                                    password: req.body.password,
                                    claims: {
                                        type: req.body.type
                                    }
                                }));
                                admin.auth().setCustomUserClaims(user.uid, { type: req.body.type }).then(() => {
                                    let data = {
                                        firstName: req.body.firstName,
                                        lastName: req.body.lastName,
                                        email: user.email,
                                        language: req.body.language,
                                        description: req.body.description,
                                        type: req.body.type,
                                        uid: user.uid,
                                        urlPhoto: req.body.urlPhoto,
                                        emailRecovery: req.body.emailRecovery,
                                        title: req.body.title,
                                        company: req.body.company,
                                        firstAccess: false,
                                        clientId: req.body.clientId
                                    }
                                    console.log(data);
                                    let userObj = userModelData(data);
                                    console.log(userObj);
                                    admin.firestore().collection("users").doc(user.uid).set(userObj).then((data) => {
                                        res.status(200).json({
                                            code: res.statusCode,
                                            message: 'success',
                                            result: user
                                        });
                                    }).catch((err) => {
                                        console.log(err);
                                        res.status(res.statusCode).json({
                                            code: res.statusCode,
                                            message: 'error-db',
                                            result: err
                                        });
                                    });
                                })
                            });
                        })

                } else {
                    console.log(err);
                    res.status(res.statusCode).json({
                        code: res.statusCode,
                        message: 'error',
                        result: error.error
                    });
                }
            })
    });
});

function userModelData(user) {
    let userFormat = {
        name: {
            first: "",
            last: ""
        },
        type: null,
        email: "",
        language: "",
        description: "",
        photoUrl: "",
        company: "",
        title: "",
        recoveryEmail: "",
        events: null,
        firstAccess: "",
        clientId: null
    };

    if (user.firstName != undefined) {
        userFormat.name.first = user.firstName;
    }

    if (user.lastName != undefined) {
        userFormat.name.last = user.lastName;
    }

    if (user.type != undefined) {
        userFormat.type = user.type;
    }

    if (user.email != undefined) {
        userFormat.email = user.email;
    }

    if (user.language != undefined) {
        userFormat.language = user.language;
    }

    if (user.description != undefined) {
        userFormat.description = user.description;
    }

    if (user.photoUrl != undefined) {
        userFormat.photoUrl = user.photoUrl;
    }

    if (user.company != undefined) {
        userFormat.company = user.company;
    }

    if (user.title != undefined) {
        userFormat.title = user.title;
    }

    if (user.recoveryEmail != undefined) {
        userFormat.recoveryEmail = user.recoveryEmail;
    }

    if (user.events != undefined) {
        userFormat.events = user.events;
    }

    if (user.firstAccess != undefined) {
        userFormat.firstAccess = user.firstAccess;
    }

    if (user.clientId != undefined) {
        userFormat.clientId = user.clientId
    }

    return userFormat;
}
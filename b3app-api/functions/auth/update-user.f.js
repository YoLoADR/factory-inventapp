const configServer = require('../config/config.server');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require(`../${configServer.master_key}`);

try {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: configServer.firebase_url,
        storageBucket: "b3app-develop.appspot.com"
    });

    admin.firestore().settings({ timestampsInSnapshots: true });

} catch (e) { console.log(e) }

const cors = require("cors")({ origin: true });

exports = module.exports = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let userId = req.body.uid;
        let user = req.body.user;
        let userData;

        console.log(user.password);
        //dados a serem alterados na autenticação caso tenha alterado a senha
        if (user.password != undefined && user.password != null && user.password != "") {
            userData = {
                email: user.email,
                password: user.password,
                displayName: user.name
            }
        } else { //dados a serem alterados na autenticação caso não tenha alterado a senha
            userData = {
                email: user.email,
                displayName: user.name
            }
        }

        console.log(userId, userData)
        admin.auth()
            .updateUser(userId, userData)
            .then(() => {
                res.status(200).json({
                    code: 200,
                    message: 'success',
                    result: true
                });
            }).catch((error) => {
                console.log("ERRO AO ALTERAR USER NA AUTENTICAÇÃO")
                res.status(500).json({
                    code: 500,
                    message: 'error',
                    result: false
                });
            })
    });
});
const Session = function() {
    let obj = {
        uid: "",
        identifier: 0,
        name: {
            DeDE: '',
            EnUS: '',
            EsES: '',
            FrFR: '',
            PtBR: ''
        },
        date: "",
        startTime: "",
        endTime: "",
        moduleId: "",
        eventId: "",
        descriptions: {
            DeDE: '',
            EnUS: '',
            EsES: '',
            FrFR: '',
            PtBR: ''
        },
        locations: {},
        tracks: {},
        groups: {},
        attendees: {},
        speakers: {},
        documents: {},
        limitAttendees: 0,
        askQuestion: false,
        askModerate: false,
    }

    return obj
}

module.exports = Session
const deleteCollection = require('./delete-collection');

const deleteAttendees = (refEvent, refModule, eventId, admin) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        Promise.all([
            deleteEvent(refEvent, eventId, admin),
            deleteModule(refModule),
            deleteUser(eventId, admin)
        ])
            .then((_) => {
                resolve(true);
            })
            .catch((err) => {
                console.log('= ERR 1 => ', err);
                reject(false);
            });
    });
}

const deleteEvent = (refEvent, eventId, admin) => {
    return new Promise((resolve, reject) => {
        refEvent
            .get()
            .then((values) => {
                let size = values.size;
                let cont = 0;
                if (size >= 1) {
                    values.forEach(el => {
                        let attendee = el.data();
                        let refCustom = refEvent.doc(attendee.uid).collection('customFields');

                        Promise.all([
                            deleteCollection(refCustom.path),
                            //removePersonalSchedule(eventId, attendee.uid, admin)
                        ])
                            .then((_) => {
                                refEvent.doc(attendee.uid).delete();
                            })
                            .catch((err) => {
                                console.log('= ERR 2 => ', err);
                                reject(err);
                            });
                        if (cont == size - 1) {
                            resolve(true);
                        }
                        cont++;
                    });
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                console.log('= ERR 3 => ', err);
                reject(err);
            });
    })
}
const deleteModule = (refModule) => {
    return new Promise((resolve, reject) => {
        let refModuleAttendee = refModule.collection('attendees');

        refModuleAttendee
            .get()
            .then((values) => {
                let size = values.size;
                let cont = 0;
                if (size >= 1) {
                    values.forEach(el => {
                        let attendee = el.data();
                        let refCustom = refModuleAttendee.doc(attendee.uid).collection('customFields');

                        Promise.all([
                            deleteCollection(refCustom.path)
                        ])
                            .then((_) => {
                                refModuleAttendee.doc(attendee.uid).delete();
                                if (cont == size - 1) {
                                    resolve(true);
                                }
                                cont++;
                            })
                            .catch((err) => {
                                console.log('= ERR 4 => ', err);
                                reject(err);
                            });
                    });
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                console.log('= ERR 5 => ', err);
                reject(err);
            });
    })
}
const deleteUser = (eventId, admin) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        let refUsers = db.collection('users');

        refUsers
            .get()
            .then((values) => {
                let size = values.size;
                let cont = 0;

                if (size >= 1) {
                    values.forEach(el => {
                        let user = el.data();
                        if (user.type == 4 || user.type == 5) {
                            let FieldValue = require('firebase-admin').firestore.FieldValue;
                            console.log('*** USER EVENTS **** ', user.events);
                            if (user.events !== undefined) {
                                let totalEvents = user.events.length;

                                let contEvent = 0;

                                for (let uid of user.events) {
                                    
                                    if (uid == eventId && totalEvents > 1) {
                                        refUsers.doc(user.uid).update({
                                            events: FieldValue.arrayRemove(eventId)
                                        });
                                        if (contEvent == totalEvents - 1) {
                                            if (cont == size - 1) {
                                                resolve(true);
                                            }
                                            cont++;
                                        }
                                        contEvent++;
                                        break;
                                    } else if (uid == eventId) {
                                        if (user.firstAccess == false) {
                                            admin.auth().deleteUser(user.uid);
                                        }
                                        refUsers.doc(user.uid).delete();
                                        if (contEvent == totalEvents - 1) {
                                            if (cont == size - 1) {
                                                resolve(true);
                                            }
                                            cont++;
                                        }
                                        contEvent++;
                                    } else {
                                        if (contEvent == totalEvents - 1) {
                                            if (cont == size - 1) {
                                                resolve(true);
                                            }
                                            cont++;
                                        }
                                        contEvent++;
                                    }

                                }
                            }
                        } else {
                            if (cont == size - 1) {
                                resolve(true);
                            }
                            cont++;
                        }
                    });
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                console.log('= ERR 6 => ', err);
                reject(err);
            });
    })
}

/*const removePersonalSchedule = (eventId, attendeeId, admin) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();
        Promise.all([
            listAttendeeSessions(attendeeId, eventId, db),
            eventCalendarModule(eventId, db)
        ])
            .then((_) => {
                resolve(true);
            })
            .catch((err) => {
                reject(err);
            });
    })
}


// returns actions related to the participant
const listAttendeeSessions = (attendeeId, eventId, db) => {
    return new Promise((resolve) => {
        const ref = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('sessions')

        ref
            .get()
            .then((snapshot) => {
                const list = []

                snapshot.forEach((childSnapshot) => {
                    list.push(childSnapshot.data())
                })

                resolve(list)
            })
            .catch((err) => {
                console.log(' ERROR LIST SESSIONS => ', err);
                reject(err);
            });
    });
}

// receives the event uid and returns the event's agenda module
const eventCalendarModule = (eventId, db) => {
    return new Promise((resolve) => {
        const ref = db.collection('events').doc(eventId).collection('modules');

        ref.where('type', '==', 13).get().then((snapshot) => {
            snapshot.forEach((childSnapshot) => {
                const module = childSnapshot.data()
                resolve(module)
            })
        })
            .catch((err) => {
                console.log(' ERROR EVENT CALENDAR => ', err);
                reject(err);
            });
    })
}*/

module.exports = deleteAttendees
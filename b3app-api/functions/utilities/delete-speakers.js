const deleteCollection = require('./delete-collection');

const deleteSpeakers = (refEvent, refModule, eventId, admin) => {
    return new Promise((resolve, reject) => {
        let db = admin.firestore();

        Promise.all([
            deleteEvent(refEvent),
            deleteModule(refModule),
            deleteUser(eventId, db)
        ])
            .then((_) => {
                resolve(true);
            })
            .catch((err) => {
                reject(false);
            });
    });
}

const deleteEvent = (refEvent) => {
    return new Promise((resolve, reject) => {
        refEvent
            .get()
            .then((values) => {
                let size = values.size;
                let cont = 0;
                if (size >= 1) {
                    values.forEach(el => {
                        let speaker = el.data();
                        let refCustom = refEvent.doc(speaker.uid).collection('customFields');

                        deleteCollection(refCustom.path);
                        refEvent.doc(speaker.uid).delete();

                        if (cont == size - 1) {
                            resolve(true);
                        }
                        cont++;
                    });
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                reject(err);
            });
    })
}
const deleteModule = (refModule) => {
    return new Promise((resolve, reject) => {
        let refModuleSpeaker = refModule.collection('speakers');

        refModuleSpeaker
            .get()
            .then((values) => {
                let size = values.size;
                let cont = 0;
                if (size >= 1) {
                    values.forEach(el => {
                        let speaker = el.data();
                        let refCustom = refModuleSpeaker.doc(speaker.uid).collection('customFields');

                        deleteCollection(refCustom.path);
                        refModuleSpeaker.doc(speaker.uid).delete();

                        if (cont == size - 1) {
                            resolve(true);
                        }
                        cont++;
                    });
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                reject(err);
            });
    })
}
const deleteUser = (eventId, db) => {
    return new Promise((resolve, reject) => {
        let refUsers = db.collection('users');

        refUsers
            .get()
            .then((values) => {
                let size = values.size;
                let cont = 0;

                if (size >= 1) {
                    values.forEach(el => {
                        let user = el.data();
                        let FieldValue = require('firebase-admin').firestore.FieldValue;
                        let totalEvents = user.events.length;
                        let contEvent = 0;

                        for (let uid of user.events) {
                            if (uid == eventId && totalEvents > 1) {
                                refUsers.doc(user.uid).update({
                                    events: FieldValue.arrayRemove(eventId)
                                });
                                if (contEvent == totalEvents - 1) {
                                    if (cont == size - 1) {
                                        resolve(true);
                                    }
                                    cont++;
                                }
                                contEvent++;
                                break;
                            } else if (uid == eventId) {
                                refUsers.doc(user.uid).delete();
                                if (contEvent == totalEvents - 1) {
                                    if (cont == size - 1) {
                                        resolve(true);
                                    }
                                    cont++;
                                }
                                contEvent++;
                            } else {
                                if (contEvent == totalEvents - 1) {
                                    if (cont == size - 1) {
                                        resolve(true);
                                    }
                                    cont++;
                                }
                                contEvent++;
                            }
                        }

                    });
                } else {
                    resolve(true);
                }
            })
            .catch((err) => {
                reject(err);
            });
    })
}

module.exports = deleteSpeakers
// const functions = require('firebase-functions');
const deleteCollection = require('./delete-collection');
const deleteAttendees = require('./delete-attendees');
const deleteSpeakers = require('./delete-speakers');

const deleteEvent = (eventId, admin) => {
    return new Promise((resolve, reject) => {
        const db = admin.firestore();

        // references
        const refCollectionsModules = db.collection('events').doc(eventId).collection('modules');
        const refCollectionsSessions = db.collection('events').doc(eventId).collection('sessions');
        const refCollectionsAttendees = db.collection('events').doc(eventId).collection('attendees');
        const refCollectionsSpeakers = db.collection('events').doc(eventId).collection('speakers');
        const refCollectionsGroups = db.collection('events').doc(eventId).collection('groups');
        const refCollectionsLocations = db.collection('events').doc(eventId).collection('locations');
        const refCollectionChat = db.collection('events').doc(eventId).collection('chat');
        const refCollectionNotifications = db.collection('events').doc(eventId).collection('notifications');
        const refCollectionFeedPosts = db.collection('events').doc(eventId).collection('feed-posts');
        const refAnalytics = db.collection('analytics').doc(eventId);
        const refModules = db.collection('modules');
        const refEvent = db.collection('events').doc(eventId);

        // remove modules event
        refModules
            .where('eventId', '==', eventId)
            .get()
            .then(modules => {
                modules.forEach(element => {
                    const module = element.data();
                    const moduleId = module.uid;
                    const ref = refModules.doc(moduleId);

                    if (module.type == 0) {
                        removeSchedule(refCollectionsSessions, ref, eventId, admin);
                    }

                    // if (module.type == 1) {
                    //     removeSpeakers(refCollectionsSpeakers, ref, eventId, admin);
                    // }

                    // if (module.type == 2) {
                    //     removeAttendees(refCollectionsAttendees, ref, eventId, admin);
                    // }

                    if (module.type == 3) {
                        const refDocuments = ref.collection('documents');
                        deleteCollection(refDocuments.path);
                    }

                    if (module.type == 5) {
                        const refGallery = ref.collection('images');
                        deleteCollection(refGallery.path);
                    }

                    if (module.type == 23) {
                        const groupRef = ref.collection('groups');
                        deleteCollection(groupRef.path);
                    }

                    if (module.type == 28) {
                        const widgetRef = ref.collection('widgets');
                        deleteCollection(widgetRef.path);
                    }

                    if (module.type == 29) {
                        const locationRef = ref.collection('locations');
                        deleteCollection(locationRef.path);
                    }

                    ref.delete();
                });
            })

        Promise.all([
            deleteCollection(refCollectionsModules.path),
            deleteCollection(refCollectionsSessions.path),
            deleteCollection(refCollectionsAttendees.path),
            deleteCollection(refCollectionsSpeakers.path),
            deleteCollection(refCollectionsGroups.path),
            deleteCollection(refCollectionsLocations.path),
            deleteCollection(refCollectionChat),
            deleteCollection(refCollectionNotifications),
            deleteCollection(refCollectionFeedPosts)
        ])

        // remove analytics
        refAnalytics.delete();
        // remove doc event
        refEvent.delete()
            .then((_) => { resolve(true) })
            .catch((err) => { reject(err) });

    })


}

const removeSchedule = (eventPathSchedule, moduleRef, eventId, admin) => {
    return new Promise((resolve, reject) => {
    });
}

const removeSpeakers = (eventPathSpeakers, moduleRef, eventId, admin) => {
    return new Promise((resolve, reject) => {
        deleteSpeakers(eventPathSpeakers, moduleRef, eventId, admin)
            .then((_) => {
                resolve(true);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

const removeAttendees = (eventPathAttendees, moduleRef, eventId, admin) => {
    return new Promise((resolve, reject) => {
        deleteAttendees(eventPathAttendees, moduleRef, eventId, admin)
            .then((_) => {
                resolve(true);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

module.exports = deleteEvent

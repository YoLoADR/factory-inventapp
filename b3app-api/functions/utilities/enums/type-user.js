let TypeUser = {
    SUPERGOD: 0,
    GOD: 1,
    CLIENT: 2,
    EMPLOYEE: 3,
    SPEAKER: 4,
    ATTENDEE: 5
}

// export user types
module.exports = TypeUser
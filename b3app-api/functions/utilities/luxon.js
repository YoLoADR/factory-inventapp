const DateTime = require('luxon').DateTime
const Settings = require('luxon').Settings


// define zone default
const defaultZoneName = () => {
    Settings.defaultZoneName = "Europe/Paris"
    return DateTime.local().zoneName
}

// create timestamp
const createTimeStamp = (date) => {
    return date.valueOf() / 1000;
}

/* create date with timezone timezone return object DateTime*/
const createDate = (year, month, day, hour, minute, seconds) => {
    const aux = year + '-' + month + '-' + day + 'T' + hour + ':' + minute + ':' + seconds;
    return DateTime.fromISO(aux);
}

// creation of the current date
const currentDate = () => {
    return DateTime.local()
}

// converts a DateTime to a string of the format dd / mm / yyyy
const convertDateToStringIsNotUSA = (date) => {
    return date.toLocaleString({ day: 'numeric', month: 'numeric', year: 'numeric' })
}

// // convert timestamp to DateTime
const convertTimestampToDate = (timestamp) => {
    return DateTime.fromMillis(timestamp * 1000);
}

//torna a hora e o minuto da data
dateTime = (date) => {
    return date.toLocaleString({ hour: 'numeric', minute: 'numeric' })
}

module.exports = {
    createTimeStamp,
    createDate,
    currentDate,
    convertDateToStringIsNotUSA,
    convertTimestampToDate,
    defaultZoneName,
    dateTime
}
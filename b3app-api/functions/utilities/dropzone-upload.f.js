const functions = require('firebase-functions');

const cors = require("cors")({
    origin: true
});

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        res.status(200).send("OK");
    });
});
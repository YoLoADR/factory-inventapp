const firebase_tools = require('firebase-tools');

const deleteCollection = (path) => {
    return firebase_tools.firestore
        .delete(path, {
            project: process.env.GCLOUD_PROJECT,
            recursive: true,
            yes: true,
        })
        .then(() => {
            return {
                path: path
            };
        })
        .catch((e) => {
            console.error('ERROR DELETE COLLECTION: ', e);
        })
}

module.exports = deleteCollection
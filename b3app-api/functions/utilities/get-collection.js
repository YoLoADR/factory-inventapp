const getCollection = (path) => {
    return new Promise((resolve) => {
        const list = []

        path
            .get()
            .then((snapshot) => {
                snapshot.forEach((childSnapshot) => {
                    const element = childSnapshot.data()
                    list.push(element)
                })
                resolve(list)
            })
    })
}

module.exports = getCollection
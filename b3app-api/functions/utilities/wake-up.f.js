const functions = require('firebase-functions');
const axios = require('axios');
const cors = require("cors")({ origin: true });

exports = module.exports = functions.runWith({
    timeoutSeconds: 540,
    memory: '2GB'
}).https.onRequest((req, res) => {
    cors(req, res, () => {
        axios.all([
            axios.post('https://us-central1-b3app-master.cloudfunctions.net/dbCheckinCheckinStatus', {
                eventId: '1',
                moduleId: '1',
                checkinId: '1'
            }),
            axios.post('https://us-central1-b3app-master.cloudfunctions.net/dbAttendeesExportCheckin', {
                eventId: '1',
                moduleId: '1',
                checkinId: '1'
            }),
            axios.get('https://us-central1-b3app-master.cloudfunctions.net/dbCustomFieldAttendeeCreate', {
                eventId: '1',
                moduleId: '1',
                checkinId: '1',
                customField: {},
                listOptions: []
            }),
            axios.get('https://us-central1-b3app-master.cloudfunctions.net/dbCustomFieldAttendeeDelete', {
                eventId: '1',
                moduleId: '1',
                customId: '1'
            }),
            axios.get('https://us-central1-b3app-master.cloudfunctions.net/dbCustomFieldAttendeeEdit', {
                eventId: '1',
                moduleId: '1',
                customField: {},
                oldCustomField: {},
                listOptionsCustomEdit: [],
                listOptionsCustomRemove: []
            }),
            axios.get('https://us-central1-b3app-develop.cloudfunctions.net/dbPersonalScheduleGetAllSessions', {
                eventId: '1',
            }),
            axios.post('https://us-central1-b3app-master.cloudfunctions.net/dbScheduleDeleteSessions', {
                sessions: []
            }),
        ]).then(axios.spread((r, re) => {
            res.send('wakeuped!!');
        })).catch(error => {
            res.send('error to wakeup!!');
        });
    });
});
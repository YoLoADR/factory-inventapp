import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
// import { storage } from 'firebase/app';
import { UUID } from 'angular2-uuid';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import { take, finalize, map, switchMap, first } from 'rxjs/operators';
import { Observable, concat } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class StorageService {
    // storage: storage.Reference = storage().ref();

    constructor(private camera: Camera, private afStorage: AngularFireStorage) {
        // this.storage = storage().ref();
    }

    // Take a picture with cellphone camera
    takePicture(onResolve) {
        const cameraOptions: CameraOptions = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetHeight: 1000,
            targetWidth: 1000,
            sourceType: this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true,
            correctOrientation: true,
            allowEdit: true
        };

        this.camera.getPicture(cameraOptions).then(
            (imageData) => {
                const captureDataUrl = 'data:image/jpeg;base64,' + imageData;

                onResolve(captureDataUrl);
            },
            (error) => { }
        );
    }

    async getPictureFromCamera(): Promise<string> {
        const cameraOptions: CameraOptions = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetHeight: 1000,
            targetWidth: 1000,
            sourceType: this.camera.PictureSourceType.CAMERA,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true,
            correctOrientation: true,
            allowEdit: true
        };

        const data = await this.camera.getPicture(cameraOptions);

        return `data:image/jpeg;base64,${data}`;
    }

    // Choose the picture on Gallery
    openGallery(onResolve) {
        const cameraOptions: CameraOptions = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetHeight: 1000,
            targetWidth: 1000,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: true
        };
        this.camera.getPicture(cameraOptions).then(
            (imageData) => {
                const captureDataUrl = 'data:image/jpeg;base64,' + imageData;

                onResolve(captureDataUrl);
            },
            (error) => { }
        );
    }

    async getPictureFromGallery(): Promise<string> {
        const cameraOptions: CameraOptions = {
            destinationType: this.camera.DestinationType.DATA_URL,
            targetHeight: 1000,
            targetWidth: 1000,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: true
        };

        const data = await this.camera.getPicture(cameraOptions);

        return `data:image/jpeg;base64,${data}`;
    }

    uploadAttendeeProfile(eventId, userId, file, onResolve) {
        let metadata = {
            contentType: 'image/jpeg'
        };
        let ref = this.afStorage
            .ref('/profile-pictures-attendees')
            .child(eventId)
            .child(userId);
        ref.putString(file, 'data_url', metadata)
            .then((snapshot) => {
                ref.getDownloadURL()
                    .pipe(take(1))
                    .subscribe(
                        (url: string) => {
                            onResolve(url);
                        },
                        (error) => {
                            onResolve(null);
                        }
                    );
            })
            .catch((error) => {
                onResolve(null);
            });
        // const ref = this.storage.child('/profile-pictures-attendees').child(eventId).child(userId);
        // ref.putString(file, 'data_url', metadata).then((snapshot) => {
        //     ref.getDownloadURL().then((url: string) => {
        //         onResolve(url);
        //     }).catch((error) => {
        //         onResolve(null);
        //     })
        // }).catch((error) => {
        //     onResolve(null);
        // });
    }

    uploadSpeakerProfile(eventId, userId, file, onResolve) {
        let metadata = {
            contentType: 'image/jpeg'
        };

        let ref = this.afStorage
            .ref('/profile-pictures-speakers')
            .child(eventId)
            .child(userId);
        ref.putString(file, 'data_url', metadata)
            .then((snapshot) => {
                ref.getDownloadURL()
                    .pipe(take(1))
                    .subscribe(
                        (url: string) => {
                            onResolve(url);
                        },
                        (error) => {
                            onResolve(null);
                        }
                    );
            })
            .catch((error) => {
                onResolve(null);
            });

        // const ref = this.storage.child('/profile-pictures-speakers').child(eventId).child(userId);
        // ref.putString(file, 'data_url', metadata).then((snapshot) => {
        //     ref.getDownloadURL().then((url: string) => {
        //         onResolve(url);
        //     }).catch((error) => {
        //         onResolve(null);
        //     })
        // }).catch((error) => {
        //     onResolve(null);
        // });
    }

    uploadChatImage(eventId: string, file: any, onResolve) {
        console.log("Event id: ", eventId);
        let metadata = {
            contentType: 'image/jpeg'
        };
        let uid = UUID.UUID();

        let ref = this.afStorage.ref('/chat').child(eventId).child(uid);
        ref.putString(file, 'data_url', metadata)
            .then((snapshot) => {
                ref.getDownloadURL()
                    .pipe(take(1))
                    .subscribe(
                        (url: string) => {
                            onResolve(url);
                        },
                        (error) => {
                            onResolve(null);
                        }
                    );
            }).catch((error) => {
                console.log("Error: ", error);
                onResolve(null);
            });
    }

    uploadImgNewsFeed(file: any, postId, eventId, moduleId) {
        return new Promise((resolve, reject) => {
            let metadata = {
                contentType: 'image/jpeg'
            };

            let ref = this.afStorage
                .ref('/news_feed')
                .child(eventId)
                .child(moduleId)
                .child(postId);
            ref.putString(file, 'data_url', metadata)
                .then((snapshot) => {
                    ref.getDownloadURL()
                        .pipe(take(1))
                        .subscribe(
                            (url: string) => {
                                resolve(url);
                            },
                            (error) => {
                                resolve(null);
                            }
                        );
                }).catch((error) => {
                    reject(null);
                });
        })
    }

    deletePost(postId: string, eventId: string, moduleId: string) {
        let deleteRef = this.afStorage
            .ref('/news_feed')
            .child(eventId)
            .child(moduleId)
            .child(postId);
        // const deleteRef = this.storage.child('/news_feed').child(eventId).child(moduleId).child(postId)
        deleteRef.delete();
    }

    /**
     * Upload doc for survey type document
     * @param file 
     * @param eventId 
     * @param moduleId 
     * @param surveyId 
     * @param questionId 
     */
    uploadSurveyDocImg(file: any, eventId: string, moduleId: string, surveyId: string, questionId: string, name: string, mobile: boolean) {
        return (new Promise((resolve, reject) => {

            if (mobile) {
                let metadata = {
                    contentType: 'image/jpeg'
                };
                let ref = this.afStorage.ref('/documents-surveys').child(eventId).child(moduleId).child(surveyId).child(questionId).child(name);
                ref.putString(file, "data_url", metadata).then((snapshot) => {
                    ref.getDownloadURL().pipe(take(1)).subscribe((url: string) => {
                        console.log("Url get: ", url);
                        resolve(url);
                    }, (error) => {
                        resolve(null);
                    })
                }).catch((error) => {
                    reject(null);
                });
            } else {
                let ref = this.afStorage.ref('/documents-surveys').child(eventId).child(moduleId).child(surveyId).child(questionId).child(name);
                ref.put(file).then((snapshot) => {
                    ref.getDownloadURL().pipe(take(1)).subscribe((url: string) => {
                        console.log("Url get: ", url);
                        resolve(url);
                    }, (error) => {
                        resolve(null);
                    })
                }).catch((error) => {
                    reject(null);
                });
            }
        }))
    }
}

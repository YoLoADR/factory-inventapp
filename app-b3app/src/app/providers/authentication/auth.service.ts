import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { auth } from 'firebase/app';
import { PathApi } from '../../paths/path-api';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { NavController, MenuController } from '@ionic/angular';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public userToken: string = null;
    public headers;
    public requestOptions;
    public tokenId: string = null;
    constructor(
        public http: HttpClient,
        private angularFireAuth: AngularFireAuth,
        private aFirestore: AngularFirestore,
        private splashScreen: SplashScreen,
        private navCtrl: NavController,
        private zone: NgZone,
        private menuCtrl: MenuController,
        private functions: AngularFireFunctions
    ) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
    }

    // Verify if e-mail exists in Firestore Database
    verifyEmailDb(email: string, onResolve) {
        let call = this.functions.httpsCallable('dbUserGetUserByEmail')
        let subs = call({ email: email })
            .subscribe((_) => {
                subs.unsubscribe();
                onResolve(_);
            });
    }

    // Verify if e-mail exists in Firebase Authentication
    verifyEmailAuth(email: string) {
        return new Promise((resolve, reject) => {
            // call http to verify if e-mail already exists
            this.http.post(PathApi.baseUrl + PathApi.authChangeUserType, email, this.requestOptions)
                .subscribe(_ => {
                    // case exists, return 'exists' to front manipulation
                    resolve('exists');
                }, e => {
                    // case not exists, return 'available' to front manipulation
                    reject('available');
                });
        })
    }

    /**
     * Make account in user first access and call Firebase Auth to set user type (access nivel)
     * @param email 
     * @param password 
     * @param userUid 
     * @param type 
     */
    createAccount(email: string, password: string, userUid: string, type) {
        let call = this.functions.httpsCallable('dbUserCreateUserFirstAccess');
        return (call({ email: email, password: password, userId: userUid, userType: type }).pipe(take(1)));
    }

    createUserPublicEvent(eventId: string, name: string, email: string, password: string, language: string, onResolve) {
        let call = this.functions.httpsCallable('dbUserCreatePublicEventUserFirstAccess');
        let subs = call({ eventId: eventId, name: name, email: email, password: password, language: language })
            .subscribe((_) => {
                subs.unsubscribe();
                if (_.status) {
                    this.login(email, password, (__) => {
                        onResolve(_);
                    });
                }
            });
    }

    makeUsers(attendee, user, eventId) {
        return new Promise((resolve, reject) => {
            let db = this.aFirestore.firestore;
            let batch = db.batch();
            let refModule = db.collection('modules').doc(attendee.moduleId).collection('attendees').doc(user.uid);
            let refEvent = db.collection('events').doc(eventId).collection('attendees').doc(user.uid);
            let userRef = db.collection('users').doc(user.uid);

            batch.set(refModule, attendee);
            batch.set(refEvent, attendee);
            batch.set(userRef, user);

            batch
                .commit()
                .then((result) => {
                    resolve(true);
                })
                .catch((e) => {
                    resolve(false);
                });
        });
    }

    checkAttendeeInAttendees(eventId: string, uid: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('attendees').doc(uid);

        ref
            .get()
            .then((snapshot) => {
                if (snapshot.exists) {
                    onResolve(true);
                } else {
                    onResolve(false);
                }
            });
    }

    login(email: string, password: string, onResolve) {
        auth().signInWithEmailAndPassword(email, password)
            .then(async (res) => {
                localStorage.setItem('userIdentifier', res.user.uid);
                await res.user.getIdToken(true).then((idToken: string) => {
                    localStorage.setItem("userToken", idToken);
                    this.tokenId = idToken;
                    onResolve(res);
                }).catch((err) => {
                    onResolve(err);
                });
            })
            .catch((err) => {
                onResolve(err);
            });
    }

    logout() {
        this.splashScreen.show();
        this.menuCtrl.toggle();
        this.removeStorageReferences();
        auth().signOut().then((_) => {
            this.navCtrl.navigateRoot(['/'])
                .then(() => {
                    window.location.reload();
                    this.splashScreen.hide();
                });
        });
    }

    removeStorageReferences() {
        localStorage.removeItem("userToken");
        localStorage.removeItem("eventId");
        localStorage.removeItem("userIdentifier");
        // localStorage.removeItem("usedLanguage");
        localStorage.removeItem("homePage");
        localStorage.removeItem("timezone");
        localStorage.removeItem("eventVisible");
    }

    getIdToken() {
        return new Promise((resolve, reject) => {
            auth().currentUser.getIdToken(true)
                .then((idToken) => {
                    resolve(idToken);
                })
                .catch((error) => {
                    reject(error)
                });
        });
    }

    claimsUser(onResolve) {
        this.getIdToken()
            .then((idToken: string) => {
                // request post
                return this.http.post(PathApi.baseUrl +
                    PathApi.authClaimsUsers,
                    { idToken: idToken },
                    this.requestOptions
                ).subscribe(
                    (claims: any) => {
                        // let aux = JSON.parse(claims['_body']);
                        onResolve(claims);
                    }
                )
            });
    }


    sendCodeNumberToEmail(user, onResolve) {
        this.http.post(PathApi.baseUrl + PathApi.dbUserSendCodeNumberEmail, user, this.requestOptions).subscribe((success) => {
            onResolve(success);
        }), err => {
            onResolve(false);
        }
    }

    verifyCodeNumber(code: string, userId: string, onResolve) {

        let call = this.functions.httpsCallable('dbUserVerifyCodeNumber');
        let subs = call({ code: code, uid: userId })
            .subscribe((_) => {
                subs.unsubscribe();
                onResolve(_);
            })
        // let db = this.aFirestore.firestore;
        // db
        //   .collection("users")
        //   .doc(userId)
        //   .get()
        //   .then((user) => {
        //     let aux = user.data();
        //     if (aux.codeNumber == code) {
        //       onResolve({
        //         code: 200,
        //         message: 'success',
        //         result: true
        //       });
        //     } else {
        //       onResolve({
        //         code: 404,
        //         message: 'error',
        //         result: false
        //       });
        //     }
        //   })
        //   .catch((error) => {
        //     onResolve({
        //       code: 404,
        //       message: 'error',
        //       result: error
        //     });
        //   });
    }

    removeCodeNumber(userId: string) {
        let db = this.aFirestore.firestore;

        db.collection("users").doc(userId).update({
            codeNumber: null
        });
    }

    public authenticated(): boolean {
        let storage = localStorage.getItem('userToken');
        if (this.tokenId === undefined || this.tokenId === null && storage !== null && storage !== undefined) {
            this.tokenId = storage;
        }
        return this.tokenId !== undefined && this.tokenId !== null;
    }

    isAuthenticated(): Observable<boolean> {
        return this.angularFireAuth.authState
            .pipe(map(u => (u) ? true : false))
    }

    getUser() {
        return this.angularFireAuth.authState.pipe(
            switchMap(u => (u) ? this.aFirestore.collection('users').doc(u.uid).valueChanges() : of(null))
        )
    }

    public current() {
        return new Promise((resolve, reject) => {
            this.angularFireAuth.authState.subscribe((auth) => {
                resolve(auth);
            });
        })
    }

    checkAuthEmail(email: string, onResolve) {
        this.angularFireAuth.auth.fetchSignInMethodsForEmail(email).then((result) => onResolve(true))
            .catch((e) => onResolve(false));
    }

    recoveryPassword(email: string, onResolve) {
        this.angularFireAuth.auth.sendPasswordResetEmail(email)
            .then((ok) => {
                onResolve(true);
            })
            .catch((e) => {
                onResolve(false);
            });
    }
}

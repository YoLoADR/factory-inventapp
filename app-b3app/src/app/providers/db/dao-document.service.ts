import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DaoDocumentsService {
    private refGetDocuments = null


    constructor(
        private aFirestore: AngularFirestore
    ) {
        AngularFirestoreModule.enablePersistence();
    }

    /**
    * get module. 
    * @param moduleId 
    */
    getModule(moduleId: string): Observable<any> {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .valueChanges())
    }

    /**
    * Get folder. 
    * @param moduleId 
    * @param folderId 
    */
    getFolder(moduleId: string, folderId: string): Observable<any> {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('folders')
            .doc(folderId)
            .valueChanges())
    }

    /**
     * Getting all folders of document module
     * @param moduleId 
     * @param order 
     */
    getFolders(moduleId: string, order: any) {
        return (this.aFirestore.collection('modules').doc(moduleId).collection('folders', (ref) => {
            let newRef: any = ref;
            if (order == 'custom') {
                newRef = newRef.orderBy(order);
            } else {
                newRef = newRef.orderBy('createdAt', order);
            }
            return (newRef);
        }).snapshotChanges().pipe(
            map((snapshot) => {
                return (snapshot.map((doc) => {
                    return (doc.payload.doc.data() as any);
                }))
            })
        ))
    }

    /**
    * get documents. 
    * @param moduleId 
    * @param folderId 
    * @param orderDocuments
    * @param orderName 
    * @returns onResolve
    */
    getDocuments(moduleId: string, folderId: string, orderDocuments: string, orderName: string) {
        let db = this.aFirestore
        let ref = null
        switch (orderDocuments) {
            case 'asc': {
                ref = db.collection('modules')
                    .doc(moduleId)
                    .collection('folders')
                    .doc(folderId)
                    .collection('documents', ref => ref.orderBy(orderName, 'asc'))

                break
            }

            case 'desc': {
                ref = db.collection('modules')
                    .doc(moduleId)
                    .collection('folders')
                    .doc(folderId)
                    .collection('documents', ref => ref.orderBy(orderName, 'desc'))

                break
            }

            case 'oldest': {
                ref = db.collection('modules')
                    .doc(moduleId)
                    .collection('folders')
                    .doc(folderId).
                    collection('documents', ref => ref.orderBy('createdAt', 'asc'))

                break
            }
            // recent'
            default: {
                ref = db.collection('modules')
                    .doc(moduleId)
                    .collection('folders')
                    .doc(folderId)
                    .collection('documents', ref => ref.orderBy('createdAt', 'desc'));

                break
            }
        }

        return (ref.valueChanges());
        // this.refGetDocuments = ref.valueChanges()
        //     .subscribe((documents) => {
        //         onResolve(documents)
        //     })
    }

    /**
    * close getDocuments() function reference 
    */
    closeRefGetDocuments() {
        if (this.refGetDocuments)
            this.refGetDocuments.unsubscribe()
    }
}
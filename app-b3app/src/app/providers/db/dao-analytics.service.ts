import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UUID } from 'angular2-uuid';
import { NotificationDateService } from '../date/notification-date.service';
import { TypeModule } from 'src/app/enums/type-module';

@Injectable({
    providedIn: 'root'
})
export class DaoAnalyticsService {

    constructor(
        private aFirestore: AngularFirestore,
        private luxon: NotificationDateService
    ) { }

    moduleAccess(eventId: string, moduleId: string) {
        let db = this.aFirestore.firestore;
        let batch = db.batch();
        let ref = db.collection('events').doc(eventId).collection('modules').doc(moduleId);
        ref
            .get()
            .then((snapshot) => {
                let module = snapshot.data();
                let increment = module.total_access + 1;

                batch.update(ref, { total_access: increment });

                batch
                    .commit()
                    .then((_) => {
                    }).catch((error) => {
                        console.error(error)
                    })
            })
            .catch((e) => {
                console.error(e);
            })
    }

    moduleAccessEvent(eventId: string) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('modules');

        ref
            .get()
            .then((snapshot) => {
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let module = element.data();

                        if (module.type == TypeModule.EVENT) {
                            this.moduleAccess(eventId, module.uid);
                        }
                    });
                }
            });
    }

    documentAccess(eventId: string, docId: string, timezone: string) {
        let db = this.aFirestore.firestore;
        // let timestamp = this.luxon.getTimeStampFromDateNow(new Date(), timezone);
        let ref = db.collection('events').doc(eventId).collection('documents').doc(docId);

        ref
            .get()
            .then((snapshot) => {
                let doc = snapshot.data();
                let increment = doc.total_access + 1;

                ref.update({ total_access: increment });
            })
    }

    galleryImageAccess(eventId: string, imageId: string, timezone: string) {
        let db = this.aFirestore.firestore;
        // let timestamp = this.luxon.getTimeStampFromDateNow(new Date(), timezone);
        let ref = db.collection('events').doc(eventId).collection('gallery-images').doc(imageId);

        ref
            .get()
            .then((snapshot) => {
                let img = snapshot.data();
                let increment = img.total_access + 1;

                ref.update({ total_access: increment });
            })
    }

    userAccess(eventId: string, timezone: string) {
        let db = this.aFirestore.firestore;
        let timestamp = this.luxon.getTimeStampFromDateNow(new Date(), timezone);
        let ref = db.collection('analytics').doc(eventId).collection('total-user-access').doc();

        let obj = {};
        let uid = UUID.UUID();
        obj[uid] = timestamp;

        ref.set(obj);
    }
}

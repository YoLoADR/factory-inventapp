import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { CeuAttendee } from 'src/app/models/ceu-attendee';
import { StorageService } from '../storage/storage.service';
import { CeuSpeaker } from 'src/app/models/ceu-speakers';
import { LuxonService } from '../luxon/luxon.service';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class DaoSpeakersService {
    refGetModule = null

    constructor(private aFirestore: AngularFirestore, private storage: StorageService, private luxon: LuxonService,
    ) {
        AngularFirestoreModule.enablePersistence();
    }

    // get the id module passed in the parameter.
    getModule(moduleId: string, onResolve) {
        this.refGetModule = this.aFirestore.collection('modules').doc(moduleId).valueChanges().subscribe((module) => {
            onResolve(module)
        })
    }

    // close get module
    closeRefGetModule() {
        if (this.refGetModule)
            this.refGetModule.unsubscribe()
    }

    getSpeakerByEvent(eventId: string, speakerId: string) {
        // let db = this.aFirestore.firestore;

        // db.collection('events')
        //     .doc(eventId)
        //     .collection('speakers')
        //     .doc(speakerId)
        //     .onSnapshot((user) => {
        //         onResolve(user.data())
        //     })
        
        return (this.aFirestore.collection('events')
            .doc(eventId)
            .collection('speakers')
            .doc(speakerId).snapshotChanges().pipe(
                map((snapshot) => {
                    return(snapshot.payload.data())
                })
            ))
    }

    getCustomFields(eventId: string, speakerId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db.collection('events')
            .doc(eventId)
            .collection('speakers')
            .doc(speakerId).collection('customFields');

        ref.onSnapshot((data) => {
            let listCustom = [];
            data.forEach(element => {
                let custom = element.data();
                listCustom.push(custom);
            });

            onResolve(listCustom)
        })
    }

    getCustomFieldOptions(moduleId: string, customId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db.collection('modules')
            .doc(moduleId)
            .collection('customFields')
            .doc(customId)
            .collection('options');

        ref.onSnapshot((data) => {
            let listOptions = [];

            data.forEach(element => {
                let option = element.data();

                listOptions.push(option);
            });

            onResolve(listOptions);
        })
    }

    getFieldOptionsCustom(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((value) => {
                let aux = value.data();
                onResolve(aux['fieldsCustom']);
            });
    }

    updateSpeaker(eventId: string, moduleId: string, speaker: CeuAttendee, customFields, file: any, onResolve) {
        let db = this.aFirestore.firestore;

        let refSpeakerEvent = db.collection('events').doc(eventId).collection('speakers').doc(speaker.uid);
        let refSpeakerModule = db.collection('modules').doc(moduleId).collection('speakers').doc(speaker.uid);

        let batch = db.batch();

        batch.update(refSpeakerEvent, speaker);
        batch.update(refSpeakerModule, speaker);

        for (let custom of customFields) {
            let refCustomEvent = refSpeakerEvent.collection('customFields').doc(custom.uid);
            let refCustomModule = refSpeakerModule.collection('customFields').doc(custom.uid);
            batch.update(refCustomEvent, { value: custom.value, textValue: custom.textValue });
            batch.update(refCustomModule, { value: custom.value, textValue: custom.textValue });
        }

        batch.commit()
            .then((data) => {
                if (file !== null && file !== undefined && file !== '') {
                    this.storage.uploadSpeakerProfile(eventId, speaker.uid, file, (url) => {
                        refSpeakerModule.update({ photoUrl: url });
                        refSpeakerEvent.update({ photoUrl: url })
                            .then(() => {
                                let result = {
                                    code: 200,
                                    message: 'Dados alterados com sucesso',
                                    result: data
                                }

                                onResolve(result)
                            })
                            .catch((error) => {
                                let result = {
                                    code: 500,
                                    message: 'Erro ao alterar foto de perfil',
                                    result: data
                                }

                                onResolve(result)
                            })
                    })
                } else {
                    let result = {
                        code: 200,
                        message: 'Dados alterados com sucesso',
                        result: data
                    }

                    onResolve(result)
                }

            })
            .catch((error) => {
                let result = {
                    code: 500,
                    message: 'Erro ao alterar dados',
                    result: error
                }

                onResolve(result)
            })

    }

    checkeditProfileActive(moduleId, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('modules').doc(moduleId).get().then((data) => {
            let doc = data.data();
            let result = doc.allowedEditProfile;

            if (result) {
                onResolve(true);
            } else {
                onResolve(false);
            }
        })
    }

    getOrder(moduleId, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('modules').doc(moduleId).get().then((data) => {
            let typeOrder = data.data().orderUsers;

            onResolve(typeOrder);
        })
    }

    getFirstSpeakersPageByModule(moduleId: string, typeOrder: string, onResolve) {
        let db = this.aFirestore.firestore;
        let firstPage;

        switch (typeOrder) {
            case 'asc': //a-z
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('name', 'asc')
                    .limit(100)
                break;

            case 'desc': //z-a
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('name', 'desc')
                    .limit(100)
                break;

            case 'oldest'://antiho-recente
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('createdAt', 'asc')
                    .limit(100)
                break;

            case 'recent': //recente-antigo
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('createdAt', 'desc')
                    .limit(100)
                break;
        }

        firstPage
            .onSnapshot((snapshot) => {
                if (snapshot.size >= 1) {
                    let nextPage = snapshot.docs[snapshot.docs.length - 1];
                    let auxArray = [];

                    snapshot.forEach(element => {
                        auxArray.push(element.data());
                    });
                    onResolve({
                        speakers: auxArray,
                        next: nextPage.data()
                    });
                } else {
                    onResolve({
                        speakers: [],
                        next: null
                    });
                }
            })
    }


    getNextSpeakersPageByModule(moduleId: string, nextPage, typeOrder: string, onResolve) {
        let db = this.aFirestore.firestore;
        let lastPage;

        switch (typeOrder) {
            case 'asc': //a-z
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('name', 'asc')
                    .startAfter(nextPage['name'])
                    .limit(100);
                break;

            case 'desc': //z-a
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('name', 'desc')
                    .startAfter(nextPage['name'])
                    .limit(100);
                break;

            case 'oldest'://antiho-recente
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('createdAt', 'asc')
                    .startAfter(nextPage['createdAt'])
                    .limit(100);
                break;

            case 'recent': //recente-antigo
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('speakers')
                    .orderBy('createdAt', 'desc')
                    .startAfter(nextPage['createdAt'])
                    .limit(100);
                break;
        }
        lastPage
            .onSnapshot((snapshot) => {
                let morePage = snapshot.docs[snapshot.docs.length - 1];
                let auxArray = [];
                if (snapshot.size == 0) {
                    onResolve(false);
                } else {
                    snapshot.forEach(element => {
                        auxArray.push(element.data());
                    });
                    onResolve({
                        speakers: auxArray,
                        next: morePage.data().name
                    });
                }
            })
    }

    getFieldOptions(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('modules')
            .doc(moduleId)
            .get()
            .then((value) => {
                let aux = value.data();
                onResolve(aux['fields']);
            });
    }

    searchSpeakersByName(moduleId: string, name: string, onResolve) {
        let db = this.aFirestore.firestore;
        name = this.convertLowerCaseUpperCase(name);
        let speakers = [];
        db
            .collection('modules')
            .doc(moduleId)
            .collection('speakers')
            .orderBy('name')
            .startAt(name)
            .endAt(name + '\uf8ff')
            .onSnapshot((values) => {
                values.forEach(el => {
                    speakers.push(el.data());
                });
                onResolve(speakers);
            });
    }

    convertLowerCaseUpperCase(name) {
        var words = name.trim().toLowerCase().split(" ");
        for (var a = 0; a < words.length; a++) {
            var w = words[a];
            words[a] = w[0].toUpperCase() + w.slice(1);
        }
        words.join(" ");
        return words[0];
    }

    // lists the speaker's sessions.
    getSessionsOfSpeaker(speakerId: string, eventId: string, onResolve) {
        let db = this.aFirestore.firestore;
        const speakers = `speakers.${speakerId}`;

        const ref = db.collection('events').doc(eventId).collection('sessions').orderBy(speakers)

        ref.onSnapshot((snapshot) => {
            const sessions = []
            snapshot.forEach((childSnapshot) => {
                const session = childSnapshot.data()
                sessions.push(session)
            })

            // sorts the sessions by the startTime field.
            sessions.sort(function (a, b) {
                return a.startTime < b.startTime ? -1 : a.startTime > b.startTime ? 1 : 0;
            });

            // traitement des dattes
            for (const session of sessions) {
                // dates
                session.date = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(session.date))
                session.startTime = this.luxon.dateTime(this.luxon.convertTimestampToDate(session.startTime))

                if (session.endTime !== "" && session.endTime !== null && typeof session.endTime !== 'undefined') {
                    session.endTime = this.luxon.dateTime(this.luxon.convertTimestampToDate(session.endTime))
                }
            }


            onResolve(sessions)
        })
    }

    /******************************************************************************** methods of global vision *************************************************************************************** */
    // loads the first 100 speakers of the module. global vision
    getFirstASpeakersGlobalVision(typeOrder, moduleId, onResolve) {
        let ref = null

        // checks the order that speakers are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'asc').limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'desc').limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'asc').limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'desc').limit(100));
                break;
        }
        // load speakers
        if (ref) {
            ref
                .valueChanges()
                .subscribe((data: any) => {
                    onResolve(data)
                })
        } else {
            onResolve([])
        }
    }


    // get all speakers of the module
    getSpeakersAllGlobalVision(moduleId) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<CeuSpeaker>('speakers')
            .snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as CeuSpeaker);
                    }))
                })
            )
        )
    }

    // get plus 100 speakers module.
    getNextPageSpeakersGlobalVision(moduleId, lastSpeaker, typeOrder, onResolve) {
        let ref = null

        // checks the order that participants are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'asc').startAfter(lastSpeaker.queryName).limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'desc').startAfter(lastSpeaker.queryName).limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'asc').startAfter(lastSpeaker.createdAt).limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'desc').startAfter(lastSpeaker.createdAt).limit(100));
                break;
        }

        ref
            .valueChanges()
            .subscribe((data: any) => {
                onResolve(data)
            })
    }

    /******************************************************************************** methods of divided by groups *************************************************************************************** */
    // loads the group speakers.
    getSpeakersGroupGlobalVision(groupId: string, typeOrder: string, moduleId: string, onResolve) {
        const group = `groups.${groupId}`

        // filters the speakers through the group
        this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('speakers', ref => ref.orderBy(group))
            .valueChanges()
            .subscribe((data) => {
                let ref = null

                // checks the order that speakers are to be loaded.
                switch (typeOrder) {
                    case 'asc': //a-z
                        data.sort(function (a, b) {
                            return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
                        });
                        break;

                    case 'desc': //z-a
                        data.sort(function (a, b) {
                            return a.queryName > b.queryName ? -1 : a.queryName < b.queryName ? 1 : 0;
                        });
                        break;

                    case 'oldest'://antiho-recente
                        data.sort(function (a, b) {
                            return a.createdAt < b.createdAt ? -1 : a.createdAt > b.createdAt ? 1 : 0;
                        });
                        break;

                    case 'recent': //recente-antigo
                        data.sort(function (a, b) {
                            return a.createdAt > b.createdAt ? -1 : a.createdAt < b.createdAt ? 1 : 0;
                        });
                        break;
                }

                onResolve(data)
            })
    }

    /******************************************************************************** groups vision *************************************************************************************** */
    // loads the speaker group speakers.
    getSpeakersGroupsVision(groups: Array<any>, typeOrder: string, moduleId: string, onResolve) {
        if (groups.length > 0) {
            const total = groups.length
            let i = 0
            const list = []

            // get the speakers of each group
            for (const group of groups) {
                const groupId = `groups.${group.uid}`

                // filters the speakers through the group
                this.aFirestore
                    .collection('modules')
                    .doc(moduleId).collection<CeuSpeaker>('speakers', ref => ref.orderBy(groupId))
                    .snapshotChanges().pipe(
                        map((snapshot) => {
                            return (snapshot.map((doc) => {
                                return (doc.payload.doc.data() as CeuSpeaker);
                            }))
                        })
                    )
                    .subscribe((snapshot) => {
                        i++

                        // withdraws replay of speakers
                        snapshot.forEach((speaker) => {
                            const pos = list.map(function (e) { return e.uid; }).indexOf(speaker.uid);
                            if (pos === -1) { list.push(speaker) }
                        })

                        // if all groups have already been processed.
                        if (i >= total) {
                            if (list.length <= 0) { onResolve([]) }


                            // checks the order that participants are to be loaded.
                            switch (typeOrder) {
                                case 'asc': //a-z
                                    list.sort(function (a, b) {
                                        return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
                                    });
                                    break;

                                case 'desc': //z-a
                                    list.sort(function (a, b) {
                                        return a.queryName > b.queryName ? -1 : a.queryName < b.queryName ? 1 : 0;
                                    });
                                    break;

                                case 'oldest'://antiho-recente
                                    list.sort(function (a, b) {
                                        return a.createdAt < b.createdAt ? -1 : a.createdAt > b.createdAt ? 1 : 0;
                                    });
                                    break;

                                case 'recent': //recente-antigo
                                    list.sort(function (a, b) {
                                        return a.createdAt > b.createdAt ? -1 : a.createdAt < b.createdAt ? 1 : 0;
                                    });
                                    break;
                            }

                            onResolve(list)
                        }
                    })
            }
        } else {
            onResolve([])
        }
    }

    /******************************************************************************** methods of limited access by groups *************************************************************************************** */
    // loads the first 100 speakers of the module. global vision
    getFirstASpeakersLimitedAccessByGroups(typeOrder, moduleId, onResolve) {
        let ref = null

        // checks the order that speakers are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'asc').limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'desc').limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'asc').limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'desc').limit(100));
                break;
        }
        // load speakers
        if (ref) {
            ref.valueChanges().subscribe((data: any) => {
                onResolve(data)
            })
        } else {
            onResolve([])
        }
    }

    // get all speakers of the module
    getSpeakersAllLimitedAccessByGroup(moduleId) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<CeuSpeaker>('speakers')
            .snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as CeuSpeaker);
                    }))
                })
            )
        )
    }

    // get plus 100 speakers module.
    getNextPageSpeakersLimitedAccessByGroup(moduleId, lastSpeaker, typeOrder, onResolve) {
        let ref = null

        // checks the order that participants are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'asc').startAfter(lastSpeaker.queryName).limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('queryName', 'desc').startAfter(lastSpeaker.queryName).limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'asc').startAfter(lastSpeaker.createdAt).limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuSpeaker>('speakers', ref => ref.orderBy('createdAt', 'desc').startAfter(lastSpeaker.createdAt).limit(100));
                break;
        }

        ref
            .valueChanges()
            .subscribe((data: any) => {
                onResolve(data)
            })
    }

}

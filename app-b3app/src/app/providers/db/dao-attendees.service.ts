import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule, AngularFirestoreCollection } from '@angular/fire/firestore';
import { CeuAttendee } from 'src/app/models/ceu-attendee';
import { StorageService } from '../storage/storage.service';
import { TypeModule } from 'src/app/models/type-module';
import { GlobalService } from 'src/app/shared/services';
import { map } from 'rxjs/operators';
import { User } from 'firebase';

@Injectable({
    providedIn: 'root'
})

export class DaoAttendeesService {
    refGetModule = null

    constructor(
        private aFirestore: AngularFirestore,
        private storage: StorageService,
        public global: GlobalService
    ) {
        AngularFirestoreModule.enablePersistence();
    }


    // get the id module passed in the parameter.
    getModule(moduleId: string, onResolve) {
        this.refGetModule = this.aFirestore.collection('modules').doc(moduleId).valueChanges().subscribe((module) => {
            onResolve(module)
        })
    }

    // close get module
    closeRefGetModule() {
        if (this.refGetModule)
            this.refGetModule.unsubscribe()
    }

    checkeditProfileActive(moduleId, onResolve) {
        let db = this.aFirestore.firestore;
        db.collection('modules').doc(moduleId).get().then((data) => {
            let doc = data.data();
            let result = doc.allowedEditProfile;

            if (result) {
                onResolve(true);
            } else {
                onResolve(false);
            }
        })
    }

    getChatStatus(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((snapshot) => {
                let module = snapshot.data();
                onResolve(module['allow_chat']);
            })
            .catch((e) => {
                onResolve(null);
            })
    }

    getFirstAttendeesPageByModule(moduleId: string, typeOrder: string, onResolve) {
        let db = this.aFirestore.firestore;
        let firstPage;

        switch (typeOrder) {
            case 'asc': //a-z
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('name', 'asc')
                    .limit(100)
                break;

            case 'desc': //z-a
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('name', 'desc')
                    .limit(100)
                break;

            case 'oldest'://antiho-recente
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('createdAt', 'asc')
                    .limit(100)
                break;

            case 'recent': //recente-antigo
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('createdAt', 'desc')
                    .limit(100)
                break;

            case 'id': //ID
                firstPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('identifier')
                    .limit(100)
                break;
        }

        firstPage
            .onSnapshot((snapshot) => {
                if (snapshot.size >= 1) {
                    let nextPage = snapshot.docs[snapshot.docs.length - 1];
                    let auxArray = [];

                    snapshot.forEach(element => {
                        auxArray.push(element.data());
                    });
                    onResolve({
                        attendees: auxArray,
                        next: nextPage.data()
                    });
                } else {
                    onResolve({
                        attendees: [],
                        next: null
                    });
                }

            })
    }

    getNextAttendeesPageByModule(moduleId: string, nextPage, typeOrder: string, onResolve) {
        let db = this.aFirestore.firestore;
        let lastPage;

        switch (typeOrder) {
            case 'asc': //a-z
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('name', 'asc')
                    .startAfter(nextPage['name'])
                    .limit(100);
                break;

            case 'desc': //z-a
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('name', 'desc')
                    .startAfter(nextPage['name'])
                    .limit(100);
                break;

            case 'oldest'://antiho-recente
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('createdAt', 'asc')
                    .startAfter(nextPage['createdAt'])
                    .limit(100);
                break;

            case 'recent': //recente-antigo
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('createdAt', 'desc')
                    .startAfter(nextPage['createdAt'])
                    .limit(100);
                break;

            case 'id': //ID
                lastPage = db.collection('modules')
                    .doc(moduleId)
                    .collection('attendees')
                    .orderBy('identifier')
                    .startAfter(nextPage['createdAt'])
                    .limit(100);
                break;
        }
        lastPage
            .onSnapshot((snapshot) => {
                let morePage = snapshot.docs[snapshot.docs.length - 1];
                let auxArray = [];

                if (snapshot.size == 0) {
                    onResolve(false);
                } else {
                    snapshot.forEach(element => {
                        auxArray.push(element.data());
                    });
                    onResolve({
                        attendees: auxArray,
                        next: morePage.data()
                    });
                }
            })
    }

    getAttendeeByEvent(eventId: string, attendeeId: string) {
        // let db = this.aFirestore.firestore;

        // db.collection('events')
        //     .doc(eventId)
        //     .collection('attendees')
        //     .doc(attendeeId)
        //     .onSnapshot((user) => {
        //         onResolve(user.data())
        //     })
        return (this.aFirestore.collection('events')
            .doc(eventId)
            .collection('attendees')
            .doc(attendeeId).snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.payload.data() as CeuAttendee);
                })
            ))
    }

    getOrder(moduleId, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('modules').doc(moduleId).get().then((data) => {
            let typeOrder = data.data().orderUsers;

            onResolve(typeOrder);
        })
    }

    getCustomFields(eventId: string, attendeeId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db.collection('events')
            .doc(eventId)
            .collection('attendees')
            .doc(attendeeId).collection('customFields');

        ref.onSnapshot((data) => {
            let listCustom = [];
            data.forEach(element => {
                let custom = element.data();
                listCustom.push(custom);
            });

            onResolve(listCustom)
        })
    }

    getCustomFieldOptions(moduleId: string, attendeeId: string, customId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db.collection('modules')
            .doc(moduleId)
            .collection('customFields')
            .doc(customId)
            .collection('options');

        ref.onSnapshot((data) => {
            let listOptions = [];

            data.forEach(element => {
                let option = element.data();

                listOptions.push(option);
            });

            onResolve(listOptions);
        })
    }

    updateAttendee(eventId: string, moduleId: string, attendee: CeuAttendee, customFields, file: any, onResolve) {
        let db = this.aFirestore.firestore;

        let refAttendeeEvent = db.collection('events').doc(eventId).collection('attendees').doc(attendee.uid);
        let refAttendeeModule = db.collection('modules').doc(moduleId).collection('attendees').doc(attendee.uid);

        let batch = db.batch();

        batch.update(refAttendeeEvent, attendee);
        batch.update(refAttendeeModule, attendee);

        for (let custom of customFields) {
            let refCustomEvent = refAttendeeEvent.collection('customFields').doc(custom.uid);
            let refCustomModule = refAttendeeModule.collection('customFields').doc(custom.uid);
            batch.update(refCustomEvent, { value: custom.value, textValue: custom.textValue });
            batch.update(refCustomModule, { value: custom.value, textValue: custom.textValue });
        }

        batch.commit()
            .then((data) => {
                if (file !== null && file !== undefined && file !== '') {
                    this.storage.uploadAttendeeProfile(eventId, attendee.uid, file, (url) => {
                        refAttendeeModule.update({ photoUrl: url });
                        refAttendeeEvent.update({ photoUrl: url })
                            .then(() => {
                                let result = {
                                    code: 200,
                                    message: 'Dados alterados com sucesso',
                                    result: data
                                }

                                onResolve(result)
                            })
                            .catch((error) => {
                                let result = {
                                    code: 500,
                                    message: 'Erro ao alterar foto de perfil',
                                    result: data
                                }

                                onResolve(result)
                            })
                    })
                } else {
                    let result = {
                        code: 200,
                        message: 'Dados alterados com sucesso',
                        result: data
                    }

                    onResolve(result)
                }

            })
            .catch((error) => {
                let result = {
                    code: 500,
                    message: 'Erro ao alterar dados',
                    result: error
                }

                onResolve(result)
            })

    }

    getFieldOptions(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((value) => {
                let aux = value.data();
                onResolve(aux['fields']);
            });
    }

    getFieldOptionsCustom(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((value) => {
                let aux = value.data();
                onResolve(aux['fieldsCustom']);
            });
    }

    searchAttendeesByName(moduleId: string, name: string, onResolve) {
        let db = this.aFirestore.firestore;
        name = this.convertLowerCaseUpperCase(name);
        let attendees = [];
        db
            .collection('modules')
            .doc(moduleId)
            .collection('attendees')
            .orderBy('name')
            .startAt(name)
            .endAt(name + '\uf8ff')
            .onSnapshot((values) => {
                values.forEach(el => {
                    attendees.push(el.data());
                });
                onResolve(attendees);
            });
    }

    convertLowerCaseUpperCase(name) {
        var words = name.trim().toLowerCase().split(" ");
        for (var a = 0; a < words.length; a++) {
            var w = words[a];
            words[a] = w[0].toUpperCase() + w.slice(1);
        }
        words.join(" ");
        return words[0];
    }

    getAvailableAttendeesChat(eventId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('modules');
        let refModule = db.collection('modules');
        ref
            .where('type', '==', TypeModule.ATTENDEE)
            .onSnapshot((snapshot) => {
                let attendees = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let module = element.data();
                        if (module.allow_chat == true) {
                            refModule
                                .doc(module.uid)
                                .collection('attendees')
                                .orderBy('queryName', 'asc')
                                .onSnapshot((response) => {
                                    if (response.size >= 1) {
                                        response.forEach(el => {
                                            let attendee = el.data();
                                            var letters = attendee.name.split('');
                                            var letter = letters[0].toUpperCase();
                                            attendee.letter = letter;
                                            attendee.principal_title = this.getAttendeePrincipalTitle(attendee.title);
                                            attendees.push(attendee);
                                        });
                                    }
                                })
                        }
                    });
                }
                onResolve(attendees);
            });
    }

    addPoints(eventId: string, moduleId: string, userId: string, points: number, onResolve) {
        let db = this.aFirestore.firestore;

        let refAttendeesEvent = db
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .doc(userId);

        let refAttendeesModule = db
            .collection('modules')
            .doc(moduleId)
            .collection('attendees')
            .doc(userId);

        refAttendeesEvent.get()
            .then((data) => {
                let attendee = data.data();

                let batch = db.batch();

                batch.update(refAttendeesEvent, { points: attendee.points + points });
                batch.update(refAttendeesModule, { points: attendee.points + points });

                batch.commit()
                    .then(() => {
                        onResolve(true);
                    });

            });
    }

    /******************************************************************************** methods of global vision *************************************************************************************** */
    // loads the first 100 participants of the module. global vision
    getFirstAttendeesGlobalVision(typeOrder, moduleId, onResolve) {
        let ref = null

        // checks the order that participants are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'asc').limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'desc').limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'asc').limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'desc').limit(100));
                break;

            case 'id': //ID
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('identifier').limit(100));
                break;
        }

        // load attendees
        if (ref) {
            ref.valueChanges()
                .subscribe((data: any) => {
                    onResolve(data)
                })
        } else {
            onResolve([])
        }
    }


    // get all attendees of the module
    getAttendeesAllGlobalVision(moduleId) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<CeuAttendee>('attendees')
            .snapshotChanges()
            .pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as CeuAttendee);
                    }))
                })
            )
        )
    }

    // get plus 100 attendees module.
    getNextPageAttendeesGlobalVision(moduleId, lastAttendee, typeOrder, onResolve) {
        let ref = null

        // checks the order that participants are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'asc').startAfter(lastAttendee.queryName).limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'desc').startAfter(lastAttendee.queryName).limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'asc').startAfter(lastAttendee.createdAt).limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'desc').startAfter(lastAttendee.createdAt).limit(100));
                break;

            case 'id': //ID
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('identifier').startAfter(lastAttendee.createdAt).limit(100));
                break;
        }

        ref.valueChanges()
            .subscribe((data: any) => {
                onResolve(data)
            })
    }

    /******************************************************************************** methods of divided by groups *************************************************************************************** */
    // loads the group attendees.
    getAttendeesGroupGlobalVision(groupId: string, typeOrder: string, moduleId: string, onResolve) {
        const group = `groups.${groupId}`

        // filters the participants through the group
        this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection<CeuAttendee>('attendees', ref => ref.orderBy(group))
            .valueChanges()
            .subscribe((data) => {
                let ref = null

                // checks the order that participants are to be loaded.
                switch (typeOrder) {
                    case 'asc': //a-z
                        data.sort(function (a, b) {
                            return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
                        });
                        break;

                    case 'desc': //z-a
                        data.sort(function (a, b) {
                            return a.queryName > b.queryName ? -1 : a.queryName < b.queryName ? 1 : 0;
                        });
                        break;

                    case 'oldest'://antiho-recente
                        data.sort(function (a, b) {
                            return a.createdAt < b.createdAt ? -1 : a.createdAt > b.createdAt ? 1 : 0;
                        });
                        break;

                    case 'recent': //recente-antigo
                        data.sort(function (a, b) {
                            return a.createdAt > b.createdAt ? -1 : a.createdAt < b.createdAt ? 1 : 0;
                        });
                        break;

                    case 'id': //ID
                        data.sort(function (a, b) {
                            return a.identifier < b.identifier ? -1 : a.identifier > b.identifier ? 1 : 0;
                        });
                        break;
                }

                onResolve(data)
            })
    }

    /******************************************************************************** groups vision *************************************************************************************** */
    // loads the participant group attendees.
    getAttendeesGroupsVision(groups: Array<any>, typeOrder: string, moduleId: string, onResolve) {
        if (groups.length > 0) {
            const total = groups.length
            let i = 0
            const list = []

            // get the attendees of each group
            for (const group of groups) {
                const groupId = `groups.${group.uid}`

                // filters the participants through the group
                this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy(groupId))
                    .valueChanges()
                    .subscribe((snapshot) => {
                        i++

                        // withdraws replay of attendees
                        snapshot.forEach((attendee) => {
                            const pos = list.map(function (e) { return e.uid; }).indexOf(attendee.uid);
                            if (pos === -1) { list.push(attendee) }
                        })

                        // if all groups have already been processed.
                        if (i >= total) {
                            if (list.length <= 0) { onResolve([]) }


                            // checks the order that participants are to be loaded.
                            switch (typeOrder) {
                                case 'asc': //a-z
                                    list.sort(function (a, b) {
                                        return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
                                    });
                                    break;

                                case 'desc': //z-a
                                    list.sort(function (a, b) {
                                        return a.queryName > b.queryName ? -1 : a.queryName < b.queryName ? 1 : 0;
                                    });
                                    break;

                                case 'oldest'://antiho-recente
                                    list.sort(function (a, b) {
                                        return a.createdAt < b.createdAt ? -1 : a.createdAt > b.createdAt ? 1 : 0;
                                    });
                                    break;

                                case 'recent': //recente-antigo
                                    list.sort(function (a, b) {
                                        return a.createdAt > b.createdAt ? -1 : a.createdAt < b.createdAt ? 1 : 0;
                                    });
                                    break;

                                case 'id': //ID
                                    list.sort(function (a, b) {
                                        return a.identifier < b.identifier ? -1 : a.identifier > b.identifier ? 1 : 0;
                                    });
                                    break;
                            }

                            onResolve(list)
                        }
                    })
            }
        } else {
            onResolve([])
        }
    }

    /******************************************************************************** methods of limited access by groups *************************************************************************************** */
    // loads the first 100 participants of the module. 
    getFirstAttendeesLimitedAccessByGroup(typeOrder, moduleId, onResolve) {
        let ref = null

        // checks the order that participants are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'asc').limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'desc').limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'asc').limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'desc').limit(100));
                break;

            case 'id': //ID
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('identifier').limit(100));
                break;
        }

        // load attendees
        if (ref) {
            ref.
                valueChanges()
                .subscribe((data: any) => {
                    onResolve(data)
                })
        } else {
            onResolve([])
        }
    }

    // get all attendees of the module
    getAttendeesAllLimitedAccessByGroup(moduleId) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<CeuAttendee>('attendees')
            .snapshotChanges()
            .pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as CeuAttendee);
                    }))
                })
            )
        )
    }

    // get plus 100 attendees module.
    getNextPageAttendeesLimitedAccessByGroup(moduleId, lastAttendee, typeOrder, onResolve) {
        let ref = null

        // checks the order that participants are to be loaded.
        switch (typeOrder) {
            case 'asc': //a-z
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'asc').startAfter(lastAttendee.queryName).limit(100));
                break;

            case 'desc': //z-a
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'desc').startAfter(lastAttendee.queryName).limit(100));
                break;

            case 'oldest'://antiho-recente
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'asc').startAfter(lastAttendee.createdAt).limit(100));
                break;

            case 'recent': //recente-antigo
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('createdAt', 'desc').startAfter(lastAttendee.createdAt).limit(100));
                break;

            case 'id': //ID
                ref = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<CeuAttendee>('attendees', ref => ref.orderBy('identifier').startAfter(lastAttendee.createdAt).limit(100));
                break;
        }

        ref
            .valueChanges()
            .subscribe((data: any) => {
                onResolve(data)
            })
    }


    getAttendeePrincipalTitle(title) {
        let principalTitle = '';
        switch (this.global.language) {
            case 'pt_BR': {
                principalTitle = title.PtBR;
                break;
            }
            case 'en_US': {
                principalTitle = title.EnUS;
                break;
            }
            case 'es_ES': {
                principalTitle = title.EsES;
                break;
            }
            case 'fr_FR': {
                principalTitle = title.FrFR;
                break;
            }
            case 'de_DE': {
                principalTitle = title.deDE;
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == '' || principalTitle == null) {
            if (title[this.convertLangFormat(this.global.event.language)] !== '') {
                principalTitle = title[this.convertLangFormat(this.global.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== '') {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        return principalTitle;
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }
}

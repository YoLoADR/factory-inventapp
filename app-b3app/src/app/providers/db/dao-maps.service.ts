import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class DaoMapsService {
    constructor(
        private aFirestore: AngularFirestore
    ) { }

    getModule(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((snapshot) => {
                onResolve(snapshot.data());
            });
    }

    getMap(moduleId: string, mapId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .collection('maps')
            .doc(mapId)
            .get()
            .then((snapshot) => {
                onResolve(snapshot.data());
            });
    }
}
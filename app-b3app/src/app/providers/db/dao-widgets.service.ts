import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DaoWidgetsService {

  constructor(private aFirestore: AngularFirestore) {
    AngularFirestoreModule.enablePersistence();
  }

  getWidgetByModule(moduleId: string, onResolve) {
    let firestore = this.aFirestore.firestore;
    firestore.collection('modules')
      .doc(moduleId)
      .collection('widgets')
      .orderBy('order', 'asc')
      .onSnapshot((data) => {
        let arrayAux = [];

        data.forEach(doc => {
          arrayAux.push(doc.data());
        });
        onResolve(arrayAux);
      })
  }

  getModule(moduleId: string, onResolve) {
    let db = this.aFirestore.firestore;

    db
      .collection('modules')
      .doc(moduleId)
      .onSnapshot((snapshot) => {
        onResolve(snapshot.data());
      });
  }
}

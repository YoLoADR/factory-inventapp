import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Survey } from 'src/app/models/survey';
import { Question } from 'src/app/models/question';
import { Answer } from 'src/app/models/answer';
import { firestore } from 'firebase/app';
import { TypeModule } from 'src/app/models/type-module';
import { map, take, switchMap } from 'rxjs/operators';
import { of, Observable, combineLatest } from 'rxjs';
import { UtilityService } from 'src/app/shared/services';

@Injectable({
    providedIn: 'root'
})
export class DaoSurveyService {

    constructor(
        private aFirestore: AngularFirestore,
        private SUtility: UtilityService
    ) {
    }

    /**
     * Get survey module for specified event
     * @param eventId 
     */
    getSurveyModule(eventId: string) {
        return (this.aFirestore
            .collection('events')
            .doc(eventId)
            .collection('modules', (ref) => ref.where('type', '==', TypeModule.SURVEY))
            .get().pipe(
                take(1),
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        return (doc.data());
                    }))
                })
            ))
    }

    /**
     * Get module of a survey
     * @param surveyModuleId 
     */
    getModule(surveyModuleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(surveyModuleId)
            .get().pipe(
                take(1),
                map((doc) => {
                    return (doc.data());
                })
            ))
    }

    /**
     * Getting surveys
     * @param moduleId 
     */
    getSurveys(moduleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('surveys', (ref) => ref.orderBy('order', 'asc'))
            .snapshotChanges().pipe(
                switchMap((docs) => {
                    let listSurveys = [];
                    let obs = [];
                    if (docs && docs.length > 0) {
                        for (let i = 0; i < docs.length; i++) {
                            let survey = this.instantiateSurvey(docs[i].payload.doc.data());
                            if (survey.visibility) {
                                let index = this.SUtility.checkIndexExists(listSurveys, survey);

                                if (index >= 0) {
                                    listSurveys[index] = survey;
                                } else {
                                    listSurveys.push(survey);
                                }
                            }
                        }
                        listSurveys.forEach((survey) => {
                            obs.push(this.getQuestions(moduleId, survey.uid).pipe(
                                map((questions) => {
                                    survey.questions = questions;
                                    return (survey);
                                })
                            ));
                        })

                        return (combineLatest(obs));
                    } else {
                        return (of([]));
                    }
                })
            ))
    }

    /**
     * Get all surveys for an event
     * @param moduleId 
     */
    getSurveysEvent(moduleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .valueChanges().pipe(
                map((docs) => {
                    return (docs.map((doc) => {
                        let survey = this.instantiateSurvey(doc);
                        return (survey);
                    }))
                })
            ))
    }

    /**
     * Get session of survey
     * @param eventId 
     * @param moduleId 
     * @param scheduleModuleId 
     * @param sessionId 
     * @param userId 
     * @param onResolve 
     */
    getSurveysSession(eventId: string, moduleId: string, scheduleModuleId: string, sessionId: string, userId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('surveys', (ref) => ref.orderBy('order', 'asc'))
            .snapshotChanges().pipe(
                switchMap(async (docs) => {
                    let listSurveys: Array<Survey> = [];
                    let listSurveysSession: Array<Survey> = [];

                    if (docs.length > 0) {
                        docs.forEach((element) => {
                            let survey = this.instantiateSurvey(element.payload.doc.data());
                            if (survey.visibility) {
                                let index = this.SUtility.checkIndexExists(listSurveys, survey);

                                if (index >= 0) {
                                    listSurveys[index] = survey;
                                } else {
                                    listSurveys.push(survey);
                                }
                            }
                        })
                        for (let survey of listSurveys) {

                            if (survey.type === 'AllSessions') {
                                listSurveysSession.push(survey);
                            }
                            else if (survey.type === 'ScheduleModule') {
                                if (scheduleModuleId === survey.module_id) {
                                    listSurveysSession.push(survey);
                                }
                            }
                            else if (survey.type === 'SessionTrack') {
                                // case this session be inside a track with permission, display feedback
                                await this.checkSpecificTrackSurvey(survey.module_id, sessionId, survey.references, (tracks: Array<boolean>) => {
                                    for (const track of tracks) {
                                        if (track) {
                                            listSurveysSession.push(survey);
                                            break;
                                        }
                                    }
                                });
                            }
                            else if (survey.type === 'SpecificSession') {
                                for (let uid of survey.references) {
                                    if (uid === sessionId) {
                                        listSurveysSession.push(survey);
                                        break;
                                    }
                                }
                            }
                            else if (survey.type === 'SpecificGroup') {
                                await this.listgroupsOfAttendee(eventId, userId, (listGroupsAttendee) => {
                                    const references: any = survey.references
                                    let findGroup = false;
                                    for (let uidGroup of references) {
                                        for (const uidGroupUser of listGroupsAttendee) {
                                            //if the user is part of the group
                                            if (uidGroup == uidGroupUser && findGroup == false) {
                                                listSurveysSession.push(survey);
                                                findGroup = true;
                                                break;
                                            }
                                        }

                                    }
                                })
                            }
                        }
                    }
                    return (listSurveysSession);
                })
            ))
    }

    /**
     * Get survey
     * @param moduleId 
     * @param surveyId 
     */
    getSurvey(moduleId: string, surveyId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .get().pipe(
                map((doc) => {
                    if (doc.exists) {
                        let survey: Survey = this.instantiateSurvey(doc.data());
                        return (survey);
                    } else {
                        return (null);
                    }
                }),
                switchMap((survey) => {
                    if (survey) {
                        return (this.getQuestions(moduleId, surveyId).pipe(
                            switchMap((questions) => {
                                survey.questions = questions;
                                return (of(survey));
                            })
                        ))
                    } else {
                        return (of(null));
                    }
                })
            ))
    }

    /**
     * Get total of questions on a survey
     * @param surveyModuleId 
     * @param surveyId 
     */
    getTotalQuestions(surveyModuleId: string, surveyId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(surveyModuleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .get().pipe(
                take(1),
                switchMap((docs) => {
                    return (of(docs.size));
                })
            ))
    }

    /**
     * Get questions of a survey
     * @param moduleId 
     * @param quizId 
     */
    getQuestions(surveyModuleId: string, surveyId: string): Observable<any> {
        return (this.aFirestore
            .collection('modules')
            .doc(surveyModuleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions', (ref) => ref.orderBy('createdAt', 'asc'))
            .get().pipe(
                take(1),
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        let question = this.instantiateQuestion(doc.data());
                        return (question);
                    }))
                }),
                switchMap((questions) => {
                    let obs = [];
                    if (questions && questions.length > 0) {
                        for (let i = 0; i < questions.length; i++) {
                            let question = questions[i];
                            obs.push(this.getAnswers(surveyModuleId, surveyId, question.uid).pipe(
                                map((answers) => {
                                    question.answers = answers;
                                    return (question);
                                })
                            ));
                        }

                        return (combineLatest(obs))
                    } else {
                        return (of([]));
                    }
                })
            ))
    }

    /**
     * Get answer for the questions of a survey
     * @param quizModuleId 
     * @param quizId 
     * @param questionId 
     */
    getAnswers(surveyModuleId: string, surveyId: string, questionId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(surveyModuleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(questionId)
            .collection('answers', (ref) => ref.orderBy('createdAt', 'asc'))
            .get().pipe(
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        let answer: Answer = this.instantiateAnswer(doc.data());
                        return (answer)
                    }))
                })
            ))
    }

    checkSpecificTrackSurvey(trackModuleId, sessionId, tracksIds, onResolve) {
        let list = [];
        for (let trackId of tracksIds) {
            this.checkTrackSurvey(trackModuleId, sessionId, trackId, (result: boolean) => {
                list.push(result);

                if (list.length == tracksIds.length) {
                    onResolve(list);
                }
            })
        }
    }

    // VERIFICA SE A TRACK EXISTE NA SESSÃO
    checkTrackSurvey(moduleId, sessionId, trackId, onResolve) {
        let db = this.aFirestore.firestore;
        db
            .collection('modules')
            .doc(moduleId)
            .collection('tracks')
            .doc(trackId)
            .collection('sessions')
            .doc(sessionId).get()
            .then((data) => {
                if (data.exists) {
                    onResolve(true);
                } else {
                    onResolve(false);
                }
            })
    }

    listgroupsOfAttendee(eventId: string, attendeeId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).get()
            .then((data) => {
                let attendee = data.data();
                let groups = attendee['groups'];
                let idsGroups: Array<String> = [];

                if (groups !== null && groups !== undefined) {
                    for (let uid in groups) {
                        idsGroups.push(uid);
                    }
                }

                onResolve(idsGroups);
            })
    }


    createResult(moduleId: string, userId: string, surveyId: string, IdQuestion: string, type: string, answer: any, timestamp: number, documentResponse: any, onResolve) {
        let db = this.aFirestore.firestore;
        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        if (type == "oneSelect" || type == "multipleSelect") {
            if (userId !== null && userId !== undefined && userId !== '') {
                this.getResponseUserQuestion(moduleId, surveyId, IdQuestion, userId).subscribe((responses) => {
                    this.deletePrevAnswersSelectOption(moduleId, surveyId, IdQuestion, userId, (result) => {
                        if (result == true) {
                            refQuestionsResult.doc(userId).set({
                                user: userId,
                                answer: answer,
                                timestamp: timestamp,
                                question: IdQuestion,
                                title: (documentResponse) ? documentResponse.title : null,
                                docType: (documentResponse) ? documentResponse.docType : null,
                            })
                                .then(() => {
                                    if (responses !== null && responses !== undefined) {
                                        onResolve(false);
                                    } else {
                                        onResolve(true);
                                    }
                                })
                                .catch((e) => {
                                });
                        }
                    })
                })
            } else {
                let document = refQuestionsResult.doc();
                userId = document.id;

                refQuestionsResult.doc(userId).set({
                    user: userId,
                    answer: answer,
                    timestamp: timestamp,
                    question: IdQuestion,
                    title: (documentResponse) ? documentResponse.title : null,
                    docType: (documentResponse) ? documentResponse.docType : null,
                })
                    .then(() => {
                        onResolve(true);
                    })

            }
        } else {
            if (userId !== null && userId !== undefined && userId !== '') {
                refQuestionsResult.doc(userId).set({
                    question: IdQuestion,
                    user: userId,
                    answer: answer,
                    timestamp: timestamp,
                    title: (documentResponse) ? documentResponse.title : null,
                    docType: (documentResponse) ? documentResponse.docType : null,
                }).then(() => {
                    onResolve(true);
                })
            } else {
                let document = refQuestionsResult.doc();
                userId = document.id;

                refQuestionsResult.doc(userId).set({
                    question: IdQuestion,
                    user: userId,
                    answer: answer,
                    timestamp: timestamp,
                    title: (documentResponse) ? documentResponse.title : null,
                    docType: (documentResponse) ? documentResponse.docType : null,
                })
                    .then(() => {
                        onResolve(true);
                    })
                    .catch((e) => {
                    });
            }
        }
    }

    /**
     * Get user response
     * @param moduleId 
     * @param surveyId 
     * @param userId 
     * @param onResolve 
     */
    getResponseUsers(moduleId, surveyId, userId, onResolve) {
        this.getQuestions(moduleId, surveyId).subscribe((questions) => {
            let listResults = {};
            for (let i = 0; i < questions.length; i++) {
                this.getResponseUserQuestion(moduleId, surveyId, questions[i].uid, userId).subscribe((response) => {
                    if (response != null) {
                        listResults[response.question] = response;
                    }

                    if (i == questions.length - 1) {
                        onResolve(listResults);
                    }
                })
            }
        })
    }

    /**
     * Get response user question
     * @param moduleId 
     * @param surveyId 
     * @param questionId 
     * @param userId 
     * @param onResolve 
     */
    getResponseUserQuestion(moduleId, surveyId, questionId, userId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(questionId)
            .collection('result').doc(userId).get().pipe(
                take(1),
                map((doc) => {
                    return (doc.data());
                })
            ))
    }

    deletePrevAnswersSelectOption(moduleId, surveyId, IdQuestion, userId, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        refQuestionsResult.doc(userId).delete().then(() => {
            onResolve(true)
        }).catch((e) => {
            onResolve(false);
        });
    }

    instantiateSurvey(data) {
        let survey = new Survey();
        survey.title = data.title;
        survey.type = data.type;
        survey.uid = data.uid;
        survey.visibility = data.visibility;
        survey.change_answer = data.change_answer;
        survey.view_answered = data.view_answered;
        survey.icon = data.icon;
        survey.iconFamily = data.iconFamily;

        data.max_responses != undefined ? survey.max_responses = data.max_responses : survey.max_responses = null;
        data.module_id != undefined ? survey.module_id = data.module_id : survey.module_id = null;
        data.references != undefined ? survey.references = data.references : survey.references = null;

        return survey;
    }

    instantiateQuestion(data) {
        let question = new Question();

        question.uid = data.uid;
        question.type = data.type;
        question.mainTitle = data.mainTitle;
        question.title = data.title;
        question.points = data.points;
        data.infobooth != undefined ? question.infobooth = data.infobooth : question.infobooth = '';
        data.maxResponses != undefined ? question.maxResponses = data.maxResponses : question.maxResponses = null;
        data.graphic != undefined ? question.graphic = data.graphic : question.graphic = null;
        data.visibility != undefined ? question.visibility = data.visibility : question.visibility = true;

        return question;
    }

    instantiateAnswer(data) {
        let answer = new Answer();
        answer.uid = data.uid;
        answer.answer = data.answer;
        data.weight != undefined ? answer.weight = data.weight : answer.weight = null;

        return answer;
    }

    setUserSurveyAnswered(eventId: string, attendeeId: string, surveyId: string) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId);
        ref.update({ answeredSurveys: firestore.FieldValue.arrayUnion(surveyId) }).then((_) => { });
    }

    generateResult1000() {
        let moduleId = '1';
        let surveyId = '48zpZRwbMJPaQg5BLNqz';
        let IdQuestion = 'TV5PKCg91ykANmpC1hi1';
        let answer = 'Olá, Mundo. Como vai você?';
        let timestamp = Date.now() / 1000 | 0;


        for (let i = 0; i < 1000; i++) {
            let userId = this.textoAleatorio(20);
            let db = this.aFirestore.firestore;

            let refQuestionsResult = db
                .collection('modules')
                .doc(moduleId)
                .collection('surveys')
                .doc(surveyId)
                .collection('questions')
                .doc(IdQuestion)
                .collection('result');

            refQuestionsResult.doc(userId).set({
                user: userId,
                answer: answer,
                timestamp: timestamp
            }).then(() => {

            }).catch((error) => {

            })
        }
    }

    testeget() {
        let moduleId = '1';
        let surveyId = '48zpZRwbMJPaQg5BLNqz';
        let IdQuestion = 'TV5PKCg91ykANmpC1hi1';
        let answer = 'Olá, Mundo. Como vai você?';
        let timestamp = Date.now() / 1000 | 0;

        let userId = this.textoAleatorio(20);
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('surveys')
            .doc(surveyId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        refQuestionsResult.get()
            .then((data) => {
                let array = [];
                data.forEach(element => {
                    array.push(element.data());
                });
            })
    }

    textoAleatorio(tamanho) {
        var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
        var aleatorio = '';
        for (var i = 0; i < tamanho; i++) {
            var rnum = Math.floor(Math.random() * letras.length);
            aleatorio += letras.substring(rnum, rnum + 1);
        }
        return aleatorio;
    }

}
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Training } from 'src/app/models/training';
import { Question } from 'src/app/models/training-question';
import { Answer } from 'src/app/models/training-answer';
import { firestore }  from 'firebase/app';
import { TypeModule } from 'src/app/models/type-module';
import { NameModule } from 'src/app/models/path/name-module';

@Injectable({
    providedIn: 'root'
})
export class DaoTrainingService {

    constructor(private aFirestore: AngularFirestore) {
        // AngularFirestoreModule.enablePersistence();  
    }

    getTrainingModule(eventId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('modules');

        ref
            .where('type', '==', TypeModule.TRAINING)
            .get()
            .then((snapshot) => {
                let auxModule = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        auxModule.push(element.data());
                    });
                }
                onResolve(auxModule);
            })
            .catch((e) => {
                onResolve(e);
            })
    }

    getModule(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .get()
            .then((snapshot) => {
                onResolve(snapshot.data());
            });
    }
    getTrainings(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let listTrainings: Array<Training> = [];

        let refTrainings = db.collection('modules').doc(moduleId).collection('trainings').orderBy('order', 'asc');

        refTrainings
            .onSnapshot((data) => {
                if (data.size >= 1) {
                    data.forEach(element => {
                        let training = this.instantiateTraining(element.data());
                        if (training.visibility) {
                            let index = this.checkIndexExists(listTrainings, training);

                            if (index >= 0) {
                                listTrainings[index] = training;
                            } else {
                                listTrainings.push(training);
                            }
                        }
                    });

                    let cont = 0;
                    for (let training of listTrainings) {
                        this.getQuestions(moduleId, training.uid, (questions) => {
                            training.questions = questions;
                            if (cont == listTrainings.length - 1) {
                                onResolve(listTrainings)
                            }

                            cont++;
                        })
                    }
                } else {
                    onResolve(listTrainings);
                }
            })
    }

    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    getTrainingsSession(eventId: string, moduleId: string, scheduleModuleId: string, sessionId: string, userId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let listTrainings: Array<Training> = [];
        let listTrainingsSession: Array<Training> = [];

        let refTrainings = db.collection('modules').doc(moduleId).collection('trainings').orderBy('order', 'asc');

        refTrainings
            .onSnapshot(async (data) => {
                if (data.size >= 1) {
                    data.forEach(element => {
                        let training = this.instantiateTraining(element.data());
                        if (training.visibility) {
                            let index = this.checkIndexExists(listTrainings, training);

                            if (index >= 0) {
                                listTrainings[index] = training;
                            } else {
                                listTrainings.push(training);
                            }
                        }
                    });

                    let cont = 0;
                    for (let training of listTrainings) {

                        if (training.type === 'AllSessions') {
                            listTrainingsSession.push(training);

                            onResolve(listTrainingsSession)

                        }
                        else if (training.type === 'ScheduleModule') {
                            if (scheduleModuleId === training.module_id) {
                                listTrainingsSession.push(training);
                            }

                            onResolve(listTrainingsSession)
                        }
                        else if (training.type === 'SessionTrack') {
                            // case this session be inside a track with permission, display feedback
                            await this.checkSpecificTrackTraining(training.module_id, sessionId, training.references, (tracks: Array<boolean>) => {
                                for (const track of tracks) {
                                    if (track) {
                                        listTrainingsSession.push(training);
                                        break;
                                    }
                                }

                                onResolve(listTrainingsSession)
                            });
                        }
                        else if (training.type === 'SpecificSession') {
                            for (let uid of training.references) {
                                if (uid === sessionId) {
                                    listTrainingsSession.push(training);
                                    break;
                                }
                            }

                            onResolve(listTrainingsSession)
                        }
                        else if (training.type === 'SpecificGroup') {
                            await this.listgroupsOfAttendee(eventId, userId, (listGroupsAttendee) => {
                                const references: any = training.references
                                let findGroup = false;
                                for (let uidGroup of references) {
                                    for (const uidGroupUser of listGroupsAttendee) {
                                        //if the user is part of the group
                                        if (uidGroup == uidGroupUser && findGroup == false) {
                                            listTrainingsSession.push(training);
                                            findGroup = true;
                                            break;
                                        }
                                    }

                                }

                                // if (cont == listTrainings.length - 1) {
                                onResolve(listTrainingsSession)
                                // }

                                // cont++;
                            })
                        } else if (training.type === 'Pattern') {
                            // if (cont == listTrainings.length - 1) {
                            onResolve(listTrainingsSession)
                            // }

                            // cont++;
                        }
                    }
                } else {
                    onResolve(listTrainingsSession);
                }
            })
    }

    getTraining(moduleId: string, trainingId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let refTraining = db.collection('modules').doc(moduleId).collection('trainings').doc(trainingId);

        refTraining.get()
            .then((data) => {
                let training: Training = this.instantiateTraining(data.data());

                this.getQuestions(moduleId, trainingId, (questions) => {
                    training.questions = questions;
                    onResolve(training)
                })
            })
    }

    getQuestions(moduleId: string, trainingId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let listQuestions: Array<Question> = [];

        let refQuestions = db.collection('modules').doc(moduleId).collection('trainings').doc(trainingId).collection('questions')
            .orderBy('createdAt', 'asc');;

        refQuestions
            .get()
            .then((data) => {
                data.forEach(element => {
                    let question = this.instantiateQuestion(element.data());
                    this.getAnswers(moduleId, trainingId, question.uid, (answers) => {
                        question.answers = answers;
                        listQuestions.push(question);
                        onResolve(listQuestions)
                    })

                });
            })
    }

    getTotalQuestions(moduleId: string, trainingId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestions = db.collection('modules').doc(moduleId).collection('trainings').doc(trainingId).collection('questions');

        refQuestions
            .get()
            .then((data) => {
                onResolve(data.size);
            })
    }

    getQuestionsAsync(moduleId: string, trainingId: string) {
        return new Promise((resolve, reject) => {
            let db = this.aFirestore.firestore;

            let listQuestions: Array<Question> = [];

            let refQuestions = db.collection('modules').doc(moduleId).collection('trainings').doc(trainingId).collection('questions')
                .orderBy('createdAt', 'asc');;

            refQuestions
                .get()
                .then((data) => {
                    data.forEach(element => {
                        let question = this.instantiateQuestion(element.data());
                        this.getAnswers(moduleId, trainingId, question.uid, (answers) => {
                            question.answers = answers;
                            listQuestions.push(question);
                            resolve(listQuestions)
                        })

                    });
                })
        })
    }

    getAnswers(moduleId: string, trainingId: string, questionId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestions = db.
            collection('modules')
            .doc(moduleId)
            .collection('trainings')
            .doc(trainingId)
            .collection('questions')
            .doc(questionId)
            .collection('answers')
            .orderBy('createdAt', 'asc');;

        refQuestions
            .get()
            .then((data) => {
                let listAnswers: Array<Answer> = [];
                data.forEach(element => {
                    let answer: Answer = this.instantiateAnswer(element.data());
                    listAnswers.push(answer);
                });

                onResolve(listAnswers);
            })
    }

    checkSpecificTrackTraining(trackModuleId, sessionId, tracksIds, onResolve) {
        let db = this.aFirestore.firestore;

        let list = [];
        for (let trackId of tracksIds) {
            this.checkTrackTraining(trackModuleId, sessionId, trackId, (result: boolean) => {
                list.push(result);

                if (list.length == tracksIds.length) {
                    onResolve(list);
                }
            })
        }
    }

    // VERIFICA SE A TRACK EXISTE NA SESSÃO
    checkTrackTraining(moduleId, sessionId, trackId, onResolve) {
        let db = this.aFirestore.firestore;
        db
            .collection('modules')
            .doc(moduleId)
            .collection('tracks')
            .doc(trackId)
            .collection('sessions')
            .doc(sessionId).get()
            .then((data) => {
                if (data.exists) {
                    onResolve(true);
                } else {
                    onResolve(false);
                }
            })
    }

    // VERIFICA SE A SESSÃO ESPECIFICA CORRESPONDE AO FEEDBACK
    // sessionOfTraining(eventId, trainingId, onResolve) {
    //     this.db.ref(this.SURVEY_CHILD).child(eventId).child(trainingId).child('references').once('value', (snapshot) => {
    //         const data = snapshot.val();
    //         let sessionIds = [];

    //         for (const uid in data) {
    //             sessionIds.push(uid);
    //         }
    //         onResolve(sessionIds);
    //     });
    // }

    listgroupsOfAttendee(eventId: string, attendeeId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).get()
            .then((data) => {
                let attendee = data.data();
                let groups = attendee['groups'];
                let idsGroups: Array<String> = [];

                if (groups !== null && groups !== undefined) {
                    for (let uid in groups) {
                        idsGroups.push(uid);
                    }
                }

                onResolve(idsGroups);
            })
    }


    createResult(moduleId: string, userId: string, trainingId: string, IdQuestion: string, type: string, answer: any, timestamp: number, verifyQuestion: boolean, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('trainings')
            .doc(trainingId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        
        if(userId !== null && userId !== undefined && userId !== '') {
            this.getResponseUserQuestion(moduleId, trainingId, IdQuestion, userId, (responses) => {
                this.deletePrevAnswersSelectOption(moduleId, trainingId, IdQuestion, userId, (result) => {
                    if (result == true) {
                        refQuestionsResult.doc(userId).set({
                            user: userId,
                            answer: answer,
                            timestamp: timestamp,
                            question: IdQuestion,
                            verifyQuestion: verifyQuestion
                        })
                            .then(() => {
                                if (responses !== null && responses !== undefined) {
                                    onResolve(false);
                                } else {
                                    onResolve(true);
                                }
                            })
                    }
                })
            })
        } else {
            let document = refQuestionsResult.doc();
            userId = document.id;

            refQuestionsResult.doc(userId).set({
                user: userId,
                answer: answer,
                timestamp: timestamp,
                question: IdQuestion,
                verifyQuestion: verifyQuestion
            })
            .then(() => {
                onResolve(true);
            })
        }
    }

    getResponseUsers(moduleId, trainingId, userId, onResolve) {
        this.getQuestions(moduleId, trainingId, (questions) => {
            let listResults = {};
            for (let i = 0; i < questions.length; i++) {
                this.getResponseUserQuestion(moduleId, trainingId, questions[i].uid, userId, (response) => {
                    if (response != null) {
                        listResults[response.question] = response;
                    }

                    if (i == questions.length - 1) {
                        onResolve(listResults);
                    }
                })
            }
        })
    }

    getResponseUserQuestion(moduleId, trainingId, questionId, userId, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db
            .collection('modules')
            .doc(moduleId)
            .collection('trainings')
            .doc(trainingId)
            .collection('questions')
            .doc(questionId)
            .collection('result')
            .doc(userId);

        ref.get()
            .then((data) => {
                if (data.exists) {
                    let result = data.data();
                    onResolve(result)
                } else {
                    onResolve(null);
                }
            })
    }

    deletePrevAnswersSelectOption(moduleId, trainingId, IdQuestion, userId, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('trainings')
            .doc(trainingId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        refQuestionsResult.doc(userId).delete().then(() => {
            onResolve(true)
        }).catch(() => {
            onResolve(false);
        })
    }

    instantiateTraining(data) {
        let training = new Training();
        training.title = data.title;
        training.type = data.type;
        training.uid = data.uid;
        training.visibility = data.visibility;
        training.change_answer = data.change_answer;
        training.view_answered = data.view_answered;
        training.icon = data.icon;
        training.iconFamily = data.iconFamily;

        data.active_timer != undefined ? training.active_timer = data.active_timer : training.active_timer = false;
        data.timer_questions != undefined ? training.timer_questions = data.timer_questions : training.timer_questions = null;
        data.max_responses != undefined ? training.max_responses = data.max_responses : training.max_responses = null;
        data.module_id != undefined ? training.module_id = data.module_id : training.module_id = null;
        data.references != undefined ? training.references = data.references : training.references = null;

        return training;  
    }

    instantiateQuestion(data) {
        console.log(data)
        let question = new Question();

        question.uid = data.uid;
        question.type = data.type;
        question.title = data.title;
        data.infobooth != undefined ? question.infobooth = data.infobooth : question.infobooth = new NameModule('', '', '', '', '');
        data.explanation != undefined ? question.explanation = data.explanation : question.explanation = new NameModule('', '', '', '', '');
        data.image != undefined ? question.image = data.image : question.image = null;
        data.maxResponses != undefined ? question.maxResponses = data.maxResponses : question.maxResponses = null;
        data.graphic != undefined ? question.graphic = data.graphic : question.graphic = null;
        data.visibility != undefined ? question.visibility = data.visibility : question.visibility = true;

        return question;
    }

    instantiateAnswer(data) {
        let answer = new Answer();
        answer.uid = data.uid;
        answer.answer = data.answer;
        data.weight != undefined ? answer.weight = data.weight : answer.weight = null;
        answer.correct = data.correct;

        return answer;
    }

    setUserTrainingAnswered(eventId: string, attendeeId: string, surveyId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId);
        ref.update({ answeredTrainings: firestore.FieldValue.arrayUnion(surveyId) })
        .then(() => {
            onResolve(true);
        });
    }

    // generateResult1000() {
    //     let moduleId = '1';
    //     let trainingId = '48zpZRwbMJPaQg5BLNqz';
    //     let IdQuestion = 'TV5PKCg91ykANmpC1hi1';
    //     let answer = 'Olá, Mundo. Como vai você?';
    //     let timestamp = Date.now() / 1000 | 0;


    //     for (let i = 0; i < 1000; i++) {
    //         let userId = this.textoAleatorio(20);
    //         let db = this.aFirestore.firestore;

    //         let refQuestionsResult = db
    //             .collection('modules')
    //             .doc(moduleId)
    //             .collection('trainings')
    //             .doc(trainingId)
    //             .collection('questions')
    //             .doc(IdQuestion)
    //             .collection('result');

    //         refQuestionsResult.doc(userId).set({
    //             user: userId,
    //             answer: answer,
    //             timestamp: timestamp
    //         }).then(() => {
    //         }).catch((error) => {
    //         })
    //     }
    // }

    // testeget() {
    //     let moduleId = '1';
    //     let trainingId = '48zpZRwbMJPaQg5BLNqz';
    //     let IdQuestion = 'TV5PKCg91ykANmpC1hi1';
    //     let answer = 'Olá, Mundo. Como vai você?';
    //     let timestamp = Date.now() / 1000 | 0;

    //     let userId = this.textoAleatorio(20);
    //     let db = this.aFirestore.firestore;

    //     let refQuestionsResult = db
    //         .collection('modules')
    //         .doc(moduleId)
    //         .collection('trainings')
    //         .doc(trainingId)
    //         .collection('questions')
    //         .doc(IdQuestion)
    //         .collection('result');

    //     refQuestionsResult.get()
    //         .then((data) => {
    //             let array = [];
    //             data.forEach(element => {
    //                 array.push(element.data());
    //             });
    //         })
    // }

    // textoAleatorio(tamanho) {
    //     var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    //     var aleatorio = '';
    //     for (var i = 0; i < tamanho; i++) {
    //         var rnum = Math.floor(Math.random() * letras.length);
    //         aleatorio += letras.substring(rnum, rnum + 1);
    //     }
    //     return aleatorio;
    // }



}
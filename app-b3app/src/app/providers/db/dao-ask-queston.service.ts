import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UUID } from 'angular2-uuid';
import { TypeModule } from 'src/app/enums/type-module';
import { AskQuestion } from 'src/app/models/ask-question';
import { map, switchMap, take } from 'rxjs/operators';
import { of, combineLatest } from 'rxjs';
import * as _ from 'lodash';
import { UtilityService } from 'src/app/shared/services';

@Injectable({
    providedIn: 'root'
})
export class DaoAskQuestionService {

    constructor(
        private aFirestore: AngularFirestore,
        private SUtility: UtilityService
    ) { }

    /**
     * Get question module
     * @param moduleId 
     */
    getQuestionsModule(moduleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .get().pipe(
                take(1),
                map((doc) => {
                    return (doc.data())
                })
            ))
    }

    /**
     * Get questions for modules
     * @param moduleId 
     */
    getAskQuestions(moduleId) {
        // let listItems = [];
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('items', (ref) => ref.orderBy('order', 'asc'))
            .snapshotChanges().pipe(
                switchMap((docs) => {
                    let listItems = [];
                    for (let i = 0; i < docs.length; i++) {
                        let item = docs[i].payload.doc.data();
                        if (item && item.visibility) {
                            listItems.push(item);
                        }
                    }
                    return (of(listItems))
                })
            ))
    }

    /**
     * Get questions for session
     * @param moduleId 
     * @param sessionId 
     * @param userId 
     * @param onResolve 
     */
    getQuestions(moduleId, sessionId, userId) {
        let listQuestions = [];
        let questions = [];

        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions',
                (ref) => ref.where("visibility", "==", true).orderBy('createdAt'))
            .snapshotChanges().pipe(
                switchMap((docs) => {
                    if (docs && docs.length > 0) {
                        listQuestions = [];
                        questions = [];
                        let obs = [];
                        for (let i = 0; i < docs.length; i++) {
                            let question = docs[i].payload.doc.data();
                            questions.push(question);

                            obs.push(this.aFirestore
                                .collection('modules')
                                .doc(moduleId)
                                .collection('sessions')
                                .doc(sessionId)
                                .collection('questions')
                                .doc(question.uid)
                                .collection('votes').snapshotChanges().pipe(
                                    map((docs) => {
                                        return (docs.map((doc) => {
                                            return (doc.payload.doc.data());
                                        }))
                                    })
                                ));
                        }
                        return (combineLatest(obs));
                    } else {
                        return (of([]));
                    }
                }),
                switchMap((votesArray: any[]) => {
                    listQuestions = _.cloneDeep(listQuestions);
                    for (let i = 0; i < votesArray.length; i++) {
                        let votes = _.cloneDeep(votesArray[i]);
                        let question = _.cloneDeep(questions[i]);
                        if (votes && question) {
                            question.totalVotes = votes.length;

                            votes.forEach((vote) => {
                                if (vote.uid == userId) {
                                    question.userLiked = true;
                                }
                            });

                            let index = this.SUtility.checkIndexExists(listQuestions, question);

                            if (index >= 0) {
                                listQuestions[index] = question;
                            } else {
                                listQuestions.push(question);
                            }
                        } else {
                            question.totalVotes = 0;

                            let index = this.SUtility.checkIndexExists(listQuestions, question);

                            if (index >= 0) {
                                listQuestions[index] = question;
                            } else {
                                listQuestions.push(question);
                            }
                        }
                    }
                    return (of(listQuestions));
                })
            ))
    }

    /**
     * Get asked item for question
     * @param moduleId 
     * @param itemId 
     * @param onResolve 
     */
    getAskItem(moduleId, itemId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.payload.data());
                })
            ))
    }

    /**
     * Get general questions
     * @param moduleId 
     * @param itemId 
     * @param userId 
     */
    getQuestionsGeneral(moduleId, itemId, userId) {
        let listQuestions = [];
        let questions = [];

        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .collection('questions', (ref) => ref.where("visibility", "==", true).orderBy('createdAt'))
            .snapshotChanges().pipe(
                switchMap((docs) => {
                    questions = [];
                    let obs = [];
                    if (docs && docs.length > 0) {
                        for (let i = 0; i < docs.length; i++) {
                            let question = docs[i].payload.doc.data();
                            questions.push(question);

                            obs.push(this.aFirestore
                                .collection('modules')
                                .doc(moduleId)
                                .collection('items')
                                .doc(itemId)
                                .collection('questions')
                                .doc(question.uid)
                                .collection('votes').snapshotChanges().pipe(
                                    map((docs) => {
                                        return (docs.map((doc) => {
                                            return (doc.payload.doc.data());
                                        }))
                                    })
                                ));
                        }
                        return (combineLatest(obs));
                    } else {
                        return (of([]));
                    }
                }),
                switchMap((votesArray: any[]) => {
                    listQuestions = _.cloneDeep(listQuestions);
                    for (let i = 0; i < votesArray.length; i++) {
                        let votes = _.cloneDeep(votesArray[i]);
                        let question = _.cloneDeep(questions[i]);
                        if (votes && question) {
                            question.totalVotes = votes.length;

                            votes.forEach((vote) => {
                                if (vote.uid == userId) {
                                    question.userLiked = true;
                                }
                            });

                            let index = this.SUtility.checkIndexExists(listQuestions, question);

                            if (index >= 0) {
                                listQuestions[index] = question;
                            } else {
                                listQuestions.push(question);
                            }
                        } else {
                            question.totalVotes = 0;

                            let index = this.SUtility.checkIndexExists(listQuestions, question);

                            if (index >= 0) {
                                listQuestions[index] = question;
                            } else {
                                listQuestions.push(question);
                            }
                        }
                    }
                    return (of(listQuestions));
                })
            ))
    }

    /**
     * Create a question for session
     * @param eventId 
     * @param moduleId 
     * @param sessionId 
     * @param question 
     * @param onResolve 
     */
    async createQuestion(eventId: string, moduleId: string, sessionId: string, question: AskQuestion) {
        let docEventId = this.aFirestore.createId();
        question.uid = docEventId;
        question = Object.assign({}, question);

        const docModule = this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(docEventId);

        try {
            await this.aFirestore
                .collection('events')
                .doc(eventId)
                .collection('sessions')
                .doc(sessionId)
                .collection('questions')
                .doc(docEventId).set(question);
            return (docModule.set(question));
        } catch (error) {
            throw {
                status: "error"
            }
        }
    }

    /**
     * Create a general question
     * @param moduleId 
     * @param itemId 
     * @param question 
     * @param onResolve 
     */
    createQuestionGeneral(moduleId: string, itemId: string, question: AskQuestion) {
        let docId = this.aFirestore.createId();
        question.uid = docId;
        question = Object.assign({}, question);
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .collection('questions')
            .doc(docId).set(question));
    }

    /**
     * Add or remove vote on question session
     * @param eventId 
     * @param moduleId 
     * @param sessionId 
     * @param userId 
     * @param questionId 
     * @param onResolve 
     */
    addOrRemoveVote(eventId, moduleId, sessionId, userId, questionId) {
        const docEvent = this.aFirestore
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(questionId)
            .collection('votes')
            .doc(userId);

        const docModule = this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('questions')
            .doc(questionId)
            .collection('votes')
            .doc(userId);

        return (docModule.get().pipe(
            switchMap(async (doc) => {
                let batch = this.aFirestore.firestore.batch();
                if (!doc.exists) {
                    batch.set(docModule.ref, { uid: userId });
                    batch.set(docEvent.ref, { uid: userId });

                    await batch.commit();
                    return (of(true));
                } else {
                    batch.delete(docModule.ref);
                    batch.delete(docEvent.ref);

                    await batch.commit();
                    return (of(false));
                }
            })
        ))
    }

    /**
     * Add or remove a vote on a question
     * @param moduleId 
     * @param itemId 
     * @param userId 
     * @param questionId 
     * @param onResolve 
     */
    addOrRemoveVoteGeneral(moduleId, itemId, userId, questionId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('items')
            .doc(itemId)
            .collection('questions')
            .doc(questionId)
            .collection('votes')
            .doc(userId)
            .get().pipe(
                take(1),
                switchMap((doc) => {
                    if (!doc.exists) {
                        doc.ref.set({ uid: userId });
                        return (of(true));
                    } else {
                        doc.ref.delete();
                        return (of(false));
                    }
                })
            ))
    }

    /**
     * Get questions module for event
     * @param eventId 
     */
    getAskQuestionModule(eventId: string) {
        return (this.aFirestore
            .collection('events')
            .doc(eventId)
            .collection('modules', (ref) => ref.where('type', "==", TypeModule.ASK_QUESTION))
            .get().pipe(
                take(1),
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        return (doc.data());
                    }))
                })
            ))
    }
}

import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { GlobalService } from 'src/app/shared/services';
import { Session } from '../../models/ceu-session';
import { TypeModule } from '../../models/type-module'
import { LuxonService } from '../luxon/luxon.service';

@Injectable({
    providedIn: 'root'
})
export class DaoPersonalAgendaService {

    constructor(
        private aFirestore: AngularFirestore,
        public global: GlobalService,
        private luxon: LuxonService,
    ) {
        AngularFirestoreModule.enablePersistence();
    }

    getModule(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .onSnapshot((snapshot) => {
                onResolve(snapshot.data());
            });
    }

    getSession(eventId: string, sessionId: string) {
        return new Promise((resolve) => {
            let db = this.aFirestore.firestore;
            const ref = db.collection('events').doc(eventId).collection('sessions').doc(sessionId);

            ref
                .get()
                .then((value) => {
                    const session = value.data()
                    resolve(session);
                }).catch((err) => {
                    resolve(false);
                })
        })
    }


    // loads the first 100 sessions of the participant's agenda.
    getFirstPageSessions(attendeeId: string, moduleId: string, onResolve) {
        this.aFirestore.collection('modules').doc(moduleId).collection('attendees').doc(attendeeId).collection<Session>('sessions', ref => ref.orderBy('startTime').limit(100))
            .valueChanges().subscribe((snapshot) => {
                if (snapshot.length <= 0) {
                    onResolve(false)
                } else {
                    const list = [];
                    let nextPage = snapshot[snapshot.length - 1];

                    snapshot.forEach((session) => {
                        list.push(session)
                    })

                    onResolve({
                        sessions: list,
                        nextPage: nextPage
                    });
                }
            })
    }

    // get plus 100 module sessions using the start time as a filter.
    getNextPageSessions(attendeeId, moduleIdPersonalId, lastStartTime, onResolve) {
        this.aFirestore.collection('modules').doc(moduleIdPersonalId).collection('attendees').doc(attendeeId).collection<Session>('sessions', ref => ref.orderBy('startTime').startAfter(lastStartTime).limit(100))
            .valueChanges().subscribe((snapshot) => {
                const list = [];
                let nextPage = snapshot[snapshot.length - 1];

                snapshot.forEach((session) => {
                    list.push(session)
                })

                onResolve({
                    sessions: list,
                    nextPage: nextPage
                });
            })
    }

    // loads all sessions from the personal calendar.
    getSessionsModule(attendeeId: string, modulePersonalAgendaId: string, onResolve) {
        let db = this.aFirestore.firestore;
        const ref = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').orderBy('startTime')

        ref.onSnapshot((snapshot) => {
            const list = []
            snapshot.forEach((session) => {
                list.push(session.data())
            })
            onResolve(list);
        })
    }

    // receives the event uid and returns the event's agenda module
    eventCalendarModule(eventId: string) {
        return new Promise((resolve: any) => {
            let db = this.aFirestore.firestore;

            const ref = db.collection('events').doc(eventId).collection('modules');

            ref.where('type', '==', TypeModule.PERSONALSCHEDULE).get().then((snapshot) => {
                snapshot.forEach((childSnapshot) => {
                    const module = childSnapshot.data()
                    resolve(module)
                })
            })

        })
    }

    // returns the dates of all sessions
    sessionDates(moduleId: string, onResolve) {
        const attendeeId = this.global.userId

        let db = this.aFirestore.firestore;
        const ref = db.collection('modules').doc(moduleId).collection('attendees').doc(attendeeId).collection('sessions').orderBy('date')

        ref.onSnapshot((snapshot) => {
            let dates = [];

            if (snapshot.docs.length > 0) {
                snapshot.forEach((element) => {
                    dates.push(element.data().date)
                })

                onResolve(dates)
            } else {
                onResolve(false)
            }
        })
    }




    // load participant's calendar filtered by date
    getFirstPageSessionsFilterByDate(moduleId: string, date: number, onResolve) {
        const auxDate = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(date))
        const attendeeId = this.global.userId

        let db = this.aFirestore.firestore
        const ref = db.collection('modules').doc(moduleId).collection('attendees').doc(attendeeId).collection('sessions').orderBy('startTime').startAt(date).limit(200)

        ref.onSnapshot((snapshot) => {
            const list = []
            let nextPage = null //save the last element

            snapshot.forEach((element) => {
                const session = element.data()
                const auxDateElement = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(session.date))

                if (auxDate === auxDateElement) {
                    list.push(session)
                    nextPage = session
                }
            })

            onResolve({
                sessions: list,
                nextPage: nextPage
            });

        })

    }


    getNextPageSessionsFilterByDate(moduleId, date, nextPage, onResolve) {
        const auxDate = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(date))
        const array1 = auxDate.split('/')

        const day = array1[0]
        const month = array1[1]
        const year = array1[2]

        const array2 = nextPage.startTime.split(':')

        const hour = array2[0]
        const minute = array2[1]

        let db = this.aFirestore.firestore;
        const ref = db.collection('modules').doc(moduleId).collection('sessions').orderBy('startTime').startAfter(this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, hour, minute, '00'))).limit(200);

        ref.get().then((snapshot) => {
            const list = []
            let nextPage = null //save the last element
            snapshot.forEach((element) => {
                const session = element.data()
                const auxDateElement = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(session.date))

                if (auxDate === auxDateElement) {
                    list.push(session)
                    nextPage = session
                }
            })

            if (list.length > 0) {
                onResolve({
                    sessions: list,
                    nextPage: nextPage
                });
            } else {
                onResolve(false)
            }
        }).catch((err) => {
            onResolve(false)
        });
    }

    // verifies that the session is part of the participant's personal agenda.
    // true -> yes
    // false -> no
    async checkFollow(session: Session, onResolve) {
        let db = this.aFirestore.firestore;
        const attendeeId = this.global.userId
        const sessionId = session.uid
        const eventId = session.eventId

        const modulePersonalAgenda: any = await this.eventCalendarModule(eventId)
        const modulePersonalAgendaId = modulePersonalAgenda.uid

        const ref = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)

        ref.onSnapshot((snapshot) => {
            onResolve(snapshot.exists)
        })
    }


    // add session to calendar
    addSession(session: Session) {
        let batch = this.aFirestore.firestore.batch()
        let db = this.aFirestore.firestore

        const sessionId = session.uid
        const moduleScheduleId = session.moduleId
        const eventId = session.eventId

        // picks up the participant's uid and module information.
        const attendeeId = this.global.userId
        const moduleAttendeeId = this.global.userModuleId

        // relates the session to the participant(bidirectional)
        const ref1 = db.collection('events').doc(eventId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId)
        const ref2 = db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId)

        const ref3 = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)
        const ref4 = db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)

        const objSession = {
            uid: sessionId,
            moduleId: moduleScheduleId,
            eventId: eventId
        }

        const objAttendee = {
            uid: attendeeId,
            email: this.global.email,
            name: this.global.displayName,
            moduleId: moduleAttendeeId,
            eventId: eventId
        }


        // module personal agenda
        const pos = this.global.modulesEvent.map(function (e) { return e.type; }).indexOf(TypeModule.PERSONALSCHEDULE);
        const modulePersonalAgendaId = this.global.modulesEvent[pos].uid

        // add info user in path agenda personal
        const ref5 = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId)
        const ref6 = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)

        batch.set(ref1, objAttendee)
        batch.set(ref2, objAttendee)
        batch.set(ref3, objSession)
        batch.set(ref4, objSession)
        batch.set(ref5, objAttendee)
        batch.set(ref6, session)

        return batch.commit().then(function () {

        }).catch((err) => {
            console.error(err)
        })
    }


    // delete session to calendar
    async deleteSession(eventId: string, moduleScheduleId: string, sessionId: string) {
        let batch = this.aFirestore.firestore.batch()
        let db = this.aFirestore.firestore

        // picks up the participant's uid and module information.
        const attendeeId = this.global.userId
        const moduleAttendeeId = this.global.userModuleId

        // relates the session to the participant(bidirectional)
        const ref1 = db.collection('events').doc(eventId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId)
        const ref2 = db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).collection('attendees').doc(attendeeId)

        const ref3 = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)
        const ref4 = db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)



        // module personal agenda
        const pos = this.global.modulesEvent.map(function (e) { return e.type; }).indexOf(TypeModule.PERSONALSCHEDULE);
        const modulePersonalAgendaId = this.global.modulesEvent[pos].uid

        const ref5 = db.collection('modules').doc(modulePersonalAgendaId).collection('attendees').doc(attendeeId).collection('sessions').doc(sessionId)

        // remove docs
        batch.delete(ref1)
        batch.delete(ref2)
        batch.delete(ref3)
        batch.delete(ref4)
        batch.delete(ref5)

        return batch.commit().then(function () {

        }).catch((err) => {
            console.error(err)
        })
    }
}
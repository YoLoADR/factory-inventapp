import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { sessionFeedback } from 'src/app/models/sessionFeedback';
import { sessionFeedbackQuestion } from 'src/app/models/sessionFeedbackQuestion';
import { sessionFeedbackAnswer } from 'src/app/models/sessionFeedbackAnswer';
import { TypeModule } from 'src/app/models/type-module';
import { map, switchMap, take } from 'rxjs/operators';
import { of, combineLatest } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DaoSessionFeedbackService {

    constructor(private aFirestore: AngularFirestore) {
        // AngularFirestoreModule.enablePersistence();  
    }

    getFeedbackModule(eventId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('modules');

        ref
            .where('type', '==', TypeModule.SESSION_FEEDBACK)
            .get()
            .then((snapshot) => {
                let auxModule = [];
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        auxModule.push(element.data());
                    });
                }
                onResolve(auxModule);
            })
            .catch((e) => {
                onResolve(e);
            })
    }

    /**
     * Get feedbacks
     * @param moduleId 
     */
    getFeedbacks(moduleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('session-feedbacks', (ref) => ref.orderBy('order', 'asc'))
            .snapshotChanges().pipe(
                switchMap((docs) => {
                    let listFeedbacks: sessionFeedback[] = [];
                    if (docs && docs.length > 0) {
                        for (let i = 0; i < docs.length; i++) {
                            let feedback = this.instantiateFeedback(docs[i].payload.doc.data());
                            if (feedback.visibility) {
                                feedback.questions.sort(function (a, b) {
                                    if (a.createdAt < b.createdAt) {
                                        return -1;
                                    }
                                    if (a.createdAt > b.createdAt) {
                                        return 1;
                                    }
                                    // a must be equal to b
                                    return 0;
                                });

                                listFeedbacks.push(feedback);
                            }
                        }
                        return (of(listFeedbacks));
                    } else {
                        return (of([]));
                    }
                })
            ))
    }

    getFeedbacksSession(eventId: string, moduleId: string, scheduleModuleId: string, sessionId: string, userId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let listFeedbacks: Array<sessionFeedback> = [];
        let listFeedbacksSession: Array<sessionFeedback> = [];

        let refFeedbacks = db.collection('modules').doc(moduleId).collection('session-feedbacks').orderBy('order', 'asc');

        refFeedbacks.get()
            .then(async (data) => {
                data.forEach(element => {
                    let feedback = this.instantiateFeedback(element.data());
                    if (feedback.visibility) {
                        listFeedbacks.push(feedback);
                    }
                });

                let cont = 0;
                for (let feedback of listFeedbacks) {

                    if (feedback.type === 'AllSessions') {
                        listFeedbacksSession.push(feedback);
                        onResolve(listFeedbacksSession)
                    }
                    else if (feedback.type === 'ScheduleModule') {
                        if (scheduleModuleId === feedback.module_id) {
                            listFeedbacksSession.push(feedback);
                        }

                        onResolve(listFeedbacksSession)
                    }
                    else if (feedback.type === 'SessionTrack') {
                        // case this session be inside a track with permission, display feedback
                        await this.checkSpecificTrackFeedback(feedback.module_id, sessionId, feedback.references, (tracks: Array<boolean>) => {
                            for (const track of tracks) {
                                if (track) {
                                    listFeedbacksSession.push(feedback);
                                    break;
                                }
                            }

                            onResolve(listFeedbacksSession)

                        });
                    }
                    else if (feedback.type === 'SpecificSession') {
                        for (let uid of feedback.references) {
                            if (uid === sessionId) {
                                listFeedbacksSession.push(feedback);
                                break;
                            }
                        }

                        onResolve(listFeedbacksSession)

                    }
                    else if (feedback.type === 'SpecificGroup') {
                        await this.listgroupsOfAttendee(eventId, userId, (listGroupsAttendee) => {
                            const references: any = feedback.references
                            let findGroup = false;
                            for (let uidGroup of references) {
                                for (const uidGroupUser of listGroupsAttendee) {
                                    //if the user is part of the group
                                    if (uidGroup == uidGroupUser && findGroup == false) {
                                        listFeedbacksSession.push(feedback);
                                        findGroup = true;
                                        break;
                                    }
                                }

                            }

                            onResolve(listFeedbacksSession)

                        })
                    } else if (feedback.type === 'Pattern') {
                        onResolve(listFeedbacksSession)
                    }
                }
            })
    }

    getFeedback(moduleId: string, feedbackId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let refFeedback = db.collection('modules').doc(moduleId).collection('session-feedbacks').doc(feedbackId);

        refFeedback.get()
            .then((data) => {
                let feedback: sessionFeedback = this.instantiateFeedback(data.data());

                this.getQuestions(moduleId, feedbackId, (questions) => {
                    feedback.questions = questions;
                    onResolve(feedback)
                })
            })
    }

    getQuestions(moduleId: string, feedbackId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let listQuestions: Array<sessionFeedbackQuestion> = [];

        let refQuestions = db.collection('modules').doc(moduleId).collection('session-feedbacks').doc(feedbackId).collection('questions')
            .orderBy('createdAt', 'asc');;

        refQuestions.get()
            .then((data) => {
                data.forEach(element => {
                    let question = this.instantiateQuestion(element.data());
                    this.getAnswers(moduleId, feedbackId, question.uid).subscribe((answers) => {
                        question.answers = answers;
                        listQuestions.push(question);
                        onResolve(listQuestions)
                    })

                });
            })
    }

    /**
     * Get answers
     * @param moduleId 
     * @param feedbackId 
     * @param questionId 
     */
    getAnswers(moduleId: string, feedbackId: string, questionId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('session-feedbacks')
            .doc(feedbackId)
            .collection('questions')
            .doc(questionId)
            .collection('answers', (ref) => ref.orderBy('createdAt', 'asc'))
            .get().pipe(
                take(1),
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        let answer: sessionFeedbackAnswer = this.instantiateAnswer(doc.data());
                        return (answer);
                    }))
                })
            ))
    }

    /**
     * Check sepecific track feedback
     * @param trackModuleId 
     * @param sessionId 
     * @param tracksIds 
     * @param onResolve 
     */
    checkSpecificTrackFeedback(trackModuleId, sessionId, tracksIds, onResolve) {
        let list = [];
        for (let trackId of tracksIds) {
            this.checkTrackFeedback(trackModuleId, sessionId, trackId).subscribe((result: boolean) => {
                list.push(result);

                if (list.length == tracksIds.length) {
                    onResolve(list);
                }
            })
        }
        return (combineLatest())
    }

    /**
     * Check track feedback
     * @param moduleId 
     * @param sessionId 
     * @param trackId 
     */
    checkTrackFeedback(moduleId, sessionId, trackId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('tracks')
            .doc(trackId)
            .collection('sessions')
            .doc(sessionId)
            .get().pipe(
                take(1),
                switchMap((doc) => {
                    if (doc.exists) {
                        return (of(true));
                    } else {
                        return (of(false));
                    }
                })
            ))
    }

    // VERIFICA SE A SESSÃO ESPECIFICA CORRESPONDE AO FEEDBACK
    // sessionOfFeedback(eventId, quizId, onResolve) {
    //     this.db.ref(this.SURVEY_CHILD).child(eventId).child(quizId).child('references').once('value', (snapshot) => {
    //         const data = snapshot.val();
    //         let sessionIds = [];

    //         for (const uid in data) {
    //             sessionIds.push(uid);
    //         }
    //         onResolve(sessionIds);
    //     });
    // }

    listgroupsOfAttendee(eventId: string, attendeeId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).get()
            .then((data) => {
                let attendee = data.data();
                let groups = attendee['groups'];
                let idsGroups: Array<String> = [];

                if (groups !== null && groups !== undefined) {
                    for (let uid in groups) {
                        idsGroups.push(uid);
                    }
                }

                onResolve(idsGroups);
            })
    }

    /**
     * Create a result
     * @param moduleId 
     * @param sessionId 
     * @param userId 
     * @param feedbackId 
     * @param IdQuestion 
     * @param type 
     * @param answer 
     * @param timestamp 
     * @param onResolve 
     */
    createResult(moduleId: string, sessionId: string, userId: string, feedbackId: string, IdQuestion: string, type: string, answer: any, timestamp: number, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('session-feedbacks')
            .doc(feedbackId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result')

        if (type == "oneSelect" || type == "multipleSelect") {
            if (userId !== null && userId !== undefined && userId !== '') {
                this.getResponseUserQuestion(moduleId, sessionId, feedbackId, IdQuestion, userId).subscribe((responses) => {
                    this.deletePrevAnswersSelectOption(moduleId, feedbackId, IdQuestion, userId, sessionId, (result) => {
                        if (result == true) {
                            refQuestionsResult
                                .doc(userId)
                                .collection('sessions')
                                .doc(sessionId)
                                .set({
                                    user: userId,
                                    answer: answer,
                                    timestamp: timestamp,
                                    session: sessionId,
                                    question: IdQuestion
                                }).then(() => {
                                    refQuestionsResult.doc(userId).set({ uid: userId }).then(() => {
                                        if (responses === null || responses === undefined) {
                                            onResolve(true);
                                        } else {
                                            onResolve(false);
                                        }
                                    })
                                })
                        }
                    })
                })
            } else {
                let document = refQuestionsResult.doc();
                userId = document.id;

                refQuestionsResult
                    .doc(userId)
                    .collection('sessions')
                    .doc(sessionId)
                    .set({
                        user: userId,
                        answer: answer,
                        timestamp: timestamp,
                        session: sessionId,
                        question: IdQuestion
                    })
                    .then(() => {
                        refQuestionsResult.doc(userId).set({ uid: userId }).then(() => {
                            onResolve(true);
                        })
                    })
            }
        } else {
            if (userId !== null && userId !== undefined && userId !== '') {
                refQuestionsResult
                    .doc(userId)
                    .collection('sessions')
                    .doc(sessionId)
                    .set({
                        question: IdQuestion,
                        session: sessionId,
                        user: userId,
                        answer: answer,
                        timestamp: timestamp
                    }).then(() => {
                        refQuestionsResult.doc(userId).set({ uid: userId }).then(() => {
                            onResolve(true);
                        })
                    })
            } else {
                let document = refQuestionsResult.doc();
                userId = document.id;

                refQuestionsResult
                    .doc(userId)
                    .collection('sessions')
                    .doc(sessionId)
                    .set({
                        question: IdQuestion,
                        session: sessionId,
                        user: userId,
                        answer: answer,
                        timestamp: timestamp
                    }).then(() => {
                        refQuestionsResult.doc(userId).set({ uid: userId }).then(() => {
                            onResolve(true);
                        })
                    })
            }
        }
    }

    /**
     * Get response users
     * @param moduleId 
     * @param sessionId 
     * @param feedbackId 
     * @param userId 
     * @param onResolve 
     */
    getResponseUsers(moduleId, sessionId, feedbackId, userId, onResolve) {
        this.getQuestions(moduleId, feedbackId, (questions) => {
            let listResults = {};
            for (let i = 0; i < questions.length; i++) {
                this.getResponseUserQuestion(moduleId, sessionId, feedbackId, questions[i].uid, userId).subscribe((response) => {
                    if (response != null) {
                        listResults[response.question] = response;
                    }

                    if (i == questions.length - 1) {
                        onResolve(listResults);
                    }
                })
            }
        })
    }

    /**
     * Get response user question
     * @param moduleId 
     * @param sessionId 
     * @param feedbackId 
     * @param questionId 
     * @param userId 
     */
    getResponseUserQuestion(moduleId, sessionId, feedbackId, questionId, userId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('session-feedbacks')
            .doc(feedbackId)
            .collection('questions')
            .doc(questionId)
            .collection('result')
            .doc(userId)
            .collection('sessions')
            .doc(sessionId)
            .get().pipe(
                switchMap((doc) => {
                    if (doc.exists) {
                        return (of(doc.data()));
                    } else {
                        return (of(null));
                    }
                })
            ))
    }

    deletePrevAnswersSelectOption(moduleId, feedbackId, IdQuestion, userId, sessionId, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('session-feedbacks')
            .doc(feedbackId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result')

        refQuestionsResult.doc(userId).collection('sessions').doc(sessionId).delete().then(() => {
            onResolve(true)
        }).catch((e) => {
            onResolve(false);
        })
    }

    instantiateFeedback(data) {
        let feedback = new sessionFeedback();
        feedback.title = data.title;
        feedback.type = data.type;
        feedback.uid = data.uid;
        feedback.visibility = data.visibility;
        feedback.change_answer = data.change_answer;
        feedback.icon = data.icon;
        feedback.iconFamily = data.iconFamily;
        // data.max_responses != undefined ? feedback.max_responses = data.max_responses : feedback.max_responses = null;
        data.module_id != undefined ? feedback.module_id = data.module_id : feedback.module_id = null;
        data.references != undefined ? feedback.references = data.references : feedback.references = null;

        let questions: Array<sessionFeedbackQuestion> = [];

        for (let questionId in data.questions) {
            if (data.questions[questionId].type == 'oneSelect' || data.questions[questionId].type == 'multipleSelect') {
                let listAnswers = data.questions[questionId].answers;
                delete data.questions[questionId].answers;
                let question: sessionFeedbackQuestion = this.instantiateQuestion(data.questions[questionId]);

                for (let answerId in listAnswers) {
                    let answer: sessionFeedbackAnswer = this.instantiateAnswer(listAnswers[answerId])
                    question.answers.push(answer);
                }

                question.answers.sort(function (a, b) {
                    if (a.createdAt < b.createdAt) {
                        return -1;
                    }
                    if (a.createdAt > b.createdAt) {
                        return 1;
                    }
                    // a must be equal to b
                    return 0;
                });

                questions.push(question);

            } else {
                let question: sessionFeedbackQuestion = this.instantiateQuestion(data.questions[questionId]);
                questions.push(question);
            }
        }

        feedback.questions = questions;

        return feedback;
    }

    instantiateQuestion(data) {
        let question = new sessionFeedbackQuestion();

        question.uid = data.uid;
        question.type = data.type;
        question.title = data.title;
        data.infobooth != undefined ? question.infobooth = data.infobooth : question.infobooth = '';
        question.points = data.points;
        data.graphic != undefined ? question.graphic = data.graphic : question.graphic = null;
        question.createdAt = data.createdAt;
        data.visibility != undefined ? question.visibility = data.visibility : question.visibility = true;

        return question;
    }

    instantiateAnswer(data) {
        let answer = new sessionFeedbackAnswer();
        answer.uid = data.uid;
        answer.answer = data.answer;
        data.weight != undefined ? answer.weight = data.weight : answer.weight = null;
        answer.createdAt = data.createdAt;

        return answer;
    }


    // generateResult1000() {
    //     let moduleId = '1';
    //     let feedbackId = '48zpZRwbMJPaQg5BLNqz';
    //     let IdQuestion = 'TV5PKCg91ykANmpC1hi1';
    //     let answer = 'Olá, Mundo. Como vai você?';
    //     let timestamp = Date.now() / 1000 | 0;


    //     for(let i=0; i < 1000; i++) {
    //         let userId = this.textoAleatorio(20);
    //         let db = this.aFirestore.firestore;

    //         let refQuestionsResult = db
    //         .collection('modules')
    //         .doc(moduleId)
    //         .collection('session-feedbacks')
    //         .doc(feedbackId)
    //         .collection('questions')
    //         .doc(IdQuestion)
    //         .collection('result');

    //         refQuestionsResult.doc(userId).set({
    //             user: userId,
    //             answer: answer,
    //             timestamp: timestamp
    //         }).then(() => {   

    //         }).catch((error) => {

    //         })
    //     }
    // }

    // testeget() {
    //     let moduleId = '1';
    //     let feedbackId = '48zpZRwbMJPaQg5BLNqz';
    //     let IdQuestion = 'TV5PKCg91ykANmpC1hi1';
    //     let answer = 'Olá, Mundo. Como vai você?';
    //     let timestamp = Date.now() / 1000 | 0;

    //     let userId = this.textoAleatorio(20);
    //     let db = this.aFirestore.firestore;

    //     let refQuestionsResult = db
    //     .collection('modules')
    //     .doc(moduleId)
    //     .collection('session-feedbacks')
    //     .doc(feedbackId)
    //     .collection('questions')
    //     .doc(IdQuestion)
    //     .collection('result');

    //     refQuestionsResult.get()
    //     .then((data) => {
    //         let array = [];
    //         data.forEach(element => {
    //             array.push(element.data());
    //         });

    //     })
    // }

    // textoAleatorio(tamanho) {
    //     var letras = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    //     var aleatorio = '';
    //     for (var i = 0; i < tamanho; i++) {
    //         var rnum = Math.floor(Math.random() * letras.length);
    //         aleatorio += letras.substring(rnum, rnum + 1);
    //     }
    //     return aleatorio;
    // }



}
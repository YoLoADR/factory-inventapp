import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})

export class DaoCustomPageService {
  private db = this.aFirestore.firestore;


  constructor(
    private aFirestore: AngularFirestore
  ) {
    AngularFirestoreModule.enablePersistence();
  }

  getModule(moduleId: string, onResolve) {
    const ref = this.db.collection('modules').doc(moduleId);

    ref
      .onSnapshot((data) => {
        onResolve(data.data());
      });
  }

  getPages(moduleId: string, onResolve) {
    const ref = this.db.collection('modules').doc(moduleId).collection('pages').orderBy('order')

    ref.onSnapshot((snapshot) => {
      let list = []

      snapshot.forEach((childSnapshot) => {
        list.push(childSnapshot.data())
      })

      onResolve(list)
    })
  }


}

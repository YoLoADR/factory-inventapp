import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DaoWordCloudService {

  constructor(private aFirestore: AngularFirestore) {
    AngularFirestoreModule.enablePersistence();
  }


  getModule(moduleId, onResolve) {
    let db = this.aFirestore.firestore;

    db
    .collection('modules')
    .doc(moduleId)
    .onSnapshot((data) => {
    let module = data.data();
    onResolve(module);
    })
  }

  getClouds(moduleId, onResolve) {
    let db = this.aFirestore.firestore;

    let ref = db
    .collection('modules')
    .doc(moduleId)
    .collection('wordClouds')

    ref.onSnapshot((data) => {
        let list = [];

        data.forEach(element => {
            list.push(element.data());
        });

        onResolve(list);
    })
  }

  getCloud(moduleId, cloudId, onResolve) {
      let db = this.aFirestore.firestore;

      let ref = db
      .collection('modules')
      .doc(moduleId)
      .collection('wordClouds')
      .doc(cloudId);

      ref.get()
      .then((data) => {
        let cloud = data.data();
        onResolve(cloud);
      })
  }

  setUserWordCloud(moduleId, cloudId, userId, answer, onResolve) {
    let db = this.aFirestore.firestore;
    
    let ref = db
    .collection('modules')
    .doc(moduleId)
    .collection('wordClouds') 
    .doc(cloudId)
    .collection('words')

    ref.add({
        userId: userId,
        answer: answer,
    })
    .then(() => {
        onResolve(true);
    })
    .catch(() => {
        onResolve(false);
    })
}

}
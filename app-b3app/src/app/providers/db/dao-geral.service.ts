import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { TypeUser } from 'src/app/models/type-user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PathApi } from '../../paths/path-api';
import { firestore } from 'firebase/app';
import 'rxjs/add/operator/catch';
import { environment } from 'src/environments/environment';
import { UUID } from 'angular2-uuid';
import { CeuAttendee } from 'src/app/models/ceu-attendee';
import { take, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DaoGeralService {
    public headers;
    public requestOptions;
    constructor(
        private aFirestore: AngularFirestore,
        public http: HttpClient,
    ) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
        // AngularFirestoreModule.enablePersistence();
    }

    loadUser(userId: string, typeUser: number, eventId: string): Observable<any> {
        if (typeUser == TypeUser.SUPERGOD || typeUser == TypeUser.GOD || typeUser == TypeUser.CLIENT || typeUser == TypeUser.EMPLOYEE) {
            return (this.aFirestore.collection('users')
                .doc(userId)
                .valueChanges());
        } else if (typeUser == TypeUser.SPEAKER) {
            return (this.aFirestore.collection('events')
                .doc(eventId)
                .collection('speakers')
                .doc(userId)
                .valueChanges());
        } else if (typeUser == TypeUser.ATTENDEE) {
            return (this.aFirestore.collection('events')
                .doc(eventId)
                .collection('attendees')
                .doc(userId)
                .valueChanges());
        }
    }

    // loadUser(userId: string, typeUser: number, eventId: string, onResolve) {
    //     let db = this.aFirestore.firestore;

    //     if (typeUser == TypeUser.SUPERGOD || typeUser == TypeUser.GOD || typeUser == TypeUser.CLIENT || typeUser == TypeUser.EMPLOYEE) {
    //         db
    //             .collection('users')
    //             .doc(userId)
    //             .onSnapshot((user) => {
    //                 if (typeof user.data() !== 'undefined' && user.data() !== null) {
    //                     onResolve(user.data());
    //                 }

    //             }, (err) => {
    //                 console.error(err)
    //                 onResolve(false);
    //             })
    //     } else if (typeUser == TypeUser.SPEAKER) {
    //         db
    //             .collection('events')
    //             .doc(eventId)
    //             .collection('speakers')
    //             .doc(userId)
    //             .onSnapshot((user) => {
    //                 onResolve(user.data());
    //             }, (err) => {
    //                 onResolve(false);
    //             })
    //     } else if (typeUser == TypeUser.ATTENDEE) {
    //         db
    //             .collection('events')
    //             .doc(eventId)
    //             .collection('attendees')
    //             .doc(userId)
    //             .onSnapshot((user) => {
    //                 onResolve(user.data());
    //             }, (err) => {
    //                 onResolve(false);
    //             })
    //     }
    // }

    addUserToEvent(user, eventId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let batch = db.batch();
        let refEvent = db.collection('events').doc(eventId);
        let refEventAttendee = db.collection('events').doc(eventId).collection('attendees').doc(user.uid);
        let refUsers = db.collection('users').doc(user.uid);

        refEvent
            .get()
            .then((snapshot) => {
                let event = snapshot.data();

                let attendee = new CeuAttendee();
                attendee.name = user.name;
                attendee.email = user.email;
                attendee.identifier = UUID.UUID();
                attendee.firstAccess = false;
                attendee.moduleId = event['default_attendee_module'];
                attendee.queryName = attendee.name.toUpperCase();
                attendee.createdAt = Date.now() / 1000 | 0;
                attendee.eventId = eventId;
                attendee.language = event['language'];
                attendee.groups = {};
                let attendeeModuleId = event['default_attendee_module'];
                let refModule = db.collection('modules').doc(attendeeModuleId);
                let refModuleAttendee = refModule.collection('attendees').doc(user.uid);

                let blockAdd = false;
                if (blockAdd == false) {
                    blockAdd = true;
                    user = Object.assign({}, user);
                    user.queryName = user.name.toUpperCase();
                    user.language = event.language;
                    user.moduleId = attendeeModuleId;
                    user.eventId = eventId;
                    user.events = firestore.FieldValue.arrayUnion(eventId);
                    user.attendeeModules = firestore.FieldValue.arrayUnion(attendeeModuleId);
                    batch.set(refModuleAttendee, attendee);
                    batch.set(refEventAttendee, attendee);
                    batch.update(refUsers, { events: user.events });

                    batch.commit()
                        .then((res) => {
                            onResolve(true);
                        })
                        .catch((e) => {
                            onResolve(false);
                        });
                }
            });
    }

    getUserFromUser(userId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('users')
            .doc(userId)
            .onSnapshot((user) => {
                onResolve(user.data());
            }, (e) => {
                onResolve(false);
            })
    }

    createAttendeeFirstAccess(oldUid: string, newUid: string) {
        return new Promise((resolve, reject) => {
            let db = this.aFirestore.firestore;

            db.collection("users")
                .doc(oldUid)
                .get()
                .then((user) => {
                    let aux = user.data();
                    let body = {
                        uid: newUid,
                        type: aux.type
                    }

                    this.http.post(PathApi.baseUrl + PathApi.authSetUserClaims, body, this.requestOptions)
                        .subscribe((_) => { });
                    if (aux.language == '' || aux.language == null || aux.language == undefined) {
                        aux.language = environment.platform.defaultLanguage;
                    }
                    let eventsId = aux.events;
                    let data = {
                        name: aux.name,
                        type: aux.type,
                        email: aux.email,
                        photoUrl: aux.photoUrl,
                        createdAt: aux.createdAt,
                        company: aux.company,
                        title: aux.title,
                        description: aux.description,
                        recoveryEmail: aux.recoveryEmail,
                        phone: null,
                        uid: newUid,
                        events: aux.events,
                        attendeeModules: aux.attendeeModules,
                        firstAccess: false,
                        language: aux.language
                    }

                    let userObj = this.userModelData(data);
                    let cont = 0;
                    for (let idEvent of eventsId) {
                        let batch = null;
                        batch = db.batch();

                        // adicionar no path users o novo
                        let newUserRef = db.collection("users").doc(newUid);
                        batch.set(newUserRef, userObj);

                        // apgar no path users antigo

                        let oldUserRef = db.collection("users").doc(oldUid);
                        batch.delete(oldUserRef);

                        let oldAttendeeRef = db.collection("events").doc(idEvent).collection("attendees").doc(oldUid);
                        let newAttendeeRef = db.collection("events").doc(idEvent).collection("attendees").doc(newUid);

                        let listCustomField = [];
                        oldAttendeeRef.collection('customFields').get().then((data) => {
                            data.forEach(doc => {
                                let custom = doc.data();
                                listCustomField.push(custom);
                            });
                        })

                        oldAttendeeRef.get().then((data) => {
                            let attendee = data.data();
                            attendee.uid = newUid;
                            attendee.firstAccess = false;
                            if (attendee.language == '' || attendee.language == null || attendee.language == undefined) {
                                attendee.language = environment.platform.defaultLanguage;
                            }
                            if (attendee.identifier == '' || attendee.identifier == null || attendee.identifier == undefined) {
                                attendee.identifier = UUID.UUID();
                            }
                            let oldModulesAttendeeRef = db.collection("modules").doc(attendee.moduleId).collection('attendees')
                                .doc(oldUid);
                            let newModulesAttendeeRef = db.collection("modules").doc(attendee.moduleId).collection('attendees')
                                .doc(newUid);


                            // adicionar no path events -> attendees
                            batch.set(newAttendeeRef, attendee);
                            // adicionar no path modules -> attendees novo
                            batch.set(newModulesAttendeeRef, attendee);

                            // apagar no path events -> attendes
                            batch.delete(oldAttendeeRef);
                            // apagar no path modules -> attendees antigo
                            batch.delete(oldModulesAttendeeRef);

                            batch.commit().then((batchOk) => {
                                let batchCustom = db.batch();
                                for (let custom of listCustomField) {
                                    let refCustomEventAttendee = newAttendeeRef.collection('customFields').doc(custom.uid);
                                    let refCustomModuleAttendee = newModulesAttendeeRef.collection('customFields').doc(custom.uid);

                                    batchCustom.set(refCustomEventAttendee, custom);
                                    batchCustom.set(refCustomModuleAttendee, custom);
                                }

                                batchCustom.commit().then(() => {
                                })

                                if (cont == eventsId.length - 1) {
                                    resolve({
                                        code: 201,
                                        message: 'success',
                                        result: batchOk
                                    });
                                }
                                cont++;
                            }).catch((batchError) => {
                                // remove da autenticação caso dê erro no banco
                                // admin.auth().deleteUser(newUid);

                                reject({
                                    code: 404,
                                    message: 'error',
                                    result: batchError
                                });
                            })
                        });
                    }
                });
        })

    }

    createSpeakerFirstAccess(oldUid: string, newUid: string) {
        return new Promise((resolve, reject) => {
            let db = this.aFirestore.firestore;
            db
                .collection("users")
                .doc(oldUid)
                .get()
                .then((user) => {
                    let aux = user.data();

                    let body = {
                        uid: newUid,
                        type: aux.type
                    }

                    this.http.post(PathApi.baseUrl + PathApi.authSetUserClaims, body, this.requestOptions)
                        .subscribe((_) => { });

                    let eventsId = aux.events;
                    if (aux.language == '' || aux.language == null || aux.language == undefined) {
                        aux.language = environment.platform.defaultLanguage;
                    }
                    if (aux.identifier == '' || aux.identifier == null || aux.identifier == undefined) {
                        aux.identifier = UUID.UUID();
                    }
                    let data = {
                        name: aux.name,
                        type: aux.type,
                        email: aux.email,
                        photoUrl: aux.photoUrl,
                        createdAt: aux.createdAt,
                        company: aux.company,
                        title: aux.title,
                        description: aux.description,
                        recoveryEmail: aux.recoveryEmail,
                        uid: newUid,
                        events: aux.events,
                        speakerModules: aux.speakerModules,
                        firstAccess: false,
                        language: aux.language,
                        edited_profile: false
                    }

                    let userObj = this.userModelDataSpeaker(data);

                    for (let idEvent of eventsId) {
                        let batch = null;
                        batch = db.batch();

                        // adicionar no path users o novo
                        let newUserRef = db.collection("users").doc(newUid);
                        batch.set(newUserRef, userObj);

                        // apgar no path users antigo

                        let oldUserRef = db.collection("users").doc(oldUid);
                        batch.delete(oldUserRef);

                        let oldSpeakerRef = db.collection("events").doc(idEvent).collection("speakers").doc(oldUid);
                        let newSpeakerRef = db.collection("events").doc(idEvent).collection("speakers").doc(newUid);

                        let listCustomField = [];
                        oldSpeakerRef.collection('customFields').get().then((data) => {
                            data.forEach(doc => {
                                let custom = doc.data();
                                listCustomField.push(custom);
                            });
                        })

                        oldSpeakerRef.get().then((data) => {
                            let speaker = data.data();
                            if (speaker.language == '' || speaker.language == null || speaker.language == undefined) {
                                speaker.language = environment.platform.defaultLanguage;
                            }
                            if (speaker.language == '' || speaker.language == null || speaker.language == undefined) {
                                environment.platform.defaultLanguage;
                            }
                            speaker.uid = newUid;
                            speaker.firstAccess = false;
                            let oldModulesSpeakerRef = db.collection("modules").doc(speaker.moduleId).collection('speakers').doc(oldUid);
                            let newModulesSpeakerRef = db.collection("modules").doc(speaker.moduleId).collection('speakers').doc(newUid);


                            // adicionar no path events -> speakers
                            batch.set(newSpeakerRef, speaker);
                            // adicionar no path modules -> speakers novo
                            batch.set(newModulesSpeakerRef, speaker);

                            // apagar no path events -> speaker
                            batch.delete(oldSpeakerRef);
                            // apagar no path modules -> speakers antigo
                            batch.delete(oldModulesSpeakerRef);

                            batch.commit().then((batchOk) => {
                                let batchCustom = db.batch();
                                for (let custom of listCustomField) {
                                    let refCustomEventSpeaker = newSpeakerRef.collection('customFields').doc(custom.uid);
                                    let refCustomModuleSpeaker = newModulesSpeakerRef.collection('customFields').doc(custom.uid);

                                    batchCustom.set(refCustomEventSpeaker, custom);
                                    batchCustom.set(refCustomModuleSpeaker, custom);
                                }

                                batchCustom.commit().then(() => {
                                })

                                resolve({
                                    code: 201,
                                    message: 'success',
                                    result: batchOk
                                });
                            }).catch((batchError) => {
                                //remove da autenticação caso dê erro no banco
                                // admin.auth().deleteUser(newUid);

                                reject({
                                    code: 404,
                                    message: 'error',
                                    result: batchError
                                });
                            })
                        });
                    }
                });
        })
    }

    userModelData(user) {
        let userFormat = {
            name: null,
            type: null,
            email: "",
            language: "",
            description: "",
            photoUrl: "",
            company: "",
            title: "",
            recoveryEmail: "",
            events: null,
            attendeeModules: null,
            firstAccess: null,
            uid: "",
            edited_profile: false
        };

        if (user.name != undefined) {
            userFormat.name = user.name;
        }

        if (user.type != undefined) {
            userFormat.type = user.type;
        }

        if (user.email != undefined) {
            userFormat.email = user.email;
        }

        if (user.language != undefined) {
            userFormat.language = user.language;
        }

        if (user.description != undefined) {
            userFormat.description = user.description;
        }

        if (user.photoUrl != undefined) {
            userFormat.photoUrl = user.photoUrl;
        }

        if (user.company != undefined) {
            userFormat.company = user.company;
        }

        if (user.title != undefined) {
            userFormat.title = user.title;
        }

        if (user.recoveryEmail != undefined) {
            userFormat.recoveryEmail = user.recoveryEmail;
        }

        if (user.events != undefined) {
            userFormat.events = user.events;
        }

        userFormat.attendeeModules = user.attendeeModules

        if (user.firstAccess != undefined) {
            userFormat.firstAccess = user.firstAccess;
        }

        if (user.uid != undefined) {
            userFormat.uid = user.uid;
        }

        return userFormat;
    }

    userModelDataSpeaker(user) {
        let userFormat = {
            name: null,
            type: null,
            email: "",
            language: "",
            description: "",
            photoUrl: "",
            company: "",
            title: "",
            recoveryEmail: "",
            events: null,
            speakerModules: null,
            firstAccess: null,
            uid: "",
            edited_profile: false
        };

        if (user.name != undefined) {
            userFormat.name = user.name;
        }

        if (user.type != undefined) {
            userFormat.type = user.type;
        }

        if (user.email != undefined) {
            userFormat.email = user.email;
        }

        if (user.language != undefined) {
            userFormat.language = user.language;
        }

        if (user.description != undefined) {
            userFormat.description = user.description;
        }

        if (user.photoUrl != undefined) {
            userFormat.photoUrl = user.photoUrl;
        }

        if (user.company != undefined) {
            userFormat.company = user.company;
        }

        if (user.title != undefined) {
            userFormat.title = user.title;
        }

        if (user.recoveryEmail != undefined) {
            userFormat.recoveryEmail = user.recoveryEmail;
        }

        if (user.events != undefined) {
            userFormat.events = user.events;
        }

        userFormat.speakerModules = user.speakerModules

        if (user.firstAccess != undefined) {
            userFormat.firstAccess = user.firstAccess;
        }

        if (user.uid != undefined) {
            userFormat.uid = user.uid;
        }

        return userFormat;
    }

    getTermsOfUse(onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('general-legal-terms')
            .doc('terms')
            .onSnapshot((data) => {
                let terms = data.data();
                onResolve(terms['termOfUse'])
            });
    }

    getPrivacy(onResolve) {
        let db = this.aFirestore.firestore;
        db
            .collection('general-legal-terms')
            .doc('terms')
            .onSnapshot((data) => {
                let terms = data.data();
                onResolve(terms['privacy'])
            });
    }

    updateUserPushNotification(userIds: any, userId: string, eventId: string, typeUser: number) {
        let db = this.aFirestore.firestore;

        if (typeUser == TypeUser.SUPERGOD || typeUser == TypeUser.GOD || typeUser == TypeUser.CLIENT || typeUser == TypeUser.EMPLOYEE) {
            db
                .collection('users')
                .doc(userId)
                .update({ notification: userIds, havePushId: true });
        } else if (typeUser == TypeUser.SPEAKER) {
            db
                .collection('events')
                .doc(eventId)
                .collection('speakers')
                .doc(userId)
                .update({ notification: userIds, havePushId: true })
        } else if (typeUser == TypeUser.ATTENDEE) {
            db
                .collection('events')
                .doc(eventId)
                .collection('attendees')
                .doc(userId)
                .update({ notification: userIds, havePushId: true })
        }
    }

    updateUserGeneral(obj, userId: string, eventId: string, typeUser: number) {
        let db = this.aFirestore.firestore;
        if (typeUser == TypeUser.SUPERGOD || typeUser == TypeUser.GOD || typeUser == TypeUser.CLIENT || typeUser == TypeUser.EMPLOYEE) {
            db
                .collection('users')
                .doc(userId)
                .update(obj);
        } else if (typeUser == TypeUser.SPEAKER) {
            db
                .collection('events')
                .doc(eventId)
                .collection('speakers')
                .doc(userId)
                .update(obj)
        } else if (typeUser == TypeUser.ATTENDEE) {
            db
                .collection('events')
                .doc(eventId)
                .collection('attendees')
                .doc(userId)
                .update(obj)
        } else if (typeUser == TypeUser.SPEAKER) {
            db
                .collection('events')
                .doc(eventId)
                .collection('speaker')
                .doc(userId)
                .update(obj)
        }
    }

    getContainer(containerId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('containers').doc(containerId);

        ref
            .get()
            .then((container) => {
                onResolve(container.data());
            })
            .catch((e) => {
                console.error(e);
                onResolve(null);
            })
    }

    getEvent(eventId: string) {
        return (this.aFirestore.collection('events').doc(eventId).get().pipe(
            take(1),
            map((doc) => {
                return (doc.data())
            })
        ));
        // let db = this.aFirestore.firestore;
        // let ref = db.collection('events').doc(eventId);

        // ref
        //     .get()
        //     .then((snapshot) => {
        //         let event = snapshot.data();
        //         onResolve(event);
        //     })
    }

}

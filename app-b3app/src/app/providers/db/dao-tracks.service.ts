import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})

export class DaoTrackService {
    private refGetTracksModule = null

    constructor(
        private aFirestore: AngularFirestore,
    ) {
        AngularFirestoreModule.enablePersistence();
    }

    // loads the module tracks.
    getTracksModule(moduleId: string, lang, onResolve) {
        const queryName = `queryName.${lang}`;
        this.refGetTracksModule = this.aFirestore.
            collection('modules')
            .doc(moduleId).collection('tracks', ref => ref.orderBy(queryName, 'asc'))
            .valueChanges()
            .subscribe((data) => {
                onResolve(data)
            })

    }

    closeGetTracksModule() {
        if (this.refGetTracksModule)
            this.refGetTracksModule.unsubscribe()
    }
}
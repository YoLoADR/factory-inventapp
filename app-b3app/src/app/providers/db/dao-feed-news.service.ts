import { Injectable } from '@angular/core';
// import { firestore } from 'firebase/app';
import { StorageService } from '../storage/storage.service';
import { Post } from '../../models/ceu-post';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DaoFeedNewsService {
    // private db

    refGetModule = null
    refGetPostsNewsFeed = null


    constructor(
        private storage: StorageService,
        private aFirestore: AngularFirestore
    ) {
        AngularFirestoreModule.enablePersistence();
        // try {
        //     firestore().enablePersistence()
        //         .then(function () {
        //             // Initialize Cloud Firestore through firebase
        //             this.db = firestore();
        //         })

        // } catch (e) {
        //     this.db = firestore();
        // }
    }

    moduleNewsFeed(moduleId, onResolve) {
        this.refGetModule = this.aFirestore.collection('modules').doc(moduleId).valueChanges().subscribe((module) => {
            onResolve(module)
        })
    }

    // close get module
    closeRefGetModule() {
        if (this.refGetModule)
            this.refGetModule.unsubscribe()
    }

    // create an id for the post that will be created
    createIdPost() {
        return this.aFirestore.collection("modules").doc().ref.id;
    }

    public allPosts = [];
    getPostsNewsFeed(moduleId: string, onResolve) {
        // get active posts only, ordened by fixedTop property
        this.refGetPostsNewsFeed = this.aFirestore.collection('modules').doc(moduleId).collection('posts', ref => ref.orderBy('fixedTop').orderBy('date', 'desc'))
            .valueChanges().subscribe((data) => {
                this.allPosts = data;
                onResolve(this.allPosts)
            })
    }

    // close GetPostsNewsFeed
    closeGetPostsNewsFeed() {
        if (this.refGetPostsNewsFeed)
            this.refGetPostsNewsFeed.unsubscribe()
    }

    getPost(moduleId: string, postId: string, onResolve) {
        this.aFirestore.collection('modules').doc(moduleId).collection('posts').doc(postId).snapshotChanges().pipe(
            map((snapshot) => {
                return (snapshot.payload.data());
            })
        ).toPromise().then((data) => {
            onResolve(data)
        })

        // ref.onSnapshot((snapshot) => {
        //     const data = snapshot.data()
        //     onResolve(data)
        // })
    }

    updatePost(post, eventId: string) {
        const moduleId = post.moduleId
        const postId = post.uid

        const ref = this.aFirestore.collection('modules').doc(moduleId).collection('posts').doc(postId);
        const refEvent = this.aFirestore.collection('events').doc(eventId).collection('feed-posts').doc(postId);

        ref.update(Object.assign({}, post));
        refEvent.update(Object.assign({}, post));
    }

    createPost(post, eventId: string) {
        return new Promise((resolve, reject) => {
            const moduleId = post.moduleId
            const postId = post.uid

            const ref = this.aFirestore.collection('modules').doc(moduleId).collection('posts').doc(postId);
            const refEvent = this.aFirestore.collection('events').doc(eventId).collection('feed-posts').doc(postId);

            ref.set(Object.assign({}, post))
                .then(() => {
                    resolve(true)
                })
                .catch((error) => {
                    reject(error)
                });
            refEvent.set(Object.assign({}, post));
        });
    }

    deletePost(post: Post, eventId: string) {
        return new Promise((resolve, reject) => {
            const moduleId = post.moduleId
            const postId = post.uid

            // delete img storarge
            if (post.img.url.length > 0) {
                this.storage.deletePost(post.uid, eventId, post.moduleId)
            }

            const ref = this.aFirestore.collection('modules').doc(moduleId).collection('posts').doc(postId)
            const refEvent = this.aFirestore.collection('events').doc(eventId).collection('feed-posts').doc(postId);
            ref.delete()
                .then(() => {
                    resolve(true)
                })
                .catch((error) => {
                    reject(error)
                })
            refEvent.delete();
        });
    }


}

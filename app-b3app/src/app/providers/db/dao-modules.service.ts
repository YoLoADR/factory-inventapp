import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { map, auditTime } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DaoModulesService {

    constructor(
        private aFirestore: AngularFirestore
    ) {
        AngularFirestoreModule.enablePersistence();
    }

    getMenuModules(eventId: string) {
        return (this.aFirestore.collection('modules', (ref) => ref.where("eventId", "==", eventId)
            .where("habilitedApp", "==", true)
            .where("order", ">=", 0)
            .orderBy("order"))
            .snapshotChanges().pipe(
                auditTime(500),
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as any);
                    }))
                })
            ))
        // let db = this.aFirestore.firestore;
        // let ref = db.collection("modules").where('eventId', '==', eventId).where('habilitedApp', '==', true).where('order', '>=', 0).orderBy('order');

        // ref.onSnapshot((snapshot) => {
        //     const list = []
        //     snapshot.forEach((document) => {
        //         if (typeof document.data() !== `undefined`) {
        //             const module = document.data()
        //             list.push(module)
        //         }
        //     })
        //     onResolve(list)
        // })
    }

    getModulesEvent(eventId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection("events").doc(eventId).collection('modules')

        ref.onSnapshot((snapshot) => {
            const list = []
            snapshot.forEach((document) => {
                const module = document.data()
                list.push(module)
            })

            onResolve(list)
        })
    }

    /**
     * Get module
     * @param moduleId 
     */
    getModule(moduleId: string) {
        return (this.aFirestore.collection('modules').doc(moduleId).snapshotChanges().pipe(
            map((snapshot) => {
                return (snapshot.payload.data() as any);
            })
        ))
    }
}

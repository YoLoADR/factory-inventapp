import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PathApi } from '../../paths/path-api';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { TypeModule } from 'src/app/enums/type-module';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { CeuUser } from 'src/app/models/ceu-user';

@Injectable({
    providedIn: 'root'
})
export class DaoEventsService {
    public headers;
    public requestOptions;
    constructor(
        private http: HttpClient,
        public global: GlobalService,
        private aFirestore: AngularFirestore
    ) {
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
        AngularFirestoreModule.enablePersistence();
    }


    /**
     * Getting event
     * @param eventId 
     */
    getEvent(eventId: string): Observable<any> {
        return (this.aFirestore.collection('events').doc(eventId).snapshotChanges().pipe(
            switchMap((doc) => {
                if (doc.payload.exists) {
                    return (of(doc.payload.data()));
                } else {
                    return ((of(null)));
                }
            })
        ))
    }

    getEventByShortcode(shortcode: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('events')
            .where('shortcode', '==', shortcode)
            .get()
            .then((values) => {
                if (values.size >= 1) {
                    values.forEach(doc => {
                        onResolve(doc.data());
                    });
                } else {
                    onResolve(null);
                }
            })
            .catch((err) => {
                onResolve(err);
            });
    }

    getAllEvents(onResolve) {
        let db = this.aFirestore.firestore;
        db.collection('events')
            .onSnapshot((events) => {
                const list = [];

                events.forEach(
                    (doc) => {
                        let event = doc.data();
                        list.push(event);
                    }
                )

                list.sort(function (a, b) {
                    return a.startDate < b.startDate ? -1 : a.startDate > b.startDate ? 1 : 0;
                });

                onResolve({
                    code: 200,
                    message: 'success',
                    result: list
                });

            })
        // .catch((error) => {
        //     onResolve({
        //         code: 404,
        //         message: 'error',
        //         result: error
        //     });
        // })
        // this.http.get(PathApi.baseUrl + PathApi.dbEventGetAllEvents, this.requestOptions).subscribe((data) => {
        //     onResolve(JSON.parse(data['_body']));
        // }), err => {
        //     onResolve(err);
        // }
    }

    getClientEvents(clientId: string, onResolve) {
        let db = this.aFirestore.firestore;
        db
            .collection('events')
            .where('client.uid', '==', clientId)
            .onSnapshot((events) => {
                const list = [];

                events.forEach(
                    (doc) => {
                        let event = doc.data();
                        list.push(event);
                    }
                )

                list.sort(function (a, b) {
                    return a.startDate < b.startDate ? -1 : a.startDate > b.startDate ? 1 : 0;
                });

                onResolve({
                    code: 200,
                    message: 'success',
                    result: list
                });

            })
        // .catch((error) => {
        //     onResolve({
        //         code: 404,
        //         message: 'error',
        //         result: error
        //     });
        // })
        // this.http.get(PathApi.baseUrl + PathApi.dbEventGetClientEvents + clientId, this.requestOptions).subscribe((data) => {
        //     onResolve(JSON.parse(data['_body']));
        // }), err => {
        //     onResolve(err);
        // }
    }

    getUserEvents(userId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let events = [];

        db
            .collection('users')
            .doc(userId)
            .onSnapshot((doc) => {
                let auxEvents = [];
                auxEvents = doc.data()['events'];

                if (auxEvents.length <= 0) { onResolve(events) }
                let cont = 0
                for (let event of auxEvents) {
                    this.getEvent(event).subscribe((eventData) => {
                        cont++

                        for (let event of events) {
                            let indexUid = this.checkUidExistsInArray(auxEvents, event.uid);
                            if (indexUid == -1) {
                                events.splice(indexUid, 1);
                            }
                        }

                        if (eventData !== null && eventData !== undefined) {
                            let index = this.checkIndexExists(events, eventData);
                            if (index == -1) {
                                events.push(eventData);
                            } else {
                                events[index] = eventData;
                            }
                        }

                        if (cont >= auxEvents.length) {
                            events.sort(function (a, b) {
                                return a.startDate < b.startDate ? -1 : a.startDate > b.startDate ? 1 : 0;
                            });

                            onResolve(events);
                        }
                    });
                }
            })
    }

    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    checkUidExistsInArray(array, item) {
        return array.map(function (e) { return e; }).indexOf(item);
    }

    getLanguageEvent(eventId) {
        return new Promise((resolve) => {
            let db = this.aFirestore.firestore;

            db.collection('events').doc(eventId).get().then((doc) => {
                const language = doc.get('language')
                resolve(language);
            })
        })
    }

    getPublicEvents(onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('events')
            .where('visibility', '==', true)
            .onSnapshot((values) => {
                let events = [];
                if (values.size >= 1) {
                    values.forEach(element => {
                        events.push(element.data());
                    });
                }
                onResolve(events);
            });
    }

    getModuleEvent(eventId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('events')
            .doc(eventId)
            .collection('modules')
            .where('type', '==', TypeModule.EVENT)
            .get()
            .then((snapshot) => {
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        onResolve(element.data());
                    });
                } else {
                    onResolve(null);
                }
            })
    }
}

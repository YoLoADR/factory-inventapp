import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Quiz } from 'src/app/models/quiz';
import { Question } from 'src/app/models/quiz-question';
import { Answer } from 'src/app/models/quiz-answer';
import { firestore } from 'firebase/app';
import { TypeModule } from 'src/app/models/type-module';
import { NameModule } from 'src/app/models/path/name-module';
import { map, take, switchMap, auditTime } from 'rxjs/operators';
import { combineLatest, of, Observable } from 'rxjs';
import { UtilityService } from 'src/app/shared/services';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class DaoQuizService {

    constructor(
        private aFirestore: AngularFirestore,
        private SUtility: UtilityService
    ) { }

    /**
     * Get quiz module for an event
     * @param eventId 
     */
    getQuizModule(eventId: string) {
        return (this.aFirestore
            .collection('events')
            .doc(eventId)
            .collection('modules', (ref) => ref.where('type', '==', TypeModule.QUIZ))
            .get().pipe(
                take(1),
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        return (doc.data());
                    }))
                })
            ))
    }

    /**
     * Get module of a quiz
     * @param quizModuleId 
     */
    getModule(quizModuleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(quizModuleId)
            .get().pipe(
                take(1),
                map((doc) => {
                    return (doc.data());
                })
            ))
    }

    /**
     * Get quizs session
     * @param eventId 
     * @param moduleId 
     * @param scheduleModuleId 
     * @param sessionId 
     * @param userId 
     * @param onResolve 
     */
    getQuizsSession(eventId: string, moduleId: string, scheduleModuleId: string, sessionId: string, userId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('quizs', (ref) => ref.orderBy('order', 'asc'))
            .snapshotChanges().pipe(
                switchMap(async (docs) => {
                    let listQuizs: Array<Quiz> = [];
                    let listQuizsSession: Array<Quiz> = [];
                    if (docs.length > 0) {
                        docs.forEach((element) => {
                            let quiz = this.instantiateQuiz(element.payload.doc.data());
                            if (quiz.visibility) {
                                let index = this.SUtility.checkIndexExists(listQuizs, quiz);

                                if (index >= 0) {
                                    listQuizs[index] = quiz;
                                } else {
                                    listQuizs.push(quiz);
                                }
                            }
                        });

                        for (let quiz of listQuizs) {

                            if (quiz.type === 'AllSessions') {
                                listQuizsSession.push(quiz);
                            }
                            else if (quiz.type === 'ScheduleModule') {
                                if (scheduleModuleId === quiz.module_id) {
                                    listQuizsSession.push(quiz);
                                }
                            }
                            else if (quiz.type === 'SessionTrack') {
                                // case this session be inside a track with permission, display feedback
                                let tracks = await this.checkSpecificTrackQuiz(quiz.module_id, sessionId, quiz.references);
                                for (const track of tracks) {
                                    if (track) {
                                        listQuizsSession.push(quiz);
                                        break;
                                    }
                                }
                            }
                            else if (quiz.type === 'SpecificSession') {
                                for (let uid of quiz.references) {
                                    if (uid === sessionId) {
                                        listQuizsSession.push(quiz);
                                        break;
                                    }
                                }
                            }
                            else if (quiz.type === 'SpecificGroup') {
                                await this.listgroupsOfAttendee(eventId, userId, (listGroupsAttendee) => {
                                    const references: any = quiz.references
                                    let findGroup = false;
                                    for (let uidGroup of references) {
                                        for (const uidGroupUser of listGroupsAttendee) {
                                            //if the user is part of the group
                                            if (uidGroup == uidGroupUser && findGroup == false) {
                                                listQuizsSession.push(quiz);
                                                findGroup = true;
                                                break;
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                    return (listQuizsSession);
                })
            ))
    }

    /**
     * Get all quizs for an event
     * @param moduleId 
     */
    getQuizsEvent(moduleId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .valueChanges().pipe(
                map((docs) => {
                    return (docs.map((doc) => {
                        let quiz = this.instantiateQuiz(doc);
                        return (quiz);
                    }))
                })
            ))
    }

    /**
     * Get quiz
     * @param moduleId 
     * @param quizId 
     */
    getQuiz(moduleId: string, quizId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .get().pipe(
                take(1),
                map((doc) => {
                    if (doc.exists) {
                        let quiz: Quiz = this.instantiateQuiz(doc.data());
                        return (quiz);
                    } else {
                        return (null);
                    }
                }),
                switchMap((quiz) => {
                    if (quiz) {
                        return (this.getQuestions(moduleId, quizId).pipe(
                            switchMap((questions) => {
                                quiz.questions = questions;
                                return (of(quiz));
                            })
                        ))
                    } else {
                        return (of(null));
                    }
                })
            ))
    }

    /**
     * Get questions of a quiz
     * @param moduleId 
     * @param quizId 
     */
    getQuestions(quizModuleId: string, quizId: string): Observable<any> {
        return (this.aFirestore
            .collection('modules')
            .doc(quizModuleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions', (ref) => ref.orderBy('createdAt', 'asc'))
            .get().pipe(
                take(1),
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        let question = this.instantiateQuestion(doc.data());
                        return (question);
                    }))
                }),
                switchMap((questions) => {
                    let obs = [];
                    if (questions && questions.length > 0) {
                        for (let i = 0; i < questions.length; i++) {
                            let question = questions[i];
                            obs.push(this.getAnswers(quizModuleId, quizId, question.uid).pipe(
                                map((answers) => {
                                    question.answers = answers;
                                    return (question);
                                })
                            ));
                        }

                        return (combineLatest(obs))
                    } else {
                        return (of([]));
                    }
                })
            ))
    }

    /**
     * Get total of questions on a quiz
     * @param quizModuleId 
     * @param quizId 
     */
    getTotalQuestions(quizModuleId: string, quizId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(quizModuleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .get().pipe(
                take(1),
                switchMap((docs) => {
                    return (of(docs.size));
                })
            ))
    }

    /**
     * Get answer for the questions of a quiz
     * @param quizModuleId 
     * @param quizId 
     * @param questionId 
     */
    getAnswers(quizModuleId: string, quizId: string, questionId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(quizModuleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(questionId)
            .collection('answers', (ref) => ref.orderBy('createdAt', 'asc'))
            .get().pipe(
                map((docs) => {
                    return (docs.docs.map((doc) => {
                        let answer: Answer = this.instantiateAnswer(doc.data());
                        return (answer)
                    }))
                })
            ))
    }

    /**
     * Check specific track on a quiz
     * @param trackModuleId 
     * @param sessionId 
     * @param tracksIds 
     */
    async checkSpecificTrackQuiz(trackModuleId, sessionId, tracksIds) {
        let list = [];
        for (let trackId of tracksIds) {
            let result = await this.checkTrackQuiz(trackModuleId, sessionId, trackId);
            list.push(result);

            if (list.length == tracksIds.length) {
                return (list);
            }
        }

    }

    /**
     * Check track on a quiz
     * @param moduleId 
     * @param sessionId 
     * @param trackId 
     * @param onResolve 
     */
    checkTrackQuiz(moduleId, sessionId, trackId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('tracks')
            .doc(trackId)
            .collection('sessions')
            .doc(sessionId).get().pipe(
                take(1),
                switchMap((docs) => {
                    if (docs.exists) {
                        return (of(true));
                    } else {
                        return (of(false));
                    }
                })
            ).toPromise())
    }

    listgroupsOfAttendee(eventId: string, attendeeId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db.collection('events').doc(eventId).collection('attendees').doc(attendeeId).get()
            .then((data) => {
                let attendee = data.data();
                let groups = attendee['groups'];
                let idsGroups: Array<String> = [];

                if (groups !== null && groups !== undefined) {
                    for (let uid in groups) {
                        idsGroups.push(uid);
                    }
                }

                onResolve(idsGroups);
            })
    }


    createResult(moduleId: string, userId: string, quizId: string, IdQuestion: string, type: string, answer: any, timestamp: number, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        if (type == "oneSelect" || type == "multipleSelect") {
            if (userId !== null && userId !== undefined && userId !== '') {
                this.getResponseUserQuestion(moduleId, quizId, IdQuestion, userId, (responses) => {
                    this.deletePrevAnswersSelectOption(moduleId, quizId, IdQuestion, userId, (result) => {
                        if (result == true) {
                            refQuestionsResult.doc(userId).set({
                                user: userId,
                                answer: answer,
                                timestamp: timestamp,
                                question: IdQuestion
                            })
                                .then(() => {
                                    if (responses !== null && responses !== undefined) {
                                        onResolve(false);
                                    } else {
                                        onResolve(true);
                                    }
                                })
                        }
                    })
                })
            } else {
                let document = refQuestionsResult.doc();
                userId = document.id;

                refQuestionsResult.doc(userId).set({
                    user: userId,
                    answer: answer,
                    timestamp: timestamp,
                    question: IdQuestion
                })
                    .then(() => {
                        onResolve(true);
                    })
            }
        } else {
            refQuestionsResult.doc(userId).set({
                question: IdQuestion,
                user: userId,
                answer: answer,
                timestamp: timestamp
            })
                .then(() => {
                    onResolve(true);
                })
        }
    }

    getResponseUsers(moduleId, quizId, userId, onResolve) {
        this.getQuestions(moduleId, quizId).subscribe((questions) => {
            let listResults = {};
            for (let i = 0; i < questions.length; i++) {
                this.getResponseUserQuestion(moduleId, quizId, questions[i].uid, userId, (response) => {
                    if (response != null) {
                        listResults[response.question] = response;
                    }

                    if (i == questions.length - 1) {
                        onResolve(listResults);
                    }
                })
            }
        })
    }

    getResponseUserQuestion(moduleId, quizId, questionId, userId, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(questionId)
            .collection('result')
            .doc(userId);

        ref.get()
            .then((data) => {
                if (data.exists) {
                    let result = data.data();
                    onResolve(result)
                } else {
                    onResolve(null);
                }
            })
    }

    deletePrevAnswersSelectOption(moduleId, quizId, IdQuestion, userId, onResolve) {
        let db = this.aFirestore.firestore;

        let refQuestionsResult = db
            .collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(IdQuestion)
            .collection('result');

        refQuestionsResult.doc(userId).delete().then(() => {
            onResolve(true)
        }).catch(() => {
            onResolve(false);
        })
    }

    /**
     * Instantiate quiz data
     * @param data 
     */
    instantiateQuiz(data) {
        let quiz = new Quiz();
        quiz.title = data.title;
        quiz.type = data.type;
        quiz.uid = data.uid;
        quiz.visibility = data.visibility;
        quiz.change_answer = data.change_answer;
        quiz.view_answered = data.view_answered;
        quiz.icon = data.icon;
        quiz.iconFamily = data.iconFamily;

        data.active_timer != undefined ? quiz.active_timer = data.active_timer : quiz.active_timer = false;
        data.timer_questions != undefined ? quiz.timer_questions = data.timer_questions : quiz.timer_questions = null;
        data.max_responses != undefined ? quiz.max_responses = data.max_responses : quiz.max_responses = null;
        data.module_id != undefined ? quiz.module_id = data.module_id : quiz.module_id = null;
        data.references != undefined ? quiz.references = data.references : quiz.references = null;

        return quiz;
    }

    /**
     * Instantiate a question
     * @param data 
     */
    instantiateQuestion(data) {
        let question = new Question();

        question.uid = data.uid;
        question.type = data.type;
        question.title = data.title;
        data.graphicDifusion != undefined ? question.graphicDifusion = data.graphicDifusion : question.graphicDifusion = false;
        data.infobooth != undefined ? question.infobooth = data.infobooth : question.infobooth = new NameModule('', '', '', '', '');
        data.image != undefined ? question.image = data.image : question.image = null;
        data.maxResponses != undefined ? question.maxResponses = data.maxResponses : question.maxResponses = null;
        data.graphic != undefined ? question.graphic = data.graphic : question.graphic = null;
        data.visibility != undefined ? question.visibility = data.visibility : question.visibility = true;

        return question;
    }

    /**
     * Instantiate answer
     * @param data 
     */
    instantiateAnswer(data) {
        let answer = new Answer();
        answer.uid = data.uid;
        answer.answer = data.answer;
        data.weight != undefined ? answer.weight = data.weight : answer.weight = null;
        answer.correct = data.correct;

        return answer;
    }

    setUserQuizAnswered(eventId: string, attendeeId: string, surveyId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId);
        ref.update({ answeredQuizes: firestore.FieldValue.arrayUnion(surveyId) })
            .then(() => {
                onResolve(true);
            });
    }

    /**
     * Check if a quiz is answered
     * @param eventId 
     * @param attendeeId 
     * @param quizId 
     */
    checkAnsweredQuiz(eventId: string, attendeeId: string, quizId: string) {
        return (this.aFirestore.collection('events').doc(eventId).collection('attendees').doc(attendeeId).get().pipe(
            map((doc) => {
                return (doc.data());
            }),
            switchMap((attendeeData) => {
                if (attendeeData.answeredQuizes.includes(quizId)) {
                    return (of(true));
                } else {
                    return (of(false));
                }
            })
        ))
    }

    /**
     * Get results of an answer
     * @param moduleId 
     * @param questionId 
     * @param quizId 
     */
    getResultsOfAnswer(moduleId: string, questionId: string, quizId: string) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection('quizs')
            .doc(quizId)
            .collection('questions')
            .doc(questionId)
            .collection('result').snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data());
                    }))
                })
            ))
    }
}
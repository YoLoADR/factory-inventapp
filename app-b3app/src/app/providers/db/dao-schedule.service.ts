import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { Session } from 'src/app/models/ceu-session';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PathApi } from 'src/app/paths/path-api';
import { map, switchMap, auditTime, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class DaoScheduleService {
    private refGetModule = null
    private refGetFirstPageSessionsGlobalVision = null
    private refGetNextPageSessionsGlobalVision = null
    private refGetSessionsGroupGlobalVision = null
    private refGetSessionsGroupsVision = null
    private refGetFirstPageSessionsLimitedAccessByGroups = null
    private refGetNextPageSessionsLimitedAccessByGroups = null
    private refGetSessionModule = null
    private refGetSessionAttendees = null

    public headers;
    public requestOptions;

    constructor(
        private aFirestore: AngularFirestore,
        public http: HttpClient,
    ) {
        AngularFirestoreModule.enablePersistence();
        this.headers = new HttpHeaders();
        this.headers.append("Accept", 'application/json');
        this.headers.append("Content-Type", 'application/json');
        this.requestOptions = { headers: this.headers };
    }

    /**
     * Getting module
     * @param moduleId 
     */
    getModule(moduleId: string): Observable<any> {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .valueChanges())
    }

    /**
     * Get session module
     * @param moduleId 
     * @param sessionId 
     */
    getSessionModule(moduleId: string, sessionId: string): Observable<Session> {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .valueChanges().pipe(
                map((doc) => {
                    return (doc as Session);
                })
            ))
    }

    /**
     * Get question for session
     * @param moduleId 
     * @param sessionId 
     * @param onResolve 
     */
    getSessionModuleAskQuestion(moduleId: string, sessionId: string) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)
            .snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.payload.data());
                })
            ));
    }

    closeRefGetSessionModule() {
        if (this.refGetSessionModule)
            this.refGetSessionModule.unsubscribe()
    }


    getSessionEvent(eventId: string, sessionId: string, onResolve) {
        let db = this.aFirestore.firestore;

        const ref = db.collection('events').doc(eventId).collection('sessions').doc(sessionId);

        ref.onSnapshot((snapshot) => {
            const session = snapshot.data()
            onResolve(session);
        })
    }


    /******************************************************************************** methods of global vision *************************************************************************************** */

    // get the first 100 sessions of the module. (global vision)
    getFirstPageSessionsGlobalVision(moduleId: string) {
        console.log("Module id: ", moduleId);
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<Session>('sessions', (ref) => ref.orderBy('startTime').limit(100))
            .valueChanges()
            .pipe(
                switchMap((docs) => {
                    console.log("Docs: ", docs);
                    if (docs.length <= 0) {
                        return (of(false));
                    } else {
                        const list = [];
                        let nextPage = docs[docs.length - 1];

                        docs.forEach((session) => {
                            list.push(session)
                        })

                        return (of({
                            sessions: list,
                            nextPage: nextPage
                        }));
                    }
                })
            )
        )
    }

    // counts the total sessions you have in the module. (global vision)
    countsSessionsInTheModule(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;
        const ref = db.collection('modules').doc(moduleId).collection('sessions').orderBy('startTime')
        let total = 0

        ref.onSnapshot((snapshot) => {
            total = snapshot.size
            onResolve(total);
        })
    }


    getAllSessionsVisionGlobal(moduleId: string) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<Session>('sessions', (ref) => ref.orderBy('startTime'))
            .snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as Session);
                    }))
                })
            )
        )
    }

    // get plus 100 module sessions using the start time as a filter.
    getNextPageSessionsGlobalVision(moduleId, lastStartTime, onResolve) {
        this.refGetNextPageSessionsGlobalVision = this.aFirestore
            .collection('modules')
            .doc(moduleId).collection<Session>('sessions', ref => ref.orderBy('startTime')
                .startAfter(lastStartTime)
                .limit(100))
            .valueChanges()
            .subscribe((snapshot) => {
                const list = [];
                let nextPage = snapshot[snapshot.length - 1];

                snapshot.forEach((session) => {
                    list.push(session)
                })

                onResolve({
                    sessions: list,
                    nextPage: nextPage
                });
            })
    }

    closeRefGetNextPageSessionsGlobalVision() {
        if (this.refGetNextPageSessionsGlobalVision)
            this.refGetNextPageSessionsGlobalVision.unsubscribe()
    }

    /******************************************************************************** methods of divided by groups *************************************************************************************** */
    // loads the group sessions.
    getSessionsGroupGlobalVision(groupId: string, moduleId: string, onResolve) {
        const group = `groups.${groupId}`

        this.refGetSessionsGroupGlobalVision = this.aFirestore
            .collection('modules')
            .doc(moduleId).collection<Session>('sessions', ref => ref.orderBy(group))
            .valueChanges()
            .subscribe((snapshot) => {
                if (snapshot.length <= 0) {
                    onResolve(false)
                } else {
                    const list = [];
                    let nextPage = snapshot[snapshot.length - 1];

                    snapshot.forEach((session) => {
                        list.push(session)
                    })

                    // sorts the sessions by the startTime field.
                    list.sort(function (a, b) {
                        return a.startTime < b.startTime ? -1 : a.startTime > b.startTime ? 1 : 0;
                    });

                    onResolve({
                        sessions: list,
                        nextPage: nextPage
                    });
                }
            })
    }

    closeRefGetSessionsGroupGlobalVision() {
        if (this.refGetSessionsGroupGlobalVision)
            this.refGetSessionsGroupGlobalVision.unsubscribe()
    }

    /******************************************************************************** groups vision *************************************************************************************** */
    // loads the participant group sessions.
    getSessionsGroupsVision(groups: Array<any>, moduleId, onResolve) {
        if (groups.length > 0) {
            const total = groups.length
            let i = 0
            let list = []
            let listObs = [];

            // get the sessions of each group
            for (const group of groups) {
                const groupId = `groups.${group.uid}`;

                // session filter per group
                this.refGetSessionsGroupsVision = this.aFirestore
                    .collection('modules')
                    .doc(moduleId)
                    .collection<Session>('sessions', ref => ref.orderBy(groupId))
                    .valueChanges().pipe(
                        auditTime(500)
                    ).subscribe((snapshot) => {
                        i++

                        // withdraws replay of sessions
                        snapshot.forEach((session) => {
                            const pos = list.map(function (e) { return e.uid; }).indexOf(session.uid);
                            if (pos === -1) { list.push(session) }
                        })

                        if (i >= total) {
                            if (list.length <= 0) { onResolve(false) }

                            if (list.length > 0) {
                                let nextPage = list[list.length - 1];

                                // sorts the sessions by the startTime field.
                                list.sort(function (a, b) {
                                    return a.startTime < b.startTime ? -1 : a.startTime > b.startTime ? 1 : 0;
                                });


                                onResolve({
                                    sessions: list,
                                    nextPage: nextPage
                                });
                            }
                        }
                    })
            }
        } else {
            onResolve(false)
        }
    }

    closeRefGetSessionsGroupsVision() {
        if (this.refGetSessionsGroupsVision)
            this.refGetSessionsGroupsVision.unsubscribe()
    }

    /******************************************************************************** methods of limited access by groups *************************************************************************************** */
    // get the first 100 sessions of the module. (limited access by groups)
    getFirstPageSessionsLimitedAccessByGroups(moduleId: string) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection<Session>('sessions', (ref) => ref.orderBy('startTime').limit(100))
            .valueChanges().pipe(
                switchMap((docs) => {
                    if (docs.length <= 0) {
                        return (of(false));
                    } else {
                        const list = [];
                        let nextPage = docs[docs.length - 1];

                        docs.forEach((session) => {
                            list.push(session)
                        })

                        return (of({
                            sessions: list,
                            nextPage: nextPage
                        }));
                    }
                })
            )
        )
    }



    // get plus 100 module sessions using the start time as a filter. (limited access by groups)
    getNextPageSessionsLimitedAccessByGroups(moduleId, lastStartTime, onResolve) {
        this.refGetNextPageSessionsLimitedAccessByGroups = this.aFirestore
            .collection('modules')
            .doc(moduleId)
            .collection<Session>('sessions', ref => ref.orderBy('startTime').startAfter(lastStartTime)
                .limit(100))
            .valueChanges().subscribe((snapshot) => {
                const list = [];
                let nextPage = snapshot[snapshot.length - 1];

                snapshot.forEach((session) => {
                    list.push(session)
                })

                onResolve({
                    sessions: list,
                    nextPage: nextPage
                });
            })
    }

    getAllSessionsLimitedAccessByGroup(moduleId: string) {
        return (this.aFirestore.collection('modules')
            .doc(moduleId)
            .collection('sessions', (ref) => ref.orderBy('startTime'))
            .snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as Session);
                    }))
                })
            )
        )
    }

    closeRefGetNextPageSessionsLimitedAccessByGroups() {
        if (this.refGetNextPageSessionsLimitedAccessByGroups)
            this.refGetNextPageSessionsLimitedAccessByGroups.unsubscribe()
    }

    /**
     * Checks whether the session is part of the user's personal calendar
     * @param moduleScheduleId 
     * @param sessionId 
     * @param attendeeId 
     */
    checkAttendeeOfTheSession(moduleScheduleId, sessionId, attendeeId) {
        return (this.aFirestore
            .collection('modules')
            .doc(moduleScheduleId)
            .collection('sessions')
            .doc(sessionId)
            .collection('attendees')
            .doc(attendeeId)
            .snapshotChanges().pipe(
                switchMap((doc) => {
                    if (typeof doc.payload.data() !== 'undefined') {
                        return (of(true));
                    } else {
                        return (of(false));
                    }
                })
            ))
    }



    // loads session participants
    getSessionAttendees(moduleScheduleId, sessionId, onResolve) {
        let db = this.aFirestore.firestore;
        const ref = db.collection('modules').doc(moduleScheduleId).collection('sessions').doc(sessionId).collection('attendees')

        ref.onSnapshot((snapshot) => {
            const attendees = []
            snapshot.forEach((attendee) => {
                attendees.push(attendee)
            })

            onResolve(attendees)
        })
    }


    // returns the dates of all sessions
    sessionDates(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;
        const ref = db.collection('modules').doc(moduleId).collection('sessions').orderBy('date');
        let dates = [];

        ref.onSnapshot((snapshot) => {
            if (snapshot.docs.length === 0) {
                onResolve(false)
            } else {
                snapshot.forEach(element => {
                    dates.push(element.data().date);
                });
                onResolve(dates)
            }
        })
    }

    searchSessionsByName(moduleId: string, name: string, onResolve) {
        let db = this.aFirestore.firestore;
        name = this.convertLowerCaseUpperCase(name);
        let sessions = [];
        db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .orderBy('name')
            .startAt(name)
            .endAt(name + '\uf8ff')
            .onSnapshot((values) => {
                values.forEach(el => {
                    sessions.push(el.data());
                });
                onResolve(sessions);
            });
    }

    convertLowerCaseUpperCase(name) {
        var words = name.trim().toLowerCase().split(" ");
        for (var a = 0; a < words.length; a++) {
            var w = words[a];
            words[a] = w[0].toUpperCase() + w.slice(1);
        }
        words.join(" ");
        return words[0];
    }

    updateBroadcastId(eventId, moduleId, sessionId, broadcastId, onResolve) {
        let db = this.aFirestore.firestore;

        const refModule = db
            .collection('modules')
            .doc(moduleId)
            .collection('sessions')
            .doc(sessionId)

        const refEvent = db
            .collection('events')
            .doc(eventId)
            .collection('sessions')
            .doc(sessionId)

        let batch = db.batch()

        batch.update(refModule, { broadcastId: broadcastId })
        batch.update(refEvent, { broadcastId: broadcastId })

        batch.commit()
            .then(() => {
                onResolve(true)
            })
            .catch((err) => {
                console.log(err)
            })
    }
}

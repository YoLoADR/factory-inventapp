import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DaoGeralService } from './dao-geral.service';

@Injectable({
  providedIn: 'root'
})
export class DaoGamificationService {

  constructor(
    private aFirestore: AngularFirestore
  ) { }

  getModule(moduleId: string, onResolve) {
    let db = this.aFirestore.firestore;

    db.collection('modules').doc(moduleId)
      .onSnapshot((snapshot) => {
        onResolve(snapshot.data());
      });
  }

  getCodes(moduleId: string, onResolve) {
    let db = this.aFirestore.firestore;
    let ref = db.collection('modules').doc(moduleId).collection('gamification-qrcodes');
    ref
      .onSnapshot((snapshot) => {
        let codes = [];
        if (snapshot.size >= 1) {
          snapshot.forEach(element => {
            let code = element.data();
            codes.push(code);
          });
        }
        onResolve(codes);
      })
  }

  checkQr(uid: string, moduleId: string, onResolve) {
    let db = this.aFirestore.firestore;
    let ref = db.collection('modules').doc(moduleId).collection('gamification-qrcodes').doc(uid);

    ref
      .get()
      .then((snapshot) => {
        if (snapshot.exists) {
          onResolve({
            status: true,
            qrcode: snapshot.data()
          });
        } else {
          onResolve({
            status: false,
            qrcode: null
          });
        }
      })
  }
}

import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CeuChat } from 'src/app/models/ceu-chat';
import { ChatMessage } from 'src/app/models/chat-message';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DaoChatService {

    constructor(
        private aFirestore: AngularFirestore
    ) { }

    async createNewChat(eventId: string, ids, timestamp) {
        try {
            let docId = this.aFirestore.createId();
            let chat = new CeuChat;
            chat.uid = docId;
            chat.last_access = {}

            chat.last_access[ids[0]] = timestamp;
            chat.last_access[ids[1]] = timestamp;

            for (let index in ids) {
                chat.members[ids[index]] = Object.assign({}, { uid: ids[index] });
            }

            chat = Object.assign({}, chat);
            await this.aFirestore.collection('events').doc(eventId).collection('chats').doc(docId).set(chat);
            return (chat.uid);
        } catch (error) {
            return (null);
        }
    }

    sendMessage(eventId: string, chatId: string, message: ChatMessage): Promise<void> {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('chats').doc(chatId).collection('messages').doc();
        message.uid = ref.id;
        message = Object.assign({}, message);

        return ref.set(message);
    }

    /**
     * Update a message
     * @param eventId 
     * @param chatId 
     * @param messageId 
     * @param newMessage 
     */
    updateMessage(eventId: string, chatId: string, messageId: string, newMessage: ChatMessage) {
        return (this.aFirestore.collection('events').doc(eventId).collection('chats').doc(chatId).collection('messages').doc(messageId).update(newMessage));
    }

    getLastMessage(eventId: string, chatId: string, onResolve) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('chats').doc(chatId).collection('messages').orderBy('send_at', 'desc');

        ref
            .onSnapshot((response) => {
                let msgs = [];
                if (response.size >= 1) {
                    response.forEach(element => {
                        msgs.push(element.data());
                    });
                    onResolve(msgs[0]);
                } else {
                    onResolve(msgs);
                }
            })
    }

    getMessages(eventId: string, chatId: string) {
        return (this.aFirestore.collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .collection('messages', (ref) => ref.orderBy('send_at', 'asc')).snapshotChanges().pipe(
                map((snapshot) => {
                    return (snapshot.map((doc) => {
                        return (doc.payload.doc.data() as ChatMessage);
                    }))
                })
            ));
    }

    saveChatLastAccess(eventId: string, userId: string, chatId: string, timestamp: number) {
        let db = this.aFirestore.firestore;
        let ref = db.collection('events').doc(eventId).collection('chats').doc(chatId);
        let access = {};
        access[userId] = timestamp;

        const updates = {};
        updates[`last_access.${userId}`] = timestamp;

        ref.update(updates);
    }
}

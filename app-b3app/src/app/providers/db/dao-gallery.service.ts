import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class DaoGalleryService {

    constructor(private aFirestore: AngularFirestore) {
        AngularFirestoreModule.enablePersistence();
    }

    getFolder(moduleId: string, folderId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .collection('folders')
            .doc(folderId)
            .onSnapshot((result) => {
                onResolve(result.data());
            });
    }

    getFolders(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .collection('folders')
            .onSnapshot((values) => {
                let folders = [];
                if (values.size >= 1) {
                    values.forEach(element => {
                        folders.push(element.data());
                    });
                }
                onResolve(folders);
            });
    }

    getImages(moduleId: string, folderId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .collection('folders')
            .doc(folderId)
            .collection('images')
            .onSnapshot((values) => {
                let images = [];
                if (values.size >= 1) {
                    values.forEach(element => {
                        images.push(element.data());
                    });
                }
                onResolve(images);
            });
    }
}

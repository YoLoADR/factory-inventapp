import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { LuxonService } from '../luxon/luxon.service';


@Injectable({
  providedIn: 'root'
})

export class DaoBroadcastService {
  private db = this.aFirestore.firestore;


  constructor(
    private aFirestore: AngularFirestore,
    private luxon: LuxonService
  ) {
    AngularFirestoreModule.enablePersistence();
  }

  getModule(moduleId: string, onResolve) {
    const ref = this.db.collection('modules').doc(moduleId);

    ref
      .onSnapshot((data) => {
        onResolve(data.data());
      });
  }

  getBroadcastsList(moduleId: string, onResolve) {
    const ref = this.db.collection('modules').doc(moduleId).collection('broadcasts')
    .orderBy('createdAt', 'desc')

    ref.get().then((data) => {
      let listBroadcastsId = [];
      data.forEach(elem => {
        listBroadcastsId.push(elem.data());
      });

      onResolve(listBroadcastsId)
    })
  }

  createBroadcast(moduleId: string, userId: string, broadcastId: string, onResolve) {
    const ref = this.db.collection('modules').doc(moduleId).collection('broadcasts').doc(broadcastId);

    ref.set({
        uid: broadcastId,
        userId: userId,
        createdAt: this.luxon.getTimestampCurrentDate()
    }).then(() => {
        onResolve(true);
    }).catch(() => {
        onResolve(false);
    })
  }


}
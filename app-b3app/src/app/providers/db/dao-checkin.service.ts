import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { TypeModule } from 'src/app/enums/type-module';


@Injectable({
    providedIn: 'root'
})
export class DaoCheckinService {
    private refLoadCheckinGlobalOrderAsc = null
    private refLoadCheckinGlobalOrderDesc = null
    private refLoadCheckinGlobalOrderPresent = null
    private refLoadCheckinGlobalOrderAway = null

    private refLoadCheckinGroupVisionOrderAsc = []
    private refLoadCheckinGroupVisionOrderDesc = []
    private refLoadCheckinGroupVisionOrderPresent = []
    private refLoadCheckinGroupVisionOrderAway = []


    private refGetCheckin = null
    private refLoadCheckinGlobalAllAttendees = null
    private refLoadCheckinGroupVisionAllAttendees = []
    private refGetModule = null

    constructor(private aFirestore: AngularFirestore) {
        AngularFirestoreModule.enablePersistence();
    }

    /**
    * get module. 
    * @param moduleId 
    * @returns onResolve
    */

    getModule(moduleId: string, onResolve) {
        let db = this.aFirestore

        this.refGetModule = db.collection('modules')
            .doc(moduleId)
            .valueChanges()
            .subscribe((module) => {
                onResolve(module)
            })
    }

    /**
    * close getModule() function reference 
    */
    closeRefGetModule() {
        if (this.refGetModule)
            this.refGetModule.unsubscribe()
    }

    getCheckins(moduleId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .doc(moduleId)
            .collection('checkins')
            .orderBy('order', 'asc')
            .onSnapshot((values) => {
                let checkins = [];
                if (values.size >= 1) {
                    values.forEach(element => {
                        checkins.push(element.data());
                    });
                }
                onResolve(checkins);
            })
    }

    getModuleCheckinAndCheckin(checkId: string, eventId: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('modules')
            .where('eventId', '==', eventId)
            .where('type', '==', TypeModule.CHECKIN)
            .get()
            .then((snapshot) => {
                if (snapshot.size >= 1) {
                    snapshot.forEach(element => {
                        let module = element.data();
                        this.getCheckinPromise(checkId, module.uid)
                            .then((res) => {
                                onResolve(res);
                            })
                    });
                }
            })
    }



    getCheckinPromise(checkinId: string, moduleId: string) {
        return new Promise((resolve, reject) => {
            let db = this.aFirestore.firestore;

            db
                .collection('modules')
                .doc(moduleId)
                .collection('checkins')
                .doc(checkinId)
                .onSnapshot((checkin) => {
                    resolve(checkin.data());
                });
        })
    }

    /**
      get checkin.
    * @param checkinId 
    * @param checkinModuleId
    * @returns onResolve
    */
    getCheckin(checkinId: string, checkinModuleId: string, onResolve) {
        let db = this.aFirestore

        this.refGetCheckin = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .valueChanges()
            .subscribe((snapshot) => {
                onResolve(snapshot)
            })
    }

    /**
     * Close closeRefLoadCheckinGlobalOrderAsc() function reference. 
     */
    closeRefGetCheckin() {
        if (this.refGetCheckin)
            this.refGetCheckin.unsubscribe()
    }


    /**
      load all attendees checkin vision global .
    * @param checkinId 
    * @param checkinModuleId
    * @returns onResolve
    */
    loadCheckinGlobalAllAttendees(checkinId: string, checkinModuleId: string, onResolve) {
        let db = this.aFirestore

        this.refLoadCheckinGlobalAllAttendees = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .valueChanges()
            .subscribe((snapshot) => {
                onResolve(snapshot)
            })
    }

    /**
    * Close refLoadCheckinGlobalAllAttendees() function reference. 
    */
    closeRefLoadCheckinGlobalAllAttendees() {
        if (this.refLoadCheckinGlobalAllAttendees)
            this.refLoadCheckinGlobalAllAttendees.unsubscribe()
    }

    /**
    * loads check in with global vision and asc order.. 
    * @param checkinId 
    * @param checkinModuleId
    * @returns onResolve
    */

    loadCheckinGlobalOrderAsc(checkinId: string, checkinModuleId: string, onResolve) {
        let db = this.aFirestore
        let attendees = [];

        //a-z
        this.refLoadCheckinGlobalOrderAsc = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees', ref => ref.orderBy('queryName', 'asc'))
            .valueChanges()
            .subscribe((snapshot) => {
                attendees = [];
                snapshot.forEach((element) => {
                    attendees.push(element)
                })

                onResolve(attendees)
            })
    }

    /**
    * Close closeRefLoadCheckinGlobalOrderAsc() function reference. 
    */
    closeRefLoadCheckinGlobalOrderAsc() {
        if (this.refLoadCheckinGlobalOrderAsc)
            this.refLoadCheckinGlobalOrderAsc.unsubscribe()
    }

    /**
    * loads check in with global vision and desc order.. 
    * @param checkinId 
    * @param checkinModuleId
    * @returns onResolve
    */

    loadCheckinGlobalOrderDesc(checkinId: string, checkinModuleId: string, onResolve) {
        let db = this.aFirestore
        let attendees = [];

        //z-a
        this.refLoadCheckinGlobalOrderDesc = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees', ref => ref.orderBy('queryName', 'desc'))
            .valueChanges()
            .subscribe((snapshot) => {
                attendees = [];
                snapshot.forEach((element) => {
                    attendees.push(element)
                })

                onResolve(attendees)
            })
    }


    /**
    * Close refLoadCheckinGlobalOrderDesc() function reference. 
    */
    closeRefLoadCheckinGlobalOrderDesc() {
        if (this.refLoadCheckinGlobalOrderDesc)
            this.refLoadCheckinGlobalOrderDesc.unsubscribe()
    }

    /**
  * loads check in with global vision and present order.. 
  * @param checkinId 
  * @param checkinModuleId
  * @returns onResolve
  */

    loadCheckinGlobalOrderPresent(checkinId: string, checkinModuleId: string, onResolve) {
        let db = this.aFirestore
        let attendees = [];

        this.refLoadCheckinGlobalOrderAway = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees', ref => ref.where('checkinStatus', '==', true).orderBy('queryName', 'asc'))
            .valueChanges()
            .subscribe((snapshot) => {
                attendees = [];
                snapshot.forEach((element) => {
                    attendees.push(element)
                })

                onResolve(attendees)
            })
    }

    /**
    * Close closeRefLoadCheckinGlobalOrderAway() function reference. 
    */
    closeRefLoadCheckinGlobalOrderAway() {
        if (this.refLoadCheckinGlobalOrderAway)
            this.refLoadCheckinGlobalOrderAway.unsubscribe()
    }

    /**
  * loads check in with global vision and away order.. 
  * @param checkinId 
  * @param checkinModuleId
  * @returns onResolve
  */

    loadCheckinGlobalOrderAway(checkinId: string, checkinModuleId: string, onResolve) {
        let db = this.aFirestore
        let attendees = [];

        this.refLoadCheckinGlobalOrderPresent = db.collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees', ref => ref.where('checkinStatus', '==', false).orderBy('queryName', 'asc'))
            .valueChanges()
            .subscribe((snapshot) => {
                attendees = [];
                snapshot.forEach((element) => {
                    attendees.push(element)
                })

                onResolve(attendees)
            })
    }

    /**
    * Close refLoadCheckinGlobalOrderPresent() function reference. 
    */
    closeRefLoadCheckinGlobalOrderPresent() {
        if (this.refLoadCheckinGlobalOrderPresent)
            this.refLoadCheckinGlobalOrderPresent.unsubscribe()
    }

    /**
    * load all attendees checkin vision group .
    * @param checkinId 
    * @param checkinModuleId
    * @param groups
    * @returns onResolve
    */
    loadCheckinGroupVisionAllAttendees(checkinId: string, checkinModuleId: string, groups: Array<any>, onResolve) {
        let db = this.aFirestore
        let aux = []
        let attendees = []
        let contGroup = 0

        if (groups.length <= 0)
            onResolve([])

        for (let i = 0; i < groups.length; i++) {
            const group = groups[i]
            const group_uid = `groups.${group.uid}`

            this.refLoadCheckinGroupVisionAllAttendees[i] = db.collection('modules')
                .doc(checkinModuleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees', ref => ref.orderBy(group_uid))
                .valueChanges()
                .subscribe((snapshot) => {
                    contGroup++

                    snapshot.forEach((attendee) => {
                        // Checks if the participant is already in the array.
                        const pos = aux.map(function (e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1) {
                            aux.push(attendee)
                        } else {
                            aux[pos] = attendee
                        }
                    })

                    // participants from all groups were sued.
                    if (contGroup >= groups.length) {
                        attendees = aux.sort(function (a, b) {
                            if (a['queryName'] < b['queryName']) { return -1; }
                            if (a['queryName'] > b['queryName']) { return 1; }
                            return 0;
                        });

                        onResolve(attendees)
                    }
                })
        }
    }


    /**
    * Close loadCheckinGroupVisionAllAttendees() function reference. 
    */
    closeRefLoadCheckinGroupVisionAllAttendees() {
        if (this.refLoadCheckinGroupVisionAllAttendees.length > 0) {
            for (const ref of this.refLoadCheckinGroupVisionAllAttendees) {
                ref.unsubscribe()
            }
        }
    }

    /**
    * loads check-in with group view and asc order.. 
    * @param checkinId 
    * @param checkinModuleId
    * @param groups
    * @returns onResolve
    */
    loadCheckinGroupVisionOrderAsc(checkinId: string, checkinModuleId: string, groups: Array<any>, onResolve) {
        let db = this.aFirestore
        let aux = []
        let attendees = []
        let contGroup = 0

        if (groups.length <= 0)
            onResolve([])

        for (let i = 0; i < groups.length; i++) {
            const group = groups[i]
            const group_uid = `groups.${group.uid}`

            this.refLoadCheckinGroupVisionOrderAsc[i] = db.collection('modules')
                .doc(checkinModuleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees', ref => ref.orderBy(group_uid))
                .valueChanges()
                .subscribe((snapshot) => {
                    contGroup++

                    snapshot.forEach((attendee) => {
                        // Checks if the participant is already in the array.
                        const pos = aux.map(function (e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1) {
                            aux.push(attendee)
                        } else {
                            aux[pos] = attendee
                        }
                    })

                    // participants from all groups were sued.
                    if (contGroup >= groups.length) {
                        attendees = aux.sort(function (a, b) {
                            if (a['queryName'] < b['queryName']) { return -1; }
                            if (a['queryName'] > b['queryName']) { return 1; }
                            return 0;
                        });

                        onResolve(attendees)
                    }
                })
        }
    }

    /**
  * Close loadCheckinGroupVisionOrderAsc() function reference. 
  */
    closeRefLoadCheckinGroupVisionOrderAsc() {
        if (this.refLoadCheckinGroupVisionOrderAsc.length > 0) {
            for (const ref of this.refLoadCheckinGroupVisionOrderAsc) {
                ref.unsubscribe()
            }
        }
    }

    /**
   * loads check-in with group view and desc order.. 
   * @param checkinId 
   * @param checkinModuleId
   * @param groups
   * @returns onResolve
   */
    loadCheckinGroupVisionOrderDesc(checkinId: string, checkinModuleId: string, groups: Array<any>, onResolve) {
        let db = this.aFirestore
        let aux = []
        let attendees = []
        let contGroup = 0

        if (groups.length <= 0)
            onResolve([])

        for (let i = 0; i < groups.length; i++) {
            const group = groups[i]
            const group_uid = `groups.${group.uid}`

            this.refLoadCheckinGroupVisionOrderDesc[i] = db.collection('modules')
                .doc(checkinModuleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees', ref => ref.orderBy(group_uid))
                .valueChanges()
                .subscribe((snapshot) => {
                    contGroup++

                    snapshot.forEach((attendee) => {
                        // Checks if the participant is already in the array.
                        const pos = aux.map(function (e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1) {
                            aux.push(attendee)
                        } else {
                            aux[pos] = attendee
                        }
                    })

                    // participants from all groups were sued.
                    if (contGroup >= groups.length) {
                        attendees = aux.sort(function (a, b) {
                            if (a['queryName'] < b['queryName']) { return 1; }
                            if (a['queryName'] > b['queryName']) { return -1; }
                            return 0;
                        });

                        onResolve(attendees)
                    }
                })
        }
    }

    /**
    * Close loadCheckinGroupVisionOrderDesc() function reference. 
    */
    closeRefLoadCheckinGroupVisionOrderDesc() {
        if (this.refLoadCheckinGroupVisionOrderDesc.length > 0) {
            for (const ref of this.refLoadCheckinGroupVisionOrderDesc) {
                ref.unsubscribe()
            }
        }
    }


    /**
   * loads check-in with group view and present order.. 
   * @param checkinId 
   * @param checkinModuleId
   * @param groups
   * @returns onResolve
   */
    loadCheckinGroupVisionOrderPresent(checkinId: string, checkinModuleId: string, groups: Array<any>, onResolve) {
        let db = this.aFirestore
        let aux = []
        let attendees = []
        let contGroup = 0

        if (groups.length <= 0)
            onResolve([])

        for (let i = 0; i < groups.length; i++) {
            const group = groups[i]
            const group_uid = `groups.${group.uid}`

            this.refLoadCheckinGroupVisionOrderPresent[i] = db.collection('modules')
                .doc(checkinModuleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees', ref => ref.orderBy(group_uid))
                .valueChanges()
                .subscribe((snapshot) => {
                    contGroup++

                    snapshot.forEach((attendee) => {
                        // Checks if the participant is already in the array.
                        const pos = aux.map(function (e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1 && attendee.checkinStatus) {
                            aux.push(attendee)
                        } else if (pos > -1 && attendee.checkinStatus) {
                            aux[pos] = attendee
                        } else if (pos > -1 && !attendee.checkinStatus) {
                            aux.splice(pos, 1);
                        }
                    })

                    // participants from all groups were sued.
                    if (contGroup >= groups.length) {
                        attendees = aux.sort(function (a, b) {
                            if (a['queryName'] < b['queryName']) { return -1; }
                            if (a['queryName'] > b['queryName']) { return 1; }
                            return 0;
                        });

                        onResolve(attendees)
                    }
                })
        }
    }
    /**
    * Close closeRefLoadCheckinGroupVisionOrderPresent() function reference. 
    */
    closeRefLoadCheckinGroupVisionOrderPresent() {
        if (this.refLoadCheckinGroupVisionOrderPresent.length > 0) {
            for (const ref of this.refLoadCheckinGroupVisionOrderPresent) {
                ref.unsubscribe()
            }
        }
    }


    /**
     * loads check-in with group view and away order.. 
     * @param checkinId 
     * @param checkinModuleId
     * @param groups
     * @returns onResolve
     */
    loadCheckinGroupVisionOrderAway(checkinId: string, checkinModuleId: string, groups: Array<any>, onResolve) {
        let db = this.aFirestore
        let aux = []
        let attendees = []
        let contGroup = 0

        if (groups.length <= 0)
            onResolve([])

        for (let i = 0; i < groups.length; i++) {
            const group = groups[i]
            const group_uid = `groups.${group.uid}`

            this.refLoadCheckinGroupVisionOrderAway[i] = db.collection('modules')
                .doc(checkinModuleId)
                .collection('checkins')
                .doc(checkinId)
                .collection('attendees', ref => ref.orderBy(group_uid))
                .valueChanges()
                .subscribe((snapshot) => {
                    contGroup++

                    snapshot.forEach((attendee) => {
                        // Checks if the participant is already in the array.
                        const pos = aux.map(function (e) { return e.uid; }).indexOf(attendee.uid);

                        if (pos === -1 && !attendee.checkinStatus) {
                            aux.push(attendee)
                        } else if (pos > -1 && !attendee.checkinStatus) {
                            aux[pos] = attendee
                        } else if (pos > -1 && attendee.checkinStatus) {
                            aux.splice(pos, 1);
                        }
                    })

                    // participants from all groups were sued.
                    if (contGroup >= groups.length) {
                        attendees = aux.sort(function (a, b) {
                            if (a['queryName'] < b['queryName']) { return -1; }
                            if (a['queryName'] > b['queryName']) { return 1; }
                            return 0;
                        });

                        onResolve(attendees)
                    }
                })
        }
    }
    /**
    * Close loadCheckinGroupVisionOrderAway() function reference. 
    */
    closeRefLoadCheckinGroupVisionOrderAway() {
        if (this.refLoadCheckinGroupVisionOrderAway.length > 0) {
            for (const ref of this.refLoadCheckinGroupVisionOrderAway) {
                ref.unsubscribe()
            }
        }
    }

    /**
     * Find Attendee Exist In Event. 
     */

    findAttendeeExistInEvent(eventId: string, uid: string, onResolve) {
        let db = this.aFirestore.firestore;

        db
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .where('uid', '==', uid)
            .get()
            .then((values) => {
                if (values.size >= 1) {
                    values.forEach(element => {
                        onResolve({
                            status: true,
                            user: element.data()
                        })
                    });
                } else {
                    onResolve({
                        status: false,
                        user: null
                    });
                }
            })
            .catch((e) => { console.log(e) });
    }


    /**
    * updates the participant's status at check in.
    * @param status
    * @param checkinId  
    * @param checkinModuleId
    * @param attendeeId
    * @returns onResolve
    */
    changeAttendeeStatus(status: boolean, checkinId: string, checkinModuleId: string, attendeeId: string, moduleAttendeeId: string, eventId: string, onResolve) {
        let db = this.aFirestore.firestore;

        let attendeeEventRef = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId)
        let attendeeModuleRef = db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId)

        attendeeEventRef.update({ checkinStatus: status })
        attendeeModuleRef.update({ checkinStatus: status })

        db
            .collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .doc(attendeeId)
            .update({
                checkinStatus: status,
                time: Date.now()
            })
            .then(() => {
                onResolve(true)
            })
            .catch((error) => {
                onResolve(false)
            })
    }

    /**
     * Validate a checkin for an attendee
     * @param status 
     * @param checkinId 
     * @param checkinModule 
     * @param attendeeId 
     * @param moduleAttendeeId 
     * @param eventId 
     * @param checkins 
     */
    validateCheckinForAttendee(status: boolean, checkinId: string, checkinModuleId: string, attendeeId: string, moduleAttendeeId: string, eventId: string, checkins: string[], onResolve) {
        let db = this.aFirestore.firestore;

        let attendeeEventRef = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId)
        let attendeeModuleRef = db.collection('modules').doc(moduleAttendeeId).collection('attendees').doc(attendeeId)

        attendeeEventRef.update({ checkinStatus: status, checkins: checkins })
        attendeeModuleRef.update({ checkinStatus: status, checkins: checkins })

        db
            .collection('modules')
            .doc(checkinModuleId)
            .collection('checkins')
            .doc(checkinId)
            .collection('attendees')
            .doc(attendeeId)
            .update({
                checkinStatus: status,
                checkins: checkins,
                time: Date.now(),
            })
            .then(() => {
                onResolve(true)
            })
            .catch((error) => {
                onResolve(false)
            })
    }

    changeOrderAttendees(moduleId, checkinId, typeOrder, onResolve) {
        let db = this.aFirestore.firestore;

        let ref = db.collection('modules').doc(moduleId).collection('checkins').doc(checkinId);

        ref.update({ typeOrder: typeOrder })
            .then(() => {
                onResolve(true);
            })
            .catch((e) => {
                onResolve(false);
            })
    }
}

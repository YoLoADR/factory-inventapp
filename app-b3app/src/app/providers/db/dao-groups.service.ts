import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { TypeModule } from '../../models/type-module'
import { NameGroup } from 'src/app/enums/name-group';

@Injectable({
    providedIn: 'root'
})

export class DaoGroupsService {
    private refGetGroups = null
    private refContSessionsOfGroup = null
    private refContAttendeesOfGroup = null
    private refContSpeakersOfGroup = null



    constructor(private aFirestore: AngularFirestore) {
        AngularFirestoreModule.enablePersistence();
    }

    // loads the event groups.
    getGroups(eventId: string, onResolve) {
        // search for group type modules.
        this.refGetGroups = this.aFirestore
            .collection('events')
            .doc(eventId)
            .collection('modules', ref => ref.where('type', '==', TypeModule.MANAGER_GROUP))
            .valueChanges()
            .subscribe((modules) => {
                if (modules.length > 0) {
                    const module = modules[0]
                    const moduleId = module.uid

                    // search the groups in the module.
                    this.aFirestore.collection('modules').doc(moduleId).collection('groups').valueChanges().subscribe((groups) => {
                        onResolve(groups)
                    })
                } else {
                    onResolve([])
                }
            })
    }

    closeRefGetGroups() {
        if (this.refGetGroups)
            this.refGetGroups.unsubscribe()
    }

    // account the number of sessions that the group has from the schedule module, passed in the parameter.
    contSessionsOfGroup(groupId: string, moduleScheduleId: string, onResolve) {
        const group = `groups.${groupId}`

        this.refContSessionsOfGroup = this.aFirestore.
            collection('modules')
            .doc(moduleScheduleId)
            .collection('sessions', ref => ref.orderBy(group))
            .valueChanges()
            .subscribe((data) => {
                const size = data.length
                onResolve(size)
            })
    }

    closeRefContSessionsOfGroup() {
        if (this.refContSessionsOfGroup)
            this.refContSessionsOfGroup.unsubscribe()
    }


    // count the number of participants the group has in the module.
    contAttendeesOfGroup(groupId: string, moduleAttendeeId: string, onResolve) {
        const group = `groups.${groupId}`

        this.refContAttendeesOfGroup = this.aFirestore.
            collection('modules')
            .doc(moduleAttendeeId)
            .collection('attendees', ref => ref.orderBy(group))
            .valueChanges()
            .subscribe((data) => {
                const size = data.length
                onResolve(size)
            })
    }

    closeRefContAttendeesOfGroup() {
        if (this.refContAttendeesOfGroup)
            this.refContAttendeesOfGroup.unsubscribe()
    }

     // count the number of speakers the group has in the module.
     contSpeakersOfGroup(groupId: string, moduleSpeakerId: string, onResolve) {
        const group = `groups.${groupId}`

        this.refContSpeakersOfGroup = this.aFirestore.
            collection('modules')
            .doc(moduleSpeakerId)
            .collection('speakers', ref => ref.orderBy(group))
            .valueChanges()
            .subscribe((data) => {
                const size = data.length
                onResolve(size)
            })
    }

    closeRefContSpeakersOfGroup() {
        if (this.refContSpeakersOfGroup)
            this.refContSpeakersOfGroup.unsubscribe()
    }



    /**
   * 
get the group with language using identifier.
   * 
   *  @param identifier group identifier
   *  @param eventId uid of the group event.

   */

    groupLanguage(identifier: string, eventId: string) {
        return new Promise(async (resolve) => {
            switch (identifier) {
                case 'de_DE':
                    identifier = NameGroup.DeDE
                    break;
                case 'en_US':
                    identifier = NameGroup.EnUS
                    break;
                case 'es_ES':
                    identifier = NameGroup.EsES
                    break;
                case 'fr_FR':
                    identifier = NameGroup.FrFR
                    break;
                default:
                    identifier = NameGroup.PtBR
                    break;
            }


            this.aFirestore
                .collection('events')
                .doc(eventId)
                .collection('modules', ref => ref.where('type', '==', TypeModule.MANAGER_GROUP))
                .valueChanges()
                .subscribe((modules) => {
                    if (modules.length > 0) {
                        const module = modules[0]
                        const moduleId = module.uid

                        // search the groups in the module.
                        this.aFirestore.collection('modules').doc(moduleId).collection('groups', ref => ref.where('identifier', '==', identifier)).valueChanges().subscribe((groups) => {
                            let group = null

                            for (const g of groups) {
                                group = g
                            }

                            resolve(group)
                        })
                    } else {
                        resolve([])
                    }
                })
        })
    }

    // transforma o array de grupos em json 
    public transformArrayToJson(groups: Array<any>) {
        let json = {}

        for (const group of groups) {
            const groupId = group.uid
            json[groupId] = group
        }

        return json
    }

}
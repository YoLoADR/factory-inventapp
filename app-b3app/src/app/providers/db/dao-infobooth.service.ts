import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class DaoInfoboothService {
    private db = this.aFirestore.firestore;

    constructor(
        private aFirestore: AngularFirestore,
    ) {
        AngularFirestoreModule.enablePersistence();
    }

    getModule(moduleId: string, onResolve) {
        const ref = this.db.collection('modules').doc(moduleId);

        ref.onSnapshot((data) => {
            onResolve(data.data());
        });
    }

    // loads the page content.
    getPage(moduleId: string, pageId: string, onResolve) {
        const ref = this.db.collection('modules').doc(moduleId).collection('pages').doc(pageId)

        ref.onSnapshot((snapshot) => {
            let page = snapshot.data()
            onResolve(page)
        })
    }

}

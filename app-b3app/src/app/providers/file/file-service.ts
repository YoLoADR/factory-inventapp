import { Injectable } from '@angular/core';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Platform } from '@ionic/angular';
import { UUID } from 'angular2-uuid';
import { File } from '@ionic-native/file/ngx';

interface FileServiceDownloadOptions {
    name?: string;
    directory?: string;
}

@Injectable()
export class FileService {

    constructor(
        private fileTransfer: FileTransfer,
        private platform: Platform,
        private file: File,

    ) { }

    async download(url: string, extension: string, options: FileServiceDownloadOptions) {
        let platforms = this.platform.platforms().join();

        options.name = (options.name || UUID.UUID());
        options.directory = (options.directory || '_downloads');


        if (platforms.includes('hybrid') && platforms.includes('cordova')) {
            if (platforms.includes('android')) {
                await this.downloadAndroid(url, extension, options);
            }
            else if (platforms.includes('ios')) {
                await this.downloadIos(url,extension, options)
            }
        }
    }

    private async downloadAndroid(url: string, extension: string, options: FileServiceDownloadOptions) {
        try {
            await this.file.createDir(this.file.externalRootDirectory, options.directory, false);
        }
        catch { }

        const fileTransfer = this.fileTransfer.create();
        const response = await fileTransfer.download(url, `${this.file.externalRootDirectory}/${options.directory}/${options.name}.${extension}`);
        
        return response;
    }

    private async downloadIos(url: string, extension:string, options: FileServiceDownloadOptions) {
         // ********* IMPORTANT: IOS NEED TO CHANGE EXTERNALROOTDIRECTORY TO OTHER PATH ****** //
            // this.file.createDir(this.file.dataDirectory, '_downloads', false).then(response => {
            // console.log('Directory created', response);
            try {
                await this.file.createDir(this.file.externalRootDirectory, options.directory, false);
            }
            catch { }
    
            const fileTransfer = this.fileTransfer.create();
            const response = await fileTransfer.download(url, `${this.file.dataDirectory}/${options.directory}/${options.name}.${extension}`);
            
            return response;
    }
}

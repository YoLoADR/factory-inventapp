import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { first, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class NotificationsService {
    headers: HttpHeaders;
    apiUrl: string;

    constructor(private afs: AngularFirestore, private http: HttpClient) {
        this.headers = new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: environment.onesignal.notification_api_id
        });

        this.apiUrl = 'https://onesignal.com/api/v1/notifications';
    }

    async sendNotification(
        eventId: string,
        message: string,
        sender_name: string,
        userIds: string[]
    ) {
        const promises = userIds.map((uid) => this.getPlayerId(eventId, uid));
        const include_player_ids = (await Promise.all(promises)).filter(
            (p) => !!p
        );

        console.log(include_player_ids);

        if (!include_player_ids.length) return;

        return this.http
            .post(
                this.apiUrl,
                {
                    include_player_ids,
                    headings: {
                        en: sender_name
                    },
                    contents: {
                        en: message
                    },
                    priority: 10,
                    api_id: environment.onesignal.notification_api_id,
                    app_id: environment.onesignal.onesignal_appid
                },
                {
                    headers: this.headers,
                    observe: 'response'
                }
            )
            .toPromise();
    }

    getPlayerId(eventId: string, uid: string) {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('attendees')
            .doc(uid)
            .valueChanges()
            .pipe(
                first(),
                map((a: any) => (a.notification ? a.notification.userId : null))
            )
            .toPromise();
    }
}

import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { retry } from 'rxjs/operators';

@Injectable()

export class BambuserService {

    private APPLICATION_ID: string = environment.platform.bambuserApplicationId;
    private READONLY_API_KEY: string = environment.platform.bambuserApiKey;
    private BaseUrl: string = 'https://api.bambuser.com/';

    public headers;
    public requestOptions;
    
    constructor(
        private http: HttpClient
    ) {
        this.headers = { headers: 
            { 
                Accept: 'application/vnd.bambuser.v1+json', 
                Authorization: 'Bearer ' + this.READONLY_API_KEY 
            }
        };
    }

    getBroadcasts(moduleId): Observable<any> {
        return this.http.get(this.BaseUrl + 'broadcasts?byAuthors=' + moduleId, this.headers)
        .pipe(
            retry(2)
        )
    }

    getBroadcast(broadcastId): Observable<any> {
        return this.http.get(this.BaseUrl + 'broadcasts/' + broadcastId, this.headers)
        .pipe(
            retry(2)
        )
    }
}
import { TestBed } from '@angular/core/testing';

import { StorageOfflineService } from './storage-offline.service';

describe('StorageOfflineService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorageOfflineService = TestBed.get(StorageOfflineService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit, NgZone } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { DaoEventsService } from '../providers/db/dao-events.service';
import { AuthService } from '../providers/authentication/auth.service';
import { Events, MenuController, NavController } from '@ionic/angular';
import { LuxonService } from '../providers/luxon/luxon.service';

@Component({
    selector: 'app-admin-events',
    templateUrl: './admin-events.page.html',
    styleUrls: ['./admin-events.page.scss'],
    providers: [DaoEventsService]
})
export class AdminEventsPage implements OnInit {
    events;

    searchOpen = false;
    container: any;
    loader: boolean = true;
    constructor(
        public global: GlobalService,
        private daoEvents: DaoEventsService,
        private auth: AuthService,
        private eventsA: Events,
        private menuCtrl: MenuController,
        private luxon: LuxonService,
        private navCtrl: NavController,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(false);
    }

    ngOnInit() {
        this.loadEvents();
    }

    loadEvents() {
        this.daoEvents.getAllEvents((data) => {
            this.zone.run(() => {
                // makes the treatment of the startDate of the event.
                for (const event of data['result']) {
                    if (typeof event.startDate == 'number') {
                        const sDate = this.luxon.convertTimestampToDate(event.startDate)
                        event.startDate = this.luxon.convertDateToFormatDDMM(sDate)
                    }
                    if (event.banner == '' || event.banner == null || event.banner == undefined) {
                        event.banner = {
                            uid: '',
                            url: ''
                        }
                    }
                }

                this.events = data['result'];
                this.loader = false;
            })
        });
    }

    logOut() {
        this.auth.logout();
    }

    openEvent(event) {
        this.loader = true;
        this.menuCtrl.enable(true);
        this.global.previousPage = 'container';
        localStorage.setItem('eventId', event.uid);
        localStorage.setItem('homePage', event.homePage);
        this.global.eventHomePage = event.homePage;
        this.global.loadService(() => {
            this.navCtrl.navigateRoot([`${event.homePage}`])
                .then((_) => {
                    this.eventsA.publish('updateScreen');
                    this.loader = false;
                });
        });

    }

}

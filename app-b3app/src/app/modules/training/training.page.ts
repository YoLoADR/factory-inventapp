import { Component, OnInit, ViewChild, ViewEncapsulation, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { MenuController, Events, AlertController, ToastController } from '@ionic/angular';
import { DaoTrainingService } from 'src/app/providers/db/dao-training.service';
import { Training } from 'src/app/models/training';
import { NotificationDateService } from 'src/app/providers/date/notification-date.service';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { environment } from 'src/environments/environment';
import { DaoInteractivityService } from 'src/app/providers/db/dao-interactivity.service';

@Component({
    selector: 'app-training',
    templateUrl: './training.page.html',
    styleUrls: ['./training.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TrainingPage implements OnInit {

    @ViewChild('mySlider', { static: false }) mySlider: any;

    eventId: string = null;
    moduleInteractivity = null;
    moduleId: string = null;
    trainingId: string = null;
    sessionId: string = null;
    userId: string = null;
    scheduleModuleId: string = null;
    training: Training = null;
    result: Array<any> = [];
    resultVerify: Array<any> = [];
    setResultVerify: boolean = false;
    totalQuestions: number = null;

    rate: number;
    ratingValue;

    indexSlide: number = 0;
    counter: number = 0;
    public interval: any;
    progress: number = 0;

    previousPage;
    view_only: boolean = null;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    todayDate = new Date();

    checkedView = false;
    totalUserAnswers: boolean = false;
    loader: boolean = true;
    allowButtonsHeader: boolean = false;
    userLanguage: string = environment.platform.defaultLanguage;

    constructor(private route: ActivatedRoute,
        private router: Router,
        public global: GlobalService,
        private menuCtrl: MenuController,
        private events: Events,
        private daoInteractivity: DaoInteractivityService,
        private daoTraining: DaoTrainingService,
        private daoAttendee: DaoAttendeesService,
        public alertCtrl: AlertController,
        public alertController: AlertController,
        public toastController: ToastController,
        private notificationDate: NotificationDateService,
        private location: Location,
        private translateService: TranslateService,
        private keyboard: Keyboard,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.trainingId = params.trainingId;
            // this.trainingId = this.route.snapshot.params['trainingId'];

            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });

            this.route.queryParams.subscribe(params => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.previousPage = this.router.getCurrentNavigation().extras.state.previousPage;
                    this.scheduleModuleId = this.router.getCurrentNavigation().extras.state.scheduleModuleId;
                    this.sessionId = this.router.getCurrentNavigation().extras.state.sessionId;
                }
            });
        })
    }

    ngOnInit() {
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.userLanguage = this.convertLangFormat(this.global.language);
    }

    ionViewDidEnter() {
        this.daoInteractivity.getInteractivityModule(this.eventId, (module) => {
            this.moduleInteractivity = module;
            this.getTraining();
        })
    }

    ngAfterViewChecked() {
        if (!this.checkedView) {
            if (this.mySlider !== undefined) {
                this.mySlider.lockSwipes(true)
                this.checkedView = true;
            }
        }
    }

    ionViewWillLeave() {
        this.indexSlide = 0;
        this.counter = 0;
        this.progress = 0;
        clearInterval(this.interval);

    }

    progressBar() {
        let activeIndex = this.indexSlide;
        let totalSlides = this.training.questions.length * 2;

        let aux = (activeIndex + 1) * (100 / totalSlides)
        this.progress = Number(aux.toFixed(2));
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    getTraining() {
        if (this.global.userLoaded || this.moduleInteractivity.answerOffline) {
            this.daoTraining.getTotalQuestions(this.moduleId, this.trainingId, (total) => {
                this.totalQuestions = total;

                this.daoTraining.getTraining(this.moduleId, this.trainingId, (training) => {
                    this.training = training;

                    let cont = 0;
                    for (let question of this.training.questions) {
                        let visibility = question.visibility;

                        if (!visibility) {
                            this.training.questions.splice(cont, 1);
                        }

                        cont++;
                    }

                    this.loader = false;

                    if (!this.training.change_answer && this.training.view_answered) {
                        this.view_only = true;
                    } else {
                        this.view_only = false;
                    }

                    this.startCountdown();
                    this.progressBar();
                    this.userId = this.global.userId;
                    this.setResult();
                });
            })
        } else {
            this.global.userLogged();
            clearInterval(this.interval);
            this.location.back();
        }
    }


    setResult() {
        this.resultVerify = new Array(this.training.questions.length).fill(null);

        for (let i = 0; i < this.training.questions.length * 2; i++) {
            if (i % 2 == 0) {
                if (this.training.questions[i / 2].type == 'multipleSelect') {
                    this.result[i] = new Array(this.training.questions[i / 2].answers.length).fill(null);
                } else {
                    this.result[i] = null;
                }
            } else {
                this.result[i] = null;
            }
        }


        this.setResultVerify = true;

        if (this.global.userLoaded) {
            this.daoTraining.getResponseUsers(this.moduleId, this.trainingId, this.userId, (responses) => {

                let cont = 0;
                for (let question of this.training.questions) {
                    if (responses[question.uid] != undefined) {
                        this.resultVerify[cont] = responses[question.uid].verifyQuestion;
                    }

                    if (responses[question.uid] == undefined) {
                        question.answered = false;
                    } else {
                        question.answered = true;

                        if (question.type == 'multipleSelect') {
                            for (let answerId of responses[question.uid].answer) {
                                let contAnswer = 0;
                                for (let answer of this.training.questions[cont].answers) {
                                    if (answerId == answer.uid) {
                                        this.result[cont * 2][contAnswer] = true;
                                    }

                                    contAnswer++;
                                }
                            }
                        } else {
                            for (let answer of this.training.questions[cont].answers) {
                                if (answer.uid == responses[question.uid].answer) {
                                    this.result[cont * 2] = answer.uid;
                                }
                            }
                        }
                    }

                    cont++;
                }
            })
        }
    }

    setEvaluation($event: { newValue: number }, index) {
        this.result[index] = $event.newValue;
    }

    dateSelected($event, index) {
        if (this.training.questions[index].answered == false) {
            let date = new Date($event);
            let timestamp = this.notificationDate.getTimeStampFromDateNow(date, this.global.event.timezone);
            this.result[index] = timestamp;
        } else {
            this.presentToastDate();
        }
    }

    async presentToastDate() {
        const toast = await this.toastController.create({
            message: this.translateService.instant('global.alerts.date_already_selected'),
            duration: 2000
        });

        toast.present();
    }

    async presentToast(answered) {
        if (answered && this.training.change_answer == false) {
            const toast = await this.toastController.create({
                message: this.translateService.instant('global.alerts.not_change_answer'),
                duration: 2000
            });

            toast.present();
        }
    }

    setVerifyAnswer(index, answer) {
        this.result[index] = answer;
        this.resultVerify[(index - 1) / 2] = answer;
    }

    async sendTraining(question, indexQuestion, indexVerifyQuestion) {
        this.totalUserAnswers = true;
        clearInterval(this.interval);

        if (this.global.userLoaded || this.moduleInteractivity.answerOffline) {

            for (let i = 0; i <= this.training.questions.length - 1; i++) {
                if (this.training.questions[i].type == 'multipleSelect') {
                    let findAnswer = false;
                    for (let result of this.result[i * 2]) {
                        if (result) {
                            findAnswer = true;
                        }
                    }

                    if (!findAnswer) {
                        this.totalUserAnswers = false;
                    }
                } else {
                    if (this.result[i * 2] == null) {
                        this.totalUserAnswers = false;
                    }
                }
            }

            if (this.totalQuestions > this.training.questions.length) {
                this.totalUserAnswers = false;
            }


            let timestamp = Date.now() / 1000 | 0;

            if (question.type == 'multipleSelect') {
                let answersSelected: Array<string> = [];
                let answersSelectedWeight: Array<number> = [];

                for (let j = 0; j < this.result[indexQuestion].length; j++) {
                    if (this.result[indexQuestion][j] == true) {
                        answersSelected.push(question.answers[j].uid);

                        //se for a opção correta
                        if (question.answers[j].correct) {
                            //adiciona os pontos
                            let weight = Number(question.answers[j].weight);
                            if (weight !== undefined && weight !== null) {
                                answersSelectedWeight.push(weight);
                            }
                        } else { // se for a opção incorreta
                            if (!this.resultVerify[indexQuestion]) { //se ele tiver respondido que acha que respondeu incorretamente
                                //adiciona os pontos
                                let weight = Number(question.answers[j].weight);
                                if (weight !== undefined && weight !== null) {
                                    answersSelectedWeight.push(weight);
                                }
                            }
                        }
                    }
                }

                if (answersSelected.length > 0) {
                    await this.createResultQuestion(question.uid, question.type, answersSelected, timestamp, this.result[indexVerifyQuestion])
                        .then((status) => {
                            question.answered = true;

                            let totalPoints = 0;
                            for (let aux of answersSelectedWeight) {
                                totalPoints = totalPoints + aux;
                            }

                            this.addUserPoints(totalPoints);

                            if (this.indexSlide >= (this.training.questions.length * 2) - 1) {
                                this.setAnsweredTraining();

                            } else {
                                setTimeout(() => {
                                    this.slideNext();
                                }, 1000);
                            }
                        })
                }

            } else if (question.type == 'oneSelect') {
                if (this.result[indexQuestion] != null && this.result[indexQuestion] != undefined && this.result[indexQuestion] != "") {
                    let ArrayAnswer: Array<String> = [this.result[indexQuestion]];

                    let totalPoints = 0;
                    for (let answer of question.answers) {
                        if (answer.uid == this.result[indexQuestion]) {

                            //caso a opção seja a correta
                            if (answer.correct) {
                                //adiciona os pontos
                                if (answer.weight !== null && answer.weight !== undefined && answer.weight != '') {
                                    totalPoints = answer.weight;
                                }
                            } else {//caso a opção seja incorreta
                                if (!this.resultVerify[indexQuestion]) { //se ele tiver respondido que acha que respondeu incorretamente
                                    //adiciona os pontos
                                    if (answer.weight !== null && answer.weight !== undefined && answer.weight != '') {
                                        totalPoints = answer.weight;
                                    }
                                }
                            }
                        }
                    }

                    await this.createResultQuestion(question.uid, question.type, ArrayAnswer, timestamp, this.result[indexVerifyQuestion])
                        .then((status) => {
                            question.answered = true;

                            if (totalPoints > 0) {
                                this.addUserPoints(totalPoints);
                            }

                            if (this.indexSlide >= (this.training.questions.length * 2) - 1) {
                                this.setAnsweredTraining();
                            } else {
                                setTimeout(() => {
                                    this.slideNext();
                                }, 1000);
                            }
                        })
                }
            }
        } else {
            this.global.userLogged();
        }
    }

    createResultQuestion(questionId, typeQuestion, answeredSelected, timestamp, verifyQuestion) {
        return new Promise((resolve, reject) => {
            this.daoTraining.createResult(this.moduleId, this.userId, this.trainingId, questionId, typeQuestion, answeredSelected, timestamp, verifyQuestion, (status) => {
                resolve(status);
            })
        })
    }

    startCountdown() {
        if (this.training.active_timer) {
            this.counter = this.training.timer_questions;
            clearInterval(this.interval);

            this.interval = setInterval(() => {
                this.counter--;

                if (this.counter < 0 && this.counter !== null) {
                    this.slideNext();
                }
            }, 1000);
        }
    }

    slideNext() {
        if (this.indexSlide >= (this.training.questions.length * 2) - 1) {
            this.indexSlide++;
            clearInterval(this.interval);
            this.setAnsweredTraining();
        } else {
            this.indexSlide++;
            clearInterval(this.interval);
            this.mySlider.lockSwipes(false);
            this.mySlider.slideNext();
            this.mySlider.lockSwipes(true);
            this.progressBar();
            this.startCountdown();
        }
    }

    slidePrevious() {
        if (this.indexSlide > 0) {
            this.indexSlide--;
            this.mySlider.lockSwipes(false);
            this.mySlider.slidePrev();
            this.mySlider.lockSwipes(true);
        }
    }

    addUserPoints(points) {
        if (points > 0 && this.global.userLoaded) {
            let userModuleId = this.global.userModuleId;

            this.daoAttendee.addPoints(this.eventId, userModuleId, this.userId, points, (result) => {

            });
        }
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('global.alerts.thankyou_answer'),
            message: this.translateService.instant('global.alerts.answer_saved_successfully'),
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        this.location.back();
                    }
                }
            ],
            backdropDismiss: false
        });

        await alert.present();

        clearInterval(this.interval);
    }

    setAnsweredTraining() {
        if (this.totalUserAnswers && this.global.userLoaded) {
            this.daoTraining.setUserTrainingAnswered(this.eventId, this.userId, this.trainingId, (data) => {
                this.presentAlertConfirm();
            });
        } else {
            this.presentAlertConfirm();
        }
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

}

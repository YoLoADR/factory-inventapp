import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TrainingPage } from './training.page';
import { RatingModule } from 'ng-starrating';
import { DatePickerModule } from 'ionic4-date-picker';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [TrainingPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: TrainingPage }
    ]),
    RatingModule,
    DatePickerModule,
    SharedModule
  ]
})
export class TrainingModule { }

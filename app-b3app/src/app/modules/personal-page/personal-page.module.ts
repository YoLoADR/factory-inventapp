import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PersonalPagePage } from './personal-page.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { AvatarModule } from 'ngx-avatar';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Market } from '@ionic-native/market/ngx';

@NgModule({
  declarations: [PersonalPagePage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PersonalPagePage }
    ]),
    AvatarModule,
    NgxQRCodeModule,
    SharedModule,
  ],
  providers:[
    File,
    FileTransfer,
    FileTransferObject,
    FileOpener,
    Market
  ]
})
export class PersonalPageModule { }

import { Component, OnInit, NgZone, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DaoAttendeesService } from "src/app/providers/db/dao-attendees.service";
import { DaoSpeakersService } from "src/app/providers/db/dao-speakers.service";
import {
    NavController,
    Events,
    MenuController,
    ModalController,
    AlertController,
    Platform,
    LoadingController,
} from "@ionic/angular";
import {
    InAppBrowser,
    InAppBrowserOptions,
} from "@ionic-native/in-app-browser/ngx";
import { GlobalService } from "src/app/shared/services";
import { LightboxImgComponent } from "src/app/components/lightbox-img/lightbox-img.component";
import { environment } from "src/environments/environment";
import {
    FileTransfer,
    FileTransferObject,
} from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { TranslateService } from "@ngx-translate/core";
import { Market } from "@ionic-native/market/ngx";
import { Subscription } from "rxjs";

@Component({
    selector: "app-personal-page",
    templateUrl: "./personal-page.page.html",
    styleUrls: ["./personal-page.page.scss"],
    providers: [DaoAttendeesService],
})
export class PersonalPagePage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    public eventId: string = null;
    public userId: string = null;
    public moduleId: string = null;
    public typeUser: number = null;
    public loader: boolean = true;
    public company: string = null;
    public title: string = null;
    public name: string = null;
    public site: string = null;
    public facebook: string = null;
    public instagram: string = null;
    public linkedin: string = null;
    public twitter: string = null;
    public phone: string = null;
    public description: string = null;
    public photoUrl: string = null;
    public type: number = null;

    public editProfileActive: boolean = false;

    public listCustomFields: Array<any> = [];
    public listCustomFieldOptions: Array<any> = [];
    public fieldsCustom: any;
    showCardCustomField: boolean = false;

    public documents = [];
    allowQR: boolean = false;
    // inapbrowser
    options: InAppBrowserOptions = {
        location: "no", //Or 'yes'
        hidden: "no", //Or  'yes'
        clearcache: "yes",
        clearsessioncache: "yes",
        hideurlbar: "yes",
        zoom: "yes", //Android only ,shows browser zoom controls
        hardwareback: "yes",
        mediaPlaybackRequiresUserAction: "no",
        shouldPauseOnSuspend: "no", //Android only
        closebuttoncaption: "back", //iOS and Android
        hidenavigationbuttons: "yes",
        disallowoverscroll: "no", //iOS only
        toolbar: "yes", //iOS only
        enableViewportScale: "no", //iOS only
        allowInlineMediaPlayback: "yes", //iOS only
        presentationstyle: "pagesheet", //iOS only
        fullscreen: "yes", //Windows only
    };
    backBtn: boolean = true;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    userLanguage: string = this.convertLangFormat(
        environment.platform.defaultLanguage
    );

    doc;
    loaderToast;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private dbAttendees: DaoAttendeesService,
        private dbSpeakers: DaoSpeakersService,
        private navCtrl: NavController,
        private theInAppBrowser: InAppBrowser,
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private zone: NgZone,
        private modalCtrl: ModalController,
        private platform: Platform,
        private transfer: FileTransfer,
        private file: File,
        private loadingCtrl: LoadingController,
        private fileOpener: FileOpener,
        private alertCtrl: AlertController,
        private translateService: TranslateService,
        private market: Market
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            //   this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem("eventId", this.eventId);
            this.userId = params.userId;
            //   this.userId = this.route.snapshot.params['userId'];
            this.typeUser = params.type;
            //   this.typeUser = this.route.snapshot.params['type'];
            this.moduleId = params.moduleId;
            //   this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe("loadColors", () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe("menuBadge", () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                });
            });
        });
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == "container") {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        this.userLanguage = this.convertLangFormat(this.global.language);
        if (this.typeUser == 5) {
            this.loadAttendee();
            this.checkeditProfileActiveAttendee();
        } else if (this.typeUser == 4) {
            this.loadSpeaker();
            this.checkeditProfileActiveSpeaker();
        } else {
            // invalid view profile
        }
        this.allowQR = this.global.event.allowProfileQR;
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    checkeditProfileActiveAttendee() {
        this.dbAttendees.checkeditProfileActive(this.moduleId, (result) => {
            this.editProfileActive = result;
        });
    }

    checkeditProfileActiveSpeaker() {
        this.dbSpeakers.checkeditProfileActive(this.moduleId, (result) => {
            this.editProfileActive = result;
        });
    }

    // load attendee
    loadAttendee() {
        this.subscriptions.push(this.dbAttendees.getAttendeeByEvent(
            this.eventId,
            this.userId).subscribe((attendee) => {
                this.zone.run(() => {
                    let aux = this.getSpeakerAttendeePrincipalTitle(
                        attendee["title"],
                        attendee["description"]
                    );
                    this.type = attendee["type"];
                    this.name = attendee["name"];
                    this.company = attendee["company"];
                    this.title = aux.title;
                    this.description = aux.description;
                    this.photoUrl = attendee["photoUrl"];
                    this.phone = attendee["phone"];
                    this.site = attendee["website"];
                    this.facebook = attendee["facebook"];
                    this.instagram = attendee["instagram"];
                    this.linkedin = attendee["linkedin"];
                    this.twitter = attendee["twitter"];

                    this.documents = [];
                    for (let uid in attendee["documents"]) {
                        this.documents.push(attendee["documents"][uid]);
                    }

                    this.loader = false;
                    this.getFieldsCustomAttendee();
                });
            }
            ));
    }

    getFieldsCustomAttendee() {
        this.dbAttendees.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.zone.run(() => {
                this.fieldsCustom = fields;
                this.getCustomFieldsAttendee();
            });
        });
    }

    getCustomFieldsAttendee() {
        this.dbAttendees.getCustomFields(
            this.eventId,
            this.userId,
            (customFields) => {
                this.zone.run(() => {
                    let displayCardCustom = false;

                    for (let aux of customFields) {
                        if (displayCardCustom == false) {
                            if (aux.type == "select") {
                                if (
                                    aux.value[this.userLanguage] !== null &&
                                    aux.value[this.userLanguage] !== undefined &&
                                    aux.value[this.userLanguage] !== ""
                                ) {
                                    displayCardCustom = true;
                                    this.showCardCustomField = true;
                                }
                            } else {
                                if (
                                    aux.textValue[this.userLanguage] !== null &&
                                    aux.textValue[this.userLanguage] !== undefined &&
                                    aux.textValue[this.userLanguage] !== ""
                                ) {
                                    displayCardCustom = true;
                                    this.showCardCustomField = true;
                                }
                            }
                        }
                        let position = this.fieldsCustom[aux.uid].order;
                        this.listCustomFields[position] = aux;
                    }

                    for (let i = 0; i < this.listCustomFields.length; i++) {
                        let custom = this.listCustomFields[i];

                        if (custom.type == "select") {
                            this.dbAttendees.getCustomFieldOptions(
                                this.moduleId,
                                this.userId,
                                custom.uid,
                                (listOptions) => {
                                    for (let option of listOptions) {
                                        if (custom.value == option.uid) {
                                            custom.value = option.answer;
                                        }
                                    }
                                }
                            );
                        }
                    }
                });
            }
        );
    }

    // load speaker
    loadSpeaker() {
        this.subscriptions.push(this.dbSpeakers.getSpeakerByEvent(this.eventId, this.userId).subscribe((speaker) => {
            this.zone.run(() => {
                let aux = this.getSpeakerAttendeePrincipalTitle(
                    speaker["title"],
                    speaker["description"]
                );
                this.type = speaker["type"];
                this.name = speaker["name"];
                this.company = speaker["company"];
                this.title = aux.title;
                this.description = aux.description;
                this.photoUrl = speaker["photoUrl"];
                this.phone = speaker["phone"];
                this.site = speaker["website"];
                this.facebook = speaker["facebook"];
                this.instagram = speaker["instagram"];
                this.linkedin = speaker["linkedin"];
                this.twitter = speaker["twitter"];
                this.loader = false;

                this.getFieldsCustomSpeaker();
            });
        }));
    }

    getFieldsCustomSpeaker() {
        this.dbSpeakers.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.zone.run(() => {
                this.fieldsCustom = fields;
                this.getCustomFieldsSpeakers();
            });
        });
    }

    getCustomFieldsSpeakers() {
        this.dbSpeakers.getCustomFields(
            this.eventId,
            this.userId,
            (customFields) => {
                this.zone.run(() => {
                    let displayCardCustom = false;
                    for (let aux of customFields) {
                        if (displayCardCustom == false) {
                            if (aux.type == "select") {
                                if (
                                    aux.value[this.userLanguage] !== null &&
                                    aux.value[this.userLanguage] !== undefined &&
                                    aux.value[this.userLanguage] !== ""
                                ) {
                                    displayCardCustom = true;
                                    this.showCardCustomField = true;
                                }
                            } else {
                                if (
                                    aux.textValue[this.userLanguage] !== null &&
                                    aux.textValue[this.userLanguage] !== undefined &&
                                    aux.textValue[this.userLanguage] !== ""
                                ) {
                                    displayCardCustom = true;
                                    this.showCardCustomField = true;
                                }
                            }
                        }

                        let position = this.fieldsCustom[aux.uid].order;
                        this.listCustomFields[position] = aux;
                    }

                    for (let i = 0; i < this.listCustomFields.length; i++) {
                        let custom = this.listCustomFields[i];

                        if (custom.type == "select") {
                            this.dbAttendees.getCustomFieldOptions(
                                this.moduleId,
                                this.userId,
                                custom.uid,
                                (listOptions) => {
                                    for (let option of listOptions) {
                                        if (custom.value == option.uid) {
                                            custom.value = option.answer;
                                        }
                                    }
                                }
                            );
                        }
                    }
                });
            }
        );
    }

    openSessionDetail(sessionId) {
        this.router.navigate([
            `/event/${this.eventId}/schedule-detail/${this.moduleId}/${sessionId}`,
        ]);
    }

    // zoom profile picture image
    openImage(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl
            .create({
                component: LightboxImgComponent,
                componentProps: {
                    img: url,
                },
            })
            .then((modal) => {
                modal.present();
            });
    }

    openLink(url: string) {
        let pattern = new RegExp("^(https?|ftp)://");
        if (!pattern.test(url)) url = "https://" + url;
        if (!this.global.isBrowser) {
            let target = "_blank";
            this.theInAppBrowser.create(url, target, this.options);
        } else {
            window.open(url, "_system");
        }
    }

    popPage() {
        this.navCtrl.pop();
    }

    editProfile() {
        this.global.previousPage = null;
        this.router.navigate([
            `/event/${this.eventId}/edit-profile/${this.moduleId}/${this.typeUser}/${this.userId}`,
        ]);
    }

    getSpeakerAttendeePrincipalTitle(title, description) {
        let principalTitle = "";
        let principalDescription = "";
        switch (this.global.language) {
            case "pt_BR": {
                principalTitle = title.PtBR;
                principalDescription = description.PtBR;
                break;
            }
            case "en_US": {
                principalTitle = title.EnUS;
                principalDescription = description.EnUS;
                break;
            }
            case "es_ES": {
                principalTitle = title.EsES;
                principalDescription = description.EsES;
                break;
            }
            case "fr_FR": {
                principalTitle = title.FrFR;
                principalDescription = description.FrFR;
                break;
            }
            case "de_DE": {
                principalTitle = title.deDE;
                principalDescription = description.deDE;
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == "" || principalTitle == null) {
            if (title[this.convertLangFormat(this.global.event.language)] !== "") {
                principalTitle =
                    title[this.convertLangFormat(this.global.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== "") {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        if (principalDescription == "" || principalDescription == null) {
            if (
                description[this.convertLangFormat(this.global.event.language)] !== ""
            ) {
                principalDescription =
                    description[this.convertLangFormat(this.global.event.language)];
            } else {
                for (let aux in description) {
                    if (description[aux] !== "") {
                        principalDescription = description[aux];
                    }
                }
            }
        }
        return {
            title: principalTitle,
            description: principalDescription,
        };
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case "pt_BR": {
                formatedLang = "PtBR";
                break;
            }
            case "en_US": {
                formatedLang = "EnUS";
                break;
            }
            case "es_ES": {
                formatedLang = "EsES";
                break;
            }
            case "fr_FR": {
                formatedLang = "FrFR";
                break;
            }
            case "de_DE": {
                formatedLang = "DeDE";
                break;
            }
        }
        return formatedLang;
    }

    openInIos() {
        return new Promise((resolve, reject) => {
            // ********* IMPORTANT: IOS NEED TO CHANGE EXTERNALROOTDIRECTORY TO OTHER PATH ****** //
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer
                .download(
                    this.doc.url,
                    this.file.dataDirectory + this.doc.name[this.userLanguage]
                )
                .then((entry) => {
                    resolve(entry.toURL());
                })
                .catch((err) => {
                    reject();
                });
        });
    }

    openInAndroid() {
        return new Promise((resolve, reject) => {
            this.file
                .createDir(this.file.externalRootDirectory, "_downloads", false)
                .then((response) => {
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    fileTransfer
                        .download(
                            this.doc.url,
                            this.file.externalRootDirectory +
                            "/_downloads/" +
                            this.doc.name[this.userLanguage]
                        )
                        .then((entry) => {
                            resolve(entry.toURL());
                        })
                        .catch((err) => {
                            reject();
                        });
                })
                .catch((err) => {
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    fileTransfer
                        .download(
                            this.doc.url,
                            this.file.externalRootDirectory +
                            "/_downloads/" +
                            this.doc.name[this.userLanguage]
                        )
                        .then((entry) => {
                            resolve(entry.toURL());
                        })
                        .catch((err) => {
                            reject();
                        });
                });
        });
    }

    async presentLoading() {
        this.loaderToast = await this.loadingCtrl.create({
            spinner: "crescent",
            duration: 5000,
        });
        await this.loaderToast.present();
    }

    async closeLoading() {
        await this.loaderToast.dismiss();
    }

    getMIMEtype(extn) {
        let ext = extn.toLowerCase();
        let MIMETypes = {
            // texts
            txt: "text/plain",
            rtf: "application/rtf",
            csv: "text/csv",
            docx:
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            doc: "application/msword",
            docm: "application/vnd.ms-word.document.macroEnabled.12",
            // pdf
            pdf: "application/pdf",
            // images
            jpg: "image/jpeg",
            bmp: "image/bmp",
            png: "image/png",
            gif: "image/gif",
            // sheets
            xls: "application/vnd.ms-excel",
            xlsx: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            xlsm: "application/vnd.ms-excel.sheet.macroEnabled.12",
            xlt: "application/vnd.ms-excel",
            xlsb: "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
            xltx:
                "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
            xltm: "application/vnd.ms-excel.template.macroEnabled.12",
            // presentations (ppt)
            ppt: "application/vnd.ms-powerpoint",
            pptx:
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            ppsx:
                "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            pps: "application/vnd.ms-powerpoint",
            ppsm: "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
        };
        return MIMETypes[ext];
    }

    async presentAlertConfirm() {
        const alert = await this.alertCtrl.create({
            header: this.translateService.instant("global.alerts.not_found_title"),
            message: this.translateService.instant(
                "global.alerts.app_open_doc_not_found_txt"
            ),
            buttons: [
                {
                    text: this.translateService.instant("global.buttons.no"),
                    role: "cancel",
                    cssClass: "secondary",
                    handler: (blah) => { },
                },
                {
                    text: this.translateService.instant("global.buttons.yes"),
                    handler: () => {
                        this.openMarketApp();
                    },
                },
            ],
        });

        await alert.present();
    }

    openMarketApp() {
        let platforms = this.platform.platforms().join();
        if (platforms.includes("android")) {
            let androidPackage = this.getPackageNameAndroid(this.doc.type);
            this.market.open(androidPackage);
        } else if (platforms.includes("ios")) {
            let iosPackage = this.getPackageNameIos(this.doc.type);
            this.market.open(iosPackage);
        }
    }

    getPackageNameAndroid(extn) {
        let ext = extn.toLowerCase();
        let packageNames = {
            // texts
            docx: "com.google.android.apps.docs.editors.docs",
            doc: "com.google.android.apps.docs.editors.docs",
            docm: "com.google.android.apps.docs.editors.docs",
            // pdf
            pdf: "com.adobe.reader",
            // sheets
            xls: "com.google.android.apps.docs.editors.sheets",
            xlsx: "com.google.android.apps.docs.editors.sheets",
            xlsm: "com.google.android.apps.docs.editors.sheets",
            xlt: "com.google.android.apps.docs.editors.sheets",
            xlsb: "com.google.android.apps.docs.editors.sheets",
            xltx: "com.google.android.apps.docs.editors.sheets",
            xltm: "com.google.android.apps.docs.editors.sheets",
            // presentations (ppt)
            ppt: "com.google.android.apps.docs.editors.slides",
            pptx: "com.google.android.apps.docs.editors.slides",
            ppsx: "com.google.android.apps.docs.editors.slides",
            pps: "com.google.android.apps.docs.editors.slides",
            ppsm: "com.google.android.apps.docs.editors.slides",
        };
        return packageNames[ext];
    }

    getPackageNameIos(extn) {
        let ext = extn.toLowerCase();
        let packageNames = {
            // texts
            docx: "documentos-google/id842842640",
            doc: "documentos-google/id842842640",
            docm: "documentos-google/id842842640",
            // pdf
            pdf: "adobe-acrobat-reader/id469337564",
            // sheets
            xls: "planilhas-google/id842849113",
            xlsx: "planilhas-google/id842849113",
            xlsm: "planilhas-google/id842849113",
            xlt: "planilhas-google/id842849113",
            xlsb: "planilhas-google/id842849113",
            xltx: "planilhas-google/id842849113",
            xltm: "planilhas-google/id842849113",
            // presentations (ppt)
            ppt: "apresentações-google/id879478102",
            pptx: "apresentações-google/id879478102",
            ppsx: "apresentações-google/id879478102",
            pps: "apresentações-google/id879478102",
            ppsm: "apresentações-google/id879478102",
        };
        return packageNames[ext];
    }

    openInweb(document) {
        if (
            document.type == "png" ||
            document.type == "jpg" ||
            document.type == "jpeg" ||
            document.type == "gif" ||
            document.type == "bmp" ||
            document.type == "js" ||
            document.type == "html" ||
            document.type == "css" ||
            document.type == "htm" ||
            document.type == "txt"
        ) {
            window.open(document.url, "_system", "location=yes");
        } else {
            let urlFinal = encodeURIComponent(document.url);
            // window.open('https://docs.google.com/viewer?url=' + urlFinal, '_system', 'location=yes');
            window.open(document.url, "_system", "location=yes");
        }
    }

    openDocument(document) {
        this.doc = document;
        let platforms = this.platform.platforms().join();
        if (platforms.includes("hybrid") && platforms.includes("cordova")) {
            if (platforms.includes("android")) {
                if (
                    document.type == "pdf" ||
                    document.type == "docx" ||
                    document.type == "doc" ||
                    document.type == "docm" ||
                    document.type == "txt" ||
                    document.type == "rtf" ||
                    document.type == "csv" ||
                    document.type == "pptx" ||
                    document.type == "ppt" ||
                    document.type == "thmx" ||
                    document.type == "ppsx" ||
                    document.type == "pps" ||
                    document.type == "ppsm" ||
                    document.type == "xlsx" ||
                    document.type == "xls" ||
                    document.type == "xlsm" ||
                    document.type == "xlt" ||
                    document.type == "xlsb" ||
                    document.type == "xltx" ||
                    document.type == "xltm" ||
                    document.type == "mp4" ||
                    document.type == "wmv" ||
                    document.type == "3gp" ||
                    document.type == "avi" ||
                    document.type == "mp3" ||
                    document.type == "wav" ||
                    document.type == "jpg" ||
                    document.type == "jpeg" ||
                    document.type == "png" ||
                    document.type == "gif" ||
                    document.type == "bmp"
                ) {
                    this.presentLoading();
                    this.openInAndroid()
                        .then((res: any) => {
                            let fileExtn = this.doc.type;
                            let fileMIMEType = this.getMIMEtype(fileExtn);
                            this.fileOpener
                                .open(res, fileMIMEType)
                                .then(() => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 50);
                                })
                                .catch((e) => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 50);
                                });
                        })
                        .catch(async (e) => {
                            if (
                                document.type == "png" ||
                                document.type == "jpg" ||
                                document.type == "jpeg" ||
                                document.type == "gif" ||
                                document.type == "bmp" ||
                                document.type == "js" ||
                                document.type == "html" ||
                                document.type == "css" ||
                                document.type == "htm" ||
                                document.type == "txt"
                            ) {
                                window.open(document.url, "_system", "location=yes");
                            } else {
                                let urlFinal = encodeURIComponent(document.url);
                                window.open(
                                    "https://docs.google.com/viewer?url=" + urlFinal,
                                    "_system",
                                    "location=yes"
                                );
                            }
                        });
                } else {
                    let urlFinal = encodeURIComponent(this.doc.url);
                    window.open(
                        "https://docs.google.com/viewer?url=" + urlFinal,
                        "_system",
                        "location=yes"
                    );
                    // this.theInAppBrowser.create('https://docs.google.com/viewer?url=' + urlFinal, '_system', 'location=yes', this.options);
                }
            } else if (platforms.includes("ios")) {
                if (
                    document.type == "pdf" ||
                    document.type == "docx" ||
                    document.type == "doc" ||
                    document.type == "docm" ||
                    document.type == "txt" ||
                    document.type == "rtf" ||
                    document.type == "csv" ||
                    document.type == "pptx" ||
                    document.type == "ppt" ||
                    document.type == "ppsx" ||
                    document.type == "pps" ||
                    document.type == "ppsm" ||
                    document.type == "xlsx" ||
                    document.type == "xls" ||
                    document.type == "xlsm" ||
                    document.type == "xlt" ||
                    document.type == "xlsb" ||
                    document.type == "xltx" ||
                    document.type == "xltm" ||
                    document.type == "mp4" ||
                    document.type == "wmv" ||
                    document.type == "3gp" ||
                    document.type == "avi" ||
                    document.type == "mp3" ||
                    document.type == "wav" ||
                    document.type == "jpg" ||
                    document.type == "jpeg" ||
                    document.type == "png" ||
                    document.type == "gif" ||
                    document.type == "bmp"
                ) {
                    this.presentLoading();
                    this.openInIos()
                        .then((res: any) => {
                            let fileExtn = this.doc.type;
                            let fileMIMEType = this.getMIMEtype(fileExtn);
                            this.fileOpener
                                .open(res, fileMIMEType)
                                .then(() => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 200);
                                })
                                .catch((e) => {
                                    if (e == "Missing Command Error") {
                                        if (
                                            document.type == "png" ||
                                            document.type == "jpg" ||
                                            document.type == "jpeg" ||
                                            document.type == "gif" ||
                                            document.type == "bmp" ||
                                            document.type == "js" ||
                                            document.type == "html" ||
                                            document.type == "css" ||
                                            document.type == "htm" ||
                                            document.type == "txt"
                                        ) {
                                            // window.open(document.url, '_system', 'location=yes');
                                            if (!this.global.isBrowser) {
                                                this.theInAppBrowser.create(
                                                    document.url,
                                                    "_system",
                                                    this.options
                                                );
                                            } else {
                                                window.open(document.url);
                                            }
                                        } else {
                                            if (!this.global.isBrowser) {
                                                let urlFinal = encodeURIComponent(document.url);
                                                this.theInAppBrowser.create(
                                                    "https://docs.google.com/viewer?url=" + urlFinal,
                                                    "_system",
                                                    this.options
                                                );
                                            } else {
                                                window.open(document.url);
                                            }
                                            // window.open('https://docs.google.com/viewer?url=' + document.url, '_system', 'location=yes');
                                        }
                                    } else {
                                        setTimeout(() => {
                                            this.closeLoading();
                                        }, 200);
                                        this.presentAlertConfirm();
                                    }

                                    console.log("Error openening file", e);
                                });
                        })
                        .catch((e) => {
                            setTimeout(() => {
                                this.closeLoading();
                            }, 50);
                            if (
                                document.type == "png" ||
                                document.type == "jpg" ||
                                document.type == "jpeg" ||
                                document.type == "gif" ||
                                document.type == "bmp" ||
                                document.type == "js" ||
                                document.type == "html" ||
                                document.type == "css" ||
                                document.type == "htm" ||
                                document.type == "txt"
                            ) {
                                // window.open(document.url, '_system', 'location=yes');
                                if (!this.global.isBrowser) {
                                    this.theInAppBrowser.create(
                                        document.url,
                                        "_system",
                                        this.options
                                    );
                                } else {
                                    window.open(document.url);
                                }
                            } else {
                                if (!this.global.isBrowser) {
                                    let urlFinal = encodeURIComponent(document.url);
                                    this.theInAppBrowser.create(
                                        "https://docs.google.com/viewer?url=" + urlFinal,
                                        "_system",
                                        this.options
                                    );
                                } else {
                                    window.open(document.url);
                                }
                                // window.open('https://docs.google.com/viewer?url=' + document.url, '_system', 'location=yes');
                            }
                        });
                } else {
                    if (
                        document.type == "png" ||
                        document.type == "jpg" ||
                        document.type == "jpeg" ||
                        document.type == "gif" ||
                        document.type == "bmp" ||
                        document.type == "js" ||
                        document.type == "html" ||
                        document.type == "css" ||
                        document.type == "htm" ||
                        document.type == "txt"
                    ) {
                        window.open(document.url, "_system", "location=yes");
                    } else {
                        let urlFinal = encodeURIComponent(document.url);
                        window.open(
                            "https://docs.google.com/viewer?url=" + urlFinal,
                            "_system",
                            "location=yes"
                        );
                    }
                }
            } else {
                this.openInweb(document);
            }
        } else {
            this.openInweb(document);
        }
    }
}

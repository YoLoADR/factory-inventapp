import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DocumentsFolderPage } from './documents-folder.page';
import { DaoDocumentsService } from 'src/app/providers/db/dao-document.service';
import { StorageOfflineService } from 'src/app/providers/storage-offline/storage-offline.service'
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Market } from '@ionic-native/market/ngx';
import { InteractivityModule } from 'src/app/content/reusable_components/interactivity/interactivity.module';

@NgModule({
    declarations: [DocumentsFolderPage],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: DocumentsFolderPage }
        ]),
        SharedModule,
        InteractivityModule
    ],
    providers: [
        DaoDocumentsService,
        StorageOfflineService,
        File,
        FileTransfer,
        FileTransferObject,
        FileOpener,
        Market
    ]
})
export class DocumentsFolderModule { }

import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DaoDocumentsService } from "src/app/providers/db/dao-document.service";
import { GlobalService, UtilityService } from "src/app/shared/services";
import {
    Events,
    MenuController
} from "@ionic/angular";
import { environment } from "src/environments/environment";
import { NetworkService } from "src/app/providers/network/network.service";
import { StorageOfflineService } from "src/app/providers/storage-offline/storage-offline.service";
import { Subscription } from "rxjs";

@Component({
    selector: "app-documents-folder",
    templateUrl: "./documents-folder.page.html",
    styleUrls: ["./documents-folder.page.scss"],
})
export class DocumentsFolderPage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    docModule: any = null;

    eventId: string = null;
    moduleId: string = null;
    folderId: string = null;

    folder: any;

    documentsSaved: any[] = [];
    documents: any[] = [];

    loader: boolean = true;

    searchOpen: boolean = false;
    searchText: string = "";
    backBtn: boolean = true;

    menu_color: string = null;
    menu_text_color: string = null;

    menuBadge: number = 0;
    allowButtonsHeader: boolean = false;

    userLanguage: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private daoDocuments: DaoDocumentsService,
        public SGlobal: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private network: NetworkService,
        private storage: StorageOfflineService,
        private SUtility: UtilityService
    ) {
        this.menuCtrl.enable(true);
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
    }

    ngOnInit() {
        if (this.SGlobal.previousPage == "container") {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        if (this.router.url == this.SGlobal.eventHomePage) {
            this.allowButtonsHeader = true;
        }

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem("eventId", this.eventId);
            this.moduleId = params.moduleId;
            this.folderId = params.folderId;

            this.loadColors();
            this.menuBadge = this.SGlobal.notificationBadge;
            this.events.subscribe("menuBadge", () => {
                this.menuBadge = this.SGlobal.notificationBadge;
            });

            // online
            if (this.network.status()) {
                // get module
                this.subscriptions.push(this.daoDocuments.getModule(this.moduleId).subscribe((module) => {
                    this.docModule = module;

                    // get folder
                    this.subscriptions.push(this.daoDocuments.getFolder(this.moduleId, this.folderId).subscribe((folder) => {
                        this.folder = folder;
                        let orderDocuments = folder.orderDocuments;
                        let orderName = `name.${this.userLanguage}`;
                        // get documents
                        this.subscriptions.push(this.daoDocuments.getDocuments(this.moduleId, this.folderId, orderDocuments, orderName).subscribe((documents) => {
                            this.documentsSaved = [];
                            this.documentsSaved = documents;
                            this.documents = [];
                            this.documents = documents;
                            this.loader = false;
                            // stores data on storage,
                            this.storage.setObject(this.folderId, {
                                module: this.docModule,
                                documents: this.documentsSaved,
                            });
                        }));
                    }));
                }));
            }
            // offline
            else {
                //iv. Get an storage Object:
                this.storage
                    .getObject(this.folderId)
                    .then((result) => {
                        if (result != null) {
                            this.docModule = result.module;
                            this.documentsSaved = result.documents;
                            this.documents = result.documents;
                        }

                        this.loader = false;
                    })
                    .catch((e) => {
                        this.docModule = null;
                        this.documentsSaved = [];
                        this.documents = [];
                        this.loader = false;
                    });
            }
        });
    }

    /**
     * Filter documents
     * @param event 
     */
    filterDocs(ev) {
        this.documents = this.documentsSaved;

        var val = ev.target.value;

        if (val && val.trim() != '') {
            this.documents = this.documentsSaved.filter((item: any) => {
                return (item.name[this.userLanguage].toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}

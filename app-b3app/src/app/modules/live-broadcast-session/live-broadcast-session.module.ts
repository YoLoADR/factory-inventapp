import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LiveBroadcastSessionPage } from './live-broadcast-session.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      { path: '', component: LiveBroadcastSessionPage }
    ]),
  ],
  declarations: [LiveBroadcastSessionPage]
})
export class LiveBroadcastSessionModule {}

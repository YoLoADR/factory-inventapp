import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SelfCheckinPage } from './self-checkin.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { AvatarModule } from 'ngx-avatar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Camera } from '@ionic-native/camera/ngx';

@NgModule({
  declarations: [SelfCheckinPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SelfCheckinPage }
    ]),
    AvatarModule,
    SharedModule
  ],
  providers: [BarcodeScanner, Camera]
})
export class SelfCheckinModule { }

import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { Events, MenuController, AlertController } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { CeuAttendee } from 'src/app/models/ceu-attendee';
import { DaoCheckinService } from 'src/app/providers/db/dao-checkin.service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-self-checkin',
    templateUrl: './self-checkin.page.html',
    styleUrls: ['./self-checkin.page.scss'],
    providers: [DaoCheckinService]
})

export class SelfCheckinPage implements OnInit {
    public module = null

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    eventId: string = null;
    moduleId: string = null;
    refModule: any = null;
    moduleDoc: AngularFirestoreDocument<any> = null;
    refAttendee: any = null;
    attendeeDoc: AngularFirestoreDocument<any> = null;
    userId = null;
    backBtn: boolean = true;
    attendee: CeuAttendee = null;
    lastCheckins: Array<any> = null;
    public scanOptions: BarcodeScannerOptions;
    allowButtonsHeader: boolean = false;
    isDegrade: boolean = false;

    constructor(
        private route: ActivatedRoute,
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private afs: AngularFirestore,
        private dbCheckin: DaoCheckinService,
        public scan: BarcodeScanner,
        private router: Router,
        public alertController: AlertController,
        private translateService: TranslateService,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
            this.moduleDoc = this.afs.collection('modules').doc(this.moduleId);
            this.refModule = this.moduleDoc.valueChanges().subscribe((data: any) => {
                this.module = data;
            });
            this.scanOptions = {
                prompt: '-',
                resultDisplayDuration: 0
            }
            this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        })
    }

    btn_color1: string = null;
    btn_color2: string = null;
    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        if (this.menu_color.includes('linear')) {
            this.isDegrade = true;
            let colors = this.separeGradientColors(this.menu_color);
            this.btn_color1 = colors[0];
            this.btn_color2 = colors[1];
        }
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    // separe gradient colors
    separeGradientColors(str) {
        let hex = [];
        let index = str.indexOf('#');

        while (index > -1) {
            hex.push(this.getHexColors(str))
            str = str.replace('#', '');
            index = str.indexOf('#');
        }

        return hex;
    }

    getHexColors(str) {
        let position = str.indexOf('#');
        let result = str.substring(position, position + 7);

        return result;
    }

    async ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
        // receives user uid
        if (this.global.userId !== null) {
            this.userId = this.global.userId;
            this.start();
        } else {
            this.userId = await this.global.loadUserId();
            this.start();
        }
    }

    start() {
        if (this.global.userLogged()) {
            this.attendeeDoc = this.afs.collection('events').doc(this.eventId).collection('attendees').doc(this.userId);
            this.refAttendee = this.attendeeDoc.valueChanges().subscribe((data: any) => {
                this.attendee = data;
                this.getLastCheckins();
            });
        }
    }

    // get the last checkins of current user
    getLastCheckins() {
        this.lastCheckins = [];
        for (let i in this.attendee.checkins) {
            this.dbCheckin.getModuleCheckinAndCheckin(this.attendee.checkins[i], this.eventId, (checkin) => {
                if (checkin !== undefined && checkin !== null)
                    this.lastCheckins.push(checkin);
            });
        }
    }

    // open qr (camera)
    openQr() {
        if (this.global.userLogged()) {
            this.scan.scan(this.scanOptions).then(barcodeData => {
                if (barcodeData.cancelled == true) {
                    // cancel operation, close camera
                    this.router.navigate([`/events/${this.eventId}/self-checkin/${this.moduleId}`]);
                } else {
                    let arrayAux = barcodeData.text.split('+');
                    this.confirmCheckin(arrayAux[0], arrayAux[1]);
                }
            }).catch(err => {
                this.invalidQrAlert();
                console.log('Error', err);
            });
        }
    }

    // confirm checkin change status
    confirmCheckin(moduleId, checkinId) {
        let newCheckins = [];
        if (!this.attendee.checkins) {
            newCheckins.push(checkinId);
        } else if ((this.attendee.checkins && this.attendee.checkins.length > 0 && !this.attendee.checkins.includes(checkinId))) {
            newCheckins = this.attendee.checkins;
            newCheckins.push(checkinId);
        } else if ((this.attendee.checkins && this.attendee.checkins.length > 0 && this.attendee.checkins.includes(checkinId))) {
            newCheckins = this.attendee.checkins;
        }
        this.dbCheckin.validateCheckinForAttendee(true, checkinId, moduleId, this.attendee.uid, this.attendee.moduleId, this.eventId, newCheckins, (status) => {

            if (status == true) {
                this.successAlert();
                this.router.navigate([`/events/${this.eventId}/self-checkin/${this.moduleId}`]);
            } else {
                this.errorManualAlert();
                this.router.navigate([`/events/${this.eventId}/self-checkin/${this.moduleId}`]);
            }
        })
    }

    async successAlert() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('global.alerts.all_right'),
            message: this.translateService.instant('global.alerts.your_presence_confirmed'),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.ok'),
                    handler: (_) => {
                    },
                }
            ]
        });

        await alert.present();
    }

    async errorManualAlert() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('global.alerts.error'),
            message: this.translateService.instant('global.alerts.not_confirm_your_presence'),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.ok'),
                    handler: (_) => {
                        //
                    },
                }
            ]
        });

        await alert.present();
    }

    async invalidQrAlert() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('global.alerts.error_scanning'),
            message: this.translateService.instant('global.alerts.invalid_qrcode'),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.yes'),
                    handler: (_) => {
                        this.openQr();
                    },
                },
                {
                    text: this.translateService.instant('global.buttons.no'),
                    handler: (_) => {
                        //
                    }
                }
            ]
        });

        await alert.present();
    }

    unsubscribe() {
        if (this.refAttendee) {
            this.refAttendee.unsubscribe();
        }
        this.refModule.unsubscribe();
    }

    ionViewDidLeave() {
        this.unsubscribe();
    }

}

import { Component, NgZone } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoGamificationService } from 'src/app/providers/db/dao-gamification.service';
import { QRCode } from 'src/app/models/gaming-qrcode';
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { DaoGeralService } from 'src/app/providers/db/dao-geral.service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';

@Component({
    selector: 'app-gamification',
    templateUrl: './gamification.page.html',
    styleUrls: ['./gamification.page.scss'],
})
export class GamificationPage {
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    eventId: string = null;
    moduleId: string = null;
    module = null;
    qrcodes: Array<QRCode> = [];
    public scanOptions: BarcodeScannerOptions;
    public readedQrCodes: number = 0;
    allowButtonsHeader: boolean = false;

    constructor(
        public global: GlobalService,
        private route: ActivatedRoute,
        private dbGamification: DaoGamificationService,
        private translateService: TranslateService,
        private toastCtrl: ToastController,
        private daoGeral: DaoGeralService,
        private dbAnalytics: DaoAnalyticsService,
        public scan: BarcodeScanner,
        private router: Router,
        private zone: NgZone
    ) {
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.snapshot.params['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.loadModule();
            this.totalQrCodes();
            this.scanOptions = {
                prompt: this.translateService.instant('pages.checkin.qrcode_focus'),
                resultDisplayDuration: 0
            }

            // save another module view access
            this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        })
    }

    ionViewWillEnter() {
        // get the value the allowButtonsHeader
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    loadModule() {
        this.dbGamification.getModule(this.moduleId, (module) => {
            this.module = module;
        })
    }

    // get total of qrcodes available
    totalQrCodes() {
        this.dbGamification.getCodes(this.moduleId, (qrcodes: Array<QRCode>) => {
            this.qrcodes = qrcodes;
            let gaming = this.global.user.gaming ? this.global.user.gaming : {};
            gaming = Object.keys(gaming);
            this.readedQrCodes = this.qrcodes.length - gaming.length;
        })
    }

    // scan qrcode to get points
    scanQr() {
        this.scan.scan(this.scanOptions).then(barcodeData => {
            if (barcodeData.cancelled) {
                this.router.navigate([`/event/${this.eventId}/gaming/${this.moduleId}`]);
            } else {
                let codeId = barcodeData.text;
                this.dbGamification.checkQr(codeId, this.moduleId, (response) => {
                    if (response.status) {
                        let gaming = this.global.user.gaming ? this.global.user.gaming : {};
                        if (!gaming[codeId]) {
                            // qrcode lido com sucesso
                            gaming[codeId] = { uid: codeId, checked: true };
                            let totalPoints = this.global.user.points + response.qrcode.points;
                            this.daoGeral.updateUserGeneral({ points: totalPoints, gaming: gaming }, this.global.userId, this.eventId, this.global.userType);
                            this.totalQrCodes();
                            this.showSuccessToast();
                        } else {
                            // usuário já leu esse qrcode
                            this.showUserAlready();
                        }
                    } else {
                        // falhou, qrcode nao existe
                        this.showErrorToast();
                    }
                });
            }
        })
    }

    // check if already scanned this qrcode
    checkAlreadyScanned(uid) {
        let gaming = this.global.user.gaming ? this.global.user.gaming : [];
        if (gaming.length >= 1) {
            return gaming.map(function (e) { return e.uid; }).indexOf(uid);
        } else {
            return -1;
        }
    }

    // success QRCode Gaming
    async showSuccessToast() {
        const toast = await this.toastCtrl.create({
            message: this.translateService.instant('pages.gamification.success_scan'),
            duration: 4000
        });
        toast.present();
    }

    // error QRCode Gaming
    async showErrorToast() {
        const toast = await this.toastCtrl.create({
            message: this.translateService.instant('pages.gamification.error_scan'),
            duration: 4000
        });
        toast.present();
    }

    // user already scanned QRCode Gaming
    async showUserAlready() {
        const toast = await this.toastCtrl.create({
            message: this.translateService.instant('pages.gamification.already_scan'),
            duration: 4000
        });
        toast.present();
    }

    // error to read QRCode Gaming
    async showReadError() {
        const toast = await this.toastCtrl.create({
            message: this.translateService.instant('pages.gamification.scan_read_error'),
            duration: 4000
        });
        toast.present();
    }


}

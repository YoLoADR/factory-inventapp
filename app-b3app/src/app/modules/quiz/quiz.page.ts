import { Component, OnInit, ViewChild, ViewEncapsulation, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { MenuController, AlertController, ToastController } from '@ionic/angular';
import { DaoQuizService } from 'src/app/providers/db/dao-quiz.service';
import { Quiz } from 'src/app/models/quiz';
import { NotificationDateService } from 'src/app/providers/date/notification-date.service';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { DaoInteractivityService } from 'src/app/providers/db/dao-interactivity.service';
import { Subscription } from 'rxjs';
import { Question } from 'src/app/models/question';
import { take } from 'rxjs/operators';

@Component({
    selector: 'app-quiz',
    templateUrl: './quiz.page.html',
    styleUrls: ['./quiz.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuizPage implements OnInit, OnDestroy {
    @Input() componentMode: boolean;
    @Input() backToList: boolean = false;
    subscriptions: Subscription[] = [];
    @ViewChild('mySlider', { static: false }) mySlider: any;

    @Input() eventId: string = null;
    @Input() moduleId: string = null;
    @Input() quizId: string = null;
    @Input() sessionId: string = null;
    @Input() userId: string = null;
    @Input() scheduleModuleId: string = null;

    @Output() stopLoaderEvent: EventEmitter<boolean> = new EventEmitter();
    @Output() goBackEvent: EventEmitter<any> = new EventEmitter();

    quiz: Quiz = null;
    currentQuestion: any;
    result: Array<any> = [];
    setResultVerify: boolean = false;
    totalQuestions: number = null;
    moduleInteractivity = null;

    rate: number;
    ratingValue;

    indexSlide: number = 0;
    counter: number = 0;
    public interval: any;
    progress: number = 0;

    previousPage;
    view_only: boolean = null;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    todayDate = new Date();

    checkedView = false;
    totalUserAnswers: boolean = false;
    loader: boolean = true;
    allowButtonsHeader: boolean = false;
    userLanguage: string = environment.platform.defaultLanguage;
    quizEnded: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public SGlobal: GlobalService,
        private menuCtrl: MenuController,
        private daoInteractivity: DaoInteractivityService,
        private daoQuiz: DaoQuizService,
        private daoAttendee: DaoAttendeesService,
        public alertCtrl: AlertController,
        public alertController: AlertController,
        public toastController: ToastController,
        private notificationDate: NotificationDateService,
        private location: Location,
        private translateService: TranslateService,
        private SUtility: UtilityService
    ) { }

    ngOnInit() {
        this.menuCtrl.enable(true);
        this.loadColors();
        if (this.router.url == this.SGlobal.eventHomePage) {
            this.allowButtonsHeader = true;
        }
        this.userId = this.SGlobal.userId;

        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        if (!this.componentMode) {

            this.route.params.subscribe((params) => {
                this.eventId = params.eventId;
                localStorage.setItem('eventId', this.eventId);
                this.moduleId = params.moduleId;
                this.quizId = params.quizId;

                this.route.queryParams.subscribe((params) => {
                    if (this.router.getCurrentNavigation().extras.state) {
                        this.previousPage = this.router.getCurrentNavigation().extras.state.previousPage;
                        this.scheduleModuleId = this.router.getCurrentNavigation().extras.state.scheduleModuleId;
                        this.sessionId = this.router.getCurrentNavigation().extras.state.sessionId;
                    }
                });
            })
        } else {
            this.daoInteractivity.getInteractivityModule(this.eventId, (module) => {
                this.moduleInteractivity = module;
                this.getQuiz();
            })
        }
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    ionViewDidEnter() {
        this.daoInteractivity.getInteractivityModule(this.eventId, (module) => {
            this.moduleInteractivity = module;
            this.getQuiz();
        })
    }

    ngAfterViewChecked() {
        if (!this.checkedView) {
            if (this.mySlider !== undefined) {
                this.mySlider.lockSwipes(true)
                this.checkedView = true;
            }
        }
    }

    ionViewWillLeave() {
        this.indexSlide = 0;
        this.counter = 0;
        this.progress = 0;
        clearInterval(this.interval);
    }

    /**
     * Go back to list surveys on component mode
     */
    goBackToList() {
        this.goBackEvent.emit(true);
    }

    progressBar() {
        let activeIndex = this.indexSlide;
        let totalSlides = this.quiz.questions.length;

        let aux = (activeIndex + 1) * (100 / totalSlides)
        this.progress = Number(aux.toFixed(2));
    }

    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
        this.link_color = this.SGlobal.eventColors.link_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
        this.bg_general_color = this.SGlobal.eventColors.bg_general_color;
    }

    /**
     * Getting quiz
     */
    getQuiz() {
        if (this.SGlobal.userLoaded || this.moduleInteractivity.answerOffline) {
            this.daoQuiz.getTotalQuestions(this.moduleId, this.quizId).subscribe((total) => {
                this.totalQuestions = total;

                this.subscriptions.push(this.daoQuiz.getQuiz(this.moduleId, this.quizId).subscribe((quiz) => {
                    this.quiz = quiz;
                    this.daoQuiz.checkAnsweredQuiz(this.eventId, this.userId, this.quizId).pipe(take(1)).subscribe((answered) => {
                        this.quizEnded = answered;
                    })
                    let cont = 0;
                    for (let question of this.quiz.questions) {
                        let visibility = question.visibility;

                        if (!visibility) {
                            this.quiz.questions.splice(cont, 1);
                        }
                        cont++;
                    }

                    if (this.quiz.questions.length > 0) {
                        this.currentQuestion = this.quiz.questions[0];
                    }

                    this.loader = false;

                    if (!this.quiz.change_answer && this.quiz.view_answered) {
                        this.view_only = true;
                    } else {
                        this.view_only = false;
                    }

                    this.startCountdown();
                    this.progressBar();
                    this.setResult();
                    this.stopLoaderEvent.emit(false);
                }));
            })
        } else {
            if (this.componentMode) {
                clearInterval(this.interval);
                this.goBackToList();
            } else {
                this.SGlobal.userLogged();
                clearInterval(this.interval);
                this.location.back();
            }
        }
    }


    setResult() {
        for (let i = 0; i < this.quiz.questions.length; i++) {
            if (this.quiz.questions[i].type == 'multipleSelect') {
                this.result[i] = new Array(this.quiz.questions[i].answers.length).fill(null);
            } else {
                this.result[i] = null;
            }
        }

        this.setResultVerify = true;

        if (this.SGlobal.userLoaded) {
            this.daoQuiz.getResponseUsers(this.moduleId, this.quizId, this.userId, (responses) => {
                let cont = 0;
                for (let question of this.quiz.questions) {
                    if (responses[question.uid] == undefined) {
                        question.answered = false;
                    } else {
                        question.answered = true;

                        if (question.type == 'multipleSelect') {
                            if (!this.quiz.change_answer) {
                                question.showTrue = true;
                            }

                            let findOneCorrect = false;

                            for (let answer of this.quiz.questions[cont].answers) {
                                if (answer.correct) {
                                    findOneCorrect = true;
                                }
                            }

                            for (let answerId of responses[question.uid].answer) {
                                let contAnswer = 0;
                                for (let answer of this.quiz.questions[cont].answers) {
                                    if (answerId == answer.uid) {
                                        this.result[cont][contAnswer] = true;

                                        if (!this.quiz.change_answer && findOneCorrect && !answer.correct) {
                                            answer.showFalse = true;
                                        }
                                    }

                                    contAnswer++;
                                }
                            }
                        } else {

                            if (!this.quiz.change_answer) {
                                question.showTrue = true;
                            }

                            let findOneCorrect = false;

                            for (let answer of this.quiz.questions[cont].answers) {
                                if (answer.correct) {
                                    findOneCorrect = true;
                                }
                            }

                            for (let answer of this.quiz.questions[cont].answers) {
                                if (answer.uid == responses[question.uid].answer) {
                                    this.result[cont] = answer.uid;

                                    if (!this.quiz.change_answer && findOneCorrect && !answer.correct) {
                                        answer.showFalse = true;
                                    }
                                }
                            }
                        }
                    }

                    cont++;
                }
            })
        }
    }

    setEvaluation($event: { newValue: number }, index) {
        this.result[index] = $event.newValue;
    }

    dateSelected($event, index) {
        if (this.quiz.questions[index].answered == false) {
            let date = new Date($event);
            let timestamp = this.notificationDate.getTimeStampFromDateNow(date, this.SGlobal.event.timezone);
            this.result[index] = timestamp;
        } else {
            this.presentToastDate();
        }
    }

    async presentToastDate() {
        const toast = await this.toastController.create({
            message: this.translateService.instant('global.alerts.date_already_selected'),
            duration: 2000
        });

        toast.present();
    }

    async presentToast(answered) {
        if (answered && this.quiz.change_answer == false) {
            const toast = await this.toastController.create({
                message: this.translateService.instant('global.alerts.not_change_answer'),
                duration: 2000
            });

            toast.present();
        }
    }

    async sendQuiz(question, index) {
        this.totalUserAnswers = true;
        clearInterval(this.interval);

        if (this.SGlobal.userLoaded || this.moduleInteractivity.answerOffline) {
            for (let i = 0; i <= this.quiz.questions.length - 1; i++) {
                if (this.quiz.questions[i].type == 'multipleSelect') {
                    let findAnswer = false;
                    for (let result of this.result[i]) {
                        if (result) {
                            findAnswer = true;
                        }
                    }

                    if (!findAnswer) {
                        this.totalUserAnswers = false;
                    }
                } else {
                    if (this.result[i] == null) {
                        this.totalUserAnswers = false;
                    }
                }
            }

            if (this.totalQuestions > this.quiz.questions.length) {
                this.totalUserAnswers = false;
            }


            let timestamp = Date.now() / 1000 | 0;

            if (question.type == 'multipleSelect') {
                let answersSelected: Array<string> = [];
                let answersSelectedWeight: Array<number> = [];

                for (let j = 0; j < this.result[index].length; j++) {
                    if (this.result[index][j] == true) {
                        answersSelected.push(question.answers[j].uid);

                        let weight = Number(question.answers[j].weight);
                        if (weight !== undefined && weight !== null) {
                            answersSelectedWeight.push(weight);
                        }

                        let findCorrect = false;
                        for (let answer of question.answers) {
                            if (answer.correct) {
                                findCorrect = true;
                            }
                        }

                        if (!question.answers[j].correct && findCorrect) {
                            question.answers[j].showFalse = true;
                        }
                    }
                }

                if (answersSelected.length > 0) {
                    await this.createResultQuestion(question.uid, question.type, answersSelected, timestamp)
                        .then((status) => {
                            question.answered = true;
                            question.showTrue = true;

                            let totalPoints = 0;
                            for (let aux of answersSelectedWeight) {
                                totalPoints = totalPoints + aux;
                            }

                            this.addUserPoints(totalPoints);

                            // if (this.indexSlide >= this.quiz.questions.length - 1) {
                            //     this.setAnsweredQuiz();

                            // } else {
                            setTimeout(() => {
                                this.slideNext();
                            }, 1000);
                            // }
                        })
                }

            } else if (question.type == 'oneSelect') {
                if (this.result[index] != null && this.result[index] != undefined && this.result[index] != "") {
                    let ArrayAnswer: Array<String> = [this.result[index]];

                    let findCorrect = false;
                    for (let answer of question.answers) {
                        if (answer.correct) {
                            findCorrect = true;
                        }
                    }

                    let totalPoints = 0;
                    for (let answer of question.answers) {
                        if (answer.uid == this.result[index]) {

                            if (answer.weight !== null && answer.weight !== undefined && answer.weight != '') {
                                totalPoints = answer.weight;
                            }

                            if (!answer.correct && findCorrect) {
                                answer.showFalse = true;
                            }
                        }
                    }

                    await this.createResultQuestion(question.uid, question.type, ArrayAnswer, timestamp)
                        .then((status) => {
                            question.answered = true;
                            question.showTrue = true;

                            if (totalPoints > 0) {
                                this.addUserPoints(totalPoints);
                            }

                            // if (this.indexSlide >= this.quiz.questions.length - 1) {
                            //     this.setAnsweredQuiz();
                            // } else {
                            setTimeout(() => {
                                this.slideNext();
                            }, 1000);
                            // }
                        })
                }
            }
        } else {
            this.SGlobal.userLogged();
        }
    }

    createResultQuestion(questionId, typeQuestion, answeredSelected, timestamp) {
        return new Promise((resolve, reject) => {
            this.daoQuiz.createResult(this.moduleId, this.userId, this.quizId, questionId, typeQuestion, answeredSelected, timestamp, (status) => {
                resolve(status);
            })
        })
    }

    startCountdown() {
        if (this.quiz.active_timer) {
            this.counter = this.quiz.timer_questions;
            clearInterval(this.interval);

            this.interval = setInterval(() => {
                this.counter--;

                if (this.counter < 0 && this.counter !== null) {
                    this.slideNext();
                }
            }, 1000);
        }
    }

    slideNext() {
        if (this.currentQuestion.graphicDifusion && !this.currentQuestion.showResult) {
            this.currentQuestion.showResult = true;
        } else {
            if (this.indexSlide >= this.quiz.questions.length - 1) {
                this.currentQuestion.showResult = false;
                // this.indexSlide++;
                // this.currentQuestion = this.quiz.questions[this.indexSlide];
                clearInterval(this.interval);
                this.setAnsweredQuiz();
            } else {
                this.currentQuestion.showResult = false;
                this.indexSlide++;
                this.currentQuestion = this.quiz.questions[this.indexSlide];
                clearInterval(this.interval);
                this.mySlider.lockSwipes(false);
                this.mySlider.slideNext();
                this.mySlider.lockSwipes(true);
                this.progressBar();
                this.startCountdown();
            }
        }
    }

    slidePrevious() {
        if (this.indexSlide > 0) {
            this.indexSlide--;
            this.currentQuestion.showResult = false;
            this.currentQuestion = this.quiz.questions[this.indexSlide];
            this.mySlider.lockSwipes(false);
            this.mySlider.slidePrev();
            this.mySlider.lockSwipes(true);
        }
    }

    addUserPoints(points) {
        if (points > 0 && this.SGlobal.userLoaded) {
            let userModuleId = this.SGlobal.userModuleId;

            this.daoAttendee.addPoints(this.eventId, userModuleId, this.userId, points, (result) => {

            });
        }
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('global.alerts.thankyou_answer'),
            message: this.translateService.instant('global.alerts.answer_saved_successfully'),
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        if (this.componentMode) {
                            this.goBackToList();
                        } else {
                            this.location.back();
                        }
                    }
                }
            ],
            backdropDismiss: false
        });

        await alert.present();

        clearInterval(this.interval);
    }

    setAnsweredQuiz() {
        if (this.quizEnded) {
            this.presentAlertConfirm();
        } else {
            this.daoQuiz.setUserQuizAnswered(this.eventId, this.userId, this.quizId, (data) => {
                this.presentAlertConfirm();
            });
        }
        // if (this.totalUserAnswers && this.SGlobal.userLoaded) {
        //     // this.daoQuiz.setUserQuizAnswered(this.eventId, this.userId, this.quizId, (data) => {
        //     //     this.presentAlertConfirm();
        //     // });
        // } else {
        //     this.presentAlertConfirm();
        // }
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SpeakersPage } from './speakers.page';
import { DaoSpeakersService } from 'src/app/providers/db/dao-speakers.service';
import { AvatarModule } from 'ngx-avatar';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [SpeakersPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SpeakersPage }
    ]),
    AvatarModule,
    SharedModule
  ],
  providers: [DaoSpeakersService]
})
export class SpeakersModule { }

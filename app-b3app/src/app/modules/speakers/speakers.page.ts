import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { DaoSpeakersService } from 'src/app/providers/db/dao-speakers.service';
import { IonInfiniteScroll, IonVirtualScroll, Events, MenuController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { TypeVisionModule } from '../../models/type-vision-module';
import { DaoGroupsService } from 'src/app/providers/db/dao-groups.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-speakers',
    templateUrl: './speakers.page.html',
    styleUrls: ['./speakers.page.scss']
})

export class SpeakersPage implements OnInit, OnDestroy {
    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonVirtualScroll, { static: false }) virtualScroll: IonVirtualScroll;
    subscriptions: Subscription[] = [];

    public module = null

    searchOpen: boolean = false;
    lastSpeakers;
    speakers: Array<any> = [];
    searchedSpeakers: Array<any> = [];
    eventId: string = null;
    moduleId: string = null;
    loader: boolean = true;
    typeOrder: string;
    typeUser: string;
    backBtn: boolean = true;
    refModule: any = null;
    refSpeakers: any = null;
    moduleCollection: AngularFirestoreDocument<any> = null;
    speakersCollection: AngularFirestoreCollection<any> = null;
    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    refAllSpeakers: any = null;
    allSpeakersCollection: AngularFirestoreCollection<any> = null;
    allSpeakersList: Array<any> = [];
    menuBadge: number = 0;

    // module views
    typeVisionModule = null//type of module views.
    GLOBAL_VISION = TypeVisionModule.GLOBAL_VISION // visão global 
    DIVIDED_BY_GROUPS = TypeVisionModule.DIVIDED_BY_GROUPS // visão dividida por grupo
    GROUP_VISION = TypeVisionModule.GROUP_VISION  // visão por grupos 
    GROUP_ACCESS_PERMISSION = TypeVisionModule.GROUP_ACCESS_PERMISSION //acessp limitado por grupo 

    viewGroup: boolean = false; //html of the event groups
    groups = [] //groups of the event.
    userGroups: any = [] //user groups

    constructor(
        private dbSpeakers: DaoSpeakersService,
        private route: ActivatedRoute,
        private router: Router,
        public toastController: ToastController,
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private dbGroups: DaoGroupsService
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.events.subscribe('languageUpdate', () => {
                this.startAll();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.menuBadge = this.global.notificationBadge;
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        // get the value of the backBtn button.
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        // get the value the allowButtonsHeader
        if (this.router.url == this.global.eventHomePage) {
            this.allowButtonsHeader = true;
        }

        this.startAll();
    }

    ngOnDestroy() {
        // this.unsubscribe();
        this.viewGroup = false
        // close getModule
        this.dbSpeakers.closeRefGetModule()
        this.dbGroups.closeRefContSpeakersOfGroup()
        // save another module view access
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    ionViewWillEnter() {
    }

    ionViewWillLeave() {
    }

    startAll() {

        // get the module information
        this.dbSpeakers.getModule(this.moduleId, async (module) => {
            this.module = module

            this.typeOrder = module.orderUsers;
            this.typeVisionModule = module.typeVision

            // verifies the type of vision of the module. global vision
            if (this.typeVisionModule === this.GLOBAL_VISION) {
                this.loader = true
                this.viewGroup = false

                // load speakers
                this.getFirstSpeakersGlobalVision()
                this.getSpeakersAllGlobalVision()
            }

            //divided by groups
            if (this.typeVisionModule === this.DIVIDED_BY_GROUPS) {
                this.loader = true
                this.viewGroup = true

                // load groups
                this.getGroupsEvent()
            }

            // group vision
            if (this.typeVisionModule === this.GROUP_VISION) {
                this.loader = true
                this.viewGroup = false

                // get user groups
                this.userGroups = []
                if (typeof this.global.groupsAttendees !== 'undefined') {
                    this.userGroups = this.global.groupsAttendees
                    // load speakers
                    this.getSpeakersGroupVision()
                }
                // if the groups have not been loaded, call the loadGroups function of the global service.
                else {
                    // get the id of the logged in user.
                    let userId = null

                    // receives user uid
                    if (typeof this.global.userId !== 'undefined' && this.global.userId !== null) {
                        userId = this.global.userId
                    } else {
                        userId = await this.global.loadUserId()
                    }

                    // get the type user of the logged in user.
                    let userType = null

                    if (typeof this.global.userType !== 'undefined' && this.global.userType !== null) {
                        userType = this.global.userType
                    } else {
                        userType = await this.global.loadUserType()
                    }

                    // get the type groupsAttendee of the logged in user.
                    this.userGroups = []
                    if (typeof this.global.groupsAttendees !== 'undefined') {
                        this.userGroups = this.global.groupsAttendees
                    } else {
                        this.userGroups = await this.global.loadGroups(userId, userType, this.eventId)
                    }

                    // load speakers
                    this.getSpeakersGroupVision()
                }
            }

            if (this.typeVisionModule === this.GROUP_ACCESS_PERMISSION) {
                this.loader = true
                this.viewGroup = false

                // load speakers
                this.getFirstSpeakersLimitedAccessByGroups()
                this.getSpeakersAllLimitedAccessByGroups()
            }

        })
    }

    speakerHeader(speaker, i, speakers) {
        if (i == 0) {
            return speaker.letter;
        } else if (i != 0 && speaker.letter != speakers[i - 1].letter) {
            return speaker.letter;
        }
    }



    async presentToast(time) {
        const toast = await this.toastController.create({
            message: time,
            duration: 3000
        });
        toast.present();
    }

    // loads more speakers when the infinite scroll is triggered
    moreSpeakers(event) {
        // verifies the type of vision of the module.
        if (this.typeVisionModule === this.GLOBAL_VISION) {
            this.moreSpeakersGlobalVision(event)
        }
        else if (this.typeVisionModule === this.GROUP_ACCESS_PERMISSION) {
            this.moreSpeakersLimitedAccessByGroups(event)
        }
        else {
            event.target.disabled = true;
        }
    }

    profileDetails(speaker) {
        let navigationExtras: NavigationExtras = {
            state: {
                user: speaker
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${this.moduleId}/4/${speaker.uid}`], navigationExtras);
        this.global.previousPage = null;
    }

    searchBar(ev) {
        if (ev.target.value.length >= 1) {
            let value = ev.target.value.toLowerCase();
            this.searchedSpeakers = [];
            this.allSpeakersList.filter(item => {
                if (item.name.toLowerCase().includes(value)) {
                    item.principal_title = this.getSpeakerPrincipalTitle(item.title);
                    this.searchedSpeakers.push(item);
                }
            })
        } else {
            this.searchedSpeakers = [];
        }
    }

    // load the event groups.
    getGroupsEvent() {
        this.dbGroups.getGroups(this.eventId, (groups) => {
            this.groups = groups

            // Sort the groups alphabetically.
            groups.sort(function (a, b) {
                return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
            });

            // checks the number of speakers in the module that belongs to each group.
            for (const group of groups) {
                const groupId = group.uid
                const moduleSpeakerId = this.moduleId

                group.qtdSessions = 0

                // count the number of speakers the group has in the module.
                this.dbGroups.contSpeakersOfGroup(groupId, moduleSpeakerId, (qtd: number) => {
                    group.qtdSessions = qtd
                })
            }

            this.loader = false
        })
    }

    /******************************************************************************** methods of global vision *************************************************************************************** */
    // loads the first 100 speakers of the module. global vision
    getFirstSpeakersGlobalVision() {
        this.dbSpeakers.getFirstASpeakersGlobalVision(this.typeOrder, this.moduleId, (data) => {
            let list = data

            //get the last participant
            if (list.length > 0) {
                this.lastSpeakers = data[data.length - 1];
            }

            list.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getSpeakerPrincipalTitle(element.title);
                this.speakers.push(element);
            });

            this.speakers = list
            this.loader = false

            // if (this.speakers.length == data.length) {
            //   this.loader = false;
            // }
        })
    }

    getSpeakerPrincipalTitle(title) {
        let principalTitle = '';
        switch (this.global.language) {
            case 'pt_BR': {
                principalTitle = title.PtBR;
                break;
            }
            case 'en_US': {
                principalTitle = title.EnUS;
                break;
            }
            case 'es_ES': {
                principalTitle = title.EsES;
                break;
            }
            case 'fr_FR': {
                principalTitle = title.FrFR;
                break;
            }
            case 'de_DE': {
                principalTitle = title.deDE;
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == '' || principalTitle == null) {
            if (title[this.convertLangFormat(this.global.event.language)] !== '') {
                principalTitle = title[this.convertLangFormat(this.global.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== '') {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        return principalTitle;
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    // get all speakers of the module
    getSpeakersAllGlobalVision() {
        this.subscriptions.push(this.dbSpeakers.getSpeakersAllGlobalVision(this.moduleId).subscribe((speakers) => {
            this.allSpeakersList = speakers;
        }))
    }

    // loads more speakers when the infinite scroll is triggered (global vision)
    moreSpeakersGlobalVision(event) {
        if (this.lastSpeakers) {
            this.dbSpeakers.getNextPageSpeakersGlobalVision(this.moduleId, this.lastSpeakers, this.typeOrder, (data) => {
                this.lastSpeakers = null

                if (data.length > 0) {
                    this.lastSpeakers = data[data.length - 1];
                }

                data.forEach(element => {
                    var letters = element.name.split('');
                    var letter = letters[0].toUpperCase();
                    element.letter = letter;
                    element.principal_title = this.getSpeakerPrincipalTitle(element.title);
                    this.speakers.push(element);
                });

                if (this.lastSpeakers) {
                    this.virtualScroll.checkEnd();
                    event.target.complete();
                } else {
                    event.target.disabled = true;
                }
            })

        } else {
            event.target.disabled = true;
        }
    }

    /******************************************************************************** methods of divided by groups *************************************************************************************** */

    // loads the speakers of the group.
    getSpeakersGroupDividByGroup(group) {
        const groupId = group.uid

        this.loader = true
        this.viewGroup = false

        // loads the group speakers.
        this.dbSpeakers.getSpeakersGroupGlobalVision(groupId, this.typeOrder, this.moduleId, (data) => {
            this.loader = false
            this.lastSpeakers = null

            if (data.length > 0) {
                this.lastSpeakers = data[data.length - 1]
            }

            data.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getSpeakerPrincipalTitle(element.title);
                this.speakers.push(element);
            });

            this.speakers = data
            this.allSpeakersList = data
        })
    }

    /******************************************************************************** methods of group vision *************************************************************************************** */
    // loads the speakers group speakers.
    getSpeakersGroupVision() {
        this.dbSpeakers.getSpeakersGroupsVision(this.userGroups, this.typeOrder, this.moduleId, (data) => {
            this.loader = false;
            this.lastSpeakers = null;

            if (data.length > 0) {
                this.lastSpeakers = data[data.length - 1];
            }

            data.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getSpeakerPrincipalTitle(element.title);
                this.speakers.push(element);
            });

            this.speakers = data;
            this.allSpeakersList = data;
        })
    }


    /******************************************************************************** methods of limited access by groups *************************************************************************************** */
    // loads the first 100 speakers of the module.
    getFirstSpeakersLimitedAccessByGroups() {
        // let start = Date.now();
        this.dbSpeakers.getFirstASpeakersLimitedAccessByGroups(this.typeOrder, this.moduleId, (data) => {
            // if (data) {
            // let millis = Date.now() - start;
            // this.presentToast("carregou em: " + millis + "ms - " + Date.now())
            // }
            let list = data

            //get the last participant
            if (list.length > 0) {
                this.lastSpeakers = data[data.length - 1];
            }

            list.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getSpeakerPrincipalTitle(element.title);
                this.speakers.push(element);
            });

            this.speakers = list
            this.loader = false

            // if (this.speakers.length == data.length) {
            //   this.loader = false;
            // }
        })
    }

    // get all speakers of the module
    getSpeakersAllLimitedAccessByGroups() {
        this.subscriptions.push(this.dbSpeakers.getSpeakersAllLimitedAccessByGroup(this.moduleId).subscribe((data) => {
            this.allSpeakersList = data
        }))
    }

    // loads more speakers when the infinite scroll is triggered 
    moreSpeakersLimitedAccessByGroups(event) {
        if (this.lastSpeakers) {
            let start = Date.now();

            this.dbSpeakers.getNextPageSpeakersLimitedAccessByGroup(this.moduleId, this.lastSpeakers, this.typeOrder, (data) => {
                this.lastSpeakers = null
                // if (data) {
                //   let millis = Date.now() - start;
                //   this.presentToast("carregou em: " + millis + "ms - " + Date.now())
                // }

                if (data.length > 0) {
                    this.lastSpeakers = data[data.length - 1];
                }

                data.forEach(element => {
                    var letters = element.name.split('');
                    var letter = letters[0].toUpperCase();
                    element.letter = letter;
                    element.principal_title = this.getSpeakerPrincipalTitle(element.title);
                    this.speakers.push(element);
                });

                if (this.lastSpeakers) {
                    this.virtualScroll.checkEnd();
                    event.target.complete();
                } else {
                    event.target.disabled = true;
                }
            })
        } else {
            event.target.disabled = true;
        }
    }


}

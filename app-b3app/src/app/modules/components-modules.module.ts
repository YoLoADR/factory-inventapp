import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SurveyPage } from './survey/survey.page';
import { RatingModule } from 'ng-starrating';
import { DatePickerModule } from 'ionic4-date-picker';
import { SharedModule } from '../shared/shared.module';
import { QuizPage } from './quiz/quiz.page';
import { QuizResultComponent } from '../content/reusable_components/interactivity/quiz-result/quiz-result.component';
import { ChartsModule } from 'ng2-charts';
import { GroupDiscussionListComponent } from './list-group-discussions/group-discussion-list/group-discussion-list.component';
import { RouterModule } from '@angular/router';
import { FilterGroupDiscussionsActivePipe } from '../pipes/filter-group-discussions-active/filter-group-discussions-active.pipe';
import { GroupDiscussionsDetailPage } from './group-discussions-detail/group-discussions-detail.page';
import { AvatarModule } from 'ngx-avatar';
import { ChatPage } from './chat/chat.page';
import { ChatSettingsPopupComponent } from '../components/chat-settings-popup/chat-settings-popup.component';
import { VisioConferenceModule } from '../content/reusable_components/visio-conference/visio-conference.module';

@NgModule({
    declarations: [
        ChatPage,
        ChatSettingsPopupComponent,
        FilterGroupDiscussionsActivePipe,
        GroupDiscussionsDetailPage,
        GroupDiscussionListComponent,
        QuizResultComponent,
        QuizPage,
        SurveyPage
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RatingModule,
        DatePickerModule,
        SharedModule,
        ChartsModule,
        RouterModule,
        AvatarModule,
        VisioConferenceModule
    ],
    exports: [
        ChatPage,
        ChatSettingsPopupComponent,
        FilterGroupDiscussionsActivePipe,
        GroupDiscussionsDetailPage,
        GroupDiscussionListComponent,
        QuizResultComponent,
        QuizPage,
        SurveyPage
    ],
    entryComponents: [
        ChatSettingsPopupComponent
    ]
})
export class ComponentsModulesModule { }

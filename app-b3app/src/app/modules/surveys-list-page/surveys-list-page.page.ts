import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { DaoSurveyService } from 'src/app/providers/db/dao-survey.service';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { GetSurveys } from 'src/app/shared/actions/interactivity.actions';
import { Survey } from 'src/app/models/survey';
import { AppState } from 'src/app/shared/reducers';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-surveys-list-page',
    templateUrl: './surveys-list-page.page.html',
    styleUrls: ['./surveys-list-page.page.scss'],
})
export class SurveysListPage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];

    surveyModule: any = null;
    surveys: Survey[] = [];

    eventId: string = null;
    moduleId: string = null;
    userId: string = null;

    backBtn: boolean = true;

    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;

    userLanguage: string = environment.platform.defaultLanguage;

    constructor(
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        public SGlobal: GlobalService,
        private menuCtrl: MenuController,
        private daoSurvey: DaoSurveyService,
        private dbAnalytics: DaoAnalyticsService,
        private SUtility: UtilityService
    ) {
        this.menuCtrl.enable(true);
        this.userId = this.SGlobal.userId;
        if (this.SGlobal.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.SGlobal.eventHomePage) {
            this.allowButtonsHeader = true;
        }

        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            this.loadColors();
        })
    }

    ngOnInit() { }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.store.dispatch(new GetSurveys({
            surveyModule: null,
            surveys: []
        }))
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    ionViewWillEnter() {
        this.getSurveys();
        // this.daoInteractivity.getInteractivityModule(this.eventId, (moduleInteractivity) => {
        //     this.SGlobal.loadService(() => {
        //         if (this.SGlobal.userLoaded || moduleInteractivity.answerOffline) {
        //         } else {
        //             this.SGlobal.userLogged();
        //         }
        //     });
        // })
    }

    /**
     * Getting all surveys for event
     */
    getSurveys() {
        this.daoSurvey.getSurveyModule(this.eventId).subscribe((modules) => {
            if (modules.length > 0) {
                this.surveyModule = modules[0];

                this.subscriptions.push(this.daoSurvey.getSurveysEvent(this.moduleId).subscribe((surveys) => {
                    this.surveys = [];
                    this.surveys = surveys;

                    this.store.dispatch(new GetSurveys({
                        surveyModule: this.surveyModule,
                        surveys: this.surveys
                    }))
                }));
            }
        })
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
    }

    ionViewWillLeave() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

}

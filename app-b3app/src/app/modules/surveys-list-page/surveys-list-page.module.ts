import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SurveysListPage } from './surveys-list-page.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { InteractivityModule } from 'src/app/content/reusable_components/interactivity/interactivity.module';

@NgModule({
    declarations: [SurveysListPage],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: SurveysListPage }
        ]),
        SharedModule,
        InteractivityModule
    ]
})
export class SurveysListModule { }

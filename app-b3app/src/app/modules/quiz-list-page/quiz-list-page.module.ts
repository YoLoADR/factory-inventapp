import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { QuizListPage } from './quiz-list-page.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { InteractivityModule } from 'src/app/content/reusable_components/interactivity/interactivity.module';

@NgModule({
    declarations: [QuizListPage],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: QuizListPage }
        ]),
        SharedModule,
        InteractivityModule
    ]
})
export class QuizListModule { }

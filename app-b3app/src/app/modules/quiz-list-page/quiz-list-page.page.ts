import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { DaoQuizService } from 'src/app/providers/db/dao-quiz.service';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { GetQuizs } from 'src/app/shared/actions/interactivity.actions';
import { AppState } from 'src/app/shared/reducers';
import { Store } from '@ngrx/store';
import { Quiz } from 'src/app/models/quiz';

@Component({
    selector: 'app-quiz-list-page',
    templateUrl: './quiz-list-page.page.html',
    styleUrls: ['./quiz-list-page.page.scss'],
})
export class QuizListPage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];

    quizModule: any = null
    quizs: Quiz[] = [];

    eventId: string = null;
    moduleId: string = null;
    userId: string = null;

    backBtn: boolean = true;

    userLanguage: string;

    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;

    constructor(
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        public SGlobal: GlobalService,
        private menuCtrl: MenuController,
        private daoQuiz: DaoQuizService,
        private dbAnalytics: DaoAnalyticsService,
        private SUtility: UtilityService
    ) {
        this.menuCtrl.enable(true);
        this.userId = this.SGlobal.userId;

        if (this.SGlobal.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.SGlobal.eventHomePage) {
            this.allowButtonsHeader = true;
        }

        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            this.loadColors();
        })
    }

    ngOnInit() { }
    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.store.dispatch(new GetQuizs({
            quizModule: null,
            quizs: []
        }))
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    ionViewWillEnter() {
        this.getQuizs();
        // this.daoInteractivity.getInteractivityModule(this.eventId, (moduleInteractivity) => {
        //     this.SGlobal.loadService(() => {
        //         if (this.SGlobal.userLoaded || moduleInteractivity.answerOffline) {
        //         } else {
        //             this.SGlobal.userLogged();
        //         }
        //     });
        // })
    }

    /**
     * Get quizs for modules
     */
    getQuizs() {
        this.daoQuiz.getQuizModule(this.eventId).subscribe((modules) => {
            if (modules.length > 0) {
                this.quizModule = modules[0];

                this.subscriptions.push(this.daoQuiz.getQuizsEvent(this.moduleId).subscribe((quizs) => {
                    this.quizs = [];
                    this.quizs = quizs;

                    this.store.dispatch(new GetQuizs({
                        quizModule: this.quizModule,
                        quizs: this.quizs
                    }))
                }));
            }
        });
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
    }

    /**
     * On leaving component
     */
    ionViewWillLeave() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}

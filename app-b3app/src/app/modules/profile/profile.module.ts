import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProfilePage } from './profile.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
  declarations: [ProfilePage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ProfilePage }
    ]),
    AvatarModule,
    SharedModule
  ],
  providers: [
    File,
    FileTransfer,
    FileTransferObject,
    FileOpener,
    Market
  ]
})
export class ProfileModule { }

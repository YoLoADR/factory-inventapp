import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { DaoSpeakersService } from 'src/app/providers/db/dao-speakers.service';
import {
    NavController,
    Events,
    MenuController,
    LoadingController,
    AlertController,
    Platform,
    ModalController
} from '@ionic/angular';
import {
    InAppBrowser,
    InAppBrowserOptions
} from '@ionic-native/in-app-browser/ngx';
import {
    GlobalService,
    WherebyService,
    UtilityService
} from 'src/app/shared/services';
import {
    FileTransfer,
    FileTransferObject,
} from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";
import { Market } from "@ionic-native/market/ngx";
import { TranslateService } from "@ngx-translate/core";
import { DaoAnalyticsService } from "src/app/providers/db/dao-analytics.service";
import { DaoModulesService } from "src/app/providers/db/dao-modules.service";
import { LightboxImgComponent } from "src/app/components/lightbox-img/lightbox-img.component";
import { environment } from "src/environments/environment";
import { take } from "rxjs/operators";
import { DaoEventsService } from "src/app/providers/db/dao-events.service";
import { Subscription } from "rxjs";
import { Location } from "@angular/common";
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
    providers: [DaoAttendeesService]
})
export class ProfilePage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    public typeUser: number;
    public eventId: string = null;
    public moduleId: string = null;
    public userId: string = null;
    public typeUserProfile: number = null;
    public loader: boolean = true;
    public company: string = null;
    public title: string = null;
    public userProfileId: string = null;
    public name: string = null;
    public site: string = null;
    public facebook: string = null;
    public instagram: string = null;
    public linkedin: string = null;
    public twitter: string = null;
    public phone: string = null;
    public description: string = null;
    public photoUrl: string = null;
    public type: number = null;
    public documents: Array<Document> = [];
    public allowChat: boolean = false;
    userModuleId: string = null;
    public listCustomFields: Array<any> = [];
    public listCustomFieldOptions: Array<any> = [];
    public fieldsCustom: any;
    public listSessions = [];
    doc;
    loaderToast;

    // inapbrowser
    options: InAppBrowserOptions = {};
    backBtn: boolean = true;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    isDegrade: boolean = false;

    theUser;
    menuBadge: number = 0;
    firstAccess: boolean = true;
    showCardCustomField: boolean = false;
    moduleName: string = '';
    allowButtonsHeader: boolean = false;
    userLanguage: string = environment.platform.defaultLanguage;
    eventLanguage: string = environment.platform.defaultLanguage;
    userLanguageFormated: string = environment.platform.defaultLanguage;
    module;
    event: any;
    constructor(
        private route: ActivatedRoute,
        private dbAttendees: DaoAttendeesService,
        private dbSpeakers: DaoSpeakersService,
        private navCtrl: NavController,
        private theInAppBrowser: InAppBrowser,
        public global: GlobalService,
        private router: Router,
        private events: Events,
        private menuCtrl: MenuController,
        private transfer: FileTransfer,
        private file: File,
        private gdService: GroupDiscussionsService,
        private fileOpener: FileOpener,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private market: Market,
        private translateService: TranslateService,
        private dbAnalytics: DaoAnalyticsService,
        private platform: Platform,
        private dbEvents: DaoEventsService,
        private dbModules: DaoModulesService,
        private modalCtrl: ModalController,
        private SWhereby: WherebyService,
        private SUtility: UtilityService,
        private location: Location
    ) {
        this.options = {
            location: 'no', //Or 'yes'
            hidden: 'no', //Or  'yes'
            clearcache: 'yes',
            clearsessioncache: 'yes',
            hideurlbar: 'yes',
            zoom: 'yes', //Android only ,shows browser zoom controls
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no', //Android only
            closebuttoncaption: this.translateService.instant(
                'global.buttons.back'
            ), //iOS and Android
            hidenavigationbuttons: 'yes',
            disallowoverscroll: 'no', //iOS only
            toolbar: 'yes', //iOS only
            enableViewportScale: 'yes', //iOS only
            allowInlineMediaPlayback: 'yes', //iOS only
            presentationstyle: 'pagesheet', //iOS only
            fullscreen: 'yes' //Windows only
        };

        this.menuCtrl.enable(true);

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.userProfileId = params.userId;
            this.userId = this.global.userId;
            this.typeUser = this.global.userType;
            this.typeUserProfile = params.type;
            this.moduleId = params.moduleId;
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.menuBadge = this.global.notificationBadge;
            });
            this.route.queryParams.subscribe((params) => {
                if (this.router.getCurrentNavigation().extras.state) {
                    this.theUser = this.router.getCurrentNavigation().extras.state.user;
                }
            });
            this.getEvent();
            this.getModule();
        });
    }

    getEvent() {
        this.subscriptions.push(
            this.dbEvents.getEvent(this.eventId).subscribe((event) => {
                this.event = event;
            })
        );
    }

    getModule() {
        this.subscriptions.push(
            this.dbModules.getModule(this.moduleId).subscribe((module) => {
                this.module = module;
                this.moduleName = module.name;
            })
        );
    }

    btn_color1: string = null;
    btn_color2: string = null;
    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        if (this.menu_color.includes('linear')) {
            this.isDegrade = true;
            let colors = this.separeGradientColors(this.menu_color);
            this.btn_color1 = colors[0];
            this.btn_color2 = colors[1];
        }
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    separeGradientColors(str) {
        let hex = [];
        let index = str.indexOf('#');

        while (index > -1) {
            hex.push(this.getHexColors(str));
            str = str.replace('#', '');
            index = str.indexOf('#');
        }

        return hex;
    }

    getHexColors(str) {
        let position = str.indexOf('#');
        let result = str.substring(position, position + 7);

        return result;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        console.log("Coucou profile");
        this.userLanguage = this.global.language;
        this.eventLanguage = this.global.event.language;
        this.userLanguageFormated = this.convertLangFormat(
            this.global.language
        );
        if (this.typeUserProfile == 5) {
            this.loadAttendee();
        } else if (this.typeUserProfile == 4) {
            this.loadSpeaker();
        } else {
            // invalid view profile
            this.location.back();
        }

        if (this.router.url == this.global.eventHomePage) {
            this.allowButtonsHeader = true;
        }
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach((sub) => sub.unsubscribe());
    }

    // load attendee
    loadAttendee() {
        this.subscriptions.push(this.dbAttendees.getAttendeeByEvent(
            this.eventId,
            this.userProfileId).subscribe((attendee) => {
                let aux = this.getSpeakerAttendeePrincipalTitle(
                    attendee['title'],
                    attendee['description']
                );
                this.type = attendee['type'];
                this.name = attendee['name'];
                this.company = attendee['company'];
                this.title = aux.title;
                this.description = aux.description;
                this.photoUrl = attendee['photoUrl'];
                this.phone = attendee['phone'];
                this.site = attendee['website'];
                this.facebook = attendee['facebook'];
                this.instagram = attendee['instagram'];
                this.linkedin = attendee['linkedin'];
                this.twitter = attendee['twitter'];
                this.userModuleId = attendee['moduleId'];
                this.firstAccess = attendee['firstAccess'];
                this.getChatStatus();
                this.loader = false;
            }
            ));
        this.getFieldsCustomAttendee();
    }

    getChatStatus() {
        this.dbAttendees.getChatStatus(this.userModuleId, (data) => {
            if (this.userId == this.userProfileId) {
                this.allowChat = false;
            } else if (this.userId == null) {
                this.allowChat = false;
            } else {
                this.allowChat = data;
            }
        });
    }

    getFieldsCustomAttendee() {
        this.dbAttendees.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.fieldsCustom = fields;
            this.getCustomFieldsAttendee();
        });
    }

    getCustomFieldsAttendee() {
        this.dbAttendees.getCustomFields(
            this.eventId,
            this.userProfileId,
            (customFields) => {
                for (let custom of customFields) {
                    custom.activeView = false;
                }
                let displayCardCustom = false;
                for (let aux of customFields) {
                    let position = this.fieldsCustom[aux.uid].order;
                    this.listCustomFields[position] = aux;
                    if (displayCardCustom == false) {
                        if (aux.type == 'select') {
                            if (
                                aux.value[this.userLanguageFormated] !== null &&
                                aux.value[this.userLanguageFormated] !==
                                    undefined &&
                                aux.value[this.userLanguageFormated] !== '' &&
                                (aux.exibicao == '1' ||
                                    (aux.exibicao == '2' &&
                                        this.userProfileId === this.userId))
                            ) {
                                displayCardCustom = true;
                                this.showCardCustomField = true;
                            }
                        } else {
                            if (
                                aux.textValue[this.userLanguageFormated] !==
                                    null &&
                                aux.textValue[this.userLanguageFormated] !==
                                    undefined &&
                                aux.textValue[this.userLanguageFormated] !==
                                    '' &&
                                (aux.exibicao == '1' ||
                                    (aux.exibicao == '2' &&
                                        this.userProfileId === this.userId))
                            ) {
                                displayCardCustom = true;
                                this.showCardCustomField = true;
                            }
                        }
                    }
                }

                for (let i = 0; i < this.listCustomFields.length; i++) {
                    let custom = this.listCustomFields[i];

                    if (custom.type == 'select') {
                        this.dbAttendees.getCustomFieldOptions(
                            this.moduleId,
                            this.userProfileId,
                            custom.uid,
                            (listOptions) => {
                                for (let option of listOptions) {
                                    if (custom.value == option.uid) {
                                        custom.value = option.answer;
                                    }
                                }
                            }
                        );
                    }

                    //se a exibição for somente para administrador e o tipo de usuário for supergod, god ou cliente
                    if (
                        (custom.exibicao == 3 || custom.exibicao == 2) &&
                        (this.typeUser == 0 ||
                            this.typeUser == 1 ||
                            this.typeUser == 2)
                    ) {
                        custom.activeView = true;
                    }

                    //se a exibição for somente para clientes e o tipo de usuário for attendee
                    if (
                        custom.exibicao == 2 &&
                        this.userId == this.userProfileId
                    ) {
                        custom.activeView = true;
                    }

                    //se o tipo de exibição for para todos
                    if (custom.exibicao == 1) {
                        custom.activeView = true;
                    }
                }
            }
        );
    }

    // load speaker
    loadSpeaker() {
        this.subscriptions.push(this.dbSpeakers.getSpeakerByEvent(
            this.eventId,
            this.userProfileId).subscribe((speaker) => {
                // this.userProfileId = this.theUser['uid'];
                let aux = this.getSpeakerAttendeePrincipalTitle(
                    speaker['title'],
                    speaker['description']
                );
                this.type = speaker['type'];
                this.name = speaker['name'];
                this.company = speaker['company'];
                this.title = aux.title;
                this.description = aux.description;
                this.photoUrl = speaker['photoUrl'];
                this.phone = speaker['phone'];
                this.site = speaker['website'];
                this.facebook = speaker['facebook'];
                this.instagram = speaker['instagram'];
                this.linkedin = speaker['linkedin'];
                this.twitter = speaker['twitter'];
                this.firstAccess = speaker['firstAccess'];
                for (let uid in speaker['documents']) {
                    this.documents.push(speaker['documents'][uid]);
                }
                this.loader = false;
            }
            ));

        this.getFieldsCustomSpeaker();
        this.getSessionsOfSpeaker();
    }

    getFieldsCustomSpeaker() {
        this.dbSpeakers.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.fieldsCustom = fields;
            this.getCustomFieldsSpeakers();
        });
    }

    getCustomFieldsSpeakers() {
        this.dbSpeakers.getCustomFields(
            this.eventId,
            this.userProfileId,
            (customFields) => {
                for (let custom of customFields) {
                    custom.activeView = false;
                }

                let displayCardCustom = false;
                for (let aux of customFields) {
                    let position = this.fieldsCustom[aux.uid].order;
                    this.listCustomFields[position] = aux;

                    if (displayCardCustom == false) {
                        if (aux.type == 'select') {
                            if (
                                aux.value[this.userLanguageFormated] !== null &&
                                aux.value[this.userLanguageFormated] !==
                                    undefined &&
                                aux.value[this.userLanguageFormated] !== '' &&
                                (aux.exibicao == '1' ||
                                    (aux.exibicao == '2' &&
                                        this.userProfileId === this.userId))
                            ) {
                                displayCardCustom = true;
                                this.showCardCustomField = true;
                            }
                        } else {
                            if (
                                aux.textValue[this.userLanguageFormated] !==
                                    null &&
                                aux.textValue[this.userLanguageFormated] !==
                                    undefined &&
                                aux.textValue[this.userLanguageFormated] !==
                                    '' &&
                                (aux.exibicao == '1' ||
                                    (aux.exibicao == '2' &&
                                        this.userProfileId === this.userId))
                            ) {
                                displayCardCustom = true;
                                this.showCardCustomField = true;
                            }
                        }
                    }
                }

                for (let i = 0; i < this.listCustomFields.length; i++) {
                    let custom = this.listCustomFields[i];

                    if (custom.type == 'select') {
                        this.dbSpeakers.getCustomFieldOptions(
                            this.moduleId,
                            custom.uid,
                            (listOptions) => {
                                for (let option of listOptions) {
                                    if (custom.value == option.uid) {
                                        custom.value = option.answer;
                                    }
                                }
                            }
                        );
                    }

                    //se a exibição for somente para administrador e o tipo de usuário for supergod, god ou cliente
                    if (
                        (custom.exibicao == 3 || custom.exibicao == 2) &&
                        (this.typeUser == 0 ||
                            this.typeUser == 1 ||
                            this.typeUser == 2)
                    ) {
                        custom.activeView = true;
                    }

                    //se a exibição for somente para clientes e o tipo de usuário for attendee
                    if (
                        custom.exibicao == 2 &&
                        this.userId == this.userProfileId
                    ) {
                        custom.activeView = true;
                    }

                    //se o tipo de exibição for para todos
                    if (custom.exibicao == 1) {
                        custom.activeView = true;
                    }
                }
            }
        );
    }

    // lists the speaker's sessions.
    getSessionsOfSpeaker() {
        this.dbSpeakers.getSessionsOfSpeaker(
            this.userProfileId,
            this.eventId,
            (data) => {
                this.listSessions = [];
                for (let session of data) {
                    // locations of session
                    const locations = [];

                    for (const uid in session.locations) {
                        locations.push(session.locations[uid]);
                    }

                    session.locations = locations;

                    // tracks of session
                    const tracks = [];

                    for (const uid in session.tracks) {
                        tracks.push(session.tracks[uid]);
                    }

                    session.tracks = tracks;

                    // groups of session
                    const groups = [];

                    for (const uid in session.groups) {
                        groups.push(session.groups[uid]);
                    }

                    session.groups = groups;
                }
                this.listSessions = data;
            }
        );
    }

    // puts the session date in front.
    dateHeader(session, index, sessionsList) {
        if (index == 0) {
            return session.date;
        } else if (index != 0 && session.date != sessionsList[index - 1].date) {
            return session.date;
        }
    }

    // redirects to schedule-detail page with uid and session module.
    openSession(session) {
        const sessionId = session.uid;
        const moduleScheduleId = session.moduleId;

        this.router.navigate([
            `/event/${this.eventId}/schedule-detail/${moduleScheduleId}/${sessionId}`
        ]);
    }

    // zoom profile picture image
    openImage(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl
            .create({
                component: LightboxImgComponent,
                componentProps: {
                    img: url
                }
            })
            .then((modal) => {
                modal.present();
            });
    }

    openLink(url: string) {
        let pattern = new RegExp('^(https?|ftp)://');
        if (!pattern.test(url)) url = 'https://' + url;
        if (!this.global.isBrowser) {
            let target = '_system';
            this.theInAppBrowser.create(url, target, this.options);
        } else {
            window.open(url, '_system');
        }
    }

    openDocument(document) {
        this.doc = document;
        let platforms = this.platform.platforms().join();
        if (platforms.includes('hybrid') && platforms.includes('cordova')) {
            if (platforms.includes('android')) {
                if (
                    document.type == 'pdf' ||
                    document.type == 'docx' ||
                    document.type == 'doc' ||
                    document.type == 'docm' ||
                    document.type == 'txt' ||
                    document.type == 'rtf' ||
                    document.type == 'csv' ||
                    document.type == 'pptx' ||
                    document.type == 'ppt' ||
                    document.type == 'thmx' ||
                    document.type == 'ppsx' ||
                    document.type == 'pps' ||
                    document.type == 'ppsm' ||
                    document.type == 'xlsx' ||
                    document.type == 'xls' ||
                    document.type == 'xlsm' ||
                    document.type == 'xlt' ||
                    document.type == 'xlsb' ||
                    document.type == 'xltx' ||
                    document.type == 'xltm' ||
                    document.type == 'mp4' ||
                    document.type == 'wmv' ||
                    document.type == '3gp' ||
                    document.type == 'avi' ||
                    document.type == 'mp3' ||
                    document.type == 'wav' ||
                    document.type == 'jpg' ||
                    document.type == 'jpeg' ||
                    document.type == 'png' ||
                    document.type == 'gif' ||
                    document.type == 'bmp'
                ) {
                    this.presentLoading();
                    this.openInAndroid()
                        .then((res: any) => {
                            let fileExtn = this.doc.type;
                            let fileMIMEType = this.getMIMEtype(fileExtn);
                            this.fileOpener
                                .open(res, fileMIMEType)
                                .then(() => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 50);
                                })
                                .catch((e) => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 50);
                                    console.log('Error openening file', e);
                                });
                        })
                        .catch(async (e) => {
                            if (
                                document.type == 'png' ||
                                document.type == 'jpg' ||
                                document.type == 'jpeg' ||
                                document.type == 'gif' ||
                                document.type == 'bmp' ||
                                document.type == 'js' ||
                                document.type == 'html' ||
                                document.type == 'css' ||
                                document.type == 'htm' ||
                                document.type == 'txt'
                            ) {
                                window.open(
                                    document.url,
                                    '_system',
                                    'location=yes'
                                );
                            } else {
                                let urlFinal = encodeURIComponent(document.url);
                                window.open(
                                    'https://docs.google.com/viewer?url=' +
                                        urlFinal,
                                    '_system',
                                    'location=yes'
                                );
                            }
                        });
                } else {
                    let urlFinal = encodeURIComponent(this.doc.url);
                    window.open(
                        'https://docs.google.com/viewer?url=' + urlFinal,
                        '_system',
                        'location=yes'
                    );
                    // this.theInAppBrowser.create('https://docs.google.com/viewer?url=' + urlFinal, '_system', 'location=yes', this.options);
                }
            } else if (platforms.includes('ios')) {
                if (
                    document.type == 'pdf' ||
                    document.type == 'docx' ||
                    document.type == 'doc' ||
                    document.type == 'docm' ||
                    document.type == 'txt' ||
                    document.type == 'rtf' ||
                    document.type == 'csv' ||
                    document.type == 'pptx' ||
                    document.type == 'ppt' ||
                    document.type == 'ppsx' ||
                    document.type == 'pps' ||
                    document.type == 'ppsm' ||
                    document.type == 'xlsx' ||
                    document.type == 'xls' ||
                    document.type == 'xlsm' ||
                    document.type == 'xlt' ||
                    document.type == 'xlsb' ||
                    document.type == 'xltx' ||
                    document.type == 'xltm' ||
                    document.type == 'mp4' ||
                    document.type == 'wmv' ||
                    document.type == '3gp' ||
                    document.type == 'avi' ||
                    document.type == 'mp3' ||
                    document.type == 'wav' ||
                    document.type == 'jpg' ||
                    document.type == 'jpeg' ||
                    document.type == 'png' ||
                    document.type == 'gif' ||
                    document.type == 'bmp'
                ) {
                    this.presentLoading();
                    this.openInIos()
                        .then((res: any) => {
                            let fileExtn = this.doc.type;
                            let fileMIMEType = this.getMIMEtype(fileExtn);
                            this.fileOpener
                                .open(res, fileMIMEType)
                                .then(() => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 200);
                                })
                                .catch((e) => {
                                    if (e == 'Missing Command Error') {
                                        if (
                                            document.type == 'png' ||
                                            document.type == 'jpg' ||
                                            document.type == 'jpeg' ||
                                            document.type == 'gif' ||
                                            document.type == 'bmp' ||
                                            document.type == 'js' ||
                                            document.type == 'html' ||
                                            document.type == 'css' ||
                                            document.type == 'htm' ||
                                            document.type == 'txt'
                                        ) {
                                            // window.open(document.url, '_system', 'location=yes');
                                            if (!this.global.isBrowser) {
                                                this.theInAppBrowser.create(
                                                    document.url,
                                                    '_system',
                                                    this.options
                                                );
                                            } else {
                                                window.open(document.url);
                                            }
                                        } else {
                                            if (!this.global.isBrowser) {
                                                let urlFinal = encodeURIComponent(
                                                    document.url
                                                );
                                                this.theInAppBrowser.create(
                                                    'https://docs.google.com/viewer?url=' +
                                                        urlFinal,
                                                    '_system',
                                                    this.options
                                                );
                                            } else {
                                                window.open(document.url);
                                            }
                                            // window.open('https://docs.google.com/viewer?url=' + document.url, '_system', 'location=yes');
                                        }
                                    } else {
                                        setTimeout(() => {
                                            this.closeLoading();
                                        }, 200);
                                        this.presentAlertConfirm();
                                    }

                                    console.log('Error openening file', e);
                                });
                        })
                        .catch((e) => {
                            setTimeout(() => {
                                this.closeLoading();
                            }, 50);
                            if (
                                document.type == 'png' ||
                                document.type == 'jpg' ||
                                document.type == 'jpeg' ||
                                document.type == 'gif' ||
                                document.type == 'bmp' ||
                                document.type == 'js' ||
                                document.type == 'html' ||
                                document.type == 'css' ||
                                document.type == 'htm' ||
                                document.type == 'txt'
                            ) {
                                // window.open(document.url, '_system', 'location=yes');
                                if (!this.global.isBrowser) {
                                    this.theInAppBrowser.create(
                                        document.url,
                                        '_system',
                                        this.options
                                    );
                                } else {
                                    window.open(document.url);
                                }
                            } else {
                                if (!this.global.isBrowser) {
                                    let urlFinal = encodeURIComponent(
                                        document.url
                                    );
                                    this.theInAppBrowser.create(
                                        'https://docs.google.com/viewer?url=' +
                                            urlFinal,
                                        '_system',
                                        this.options
                                    );
                                } else {
                                    window.open(document.url);
                                }
                                // window.open('https://docs.google.com/viewer?url=' + document.url, '_system', 'location=yes');
                            }
                        });
                } else {
                    if (
                        document.type == 'png' ||
                        document.type == 'jpg' ||
                        document.type == 'jpeg' ||
                        document.type == 'gif' ||
                        document.type == 'bmp' ||
                        document.type == 'js' ||
                        document.type == 'html' ||
                        document.type == 'css' ||
                        document.type == 'htm' ||
                        document.type == 'txt'
                    ) {
                        window.open(document.url, '_system', 'location=yes');
                    } else {
                        let urlFinal = encodeURIComponent(document.url);
                        window.open(
                            'https://docs.google.com/viewer?url=' + urlFinal,
                            '_system',
                            'location=yes'
                        );
                    }
                }
            } else {
                this.openInweb(document);
            }
        } else {
            this.openInweb(document);
        }
        this.dbAnalytics.documentAccess(
            this.eventId,
            document.uid,
            this.global.event.timezone
        );
    }

    openInweb(document) {
        if (
            document.type == 'png' ||
            document.type == 'jpg' ||
            document.type == 'jpeg' ||
            document.type == 'gif' ||
            document.type == 'bmp' ||
            document.type == 'js' ||
            document.type == 'html' ||
            document.type == 'css' ||
            document.type == 'htm' ||
            document.type == 'txt'
        ) {
            window.open(document.url, '_system', 'location=yes');
        } else {
            let urlFinal = encodeURIComponent(document.url);
            // window.open('https://docs.google.com/viewer?url=' + urlFinal, '_system', 'location=yes');
            window.open(document.url, '_system', 'location=yes');
        }
    }

    getMIMEtype(extn) {
        let ext = extn.toLowerCase();
        let MIMETypes = {
            // texts
            txt: 'text/plain',
            rtf: 'application/rtf',
            csv: 'text/csv',
            docx:
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            doc: 'application/msword',
            docm: 'application/vnd.ms-word.document.macroEnabled.12',
            // pdf
            pdf: 'application/pdf',
            // images
            jpg: 'image/jpeg',
            bmp: 'image/bmp',
            png: 'image/png',
            gif: 'image/gif',
            // sheets
            xls: 'application/vnd.ms-excel',
            xlsx:
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            xlsm: 'application/vnd.ms-excel.sheet.macroEnabled.12',
            xlt: 'application/vnd.ms-excel',
            xlsb: 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            xltx:
                'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            xltm: 'application/vnd.ms-excel.template.macroEnabled.12',
            // presentations (ppt)
            ppt: 'application/vnd.ms-powerpoint',
            pptx:
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            ppsx:
                'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            pps: 'application/vnd.ms-powerpoint',
            ppsm: 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'
        };
        return MIMETypes[ext];
    }

    openInAndroid() {
        return new Promise((resolve, reject) => {
            this.file
                .createDir(this.file.externalRootDirectory, '_downloads', false)
                .then((response) => {
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    fileTransfer
                        .download(
                            this.doc.url,
                            this.file.externalRootDirectory +
                                '/_downloads/' +
                                this.doc.name[this.userLanguageFormated]
                        )
                        .then((entry) => {
                            resolve(entry.toURL());
                        })
                        .catch((err) => {
                            reject();
                        });
                })
                .catch((err) => {
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    fileTransfer
                        .download(
                            this.doc.url,
                            this.file.externalRootDirectory +
                                '/_downloads/' +
                                this.doc.name[this.userLanguageFormated]
                        )
                        .then((entry) => {
                            resolve(entry.toURL());
                        })
                        .catch((err) => {
                            reject();
                        });
                });
        });
    }

    openInIos() {
        return new Promise((resolve, reject) => {
            // ********* IMPORTANT: IOS NEED TO CHANGE EXTERNALROOTDIRECTORY TO OTHER PATH ****** //
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer
                .download(
                    this.doc.url,
                    this.file.dataDirectory +
                        this.doc.name[this.userLanguageFormated]
                )
                .then((entry) => {
                    resolve(entry.toURL());
                })
                .catch((err) => {
                    reject();
                });
        });
    }

    async presentLoading() {
        this.loaderToast = await this.loadingCtrl.create({
            spinner: 'crescent',
            duration: 5000
        });
        await this.loaderToast.present();
    }

    async closeLoading() {
        await this.loaderToast.dismiss();
    }

    async presentAlertConfirm() {
        const alert = await this.alertCtrl.create({
            header: this.translateService.instant(
                'global.alerts.not_found_title'
            ),
            message: this.translateService.instant(
                'global.alerts.app_open_doc_not_found_txt'
            ),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.no'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {}
                },
                {
                    text: this.translateService.instant('global.buttons.yes'),
                    handler: () => {
                        this.openMarketApp();
                    }
                }
            ]
        });

        await alert.present();
    }

    openMarketApp() {
        let platforms = this.platform.platforms().join();
        if (platforms.includes('android')) {
            let androidPackage = this.getPackageNameAndroid(this.doc.type);
            this.market.open(androidPackage);
        } else if (platforms.includes('ios')) {
            let iosPackage = this.getPackageNameIos(this.doc.type);
            this.market.open(iosPackage);
        }
    }

    getPackageNameAndroid(extn) {
        let ext = extn.toLowerCase();
        let packageNames = {
            // texts
            docx: 'com.google.android.apps.docs.editors.docs',
            doc: 'com.google.android.apps.docs.editors.docs',
            docm: 'com.google.android.apps.docs.editors.docs',
            // pdf
            pdf: 'com.adobe.reader',
            // sheets
            xls: 'com.google.android.apps.docs.editors.sheets',
            xlsx: 'com.google.android.apps.docs.editors.sheets',
            xlsm: 'com.google.android.apps.docs.editors.sheets',
            xlt: 'com.google.android.apps.docs.editors.sheets',
            xlsb: 'com.google.android.apps.docs.editors.sheets',
            xltx: 'com.google.android.apps.docs.editors.sheets',
            xltm: 'com.google.android.apps.docs.editors.sheets',
            // presentations (ppt)
            ppt: 'com.google.android.apps.docs.editors.slides',
            pptx: 'com.google.android.apps.docs.editors.slides',
            ppsx: 'com.google.android.apps.docs.editors.slides',
            pps: 'com.google.android.apps.docs.editors.slides',
            ppsm: 'com.google.android.apps.docs.editors.slides'
        };
        return packageNames[ext];
    }

    getPackageNameIos(extn) {
        let ext = extn.toLowerCase();
        let packageNames = {
            // texts
            docx: 'documentos-google/id842842640',
            doc: 'documentos-google/id842842640',
            docm: 'documentos-google/id842842640',
            // pdf
            pdf: 'adobe-acrobat-reader/id469337564',
            // sheets
            xls: 'planilhas-google/id842849113',
            xlsx: 'planilhas-google/id842849113',
            xlsm: 'planilhas-google/id842849113',
            xlt: 'planilhas-google/id842849113',
            xlsb: 'planilhas-google/id842849113',
            xltx: 'planilhas-google/id842849113',
            xltm: 'planilhas-google/id842849113',
            // presentations (ppt)
            ppt: 'apresentações-google/id879478102',
            pptx: 'apresentações-google/id879478102',
            ppsx: 'apresentações-google/id879478102',
            pps: 'apresentações-google/id879478102',
            ppsm: 'apresentações-google/id879478102'
        };
        return packageNames[ext];
    }

    popPage() {
        this.navCtrl.pop();
    }

    getSpeakerAttendeePrincipalTitle(title, description) {
        let principalTitle = '';
        let principalDescription = '';
        switch (this.global.language) {
            case 'pt_BR': {
                principalTitle = title.PtBR ? title.PtBR : '';
                principalDescription = description.PtBR ? description.PtBR : '';
                break;
            }
            case 'en_US': {
                principalTitle = title.EnUS ? title.EnUS : '';
                principalDescription = description.EnUS ? description.EnUS : '';
                break;
            }
            case 'es_ES': {
                principalTitle = title.EsES ? title.EsES : '';
                principalDescription = description.EsES ? description.EsES : '';
                break;
            }
            case 'fr_FR': {
                principalTitle = title.FrFR ? title.FrFR : '';
                principalDescription = description.FrFR ? description.FrFR : '';
                break;
            }
            case 'de_DE': {
                principalTitle = title.DeDE ? title.DeDE : '';
                principalDescription = description.DeDE ? description.DeDE : '';
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == '' || principalTitle == null) {
            if (
                title[this.convertLangFormat(this.global.event.language)] !== ''
            ) {
                principalTitle =
                    title[this.convertLangFormat(this.global.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== '') {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        if (principalDescription == '' || principalDescription == null) {
            if (
                description[
                    this.convertLangFormat(this.global.event.language)
                ] !== ''
            ) {
                principalDescription =
                    description[
                        this.convertLangFormat(this.global.event.language)
                    ];
            } else {
                for (let aux in description) {
                    if (description[aux] !== '') {
                        principalDescription = description[aux];
                    }
                }
            }
        }
        return {
            title: principalTitle,
            description: principalDescription
        };
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR';
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    async openChat() {
        if (this.firstAccess) {
            // usuário ainda não pode receber msgs
            const alert = await this.alertCtrl.create({
                header: this.translateService.instant('pages.chat.chat'),
                message: this.translateService.instant(
                    'global.alerts.user_chat_not_available'
                ),
                buttons: [this.translateService.instant('global.buttons.ok')]
            });

            await alert.present();
        } else {
            const chatId = await this.gdService.getOrCreateChat(
                this.eventId,
                this.global.userId,
                this.userProfileId
            );
            // vai para o chat
            this.router.navigate([`/event/${this.eventId}/chat/${chatId}`]);
        }
    }

    /**
     * Create and send mess
     */
    async createVisio() {
        try {
            if (this.userId != this.userProfileId) {
                /*this.SWhereby.sendMessageVisioOnChat(this.eventId, [this.userId, this.userProfileId]).pipe(take(1)).subscribe((status) => {
                    if (status) {
                        // Analytics created new room for 2
                        this.SWhereby.analyticsNewRoomFor2(this.eventId, [this.userId, this.userProfileId]);

                        this.SUtility.presentToast(this.translateService.instant('global.toasts.visios.created'), 2500, 'bottom', false, 'primary');
                        this.openChat();
                    } else {
                        this.SUtility.presentToast(this.translateService.instant('global.toasts.visios.error'), 2500, 'bottom', false, 'danger');
                    }
                });*/
            } else {
                this.SUtility.presentToast(
                    this.translateService.instant(
                        'global.toasts.visios.error-same-user'
                    ),
                    2500,
                    'bottom',
                    false,
                    'danger'
                );
            }
        } catch (error) {
            this.SUtility.presentToast(
                this.translateService.instant('global.toasts.visios.error'),
                2500,
                'bottom',
                false,
                'danger'
            );
        }
    }
}

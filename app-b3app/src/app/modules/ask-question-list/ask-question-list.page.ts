import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { DaoAskQuestionService } from 'src/app/providers/db/dao-ask-queston.service';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-ask-question-list',
    templateUrl: './ask-question-list.page.html',
    styleUrls: ['./ask-question-list.page.scss'],
})
export class AskQuestionListPage implements OnInit {
    subscriptions: Subscription[] = [];

    questionModule: any = null;

    eventId: string = null;
    moduleId: string = null;
    userId: string = null;

    listItems: any[] = [];

    allowButtonsHeader: boolean = false;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    bg_content_color: string = null;

    loader: boolean = true;

    userLanguage: string;

    constructor(private route: ActivatedRoute,
        private router: Router,
        public SGlobal: GlobalService,
        private menuCtrl: MenuController,
        private daoAskQuestion: DaoAskQuestionService,
        private dbAnalytics: DaoAnalyticsService,
        private SUtility: UtilityService
    ) {
        this.menuCtrl.enable(true);
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            this.loadColors();
        })
    }

    ngOnInit() {
        this.daoAskQuestion.getQuestionsModule(this.moduleId).subscribe((questionModule) => {
            this.questionModule = questionModule;
        })

        if (this.router.url == this.SGlobal.eventHomePage) {
            this.allowButtonsHeader = true;
        }
        this.getAskQuestions();
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
    }

    /**
     * Get ask questions
     */
    getAskQuestions() {
        this.subscriptions.push(this.daoAskQuestion.getAskQuestions(this.moduleId).subscribe((listItems) => {
            this.listItems = listItems;
            this.loader = false;
        }));
    }

    /**
     * Open question module
     * @param item 
     */
    openItem(item) {
        this.router.navigate([`/event/${this.eventId}/ask-question/${this.moduleId}/${item.uid}`]);
    }

    /**
     * On leaving component
     */
    ionViewWillLeave() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }

    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SchedulePage } from './schedule.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { DaoScheduleService } from 'src/app/providers/db/dao-schedule.service';

@NgModule({
  declarations: [SchedulePage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SchedulePage }
    ]),
    SharedModule
  ],
  providers: [DaoScheduleService]
})
export class ScheduleModule {
}

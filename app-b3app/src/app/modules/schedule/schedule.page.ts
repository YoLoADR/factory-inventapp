import { Component, ViewChild, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { IonInfiniteScroll, IonVirtualScroll, Events, MenuController } from '@ionic/angular';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { GlobalService } from 'src/app/shared/services';
import { ToastController } from '@ionic/angular';
import { CeuTrack } from 'src/app/models/ceu-track';
import { TranslateService } from '@ngx-translate/core';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { DaoScheduleService } from 'src/app/providers/db/dao-schedule.service';
import { DaoTrackService } from 'src/app/providers/db/dao-tracks.service';
import { TypeVisionModule } from '../../models/type-vision-module';
import { DaoGroupsService } from '../../providers/db/dao-groups.service';
import { CeuGroup } from 'src/app/models/ceu-group';
import { DaoSessionFeedbackService } from 'src/app/providers/db/dao-session-feedback.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/shared/reducers';
import { GetScheduleSessions } from 'src/app/shared/actions/schedules.actions';

@Component({
    selector: 'app-schedule',
    templateUrl: './schedule.page.html',
    styleUrls: ['./schedule.page.scss'],
})

export class SchedulePage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    public module = null
    cancelTxt: string = null;

    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonVirtualScroll, { static: false }) virtualScroll: IonVirtualScroll;
    allowButtonsHeader: boolean = false;

    public slideOpts = {
        effect: 'flip',
        slidesPerView: 6,
        slidesPerColumn: 1,
        updateOnWindowResize: true,
        centerInsufficientSlides: true,
    };

    eventId: string = null;
    moduleId: string = null;
    sessionFeedbackModule: string = null;

    lastSession = null

    loader: boolean;
    loaderByTrack: boolean = false;

    habilitedLimit: boolean = false

    language: string = null

    selectedDate = 'all'
    private sessions = []
    private sessionsAll = []
    private dates: Array<string> = []

    tracks: Array<CeuTrack> = []
    selectedTrackId = null

    groups = [] //groups of the event.
    userGroups: any = [] //user groups

    // tabs filters
    viewHour: boolean = false; // html filter by date
    viewTrack: boolean = false; //html filter by track
    viewGroup: boolean = false; //html of the event groups

    // btns
    backBtn: boolean = true;

    searchBtn: boolean = false//controls on the html button that activates the searchOpen ion-searchbar.
    searchOpen: boolean = false; //controls in HTML the searchOpen's Ion-searchbar.



    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    // module views
    typeVisionModule = null//type of module views.
    GLOBAL_VISION = TypeVisionModule.GLOBAL_VISION // visão global 
    DIVIDED_BY_GROUPS = TypeVisionModule.DIVIDED_BY_GROUPS // visão dividida por grupo
    GROUP_VISION = TypeVisionModule.GROUP_VISION  // visão por grupos 
    GROUP_ACCESS_PERMISSION = TypeVisionModule.GROUP_ACCESS_PERMISSION //acessp limitado por grupo 
    userLanguage: string = environment.platform.defaultLanguage;
    eventLanguage: string = environment.platform.defaultLanguage;
    userFormatedLang: string = environment.platform.defaultLanguage;

    allowCloseReferences: boolean = true;

    sessionIndex: number = -1;

    constructor(
        private route: ActivatedRoute,
        private luxon: LuxonService,
        public global: GlobalService,
        private dbSchedule: DaoScheduleService,
        public toastController: ToastController,
        private router: Router,
        private events: Events,
        private menuCtrl: MenuController,
        private translateService: TranslateService,
        private dbAnalytics: DaoAnalyticsService,
        private dbTracks: DaoTrackService,
        private dbGroups: DaoGroupsService,
        private daoFeedback: DaoSessionFeedbackService,
        private zone: NgZone,
        private store: Store<AppState>
    ) {
        this.cancelTxt = this.translateService.instant('global.buttons.cancel');
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;

            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
            this.events.subscribe('languageUpdate', (language) => {
                this.userLanguage = language;
            });
            this.loadModuleFeedback();
        })
    }

    loadModuleFeedback() {
        this.daoFeedback.getFeedbackModule(this.eventId, (modules) => {
            if (modules.length >= 1) {
                this.sessionFeedbackModule = modules[0].uid;
            }
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    scheduleTrackName: string = this.translateService.instant('pages.schedule.filter_track');

    ngOnInit() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);

        if (this.router.url == this.global.eventHomePage) {
            this.allowButtonsHeader = true;
        }

        this.userLanguage = this.global.language;
        this.eventLanguage = this.global.event.language;
        this.userFormatedLang = this.convertLangFormat(this.global.language);
        this.loaderByTrack = false

        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        this.startAll();
    }

    ionViewWillEnter() {
        if (this.sessions.length > 0) {
            this.virtualScroll.checkEnd();
            if (this.sessionsAll.length == 1) {
                this.openSession(this.sessionsAll[0].uid);
            }
        }
    }

    headerHeightFn(item, index) {
        return (30);
    }

    itemHeightFn(item, index) {
        return (75);
    }

    startAll() {
        this.sessions = []
        this.sessionsAll = []
        this.selectedDate = 'all'
        this.selectedTrackId = null
        // load the module tracks. 
        this.getTracksModule()

        // get the module information
        this.subscriptions.push(this.dbSchedule.getModule(this.moduleId).subscribe(async (module) => {
            this.module = module;

            this.typeVisionModule = module.typeVision;
            this.scheduleTrackName = module.trackName[this.userFormatedLang];

            // verifies the type of vision of the module. global vision
            if (this.typeVisionModule === this.GLOBAL_VISION) {
                this.getGlobalVision()
            }

            //divided by groups
            if (this.typeVisionModule === this.DIVIDED_BY_GROUPS) {
                this.getDividedByGroupsVision()
            }

            // group vision
            if (this.typeVisionModule === this.GROUP_VISION) {
                this.getGroupVision()
            }

            //limited access by groups
            if (this.typeVisionModule === this.GROUP_ACCESS_PERMISSION) {
                this.getLimitedAccessByGroupsVision()
            }
        }))
    }

    ionViewWillLeave() {
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.selectedDate = 'all'
        this.selectedTrackId = null
        this.viewHour = false
        this.viewTrack = false
        this.viewGroup = false

        this.searchBtn = false
        this.searchOpen = false

        this.groups = []
        this.tracks = []
        this.sessions = []
        this.sessionsAll = []

        // close refs
        this.dbSchedule.closeRefGetNextPageSessionsGlobalVision()
        this.dbSchedule.closeRefGetSessionsGroupGlobalVision()
        this.dbSchedule.closeRefGetSessionsGroupsVision()
        this.dbSchedule.closeRefGetNextPageSessionsLimitedAccessByGroups()

        this.dbGroups.closeRefGetGroups()
        this.dbGroups.closeRefContSessionsOfGroup()

        this.dbTracks.closeGetTracksModule()
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }


    // puts the session date in front.
    dateHeader(session, index, sessionsList) {
        if (index == 0) {
            return session.date;
        } else if (index != 0 && session.date != sessionsList[index - 1].date) {
            return session.date;
        }
    }

    // clear the session fields for front view
    setTheSession(session) {
        // dates
        if (typeof session.date === 'number') {
            session.date = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(session.date))
        }

        if (typeof session.startTime === 'number') {
            session.startTime = this.luxon.dateTime(this.luxon.convertTimestampToDate(session.startTime))
        }


        if (session.endTime !== "" && session.endTime !== null && typeof session.endTime !== 'undefined' && typeof session.endTime === 'number') {
            session.endTime = this.luxon.dateTime(this.luxon.convertTimestampToDate(session.endTime))
        }

        // locations of session
        const locations = []

        for (const uid in session.locations) {
            locations.push(session.locations[uid])
        }

        // sort by the order field.
        locations.sort(function (a, b) {
            return a.order - b.order;
        });

        session.locations = locations

        // tracks of session
        const tracks = []

        for (const uid in session.tracks) {
            tracks.push(session.tracks[uid])
        }

        tracks.sort((a, b) => (a.identifier < b.identifier) ? -1 : (a.identifier > b.identifier) ? 1 : 0);

        session.tracks = tracks

        // groups of session
        const groups = []

        for (const uid in session.groups) {
            groups.push(session.groups[uid])
        }

        session.groups = groups


        return session
    }

    // loads the module tracks.
    allowFilterTracks: boolean = false;
    getTracksModule() {
        this.dbTracks.getTracksModule(this.moduleId, this.userFormatedLang, (data) => {
            if (data.length >= 1) { this.allowFilterTracks = true; } else { this.allowFilterTracks = false; }
            this.tracks = []
            this.tracks = data;
        })
    }

    // load the event groups.
    getGroupsEvent() {
        this.dbGroups.getGroups(this.eventId, (groups: Array<CeuGroup>) => {
            // Sort the groups alphabetically.
            groups.sort(function (a, b) {
                return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
            });

            // checks the number of sessions in the module that belongs to each group.
            for (const group of groups) {
                const groupId = group.uid
                const moduleScheduleId = this.moduleId

                group.qtdSessions = 0

                // account the number of sessions that the group has from the schedule module.
                this.dbGroups.contSessionsOfGroup(groupId, moduleScheduleId, (qtd: number) => {
                    group.qtdSessions = qtd
                })
            }

            this.groups = groups
            this.loader = false
        })
    }

    async presentToast(time) {
        const toast = await this.toastController.create({
            message: time,
            duration: 3000
        });

        toast.present();
    }

    // filter sessions by name
    filterSessionsByName(ev) {
        const letters = ev.target.value

        this.sessions = []
        this.lastSession = null
        this.loader = true
        this.loaderByTrack = true
        this.selectedDate = null

        // makes the session filter by name.
        for (const session of this.sessionsAll) {
            if (session.name[this.userFormatedLang].toLowerCase().indexOf(letters.toLowerCase()) > -1) {
                this.sessions.push(session)
            }
        }

        if (letters.length === 0) {
            this.sessions = this.sessionsAll
        }


        this.loader = false
        this.loaderByTrack = false
    }

    // filter session by date
    activeDate: string = 'all';
    filterByDate(date: string) {
        this.sessions = []
        this.loader = true
        this.loaderByTrack = true
        this.lastSession = null
        this.selectedDate = date
        this.activeDate = date;
        // Filter by dates
        if (this.selectedDate == 'all') {
            this.sessions = this.sessionsAll
        } else {
            for (const session of this.sessionsAll) {
                let auxDate = this.prepareFilterDate(session.date);
                if (auxDate === this.selectedDate) {
                    this.sessions.push(session)
                }
            }
        }


        this.loader = false
        this.loaderByTrack = false
    }

    // filter session by track
    filterByTrack($ev) {
        this.sessions = []
        this.loaderByTrack = true
        this.loader = false
        this.lastSession = null
        this.selectedDate = null

        if ($ev.target.value === 'all') {
            this.sessions = this.sessionsAll;
        } else {
            let track = $ev.target.value
            this.selectedTrackId = track.uid

            // filter the sessions by track
            for (const session of this.sessionsAll) {
                const index = session.tracks.map(function (e) { return e.uid; }).indexOf(this.selectedTrackId);

                if (index > -1) {
                    this.sessions.push(session)
                }
            }
        }

        this.loaderByTrack = false
    }


    // activates and deactivates boolean variables that control the html tab, the part of filters by date and by tracks
    changeView($ev) {
        this.selectedDate = null
        this.selectedTrackId = null

        switch ($ev.detail.value) {
            case 'byHour':
                this.viewTrack = false;
                this.viewHour = true;
                break;
            case 'byTracks':
                this.viewHour = false;
                this.viewTrack = true;
                break;
        }
    }

    openSession(sessionId) {
        this.sessionIndex = this.sessions.indexOf(this.sessions.find((session) => session.uid == sessionId));
        this.allowCloseReferences = false;
        let navigationExtras: NavigationExtras = {
            state: {
                sessionFeedbackModule: this.sessionFeedbackModule
            }
        }

        this.router.navigate([`/event/${this.eventId}/schedule-detail/${this.moduleId}/${sessionId}`], navigationExtras)
    }


    // loads more sessions when the infinite scroll is triggered
    moreSessions(event) {
        // verifies the type of vision of the module.
        if (this.typeVisionModule === this.GLOBAL_VISION) {
            this.moreSessionsGlobalVision(event)
        }
        else if (this.typeVisionModule === this.GROUP_ACCESS_PERMISSION) {
            this.moreSessionsLimitedAccessByGroups(event)
        }
        else {
            event.target.disabled = true;
        }
    }


    /******************************************************************************** methods of global vision *************************************************************************************** */

    allSessionsLength: number = 0; //get number of sessions you have in module
    allowFilterByDate: boolean = false; //controls the date filter

    // load the global view
    getGlobalVision() {
        this.loader = true

        this.viewHour = true
        this.viewTrack = false
        this.viewGroup = false

        this.searchBtn = true

        // loads the first 100 sessions of the module.
        this.dbSchedule.getFirstPageSessionsGlobalVision(this.moduleId).subscribe((data) => {
            let nextPage = null
            let list = []

            if (data) {
                nextPage = data['nextPage']
                list = data['sessions']
            }

            // takes the start time of the last session loaded.
            if (nextPage) {
                this.lastSession = nextPage;
            }

            // treatment of sessions
            for (let session of list) {
                session = this.setTheSession(session)
            }

            this.sessions = list;
            // this.loadAllSessionsAndDates();
            this.loader = false
        })


        // counts the total sessions you have in the module. (global vision)
        this.dbSchedule.countsSessionsInTheModule(this.moduleId, (total) => {
            this.allSessionsLength = total;
        })

        // load all module sessions and all different dates. Using the api
        this.dbSchedule.getAllSessionsVisionGlobal(this.moduleId).subscribe((sessions) => {
            this.sessionsAll = [];
            this.dates = [];

            sessions.forEach((session) => {
                this.sessionsAll.push(this.setTheSession(session));
                // pick up all different dates of sessions
                const date = this.prepareFilterDate(session.date);
                const index = this.dates.indexOf(date);

                if (index === -1) {
                    this.dates.push(date);
                }
            })

            // Push sessions on store
            this.pushScheduleSessionsOnStore(this.sessionsAll);

            if (this.dates) this.allowFilterByDate = true;
        });
    }

    // loads more sessions when the infinite scroll is triggered (global vision)
    moreSessionsGlobalVision(event) {
        if (this.sessions.length < this.allSessionsLength) {
            if (this.lastSession) {
                // convert a date plus the start time of the last session to timastamp for the search of the bank.
                let year = '2019'
                let month = '12'
                let day = '01'
                let hour = '00'
                let minute = '00'
                let seconds = '00'

                const array1 = this.lastSession.date.split('/')
                day = array1[0]
                month = array1[1]
                year = array1[2]

                const array2 = this.lastSession.startTime.split(':')
                hour = array2[0]
                minute = array2[1]

                const lastTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, hour, minute, seconds))

                // carries 100 more sessions
                this.dbSchedule.getNextPageSessionsGlobalVision(this.moduleId, lastTime, (data) => {
                    this.lastSession = null
                    let list = []

                    if (data['sessions'].length >= 1) {
                        this.lastSession = data['nextPage']
                        list = data['sessions']
                    }

                    // treatment of sessions
                    for (let session of list) {
                        session = this.setTheSession(session);
                        this.sessions.push(session);
                        if (this.sessions.length == this.allSessionsLength) {
                            event.target.disabled = true;
                        }
                    }

                    // check if you still have sessions to search
                    if (this.lastSession) {
                        this.virtualScroll.checkEnd();
                        event.target.complete();
                    } else {
                        event.target.disabled = true;
                    }
                })
            } else {
                event.target.disabled = true;
            }
        }
        // If the array that stores all module sessions has not been loaded.
        else if (this.allSessionsLength == 0) {
            this.virtualScroll.checkEnd();
            event.target.complete();
        } else {
            event.target.disabled = true;
        }
    }

    /******************************************************************************** methods of divided by groups *************************************************************************************** */

    // load the divided by groups view
    getDividedByGroupsVision() {
        this.loader = true

        this.viewHour = false
        this.viewTrack = false
        this.viewGroup = true

        this.searchBtn = false

        // load groups
        this.getGroupsEvent()
    }

    // loads the sessions of the group.
    getSessionsGroupDividByGroup(group) {
        const groupId = group.uid

        this.loader = true //provisório

        // displays time, track, and html name filters.
        this.viewHour = true
        this.viewTrack = false
        this.searchBtn = true

        // hides part of html groups.
        this.viewGroup = false

        // loads the group sessions.
        this.dbSchedule.getSessionsGroupGlobalVision(groupId, this.moduleId, (data) => {
            this.loader = false

            let nextPage = null
            let list = []

            if (data) {
                nextPage = data['nextPage']
                list = data['sessions']
            }

            // takes the start time of the last session loaded.
            if (nextPage) {
                this.lastSession = nextPage;
            }

            // treatment of sessions
            for (let session of list) {
                session = this.setTheSession(session)
            }

            this.sessions = list;
            this.sessionsAll = list;

            // Push sessions on store
            this.pushScheduleSessionsOnStore(this.sessionsAll);

            // pick up all different dates of sessions
            this.dates = []

            for (let session of this.sessionsAll) {
                const date = this.prepareFilterDate(session.date)
                const index = this.dates.indexOf(date)

                if (index === -1) {
                    this.dates.push(date)
                }
            }

            // enable filter by date
            if (this.dates) this.allowFilterByDate = true;
        })
    }


    /******************************************************************************** methods of group vision *************************************************************************************** */
    // group vision
    async getGroupVision() {
        this.loader = true

        this.viewHour = true
        this.viewTrack = false
        this.viewGroup = false

        this.searchBtn = true

        // get user groups
        this.userGroups = []
        if (typeof this.global.groupsAttendees !== 'undefined') {
            this.userGroups = this.global.groupsAttendees;

            // load sessions
            this.getSessionsGroupVision()
        }
        // if the groups have not been loaded, call the loadGroups function of the global service.
        else {
            // get the id of the logged in user.
            let userId = null

            // receives user uid
            if (typeof this.global.userId !== 'undefined' && this.global.userId !== null) {
                userId = this.global.userId
            } else {
                userId = await this.global.loadUserId()
            }

            // get the type user of the logged in user.
            let userType = null

            if (typeof this.global.userType !== 'undefined' && this.global.userType !== null) {
                userType = this.global.userType
            } else {
                userType = await this.global.loadUserType()
            }

            // get the type groupsAttendee of the logged in user.
            this.userGroups = []
            if (typeof this.global.groupsAttendees !== 'undefined') {
                this.userGroups = this.global.groupsAttendees
            } else {
                this.userGroups = await this.global.loadGroups(userId, userType, this.eventId)
            }

            // load sessions
            this.getSessionsGroupVision()
        }

    }


    // loads the participant group sessions.
    getSessionsGroupVision() {
        this.dbSchedule.getSessionsGroupsVision(this.userGroups, this.moduleId, (data) => {
            this.loader = false;

            let nextPage = null;
            let list = [];

            if (data) {
                nextPage = data['nextPage'];
                list = data['sessions'];
            }

            // takes the start time of the last session loaded.
            if (nextPage) {
                this.lastSession = nextPage;
            }

            // treatment of sessions
            for (let session of list) {
                session = this.setTheSession(session);
            }

            this.sessions = list;
            this.sessionsAll = list;

            // Push sessions on store
            this.pushScheduleSessionsOnStore(this.sessionsAll);

            // pick up all different dates of sessions
            this.dates = [];

            for (let session of this.sessionsAll) {
                const date = this.prepareFilterDate(session.date);
                const index = this.dates.indexOf(date);

                if (index === -1) {
                    this.dates.push(date);
                }
            }
            // this.loadAllSessionsAndDates();
            // enable filter by date
            if (this.dates) this.allowFilterByDate = true;
        })
    }


    /******************************************************************************** methods of limited access by groups *************************************************************************************** */
    //limited access by groups
    getLimitedAccessByGroupsVision() {
        this.loader = true

        this.viewHour = true
        this.viewTrack = false
        this.viewGroup = false
        this.searchBtn = true

        // get the first 100 sessions of the module.
        this.dbSchedule.getFirstPageSessionsLimitedAccessByGroups(this.moduleId).subscribe((data) => {
            let nextPage = null
            let list = []
            this.sessions = []

            if (data) {
                nextPage = data['nextPage']
                list = data['sessions']
            }

            // takes the start time of the last session loaded.
            if (nextPage) {
                this.lastSession = nextPage;
            }

            // treatment of sessions
            for (let session of list) {
                session = this.setTheSession(session)
            }

            this.sessions = list
            this.loader = false
        })


        // counts the total sessions you have in the module. (global vision)
        this.dbSchedule.countsSessionsInTheModule(this.moduleId, (total) => {
            this.allSessionsLength = total;
        })

        // load all module sessions and all different dates. Using the api
        this.dbSchedule.getAllSessionsLimitedAccessByGroup(this.moduleId).subscribe((sessions) => {
            this.sessionsAll = [];
            this.dates = [];

            sessions.forEach((session) => {
                this.sessionsAll.push(this.setTheSession(session));
                // pick up all different dates of sessions
                const date = this.prepareFilterDate(session.date);

                const index = this.dates.indexOf(date);

                if (index === -1) {
                    this.dates.push(date);
                }
            })

            // Push sessions on store
            this.pushScheduleSessionsOnStore(this.sessionsAll);

            if (this.dates) this.allowFilterByDate = true;
        });
    }






    // loads more sessions when the infinite scroll is triggered (limited access by groups )
    moreSessionsLimitedAccessByGroups(event) {
        if (this.sessions.length < this.allSessionsLength) {
            if (this.lastSession) {
                // convert a date plus the start time of the last session to timastamp for the search of the bank.
                let year = '2019'
                let month = '12'
                let day = '01'
                let hour = '00'
                let minute = '00'
                let seconds = '00'

                const array1 = this.lastSession.date.split('/')
                day = array1[0]
                month = array1[1]
                year = array1[2]

                const array2 = this.lastSession.startTime.split(':')
                hour = array2[0]
                minute = array2[1]

                const lastTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, hour, minute, seconds))

                // carries 100 more sessions
                this.dbSchedule.getNextPageSessionsLimitedAccessByGroups(this.moduleId, lastTime, (data) => {
                    this.lastSession = null
                    let list = []

                    if (data) {
                        this.lastSession = data['nextPage']
                        list = data['sessions']
                    }

                    // treatment of sessions
                    for (let session of list) {
                        session = this.setTheSession(session)
                        this.sessions.push(session)
                    }

                    // check if you still have sessions to search
                    if (this.lastSession) {
                        this.virtualScroll.checkEnd();
                        event.target.complete();
                    } else {
                        event.target.disabled = true;
                    }
                })
            } else {
                event.target.disabled = true;
            }
        }
        // If the array that stores all module sessions has not been loaded.
        else if (this.allSessionsLength == 0) {
            this.virtualScroll.checkEnd();
            event.target.complete();
        } else {
            event.target.disabled = true;
        }


    }

    /**
     * Push schedule sessions on store
     * @param sessions 
     */
    pushScheduleSessionsOnStore(sessions: any[]) {
        this.store.dispatch(new GetScheduleSessions(sessions));
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    prepareFilterDate(oldDate: string) {
        let arrayDate = oldDate.split('/');
        let date = new Date(Number(arrayDate[2]), Number(arrayDate[1]) - 1, Number(arrayDate[0]));
        let day = '0' + date.getDate();
        let month = this.selectMonth(date.getMonth() + 1);
        let finalDate = day.substr(-2) + ' ' + month.substring(0, 3);
        return finalDate;

    }

    selectMonth(month: number) {
        let finalMonth;
        switch (month) {
            case 1:
                finalMonth = this.translateService.instant('global.texts.january')
                break;
            case 2:
                finalMonth = this.translateService.instant('global.texts.february')
                break;
            case 3:
                finalMonth = this.translateService.instant('global.texts.march')
                break;
            case 4:
                finalMonth = this.translateService.instant('global.texts.april')
                break;
            case 5:
                finalMonth = this.translateService.instant('global.texts.may')
                break;
            case 6:
                finalMonth = this.translateService.instant('global.texts.june')
                break;
            case 7:
                finalMonth = this.translateService.instant('global.texts.july')
                break;
            case 8:
                finalMonth = this.translateService.instant('global.texts.august')
                break;
            case 9:
                finalMonth = this.translateService.instant('global.texts.september')
                break;
            case 10:
                finalMonth = this.translateService.instant('global.texts.october')
                break;
            case 11:
                finalMonth = this.translateService.instant('global.texts.november')
                break;
            case 12:
                finalMonth = this.translateService.instant('global.texts.december')
                break;
        }
        return finalMonth;
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ChatPage } from './chat.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { Camera } from '@ionic-native/camera/ngx';
import { AvatarModule } from 'ngx-avatar';
import { VisioConferenceModule } from 'src/app/content/reusable_components/visio-conference/visio-conference.module';
import { ComponentsModulesModule } from '../components-modules.module';

const routes: Routes = [
    {
        path: '',
        component: ChatPage
    },
    {
        path: 'detail',
        loadChildren:
            'src/app/modules/group-discussions-detail/group-discussions-detail.module#GroupDiscussionsDetailPageModule'
    }
];
@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild(routes),
        AvatarModule,
        SharedModule,
        VisioConferenceModule,
        ComponentsModulesModule
    ],
    providers: [Camera]
})
export class ChatModule {}

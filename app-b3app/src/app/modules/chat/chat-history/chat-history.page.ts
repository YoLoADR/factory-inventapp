import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { ActivatedRoute } from '@angular/router';
// import { PathComponent } from 'src/app/models/path/path-components';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { CeuChat } from 'src/app/models/ceu-chat';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { ChatListComponent } from 'src/app/components/chat-list/chat-list.component';
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DaoChatService } from 'src/app/providers/db/dao-chat.service';

@Component({
    selector: 'app-chat-history',
    templateUrl: './chat-history.page.html',
    styleUrls: ['./chat-history.page.scss']
})
export class ChatHistoryPage implements OnInit {
    eventId: string;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    chatBadge: number = 0;
    backBtn: boolean = true;
    q: string;

    chats$: Observable<any[]>;
    notifications$: any = {};

    refChats: any = null;
    chatsCollection: AngularFirestoreCollection<any> = null;

    allChats: Array<CeuChat> = [];
    allChatsSearch: Array<CeuChat> = [];
    userId: string = null;
    searchOpen: boolean = false;

    constructor(
        private menuCtrl: MenuController,
        public global: GlobalService,
        private route: ActivatedRoute,
        private modalCtrl: ModalController,
        private dbAttendees: DaoAttendeesService,
        private gdService: GroupDiscussionsService,
        private dbChat: DaoChatService
    ) {
        this.menuCtrl.enable(true);
        this.q = '';
        this.eventId = this.route.snapshot.paramMap.get('eventId');
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        this.loadColors();

        this.chats$ = this.gdService.chats(this.eventId).pipe(
            tap((chats) => {
                console.log("Chats: ", chats);
                chats.map((c) => {
                    this.notifications$[c.uid] = this.badgeNotifications(c.uid);
                });
            })
        );
    }

    badgeNotifications(chatId: string) {
        return this.gdService.badgeNotifications(this.eventId, chatId);
    }

    getAttendee(uid: string) {
        return (this.dbAttendees.getAttendeeByEvent(this.eventId, uid).toPromise());
        // return new Promise((resolve) => {
        //     this.dbAttendees.getAttendeeByEvent(this.eventId, uid, (attendee) => {
        //         resolve(attendee);
        //     });
        // })
    }

    getLastMessage(chatId: string) {
        return new Promise((resolve) => {
            this.dbChat.getLastMessage(this.eventId, chatId, (msg) => {
                resolve(msg);
            });
        })
    }

    getChatUrl(chat: any) {
        return chat.group
            ? `/event/${this.eventId}/chat/group/${chat.uid}`
            : `/event/${this.eventId}/chat/${chat.uid}`;
    }

    async newChat() {
        const modal = await this.modalCtrl.create({
            component: ChatListComponent,
            componentProps: {
                eventId: this.eventId,
                userId: this.global.userId
            }
        });

        return await modal.present();
    }

    searchBar(ev) {
        this.q = ev.target.value.toLowerCase();
    }
}

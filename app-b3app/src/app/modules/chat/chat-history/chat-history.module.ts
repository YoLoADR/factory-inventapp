import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChatHistoryPage } from './chat-history.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { AvatarModule } from 'ngx-avatar';
import { FilterChatsPipe } from 'src/app/pipes/filter-chats/filter-chats.pipe';

@NgModule({
    declarations: [ChatHistoryPage, FilterChatsPipe],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([{ path: '', component: ChatHistoryPage }]),
        AvatarModule,
        SharedModule
    ]
})
export class ChatHistoryModule {}

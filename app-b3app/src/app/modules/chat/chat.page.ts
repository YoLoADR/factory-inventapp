import { Component, OnInit, OnDestroy, ViewChild, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import {
    ActionSheetController,
    PopoverController,
    IonContent,
    Events,
    Platform
} from '@ionic/angular';
import {
    GlobalService,
    WherebyService,
    UtilityService
} from 'src/app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ChatMessage } from '../../models/chat-message';
import { NotificationDateService } from 'src/app/providers/date/notification-date.service';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { DaoChatService } from 'src/app/providers/db/dao-chat.service';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from 'src/app/providers/storage/storage.service';
import { first, tap } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';
import { ChatSettingsPopupComponent } from 'src/app/components/chat-settings-popup/chat-settings-popup.component';
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';
import { DateTime } from 'luxon';
import { NotificationsService } from 'src/app/providers/notifications.service';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss']
})
export class ChatPage implements OnInit, OnDestroy {
    @ViewChild('msgs', { static: false }) content: IonContent;
    @ViewChild('msgsComponentMode', { static: false }) componentModeContent: ElementRef;

    @Input() componentMode: boolean = false;
    @Output() goBackEvent: EventEmitter<any> = new EventEmitter();

    subscriptions: Subscription[] = [];

    isMobile: boolean = false;

    @Input() eventId: string = null;
    @Input() chatId: string = null;
    @Input() groupId: string = null;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;

    groupDiscussion;
    chat;

    userNameTitle: string = null;

    isVisioEnabled: boolean = false;

    messages: Array<ChatMessage> = [];
    messagesLength: number = 0;
    message: any = '';
    loader: boolean = true;
    fileUrl: string = '';

    loaderUploadPic: boolean = false;

    messages$: Observable<any[]>;

    showVisioWebApp: boolean = false;
    urlVisio: string;
    msgLinkedToVisioOpened: ChatMessage;

    constructor(
        public global: GlobalService,
        private route: ActivatedRoute,
        private events: Events,
        private location: Location,
        private luxon: NotificationDateService,
        private dbAttendees: DaoAttendeesService,
        private popoverController: PopoverController,
        private gdService: GroupDiscussionsService,
        private dbChat: DaoChatService,
        private actionSheetController: ActionSheetController,
        private translateService: TranslateService,
        private storage: StorageService,
        private SWhereby: WherebyService,
        private translate: TranslateService,
        private SUtility: UtilityService,
        private notificationsService: NotificationsService,
        private platform: Platform
    ) {
        if (!this.componentMode) {
            this.eventId = this.route.snapshot.paramMap.get('eventId');
            this.chatId = this.route.snapshot.paramMap.get('chatId');
            this.groupId = this.route.snapshot.paramMap.get('groupId');
        }
        localStorage.setItem('eventId', this.eventId);

        this.loadColors();
        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
        (this.platform.is('ios') || this.platform.is('android')) ? this.isMobile = true : this.isMobile = false;
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        this.messages$ = this.gdService
            .messages(this.eventId, this.groupId || this.chatId)
            .pipe(tap((m) => {
                this.messagesLength = m.length;
                this.scrollToBottom(m.length);
            }));

        this.subscriptions.push(
            this.SWhereby.isVisioEnabled(this.eventId, this.groupId).subscribe(
                (isEnabled) => (this.isVisioEnabled = isEnabled)
            )
        );
        this.subscriptions.push(
            this.gdService
                .chat(this.eventId, this.groupId || this.chatId)
                .subscribe((c) => (this.chat = c))
        );
        if (this.groupId) {
            this.subscriptions.push(
                this.gdService
                    .groupDiscussion(this.groupId)
                    .subscribe((g) => (this.groupDiscussion = g))
            );
        }

        this.getChatTitle();
    }

    ngOnDestroy() {
        this.subscriptions.map((s) => s.unsubscribe());
    }

    ionViewWillEnter() {
        this.updateLastAccess();
    }

    async updateLastAccess() {
        const timestamp = this.luxon.getTimeStampFromDateNow(
            new Date(),
            this.global.event.timezone
        );

        try {
            await this.gdService.updateLastAccess(
                this.eventId,
                this.groupId || this.chatId,
                timestamp
            );
        } catch (e) {
            console.log(e);
        }
    }

    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: ChatSettingsPopupComponent,
            componentProps: { params: this.route.snapshot.params },
            event: ev,
            animated: true,
            showBackdrop: true
        });

        return await popover.present();
    }

    // unique color for each user in the chat
    getColorFromUID(uid: string) {
        const r = uid.charCodeAt(0) + uid.charCodeAt(1);
        const g = uid.charCodeAt(2) + uid.charCodeAt(3);
        const b = 0;

        if (this.groupId) {
            return `rgba(${r}, ${g}, ${b}, 0.3)`;
        }

        return uid === this.global.userId
            ? 'rgba(224, 247, 245, 0.3)'
            : `rgba(234, 250, 224, 0.3)`;
    }

    // get chat title to display
    async getChatTitle() {
        if (this.chatId) {
            this.subscriptions.push(this.dbAttendees.getAttendeeByEvent(
                this.eventId,
                this.chatId.split('-').find((m) => m !== this.global.userId)).subscribe((user: any) => {
                    this.userNameTitle = user.name;
                }));
        }

        if (this.groupId) {
            try {
                const gd = await this.gdService
                    .groupDiscussion(this.groupId)
                    .pipe(first())
                    .toPromise();

                this.userNameTitle = gd.title;
            } catch (e) {
                console.log(e);
            }
        }
    }

    /**
     * Get an image to send from mobile
     */
    async getImageToSend() {
        const actionSheet = await this.actionSheetController.create({
            buttons: [{
                text: this.translateService.instant('global.buttons.cancel'),
                role: 'destructive',
                icon: 'close',
                handler: () => { }
            }, {
                text: this.translateService.instant('global.buttons.take_picture'),
                icon: 'camera',
                handler: () => {
                    this.storage.takePicture((data) => {
                        this.uploadImage(data);
                    });
                }
            }, {
                text: this.translateService.instant('global.buttons.open_gallery'),
                icon: 'photos',
                handler: () => {
                    this.storage.openGallery((data) => {
                        this.uploadImage(data);
                    });
                }
            }]
        });

        await actionSheet.present();
    }

    /**
     * Get an image to send from web
     * @param ev 
     */
    getImageToSendFromWeb(ev) {
        const fileWeb: File = ev.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(fileWeb);
        reader.onload = () => {
            this.uploadImage(reader.result);
        }
        reader.onerror = error => console.log(error);
    }

    /**
     * Convert timestamp to hours/minutes format
     * @param timestamp 
     */
    convertTimestampToDate(timestamp: number) {
        return DateTime.fromMillis(timestamp).toLocaleString(
            DateTime.DATETIME_SHORT
        );
    }

    // upload the image to send in chat message
    uploadImage(file) {
        this.loaderUploadPic = true;
        this.storage.uploadChatImage(this.eventId, file, (url) => {
            this.fileUrl = url;
            this.loaderUploadPic = false;
        });
    }

    scrollToBottom(len: number) {
        if (len > 5) {
            setTimeout(() => {
                (!this.componentMode) ? this.content.scrollToBottom(500) : this.componentModeContent.nativeElement.scrollTop = this.componentModeContent.nativeElement.scrollHeight;
            }, 500);
        }
    }

    msg() {
        const msg = new ChatMessage();

        msg.from_user = this.global.userId;
        msg.message = this.message;
        //timezone temporario
        let auxTimezone =
            '(UTC-3) Brasilia, Salvador, Fortaleza, Belo Horizonte, Rio de Janeiro, Porto Alegre, São Paulo';
        msg.send_at = this.luxon.getTimeStampFromDateNow(
            new Date(),
            auxTimezone
        );
        msg.message_picture = this.fileUrl;
        msg.send_from_user_name = this.global.displayName;
        msg.send_to_user_name = this.userNameTitle;
        msg.send_from_user_photo = this.global.photoUrl;
        msg.eventId = this.eventId;
        msg.message_picture = this.fileUrl;

        return msg;
    }

    // send message in chat to user
    async sendMsg() {
        const copyMessage = this.message;
        const copyFileUrl = this.fileUrl;

        try {
            const msg = this.msg();
            const userIds = this.groupId
                ? this.groupDiscussion.activeParticipants.filter(
                    (p) =>
                        p !== this.global.userId &&
                        !(
                            this.groupDiscussion.mutedParticipants || []
                        ).includes(p)
                )
                : Object.keys(this.chat.members).filter(
                    (m) => m !== this.global.userId
                );

            await this.gdService.addMessage(
                this.eventId,
                this.chatId || this.groupId,
                msg,
                !!this.groupId
            );

            if (this.componentMode) {
                this.scrollToBottom(this.messagesLength);
            }

            await this.notificationsService.sendNotification(
                this.eventId,
                msg.message,
                msg.send_from_user_name,
                userIds
            );

            this.events.publish('updateChatBadge');
            this.events.publish('chatHistoryBadge');

            this.message = '';
            this.fileUrl = '';
        } catch (e) {
            this.fileUrl = copyFileUrl;
            this.message = copyMessage;
        }
    }

    ionViewWillLeave() {
        this.updateLastAccess();

        this.events.publish('updateChatBadge');
        this.events.publish('chatHistoryBadge');
    }

    back() {
        this.location.back();
    }

    sendMessageVisio(url: string): Promise<void> {
        const message = {
            message: this.translate.instant('global.texts.visio-launched'),
            url,
            from_user: this.global.userId,
            send_at: this.luxon.getTimeStampFromDateNow(
                new Date(),
                this.global.event.timezone
            ),
            send_from_user_name: this.global.displayName,
            send_from_user_photo: this.global.photoUrl,
            eventId: this.eventId
        } as ChatMessage;

        return this.dbChat.sendMessage(
            this.eventId,
            this.chatId || this.groupId,
            message
        );
    }

    async createVisio() {
        const startDate = DateTime.local().toISO();
        const endDate = DateTime.local().plus({ days: 1 }).toISO();
        const meetingData = {
            roomNamePrefix: `/${this.groupId || this.eventId}`,
            roomMode: (this.groupId) ? 'group' : 'normal',
            startDate,
            endDate
        };
        const response = await this.SWhereby.createMeetingOnWhereby(
            meetingData
        );
        const membersList = await this.gdService.getChatMembers(
            this.eventId,
            this.chatId || this.groupId
        );
        const url = response.body.roomUrl;
        const members = membersList.map((id) => {
            return {
                [id]: true
            };
        });
        const visio = {
            uid: this.chatId || this.groupId,
            startDate,
            endDate,
            url,
            members
        };

        await this.SWhereby.setVisio(this.eventId, visio);

        await this.sendMessageVisio(url);
        this.SWhereby.analyticsNewRoom(this.eventId, Object.keys(members));

        this.SUtility.presentToast(
            this.translateService.instant('global.toasts.visios.created'),
            2500,
            'bottom',
            false,
            'primary'
        );
    }

    async onOpenVisio() {
        try {
            if (!this.isVisioEnabled) {
                this.SUtility.presentToast(
                    this.translateService.instant(
                        'global.toasts.visios.not-available'
                    ),
                    2500,
                    'bottom',
                    false,
                    'danger'
                );

                return;
            }

            const url = await this.SWhereby.getVisio(
                this.eventId,
                this.chatId || this.groupId
            );

            if (!url) {
                await this.createVisio();
            }

            this.SWhereby.analyticsNewAccessToRoom(
                this.eventId,
                this.global.userId
            );

            const message = this.msg();

            message.url = url;

            this.msgLinkedToVisioOpened = message;

            if (this.SWhereby.visioAvailableOnMobile()) {
                const fullUrl =
                    url +
                    '?embed&iframeSource=invent-app&chat=on&screenshare=on&skipMediaPermissionPrompt&leaveButton=' +
                    (this.SWhereby.isIOS() ? 'on' : 'off');


                if (
                    this.SWhereby.visioAvailableOnSF() &&
                    this.SWhereby.isAndroid()
                ) {
                    this.subscriptions.push(
                        this.SWhereby.openSFVisio(fullUrl).subscribe()
                    );
                } else {
                    this.subscriptions.push(
                        this.SWhereby.openInAppBrowserVisio(fullUrl).subscribe()
                    );
                }
            } else {
                const fullUrl = url + '?embed&iframeSource=invent-app';

                this.urlVisio = fullUrl;
                this.showVisioWebApp = true;
            }
        } catch (e) {

            this.SUtility.presentToast(
                this.translateService.instant('global.toasts.visios.error'),
                2500,
                'bottom',
                false,
                'danger'
            );
        }
    }

    async onCloseVisio() {
        this.showVisioWebApp = false;

        try {
            if (this.msgLinkedToVisioOpened) {
                this.msgLinkedToVisioOpened.url = 'leaved';
                this.msgLinkedToVisioOpened.message = this.translateService.instant(
                    'global.texts.visio-leaved'
                );
                await this.gdService.addMessage(
                    this.eventId,
                    this.chatId || this.groupId,
                    this.msgLinkedToVisioOpened,
                    !!this.groupId
                );
                this.msgLinkedToVisioOpened = null;
            }
        } catch (error) {
            this.msgLinkedToVisioOpened = null;
        }
    }

    /**
     * Go back to list surveys on component mode
     */
    goBackToList() {
        this.goBackEvent.emit(true);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AvatarModule } from 'ngx-avatar';

import { IonicModule } from '@ionic/angular';

import { ListBroadcastsPage } from './list-broadcasts.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      { path: '', component: ListBroadcastsPage }
    ]),
    AvatarModule
  ],
  declarations: [ListBroadcastsPage]
})
export class ListBroadcastsModule {}

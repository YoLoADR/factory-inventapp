import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { BambuserService } from 'src/app/providers/bambuser/bambuser.service';
import { Events } from '@ionic/angular';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { TypeUser } from '../../models/type-user';
import { DaoBroadcastService } from '../../providers/db/dao-broadcast.service';
import { DaoGeralService } from 'src/app/providers/db/dao-geral.service';

@Component({
    selector: 'app-list-broadcasts',
    templateUrl: './list-broadcasts.page.html',
    styleUrls: ['./list-broadcasts.page.scss'],
})
export class ListBroadcastsPage implements OnInit {

    eventId: string = null;
    moduleId: string = null;
    sessionId: string = null;
    userType: number = null;

    isDegrade: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    allow_broadcast: boolean = false;
    isFetching = false;
    errorMessage = '';
    mayShowSpinner = true;
    refresher: any;
    broadcasts = [];
    moment: any;

    constructor(
        private route: ActivatedRoute,
        public global: GlobalService,
        private bambuser: BambuserService,
        private events: Events,
        private luxon: LuxonService,
        private router: Router,
        private dbBroadcast: DaoBroadcastService,
        private dbGeral: DaoGeralService
    ) {
        this.allow_broadcast = this.global.allow_broadcast;
        this.userType = this.global.userType;

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            this.sessionId = params.sessionId;
        })
    }

    ngOnInit() {
        this.loadColors();

        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }

    ionViewDidEnter() {
        this.reloadData();
    }

    // loads the colors of the event.
    btn_color1: string = null;
    btn_color2: string = null;
    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        if (this.menu_color.includes('linear')) {
            this.isDegrade = true;
            let colors = this.separeGradientColors(this.menu_color);
            this.btn_color1 = colors[0];
            this.btn_color2 = colors[1];
        }
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    separeGradientColors(str) {
        let hex = [];
        let index = str.indexOf('#');

        while (index > -1) {
            hex.push(this.getHexColors(str))
            str = str.replace('#', '');
            index = str.indexOf('#');
        }

        return hex;
    }

    getHexColors(str) {
        let position = str.indexOf('#');
        let result = str.substring(position, position + 7);

        return result;
    }

    reloadData() {
        return new Promise((resolve, reject) => {
            // TODO: support pagination / endless scroll
            this.errorMessage = '';
            this.isFetching = true;

            this.dbBroadcast.getBroadcastsList(this.moduleId, (list) => {
                if (list.length < 1) {
                    this.mayShowSpinner = false;
                    this.isFetching = false;
                    resolve(true)
                }

                this.bambuser.getBroadcasts(this.moduleId).subscribe((snapshot) => {
                    let cont = 0;
                    this.broadcasts = [];

                    for (let broadcast of snapshot.results) {
                        let index = list.map(function (e) { return e.uid; }).indexOf(broadcast.id);
                        let userId = list[index].userId;

                        this.dbGeral.getUserFromUser(userId, (data) => {
                            let typeUser = data.type;

                            if (typeUser == TypeUser.SUPERGOD ||
                                typeUser == TypeUser.GOD ||
                                typeUser == TypeUser.CLIENT ||
                                typeUser == TypeUser.EMPLOYEE) {

                                broadcast.photoUrl = data.photoUrl;
                                broadcast.userName = data.name;
                                this.broadcasts.push(broadcast)

                                if (cont == list.length - 1) {
                                    this.sortBroadcasts();
                                    resolve(true)
                                }

                                cont++;

                            } else {
                                this.dbGeral.loadUser(userId, typeUser, this.eventId).subscribe(async (user: any) => {
                                    broadcast.photoUrl = user.photoUrl;
                                    broadcast.userName = user.name;
                                    this.broadcasts.push(broadcast)

                                    if (cont == list.length - 1) {
                                        this.sortBroadcasts();
                                        resolve(true)
                                    }

                                    cont++;
                                })
                            }

                            this.mayShowSpinner = false;
                            this.isFetching = false;
                        })
                    }

                }, (err) => {

                })
            })

        })
    }

    onPullToRefresh(refresher) {
        // don't show our own spinner: refresher component has an internal spinner
        this.mayShowSpinner = true;
        this.reloadData().then(() => {
            refresher.target.complete();
            this.mayShowSpinner = false;
            this.isFetching = false;
        })
    }

    startLive() {
        this.router.navigate([`/event/${this.eventId}/live-broadcast/${this.moduleId}`])
    }

    playBroadcast(broadcast) {
        let broadcastId = broadcast.id;
        this.router.navigate([`/event/${this.eventId}/watch-broadcast/${this.moduleId}/${broadcastId}`])
    }

    sortBroadcasts() {
        this.broadcasts.sort((a, b) => {
            if (a.created > b.created) return -1;

            if (a.created < b.created) return 1;

            return 0;
        })

        this.broadcasts.sort((a, b) => {
            if (a.type == 'live' && b.type == 'archived') return -1;

            if (a.type == 'archived' && b.type == 'live') return 1;

            return 0;
        })
    }

    ionViewWillLeave() {
    }

}

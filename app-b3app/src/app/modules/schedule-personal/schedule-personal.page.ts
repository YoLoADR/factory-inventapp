import { Component, ViewChild, NgZone } from '@angular/core';
import { DaoPersonalAgendaService } from 'src/app/providers/db/dao-personal-agenda.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IonInfiniteScroll, IonVirtualScroll, Events, MenuController } from '@ionic/angular';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { GlobalService } from 'src/app/shared/services';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-schedule-personal',
    templateUrl: './schedule-personal.page.html',
    styleUrls: ['./schedule-personal.page.scss'],
})

export class SchedulePersonalPage {
    public module = null

    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonVirtualScroll, { static: false }) virtualScroll: IonVirtualScroll;
    allowButtonsHeader: boolean = false;
    userLoggedId: any = null
    eventId: string = null;
    moduleId: string = null;
    sessions: Array<any> = [];

    lastSession = null
    loader: boolean
    selectedDate = 'all'

    public slideOpts = {
        effect: 'flip',
        slidesPerView: 6,
        slidesPerColumn: 1,
        updateOnWindowResize: true,
        centerInsufficientSlides: true,
    };

    // references
    private refSessionsAll: any = null
    private refSessionsSelectedDateAll: any = null
    private refSessionsDateHttp: any = null
    private refSessionsDates: any = null
    private refSessionsByDateSelected: any = null

    // http
    public headers;
    public requestOptions;

    dates: Array<string> = []
    private sessionsAll = []
    backBtn: boolean = true;

    // Variable that receives the function value filterSessionsByName
    searchOpen: boolean = false;
    searchText: string = '';

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    userLanguage: string = environment.platform.defaultLanguage;
    eventLanguage: string = environment.platform.defaultLanguage;
    userFormatedLang: string = environment.platform.defaultLanguage;
    cancelTxt: string = null;
    constructor(
        private dbPersonal: DaoPersonalAgendaService,
        private dbAnalytics: DaoAnalyticsService,
        private route: ActivatedRoute,
        private router: Router,
        private luxon: LuxonService,
        public global: GlobalService,
        public toastController: ToastController,
        private events: Events,
        private menuCtrl: MenuController,
        private zone: NgZone,
        private translateService: TranslateService
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            localStorage.setItem('eventId', this.eventId);
            this.menu_color = this.global.eventColors.menu_color;
            this.menu_text_color = this.global.eventColors.menu_text_color;
            this.title_color = this.global.eventColors.title_color;
            this.text_content_color = this.global.eventColors.text_content_color;
            this.link_color = this.global.eventColors.link_color;
            this.bg_content_color = this.global.eventColors.bg_content_color;
            this.bg_general_color = this.global.eventColors.bg_general_color;

            this.loadColors();

            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });

            this.menuBadge = this.global.notificationBadge;

            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });

            this.events.subscribe('languageUpdate', (language) => {
                this.userLanguage = language;
            });

            this.dbPersonal.getModule(this.moduleId, (module) => {
                this.module = module
            });
            this.cancelTxt = this.translateService.instant('global.buttons.cancel');
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    async ngOnInit() {
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.selectedDate = 'all'
        this.loader = true

        this.userLanguage = this.global.language;
        this.eventLanguage = this.global.event.language;
        this.userFormatedLang = this.convertLangFormat(this.global.language);
        // receives user uid
        if (this.global.userId !== null) {
            this.userLoggedId = this.global.userId
        } else {
            this.userLoggedId = await this.global.loadUserId()
        }

        // verifies that the user is logged in.    // 
        // the user is logged in.
        if (this.userLoggedId !== null) {
            // load the user's personal calendar.
            this.getFirstSessions()
            this.getSessionsAll()
        }
        // The user is not logged in.  // 
        else {
            this.loader = false
            this.sessions = []
            this.sessionsAll = []
        }
    }

    async ionViewWillEnter() {
        // save another module view access
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);

        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
    }

    // get all sessions of the module and separates the dates to be used in filters on the front.
    getSessionsAll() {
        this.dbPersonal.getSessionsModule(this.userLoggedId, this.moduleId, (sessions) => {
            this.sessionsAll = []
            this.dates = []

            for (const session of sessions) {
                // add the date in the general list of sessions
                this.sessionsAll.push(this.setTheSession(session))

                // pick up all different dates of sessions
                const date = this.prepareFilterDate(session.date);
                const index = this.dates.indexOf(date)

                if (index === -1) {
                    this.dates.push(date)
                }
            }
        })
    }


    // get the first 100 sessions of the module.
    getFirstSessions() {
        this.dbPersonal.getFirstPageSessions(this.userLoggedId, this.moduleId, (data) => {
            let nextPage = null
            let list = []

            if (data) {
                nextPage = data['nextPage']
                list = data['sessions']
            }

            // takes the start time of the last session loaded.
            if (nextPage) {
                this.lastSession = nextPage;
            }

            // treatment of sessions
            for (let session of list) {
                session = this.setTheSession(session)
            }

            this.sessions = list
            this.loader = false
        })
    }

    // clear the session fields for front view
    setTheSession(session) {
        // dates
        session.date = this.luxon.convertDateToStringIsNotUSA(this.luxon.convertTimestampToDate(session.date))
        session.startTime = this.luxon.dateTime(this.luxon.convertTimestampToDate(session.startTime))

        if (session.endTime !== "" && session.endTime !== null && typeof session.endTime !== 'undefined') {
            session.endTime = this.luxon.dateTime(this.luxon.convertTimestampToDate(session.endTime))
        }

        // locations of session
        const locations = []

        for (const uid in session.locations) {
            locations.push(session.locations[uid])
        }

        // sort by the order field.
        locations.sort(function (a, b) {
            return a.order - b.order;
        });

        session.locations = locations

        // tracks of session
        const tracks = []

        for (const uid in session.tracks) {
            tracks.push(session.tracks[uid])
        }

        session.tracks = tracks


        return session
    }

    // filter session by date
    activeDate: string = 'all';
    filterByDate(date: string) {
        this.sessions = []
        this.loader = true
        this.lastSession = null
        this.selectedDate = date;
        this.activeDate = date;
        // Filter by dates
        if (this.selectedDate == 'all') {
            this.sessions = this.sessionsAll
        } else {
            for (const session of this.sessionsAll) {
                let auxDate = this.prepareFilterDate(session.date);
                if (auxDate === this.selectedDate) {
                    this.sessions.push(session)
                }
            }
        }

        this.loader = false
    }

    // filter sessions by name
    filterSessionsByName(ev) {
        const letters = ev.target.value

        this.sessions = []
        this.lastSession = null
        this.loader = true
        this.selectedDate = null

        // makes the session filter by name.
        for (const session of this.sessionsAll) {
            if (session.name[this.userFormatedLang].toLowerCase().indexOf(letters.toLowerCase()) > -1) {
                this.sessions.push(session)
            }
        }

        if (letters.length === 0) {
            this.sessions = this.sessionsAll
        }


        this.loader = false
    }


    ionViewWillLeave() {
        this.selectedDate = 'all'
        // this.closeSessionsAllCollectionRef()
    }

    // loads more sessions when the infinite scroll is triggered
    moreSessions(event) {
        if (this.lastSession) {
            // convert a date plus the start time of the last session to timastamp for the search of the bank.
            let year = '2019'
            let month = '12'
            let day = '01'
            let hour = '00'
            let minute = '00'
            let seconds = '00'

            const array1 = this.lastSession.date.split('/')
            day = array1[0]
            month = array1[1]
            year = array1[2]

            const array2 = this.lastSession.startTime.split(':')
            hour = array2[0]
            minute = array2[1]

            const lastTime = this.luxon.createTimeStamp(this.luxon.createDate(year, month, day, hour, minute, seconds))

            // carries 100 more sessions
            this.dbPersonal.getNextPageSessions(this.userLoggedId, this.moduleId, lastTime, (data) => {
                this.lastSession = null
                let list = []

                if (data) {
                    this.lastSession = data['nextPage']
                    list = data['sessions']
                }

                // treatment of sessions
                for (let session of list) {
                    session = this.setTheSession(session)
                    this.sessions.push(session)
                }

                // check if you still have sessions to search
                if (this.lastSession) {
                    this.virtualScroll.checkEnd();
                    event.target.complete();
                } else {
                    event.target.disabled = true;
                }
            })
        } else {
            event.target.disabled = true;
        }
    }



    dateHeader(session, index, sessionsList) {
        if (index == 0) {
            return session.date;
        } else if (index != 0 && session.date != sessionsList[index - 1].date) {
            return session.date;
        }
    }




    async presentToast(time) {
        const toast = await this.toastController.create({
            message: time,
            duration: 3000
        });

        toast.present();
    }


    // close reference refSessionsAll flow
    closeSessionsAllCollectionRef() {
        this.refSessionsAll.unsubscribe()
    }

    // close reference refSessionsSelectedDateAll flow
    closeSessionsSelectedAllRef() {
        this.refSessionsSelectedDateAll.unsubscribe()
    }

    // close reference refSessionsDateHttp flow
    closeSessionsDateHttp() {
        this.refSessionsDateHttp.unsubscribe()
    }

    // close reference refSessionsDate flow
    closeSessionsDate() {
        this.refSessionsDates.unsubscribe()
    }

    // close reference refSessionsByDateSelected flow
    closeSessionsByDateSelected() {
        this.refSessionsByDateSelected.unsubscribe()
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    prepareFilterDate(oldDate: string) {
        let arrayDate = oldDate.split('/');
        let date = new Date(Number(arrayDate[2]), Number(arrayDate[1]) - 1, Number(arrayDate[0]));
        let day = '0' + date.getDate();
        let month = this.selectMonth(date.getMonth() + 1);
        let finalDate = day.substr(-2) + ' ' + month.substring(0, 3);
        return finalDate;

    }

    selectMonth(month: number) {
        let finalMonth;
        switch (month) {
            case 1:
                finalMonth = this.translateService.instant('global.texts.january')
                break;
            case 2:
                finalMonth = this.translateService.instant('global.texts.february')
                break;
            case 3:
                finalMonth = this.translateService.instant('global.texts.march')
                break;
            case 4:
                finalMonth = this.translateService.instant('global.texts.april')
                break;
            case 5:
                finalMonth = this.translateService.instant('global.texts.may')
                break;
            case 6:
                finalMonth = this.translateService.instant('global.texts.june')
                break;
            case 7:
                finalMonth = this.translateService.instant('global.texts.july')
                break;
            case 8:
                finalMonth = this.translateService.instant('global.texts.august')
                break;
            case 9:
                finalMonth = this.translateService.instant('global.texts.september')
                break;
            case 10:
                finalMonth = this.translateService.instant('global.texts.october')
                break;
            case 11:
                finalMonth = this.translateService.instant('global.texts.november')
                break;
            case 12:
                finalMonth = this.translateService.instant('global.texts.december')
                break;
        }
        return finalMonth;
    }


}

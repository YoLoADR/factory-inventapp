import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SchedulePersonalPage } from './schedule-personal.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { DaoPersonalAgendaService } from 'src/app/providers/db/dao-personal-agenda.service';

@NgModule({
  declarations: [SchedulePersonalPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SchedulePersonalPage }
    ]),
    SharedModule
  ],
  providers: [DaoPersonalAgendaService]
})
export class SchedulePersonalModule { }

import { Component, OnInit, NgZone } from '@angular/core';
import { MenuController, Events, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { DaoTrainingService } from 'src/app/providers/db/dao-training.service';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { environment } from 'src/environments/environment';
import { DaoInteractivityService } from 'src/app/providers/db/dao-interactivity.service';

@Component({
    selector: 'app-training-list-session',
    templateUrl: './training-list-session.page.html',
    styleUrls: ['./training-list-session.page.scss'],
})
export class TrainingListSessionPage implements OnInit {

    eventId: string = null;
    moduleId: string = null;
    scheduleModuleId: string = null;
    sessionId: string = null;
    userId: string = null;

    public trainings;
    public answeredTrainings = [];
    public unansweredTrainings = [];
    public firstLoad = true;
    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    loader: boolean = true;
    userLanguage: string = environment.platform.defaultLanguage;

    constructor(private route: ActivatedRoute,
        private router: Router,
        public global: GlobalService,
        private menuCtrl: MenuController,
        private events: Events,
        private daoTraining: DaoTrainingService,
        public toastController: ToastController,
        private dbAnalytics: DaoAnalyticsService,
        private daoInteractivity: DaoInteractivityService,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.scheduleModuleId = params.scheduleModuleId;
            // this.scheduleModuleId = this.route.snapshot.params['scheduleModuleId'];
            this.sessionId = params.sessionId;
            // this.sessionId = this.route.snapshot.params['sessionId'];

            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
        })
    }

    ngOnInit() {
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.userLanguage = this.convertLangFormat(this.global.language);
    }

    ionViewWillEnter() {
        this.daoInteractivity.getInteractivityModule(this.eventId, (moduleInteractivity) => {
            this.global.loadService(() => {
                if (this.global.userLoaded || moduleInteractivity.answerOffline) {
                    this.getTrainings();
                } else {
                    this.global.userLogged();
                }
            });
        })
    }

    getTrainings() {
        // let startTotalTimer: any = new Date();
        this.userId = this.global.userId;

        this.daoTraining.getTrainingsSession(this.eventId, this.moduleId, this.scheduleModuleId, this.sessionId, this.userId, (trainings) => {
            let timer = 0;

            if (this.firstLoad) {
                timer = 0;
                this.firstLoad = false;
            } else {
                timer = 1000;
            }

            setTimeout(() => {

                this.trainings = [];
                this.unansweredTrainings = [];
                this.answeredTrainings = [];

                this.trainings = trainings;
                for (let training of this.trainings) {
                    this.daoTraining.getQuestionsAsync(this.moduleId, training.uid).then((questions) => {
                        // this.daoTraining.getQuestions2(this.moduleId, training.uid, (questions) => {
                        training.questions = questions;
                        this.zone.run(() => {
                            training.questions = questions;

                            let indexAnswered = this.checkAnsweredTrainings(training.uid);
                            if (indexAnswered >= 0) {
                                let index = this.checkIndexExists(this.answeredTrainings, training);

                                // if the object already exists in the array
                                if (index >= 0) {
                                    //if visibility is on
                                    if (training.visibility) {
                                        //update object
                                        this.answeredTrainings[index] = training;
                                    } else {
                                        //if visibility is off removes object from array
                                        for (let i = 0; i <= this.answeredTrainings.length; i++) {
                                            if (this.answeredTrainings[i].uid == training.uid) {
                                                this.answeredTrainings.splice(i, 1);
                                            }
                                        }
                                    }
                                } else { // if the training does not already exist in the array and has active visibility, push
                                    if (training.visibility) {
                                        this.answeredTrainings.push(training);
                                    }
                                }
                            } else {
                                let index = this.checkIndexExists(this.unansweredTrainings, training);

                                // if the object already exists in the array
                                if (index >= 0) {
                                    //if visibility is on
                                    if (training.visibility) {
                                        //update object
                                        this.unansweredTrainings[index] = training;
                                    } else { //if visibility is off removes object from array
                                        for (let i = 0; i <= this.unansweredTrainings.length; i++) {
                                            if (this.unansweredTrainings[i].uid == training.uid) {
                                                this.unansweredTrainings.splice(i, 1);
                                            }
                                        }
                                    }
                                } else { // if the training does not already exist in the array and has active visibility, push
                                    if (training.visibility) {
                                        this.unansweredTrainings.push(training);
                                    }
                                }
                            }
                        })
                    });
                }
                this.loader = false;

            }, timer);
        });
    }

    async presentToastTotalTimer(timer) {
        const toast = await this.toastController.create({
            message: 'TIMER TOOOTAL: ' + timer,
            duration: 4000,
            position: 'bottom'
        });
        toast.present();
    }

    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    checkAnsweredTrainings(trainingId) {
        if (this.userId === null || this.userId === undefined) {
            return -1;
        }

        if (this.global.user.answeredTrainings) {
            return this.global.user.answeredTrainings.map(function (e) { return e; }).indexOf(trainingId);
        } else {
            return -1;
        }
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    openUnansweredTraining(index) {
        let navigationExtras: NavigationExtras = {
            state: {
                previousPage: 'trainingListSession',
                sessionId: this.sessionId,
                scheduleModuleId: this.scheduleModuleId
            }
        };

        this.router.navigate([`/event/${this.eventId}/training/${this.moduleId}/${this.unansweredTrainings[index].uid}`], navigationExtras);
    }

    openAnsweredTraining(index) {

        if (this.answeredTrainings[index].change_answer || this.answeredTrainings[index].view_answered) {
            let navigationExtras: NavigationExtras = {
                state: {
                    previousPage: 'trainingListSession',
                    sessionId: this.sessionId,
                    scheduleModuleId: this.scheduleModuleId
                }
            };

            this.router.navigate([`/event/${this.eventId}/training/${this.moduleId}/${this.answeredTrainings[index].uid}`], navigationExtras);
        }
    }

    popPage() {
        // this.navCtrl.back();
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    ionViewWillLeave() {
        // save another module view access
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }

}

/// <reference types="@types/googlemaps" />

import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { Events } from '@ionic/angular';
import { DaoMapsService } from 'src/app/providers/db/dao-maps.service';
declare let google: any;

@Component({
    selector: 'app-map-detail',
    templateUrl: './map-detail.page.html',
    styleUrls: ['./map-detail.page.scss'],
    providers: [DaoMapsService]
})
export class MapDetailPage implements OnInit {
    @ViewChild('map', { static: false }) mapElement;
    map: any;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    eventId: string = null;
    moduleId: string = null;
    mapId: string = null;
    mapName: string = null;
    mapAddress: string = null;
    menuBadge: number = 0;
    loader: boolean = true;
    allowButtonsHeader: boolean = false;
    constructor(
        private route: ActivatedRoute,
        public global: GlobalService,
        private events: Events,
        private dbMaps: DaoMapsService,
        private router: Router,
        private zone: NgZone
    ) {
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.mapId = params.mapId;
            // this.mapId = this.route.snapshot.params['mapId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
        this.dbMaps.getMap(this.moduleId, this.mapId, (result) => {
            if (result !== null && result !== undefined) {
                this.mapAddress = result.address;
                this.mapName = result.name;
                this.initMap(this.mapAddress);
            } else {

            }
        });
    }

    // start map
    async initMap(address) {
        setTimeout(async () => {
            let map;
            let latLng = new google.maps.LatLng(0, 0);
            map = await new google.maps.Map(this.mapElement.nativeElement, {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            this.searchAddress(map, address);
            this.loader = false;
        }, 2000);
    }

    // search map address
    searchAddress(map, address) {
        const geoCoder = new google.maps.Geocoder();
        let myResult = null;
        geoCoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                myResult = results[0].geometry.location; // LatLng

                new google.maps.Marker({
                    position: myResult,
                    map: map,
                    title: 'Localização!'
                });

                map.setCenter(myResult);
                map.setZoom(16);
            } else {
                console.log('not load map')
            }
        })
    }

}

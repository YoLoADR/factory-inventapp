import { Component, OnInit, NgZone } from '@angular/core';
import { DaoCheckinService } from 'src/app/providers/db/dao-checkin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { Events, MenuController } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { AuthService } from 'src/app/providers/authentication/auth.service';
import { TypeVisionCheckin } from 'src/app/enums/type-vision-checkin';

@Component({
    selector: 'app-checkin',
    templateUrl: './checkin.page.html',
    styleUrls: ['./checkin.page.scss'],
    providers: [DaoCheckinService]
})

export class CheckinPage implements OnInit {
    public module = null

    moduleId: string;
    eventId: string;
    checkins: Array<any> = [];
    backBtn: boolean = true;
    searchOpen: boolean = false;
    searchText: string = '';
    public requestOptions;
    public headers;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    allowButtonsHeader: boolean = false;
    loader: boolean = true;
    constructor(
        private daoCheckin: DaoCheckinService,
        private route: ActivatedRoute,
        private router: Router,
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private zone: NgZone,
        private dbAnalytics: DaoAnalyticsService,
        private auth: AuthService
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.loadModule();
        this.loadCheckins();
    }

    loadModule() {
        this.daoCheckin.getModule(this.moduleId, (module) => {
            this.module = module
        });
    }

    loadCheckins() {
        this.daoCheckin.getCheckins(this.moduleId, (checkins) => {
            this.zone.run(() => {
                this.checkins = [];

                // Checks if the group can be viewed.
                this.auth.current().then((userAuth) => {
                    for (const checkin of checkins) {
                        if (checkin.typeVision === TypeVisionCheckin.GLOBAL_VISION && checkin.visibility) {
                            this.checkins.push(checkin)
                        } else {
                            if (userAuth && checkin.visibility) {
                                let groupsAttendees = typeof this.global.groupsAttendees !== 'undefined' && this.global.groupsAttendees !== null ? this.global.groupsAttendees : []
                                for (const group of groupsAttendees) {
                                    const groupId = group.uid
                                    let index = this.checkins.map(function (e) { return e.uid; }).indexOf(checkin.uid)

                                    if (checkin.groups[groupId] && index <= -1) {
                                        this.checkins.push(checkin)
                                    }
                                }
                            }
                        }
                    }
                })

                this.loader = false;
            });
        });
    }

    openDetail(uid) {
        this.router.navigate([`/event/${this.eventId}/checkin-detail/${this.moduleId}/${uid}`]);
    }

    ionViewWillLeave() {
        // save another module view access
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }
}

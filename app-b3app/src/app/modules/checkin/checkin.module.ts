import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CheckinPage } from './checkin.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CheckinPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: CheckinPage }
    ]),
    SharedModule
  ]
})
export class CheckinModule { }

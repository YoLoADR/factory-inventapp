import { Component, NgZone } from '@angular/core';
import { MenuController, Events, ModalController, ActionSheetController, PopoverController, Platform } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoFeedNewsService } from '../../providers/db/dao-feed-news.service'
import { StorageService } from 'src/app/providers/storage/storage.service';
import { TranslateService } from '@ngx-translate/core';
import { Post } from 'src/app/models/ceu-post';
import { FeedCommentComponent } from 'src/app/components/feed-comment/feed-comment.component';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { ToastController } from '@ionic/angular';
import { FeedMoreOptionsComponent } from 'src/app/components/feed-more-options/feed-more-options.component';
import { LightboxImgComponent } from 'src/app/components/lightbox-img/lightbox-img.component';


@Component({
    selector: 'app-feed-news',
    templateUrl: './feed-news.page.html',
    styleUrls: ['./feed-news.page.scss'],
})

export class FeedNewsPage {
    isMobile: boolean = false;
    public module = null
    userLoggedId = null

    eventId: string = null;
    moduleId: string = null;
    allowButtonsHeader: boolean = false;
    backBtn: boolean = true;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    posts = []

    myImg: string = null;
    textArea = ""
    loaderMyImg: boolean = false;
    loaderPost: boolean = false;

    // module
    isPublic = true
    isNeed_moderate = true
    isSocial_sharing = true
    isComments = true

    constructor(
        private platform: Platform,
        private route: ActivatedRoute,
        private menuCtrl: MenuController,
        public global: GlobalService,
        private events: Events,
        private modalCtrl: ModalController,
        private dbFeed: DaoFeedNewsService,
        public actionSheetCtrl: ActionSheetController,
        private storage: StorageService,
        private actionSheetController: ActionSheetController,
        private translateService: TranslateService,
        private dbAnalytics: DaoAnalyticsService,
        private router: Router,
        public toastController: ToastController,
        public popoverController: PopoverController,
        private zone: NgZone
    ) {
        if (this.platform.is('ios') || this.platform.is('android')) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            //   this.eventId = this.route.snapshot.params['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            //   this.moduleId = this.route.snapshot.params['moduleId'];

            this.loadColors();

            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });

            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    async ionViewWillEnter() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        // receives user uid
        if (this.global.userId !== null) {
            this.userLoggedId = this.global.userId
        } else {
            this.userLoggedId = await this.global.loadUserId()
        }

        // load module
        this.dbFeed.moduleNewsFeed(this.moduleId, (module) => {
            this.module = module

            this.isPublic = module.public
            this.isNeed_moderate = module.need_moderate
            this.isSocial_sharing = module.social_sharing
            this.isComments = module.comment
        });
        this.loadPosts();
    }

    loadPosts() {      //load posts
        this.dbFeed.getPostsNewsFeed(this.moduleId, (data) => {
            this.zone.run(() => {
                if (this.posts.length <= 0) {
                    this.posts = data;
                } else {
                    for (let post of data) {
                        let index = this.checkIndex(post.uid);
                        if (index > -1) {
                            this.posts[index].date = post.date;
                            this.posts[index].description = post.description;
                            this.posts[index].fixedTop = post.fixedTop;
                            this.posts[index].img = post.img;
                            this.posts[index].comments = post.comments;
                            this.posts[index].active = post.active;
                        } else {
                            this.posts.unshift(post);
                        }
                    }
                }
            })
        });
    }


    checkIndex(uid) {
        return this.posts.map(function (e) { return e['uid']; }).indexOf(uid);
    }

    // zoom profile picture image
    openImage(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: url
            }
        }).then(modal => {
            modal.present();
        });
    }

    async viewComments(uid) {
        const modal = await this.modalCtrl.create({
            component: FeedCommentComponent,
            componentProps: {
                'eventId': this.eventId,
                'moduleId': this.moduleId,
                'postId': uid,
                'userLoggedId': this.userLoggedId,
            }
        });
        return await modal.present();
    }


    // account the number of likes in the post.
    contLikes(likes) {
        let cont = 0

        for (let i in likes) {
            cont++
        }

        return cont
    }

    // verifies if the user liked the post
    verifiesLikedPost(post) {
        return typeof post.likes[this.userLoggedId] !== 'undefined'
    }

    // add the like in post
    addLike(post) {
        if (this.global.userLogged()) {
            this.zone.run(() => {
                post.likes[this.userLoggedId] = true
                this.dbFeed.updatePost(post, this.eventId);
            })
        }
    }

    // remove the like in post
    removeLike(post) {
        if (this.global.userLogged()) {
            this.zone.run(() => {
                delete post.likes[this.userLoggedId]
                this.dbFeed.updatePost(post, this.eventId);
            })
        }
    }

    // account the number of comments in the post.
    contComments(comments) {
        let cont = 0

        for (let i in comments) {
            cont++
        }

        return cont
    }


    async getImageToSend() {
        this.loaderMyImg = true;
        const actionSheet = await this.actionSheetController.create({
            buttons: [{
                text: this.translateService.instant('global.buttons.cancel'),
                role: 'destructive',
                icon: 'close',
                handler: () => {
                    this.loaderMyImg = false;
                }
            }, {
                text: this.translateService.instant('global.buttons.take_picture'),
                icon: 'camera',
                handler: () => {
                    this.storage.takePicture((data) => {
                        this.myImg = data;
                        if (this.myImg) {
                            this.loaderMyImg = false;
                        }
                    });
                }
            }, {
                text: this.translateService.instant('global.buttons.open_gallery'),
                icon: 'photos',
                handler: () => {
                    this.storage.openGallery((data) => {
                        this.myImg = data;
                        if (this.myImg) {
                            this.loaderMyImg = false;
                        }
                    });
                }
            }]
        });
        await actionSheet.present();
    }

    /**
     * Get an image to send from web
     * @param ev 
     */
    getImageToSendFromWeb(ev) {
        console.log("Image from web");
        this.loaderMyImg = true;
        const fileWeb: File = ev.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(fileWeb);
        reader.onload = () => {
            this.myImg = reader.result as string;
            if (this.myImg) {
                this.loaderMyImg = false;
            }
        }
        reader.onerror = error => {
            console.log(error);
            this.loaderMyImg = false;
        }
    }



    // create post
    async createPost(form) {
        if (this.global.userLogged()) {
            this.loaderPost = true

            // get uid post
            const postId = this.dbFeed.createIdPost()
            const fixedTop = 1
            const description = this.textArea

            // data creator post
            let creator: any = {
                name: this.global.displayName ? this.global.displayName : "",
                img: {
                    url: this.global.photoUrl ? this.global.photoUrl : "",
                    uid: this.global.userId ? this.global.userId : ""
                },
                moduleId: this.global.user.moduleId ? this.global.user.moduleId : "",
                type: this.global.userType ? this.global.userType : 5,
                uid: this.global.userId ? this.global.userId : ""
            }

            // create object img
            let img: any = {
                name: "",
                url: ""
            }

            // create url storage
            if (this.myImg !== null && typeof this.myImg !== 'undefined') {
                img['url'] = await this.storage.uploadImgNewsFeed(this.myImg, postId, this.eventId, this.moduleId)
            }

            let active;
            if (!this.isNeed_moderate) {
                active = true;
            } else {
                active = false;
            }
            const post = new Post(postId, this.moduleId, this.eventId, img, description, creator, fixedTop, active)
            this.dbFeed.createPost(post, this.eventId)
                .then((_) => {
                    this.loaderPost = false;
                    this.myImg = null
                    this.textArea = ""

                    if (this.isNeed_moderate) {
                        this.presentToastFeedModerate()
                    }
                })
                .catch((e) => {
                    this.loaderPost = false;
                });
        }
    }

    deletePost(post) {
        console.log(post)
        // this.dbFeed.deletePost(post, this.eventId);
    }

    async getBase64ImageFromUrl(imageUrl) {
        var res = await fetch(imageUrl);
        var blob = await res.blob();

        return new Promise((resolve, reject) => {
            var reader = new FileReader();
            reader.addEventListener("load", function () {
                resolve(reader.result);
            }, false);

            reader.onerror = () => {
                return reject(this);
            };
            reader.readAsDataURL(blob);
        })
    }

    // displayed tost with the moderation message.
    async presentToastFeedModerate() {
        const toast = await this.toastController.create({
            message: this.translateService.instant('global.alerts.post_moderated'),
            duration: 3000
        });

        toast.present();
    }

    async moreOptions(post: Post) {
        const popover = await this.popoverController.create({
            component: FeedMoreOptionsComponent,
            componentProps: {
                posts: this.posts,
                post: post,
                eventId: this.eventId,
                moduleId: this.moduleId,
                isModal: false
            },
            translucent: true,
            animated: true
        });
        return await popover.present();
    }

    redirectProfile(creator: any) {
        if (creator.moduleId !== null && creator.moduleId !== undefined && creator.moduleId !== '') {
            this.router.navigateByUrl(`/event/${this.eventId}/profile/${creator.moduleId}/${creator.type}/${creator.uid}`);
        }
    }

    ionViewWillLeave() {
        // close getModule
        this.dbFeed.closeRefGetModule();
        // this.dbFeed.closeGetPostsNewsFeed();
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }
}

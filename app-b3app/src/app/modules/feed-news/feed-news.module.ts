import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FeedNewsPage } from './feed-news.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { AvatarModule } from 'ngx-avatar';
import { DaoFeedNewsService } from 'src/app/providers/db/dao-feed-news.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@NgModule({
  declarations: [FeedNewsPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: FeedNewsPage }
    ]),
    AvatarModule,
    SharedModule
  ],
  providers: [
    DaoFeedNewsService,
    SocialSharing
  ]
})
export class FeedNewsModule { }

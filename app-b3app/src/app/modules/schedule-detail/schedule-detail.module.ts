import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ScheduleDetailPage } from './schedule-detail.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { AvatarModule } from 'ngx-avatar';
import { RatingModule } from 'ng-starrating';
import { DatePickerModule } from 'ionic4-date-picker';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import { Market } from '@ionic-native/market/ngx';
import { DaoScheduleService } from 'src/app/providers/db/dao-schedule.service';
import { InteractivityModule } from 'src/app/content/reusable_components/interactivity/interactivity.module';
import { VisioConferenceModule } from 'src/app/content/reusable_components/visio-conference/visio-conference.module';

@NgModule({
    declarations: [ScheduleDetailPage],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: ScheduleDetailPage }
        ]),
        AvatarModule,
        RatingModule,
        DatePickerModule,
        SharedModule,
        InteractivityModule,
        VisioConferenceModule
    ],
    providers: [
        File,
        FileTransfer,
        FileTransferObject,
        FileOpener,
        Market,
        DaoScheduleService
    ]
})
export class ScheduleDetailModule { }

import { Component, OnDestroy, OnInit } from "@angular/core";
import {
    Events,
    MenuController,
    Platform,
    NavController,
} from "@ionic/angular";
import { DaoScheduleService } from "src/app/providers/db/dao-schedule.service";
import { DaoEventsService } from "src/app/providers/db/dao-events.service";
import { ActivatedRoute, Router } from "@angular/router";
import { GlobalService, UtilityService } from "src/app/shared/services";
import { LuxonService } from "src/app/providers/luxon/luxon.service";
import { Session } from "../../models/ceu-session";
import { DaoPersonalAgendaService } from "src/app/providers/db/dao-personal-agenda.service";
import { DaoSurveyService } from "src/app/providers/db/dao-survey.service";
import { Survey } from "src/app/models/survey";
import { DaoSessionFeedbackService } from "src/app/providers/db/dao-session-feedback.service";
import { sessionFeedback } from "src/app/models/sessionFeedback";
import { DaoAskQuestionService } from "src/app/providers/db/dao-ask-queston.service";
import { DaoQuizService } from "src/app/providers/db/dao-quiz.service";
import { Quiz } from "src/app/models/quiz";
import { DaoTrainingService } from "src/app/providers/db/dao-training.service";
import { Training } from "src/app/models/training";
import { environment } from "src/environments/environment";
import { DaoInteractivityService } from "src/app/providers/db/dao-interactivity.service";
import { TypeUser } from "../../models/type-user";
import { Subscription } from "rxjs";
import { AskQuestion } from "src/app/models/ask-question";
import { Store } from '@ngrx/store';
import { AppState } from "src/app/shared/reducers";
import { GetQuestions, GetQuizs, GetSurveys, GetSpeakers, GetDocuments, GetFeedbacks } from "src/app/shared/actions/interactivity.actions";
import { DaoModulesService } from "src/app/providers/db/dao-modules.service";
import { sessionIsLastPrev, getNextSession, getPrevSession } from "src/app/shared/selectors/schedules.selectors";
import { take, tap } from "rxjs/operators";
import { GroupDiscussionsService } from "src/app/shared/services/group-discussions/group-discussions.service";

@Component({
    selector: "app-schedule-detail",
    templateUrl: "./schedule-detail.page.html",
    styleUrls: ["./schedule-detail.page.scss"],
})
export class ScheduleDetailPage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    moduleAgenda: any = null;
    isMobile: boolean = false;

    // parameters
    eventId: string = null;
    moduleId: string = null;
    sessionId: string = null;

    event: any;
    module: any;

    allow_broadcast: boolean = null;

    moduleInteractivity: any = null;

    userId: string = null;
    userType: number = null;
    isSessionSpeaker: boolean = false;
    date: string = "";
    startTime: string = "";
    endTime: string = "";
    locations = [];
    tracks = [];
    speakers: any[] = [];
    documents: any[] = [];

    // survey
    surveyModule: any = null;
    surveys: Array<Survey> = [];

    // quiz
    quizs: Array<Quiz> = [];
    quizModule: any = null;

    trainings: Array<Training> = [];
    public trainingModule = null;
    trainingIcon: string = null;
    trainingIconFamily: string = null;
    trainingModuleId: string = null;

    questions: AskQuestion[] = [];
    questionModule: any = null;

    sessionFeedbackModule: string = null;

    session = new Session();
    feedbacks: Array<sessionFeedback> = [];
    language: any = "en_US";

    // attributes used for the personal agenda module
    qtdAttendees: number = null; //save the number of registered participants in the session.
    habiliedPersonal: boolean = null; //saves the value that verifies that the module is ready to be used in the personal calendar.
    habilitedLimit: boolean = null; //saves the value that checks whether the option to limit the number of sessions added in the personal calendar.
    statePersonalAgenda: boolean = null; //saves the value if the session is part of the user's personal calendar. (yes = true and no = false)

    sessionObj;

    result = [];
    buttonColorYes = [];
    buttonColorNo = [];
    setResult: boolean = false;

    loadingSend: Array<boolean>;
    loader: boolean = true;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    userLanguage: string = environment.platform.defaultLanguage;
    userLanguageFeedback: string = environment.platform.defaultLanguage;
    eventLanguage: string = environment.platform.defaultLanguage;

    fullWidth: boolean = true;

    // Prev and next buttons
    allowNextPrevBtn: boolean = false;
    prevBtn: boolean = false;
    nextBtn: boolean = false;

    allowDiscussionsGroups: boolean = false;

    constructor(
        private store: Store<AppState>,
        private dbSchedule: DaoScheduleService,
        private dbPersonal: DaoPersonalAgendaService,
        private route: ActivatedRoute,
        private router: Router,
        private dbEvent: DaoEventsService,
        public SGlobal: GlobalService,
        private luxon: LuxonService,
        private events: Events,
        private menuCtrl: MenuController,
        private DaoSurvey: DaoSurveyService,
        private DaoQuiz: DaoQuizService,
        private DaoTraining: DaoTrainingService,
        private daoAskQuestion: DaoAskQuestionService,
        private daoFeedback: DaoSessionFeedbackService,
        private daoInteractivity: DaoInteractivityService,
        private SModule: DaoModulesService,
        private SGroupsDiscussions: GroupDiscussionsService,
        private platform: Platform,
        private SUtility: UtilityService,
        private navCtrl: NavController
    ) {
    }

    async ngOnInit() {
        this.userId = this.SGlobal.userId;
        this.userType = this.SGlobal.userType;
        this.allow_broadcast = this.SGlobal.allow_broadcast;

        if (this.platform.is('mobile')) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }

        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        this.eventLanguage = this.SGlobal.event.language;
        this.loadColors();

        this.menuBadge = this.SGlobal.notificationBadge;
        this.events.subscribe("menuBadge", () => {
            this.menuBadge = this.SGlobal.notificationBadge;
        });

        // checks to see if the global service has a language selected. if you do not have the language of the event.
        if (
            typeof this.SGlobal.language !== "undefined" &&
            this.SGlobal.language !== null
        ) {
            this.language = this.SGlobal.language;
        } else {
            this.language = await this.dbEvent.getLanguageEvent(this.eventId);
        }

        // receives the parameters
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.sessionId = params.sessionId;
            this.moduleId = params.moduleId;
            localStorage.setItem("eventId", this.eventId);

            this.getEvent();

            if (this.router.getCurrentNavigation().extras.state != undefined) {
                if (
                    this.router.getCurrentNavigation().extras.state
                        .sessionFeedbackModule != null
                ) {
                    this.sessionFeedbackModule = this.router.getCurrentNavigation().extras.state.sessionFeedbackModule;
                    localStorage.setItem("feedbackModuleId", this.sessionFeedbackModule);
                    this.getFeedbacks();
                } else {
                    localStorage.setItem("feedbackModuleId", null);
                    this.getFeedbacks();
                }
            } else {
                this.sessionFeedbackModule = localStorage.getItem("feedbackModuleId");
                this.getFeedbacks();
            }
            this.events.subscribe("languageUpdate", (language) => {
                this.userLanguage = language;
            });

            this.menuCtrl.enable(true);
        });
    }

    ionViewWillEnter() {
        this.initSession();
    }

    /**
     * Init session
     */
    async initSession() {
        this.loadSession();

        // statePersonalAgenda attribute created to help the viewing of personal calendar icons in html
        // Note: When updating the session this field should be deleted.
        if (
            this.userId !== "" &&
            this.userId !== null &&
            typeof this.userId !== "undefined"
        ) {
            this.checkAttendeeOfTheSession();
        }
    }

    /**
     * Get event data
     */
    getEvent() {
        this.subscriptions.push(this.dbEvent.getEvent(this.eventId).subscribe((event) => {
            this.event = event;
            if (this.event.availableModules.module_group_discussion) {
                this.subscriptions.push(this.SGroupsDiscussions.getGroupsForEventForSession(this.eventId, this.sessionId)
                    .subscribe((grps) => {
                        this.allowDiscussionsGroups = (grps && grps.length > 0) ? true : false;
                        this.checkInteractivity();
                    }));
            }
        }));

        this.subscriptions.push(this.SModule.getModule(this.moduleId).subscribe((moduleEvent) => {
            this.module = moduleEvent;
            if (this.module.allowNextPrevBtn) {
                this.allowNextPrevBtn = true;

                this.store.select(sessionIsLastPrev(), this.sessionId).pipe(
                    take(1)
                ).subscribe((response) => {
                    if (response.status == 'error') {
                        this.allowNextPrevBtn = false;
                        this.prevBtn = false;
                        this.nextBtn = false;
                    } else {
                        this.prevBtn = (response.first) ? false : true;
                        this.nextBtn = (response.last) ? false : true;
                    }
                })

            } else {
                this.allowNextPrevBtn = false;
            }
        }))
    }

    /**
     * Go next or previous session
     * @param type 
     */
    goNextOrPrevSession(type) {
        this.store.select((type == 'prev') ? getPrevSession() : getNextSession(), this.sessionId).pipe(
            take(1)
        ).subscribe((response) => {
            this.sessionId = response.session.uid;
            this.store.select(sessionIsLastPrev(), this.sessionId).pipe(
                take(1)
            ).subscribe((response) => {
                if (response.status == 'error') {
                    this.prevBtn = false;
                    this.nextBtn = false;
                } else {
                    this.prevBtn = (response.first) ? false : true;
                    this.nextBtn = (response.last) ? false : true;
                }
            })
            this.initSession();
        })
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
        this.link_color = this.SGlobal.eventColors.link_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
        this.bg_general_color = this.SGlobal.eventColors.bg_general_color;
    }

    /**
     * Loading session
     */
    loadSession() {
        this.dbSchedule.getSessionModule(this.moduleId, this.sessionId).subscribe((session) => {
            this.session = this.setTheSession(session);
            this.getInfoModule();
            this.getSessionAttendees();

            this.getSurveys();
            this.getQuiz();
            this.getTraining();

            if (this.session.askQuestion) {
                this.getAskQuestion();
            } else {
                this.store.dispatch(new GetQuestions({
                    questionModule: null,
                    questions: []
                }))
            }
        });
    }

    /**
     * Check interactivity for full width or not
     */
    checkInteractivity() {
        if ((this.surveys && this.surveys.length > 0)
            || (this.questionModule && this.session.askQuestion)
            || (this.quizs && this.quizs.length > 0)
            // || (this.speakers && this.speakers.length > 0)
            // || (this.documents && this.documents.length > 0)
            // || (this.feedbacks && this.feedbacks.length > 0)
            || this.allowDiscussionsGroups) {
            this.fullWidth = false;
        } else {
            this.fullWidth = true;
        }
    }

    /**
     * Setting session data
     * @param session 
     */
    setTheSession(session) {
        // dates
        this.date = this.luxon.convertDateToStringIsNotUSA(
            this.luxon.convertTimestampToDate(session.date)
        );
        this.startTime = this.luxon.dateTime(
            this.luxon.convertTimestampToDate(session.startTime)
        );

        if (
            session.endTime !== "" &&
            session.endTime !== null &&
            typeof session.endTime !== "undefined"
        ) {
            this.endTime = this.luxon.dateTime(
                this.luxon.convertTimestampToDate(session.endTime)
            );
        }

        /**
         * Build locations of session
         */
        this.locations = [];

        for (const uid in session.locations) {
            this.locations.push(session.locations[uid]);
        }

        // Sort locations by the order field.
        this.locations.sort(function (a, b) {
            return a.order - b.order;
        });

        /**
         * Build tracks of session
         */
        this.tracks = [];

        for (const uid in session.tracks) {
            this.tracks.push(session.tracks[uid]);
        }

        /**
         * Build speakers list
         */
        this.speakers = [];

        for (const uid in session.speakers) {
            let speakerData = this.getSpeakerPrincipalTitle(
                session.speakers[uid].title,
                session.speakers[uid].description
            );
            session.speakers[uid].principal_title = speakerData.title;
            session.speakers[uid].principal_description = speakerData.description;

            this.speakers.push(session.speakers[uid]);

            if (this.userId == uid) {
                this.isSessionSpeaker = true;
            }
        }

        // Ordering speakers by name.
        this.speakers = this.speakers.sort(function (a, b) {
            return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
        });

        // Dispatch speakers list on store
        this.store.dispatch(new GetSpeakers({
            speakers: this.speakers
        }))

        /**
         * Build documents list
         */
        this.documents = [];

        for (const uid in session.documents) {
            this.documents.push(session.documents[uid]);
        }

        // Dispatch document list on store
        this.store.dispatch(new GetDocuments({
            documents: this.documents
        }))

        return (session);
    }

    /*
     * Checks whether the session is part of the user's personal calendar
     */
    checkAttendeeOfTheSession() {
        this.dbSchedule.checkAttendeeOfTheSession(
            this.moduleId,
            this.sessionId,
            this.userId).subscribe((status: boolean) => {
                this.statePersonalAgenda = status;
            });
    }

    /* 
     * Loads the module to fetch the values of the enabledPersonal and enabledLimit attributes.
     */
    getInfoModule() {
        this.subscriptions.push(this.dbSchedule.getModule(this.moduleId).subscribe((module) => {
            this.moduleAgenda = module;
            if (typeof module !== "undefined" && module !== null) {
                this.habiliedPersonal = module.habiliedPersonal;
                this.habilitedLimit = module.habilitedLimit;
            }
        }));
    }

    /*
     * Loads session participants
     */
    getSessionAttendees() {
        this.dbSchedule.getSessionAttendees(
            this.moduleId,
            this.sessionId,
            (attendees) => {
                this.qtdAttendees = attendees.length;
            }
        );
    }

    /**
     * Reset interactivity
     */
    resetInteractivity() {
        this.store.dispatch(new GetSpeakers({
            speakers: []
        }))
    }

    ionViewWillLeave() {
        this.resetInteractivity();
        this.dbSchedule.closeRefGetSessionModule();
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Get surveys for event
     */
    getSurveys() {
        if (this.userId !== null || this.moduleInteractivity.answerOffline) {
            this.DaoSurvey.getSurveyModule(this.eventId).subscribe((modules) => {
                if (modules.length > 0) {
                    this.surveyModule = modules[0];

                    this.subscriptions.push(this.DaoSurvey.getSurveysSession(
                        this.eventId,
                        this.surveyModule.uid,
                        this.moduleId,
                        this.sessionId,
                        this.userId).subscribe((surveys) => {
                            this.surveys = [];
                            this.surveys = surveys;

                            this.checkInteractivity();
                            this.store.dispatch(new GetSurveys({
                                surveyModule: this.surveyModule,
                                surveys: this.surveys
                            }))
                        }));
                } else {
                    this.checkInteractivity();
                    this.store.dispatch(new GetSurveys({
                        surveyModule: null,
                        surveys: []
                    }))
                }
            });
        }
    }

    /**
     * Get quizs for event
     */
    getQuiz() {
        if (this.userId !== null || this.moduleInteractivity.answerOffline) {
            this.DaoQuiz.getQuizModule(this.eventId).subscribe((modules) => {
                if (modules.length > 0) {
                    this.quizModule = modules[0];

                    this.subscriptions.push(this.DaoQuiz.getQuizsSession(
                        this.eventId,
                        this.quizModule.uid,
                        this.moduleId,
                        this.sessionId,
                        this.userId).subscribe((quizs) => {
                            this.quizs = [];
                            this.quizs = quizs;

                            this.checkInteractivity();
                            this.store.dispatch(new GetQuizs({
                                quizModule: this.quizModule,
                                quizs: this.quizs
                            }))
                        }));
                } else {
                    this.checkInteractivity();
                    this.store.dispatch(new GetQuizs({
                        quizModule: null,
                        quizs: []
                    }))
                }
            });
        }
    }

    getTraining() {
        if (this.userId !== null || this.moduleInteractivity.answerOffline) {
            this.DaoTraining.getTrainingModule(this.eventId, (modules) => {
                if (modules.length > 0) {
                    this.trainingModule = modules[0];
                    this.trainingIcon = modules[0].icon;
                    this.trainingIconFamily = modules[0].iconFamily;
                    this.trainingModuleId = modules[0].uid;

                    this.DaoTraining.getTrainingsSession(
                        this.eventId,
                        this.trainingModuleId,
                        this.moduleId,
                        this.sessionId,
                        this.userId,
                        (trainings) => {
                            this.trainings = [];
                            this.trainings = trainings;
                        }
                    );
                }
            });
        }
    }

    /**
     * Get asked questions
     */
    getAskQuestion() {
        this.daoAskQuestion.getAskQuestionModule(this.eventId).subscribe((modules) => {
            if (modules.length > 0) {
                this.questionModule = modules[0];

                this.subscriptions.push(this.daoAskQuestion.getQuestions(this.session.moduleId, this.sessionId, this.userId).subscribe((questions) => {
                    if (questions !== null) {
                        questions = questions.sort(function (a, b) {
                            if (a.totalVotes < b.totalVotes) {
                                return 1;
                            }
                            if (a.totalVotes > b.totalVotes) {
                                return -1;
                            }
                            // a must be equal to b
                            return 0;
                        });

                        this.questions = [];
                        this.questions = questions;
                    } else {
                        this.questions = [];
                    }
                    this.checkInteractivity();
                    this.store.dispatch(new GetQuestions({
                        questionModule: this.questionModule,
                        questions: this.questions
                    }))
                }))
            } else {
                this.checkInteractivity();
                this.store.dispatch(new GetQuestions({
                    questionModule: null,
                    questions: []
                }))
            }
        });
    }

    /**
     * Get feedbacks
     */
    getFeedbacks() {
        this.daoInteractivity.getInteractivityModule(this.eventId, (moduleInteractivity) => {
            this.moduleInteractivity = moduleInteractivity;

            if (
                (this.SGlobal.userLoaded || this.moduleInteractivity.answerOffline) &&
                this.sessionFeedbackModule !== null &&
                this.sessionFeedbackModule !== undefined
            ) {
                this.subscriptions.push(this.daoFeedback.getFeedbacks(this.sessionFeedbackModule).subscribe(async (feedbacks) => {
                    this.feedbacks = [];

                    if (feedbacks && feedbacks.length > 0) {
                        this.loadingSend = new Array(feedbacks.length).fill(false);

                        for (let feedback of feedbacks) {
                            if (feedback.type === "AllSessions") {
                                if (this.SUtility.checkIndexExists(this.feedbacks, feedback) == -1) {
                                    this.feedbacks.push(feedback);
                                    this.loader = false;
                                }
                            } else if (feedback.type === "ScheduleModule") {
                                if (this.moduleId === feedback.module_id) {
                                    if (
                                        this.SUtility.checkIndexExists(this.feedbacks, feedback) == -1
                                    ) {
                                        this.feedbacks.push(feedback);
                                        this.loader = false;
                                    }
                                }
                            } else if (feedback.type === "SessionTrack") {
                                // case this session be inside a track with permission, display feedback
                                await this.daoFeedback.checkSpecificTrackFeedback(
                                    feedback.module_id,
                                    this.sessionId,
                                    feedback.references,
                                    (tracks: Array<boolean>) => {
                                        for (const track of tracks) {
                                            if (track) {
                                                if (
                                                    this.SUtility.checkIndexExists(this.feedbacks, feedback) ==
                                                    -1
                                                ) {
                                                    this.feedbacks.push(feedback);
                                                    this.loader = false;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                );
                            } else if (feedback.type === "SpecificSession") {
                                for (let uid of feedback.references) {
                                    if (uid === this.sessionId) {
                                        if (
                                            this.SUtility.checkIndexExists(this.feedbacks, feedback) == -1
                                        ) {
                                            this.feedbacks.push(feedback);
                                            this.loader = false;
                                        }
                                        break;
                                    }
                                }
                            } else if (feedback.type === "SpecificGroup") {
                                await this.daoFeedback.listgroupsOfAttendee(
                                    this.eventId,
                                    this.userId,
                                    (listGroupsAttendee) => {
                                        const references: any = feedback.references;
                                        let findGroup = false;
                                        for (let uidGroup of references) {
                                            for (const uidGroupUser of listGroupsAttendee) {
                                                //if the user is part of the group
                                                if (
                                                    uidGroup == uidGroupUser &&
                                                    findGroup == false
                                                ) {
                                                    findGroup = true;
                                                    if (
                                                        this.SUtility.checkIndexExists(
                                                            this.feedbacks,
                                                            feedback
                                                        ) == -1
                                                    ) {
                                                        this.feedbacks.push(feedback);
                                                        this.loader = false;
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                );
                            }
                        }

                        setTimeout(() => {
                            this.loader = false;
                            this.store.dispatch(new GetFeedbacks({
                                feedbacks: this.feedbacks,
                                feedbackModule: this.sessionFeedbackModule
                            }));
                        }, 300);
                    } else {
                        this.loader = false;
                        this.store.dispatch(new GetFeedbacks({
                            feedbacks: [],
                            feedbackModule: null
                        }));
                    }
                }));
            } else {
                this.loader = false;
                this.store.dispatch(new GetFeedbacks({
                    feedbacks: [],
                    feedbackModule: null
                }));
            }
        });
    }

    /*
     * Add session to participant's personal calendar
     */
    addSession() {
        if (this.SGlobal.userLogged()) {
            this.dbPersonal.addSession(this.session);
        }
    }

    /* 
     * Removes the session from the participant's personal calendar
     */
    removeSession() {
        const sessionId = this.session.uid;
        if (this.SGlobal.userLogged()) {
            this.dbPersonal.deleteSession(this.eventId, this.moduleId, sessionId);
        }
    }

    /**
     * Open training list session
     */
    openTrainingsListSession() {
        this.router.navigate([
            `/event/${this.eventId}/training-list-session/${this.trainingModuleId}/${this.moduleId}/${this.sessionId}`,
        ]);
    }

    /**
     * Open broadcast page
     */
    openBroadcastPage() {
        if (
            this.SGlobal.userLoaded &&
            this.userType == TypeUser.SPEAKER &&
            this.isSessionSpeaker
        ) {
            // trocar true por condição de livestream ativada no evento
            this.router.navigate([
                `/event/${this.eventId}/live-broadcast-session/${this.moduleId}/${this.sessionId}`,
            ]);
        }
    }

    /**
     * Open live page
     */
    openLivePage() {
        if (
            this.session.broadcastId != "" &&
            this.session.broadcastId !== undefined
        ) {
            // trocar true por condição de livestream ativada no evento
            this.router.navigate([
                `/event/${this.eventId}/watch-broadcast-session/${this.moduleId}/${this.sessionId}`,
            ]);
        }
    }

    /**
     * Get speaker principal title
     * @param title 
     * @param description 
     */
    getSpeakerPrincipalTitle(title, description) {
        let principalTitle = "";
        let principalDescription = "";
        switch (this.SGlobal.language) {
            case "pt_BR": {
                principalTitle = title.PtBR;
                principalDescription = description.PtBR;
                break;
            }
            case "en_US": {
                principalTitle = title.EnUS;
                principalDescription = description.EnUS;
                break;
            }
            case "es_ES": {
                principalTitle = title.EsES;
                principalDescription = description.EsES;
                break;
            }
            case "fr_FR": {
                principalTitle = title.FrFR;
                principalDescription = description.FrFR;
                break;
            }
            case "de_DE": {
                principalTitle = title.deDE;
                principalDescription = description.deDE;
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == "" || principalTitle == null) {
            if (title[this.SUtility.convertLangFormat(this.SGlobal.event.language)] !== "") {
                principalTitle =
                    title[this.SUtility.convertLangFormat(this.SGlobal.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== "") {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        if (principalDescription == "" || principalDescription == null) {
            if (
                description[this.SUtility.convertLangFormat(this.SGlobal.event.language)] !== ""
            ) {
                principalDescription =
                    description[this.SUtility.convertLangFormat(this.SGlobal.event.language)];
            } else {
                for (let aux in description) {
                    if (description[aux] !== "") {
                        principalDescription = description[aux];
                    }
                }
            }
        }
        return {
            title: principalTitle,
            description: principalDescription,
        };
    }

    navigateBack() {
        this.navCtrl.back();
    }
}

import { Component, OnInit } from '@angular/core';
import { Platform, ToastController, Events, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { environment } from '../../../environments/environment'
import { DaoBroadcastService } from 'src/app/providers/db/dao-broadcast.service';

@Component({
    selector: 'app-live-broadcast',
    templateUrl: './live-broadcast.page.html',
    styleUrls: ['./live-broadcast.page.scss'],
})
export class LiveBroadcastPage implements OnInit {

    // parameters
    eventId: string = null;
    moduleId: string = null;
    userId: string = null;

    allow_broadcast: boolean = null;
    isBroadcasting = false;
    isPending = false;
    broadcaster: any;
    errListenerId = false;
    broadcastListenerId = false;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    constructor(
        private toastCtrl: ToastController,
        public platform: Platform,
        private dbBroadcast: DaoBroadcastService,
        private route: ActivatedRoute,
        public global: GlobalService,
        private events: Events,
        private alertCtrl: AlertController
    ) {
        this.allow_broadcast = this.global.allow_broadcast;

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            this.userId = this.global.userId;

            platform.ready().then(() => {
                // Using array syntax workaround, since types are not declared.
                if (window['bambuser']) {
                    this.broadcaster = window['bambuser']['broadcaster'];
                    this.broadcaster.setApplicationId(environment.platform.bambuserApplicationId);
                } else {
                    // Cordova plugin not installed or running in a web browser
                }
            });
        })
    }

    ngOnInit() {
        this.loadColors();

        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }

    // loads the colors of the event.
    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    async ionViewDidEnter() {
        if (this.allow_broadcast) {
            // Engage our Ionic CSS background overrides that ensure viewfinder is visible.
            document.getElementsByTagName('body')[0].classList.add("show-viewfinder");

            if (!this.platform.is('cordova')) {
                await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
                alert('This Ionic app is currently not running within a Cordova project. Broadcasting is only supported on iOS and Android devices.');
                return;
            }

            await this.platform.ready();

            if (!this.broadcaster) {
                await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
                alert('Broadcasting plugin not detected. Try running `cordova plugin add cordova-plugin-bambuser` and rebuild your app.');
                return;
            }

            this.broadcaster.showViewfinderBehindWebView();
        }
    }

    ionViewWillLeave() {
        if (this.allow_broadcast) {
            // Disengage our Ionic CSS background overrides, to ensure the rest of the app looks ok.
            document.getElementsByTagName('body')[0].classList.remove("show-viewfinder");

            if (this.broadcaster) {
                this.broadcaster.hideViewfinder();
            }
        }
    }

    async start(title) {
        if (!this.allow_broadcast) return;
        if (this.isBroadcasting || this.isPending) return;
        this.isPending = true;
        const toast = this.toastCtrl.create({
            message: 'Starting broadcast...',
            position: 'middle',
        });

        (await toast).present();

        console.log('Starting broadcast');
        try {
            await this.broadcaster.startBroadcast();
            await this.broadcaster.setTitle(title);
            await this.broadcaster.setAuthor(this.moduleId);

            (await toast).dismiss();
            this.isBroadcasting = true;
            this.isPending = false;

            this.listenForError();
            this.listenForBroadcastId();

        } catch (e) {
            (await toast).dismiss();
            this.isPending = false;
            alert('Failed to start broadcast');
            console.log(e);
        }
    }

    async stop() {
        if (!this.allow_broadcast) return;
        if (!this.isBroadcasting || this.isPending) return;
        this.isPending = true;
        const toast = this.toastCtrl.create({
            message: 'Ending broadcast...',
            position: 'middle'
        });
        (await toast).present();

        console.log('Ending broadcast');
        try {
            await this.broadcaster.stopBroadcast();
            (await toast).dismiss();
            this.isBroadcasting = false;
            this.isPending = false;
        } catch (e) {
            (await toast).dismiss();
            this.isPending = false;
            alert('Failed to stop broadcast');
            console.log(e);
        }
    }

    listenForError() {
        if (this.errListenerId) return;
        this.errListenerId = this.broadcaster.addEventListener('connectionError', async status => {
            this.isBroadcasting = false;
            this.isPending = false;
            const toast = this.toastCtrl.create({
                message: 'Connection error',
                position: 'middle',
                duration: 3000,
            });
            (await toast).present();
        });
    }

    listenForBroadcastId() {
        if (this.broadcastListenerId) return;
        this.broadcastListenerId = this.broadcaster.addEventListener('broadcastIdAvailable', broadcastId => {


            this.dbBroadcast.createBroadcast(this.moduleId, this.userId, broadcastId, async (data) => {
                if (data) {
                    const toast = this.toastCtrl.create({
                        message: `Broadcast id received: ${broadcastId}`,
                        position: 'middle',
                        duration: 3000,
                    });
                    (await toast).present();
                } else {
                    const toast = this.toastCtrl.create({
                        message: `Error received broadcastId`,
                        position: 'middle',
                        duration: 3000,
                    });
                    (await toast).present();
                }
            })

        });
    }

    switchCamera() {
        if (this.broadcaster && this.allow_broadcast) {
            this.broadcaster.switchCamera();
        }
    }

    async presentPrompt() {
        console.log('alert function call')
        let alert = await this.alertCtrl.create({
            header: 'Título',
            inputs: [
                {
                    name: 'title',
                    placeholder: 'Insira o título'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Iniciar',
                    handler: data => {
                        if (data.title !== '' && data.title !== null) {
                            this.start(data.title);
                        } else {
                            console.log('titulo inválido')
                        }
                    }
                }
            ]
        });

        alert.present();
    }

}

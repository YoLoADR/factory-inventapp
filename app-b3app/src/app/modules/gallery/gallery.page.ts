import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoGalleryService } from 'src/app/providers/db/dao-gallery.service';
import { GalleryFolder } from '../../models/ceu-gallery-folder';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Events, MenuController, ModalController } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';


@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.page.html',
    styleUrls: ['./gallery.page.scss'],
})

export class GalleryPage implements OnInit {
    public module = null

    eventId: string = null;
    moduleId: string = null;
    folders: Array<GalleryFolder> = [];
    loader: boolean = true;
    private refGallery: any = null;
    private galleryCollection: AngularFirestoreCollection<any> = null
    backBtn: boolean = true;
    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    refModule: any = null;
    galleryDocument: AngularFirestoreDocument<any> = null;
    menuBadge: number = 0;
    allowOpen: boolean = true;
    blockRedirect: boolean = false;

    userLanguage: string = environment.platform.defaultLanguage;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public global: GlobalService,
        private afs: AngularFirestore,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });


            this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        })
    }

    ionViewDidEnter() {
        // if (this.blockRedirect == false) {
        this.galleryDocument = this.afs.collection('modules').doc(this.moduleId);
        this.refModule = this.galleryDocument.valueChanges().subscribe((data) => {

            this.module = data
            let order;

            if (data['typeOrder'] == 'recent') { order = 'desc' } else if (data['typeOrder'] == 'oldest') { order = 'asc' } else if (data['typeOrder'] == 'custom') { order = 'custom' }

            this.startFolders(order)
                .then((data: any) => {
                    this.loader = false;
                });
        });

    }

    startFolders(order) {
        return new Promise((resolve, reject) => {
            if (order == 'custom') {
                this.galleryCollection = this.afs.collection('modules').doc(this.moduleId).collection('folders', ref => ref.orderBy('order'));
            } else {
                this.galleryCollection = this.afs.collection('modules').doc(this.moduleId).collection('folders', ref => ref.orderBy('createdAt', order));

            }
            this.refGallery = this.galleryCollection.valueChanges().subscribe((data: any) => {
                this.folders = data;
                resolve(this.folders);
            })
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.userLanguage = this.convertLangFormat(this.global.language);
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    ngOnDestroy() {
        this.global.previousPage = null;
        this.allowOpen = true;
    }

    unsubscribe() {
        try {
            this.refGallery.unsubscribe();
            this.allowOpen = true;
        } catch (e) {
            console.log();
        }
    }

    ionViewWillLeave() {
        this.unsubscribe();
    }

}

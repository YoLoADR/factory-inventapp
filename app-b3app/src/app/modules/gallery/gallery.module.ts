import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GalleryPage } from './gallery.page';
import { DaoGalleryService } from 'src/app/providers/db/dao-gallery.service';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [GalleryPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: GalleryPage }
    ]),
    SharedModule
  ],
  providers: [DaoGalleryService]
})
export class GalleryModule { }

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GroupDiscussion } from 'src/app/models/group-discussion';
import { Observable } from 'rxjs';
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';
import { catchError, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-group-discussions-detail',
    templateUrl: './group-discussions-detail.page.html',
    styleUrls: ['./group-discussions-detail.page.scss']
})
export class GroupDiscussionsDetailPage implements OnInit {
    @Input() componentMode: boolean = false;
    @Input() eventId: string;
    @Input() groupId: string;
    group$: Observable<GroupDiscussion>;
    event$: Observable<any>;
    docs$: Observable<any>;
    @Output() goBackEvent: EventEmitter<any> = new EventEmitter();
    event: any;
    group: GroupDiscussion;
    docs: any;

    constructor(
        private route: ActivatedRoute,
        private gdService: GroupDiscussionsService
    ) { }

    ngOnInit() {
        if (!this.componentMode) {
            this.eventId = this.route.snapshot.paramMap.get('eventId');
            this.groupId = this.route.snapshot.paramMap.get('groupId');
        }
        this.group$ = this.gdService.groupDiscussion(this.groupId);
        this.event$ = this.gdService.event(this.eventId);
        this.docs$ = this.gdService.images(this.eventId, this.groupId);
    }

    /**
     * Go back to list surveys on component mode
     */
    goBackToList() {
        this.goBackEvent.emit(true);
    }
}

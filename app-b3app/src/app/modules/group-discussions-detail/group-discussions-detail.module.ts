import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GroupDiscussionsDetailPage } from './group-discussions-detail.page';
import { TranslateModule } from '@ngx-translate/core';
import { AvatarModule } from 'ngx-avatar';
import { ComponentsModulesModule } from '../components-modules.module';

const routes: Routes = [
    {
        path: '',
        component: GroupDiscussionsDetailPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        TranslateModule.forChild(),
        AvatarModule,
        ComponentsModulesModule
    ]
})
export class GroupDiscussionsDetailPageModule { }

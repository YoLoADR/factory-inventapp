import {
    Component,
    ViewEncapsulation,
    HostListener,
    OnInit
} from '@angular/core';
import { DaoWidgetsService } from 'src/app/providers/db/dao-widgets.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {
    LoadingController,
    Events,
    MenuController,
    ToastController
} from '@ionic/angular';
import {
    InAppBrowser,
    InAppBrowserOptions
} from '@ionic-native/in-app-browser/ngx';
import { GlobalService } from 'src/app/shared/services';
import {
    AngularFirestore,
    AngularFirestoreCollection,
    AngularFirestoreDocument
} from '@angular/fire/firestore';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { TypeUser } from 'src/app/models/type-user';
import { TypeVisionGroup } from 'src/app/enums/type-vision-group';
import { ModuleWidget } from 'src/app/models/module-widget';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/shared/reducers';
import { Subscription, Observable, of } from 'rxjs';
import { getConnectionStatus } from 'src/app/shared/selectors/utility.selectors';
import { mergeMap, map, tap, first, take } from 'rxjs/operators';
import { getUserData } from 'src/app/shared/selectors/user.selectors';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { DaoSpeakersService } from 'src/app/providers/db/dao-speakers.service';
import { DaoScheduleService } from 'src/app/providers/db/dao-schedule.service';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { TypeVisionModule } from 'src/app/models/type-vision-module';
import { TypeModule } from 'src/app/enums/type-module';
import { DaoSessionFeedbackService } from 'src/app/providers/db/dao-session-feedback.service';
import { DaoModulesService } from 'src/app/providers/db/dao-modules.service';

@Component({
    selector: 'app-widgets',
    templateUrl: './widgets.page.html',
    styleUrls: ['./widgets.page.scss'],
    encapsulation: ViewEncapsulation.None
})
export class WidgetsPage implements OnInit {
    public module: ModuleWidget;

    eventId: string = null;
    moduleId: string = null;
    widgets$: Observable<any[]>;
    loader: boolean = true;
    container: any;

    // inapbrowser
    options: InAppBrowserOptions = {
        location: 'no', //Or 'yes'
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        hideurlbar: 'yes',
        zoom: 'yes', //Android only ,shows browser zoom controls
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only
        closebuttoncaption: 'back', //iOS and Android
        hidenavigationbuttons: 'yes',
        disallowoverscroll: 'no', //iOS only
        toolbar: 'yes', //iOS only
        enableViewportScale: 'no', //iOS only
        allowInlineMediaPlayback: 'yes', //iOS only
        presentationstyle: 'pagesheet', //iOS only
        fullscreen: 'yes' //Windows only
    };

    event = null;
    backBtn: boolean = true;
    refWidgets: any = null;
    refEvent: any = null;
    widgetsCollection: AngularFirestoreCollection = null;
    eventDoc: AngularFirestoreDocument = null;
    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    flexerWidth: any = null;
    interval: any;
    allowLoad: boolean = true;
    userId: any = null; //Saves the uid of the logged in user.
    typeUser: any = null; //Saves the type of logged in user.
    intervalWidgets;
    userLanguage: string = 'pt_BR';
    userLanguageFormated: string = 'PtBR';

    moduleBackgroundColor: string = '#F6F6F6';
    moduleBackgroundImage: string = '';
    moduleBackgroundTypeImage: boolean = false;

    connectionStatus: string = 'PENDING';

    constructor(
        private dbWidgets: DaoWidgetsService,
        private route: ActivatedRoute,
        public loadingCtrl: LoadingController,
        private theInAppBrowser: InAppBrowser,
        private safariViewController: SafariViewController,
        public global: GlobalService,
        private afs: AngularFirestore,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private router: Router,
        private toastCtrl: ToastController,
        private store: Store<AppState>,
        private dbSpeakers: DaoSpeakersService,
        private dbSchedule: DaoScheduleService,
        private dbAttendees: DaoAttendeesService,
        private daoFeedback: DaoSessionFeedbackService,
        private dbModules: DaoModulesService
    ) {
        // Subscription on connection status
        this.store.select(getConnectionStatus).subscribe((networkStatus) => {
            if (
                this.connectionStatus == 'OFFLINE' &&
                networkStatus == 'ONLINE'
            ) {
                this.initWidget();
            }

            this.connectionStatus = networkStatus;
        });
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    @HostListener('window:resize', ['$event'])
    onResize() {
        setTimeout(() => {
            let element = document.getElementsByClassName('flexer');
            let size = element.length - 1;
            if (
                element[size].clientWidth > 0 &&
                element[size].clientWidth !== this.flexerWidth
            ) {
                this.loadWidgets();
            }
        }, 200);
    }

    ngOnInit() {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            this.loadColors();
            this.global.loadService(() => { });
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.menuBadge = this.global.notificationBadge;
            });

            this.events.subscribe('globalUserLoaded', () => {
                this.userLanguage = this.global.language;
                this.userLanguageFormated = this.convertLangFormat(
                    this.userLanguage
                );
            });
            this.initWidget();
        });
    }

    initWidget() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        this.userLanguage = this.global.language;
        this.userLanguageFormated = this.convertLangFormat(this.userLanguage);
        this.userId = this.global.userId;
        this.typeUser = this.global.userType;
        // get widget module
        this.dbWidgets.getModule(this.moduleId, (module: ModuleWidget) => {
            this.module = module;
            this.module.moduleBackgroundTypeImage = this.module
                .moduleBackgroundTypeImage
                ? this.module.moduleBackgroundTypeImage
                : this.moduleBackgroundTypeImage;
            this.module.moduleBackgroundImage = this.module
                .moduleBackgroundImage
                ? this.module.moduleBackgroundImage
                : this.moduleBackgroundImage;
            this.module.moduleBackgroundColor = this.module
                .moduleBackgroundColor
                ? this.module.moduleBackgroundColor
                : this.moduleBackgroundColor;
        });

        if (this.router.url == this.global.eventHomePage) {
            this.allowButtonsHeader = true;
        }
        this.loadEvent();
        this.loadWidgets();
    }

    // get background color of this module
    getBackground() {
        let style = {};
        style = { 'background-color': '#F6F6F6' };
        if (this.module !== undefined) {
            if (this.module['moduleBackgroundTypeImage']) {
                style = {
                    'background-image':
                        "url('" + this.module['moduleBackgroundImage'] + "')",
                    'background-size': 'cover',
                    'min-height': '100vh',
                    'background-repeat': 'no-repeat'
                };
            } else {
                if (!this.module['moduleBackgroundIsDegrade']) {
                    style = {
                        'background-color': this.module[
                            'moduleBackgroundColor'
                        ],
                        'min-height': '100vh'
                    };
                } else {
                    style = {
                        background:
                            'linear-gradient(to right, ' +
                            this.module['moduleBackgroundColor'] +
                            ',' +
                            this.module['moduleBackgroundColorSecondary'] +
                            ')',
                        'min-height': '100vh'
                    };
                }
            }
        }
        return style;
    }

    async presentLanguage(a) {
        const toast = await this.toastCtrl.create({
            message: a,
            duration: 5000
        });
        toast.present();
    }

    loadEvent() {
        this.eventDoc = this.afs.collection('events').doc(this.eventId);
        this.refEvent = this.eventDoc.valueChanges().subscribe((data: any) => {
            this.event = data;
        });
    }

    // load all widgets
    async loadWidgets() {
        this.typeUser = this.global.userType;
        this.userId = this.global.userId;
        if (
            typeof this.userId === 'undefined' ||
            this.userId === null ||
            typeof this.typeUser === 'undefined' ||
            this.typeUser === null
        ) {
            this.userId = await this.global.loadUserId();
            this.typeUser = await this.global.loadUserType();
        }

        this.widgets$ = this.afs
            .collection('modules')
            .doc(this.moduleId)
            .collection('widgets', (ref) => ref.orderBy('order', 'asc'))
            .valueChanges()
            .pipe(
                mergeMap((widgets: any) => {
                    return this.store.select(getUserData).pipe(
                        mergeMap((userData) => {
                            let aux = []; //list aux for vision group
                            // if (userData) {
                            // check widgets view type
                            // Logged
                            if (this.userId) {
                                for (let widget of widgets) {
                                    // if the type of vision is limited by groups
                                    if (
                                        this.global.userType >=
                                        TypeUser.SUPERGOD &&
                                        this.global.userType <=
                                        TypeUser.EMPLOYEE
                                    ) {
                                        aux.push(widget);
                                    } else if (
                                        widget.typeVision ===
                                        TypeVisionGroup.GROUP_ACCESS_PERMISSION
                                    ) {
                                        for (let index in widget.groups) {
                                            // checks whether the group belongs to the user groups
                                            const pos = this.global.groupsAttendees
                                                .map(function (e) {
                                                    return e.uid;
                                                })
                                                .indexOf(
                                                    widget.groups[index].uid
                                                );

                                            if (pos >= 0) {
                                                aux.push(widget);
                                                break;
                                            }
                                        }
                                    } else {
                                        aux.push(widget);
                                    }
                                }
                                // not logged
                            } else {
                                for (let widget of widgets) {
                                    if (
                                        widget.typeVision !==
                                        TypeVisionGroup.GROUP_ACCESS_PERMISSION
                                    ) {
                                        aux.push(widget);
                                    }
                                }
                            }

                            // get flexer (HTML GRID)
                            let element = document.getElementsByClassName(
                                'flexer'
                            );
                            let size = element.length - 1;
                            this.flexerWidth =
                                element[size].clientWidth > 0
                                    ? element[size].clientWidth
                                    : this.flexerWidth;

                            if (this.flexerWidth > 0) {
                                for (let item of aux) {
                                    if (
                                        item.proportion.includes('col-1row')
                                    ) {
                                        item.finalHeight =
                                            this.flexerWidth / 2;
                                    } else if (
                                        item.proportion.includes('col-2row')
                                    ) {
                                        item.finalHeight = this.flexerWidth;
                                    } else if (
                                        item.proportion.includes(
                                            'col-05row'
                                        )
                                    ) {
                                        item.finalHeight =
                                            this.flexerWidth / 4;
                                    }

                                    // Substract 17px in any case
                                    item.finalHeight -= 17;

                                    // case route is personal page, redirect to profile (SUPERGOD/GOD/CLIENT DOESN'T HAVE A PROFILE)
                                    if (
                                        item.route.includes('personal-page')
                                    ) {
                                        if (
                                            this.global.userType !==
                                            undefined &&
                                            this.global.userType !== null
                                        ) {
                                            if (this.global.userType >= 4) {
                                                item.route = `/event/${this.eventId}/personal-page/${this.global.userModuleId}/${this.global.userType}/${this.global.userId}`;
                                            } else {
                                                item.route = `/event/${this.eventId}/widget/${this.moduleId}`;
                                            }
                                        } else {
                                            item.route = `/event/${this.eventId}/widget/${this.moduleId}`;
                                        }
                                    }

                                    if (item.isExtern) {
                                        let pattern = new RegExp(
                                            '^(https?|ftp)://'
                                        );
                                        if (!pattern.test(item.route))
                                            item.route =
                                                'https://' + item.route;
                                    }
                                }

                                return of(aux);
                                // this.widgets = aux;
                            }
                            // } else {
                            //     return of([]);
                            // }
                        })
                    );
                })
            );
    }

    // open external link
    async openLink(url: string, behavior: string) {
        const available = await this.safariViewController.isAvailable();

        if (behavior === 'sf' && available) {
            this.safariViewController
                .show({
                    url: url,
                    hidden: false,
                    animated: false,
                    transition: 'curl',
                    enterReaderModeIfAvailable: true
                })
                .subscribe(
                    (result: any) => {
                        if (result.event === 'opened') console.log('Opened');
                        else if (result.event === 'loaded')
                            console.log('Loaded');
                        else if (result.event === 'closed')
                            console.log('Closed');
                    },
                    (error: any) => console.error(error)
                );
        } else {
            if (!this.global.isBrowser) {
                let target = '_system';
                this.theInAppBrowser.create(url, target, this.options);
            } else {
                window.open(url, '_system');
            }
        }
    }

    unsubscribe() {
        try {
            this.refEvent.unsubscribe();
            this.refWidgets.unsubscribe();
        } catch (e) { }
    }

    ionViewWillLeave() {
        this.allowLoad = true;
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR';
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    openWidget(widget) {
        let splitedRoute = widget.route.split('/');
        if (splitedRoute.includes('attendees') || splitedRoute.includes('schedule') || splitedRoute.includes('speakers')) {
            this.dbModules.getModule(splitedRoute[4]).pipe(take(1)).subscribe((module) => {
                this.checkNumberOfSub(module, widget);
            })
        } else {
            this.basicModuleRouting(widget);
        }
    }

    basicModuleRouting(widget) {
        this.router.navigate([widget.route]);
    }

    routingToSessionDirectly(moduleId, sessionId, sessionFeedbackModule) {
        let navigationExtras: NavigationExtras = {
            state: {
                sessionFeedbackModule: sessionFeedbackModule
            }
        }

        this.router.navigate([`/event/${this.eventId}/schedule-detail/${moduleId}/${sessionId}`], navigationExtras);
    }

    routingToSpeakerDirectly(moduleId, speaker) {
        let navigationExtras: NavigationExtras = {
            state: {
                user: speaker
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${moduleId}/4/${speaker.uid}`], navigationExtras);
    }

    routingToAttendeeDirectly(moduleId, attendee) {
        let navigationExtras: NavigationExtras = {
            state: {
                user: attendee
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${moduleId}/${attendee.type}/${attendee.uid}`], navigationExtras);
    }

    /**
     * Check number of sesions/speaker/orators
     * @param module 
     */
    async checkNumberOfSub(module, widget) {
        if (module.type == TypeModule.SCHEDULE) {
            this.dbSchedule.closeRefGetSessionsGroupsVision();

            this.daoFeedback.getFeedbackModule(this.global.eventId, async (modules) => {
                let sessionFeedbackModule = null;
                if (modules.length >= 1) {
                    sessionFeedbackModule = modules[0].uid;
                }

                // verifies the type of vision of the module. global vision
                if (module.typeVision === TypeVisionModule.GLOBAL_VISION) {
                    this.dbSchedule.getAllSessionsVisionGlobal(module.uid).pipe(take(1)).subscribe((sessions) => {
                        if (sessions.length == 1) {
                            this.routingToSessionDirectly(module.uid, sessions[0].uid, sessionFeedbackModule);
                        } else {
                            this.basicModuleRouting(widget);
                        }
                    })
                }

                // group vision
                if (module.typeVision === TypeVisionModule.GROUP_VISION) {
                    let groups = (this.global.groupsAttendees) ? this.global.groupsAttendees : await this.global.loadGroups(this.global.userId, this.global.userType, this.eventId);
                    this.dbSchedule.getSessionsGroupsVision(groups, module.uid, (data) => {
                        if (data['sessions'].length == 1) {
                            this.routingToSessionDirectly(module.uid, data['sessions'][0].uid, sessionFeedbackModule);
                        } else {
                            this.basicModuleRouting(widget);
                        }
                    })
                }

                //limited access by groups
                if (module.typeVision === TypeVisionModule.GROUP_ACCESS_PERMISSION) {
                    this.dbSchedule.getAllSessionsLimitedAccessByGroup(module.uid).pipe(take(1)).subscribe((sessions) => {
                        if (sessions.length == 1) {
                            this.routingToSessionDirectly(module.uid, sessions[0].uid, sessionFeedbackModule);
                        } else {
                            this.basicModuleRouting(widget);
                        }
                    })
                }
            })
        } else if (module.type == TypeModule.SPEAKER) {
            // verifies the type of vision of the module. global vision
            if (module.typeVision === TypeVisionModule.GLOBAL_VISION) {
                this.dbSpeakers.getSpeakersAllGlobalVision(module.uid).pipe(take(1)).subscribe((speakers) => {
                    if (speakers.length == 1) {
                        this.routingToSpeakerDirectly(module.uid, speakers[0]);
                    } else {
                        this.basicModuleRouting(widget);
                    }
                })
            }

            // group vision
            if (module.typeVision === TypeVisionModule.GROUP_VISION) {
                let groups = (this.global.groupsAttendees) ? this.global.groupsAttendees : await this.global.loadGroups(this.global.userId, this.global.userType, this.eventId);
                this.dbSpeakers.getSpeakersGroupsVision(groups, 'asc', module.uid, (speakers) => {
                    if (speakers.length == 1) {
                        this.routingToSpeakerDirectly(module.uid, speakers[0]);
                    } else {
                        this.basicModuleRouting(widget);
                    }
                })
            }

            //limited access by groups
            if (module.typeVision === TypeVisionModule.GROUP_ACCESS_PERMISSION) {
                this.dbSpeakers.getSpeakersAllLimitedAccessByGroup(module.uid).pipe(take(1)).subscribe((speakers) => {
                    if (speakers.length == 1) {
                        this.routingToSpeakerDirectly(module.uid, speakers[0]);
                    } else {
                        this.basicModuleRouting(widget);
                    }
                })
            }
        } else if (module.type == TypeModule.ATTENDEE) {
            // verifies the type of vision of the module. global vision
            if (module.typeVision === TypeVisionModule.GLOBAL_VISION) {
                this.dbAttendees.getAttendeesAllGlobalVision(module.uid).pipe(take(1)).subscribe((attendees) => {
                    if (attendees.length == 1) {
                        this.routingToAttendeeDirectly(module.uid, attendees[0]);
                    } else {
                        this.basicModuleRouting(widget);
                    }
                })
            }

            // group vision
            if (module.typeVision === TypeVisionModule.GROUP_VISION) {
                let groups = (this.global.groupsAttendees) ? this.global.groupsAttendees : await this.global.loadGroups(this.global.userId, this.global.userType, this.eventId);
                this.dbAttendees.getAttendeesGroupsVision(groups, 'asc', module.uid, (attendees) => {
                    if (attendees.length == 1) {
                        this.routingToAttendeeDirectly(module.uid, attendees[0]);
                    } else {
                        this.basicModuleRouting(widget);
                    }
                })
            }

            //limited access by groups
            if (module.typeVision === TypeVisionModule.GROUP_ACCESS_PERMISSION) {
                this.dbAttendees.getAttendeesAllLimitedAccessByGroup(module.uid).pipe(take(1)).subscribe((attendees) => {
                    if (attendees.length == 1) {
                        this.routingToAttendeeDirectly(module.uid, attendees[0]);
                    } else {
                        this.basicModuleRouting(widget);
                    }
                })
            }
        } else {
            this.basicModuleRouting(widget);
        }
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { WidgetsPage } from './widgets.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { DaoWidgetsService } from 'src/app/providers/db/dao-widgets.service';
import { IonicModule } from '@ionic/angular';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  declarations: [WidgetsPage],
  imports: [
    IonicModule,
    IonicModule,
CommonModule,
    RouterModule.forChild([
      { path: '', component: WidgetsPage }
    ]),
    AngularSvgIconModule,
    SharedModule
  ],
  providers: [
    DaoWidgetsService
  ]
})
export class WidgetsModule { }

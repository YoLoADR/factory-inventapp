import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGroupDiscussionsPage } from './list-group-discussions.page';

describe('ListGroupDiscussionsPage', () => {
  let component: ListGroupDiscussionsPage;
  let fixture: ComponentFixture<ListGroupDiscussionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGroupDiscussionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGroupDiscussionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

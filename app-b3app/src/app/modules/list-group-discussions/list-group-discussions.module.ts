import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListGroupDiscussionsPage } from './list-group-discussions.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModulesModule } from '../components-modules.module';

const routes: Routes = [
    {
        path: '',
        component: ListGroupDiscussionsPage
    },
    {
        path: 'group-chat/:groupId',
        loadChildren: 'src/app/modules/chat/chat.module#ChatModule'
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        SharedModule,
        ComponentsModulesModule
    ],
    declarations: [
        ListGroupDiscussionsPage
    ]
})
export class ListGroupDiscussionsPageModule { }

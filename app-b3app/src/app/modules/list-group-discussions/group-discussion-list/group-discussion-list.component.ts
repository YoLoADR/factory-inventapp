import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { GroupDiscussion } from 'src/app/models/group-discussion';
import { GlobalService } from 'src/app/shared/services';
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';
import { DateTime } from 'luxon';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
    selector: 'app-group-discussion-list',
    templateUrl: './group-discussion-list.component.html',
    styleUrls: ['./group-discussion-list.component.scss'],
})
export class GroupDiscussionListComponent implements OnInit {
    @Input() eventId: string;
    @Input() groupModuleId: string;
    @Input() sessionId: string;
    groupId: string;
    userId$: Promise<string>;
    groupDiscussions$: Observable<GroupDiscussion[]>;
    notifications$: any = {};

    showGroupsList: boolean = true;
    componentMode: boolean = false;

    constructor(
        public global: GlobalService,
        private gdService: GroupDiscussionsService,
        private router: Router
    ) { }

    ngOnInit() {
        this.userId$ = this.gdService.getUserId();
        if (!this.sessionId && this.groupModuleId) {
            this.componentMode = false;
            this.groupDiscussions$ = this.gdService
                .groupDiscussions(this.eventId, this.groupModuleId)
                .pipe(
                    tap((grps) => {
                        if (grps && grps.length == 1) {
                            this.goToGroup(grps[0].id);
                        }
                        grps.map((grp) => (this.notifications$[grp.id] = this.badgeNotifications(grp.id)))
                    })
                );
        } else if (this.sessionId) {
            this.componentMode = true;
            this.groupDiscussions$ = this.gdService
                .getGroupsForEventForSession(this.eventId, this.sessionId)
                .pipe(
                    tap((grps) => {
                        if (grps && grps.length == 1) {
                            this.goToGroup(grps[0].id);
                        }
                        grps.map((grp) => (this.notifications$[grp.id] = this.badgeNotifications(grp.id)))
                    })
                );
        }
    }

    convertTimestampToDate(timestamp: number) {
        return DateTime.fromMillis(timestamp).toLocaleString(
            DateTime.DATETIME_SHORT
        );
    }

    badgeNotifications(groupId: string) {
        return this.gdService.badgeNotifications(this.eventId, groupId);
    }

    isMuted(mutedParticipants: string[], uid: string) {
        return (mutedParticipants || []).includes(uid);
    }

    goToGroup(groupId) {
        if (!this.componentMode) {
            // Navigate to group discussion page
            this.router.navigate(['/event/' + this.eventId + '/list-group-discussions/' + this.groupModuleId + '/group-chat/' + groupId]);
        } else {
            // Component mode
            this.groupId = groupId;
            this.showGroupsList = false;
        }
    }

    /**
     * Go back here from child component
     */
    goBackToList() {
        this.showGroupsList = true;
        this.groupId = null;
    }
}

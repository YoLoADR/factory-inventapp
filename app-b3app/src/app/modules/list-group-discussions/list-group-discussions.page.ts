import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';
import { GlobalService } from 'src/app/shared/services';

@Component({
    selector: 'app-list-group-discussions',
    templateUrl: './list-group-discussions.page.html',
    styleUrls: ['./list-group-discussions.page.scss']
})
export class ListGroupDiscussionsPage implements OnInit {
    eventId: string;
    groupModuleId: string;
    module$: Observable<any>;
    event$: Observable<any>;
    currentUser$: Observable<any>;

    constructor(
        private route: ActivatedRoute,
        public global: GlobalService,
        private gdService: GroupDiscussionsService
    ) { }

    ngOnInit() {
        this.eventId = this.route.snapshot.paramMap.get('eventId');
        this.groupModuleId = this.route.snapshot.paramMap.get('moduleId');
        this.module$ = this.gdService.module(this.groupModuleId);
        this.event$ = this.gdService.event(this.eventId);
        this.currentUser$ = this.gdService.currentUser();
    }
}

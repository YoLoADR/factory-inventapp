/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoEventsService } from 'src/app/providers/db/dao-events.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { MenuController, Events } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { CeuEvent } from '../../models/ceu-event';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { TranslateService } from '@ngx-translate/core';
import { LuxonService } from 'src/app/providers/luxon/luxon.service';
import { EventFieldsVisibility } from 'src/app/models/event-fields-visibility';
declare let google: any;

@Component({
    selector: 'app-event',
    templateUrl: './event.page.html',
    styleUrls: ['./event.page.scss'],
})
export class EventPage implements OnInit {
    public eventId: string = null;
    public module = null

    event: CeuEvent = null;
    eventAddress;
    description: any = null;
    userLanguage: string = null;

    dayStartDate: string = null; //print the event start day
    dayEndDate: string = null; //print the event end day

    monthStartDate: string = null; //print the event start month
    monthEndDate: string = null; //print the event end month

    yearStartDate: string = null;
    yearEndDate: string = null;

    loader: boolean = true;
    @ViewChild('map', { static: false }) mapElement;
    map: any;
    mapDisplay: boolean = false;
    private refEvent: any = null;
    private eventDoc: AngularFirestoreDocument<any> = null;

    // inapbrowser
    options: InAppBrowserOptions = {
        location: 'no',//Or 'yes' 
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        hideurlbar: 'yes',
        zoom: 'yes',//Android only ,shows browser zoom controls 
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only 
        closebuttoncaption: 'back', //iOS and Android
        hidenavigationbuttons: 'yes',
        disallowoverscroll: 'no', //iOS only 
        toolbar: 'yes', //iOS only 
        enableViewportScale: 'no', //iOS only 
        allowInlineMediaPlayback: 'yes',//iOS only 
        presentationstyle: 'pagesheet',//iOS only 
        fullscreen: 'yes',//Windows only    
    }

    backBtn: boolean = true;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    allowButtonsHeader: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private theInAppBrowser: InAppBrowser,
        private menuCtrl: MenuController,
        public global: GlobalService,
        private afs: AngularFirestore,
        private events: Events,
        private dbAnalytics: DaoAnalyticsService,
        private dbEvent: DaoEventsService,
        private luxon: LuxonService,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.snapshot.params['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        this.loadEvent();

        this.dbAnalytics.moduleAccessEvent(this.eventId);

        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
    }

    loadEvent() {
        // get the name of the event.
        this.dbEvent.getModuleEvent(this.eventId, (module) => {
            this.module = module
        });

        // get data event
        this.eventDoc = this.afs.collection('events').doc(this.eventId);
        this.refEvent = this.eventDoc.valueChanges().subscribe((data: any) => {
            this.event = data;

            if (this.event['eventFields'] === undefined) {
                this.event['eventFields'] = new EventFieldsVisibility();
            }

            this.description = this.event.description

            // the user's language.
            if (this.global.language == null) {
                this.userLanguage = data.language;
            } else {
                this.userLanguage = this.global.language;
            }

            // arrange the dates of the event.

            // get the start and end days of the event.

            // start date
            const auxDayStartDate = this.luxon.convertTimestampToDate(this.event.startDate).day

            if (auxDayStartDate < 10) {
                this.dayStartDate = `0${auxDayStartDate}`
            } else {
                this.dayStartDate = auxDayStartDate.toString()
            }

            // end dates
            const auxDayEndDate = this.luxon.convertTimestampToDate(this.event.endDate).day

            if (auxDayEndDate < 10) {
                this.dayEndDate = `0${auxDayEndDate}`
            } else {
                this.dayEndDate = auxDayEndDate.toString()
            }

            // start date
            //  get up the months of the event.       
            let auxYear = this.luxon.convertTimestampToDate(this.event.startDate).year;
            this.yearStartDate = auxYear.toString().substring(2, 4);
            switch (this.userLanguage) {
                case 'pt_BR' || 'pt-BR':
                    this.monthStartDate = this.luxon.convertTimestampToDate(this.event.startDate).setLocale('pt-BR').toLocaleString({ month: 'long' })
                    break;
                case 'en_US' || 'en-US':
                    this.monthStartDate = this.luxon.convertTimestampToDate(this.event.startDate).setLocale('en-US').toLocaleString({ month: 'long' })
                    break;
                case 'es_ES' || 'es-ES':
                    this.monthStartDate = this.luxon.convertTimestampToDate(this.event.startDate).setLocale('es-ES').toLocaleString({ month: 'long' })
                    break;
                case 'fr_FR' || 'fr-FR':
                    this.monthStartDate = this.luxon.convertTimestampToDate(this.event.startDate).setLocale('fr-FR').toLocaleString({ month: 'long' })
                    break;
                case 'de_DE' || 'de-DE':
                    this.monthStartDate = this.luxon.convertTimestampToDate(this.event.startDate).setLocale('de-DE').toLocaleString({ month: 'long' })
                    break;
            }

            // this.monthStartDate = this.monthStartDate.substring(0, 3);

            // end date
            let auxYearEnd = this.luxon.convertTimestampToDate(this.event.endDate).year;
            this.yearEndDate = auxYearEnd.toString().substring(2, 4);
            switch (this.userLanguage) {
                case 'pt_BR' || 'pt-BR':
                    this.monthEndDate = this.luxon.convertTimestampToDate(this.event.endDate).setLocale('pt-BR').toLocaleString({ month: 'long' })
                    break;
                case 'en_US' || 'en-US':
                    this.monthEndDate = this.luxon.convertTimestampToDate(this.event.endDate).setLocale('en-US').toLocaleString({ month: 'long' })
                    break;
                case 'es_ES' || 'es-ES':
                    this.monthEndDate = this.luxon.convertTimestampToDate(this.event.endDate).setLocale('es-ES').toLocaleString({ month: 'long' })
                    break;
                case 'fr_FR' || 'fr-FR':
                    this.monthEndDate = this.luxon.convertTimestampToDate(this.event.endDate).setLocale('fr-FR').toLocaleString({ month: 'long' })
                    break;
                case 'de_DE' || 'de-DE':
                    this.monthEndDate = this.luxon.convertTimestampToDate(this.event.endDate).setLocale('de-DE').toLocaleString({ month: 'long' })
                    break;
            }


            if (data.placeAddress) {
                this.initMap(data.placeAddress);
            }
        });

        this.loader = false;
    }

    async initMap(address) {
        setTimeout(async () => {
            let map;
            let latLng = new google.maps.LatLng(0, 0);
            map = await new google.maps.Map(this.mapElement.nativeElement, {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            });
            this.searchAddress(map, address);
        }, 2000);
    }


    searchAddress(map, address) {
        const geoCoder = new google.maps.Geocoder();
        let myResult = null;
        geoCoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                myResult = results[0].geometry.location; // LatLng

                new google.maps.Marker({
                    position: myResult,
                    map: map,
                    title: 'Localização!'
                });

                map.setCenter(myResult);
                map.setZoom(16);
                this.mapDisplay = true;
            }
        })
    }

    openLink(url: string) {
        if (!this.global.isBrowser) {
            let target = "_blank";
            this.theInAppBrowser.create(url, target, this.options);
        } else {
            window.open(url, '_system');
        }
    }

    unsubscribe() {
        this.refEvent.unsubscribe();
    }

    ionViewWillLeave() {
        this.unsubscribe();
    }

}

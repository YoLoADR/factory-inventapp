import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EventPage } from './event.page';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { PathComponent } from 'src/app/models/path/path-components';

@NgModule({
  declarations: [EventPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: EventPage }
    ]),
    TranslateModule.forChild(),
    SharedModule
  ],
})
export class EventModule { }

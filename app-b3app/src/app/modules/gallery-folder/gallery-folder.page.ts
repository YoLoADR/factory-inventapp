import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoGalleryService } from 'src/app/providers/db/dao-gallery.service';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Events, MenuController, ModalController } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { LightboxImgComponent } from 'src/app/components/lightbox-img/lightbox-img.component';
import { FileService } from 'src/app/providers/file/file-service';


@Component({
    selector: 'app-gallery-folder',
    templateUrl: './gallery-folder.page.html',
    styleUrls: ['./gallery-folder.page.scss'],
})
export class GalleryFolderPage implements OnInit {

    public module = null
    refModule: any = null;
    documentDoc: AngularFirestoreDocument<any> = null;

    eventId: string = null;
    moduleId: string = null;
    folderId: string = null;

    images: Array<any> = [];
    loader: boolean = true;
    grid: boolean = true;
    allowGrid: boolean = false;
    allowDownload: boolean = false;
    allowDownloadAll = false;
    downloadingAll = false;
    backBtn: boolean = true;
    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    private refGallery: any = null;
    private galleryCollection: AngularFirestoreCollection<any> = null
    menuBadge: number = 0;
    doubleBack: boolean = false;
    constructor(
        private route: ActivatedRoute,
        private daoGallery: DaoGalleryService,
        public global: GlobalService,
        private afs: AngularFirestore,
        private events: Events,
        private menuCtrl: MenuController,
        private router: Router,
        private dbAnalytics: DaoAnalyticsService,
        private modalCtrl: ModalController,
        private zone: NgZone,
        private fileService: FileService
    ) {
        this.menuCtrl.enable(true);
        this.eventId = this.route.snapshot.params['eventId']; //this.route.parent.params['_value']['eventId'];
        localStorage.setItem('eventId', this.eventId);
        this.moduleId = this.route.snapshot.params['moduleId'];
        this.folderId = this.route.snapshot.params['folderId'];
        this.loadColors();
        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
        this.menuBadge = this.global.notificationBadge;
        this.events.subscribe('menuBadge', () => {
            this.zone.run(() => {
                this.menuBadge = this.global.notificationBadge;
            })
        });
        // if (this.router.getCurrentNavigation().extras.state) {
        //   if (this.router.getCurrentNavigation().extras.state['doubleBack'] == true) this.doubleBack = true;
        // }
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.getImages();
    }

    ionViewDidEnter() {
        this.documentDoc = this.afs.collection('modules').doc(this.moduleId);

        this.refModule = this.documentDoc.valueChanges().subscribe((data) => {
            this.module = data
        });
    }

    getImages() {
        this.daoGallery.getFolder(this.moduleId, this.folderId, (folder) => {
            if (folder.grid !== null && folder.grid !== undefined) {
                this.grid = folder.grid;
            }
            if (folder.allowGrid !== null && folder.allowGrid !== undefined) {
                this.allowGrid = folder.allowGrid;
            }



            if (folder.allowDownloadAll !== null && folder.allowDownloadAll !== undefined) {
                this.allowDownloadAll = folder.allowDownloadAll;
            }

            if (folder.allowDownload !== null && folder.allowDownload !== undefined) {
                this.allowDownload = folder.allowDownload;
            }


            switch (folder.orderGallery) {
                case 'asc':
                    this.galleryCollection = this.afs.collection('modules').doc(this.moduleId).collection('folders').doc(this.folderId).collection('images', ref => ref.orderBy('name', 'asc'));
                    break;
                case 'desc':
                    this.galleryCollection = this.afs.collection('modules').doc(this.moduleId).collection('folders').doc(this.folderId).collection('images', ref => ref.orderBy('name', 'desc'));
                    break;
                case 'oldest':
                    this.galleryCollection = this.afs.collection('modules').doc(this.moduleId).collection('folders').doc(this.folderId).collection('images', ref => ref.orderBy('createdAt', 'asc'));
                    break;
                case 'recent':
                    this.galleryCollection = this.afs.collection('modules').doc(this.moduleId).collection('folders').doc(this.folderId).collection('images', ref => ref.orderBy('createdAt', 'desc'));
                    break;
            }
            this.refGallery = this.galleryCollection.valueChanges().subscribe((data: any) => {
                this.images = [];
                this.images = data;
                this.loader = false;
            });
        })
        // this.daoGallery.getImages(this.moduleId, this.folderId, (images: Array<any>) => {
        //   this.images = [];
        //   this.images = images;
        //   this.loader = false;
        // });
    }

    changeGrid() {
        this.grid = !this.grid;
    }

    async downloadAll() {
        if (!this.allowDownloadAll || this.downloadingAll || this.images.length == 0) {
            return;
        }

        this.downloadingAll = true;
        for (let image of this.images) {
            try {
                if (!image.url) {
                    continue;
                }

                const extension = (image.type || 'jpg');

                await this.fileService.download(image.url, extension, {
                    directory: 'app_pictures',
                    name: image.name
                });
            }
            catch (e) {
                alert('Error: ' + JSON.stringify(e));
            }
        }
        this.downloadingAll = false;
    }

    // zoom image
    openImage(image) {
        // this.photoViewer.show(image.url, '', { share: false });
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: image.url,
                type: image.type,
                name: image.name,
                allowDownload: this.allowDownload
            }
        }).then(modal => {
            modal.present();
        });
        this.dbAnalytics.galleryImageAccess(this.eventId, image.uid, this.global.event.timezone);
    }

    // backButton() {
    //   if (this.doubleBack == true) {
    //     this.events.publish('blockReloadFolders');
    //     this.location.back();
    //     this.location.back();
    //   } else {
    //     this.location.back();
    //   }
    // }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GalleryFolderPage } from './gallery-folder.page';
import { DaoGalleryService } from 'src/app/providers/db/dao-gallery.service';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

import  { FileService } from 'src/app/providers/file/file-service';

@NgModule({
  declarations: [GalleryFolderPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: GalleryFolderPage }
    ]),
    SharedModule
  ],
  providers: [
    DaoGalleryService,  
    FileTransfer,
    File,
    FileService
  ]
})
export class GalleryFolderModule { }

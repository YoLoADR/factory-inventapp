import { Component, ViewChild, NgZone, OnInit, OnDestroy } from '@angular/core';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { IonInfiniteScroll, IonVirtualScroll, Events, MenuController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { TypeVisionModule } from '../../models/type-vision-module';
import { DaoGroupsService } from 'src/app/providers/db/dao-groups.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-attendees',
    templateUrl: './attendees.page.html',
    styleUrls: ['./attendees.page.scss']
})

export class AttendeesPage implements OnInit, OnDestroy {
    public module = null

    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonVirtualScroll, { static: false }) virtualScroll: IonVirtualScroll;
    subscriptions: Subscription[] = [];
    searchOpen: boolean = false;
    searchText: string = '';
    lastAttendees;
    attendees: Array<any> = [];
    searchedAttendees: Array<any> = [];
    eventId: string = null;
    moduleId: string = null;
    loader: boolean = true;
    typeOrder: string;
    backBtn: boolean = true;
    refModule: any = null;
    refAttendees: any = null;
    moduleDoc: AngularFirestoreDocument<any> = null;
    attendeeCollection: AngularFirestoreCollection<any> = null;

    refAllAttendees: any = null;
    allAttendeesCollection: AngularFirestoreCollection<any> = null;
    allAttendeesList: Array<any> = [];
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    allowButtonsHeader: boolean = false;

    // module views
    typeVisionModule = null//type of module views.
    GLOBAL_VISION = TypeVisionModule.GLOBAL_VISION // visão global 
    DIVIDED_BY_GROUPS = TypeVisionModule.DIVIDED_BY_GROUPS // visão dividida por grupo
    GROUP_VISION = TypeVisionModule.GROUP_VISION  // visão por grupos 
    GROUP_ACCESS_PERMISSION = TypeVisionModule.GROUP_ACCESS_PERMISSION //acessp limitado por grupo 

    groups = [] //groups of the event.
    userGroups: any = [] //user groups
    viewGroup: boolean = false; //html of the event groups

    constructor(
        private dbAttendees: DaoAttendeesService,
        private route: ActivatedRoute,
        private router: Router,
        public toastController: ToastController,
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private dbGroups: DaoGroupsService
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.events.subscribe('languageUpdate', () => {
                this.startAll();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.menuBadge = this.global.notificationBadge;
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        // get the value of the backBtn button.
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        // get the value the allowButtonsHeader
        if (this.router.url == this.global.eventHomePage) {
            this.allowButtonsHeader = true;
        }
        this.startAll();
    }

    ionViewWillEnter() {
    }

    ionViewWillLeave() {
    }

    ngOnDestroy() {
        this.viewGroup = false
        // close getModule
        this.dbAttendees.closeRefGetModule();
        this.dbGroups.closeRefContAttendeesOfGroup();
        // save another module view access
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    startAll() {
        // get the module information
        this.dbAttendees.getModule(this.moduleId, async (module) => {
            this.module = module

            this.typeOrder = module.orderUsers;
            this.typeVisionModule = module.typeVision

            // verifies the type of vision of the module. global vision
            if (this.typeVisionModule === this.GLOBAL_VISION) {
                this.viewGroup = false

                this.getFirstAttendeesGlobalVision()
                this.getAttendeesAllGlobalVision()
            }

            //divided by groups
            if (this.typeVisionModule === this.DIVIDED_BY_GROUPS) {
                this.loader = true
                this.viewGroup = true

                // load groups
                this.getGroupsEvent()
            }

            // group vision
            if (this.typeVisionModule === this.GROUP_VISION) {
                this.loader = true
                this.viewGroup = false

                // get user groups
                this.userGroups = []
                if (typeof this.global.groupsAttendees !== 'undefined') {
                    this.userGroups = this.global.groupsAttendees
                    // load attendees
                    this.getAttendeesGroupVision()
                }
                // if the groups have not been loaded, call the loadGroups function of the global service.
                else {
                    // get the id of the logged in user.
                    let userId = null

                    // receives user uid
                    if (typeof this.global.userId !== 'undefined' && this.global.userId !== null) {
                        userId = this.global.userId
                    } else {
                        userId = await this.global.loadUserId()
                    }

                    // get the type user of the logged in user.
                    let userType = null

                    if (typeof this.global.userType !== 'undefined' && this.global.userType !== null) {
                        userType = this.global.userType
                    } else {
                        userType = await this.global.loadUserType()
                    }

                    // get the type groupsAttendee of the logged in user.
                    this.userGroups = []
                    if (typeof this.global.groupsAttendees !== 'undefined') {
                        this.userGroups = this.global.groupsAttendees
                    } else {
                        this.userGroups = await this.global.loadGroups(userId, userType, this.eventId)
                    }

                    // load attendees
                    this.getAttendeesGroupVision()
                }
            }

            // group access permission
            if (this.typeVisionModule === this.GROUP_ACCESS_PERMISSION) {
                this.viewGroup = false

                this.getFirstAttendeesLimitedAccessByGroup()
                this.getAttendeesAllLimitedAccessByGroup()
            }
        })
    }
    async presentToast(time) {
        const toast = await this.toastController.create({
            message: time,
            duration: 3000
        });
        toast.present();
    }

    attendeeHeader(attendee, i, attendees) {
        if (i == 0) {
            return attendee.letter;
        } else if (i != 0 && attendee.letter != attendees[i - 1].letter) {
            return attendee.letter;
        }
    }


    // loads more attendees when the infinite scroll is triggered
    moreAttendees(event) {
        // verifies the type of vision of the module.
        if (this.typeVisionModule === this.GLOBAL_VISION) {
            this.moreAttendeesGlobalVision(event)
        }
        else if (this.typeVisionModule === this.GROUP_ACCESS_PERMISSION) {
            this.moreAttendeesAccessByGroup(event)
        }
        else {
            event.target.disabled = true;
        }
    }

    profileDetails(attendee) {
        let navigationExtras: NavigationExtras = {
            state: {
                user: attendee
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${this.moduleId}/${attendee.type}/${attendee.uid}`], navigationExtras);
        this.global.previousPage = null;
    }

    searchBar(ev) {
        if (ev.target.value.length >= 1) {
            let value = ev.target.value.toLowerCase();
            this.searchedAttendees = [];
            this.allAttendeesList.filter(item => {
                if (item.name.toLowerCase().includes(value)) {
                    item.principal_title = this.getAttendeePrincipalTitle(item.title);
                    this.searchedAttendees.push(item);
                }
            })
        } else {
            this.searchedAttendees = [];
        }
    }



    // load the event groups.
    getGroupsEvent() {
        this.dbGroups.getGroups(this.eventId, (groups) => {
            this.groups = groups

            // Sort the groups alphabetically.
            groups.sort(function (a, b) {
                return a.queryName < b.queryName ? -1 : a.queryName > b.queryName ? 1 : 0;
            });

            // checks the number of sessions in the module that belongs to each group.
            for (const group of groups) {
                const groupId = group.uid
                const moduleAttendeeId = this.moduleId

                group.qtdSessions = 0

                // count the number of participants the group has in the module.
                this.dbGroups.contAttendeesOfGroup(groupId, moduleAttendeeId, (qtd: number) => {
                    group.qtdSessions = qtd
                })
            }

            this.loader = false
        })
    }

    /******************************************************************************** methods of global vision *************************************************************************************** */

    // loads the first 100 participants of the module. global vision
    getFirstAttendeesGlobalVision() {
        this.dbAttendees.getFirstAttendeesGlobalVision(this.typeOrder, this.moduleId, (data) => {

            let list = data

            //get the last participant
            if (list.length > 0) {
                this.lastAttendees = data[data.length - 1];
            }

            list.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getAttendeePrincipalTitle(element.title);
                this.attendees.push(element);
            });

            this.attendees = list
            this.loader = false
        })
    }

    // get all attendees of the module
    getAttendeesAllGlobalVision() {
        this.subscriptions.push(this.dbAttendees.getAttendeesAllGlobalVision(this.moduleId).subscribe((data) => {
            this.allAttendeesList = data
        }))
    }


    // loads more attendees when the infinite scroll is triggered (global vision)
    moreAttendeesGlobalVision(event) {
        if (this.lastAttendees) {
            let start = Date.now();

            this.dbAttendees.getNextPageAttendeesGlobalVision(this.moduleId, this.lastAttendees, this.typeOrder, (data) => {

                this.lastAttendees = null

                if (data.length > 0) {
                    this.lastAttendees = data[data.length - 1];
                }

                data.forEach(element => {
                    var letters = element.name.split('');
                    var letter = letters[0].toUpperCase();
                    element.letter = letter;
                    element.principal_title = this.getAttendeePrincipalTitle(element.title);
                    this.attendees.push(element);
                });

                if (this.lastAttendees) {
                    this.virtualScroll.checkEnd();
                    event.target.complete();
                } else {
                    event.target.disabled = true;
                }
            })
        } else {
            event.target.disabled = true;
        }
    }


    /******************************************************************************** methods of divided by groups *************************************************************************************** */
    // loads the attendees of the group.
    getAttendeesGroupDividByGroup(group) {
        const groupId = group.uid

        this.loader = true
        this.viewGroup = false

        // loads the group attendees.
        this.dbAttendees.getAttendeesGroupGlobalVision(groupId, this.typeOrder, this.moduleId, (data) => {
            this.loader = false
            this.lastAttendees = null

            if (data.length > 0) {
                this.lastAttendees = data[data.length - 1]
            }

            data.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getAttendeePrincipalTitle(element.title);
                this.attendees.push(element);
            });

            this.attendees = data
            this.allAttendeesList = data
        })
    }

    /******************************************************************************** methods of group vision *************************************************************************************** */
    // loads the participant group attendees.
    getAttendeesGroupVision() {
        this.dbAttendees.getAttendeesGroupsVision(this.userGroups, this.typeOrder, this.moduleId, (data) => {
            this.loader = false
            this.lastAttendees = null

            if (data.length > 0) {
                this.lastAttendees = data[data.length - 1]
            }

            data.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getAttendeePrincipalTitle(element.title);
                this.attendees.push(element);
            });

            this.attendees = data
            this.allAttendeesList = data
        })
    }

    /******************************************************************************** methods of limited access by groups *************************************************************************************** */
    // loads the first 100 participants of the module. 
    getFirstAttendeesLimitedAccessByGroup() {
        // let start = Date.now();
        this.dbAttendees.getFirstAttendeesGlobalVision(this.typeOrder, this.moduleId, (data) => {
            let list = data

            //get the last participant
            if (list.length > 0) {
                this.lastAttendees = data[data.length - 1];
            }

            list.forEach(element => {
                var letters = element.name.split('');
                var letter = letters[0].toUpperCase();
                element.letter = letter;
                element.principal_title = this.getAttendeePrincipalTitle(element.title);
                this.attendees.push(element);
            });

            this.attendees = list
            this.loader = false

            // if (this.attendees.length == data.length) {
            //   this.loader = false;
            // }
        })
    }

    // get all attendees of the module
    getAttendeesAllLimitedAccessByGroup() {
        this.subscriptions.push(this.dbAttendees.getAttendeesAllLimitedAccessByGroup(this.moduleId).subscribe((data) => {
            this.allAttendeesList = data
        }))
    }


    // loads more attendees when the infinite scroll is triggered 
    moreAttendeesAccessByGroup(event) {
        if (this.lastAttendees) {
            let start = Date.now();

            this.dbAttendees.getNextPageAttendeesLimitedAccessByGroup(this.moduleId, this.lastAttendees, this.typeOrder, (data) => {
                this.lastAttendees = null

                if (data.length > 0) {
                    this.lastAttendees = data[data.length - 1];
                }

                data.forEach(element => {
                    var letters = element.name.split('');
                    var letter = letters[0].toUpperCase();
                    element.letter = letter;
                    element.principal_title = this.getAttendeePrincipalTitle(element.title);
                    this.attendees.push(element);
                });

                if (this.lastAttendees) {
                    this.virtualScroll.checkEnd();
                    event.target.complete();
                } else {
                    event.target.disabled = true;
                }
            })
        } else {
            event.target.disabled = true;
        }
    }


    getAttendeePrincipalTitle(title) {
        let principalTitle = '';
        switch (this.global.language) {
            case 'pt_BR': {
                principalTitle = title.PtBR;
                break;
            }
            case 'en_US': {
                principalTitle = title.EnUS;
                break;
            }
            case 'es_ES': {
                principalTitle = title.EsES;
                break;
            }
            case 'fr_FR': {
                principalTitle = title.FrFR;
                break;
            }
            case 'de_DE': {
                principalTitle = title.deDE;
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == '' || principalTitle == null) {
            if (title[this.convertLangFormat(this.global.event.language)] !== '') {
                principalTitle = title[this.convertLangFormat(this.global.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== '') {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        return principalTitle;
    }

    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AttendeesPage } from './attendees.page';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { AvatarModule } from 'ngx-avatar';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [AttendeesPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: AttendeesPage }
    ]),
    NgxQRCodeModule,
    AvatarModule,
    SharedModule
  ],
  providers: [DaoAttendeesService]
})
export class AttendeesModule {
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { MenuController, Events, AlertController } from '@ionic/angular';
import { DaoWordCloudService } from 'src/app/providers/db/dao-word-cloud.service';
import { TranslateService } from '@ngx-translate/core';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
    selector: 'app-word-cloud',
    templateUrl: './word-cloud.page.html',
    styleUrls: ['./word-cloud.page.scss'],
})
export class WordCloudPage implements OnInit {

    eventId: string = null;
    moduleId: string = null;
    cloudId: string = null;
    module;
    form: FormGroup;

    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    loader: boolean = false;
    moduleName: string = null;

    cloud = null;
    wordRequired = false;
    wordMax = false;
    wordTitle: string;
    wordSubtitle: string;
    wordMaxChars: number;

    constructor(private route: ActivatedRoute,
        private fb: FormBuilder,
        public global: GlobalService,
        private menuCtrl: MenuController,
        private events: Events,
        private daoWordCloud: DaoWordCloudService,
        public alertController: AlertController,
        private translateService: TranslateService,
        private keyboard: Keyboard
    ) {

        // get event id and module id
        this.route.params.subscribe((params) => {
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.cloudId = params.cloudId;
            // this.cloudId = this.route.snapshot.params['cloudId'];
            this.menuCtrl.enable(true);

            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });

            this.daoWordCloud.getCloud(this.moduleId, this.cloudId, (cloud) => {
                this.cloud = cloud;

                this.wordTitle = cloud['title'];
                this.wordSubtitle = cloud['subtitle'];
                this.wordMaxChars = cloud['max_characters'];

                this.form = fb.group({
                    'wordAnswer': [null, Validators.compose([Validators.required, Validators.maxLength(this.cloud.wordMaxChars), Validators.pattern('^[A-Za-z]+$')])],
                });
            })

            this.form = fb.group({
                'wordAnswer': [null, Validators.compose([Validators.required, Validators.pattern('^[A-Za-z]+$')])],
            });
        })
    }

    ngOnInit() {
        this.daoWordCloud.getModule(this.moduleId, (module) => {
            this.module = module;
            this.moduleName = module.name;
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    sendWord() {
        let valid = !this.form.get('wordAnswer').invalid;

        if (valid) {
            const answer = this.form.value.wordAnswer;
            if (answer == '' || answer == null) {
                this.wordMax = false;
                this.wordRequired = true;
            } else if (answer !== '' && answer.length < this.wordMaxChars) {
                this.loader = true;
                this.daoWordCloud.setUserWordCloud(this.moduleId, this.cloudId, this.global.userId, answer, (data) => {
                    if (data) {
                        this.wordMax = false;
                        this.wordRequired = false;
                        this.presentAlertConfirm();
                    }
                });
            } else {
                this.wordRequired = false;
                this.wordMax = true;
            }
        }
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('pages.word_cloud.confirm_alert'),
            message: this.translateService.instant('pages.word_cloud.confirm_alert_message'),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.ok'),
                    handler: () => {
                        this.loader = false;
                        this.form.reset();
                    }
                }
            ],
            backdropDismiss: false
        });

        await alert.present();
    }

}

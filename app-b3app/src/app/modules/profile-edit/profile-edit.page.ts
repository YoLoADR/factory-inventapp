import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { DaoSpeakersService } from 'src/app/providers/db/dao-speakers.service';
import { NavController, LoadingController, Events, MenuController, ModalController } from '@ionic/angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { AlertController } from '@ionic/angular';
import { StorageService } from '../../providers/storage/storage.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GlobalService } from 'src/app/shared/services';
import { DaoEventsService } from 'src/app/providers/db/dao-events.service';
import { TranslateService } from '@ngx-translate/core';
import { LightboxImgComponent } from 'src/app/components/lightbox-img/lightbox-img.component';
import { TypeUser } from 'src/app/models/type-user';
import { RegexProvider } from 'src/app/providers/regex/regex.service';
import { environment } from 'src/environments/environment';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-profile-edit',
    templateUrl: './profile-edit.page.html',
    styleUrls: ['./profile-edit.page.scss'],
})

export class ProfileEditPage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    public eventId: string = null;
    public userId: string = null;
    public moduleId: string = null;
    public typeUser: number = null;

    public attendee: any;

    public loader: boolean = true;
    public company: string = '';
    public title: string = '';
    public name: string = '';
    public site: string = '';
    public facebook: string = '';
    public instagram: string = '';
    public linkedin: string = '';
    public twitter: string = '';
    // public phone: string = '';
    public description: string = '';
    public photoUrl: string = null;
    public file: any = null;

    public listCustomFields: Array<any> = [];
    public listCustomFieldOptions: Array<any> = [];

    // inapbrowser
    options: InAppBrowserOptions = {
        location: 'no',//Or 'yes' 
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        hideurlbar: 'yes',
        zoom: 'yes',//Android only ,shows browser zoom controls 
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only 
        closebuttoncaption: 'back', //iOS and Android
        hidenavigationbuttons: 'yes',
        disallowoverscroll: 'no', //iOS only 
        toolbar: 'yes', //iOS only 
        enableViewportScale: 'no', //iOS only 
        allowInlineMediaPlayback: 'yes',//iOS only 
        presentationstyle: 'pagesheet',//iOS only 
        fullscreen: 'yes',//Windows only    
    }
    formValidation: FormGroup;
    fieldOptions;
    fieldsCustomOptions;
    platformIs: boolean;
    container: any;
    backBtn: boolean = true;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    pictureLoader: boolean = false;

    // langs enable
    enableLangPt: boolean = false;
    enableLangDe: boolean = false;
    enableLangEs: boolean = false;
    enableLangEn: boolean = false;
    enableLangFr: boolean = false;
    userLanguage: string = environment.platform.defaultLanguage;
    hideMenu: boolean = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private dbAttendees: DaoAttendeesService,
        private dbSpeakers: DaoSpeakersService,
        private dbEvent: DaoEventsService,
        private navCtrl: NavController,
        private theInAppBrowser: InAppBrowser,
        private storage: StorageService,
        public alertController: AlertController,
        private fb: FormBuilder,
        public global: GlobalService,
        public loadingCtrl: LoadingController,
        private events: Events,
        private menuCtrl: MenuController,
        private modalCtrl: ModalController,
        private translateService: TranslateService,
        private zone: NgZone,
        private regex: RegexProvider,
        private keyboard: Keyboard,
        private location: Location
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.userId = params.userId;
            // this.userId = this.route.snapshot.params['userId'];
            this.typeUser = params.type;
            // this.typeUser = this.route.snapshot.params['type'];
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });

            this.formValidation = fb.group({
                'name': [null],
                'company': [null],
                'title': [null],
                'email': [null],
                // 'phone': [null],
                'site': [null],
                'linkedin': [null],
                'facebook': [null],
                'instagram': [null],
                'twitter': [null],
                'description': [null],
                // 'language': [null]
            });

            this.platformIs = this.global.isBrowser;
        })
        // *ngIf="event.description.pt_BR !== undefined && event.description.pt_BR !== null">
    }


    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }
        this.userLanguage = this.formatUserLang(this.global.language);

        this.startEvent();
        if (this.typeUser == 4) {
            this.getFieldOptionsSpeaker();
        } else if (this.typeUser == 5) {
            this.getFieldOptionsAttendee();
        }
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    // load attendee
    allowLoaderFirstAccess: boolean = true;
    loadAttendee() {
        this.subscriptions.push(this.dbAttendees.getAttendeeByEvent(this.eventId, this.userId).subscribe((attendee) => {
            let aux = this.getSpeakerPrincipalTitle(attendee.title, attendee.description);
            this.attendee = attendee;
            this.photoUrl = attendee['photoUrl'];

            this.formValidation.patchValue({
                'name': attendee['name'],
                'company': attendee['company'],
                'title': aux.title,
                'email': attendee['email'],
                'description': aux.description,
                'photoUrl': attendee['photoUrl'],
                'site': attendee['website'],
                'facebook': attendee['facebook'],
                'instagram': attendee['instagram'],
                'linkedin': attendee['linkedin'],
                'twitter': attendee['twitter'],
                // 'phone': attendee['phone'],
                // 'language': attendee['language']
            });

            if (this.allowLoaderFirstAccess) {
                this.allowLoaderFirstAccess = false;
                this.loader = false;
            }

            this.getFieldsCustomAttendee();
            this.setUniqueEditFields();
        }));
    }

    getFieldsCustomAttendee() {
        this.dbAttendees.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.fieldsCustomOptions = fields;
            this.getCustomFieldsAttendee();
        });
    }


    getFieldOptionsAttendee() {
        this.dbAttendees.getFieldOptions(this.moduleId, (fields) => {
            this.fieldOptions = fields;
            this.setControlFields(fields);
        });
    }

    getFieldOptionsSpeaker() {
        this.dbSpeakers.getFieldOptions(this.moduleId, (fields) => {
            this.fieldOptions = fields;
            this.setControlFields(fields);
        });
    }


    setControlFields(fields) {
        if (fields.name.required == true) {
            this.formValidation.setControl('name', new FormControl(null, Validators.required));
        }
        if (fields.company.required == true) {
            this.formValidation.setControl('company', new FormControl(null, Validators.required));
        }
        if (fields.title.required == true) {
            this.formValidation.setControl('title', new FormControl(null, Validators.required));
        }
        if (fields.email.required == true) {
            this.formValidation.setControl('email', new FormControl(null, Validators.required));
        }
        // if (fields.phone.required == true) {
        //   this.formValidation.setControl('phone', new FormControl(null, Validators.required));
        // }
        if (fields.site.required == true) {
            this.formValidation.setControl('site', new FormControl(null, Validators.required));
        }
        if (fields.linkedin.required == true) {
            this.formValidation.setControl('linkedin', new FormControl(null, Validators.required));
        }
        if (fields.facebook.required == true) {
            this.formValidation.setControl('facebook', new FormControl(null, Validators.required));
        }
        if (fields.instagram.required == true) {
            this.formValidation.setControl('instagram', new FormControl(null, Validators.required));
        }
        if (fields.twitter.required == true) {
            this.formValidation.setControl('twitter', new FormControl(null, Validators.required));
        }
        if (fields.description.required == true) {
            this.formValidation.setControl('description', new FormControl(null, Validators.required));
        }

        if (this.typeUser == 4) {
            this.loadSpeaker();
        } else if (this.typeUser == 5) {
            this.loadAttendee();
        }

    }

    setUniqueEditFields() {
        let value = this.formValidation.value;
        if (this.fieldOptions.name.unique_edit == true && value.name !== null) {
            this.formValidation.get('name').disable();
        }
        if (this.fieldOptions.company.unique_edit == true && value.company !== null) {
            this.formValidation.get('company').disable();
        }
        if (this.fieldOptions.title.unique_edit == true && value.title !== null) {
            this.formValidation.get('title').disable();
        }
        if (this.fieldOptions.email.unique_edit == true && value.email !== null) {
            this.formValidation.get('email').disable();
        }
        // if (this.fieldOptions.phone.unique_edit == true && value.phone !== null) {
        //   this.formValidation.get('phone').disable();
        // }
        if (this.fieldOptions.site.unique_edit == true && value.site !== null) {
            this.formValidation.get('site').disable();
        }
        if (this.fieldOptions.linkedin.unique_edit == true && value.linkedin !== null) {
            this.formValidation.get('linkedin').disable();
        }
        if (this.fieldOptions.facebook.unique_edit == true && value.facebook !== null) {
            this.formValidation.get('facebook').disable();
        }
        if (this.fieldOptions.instagram.unique_edit == true && value.instagram !== null) {
            this.formValidation.get('instagram').disable();
        }
        if (this.fieldOptions.twitter.unique_edit == true && value.twitter !== null) {
            this.formValidation.get('twitter').disable();
        }
        if (this.fieldOptions.description.unique_edit == true && value.description !== null) {
            this.formValidation.get('description').disable();
        }
    }

    getCustomFieldsAttendee() {
        this.dbAttendees.getCustomFields(this.eventId, this.userId, (customFields) => {
            // this.listCustomFields = customFields;
            for (let aux of customFields) {
                let position = this.fieldsCustomOptions[aux.uid].order;
                this.listCustomFields[position] = aux;
            }

            for (let i = 0; i < this.listCustomFields.length; i++) {
                let custom = this.listCustomFields[i];

                if (custom.type == 'select') {
                    if (custom.value == '' || custom.value == null || custom.value == undefined) { custom.value = '-1'; }
                    this.dbAttendees.getCustomFieldOptions(this.moduleId, this.userId, custom.uid, (listOptions) => {
                        this.zone.run(() => {
                            this.listCustomFieldOptions[i] = listOptions;
                            if (i == this.listCustomFieldOptions.length - 1) {
                                this.setFieldOptionsInCustomAttendee();
                            }
                        })

                    })
                } else {
                    this.listCustomFieldOptions[i] = null;

                    if (i == this.listCustomFieldOptions.length - 1) {
                        this.setFieldOptionsInCustomAttendee();
                    }
                }
            }
        })
    }

    // load speaker
    loadSpeaker() {
        this.subscriptions.push(this.dbSpeakers.getSpeakerByEvent(this.eventId, this.userId).subscribe((attendee) => {
            this.attendee = attendee;
            this.photoUrl = attendee['photoUrl'];
            let auxSpeaker = this.getSpeakerPrincipalTitle(attendee['title'], attendee['description']);

            this.formValidation.patchValue({
                'name': attendee['name'],
                'company': attendee['company'],
                'title': auxSpeaker.title,
                'email': attendee['email'],
                'description': this.regex.removeHtml(auxSpeaker.description),
                'photoUrl': attendee['photoUrl'],
                'site': attendee['website'],
                'facebook': attendee['facebook'],
                'instagram': attendee['instagram'],
                'linkedin': attendee['linkedin'],
                'twitter': attendee['twitter'],
                // 'phone': attendee['phone'],
                // 'language': attendee['language']
            });

            this.setUniqueEditFields();
            this.loader = false;

            this.getFieldsCustomSpeaker();

        }));
    }

    getFieldsCustomSpeaker() {
        this.dbSpeakers.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.fieldsCustomOptions = fields;
            this.getCustomFieldsSpeaker();
        });
    }

    getCustomFieldsSpeaker() {
        this.dbSpeakers.getCustomFields(this.eventId, this.userId, (customFields) => {
            this.zone.run(() => {
                for (let aux of customFields) {
                    let position = this.fieldsCustomOptions[aux.uid].order;
                    this.listCustomFields[position] = aux;
                }

                for (let i = 0; i < this.listCustomFields.length; i++) {
                    let custom = this.listCustomFields[i];
                    if (custom.type == 'select') {
                        if (custom.value == '' || custom.value == null || custom.value == undefined) custom.value = '-1';

                        this.dbSpeakers.getCustomFieldOptions(this.moduleId, custom.uid, (listOptions) => {
                            this.listCustomFieldOptions[i] = listOptions;

                            if (i == this.listCustomFieldOptions.length - 1) {
                                this.setFieldOptionsInCustomSpeaker();
                            }
                        })
                    } else {
                        this.listCustomFieldOptions[i] = null;

                        if (i == this.listCustomFieldOptions.length - 1) {
                            this.setFieldOptionsInCustomSpeaker();
                        }
                    }
                }
            })
        })
    }


    setFieldOptionsInCustomAttendee() {
        this.dbAttendees.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.fieldsCustomOptions = fields;

            for (let custom of this.listCustomFields) {
                custom.required = this.fieldsCustomOptions[custom.uid].required;
                custom.unique_edit = this.fieldsCustomOptions[custom.uid].unique_edit;
                custom.initial_value = custom.value;

            }
        });
    }

    setFieldOptionsInCustomSpeaker() {
        this.dbSpeakers.getFieldOptionsCustom(this.moduleId, (fields) => {
            this.fieldsCustomOptions = fields;

            for (let custom of this.listCustomFields) {
                custom.required = this.fieldsCustomOptions[custom.uid].required;
                custom.unique_edit = this.fieldsCustomOptions[custom.uid].unique_edit;
                custom.initial_value = custom.value;

            }
        });
    }

    async selectedImage() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('pages.editprofile.profile_pic_select'),
            message: this.translateService.instant('pages.editprofile.gallery_or_camera'),
            buttons: [
                {
                    text: this.translateService.instant('pages.editprofile.gallery'),
                    handler: () => {
                        this.pictureLoader = true;
                        this.storage.openGallery((data) => {
                            this.file = data;
                            this.pictureLoader = false;
                        })
                    }
                },
                {
                    text: this.translateService.instant('pages.editprofile.camera'),
                    handler: () => {
                        this.pictureLoader = true;
                        this.storage.takePicture((data) => {
                            this.file = data;
                            this.pictureLoader = false;
                        })
                    }
                }
            ]
        });

        await alert.present();
    }

    selectPictureFromWeb(ev) {
        this.pictureLoader = true;
        const fileWeb: File = ev.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(fileWeb);
        reader.onload = () => this.file = reader.result;
        reader.onerror = error => console.log(error);
        this.pictureLoader = false;
    }

    updateUser() {
        this.loader = true;
        let value = this.formValidation.getRawValue();
        let userLanguage = this.formatUserLang(this.global.language);
        let requiredEmpty = false;
        if (this.fieldOptions['name'].required == true && (value.name == null || value.name == undefined || value.name == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['company'].required == true && (value.company == null || value.company == undefined || value.company == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['title'].required == true && (value.title == null || value.title == undefined || value.title == '')) {
            requiredEmpty = true;
        }
        // if (this.fieldOptions['phone'].required == true && (value.phone == null || value.phone == undefined || value.phone == '')) {
        //   requiredEmpty = true;
        // }
        if (this.fieldOptions['site'].required == true && (value.site == null || value.site == undefined || value.site == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['linkedin'].required == true && (value.linkedin == null || value.linkedin == undefined || value.linkedin == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['facebook'].required == true && (value.facebook == null || value.facebook == undefined || value.facebook == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['instagram'].required == true && (value.instagram == null || value.instagram == undefined || value.instagram == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['twitter'].required == true && (value.twitter == null || value.twitter == undefined || value.twitter == '')) {
            requiredEmpty = true;
        }
        if (this.fieldOptions['description'].required == true && (value.description == null || value.description == undefined || value.description == '')) {
            requiredEmpty = true;
        }

        for (let custom of this.listCustomFields) {
            // if (this.typeUser == TypeUser.SPEAKER) {
            if (custom.type == 'select' && custom.value[this.userLanguage] == '-1') {
                custom.value[this.userLanguage] = null;
            }
            if (custom.required == true && (custom.value[this.userLanguage] == '' || custom.textValue[this.userLanguage] == '')) {
                requiredEmpty = true;
            }

            if (custom.type == 'text') {
                for (let lang in custom.textValue) {
                    if (lang !== this.userLanguage) {
                        custom.textValue[lang] = custom.textValue[this.userLanguage];
                    }
                }
            }

            // } else if (this.typeUser == TypeUser.ATTENDEE) {
            //   if (custom.required == true && (custom.value == '' || custom.value == undefined || custom.value == null)) {
            //     requiredEmpty = true;
            //   }
            //   if (custom.type == 'select' && custom.value == '-1') {
            //     custom.value = null;
            //   }
            // }

        }
        if (requiredEmpty == false) {
            this.attendee.name = value.name;
            this.attendee.company = value.company;

            // if (this.typeUser == TypeUser.SPEAKER) {
            this.attendee.title[userLanguage] = value.title ? value.title : '';
            this.attendee.description[userLanguage] = value.description ? value.description : '';
            // } else {
            //   this.attendee.title = value.title;
            //   this.attendee.description = value.description;
            // }
            // this.attendee.phone = value.phone;
            this.attendee.website = value.site;
            this.attendee.linkedin = value.linkedin;
            this.attendee.facebook = value.facebook;
            this.attendee.instagram = value.instagram;
            this.attendee.twitter = value.twitter;
            this.attendee.edited_profile = true;
            // this.attendee.language = value.language;
            if (this.typeUser == TypeUser.ATTENDEE) {
                if (this.global.userEditProfile !== true) {
                    this.attendee.edited_profile = true;
                }
                this.dbAttendees.updateAttendee(this.eventId, this.moduleId, this.attendee, this.listCustomFields, this.file, async (data) => {
                    if (data.code == 200) {
                        await this.presentAlertSuccess();
                        this.events.publish('languageUpdate', this.global.language);
                        this.menuCtrl.enable(true);
                        this.hideMenu = false;
                        this.loader = false;
                    } else {
                        this.presentAlertError();
                        this.loader = false;
                    }
                })
            } else if (this.typeUser == 4) {
                this.dbSpeakers.updateSpeaker(this.eventId, this.moduleId, this.attendee, this.listCustomFields, this.file, (data) => {
                    if (data.code == 200) {
                        this.presentAlertSuccess();
                        this.events.publish('languageUpdate', this.global.language);
                        this.menuCtrl.enable(true);
                        this.hideMenu = false;
                        this.loader = false;
                    } else {
                        this.presentAlertError();
                        this.loader = false;
                    }
                })
            }
        } else {
            setTimeout(() => {
                this.presentAlertErrorRequiredField();
                this.loader = false;
            }, 1000);
        }

    }

    async presentAlertErrorRequiredField() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('pages.editprofile.required_field_title'),
            message: this.translateService.instant('pages.editprofile.required_fields_text'),
            buttons: [{
                text: this.translateService.instant('global.buttons.ok')
            }]
        })

        await alert.present();
    }


    async presentAlertSuccess() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('pages.editprofile.all_ok'),
            message: this.translateService.instant('pages.editprofile.all_ok_subtitle'),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.ok'),
                    handler: () => {
                        this.personalPage();
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentAlertError() {
        const alert = await this.alertController.create({
            header: this.translateService.instant('global.alerts.error'),
            message: this.translateService.instant('global.alerts.not_success'),
            buttons: [{
                text: this.translateService.instant('global.buttons.ok')
            }]
        });

        await alert.present();
    }

    async openLoadingContainer() {
        if (!this.container) {
            this.container = await this.loadingCtrl.create({
                spinner: 'crescent'
            });
            this.container.present();
        }
    }

    async closeLoadingContainer() {
        if (this.container) {
            await this.container.dismiss();
            this.container = null;
        }
    }

    // zoom profile picture image
    openImage(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: url
            }
        }).then(modal => {
            modal.present();
        });
    }

    openLink(url: string) {
        let target = "_blank";
        this.theInAppBrowser.create(url, target, this.options);
    }

    personalPage() {
        try {
            if (this.global.cantGoBackFromProfile) {
                this.router.navigate([`/event/${this.eventId}/personal-page/${this.moduleId}/${this.typeUser}/${this.userId}`]);
            } else {
                this.location.back();
            }
        } catch (e) {
            this.router.navigate([`/event/${this.eventId}/personal-page/${this.moduleId}/${this.typeUser}/${this.userId}`]);
        }
    }

    startEvent() {
        this.subscriptions.push(this.dbEvent.getEvent(this.eventId).subscribe((event) => {
            if (event.required_edit_profile) {
                if (this.global.user.edited_profile == false || this.global.user.edited_profile == null || this.global.user.edited_profile == undefined) {
                    this.menuCtrl.enable(false);
                    this.hideMenu = true;
                }
            }
            if (event.description.de_DE !== undefined && event.description.de_DE !== null) {
                this.enableLangDe = true;
            }

            if (event.description.es_ES !== undefined && event.description.es_ES !== null) {
                this.enableLangEs = true;
            }

            if (event.description.en_US !== undefined && event.description.en_US !== null) {
                this.enableLangEn = true;
            }

            if (event.description.fr_FR !== undefined && event.description.fr_FR !== null) {
                this.enableLangFr = true;
            }

            if (event.description.pt_BR !== undefined && event.description.pt_BR !== null) {
                this.enableLangPt = true;
            }
        }));
    }

    getSpeakerPrincipalTitle(title, description) {
        let principalTitle = '';
        let principalDescription = '';
        switch (this.global.language) {
            case 'pt_BR': {
                principalTitle = title.PtBR ? title.PtBR : '';
                principalDescription = description.PtBR ? description.PtBR : '';
                break;
            }
            case 'en_US': {
                principalTitle = title.EnUS ? title.EnUS : '';
                principalDescription = description.EnUS ? description.EnUS : '';
                break;
            }
            case 'es_ES': {
                principalTitle = title.EsES ? title.EsES : '';
                principalDescription = description.EsES ? description.EsES : '';
                break;
            }
            case 'fr_FR': {
                principalTitle = title.FrFR ? title.FrFR : '';
                principalDescription = description.FrFR ? description.FrFR : '';
                break;
            }
            case 'de_DE': {
                principalTitle = title.DeDE ? title.DeDE : '';
                principalDescription = description.DeDE ? description.DeDE : '';
                break;
            }
        }

        // case blank, get principal event language title or first language !== blank
        if (principalTitle == '' || principalTitle == null) {
            if (title[this.formatUserLang(this.global.event.language)] !== '') {
                principalTitle = title[this.formatUserLang(this.global.event.language)];
            } else {
                for (let aux in title) {
                    if (title[aux] !== '') {
                        principalTitle = title[aux];
                    }
                }
            }
        }
        if (principalDescription == '' || principalDescription == null) {
            if (description[this.formatUserLang(this.global.event.language)] !== '') {
                principalDescription = description[this.formatUserLang(this.global.event.language)];
            } else {
                for (let aux in description) {
                    if (description[aux] !== '') {
                        principalDescription = description[aux];
                    }
                }
            }
        }
        return {
            title: principalTitle,
            description: principalDescription
        };
    }

    formatUserLang(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR';
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return formatedLang;
    }

    ionViewWillLeave() {
        this.menuCtrl.enable(true);
    }

    popPage() {
        this.navCtrl.back();
    }
}

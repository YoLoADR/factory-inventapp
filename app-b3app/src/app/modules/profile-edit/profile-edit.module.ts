import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProfileEditPage } from './profile-edit.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { Camera } from '@ionic-native/camera/ngx';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
    declarations: [ProfileEditPage],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: ProfileEditPage }
        ]),
        AvatarModule,
        SharedModule
    ],
    providers: [Camera]
})
export class ProfileEditModule { }

import { Component, OnInit, NgZone } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { Events, MenuController, ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { LightboxImgComponent } from 'src/app/components/lightbox-img/lightbox-img.component';

@Component({
    selector: 'app-location',
    templateUrl: './location.page.html',
    styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
    backBtn: boolean = true;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    eventId: string = null;
    locationId: string = null;

    refLocation: any = null;
    locationDocument: AngularFirestoreDocument<any> = null;
    location: any = null;
    allowButtonsHeader: boolean = false;

    constructor(
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private route: ActivatedRoute,
        private afs: AngularFirestore,
        private router: Router,
        private modalCtrl: ModalController,
        private zone: NgZone
    ) {
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.locationId = params.locationId;
            // this.locationId = this.route.snapshot.params['locationId'];
            this.menuCtrl.enable(true);
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });

            this.getLocation();
        })
    }

    getLocation() {
        this.locationDocument = this.afs.collection('events').doc(this.eventId).collection('locations').doc(this.locationId);
        this.refLocation = this.locationDocument.valueChanges().subscribe((data: any) => {
            this.location = data;
        });
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
        // this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }

    // zoom profile picture image
    openImage(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: url
            }
        }).then(modal => {
            modal.present();
        });
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InteractivityPage } from './interactivity.page';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [InteractivityPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: InteractivityPage }
    ]),
    SharedModule
  ]
})
export class InteractivityModule { }

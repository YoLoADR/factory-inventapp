import { Component, OnInit } from '@angular/core';
import { MenuController, Events } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoInteractivityService } from 'src/app/providers/db/dao-interactivity.service';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { DaoQuizService } from 'src/app/providers/db/dao-quiz.service';
import { DaoAskQuestionService } from 'src/app/providers/db/dao-ask-queston.service';

@Component({
    selector: 'app-interactivity',
    templateUrl: './interactivity.page.html',
    styleUrls: ['./interactivity.page.scss'],
})
export class InteractivityPage implements OnInit {

    eventId: string = null;
    backBtn: boolean = true;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    moduleInteractivity = null;
    moduleSurvey = null;
    moduleQuiz = null;
    moduleTraining = null;
    moduleAskQuestion = null;
    moduleWordCloud = null;
    allowButtonsHeader: boolean = false;

    constructor(
        private events: Events,
        private route: ActivatedRoute,
        private router: Router,
        public global: GlobalService,
        private menuCtrl: MenuController,
        private daoInteractivity: DaoInteractivityService,
        private daoQuizService: DaoQuizService,
        private daoAskQuestionService: DaoAskQuestionService,
        private dbAnalytics: DaoAnalyticsService

    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            localStorage.setItem('eventId', this.eventId);
            this.loadColors();
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.menuBadge = this.global.notificationBadge;
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.allowButtonsHeader = true;
        }
        this.start();
    }

    start() {
        this.getQuiz();
        this.getAskQuestion();
        Promise.all([
            this.getInteractivity(),
            this.getSurvey(),
            this.getTraining(),
            this.getWordCloud()
        ])
            .then((response) => {
                this.moduleInteractivity = response[0];
                this.moduleSurvey = response[1][0];
                this.moduleTraining = response[2][0];
                this.moduleWordCloud = response[3][0];
            })
            .catch((e) => {
                console.log(e)
            });
    }

    getInteractivity() {
        return new Promise((resolve) => {
            this.daoInteractivity.getInteractivityModule(this.eventId, (moduleInteractivity) => {
                resolve(moduleInteractivity);
            })
        })
    }

    getSurvey() {
        return new Promise((resolve) => {
            this.daoInteractivity.getSurveyModule(this.eventId, (moduleSurvey) => {
                resolve(moduleSurvey);
            });
        })
    }

    /**
     * Get quiz module
     */
    getQuiz() {
        this.daoQuizService.getQuizModule(this.eventId).subscribe((modulesQuiz) => {
            if (modulesQuiz.length > 0) {
                this.moduleQuiz = modulesQuiz[0];
            }
        });
    }

    getTraining() {
        return new Promise((resolve) => {
            this.daoInteractivity.getTrainingModule(this.eventId, (moduleTraining) => {
                resolve(moduleTraining)
            })
        })
    }

    getAskQuestion() {
        this.daoAskQuestionService.getAskQuestionModule(this.eventId).subscribe((modulesAskQuestion) => {
            if (modulesAskQuestion.length > 0) {
                this.moduleAskQuestion = modulesAskQuestion[0];
            }
        })
    }

    getWordCloud() {
        return new Promise((resolve) => {
            this.daoInteractivity.getWordCloudModule(this.eventId, (moduleWordCloud) => {
                resolve(moduleWordCloud)
            })
        })
    }

    openPage(url) {
        if (this.moduleInteractivity.answerOffline || this.global.userLoaded) {
            this.router.navigate([url]);
        } else {
            this.global.userLogged();
        }
    }

    ionViewWillLeave() {
        // save another module view access
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleInteractivity['uid']);
    }
}

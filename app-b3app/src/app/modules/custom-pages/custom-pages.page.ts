import { Component, OnInit, NgZone } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { MenuController, Events } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { ActivatedRoute } from '@angular/router';
import { DaoCustomPageService } from 'src/app/providers/db/dao-custom-page.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-custom-pages',
    templateUrl: './custom-pages.page.html',
    styleUrls: ['./custom-pages.page.scss'],
})
export class CustomPagesPage implements OnInit {
    public module = null
    backBtn: boolean = true;

    menu_color: string = null;

    eventId: string = null;
    moduleId: string = null;

    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;

    menuBadge: number = 0;
    pages = []
    allowButtonsHeader: boolean = false;
    loader: boolean = true;
    constructor(
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private route: ActivatedRoute,
        private router: Router,
        private zone: NgZone,
        private dbCustomPage: DaoCustomPageService
    ) {
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];

            // enable menu
            this.menuCtrl.enable(true);

            // loads the colors of the event
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });


            // pick up notification module information
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
        })
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        // computes one access in the analytics
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);

        this.loadModule();
        this.loadPages();
    }

    loadModule() {
        this.dbCustomPage.getModule(this.moduleId, (module) => {
            this.module = module;
        });
    }

    loadPages() {
        this.dbCustomPage.getPages(this.moduleId, (pages) => {
            this.zone.run(() => {
                this.pages = pages;
                this.loader = false;
            });
            // for(let page of pages) {
            //   this.pages.push(page);
            // }

        })
    }

    openPage(page) {
        this.router.navigate([`/event/${this.eventId}/page-booth/${page.moduleId}/pageId/${page.uid}`])

    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

}

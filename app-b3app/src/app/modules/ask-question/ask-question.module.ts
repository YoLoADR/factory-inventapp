import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AskQuestionPage } from './ask-question.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { InteractivityModule } from 'src/app/content/reusable_components/interactivity/interactivity.module';

@NgModule({
    declarations: [AskQuestionPage],
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: AskQuestionPage }
        ]),
        SharedModule,
        InteractivityModule
    ]
})
export class AskQuestionModule { }

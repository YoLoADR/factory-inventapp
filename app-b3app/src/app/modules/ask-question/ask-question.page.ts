import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { DaoAskQuestionService } from 'src/app/providers/db/dao-ask-queston.service';
import { environment } from 'src/environments/environment';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { Subscription } from 'rxjs';
import { AskQuestion } from 'src/app/models/ask-question';
import { AppState } from 'src/app/shared/reducers';
import { Store } from '@ngrx/store';
import { GetQuestions } from 'src/app/shared/actions/interactivity.actions';

@Component({
    selector: 'app-ask-question-page',
    templateUrl: './ask-question.page.html',
    styleUrls: ['./ask-question.page.scss'],
})
export class AskQuestionPage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];

    eventId: string = null;
    moduleId: string = null;
    itemId: string = null;

    userId: string = null;

    questionModule: any = null;
    questions: AskQuestion[] = [];

    menu_color: string = null;
    menu_text_color: string = null;
    todayDate = new Date();
    allowButtonsHeader: boolean = false;

    userLanguage: string;

    constructor(
        private store: Store<AppState>,
        private route: ActivatedRoute,
        private router: Router,
        private daoAskQuestion: DaoAskQuestionService,
        private dbAnalytics: DaoAnalyticsService,
        private SGlobal: GlobalService,
        private SUtility: UtilityService
    ) { }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            this.itemId = params.itemId;
            this.userId = this.SGlobal.userId;
            localStorage.setItem('eventId', this.eventId);

            this.loadColors();

            if (this.router.url == this.SGlobal.eventHomePage) {
                this.allowButtonsHeader = true;
            }

            this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;

            this.getItem();
        })
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.store.dispatch(new GetQuestions({
            questionModule: null,
            questions: []
        }))
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Getting item
     */
    getItem() {
        this.subscriptions.push(this.daoAskQuestion.getAskItem(this.moduleId, this.itemId).subscribe((data) => {
            if (data) {
                this.questionModule = data;
                this.getQuestions();
            }
        }))
    }

    /**
     * Get questions
     */
    getQuestions() {
        this.subscriptions.push(this.daoAskQuestion.getQuestionsGeneral(this.moduleId, this.itemId, this.userId).subscribe((questions) => {
            if (questions !== null) {
                questions = questions.sort((a, b) => {
                    if (a.totalVotes < b.totalVotes) {
                        return 1;
                    }
                    if (a.totalVotes > b.totalVotes) {
                        return -1;
                    }
                    return 0;
                });

                this.questions = [];
                this.questions = questions;
            } else {
                this.questions = null;
            }

            this.store.dispatch(new GetQuestions({
                questionModule: this.questionModule,
                questions: this.questions
            }))
        }))
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
    }

    /**
     * On leaving component
     */
    ionViewWillLeave() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SurveyPage } from './survey.page';
import { RatingModule } from 'ng-starrating';
import { DatePickerModule } from 'ionic4-date-picker';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { ComponentsModulesModule } from '../components-modules.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        RouterModule.forChild([
            { path: '', component: SurveyPage }
        ]),
        RatingModule,
        DatePickerModule,
        SharedModule,
        ComponentsModulesModule
    ]
})
export class SurveyModule { }

import { Component, OnInit, NgZone } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { MenuController, Events, ModalController } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoInfoboothService } from 'src/app/providers/db/dao-infobooth.service';
import { LightboxImgComponent } from 'src/app/components/lightbox-img/lightbox-img.component';

@Component({
    selector: 'app-page-booth',
    templateUrl: './page-booth.page.html',
    styleUrls: ['./page-booth.page.scss'],
})

export class PageBoothPage implements OnInit {
    public module = null

    backBtn: boolean = true;

    menu_color: string = null;

    eventId: string = null;
    moduleId: string = null;
    pageId: string = null

    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    allowButtonsHeader: boolean = false;
    title: any
    htmlContent: any
    loader: boolean = true;
    user: any;

    constructor(
        public global: GlobalService,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private route: ActivatedRoute,
        private dbInfobooth: DaoInfoboothService,
        private router: Router,
        private zone: NgZone,
        private modalCtrl: ModalController
    ) {

        this.user = this.global.user;
        console.log("User: ", this.user);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.pageId = params.pageId;
            // this.pageId = this.route.snapshot.params['pageId'];
            localStorage.setItem('eventId', this.eventId);

            // enable menu
            this.menuCtrl.enable(true);

            // loads the colors of the event
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });

            // pick up notification module information
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
        })
    }

    ngOnInit() {
        this.title = {
            PtBR: '',
            EnUS: '',
            EsES: '',
            FrFR: '',
            DeDE: ''
        }

        this.htmlContent = {
            PtBR: '',
            EnUS: '',
            EsES: '',
            FrFR: '',
            DeDE: ''
        }

        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        // computes one access in the analytics
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);

        this.loadModule();
        // loads the data from the infobooth page
        this.loadPage();
        console.log("Module id: ", this.moduleId, " - ", this.pageId);
    }

    loadModule() {
        this.dbInfobooth.getModule(this.moduleId, (module) => {
            this.module = module
        });
    }

    loadPage() {
        this.dbInfobooth.getPage(this.moduleId, this.pageId, (page) => {
            this.zone.run(() => {
                if (page !== undefined) {
                    this.title = page.title;
                    this.htmlContent = page.htmlContent;
                }
                this.loader = false;
            });
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    // zoom profile picture image
    openImage(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: url
            }
        }).then(modal => {
            modal.present();
        });

    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PageBoothPage } from './page-booth.page';
import { PinchZoomModule } from 'ngx-pinch-zoom';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [PageBoothPage],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PageBoothPage }
    ]),
    PinchZoomModule,
    SharedModule
  ]
})
export class PageBoothModule { }

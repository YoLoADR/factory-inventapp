import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WatchBroadcastSessionPage } from './watch-broadcast-session.page';

const routes: Routes = [
  {
    path: '',
    component: WatchBroadcastSessionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WatchBroadcastSessionPage]
})
export class WatchBroadcastSessionModule {}

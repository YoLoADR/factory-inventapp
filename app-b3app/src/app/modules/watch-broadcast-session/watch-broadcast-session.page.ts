import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, Events } from '@ionic/angular';
import { DaoScheduleService } from 'src/app/providers/db/dao-schedule.service';
import { ActivatedRoute } from '@angular/router'
import { HttpErrorResponse } from '@angular/common/http'
import { throwError } from 'rxjs';
import { GlobalService } from 'src/app/shared/services';
import { BambuserService } from 'src/app/providers/bambuser/bambuser.service';
// import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-watch-broadcast-session',
    templateUrl: './watch-broadcast-session.page.html',
    styleUrls: ['./watch-broadcast-session.page.scss'],
})
export class WatchBroadcastSessionPage implements OnInit {

    // parameters
    eventId: string = null;
    moduleId: string = null;
    sessionId: string = null;
    allow_broadcast: boolean = null;

    isFetching = false;
    errorMessage = '';
    mayShowSpinner = true;
    refresher: any;
    broadcasts = [];
    moment: any;

    resourceUri = null;
    autoplay = true;
    showCloseButton = false;

    playerNative = true;
    playerJavascript = true;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;

    //javascript player
    @ViewChild('playerJavascript', { static: false }) playerEl: ElementRef;
    playerLog = [];

    //native player
    player: any;

    // Handle API errors
    handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };

    constructor(
        public navCtrl: NavController,
        public platform: Platform,
        private zone: NgZone,
        private dbSchedule: DaoScheduleService,
        private route: ActivatedRoute,
        public global: GlobalService,
        private bambuser: BambuserService,
        private events: Events,
        // private translateService: TranslateService
    ) {
        this.allow_broadcast = this.global.allow_broadcast;

        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            this.sessionId = params.sessionId;
        })
    }

    ngOnInit() {
        this.loadColors();

        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }

    ionViewDidEnter() {
        if (this.allow_broadcast) {
            this.reloadData();
        }
    }

    // loads the colors of the event.
    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    reloadData() {
        // TODO: support pagination / endless scroll
        this.errorMessage = '';
        this.isFetching = true;

        this.dbSchedule.getSessionModule(this.moduleId, this.sessionId).subscribe((session) => {
            let broadcastId = null;

            if (session.broadcastId) {
                broadcastId = session.broadcastId;

                this.bambuser.getBroadcast(broadcastId).subscribe(response => {
                    this.isFetching = false;
                    let broadcast = response;
                    this.playBroadcast(broadcast);
                }, err => {
                    this.errorMessage = err;
                    this.isFetching = false;
                })
            }

        })

    }

    playBroadcast(broadcast) {
        if (broadcast.type === 'live' && window['bambuser'] && window['bambuser']['player']) {
            this.resourceUri = broadcast.resourceUri;
            this.autoplay = true;
            this.javascriptPlayer();

        } else {
            this.resourceUri = broadcast.resourceUri;
            this.autoplay = true;
            this.showCloseButton = true;
            this.javascriptPlayer();
        }
    }

    javascriptPlayer() {
        this.playerNative = false;

        // BambuserPlayer is loaded to window in index.html
        const BambuserPlayer: any = window['BambuserPlayer'];

        let resourceUri = this.resourceUri;
        if (!resourceUri) {
            // ...or fall back to using a static resourceUri, for demo purposes
            resourceUri = 'https://cdn.bambuser.net/broadcasts/0b9860dd-359a-67c4-51d9-d87402770319?da_signature_method=HMAC-SHA256&da_id=9e1b1e83-657d-7c83-b8e7-0b782ac9543a&da_timestamp=1482921565&da_static=1&da_ttl=0&da_signature=cacf92c6737bb60beb1ee1320ad85c0ae48b91f9c1fbcb3032f54d5cfedc7afe';
        }

        // https://bambuser.com/docs/playback/web-player/#javascript-api
        const player = BambuserPlayer.create(this.playerEl.nativeElement, resourceUri);
        player.controls = true;

        const log = str => {
            this.zone.run(() => {
                // this.playerLog.unshift(`${player.currentTime} ${player.duration} ${str}`);
            });
        }

        // Make player available in console, for debugging purposes
        window['player'] = player;

        // Log all player events as they occur, for debugging purposes
        [
            'canplay',
            'durationchange',
            'ended',
            'error',
            'loadedmetadata',
            'pause',
            'play',
            'playing',
            'progress',
            'seeked',
            'seeking',
            'timeupdate',
            'volumechange',
            'waiting'
        ].map(eventName => player.addEventListener(eventName, e => log(eventName)));

        if (this.autoplay) {
            player.play();
        }

        if (this.showCloseButton) {
            this.showCloseButton = true;
        }

        this.openFullscreen();
    }


    openFullscreen() {
        if (this.playerEl.nativeElement.requestFullscreen) {
            this.playerEl.nativeElement.requestFullscreen();
        } else if (this.playerEl.nativeElement.mozRequestFullScreen) { /* Firefox */
            this.playerEl.nativeElement.mozRequestFullScreen();
        } else if (this.playerEl.nativeElement.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            this.playerEl.nativeElement.webkitRequestFullscreen();
        } else if (this.playerEl.nativeElement.msRequestFullscreen) { /* IE/Edge */
            this.playerEl.nativeElement.msRequestFullscreen();
        }
    }

    closePlayer() {
        // Relevant only if player is opened as a modal
        this.navCtrl.pop();
    }

    ionViewWillLeave() {
    }
}

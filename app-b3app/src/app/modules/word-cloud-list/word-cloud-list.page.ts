import { Component, OnInit, NgZone } from '@angular/core';
import { MenuController, Events } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { ActivatedRoute, Router } from '@angular/router';
import { DaoWordCloudService } from 'src/app/providers/db/dao-word-cloud.service';
import { WordCloud } from 'src/app/models/wordCloud';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';

@Component({
    selector: 'app-word-cloud-list',
    templateUrl: './word-cloud-list.page.html',
    styleUrls: ['./word-cloud-list.page.scss'],
})
export class WordCloudListPage implements OnInit {

    eventId: string = null;
    moduleId: string = null;
    userId: string = null;

    allowButtonsHeader: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    loader: boolean = true;
    moduleName: string = null;

    clouds: Array<WordCloud> = [];
    module;
    constructor(private route: ActivatedRoute,
        private router: Router,
        public global: GlobalService,
        private menuCtrl: MenuController,
        private events: Events,
        private daoWordCloud: DaoWordCloudService,
        private zone: NgZone,
        private dbAnalytics: DaoAnalyticsService
    ) {

        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
        })
    }

    ngOnInit() {
        this.daoWordCloud.getModule(this.moduleId, (module) => {
            this.module = module;
            this.moduleName = module.name;
        })

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }

        this.getWordClouds();
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    getWordClouds() {
        this.daoWordCloud.getClouds(this.moduleId, (clouds) => {
            this.clouds = [];
            this.clouds = clouds;
        })
    }

    openItem(item) {
        this.router.navigate([`/event/${this.eventId}/word-cloud/${this.moduleId}/${item.uid}`]);
    }

    ionViewWillLeave() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }
}

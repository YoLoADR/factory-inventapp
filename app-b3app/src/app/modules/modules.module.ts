import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
    { path: '', redirectTo: 'info', pathMatch: 'full' },
    {
        path: 'info',
        loadChildren: './event/event.module#EventModule'
    },
    {
        path: 'widget/:moduleId',
        loadChildren: './widgets/widgets.module#WidgetsModule'
    },
    {
        path: 'attendees/:moduleId',
        loadChildren: './attendees/attendees.module#AttendeesModule'
    },
    {
        path: 'speakers/:moduleId',
        loadChildren: './speakers/speakers.module#SpeakersModule'
    },
    {
        path: 'schedule/:moduleId',
        loadChildren: './schedule/schedule.module#ScheduleModule'
    },
    {
        path: 'personal-schedule/:moduleId',
        loadChildren:
            './schedule-personal/schedule-personal.module#SchedulePersonalModule'
    },
    {
        path: 'schedule-detail/:moduleId/:sessionId',
        loadChildren:
            './schedule-detail/schedule-detail.module#ScheduleDetailModule'
    },
    {
        path: 'list-broadcasts/:moduleId',
        loadChildren:
            './list-broadcasts/list-broadcasts.module#ListBroadcastsModule'
    },
    {
        path: 'watch-broadcast/:moduleId/:broadcastId',
        loadChildren:
            './watch-broadcast/watch-broadcast.module#WatchBroadcastModule'
    },
    {
        path: 'live-broadcast/:moduleId',
        loadChildren:
            './live-broadcast/live-broadcast.module#LiveBroadcastModule'
    },
    {
        path: 'live-broadcast-session/:moduleId/:sessionId',
        loadChildren:
            './live-broadcast-session/live-broadcast-session.module#LiveBroadcastSessionModule'
    },
    {
        path: 'watch-broadcast-session/:moduleId/:sessionId',
        loadChildren:
            './watch-broadcast-session/watch-broadcast-session.module#WatchBroadcastSessionModule'
    },
    {
        path: 'location/:locationId',
        loadChildren: './location/location.module#LocationModule'
    },
    {
        path: 'profile/:moduleId/:type/:userId',
        loadChildren: './profile/profile.module#ProfileModule'
    },
    {
        path: 'personal-page/:moduleId/:type/:userId',
        loadChildren: './personal-page/personal-page.module#PersonalPageModule'
    },
    {
        path: 'edit-profile/:moduleId/:type/:userId',
        loadChildren: './profile-edit/profile-edit.module#ProfileEditModule'
    },
    {
        path: 'gallery/:moduleId',
        loadChildren: './gallery/gallery.module#GalleryModule'
    },
    {
        path: 'gallery/:moduleId/folder/:folderId',
        loadChildren:
            './gallery-folder/gallery-folder.module#GalleryFolderModule'
    },
    {
        path: 'documents/:moduleId',
        loadChildren:
            '../content/pages/documents-module/documents-module.module#DocumentsModulePageModule'
    },
    {
        path: 'documents/:moduleId/folder/:folderId',
        loadChildren:
            './documents-folder/documents-folder.module#DocumentsFolderModule'
    },
    {
        path: 'checkin/:moduleId',
        loadChildren: './checkin/checkin.module#CheckinModule'
    },
    {
        path: 'checkin-detail/:moduleId/:checkinId',
        loadChildren:
            './checkin-detail/checkin-detail.module#CheckinDetailModule'
    },
    {
        path: 'surveys-list/:moduleId',
        loadChildren:
            './surveys-list-page/surveys-list-page.module#SurveysListModule'
    },
    {
        path: 'survey/:moduleId/:surveyId',
        loadChildren: './survey/survey.module#SurveyModule'
    },
    {
        path: 'quiz-list/:moduleId',
        loadChildren: './quiz-list-page/quiz-list-page.module#QuizListModule'
    },
    {
        path: 'quiz/:moduleId/:quizId',
        loadChildren: './quiz/quiz.module#QuizModule'
    },
    {
        path: 'training-list/:moduleId',
        loadChildren: './training-list/training-list.module#TrainingListModule'
    },
    {
        path: 'training-list-session/:moduleId/:scheduleModuleId/:sessionId',
        loadChildren:
            './training-list-session/training-list-session.module#TrainingListSessionModule'
    },
    {
        path: 'training/:moduleId/:trainingId',
        loadChildren: './training/training.module#TrainingModule'
    },
    {
        path: 'ask-question-list/:moduleId',
        loadChildren:
            './ask-question-list/ask-question-list.module#AskQuestionListModule'
    },
    {
        path: 'ask-question/:moduleId/:itemId',
        loadChildren: './ask-question/ask-question.module#AskQuestionModule'
    },
    {
        path: 'word-cloud-list/:moduleId',
        loadChildren:
            './word-cloud-list/word-cloud-list.module#WordCloudListModule'
    },
    {
        path: 'word-cloud/:moduleId/:cloudId',
        loadChildren: './word-cloud/word-cloud.module#WordCloudModule'
    },
    {
        path: 'interactivity',
        loadChildren: './interactivity/interactivity.module#InteractivityModule'
    },
    {
        path: 'feed-news/:moduleId',
        loadChildren: './feed-news/feed-news.module#FeedNewsModule'
    },
    {
        path: 'chat-history',
        loadChildren:
            './chat/chat-history/chat-history.module#ChatHistoryModule'
    },
    { path: 'chat/:chatId', loadChildren: './chat/chat.module#ChatModule' },
    {
        path: 'chat/group/:groupId',
        loadChildren: './chat/chat.module#ChatModule'
    },
    {
        path: 'maps/:moduleId',
        loadChildren: './maps-list/maps-list.module#MapsListModule'
    },
    {
        path: 'map/:moduleId/:mapId',
        loadChildren: './map-detail/map-detail.module#MapDetailModule'
    },
    {
        path: 'ranking/:moduleId',
        loadChildren: './ranking/ranking.module#RankingModule'
    },
    {
        path: 'self-checkin/:moduleId',
        loadChildren: './self-checkin/self-checkin.module#SelfCheckinModule'
    },
    {
        path: 'page-booth/:moduleId/pageId/:pageId',
        loadChildren: './page-booth/page-booth.module#PageBoothModule'
    },
    {
        path: 'custom-pages/:moduleId',
        loadChildren: './custom-pages/custom-pages.module#CustomPagesModule'
    },
    {
        path: 'gaming/:moduleId',
        loadChildren: './gamification/gamification.module#GamificationModule'
    },
    {
        path: 'list-group-discussions/:moduleId',
        loadChildren:
            './list-group-discussions/list-group-discussions.module#ListGroupDiscussionsPageModule'
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    declarations: [],
    providers: []
})
export class ModulesModule {}

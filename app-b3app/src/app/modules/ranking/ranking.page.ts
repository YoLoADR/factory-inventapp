import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Events, MenuController, IonInfiniteScroll, IonVirtualScroll } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { CeuAttendee } from 'src/app/models/ceu-attendee';

@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.page.html',
    styleUrls: ['./ranking.page.scss'],
})

export class RankingPage implements OnInit {
    public module = null

    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonVirtualScroll, { static: false }) virtualScroll: IonVirtualScroll;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    eventId: string = null;
    moduleId: string = null;
    refModule: any = null;
    refAttendees: any = null;
    moduleDoc: AngularFirestoreDocument<any> = null;
    attendeeCollection: AngularFirestoreCollection<any> = null;
    allowButtonsHeader: boolean = false;
    refAllAttendees: any = null;
    allAttendeesCollection: AngularFirestoreCollection<any> = null;
    allAttendeesList: Array<any> = [];
    lastAttendees;
    attendees: Array<CeuAttendee> = [];
    loader: boolean = true;
    myUser: CeuAttendee = null;
    searchOpen: boolean = false;
    constructor(
        private route: ActivatedRoute,
        public global: GlobalService,
        private afs: AngularFirestore,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private router: Router,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];
            this.loadColors();
            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });
            this.menuBadge = this.global.notificationBadge;
            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });
            this.moduleDoc = this.afs.collection('modules').doc(this.moduleId);
            this.refModule = this.moduleDoc.valueChanges().subscribe((data: any) => {
                this.module = data;
                this.start();
            });
            this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
    }

    // get attendees and points
    start() {
        this.allAttendeesCollection = this.afs.collection('events').doc(this.eventId).collection<CeuAttendee>('attendees', ref => ref.orderBy('queryName', 'asc'));
        this.refAllAttendees = this.allAttendeesCollection.valueChanges().subscribe((allAttendees) => {
            this.allAttendeesList = [];
            allAttendees = allAttendees.sort(function (a, b) {
                if (a['points'] < b['points']) { return 1; }
                if (a['points'] > b['points']) { return -1; }
                return 0;
            });
            for (let i = 0; i < allAttendees.length; i++) {
                let attendee = allAttendees[i];
                let position = i + 1;
                attendee.position = position;
                if (attendee.uid == this.global.userId) {
                    this.myUser = attendee;
                }
                this.allAttendeesList.push(attendee);
            }
        });
        this.getFirstAttendees();
    }

    getFirstAttendees() {
        this.attendeeCollection = this.afs
            .collection('events')
            .doc(this.eventId)
            .collection<CeuAttendee>('attendees', ref => ref.orderBy('points', 'desc').limit(100));

        this.refAttendees = this.attendeeCollection.valueChanges().subscribe((data: any) => {
            data = data.sort(function (a, b) {
                if (a['points'] < b['points']) { return 1; }
                if (a['points'] > b['points']) { return -1; }
                return 0;
            });
            if (data.length > 0) {
                this.lastAttendees = data[data.length - 1];
            }
            this.attendees = [];
            for (let i = 0; i < data.length; i++) {
                let attendee = data[i];
                let position = i + 1;
                attendee.position = position;
                this.attendees.push(attendee);
            }
            this.loader = false;
        });
    }

    getMoreAttendees(event) {
        this.attendeeCollection = this.afs
            .collection('events')
            .doc(this.eventId)
            .collection<CeuAttendee>('attendees', ref => ref.orderBy('points', 'desc').startAfter(this.lastAttendees.name).limit(100));

        this.refAttendees = this.attendeeCollection.valueChanges().subscribe((data: any) => {
            data = data.sort(function (a, b) {
                if (a['points'] < b['points']) { return 1; }
                if (a['points'] > b['points']) { return -1; }
                return 0;
            });
            if (data.length > 0) {
                this.lastAttendees = data[data.length - 1];
            }

            for (let i = 0; i < data.length; i++) {
                let attendee = data[i];
                let position = i + 1;
                attendee.position = position;
                this.attendees.push(attendee);
            }
            this.loader = false;
            if (data.length > 0) {
                this.virtualScroll.checkEnd();
                event.target.complete();
            } else {
                event.target.disabled = true;
            }
        });
    }

    searchBar(ev) {
        if (ev.target.value.length >= 1) {
            let value = ev.target.value.toLowerCase();
            this.attendees = [];
            this.allAttendeesList.filter(item => {
                if (item.name.toLowerCase().includes(value)) {
                    this.attendees.push(item);
                }
            })
        } else {
            this.getFirstAttendees();
        }
    }

}

import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Events, MenuController, ModalController } from '@ionic/angular';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { LightboxImgComponent } from 'src/app/components/lightbox-img/lightbox-img.component';

@Component({
    selector: 'app-maps-list',
    templateUrl: './maps-list.page.html',
    styleUrls: ['./maps-list.page.scss'],
})
export class MapsListPage implements OnInit {
    public module = null
    loader: boolean = true;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    menuBadge: number = 0;
    eventId: string = null;
    moduleId: string = null;
    refModule: any = null;
    moduleDoc: AngularFirestoreDocument<any> = null;
    refMaps: any = null;
    mapsCollection: AngularFirestoreCollection<any> = null;
    mapsList: Array<any> = [];
    typeOrder: string = null;
    backBtn: boolean = true;
    allowButtonsHeader: boolean = false;
    searchOpen: boolean = false;
    constructor(
        private route: ActivatedRoute,
        public global: GlobalService,
        private afs: AngularFirestore,
        private events: Events,
        private menuCtrl: MenuController,
        private dbAnalytics: DaoAnalyticsService,
        private router: Router,
        private modalCtrl: ModalController,
        private zone: NgZone
    ) {
        this.menuCtrl.enable(true);
        this.route.params.subscribe((params) => {
            this.eventId = params.eventId;
            // this.eventId = this.route.parent.params['_value']['eventId'];
            localStorage.setItem('eventId', this.eventId);
            this.moduleId = params.moduleId;
            // this.moduleId = this.route.snapshot.params['moduleId'];

            this.loadColors();

            this.events.subscribe('loadColors', () => {
                this.loadColors();
            });

            this.menuBadge = this.global.notificationBadge;

            this.events.subscribe('menuBadge', () => {
                this.zone.run(() => {
                    this.menuBadge = this.global.notificationBadge;
                })
            });

            this.moduleDoc = this.afs.collection('modules').doc(this.moduleId);
            this.refModule = this.moduleDoc.valueChanges().subscribe((data: any) => {
                this.module = data;
                this.typeOrder = data.orderMaps;
                this.start();
            });

            // save another module view access
            this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
        })
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        if (this.global.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.global.eventHomePage) {
            this.zone.run(() => {
                this.allowButtonsHeader = true;
            })
        }
    }

    // list maps
    start() {
        switch (this.typeOrder) {
            case 'asc': //a-z
                this.mapsCollection = this.afs
                    .collection('modules')
                    .doc(this.moduleId)
                    .collection('maps', ref => ref.orderBy('name', 'asc'));
                break;

            case 'desc': //z-a
                this.mapsCollection = this.afs
                    .collection('modules')
                    .doc(this.moduleId)
                    .collection('maps', ref => ref.orderBy('name', 'desc'));
                break;

            case 'oldest'://antiho-recente
                this.mapsCollection = this.afs
                    .collection('modules')
                    .doc(this.moduleId)
                    .collection('maps', ref => ref.orderBy('createdAt', 'asc'));
                break;

            case 'recent': //recente-antigo
                this.mapsCollection = this.afs
                    .collection('modules')
                    .doc(this.moduleId)
                    .collection('maps', ref => ref.orderBy('createdAt', 'desc'));
                break;
        }

        this.refMaps = this.mapsCollection.valueChanges().subscribe((data: any) => {
            this.mapsList = [];
            this.mapsList = data;
            this.loader = false;
        });
    }

    // zoom profile picture image
    zoomImg(url: string) {
        // this.photoViewer.show(url, '', { share: false });
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: url
            }
        }).then(modal => {
            modal.present();
        });
    }
}

import { Pipe, PipeTransform } from '@angular/core';
import { GroupDiscussion } from 'src/app/models/group-discussion';

@Pipe({
    name: 'filterGroupDiscussionsActive'
})
export class FilterGroupDiscussionsActivePipe implements PipeTransform {
    transform(gds: GroupDiscussion[], args: any[]): GroupDiscussion[] {
        return gds.filter((gd) =>
            args[0]
                ? gd.activeParticipants.includes(args[1])
                : !gd.activeParticipants.includes(args[1])
        );
    }
}

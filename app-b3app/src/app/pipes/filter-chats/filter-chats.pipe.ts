import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterChats'
})
export class FilterChatsPipe implements PipeTransform {
    transform(chats: any[], q: string): any {
        return chats.filter((chat) =>
            chat.lastMessage.send_from_user_name
                .toLowerCase()
                .includes(q.toLowerCase())
        );
    }
}

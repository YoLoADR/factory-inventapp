import { Pipe, PipeTransform } from '@angular/core';
import { DateTime } from 'luxon';

@Pipe({
    name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
    transform(timestamp: firebase.firestore.Timestamp): string {
        return DateTime.fromMillis(timestamp.toMillis()).toLocaleString(
            DateTime.DATETIME_SHORT
        );
    }
}

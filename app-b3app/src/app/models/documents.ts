export class Document {
    private _uid: string;
    private _name: string;
    private _url: string;
    private _type: string;
    private _icon: string;
    private _moduleId: string;

    constructor(name: string, type: string) {
        this._name = name;
        this._type = type;
        this._icon = this.checkType(type);
        this._url = null;
    }

    checkType(type: string): string {
        let icon = "";
        switch (type) {
            case 'doc':
            case 'docx':
            case 'docm':
            case 'DOC':
            case 'DOCX':
            case 'DOCM':
                icon = 'doc_ico.png';
                break;

            case 'xls':
            case 'xlt':
            case 'xls':
            case 'xml':
            case 'xlsx':
            case 'xlsm':
            case 'xlsb':
            case 'xltx':
            case 'xltm':
            case 'XLS':
            case 'XLT':
            case 'XLS':
            case 'XML':
            case 'XLSX':
            case 'XLSM':
            case 'XLSB':
            case 'XLTX':
            case 'XLTM':
                icon = 'excel_ico.png';
                break;

            case 'pdf':
            case 'PDF':
                icon = 'pdf_ico.png';
                break;

            case 'png':
            case 'PNG':
                icon = 'png_ico.png';
                break;

            case 'jpg':
            case 'jpeg':
            case 'JPG':
            case 'JPEG':
                icon = 'jpg_ico.png';
                break;

            case 'ppt':
            case 'pptx':
            case 'thmx':
            case 'ppsx':
            case 'pps':
            case 'ppsm':
            case 'PPT':
            case 'PPTX':
            case 'THMX':
            case 'PPSX':
            case 'PPS':
            case 'PPSM':
                icon = 'ppt_ico.png';
                break;

            case 'mp4':
            case 'wmv':
            case '3gp':
            case 'avi':
            case 'mp3':
            case 'wav':
            case 'MP4':
            case 'WMV':
            case '3GP':
            case 'AVI':
            case 'MP3':
            case 'WAV':
                icon = 'media_ico.png';
                break;

            default:
                icon = 'generic-file_ico.png';

        }

        return icon;
    }


    /**
    * Getter icon
    * @return {string}
    */
    public get icon(): string {
        return this._icon;
    }

    /**
     * Setter icon
     * @param {string} value
     */
    public set icon(value: string) {
        this._icon = value;
    }

    /**
     * Getter uid
     * @return {string}
     */
    public get uid(): string {
        return this._uid;
    }

    /**
     * Getter name
     * @return {string}
     */
    public get name(): string {
        return this._name;
    }

    /**
     * Getter url
     * @return {string}
     */
    public get url(): string {
        return this._url;
    }

    /**
     * Getter type
     * @return {string}
     */
    public get type(): string {
        return this._type;
    }

    /**
     * Setter uid
     * @param {string} value
     */
    public set uid(value: string) {
        this._uid = value;
    }

    /**
     * Setter name
     * @param {string} value
     */
    public set name(value: string) {
        this._name = value;
    }

    /**
     * Setter url
     * @param {string} value
     */
    public set url(value: string) {
        this._url = value;
    }

    /**
     * Setter type
     * @param {string} value
     */
    public set type(value: string) {
        this._type = value;
    }


    /**
     * Getter moduleId
     * @return {string}
     */
    public get moduleId(): string {
        return this._moduleId;
    }

    /**
     * Setter moduleId
     * @param {string} value
     */
    public set moduleId(value: string) {
        this._moduleId = value;
    }
}
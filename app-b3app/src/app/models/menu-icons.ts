import { PathIcons } from "./path/path-icons";

export class MenuIcons {
    public icon: string;
    public family: string;
}

export const icons: MenuIcons[] = [
    { icon: PathIcons.icon_settings, family: 'material-icons' },
    { icon: PathIcons.icon_gavel, family: 'material-icons' },
    { icon: PathIcons.icon_notications, family: 'material-icons' },
    { icon: PathIcons.icon_map, family: 'material-icons' },
    { icon: PathIcons.icon_view_agenda, family: 'material-icons' },
    { icon: PathIcons.icon_info, family: 'material-icons' },
    { icon: PathIcons.icon_people, family: 'material-icons' },
    { icon: PathIcons.icon_mic, family: 'material-icons' },
    { icon: PathIcons.icon_date_range, family: 'material-icons' },
    { icon: PathIcons.icon_calendar_today, family: 'material-icons' },
    { icon: PathIcons.icon_survey, family: 'material-icons' },
    { icon: PathIcons.icon_group, family: 'material-icons' },
    { icon: PathIcons.icon_document, family: 'material-icons' },
    { icon: PathIcons.icon_gallery, family: 'material-icons' },
    { icon: PathIcons.icon_checkin, family: 'material-icons' },
    { icon: PathIcons.icon_self_checkin, family: 'material-icons' },
    { icon: PathIcons.icon_event, family: 'material-icons' },
]
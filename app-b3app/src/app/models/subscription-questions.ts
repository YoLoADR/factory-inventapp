export class SubscriptionQuestions {
    uid: string;
    title: string;
    type: string;
    graphic: string;
    maxResponses: number;
}
export class ColorsChange {
    uid: string;
    headerColor: string; // cor do cabeçalho
    titleColor: string;
    menuTextColor: string; // cor do texto o menu
    contTextColor: string; // cor da fonte
    linkColor: string; //cor do link
    bgContentColor: string; // cor do card

    constructor() {
        this.uid = null;
        this.headerColor = "";
        this.titleColor = "";
        this.menuTextColor = "";
        this.contTextColor = "";
        this.linkColor = "";
        this.bgContentColor = "";
    }
}
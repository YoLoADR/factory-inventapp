export class FeedNewsComment {
    uid: string;
    text: string;
    userId: string;
    user_name: string;
    user_urlPhoto: string;
}
export class ChatHistory {
    msgDate: number;
    userName: string;
    userPicture: string;
    userStatus: string;
    lastMessage: string;
    badge: number;
    chatId: string;
    type: string;
    userId: string;
    senderUid: string;
}
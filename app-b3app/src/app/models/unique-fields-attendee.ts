export class UniqueFieldsAttendee {
    name: boolean;
    company: boolean;
    title: boolean;
    phone: boolean;
    site: boolean;
    linkedin: boolean;
    facebook: boolean;
    instagram: boolean;
    twitter: boolean;
    description: boolean;

    constructor() {
        this.name = false;
        this.company = false;
        this.title = false;
        this.phone = false;
        this.site = false;
        this.linkedin = false;
        this.facebook = false;
        this.instagram = false;
        this.twitter = false;
        this.description = false;
    }
}
export class Logo {
    private _uid: string;
    private _url: string;

    constructor() {
        this._url = "";
    }

    /**
     * Getter uid
     * @return {string}
     */
    public get uid(): string {
        return this._uid;
    }

    /**
     * Getter url
     * @return {string}
     */
    public get url(): string {
        return this._url;
    }

    /**
     * Setter uid
     * @param {string} value
     */
    public set uid(value: string) {
        this._uid = value;
    }

    /**
     * Setter url
     * @param {string} value
     */
    public set url(value: string) {
        this._url = value;
    }

}
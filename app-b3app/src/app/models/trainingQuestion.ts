import { trainingAnswer } from './trainingAnswer';

export class trainingQuestion {
    uid: string;
    title: string;
    explanation: string;
    type: string;
    graphic: string;
    answers: Array<trainingAnswer> = [];
    maxResponses: number;
    image: any;
}

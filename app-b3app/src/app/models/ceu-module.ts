export class CeuModule {
    private _uid: string;
    private _name: string;
    private _uidEvent: string;

    private _iconBackOffice: string;
    private _iconApp: string;

    private _viewBackOffice: string; //component
    private _viewApp: string; //component

    private _habilitedBackOffice: boolean;
    private _habilitedApp: boolean;

    private _page: any;

    private _order: number;

    private _type: number;

    private _access_group: string; // uid do grupo que temm permissão de acesso ao módulo
    private _typeVision : number;



    constructor(uid: string, name: string, icon: string, view: string, uidEvent: string, access_group: string, type:number, typeVision: number) {
        this._uid = uid;
        this._name = name;
        this._uidEvent = uidEvent;
        this._iconApp = icon;
        this._viewApp = view;
        this._type = type;
        this._access_group = null;
        this._typeVision = null;
        
        if(typeof access_group !== 'undefined'){
            this._access_group = access_group;
        }

        if(typeof typeVision !== 'undefined'){
            this._typeVision = typeVision;
        }
    }
    

    /**
     * Getter type
     * @return {number}
     */
	public get type(): number {
		return this._type;
	}

    /**
     * Setter type
     * @param {number} value
     */
	public set type(value: number) {
		this._type = value;
	}
    

    /**
     * Getter uid
     * @return {string}
     */
    public get uid(): string {
        return this._uid;
    }

    /**
     * Getter name
     * @return {string}
     */
    public get name(): string {
        return this._name;
    }

    /**
     * Getter uidEvent
     * @return {string}
     */
    public get uidEvent(): string {
        return this._uidEvent;
    }

    /**
     * Getter iconBackOffice
     * @return {string}
     */
    public get iconBackOffice(): string {
        return this._iconBackOffice;
    }

    /**
     * Getter iconApp
     * @return {string}
     */
    public get iconApp(): string {
        return this._iconApp;
    }

    /**
     * Getter viewBackOffice
     * @return {string}
     */
    public get viewBackOffice(): string {
        return this._viewBackOffice;
    }

    /**
     * Getter viewApp
     * @return {string}
     */
    public get viewApp(): string {
        return this._viewApp;
    }

    /**
     * Getter habilitedBackOffice
     * @return {boolean}
     */
    public get habilitedBackOffice(): boolean {
        return this._habilitedBackOffice;
    }

    /**
     * Getter habilitedApp
     * @return {boolean}
     */
    public get habilitedApp(): boolean {
        return this._habilitedApp;
    }

    /**
     * Setter uid
     * @param {string} value
     */
    public set uid(value: string) {
        this._uid = value;
    }

    /**
     * Setter name
     * @param {string} value
     */
    public set name(value: string) {
        this._name = value;
    }

    /**
     * Setter uidEvent
     * @param {string} value
     */
    public set uidEvent(value: string) {
        this._uidEvent = value;
    }

    /**
     * Setter iconBackOffice
     * @param {string} value
     */
    public set iconBackOffice(value: string) {
        this._iconBackOffice = value;
    }

    /**
     * Setter iconApp
     * @param {string} value
     */
    public set iconApp(value: string) {
        this._iconApp = value;
    }

    /**
     * Setter viewBackOffice
     * @param {string} value
     */
    public set viewBackOffice(value: string) {
        this._viewBackOffice = value;
    }

    /**
     * Setter viewApp
     * @param {string} value
     */
    public set viewApp(value: string) {
        this._viewApp = value;
    }

    /**
     * Setter habilitedBackOffice
     * @param {boolean} value
     */
    public set habilitedBackOffice(value: boolean) {
        this._habilitedBackOffice = value;
    }

    /**
     * Setter habilitedApp
     * @param {boolean} value
     */
    public set habilitedApp(value: boolean) {
        this._habilitedApp = value;
    }

    /**
     * Getter page
     * @return {any}
     */
	public get page(): any {
		return this._page;
	}

    /**
     * Setter page
     * @param {any} value
     */
	public set page(value: any) {
		this._page = value;
    }
    

    /**
     * Getter order
     * @return {number}
     */
	public get order(): number {
		return this._order;
	}

    /**
     * Setter order
     * @param {number} value
     */
	public set order(value: number) {
		this._order = value;
    }
    
     /**
     * Getter access_group
     * @return {string}
     */
	public get access_group(): string {
		return this._access_group;
	}

    /**
     * Setter access_group
     * @param {string} value
     */
	public set access_group(value: string) {
		this._access_group = value;
	}
    


    /**
     * Getter typeVision
     * @return {number}
     */
	public get typeVision(): number {
		return this._typeVision;
	}

    /**
     * Setter typeVision
     * @param {number} value
     */
	public set typeVision(value: number) {
		this._typeVision = value;
	}
}
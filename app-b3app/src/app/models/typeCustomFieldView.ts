/**OPÇÕES DE VISUALIZAÇÕES DO CUSTOMFIELD
 * 
 */

export enum TypeCustomFieldView{
    GLOBAL = "0",
    PROFILE = "1",
    ADMINISTRATOR = "2"
}
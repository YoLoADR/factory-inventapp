export enum TypeUser {
    SUPERGOD = 0,
    GOD = 1,
    CLIENT = 2,
    EMPLOYEE = 3,
    SPEAKER = 4,
    ATTENDEE = 5
}


export class AskQuestion {
    uid: string;
    userId: string;
    question: string;
    visibility: boolean;
    createdAt: number;

    constructor(){
        this.uid = null;
        this.userId = null;
        this.question = null;
        this.visibility = false;
        this.createdAt = null;
    }


}
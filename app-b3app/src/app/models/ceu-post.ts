/*Post nnews feed */
import { DateTime } from 'luxon';


export class Post {
    uid: string
    moduleId: string
    eventId: string

    img: any = {} // image url stores in firebase storage. name and url
    likes: any = {}
    comments: any = {}

    description: string
    date: number //date of creation
    creator: any //creator of the post. (Can be client, god, supergod or participant)
    fixedTop: number //defines whether the post should be at the top of the feed. (0 for true and 1 for false)
    active: boolean

    constructor(uid: string, moduleId: string, eventId: string, img: any, description: string, creator: any, fixedTop: number, active: boolean) {
        this.uid = uid
        this.moduleId = moduleId
        this.eventId = eventId
        
        this.img = img

        this.description = description
        this.creator = creator
        this.fixedTop = fixedTop
        this.active = active

        // create date of 
        this.date = DateTime.local().valueOf() / 1000;

        this.likes = {}
        this.comments = {}
    }

}


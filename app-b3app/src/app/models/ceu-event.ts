import { Session } from './ceu-session';
import { EventFieldsVisibility } from './event-fields-visibility';

export class CeuEvent {
    uid: string
    name: string
    description: Object;
    site: string
    subdomain: any;
    language: string;
    startDate: any
    endDate: any
    timeZone: number
    timeFormat: number
    locationName: string
    attendees: Map<string, boolean>
    sessions: Map<string, Session>
    speakers: Map<string, boolean>
    logo: any;
    banner: any;
    placeName: string; // nome do local do evento.
    placeAddress: string; //Endereço do local
    url: string;
    numberAttendee: number;
    emailSupport: string;
    descriptionGroup: string;
    shortcode: string;
    visibility: boolean;
    homePage: string;
    required_edit_profile: boolean;
    allow_chat: boolean;
    allow_visio: boolean;
    allow_visio_for_2: boolean;
    eventFields: EventFieldsVisibility;

    constructor() {
        this.uid = null;
        this.name = null;
        this.description = null;
        this.site = null;
        this.subdomain = null;
        this.language = null;
        this.startDate = null;
        this.endDate = null;
        this.timeZone = null;
        this.timeFormat = null;
        this.locationName = null;
        this.placeAddress = null;
        this.attendees = null;
        this.sessions = null;
        this.speakers = null;
        this.logo = {
            uid: '',
            url: ''
        };
        this.banner = {
            uid: '',
            url: ''
        };
        this.placeName = null;
        this.url = null;
        this.numberAttendee = null;
        this.emailSupport = null;
        this.shortcode = null;
        this.homePage = null;
        this.required_edit_profile = null;
        this.allow_chat = null;
        this.allow_visio = false;
        this.allow_visio_for_2 = false;
    }
}

export class CeuTrack{
    uid: string;
    name: string;
    description: string;
    color: string;
    attendees;
}

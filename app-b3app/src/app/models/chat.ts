export class Chat {
    uid: string;
    message: Array<string>;
    msgDate: number;
    type: string;
    userUid: string;
    readStatus: boolean;
}
export class Notifications { // Model da push notification - Utilizado para transação de dados

    private _title: string; // Título
    private _message: string; // Menssagem
    private _image: string; // Ícone da notificação
    private _groups: Array<string> = [];
    private _date: any;
    private _timestamp: any;
    private _uid: string;
    private _read: boolean;
    private _group: boolean;

    constructor(title: string, message: string, groups: Array<string>, date: any, timestamp: any, uid: string, read: boolean, group: boolean, image?: string) {
        this._title = title;
        this._message = message;
        this._image = image;
        this._groups = groups;
        this._date = date;
        this._timestamp = timestamp;
        this._uid = uid;
        this._read = read;
        this._group = group;
    }

    // Getters
    /**
     * Getter title
     * @return {string}
     */
    public get title(): string {
        return this._title;
    }

    /**
     * Getter message
     * @return {string}
     */
    public get message(): string {
        return this._message;
    }

    /**
     * Getter image
     * @return {string}
     */
    public get image(): string {
        return this._image;
    }

    /**
    * Getter groups
    * @return {Array<string> }
    */
    public get groups(): Array<string> {
        return this._groups;
    }

    /**
     * Getter date
     * @return {string}
     */
    public get date(): any {
        return this._date;
    }

    /**
     * Getter timestamp
     * @return {string}
     */
    public get timestamp(): any {
        return this._timestamp;
    }

    /**
     * Getter uid
     * @return {string}
     */
    public get uid(): any {
        return this._uid;
    }

    // Getters
    /**
     * Getter read
     * @return {boolean}
     */
    public get read(): boolean {
        return this._read;
    }

    /**
 * Getter group
 * @return {boolean}
 */
    public get group(): boolean {
        return this._group;
    }

    // Setters
    /**
     * Setter title
     * @param {string} value
     */
    public set title(value: string) {
        this._title = value;
    }

    /**
     * Setter message
     * @param {string} value
     */
    public set message(value: string) {
        this._message = value;
    }

    /**
     * Setter image
     * @param {string} value
     */
    public set image(value: string) {
        this._image = value;
    }

    /**
     * Setter groups
     * @param {Array<string> } value
     */
    public set groups(value: Array<string>) {
        this._groups = value;
    }

    /**
  * Setter date
  * @param {string} value
  */
    public set date(value: any) {
        this._date = value;
    }

    /**
     * Setter timestamp
     * @param {string} value
     */
    public set timestamp(value: any) {
        this._timestamp = value;
    }

    /**
  * Setter uid
  * @param {string} value
  */
    public set uid(value: any) {
        this._uid = value;
    }

    /**
        * Setter read
        * @param {boolean} value
        */
    public set read(value: boolean) {
        this._read = value;
    }

    /**
     * Setter group
     * @param {boolean} value
     */
    public set group(value: boolean) {
        this._group = value;
    }
}
export class CeuChat {
    uid: string;
    members;
    user_name: string;
    user_picture: string;
    last_message: any;
    badge_number: number;
    last_access;
    secondary_id: string;
    constructor() {
        this.uid = null;
        this.members = {};
    }
}

import { Injectable } from '@angular/core';

@Injectable()

export class PathIcons {
    static icon_settings = 'settings ' //Gerenciar Módulos
    static icon_gavel = 'gavel' //Legal
    static icon_notications = 'notifications' //Notificações
    static icon_map = 'map' //Localização
    static icon_view_agenda = 'view_agenda' //Grupos widgets
    static icon_info = 'info' //Informações
    static icon_people = 'people' //Participantes
    static icon_mic = 'mic' //palestrantes
    static icon_date_range = 'date_range' //Cronograma
    static icon_calendar_today = 'calendar_today' //Agenda pessoal
    static icon_survey = 'feedback'
    static icon_group = 'group_work' // gerenciar grupos
    static icon_document = 'insert_drive_file' // documents
    static icon_gallery = 'collections' // gallery
    static icon_checkin = 'check_circle_outline' // checkin
    static icon_self_checkin = 'mobile_friendly' // self checkin
    static icon_event = 'info'
}


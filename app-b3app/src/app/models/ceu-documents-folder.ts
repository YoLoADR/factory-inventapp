export class DocumentsFolder {
    uid: string;
    name: string;
    orderDocuments: string;
    createdAt: number;
    constructor(name: string) {
        this.uid = null;
        this.name = name;
        this.orderDocuments = 'recent';
        this.createdAt = null;
    }
}
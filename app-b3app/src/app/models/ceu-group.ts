export class CeuGroup {
    uid: string;
    name: string;
    queryName: string; //saves the group name in upper case to do the sorting.
    identifier: number;
    moduleId: string
    eventId: string
    bgColor: string;
    txtColor: string;
    createdAt: Number;

    qtdSessions: number //save the number of sessions that belongs to the group.


    //sessions
    //attendees

    constructor() {
        this.uid = null;
        this.name = null;
        this.queryName = null
        this.identifier = null
        this.moduleId = null
        this.eventId = null
        this.bgColor = null
        this.txtColor = null
        this.createdAt = null
        this.qtdSessions = null
    }


  

}


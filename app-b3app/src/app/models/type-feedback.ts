export enum TypeFeedback{
    ALL_SESSIONS = 'AllSessions',
    SCHEDULE_MODULE = 'ScheduleModule',
    SESSION_TRACK = 'SessionTrack',
    SPECIFIC_SESSION = 'SpecificSession',
    SPECIFIC_GROUP = 'SpecificGroup'
}
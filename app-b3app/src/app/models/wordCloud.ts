export class WordCloud {
    private _uid: string;
    private _answer: string;
    private _total: number;
    private _title: string;
    private _subtitle: string;
    private _max_characters: number;
    private _visibility: boolean;
  
    constructor() {
      this._answer = '';
      this._total = 1;
      this._title = ''
      this._subtitle = '';
      this._max_characters = 10;
      this._visibility = true;
    }
  
    get uid(): string {
      return this._uid;
    }
  
    set uid(value: string) {
      this._uid = value;
    }
  
    get answer(): string {
      return this._answer;
    }
  
    set answer(value: string) {
      this._answer = value;
    }
  
    get total(): number {
      return this._total;
    }
  
    set total(value: number) {
      this._total = value;
    }
  
    get title(): string {
      return this._title;
    }
  
    set title(value: string) {
      this._title = value;
    }
  
    get subtitle(): string {
      return this._subtitle;
    }
  
    set subtitle(value: string) {
      this._subtitle = value;
    }
  
    get max_characters(): number {
      return this._max_characters;
    }
  
    set max_characters(value: number) {
      this._max_characters = value;
    }
  
    get visibility(): boolean {
      return this._visibility;
    }
  
    set visibility(value: boolean) {
      this._visibility = value;
    }
  }
  
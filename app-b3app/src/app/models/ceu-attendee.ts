export class CeuAttendee {
    uid: string;
    identifier: string;
    name: string;
    queryName: string;
    email: string;
    company: string;
    title: any;
    description: any;
    events;
    facebook: string;
    instagram: string;
    twitter: string;
    linkedin: string;
    website: string;
    picture: string;
    selfEditLink: string;
    aboutMe: string;
    language: string;
    checkin: boolean;
    points: number;
    groups: any;
    customFields: any;
    nation: string;
    city: string;
    state: string;
    phone: string;
    letter: string;
    emailRecovery: string;
    moduleId: string;
    eventId: string;
    checkinStatus: boolean;
    edited_profile: boolean;
    photoUrl: string;
    type: number;
    chatStatus: boolean;
    customField;
    // dupla de quarto
    invitationSent: boolean; // convite enviado - dupla de quarto 
    invitationReceived: boolean //convite recebido
    checkins;
    createdAt: number;
    firstAccess: boolean;
    constructor() {
        this.firstAccess = true;
        this.uid = "";
        this.identifier = "";
        this.name = "";
        this.email = "";
        this.company = "";
        this.title = {
            PtBR: "",
            EnUS: "",
            EsES: "",
            FrFR: "",
            DeDE: ""
        };
        this.description = {
            PtBR: "",
            EnUS: "",
            EsES: "",
            FrFR: "",
            DeDE: ""
        };
        this.facebook = "";
        this.instagram = "";
        this.twitter = "";
        this.linkedin = "";
        this.website = "";
        this.picture = "";
        this.selfEditLink = "";
        this.aboutMe = "";
        this.eventId = "";
        this.language = "";
        this.points = 0;
        this.type = 5;
        this.chatStatus = null;
        this.createdAt = null;
        this.photoUrl = '';
        this.nation = "";
        this.city = "";
        this.state = "";
        this.phone = "";
        this.letter = "";
        this.emailRecovery = "";
        this.moduleId = ""
        this.invitationSent = null;
        this.invitationReceived = null;
        this.edited_profile = null;
        this.checkins = [];
        this.queryName = "";
    }
}
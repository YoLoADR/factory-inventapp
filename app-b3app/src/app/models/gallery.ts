export class Gallery {
    private _uid: string;
    private _name: string;
    private _url: string;


    constructor(name: string) {
        this._name = name;
        this._url = "";
    }

    /**
     * Getter uid
     * @return {string}
     */
    public get uid(): string {
        return this._uid;
    }

    /**
     * Getter name
     * @return {string}
     */
    public get name(): string {
        return this._name;
    }

    /**
     * Getter url
     * @return {string}
     */
    public get url(): string {
        return this._url;
    }

    /**
     * Setter uid
     * @param {string} value
     */
    public set uid(value: string) {
        this._uid = value;
    }

    /**
     * Setter name
     * @param {string} value
     */
    public set name(value: string) {
        this._name = value;
    }

    /**
     * Setter url
     * @param {string} value
     */
    public set url(value: string) {
        this._url = value;
    }

}
import { Participant } from './participant';
import { ChatMessage } from './chat-message';

export class GroupDiscussion {
    id: string;
    title: string;
    description: string;
    eventId: string;
    moduleId: string;
    createdAt: firebase.firestore.Timestamp;
    groupCreator: string;
    participants: Participant[];
    members: string[];
    visibility: boolean;
    visio: boolean;
    lastMessage: ChatMessage;
    activeParticipants: string[];
    mutedParticipants: string[];
    notifications$: any = {};
    sessions: string[];
    link_sessions: string;

    constructor(
        groupCreator: string,
        id: string = '',
        title: string = '',
        description: string = '',
        eventId: string = '',
        moduleId: string = '',
        createdAt: firebase.firestore.Timestamp = null,
        participants: Participant[] = [],
        members: string[] = [],
        visibility: boolean = true,
        visio: boolean = true,
        lastMessage: ChatMessage = null,
        activeParticipants: string[] = [],
        mutedParticipants: string[] = [],
        sessions: string[] = [],
        link_sessions: string = ''
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.eventId = eventId;
        this.moduleId = moduleId;
        this.createdAt = createdAt;
        this.groupCreator = groupCreator;
        this.participants = participants;
        this.members = members;
        this.visibility = visibility;
        this.visio = visio;
        this.lastMessage = lastMessage;
        this.activeParticipants = activeParticipants;
        this.mutedParticipants = mutedParticipants;
        this.sessions = sessions;
        this.link_sessions = link_sessions;
    }
}

import { customField } from "./customField";

export class CeuUser {
    uid: string;
    identifier: string;
    name: string;
    email: string;
    emailRecovery: string;
    company: string;
    title: string;
    description: string;
    type: number;
    events;
    facebook: string;
    phone: string;
    instagram: string;
    twitter: string;
    linkedin: string;
    website: string;
    picture: string;
    selfEditLink: string;
    aboutMe: string;
    password: string;
    photoUrl: string;
    language: string;
    checkin: boolean;
    index: number;
    token: string;
    firstAccess: boolean;
    points: number;
    groups: any;
    customFields: any;
    terms_use: string;
    privacity: string;
    createdAt: Date;
    clientId: string;
    attendeeModules;

    constructor() {
        this.name = "";
        this.email = "";
        this.uid = "";
        this.identifier = "";
        this.emailRecovery = "";
        this.company = "";
        this.title = "";
        this.description = "";
        this.type = 0;
        this.facebook = "";
        this.instagram = "";
        this.twitter = "";
        this.linkedin = "";
        this.website = "";
        this.picture = "";
        this.selfEditLink = "";
        this.aboutMe = "";
        this.phone = "";
        this.photoUrl = "";
        this.language = "pt_BR";
        this.token = "";
        this.groups = null;
        this.customFields = null;
        this.terms_use = "";
        this.privacity = "";
        this.createdAt = null;
        this.clientId = null;
    }
}

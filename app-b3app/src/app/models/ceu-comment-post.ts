import { DateTime } from 'luxon';

export class CommentPost{
    uid: string
    criator: any
    date: number
    text: string

    constructor(uid: string, criator: any, text: string){
        this.uid = uid,
        this.criator = criator
        this.text = text

        this.date = DateTime.local().valueOf() / 1000;
    }

}
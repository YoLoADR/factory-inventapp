export class Maps {
    private _uid: string;
    private _name: string;
    private _type: string;
    private _local: string;
    private _url: string;

    constructor() {
    }


    get uid(): string {
        return this._uid;
    }

    set uid(value: string) {
        this._uid = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get url(): string {
        return this._url;
    }

    set url(value: string) {
        this._url = value;
    }

    get local(): string {
        return this._local;
    }

    set local(value: string) {
        this._local = value;
    }
}

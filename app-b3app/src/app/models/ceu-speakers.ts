import { CeuTrack } from './ceu-track';
import { customField } from './customField';
import { Session } from './ceu-session';
import { Document } from './documents';

export class CeuSpeaker {
    private _uid: string;
    private _identifier: string; //atributo usado para relaciona o speaker com as sessões
    private _moduleId: string;
    private _name: string;
    private _email: string;
    private _company: string;
    private _title: string;
    private _description: string;
    private _facebook: string;
    private _instagram: string;
    private _twitter: string;
    private _linkedin: string;
    private _website: string;
    private _picture: string;
    private _selfEditLink: string;
    private _urlPhoto: string;
    private _tracks: Array<CeuTrack>;
    private _letter: string;
    private _customFields: Array<customField>;
    private _sessions: Array<Session>;
    private _documents: Array<Document>;
    private _edited_profile: boolean;
    constructor() {
        this._uid = null;
        this._identifier = null;
        this._moduleId = null;
        this._name = null;
        this._email = null;
        this._company = null;
        this._title = null;
        this._description = null;
        this._facebook = null;
        this._instagram = null;
        this._twitter = null;
        this._linkedin = null;
        this._website = null;
        this._picture = null;
        this._selfEditLink = null;
        this._tracks = null;
        this._urlPhoto = "http://lentech.org/images/no_avatar.png";
        this._customFields = [];
        this._sessions = [];
        this._documents = [];
        this._edited_profile = null;
    }


    /**
     * Getter sessions
     * @return {Array<Session>}
     */
    public get sessions(): Array<Session> {
        return this._sessions;
    }

    /**
     * Setter sessions
     * @param {Array<Session>} value
     */
    public set sessions(value: Array<Session>) {
        this._sessions = value;
    }

    /**
 * Getter documents
 * @return {Array<Document>}
 */
    public get documents(): Array<Document> {
        return this._documents;
    }

    /**
     * Setter documents
     * @param {Array<Document>} value
     */
    public set documents(value: Array<Document>) {
        this._documents = value;
    }


    /**
    * Getter uid
    * @return {string}
    */
    public get tracks(): Array<CeuTrack> {
        return this._tracks;
    }

    /**
    * Getter uid
    * @return {string}
    */
    public get uid(): string {
        return this._uid;
    }

    /**
     * Getter identifier
     * @return {string}
     */
    public get identifier(): string {
        return this._identifier;
    }

    /**
     * Getter name
     * @return {string}
     */
    public get name(): string {
        return this._name;
    }

    /**
     * Getter email
     * @return {string}
     */
    public get email(): string {
        return this._email;
    }

    /**
     * Getter company
     * @return {string}
     */
    public get company(): string {
        return this._company;
    }

    /**
     * Getter title
     * @return {string}
     */
    public get title(): string {
        return this._title;
    }

    /**
     * Getter description
     * @return {string}
     */
    public get description(): string {
        return this._description;
    }

    /**
     * Getter facebook
     * @return {string}
     */
    public get facebook(): string {
        return this._facebook;
    }

    /**
     * Getter instagram
     * @return {string}
     */
    public get instagram(): string {
        return this._instagram;
    }

    /**
     * Getter twitter
     * @return {string}
     */
    public get twitter(): string {
        return this._twitter;
    }

    /**
     * Getter linkedin
     * @return {string}
     */
    public get linkedin(): string {
        return this._linkedin;
    }

    /**
     * Getter website
     * @return {string}
     */
    public get website(): string {
        return this._website;
    }

    /**
     * Getter picture
     * @return {string}
     */
    public get picture(): string {
        return this._picture;
    }

    /**
     * Getter selfEditLink
     * @return {string}
     */
    public get selfEditLink(): string {
        return this._selfEditLink;
    }

    /**
     * Getter urlPhoto
     * @return {string}
     */
    public get urlPhoto(): string {
        return this._urlPhoto;
    }

    /**
     * Setter uid
     * @param {string} value
     */
    public set uid(value: string) {
        this._uid = value;
    }

    /**
     * Setter identifier
     * @param {string} value
     */
    public set identifier(value: string) {
        this._identifier = value;
    }

    /**
     * Setter name
     * @param {string} value
     */
    public set name(value: string) {
        this._name = value;
    }

    /**
     * Setter email
     * @param {string} value
     */
    public set email(value: string) {
        this._email = value;
    }

    /**
     * Setter company
     * @param {string} value
     */
    public set company(value: string) {
        this._company = value;
    }

    /**
     * Setter title
     * @param {string} value
     */
    public set title(value: string) {
        this._title = value;
    }

    /**
     * Setter description
     * @param {string} value
     */
    public set description(value: string) {
        this._description = value;
    }

    /**
     * Setter facebook
     * @param {string} value
     */
    public set facebook(value: string) {
        this._facebook = value;
    }

    /**
     * Setter instagram
     * @param {string} value
     */
    public set instagram(value: string) {
        this._instagram = value;
    }

    /**
     * Setter twitter
     * @param {string} value
     */
    public set twitter(value: string) {
        this._twitter = value;
    }

    /**
     * Setter linkedin
     * @param {string} value
     */
    public set linkedin(value: string) {
        this._linkedin = value;
    }

    /**
     * Setter website
     * @param {string} value
     */
    public set website(value: string) {
        this._website = value;
    }

    /**
     * Setter picture
     * @param {string} value
     */
    public set picture(value: string) {
        this._picture = value;
    }

    /**
     * Setter selfEditLink
     * @param {string} value
     */
    public set selfEditLink(value: string) {
        this._selfEditLink = value;
    }

    /**
     * Setter urlPhoto
     * @param {string} value
     */
    public set urlPhoto(value: string) {
        this._urlPhoto = value;
    }

    /**
     * Getter moduleId
     * @return {string}
     */
    public get moduleId(): string {
        return this._moduleId;
    }

    /**
     * Setter moduleId
     * @param {string} value
     */
    public set moduleId(value: string) {
        this._moduleId = value;
    }

    public set tracks(value: Array<CeuTrack>) {
        this._tracks = value;
    }

    /**
     * Getter letter
     * @return {string}
     */
    public get letter(): string {
        return this._letter;
    }

    /**
     * Setter letter
     * @param {string} value
     */
    public set letter(value: string) {
        this._letter = value;
    }


    /**
     * Getter customFields
     * @return {Array<customField>}
     */
    public get customFields(): Array<customField> {
        return this._customFields;
    }

    /**
     * Setter customFields
     * @param {Array<customField>} value
     */
    public set customFields(value: Array<customField>) {
        this._customFields = value;
    }


    /**
      * Getter edited_profile
      * @return {boolean}
      */
    public get edited_profile(): boolean {
        return this._edited_profile;
    }

    /**
     * Setter edited_profile
     * @param {boolean} value
     */
    public set edited_profile(value: boolean) {
        this._edited_profile = value;
    }
}
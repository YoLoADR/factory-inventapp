export class SubscriptionAnswer {
    uid: string;
    answer: Array<string>;
    quantity: Array<number>;
    quantity_remaining: Array<number>;

    constructor() {
        this.answer = new Array<string>();
        this.quantity = new Array<number>();
        this.quantity_remaining = new Array<number>();
    }


}
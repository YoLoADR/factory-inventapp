export class customField {
    private _uid: string;
    private _nomeCampo: string;
    private _exibicao: string;
    private _edicao: boolean;
    private _value: string;
    
    constructor(){
        this._uid = null;
        this._nomeCampo = null;
        this._exibicao = null;
        this._edicao = false;
        this._value = null;
    }


    /**
     * Getter value
     * @return {string}
     */
	public get value(): string {
		return this._value;
	}

    /**
     * Setter value
     * @param {string} value
     */
	public set value(value: string) {
		this._value = value;
	}


    /**
     * Getter uid
     * @return {string}
     */
	public get uid(): string {
		return this._uid;
	}

    /**
     * Getter nomeCampo
     * @return {string}
     */
	public get nomeCampo(): string {
		return this._nomeCampo;
	}

    /**
     * Getter exibicao
     * @return {string}
     */
	public get exibicao(): string {
		return this._exibicao;
	}

    /**
     * Getter edicao
     * @return {string}
     */
	public get edicao(): boolean {
		return this._edicao;
	}

   /**
     * Setter uid
     * @param {string} value
     */
	public set uid(value: string) {
		this._uid = value;
	}

    /**
     * Setter nomeCampo
     * @param {string} value
     */
	public set nomeCampo(value: string) {
		this._nomeCampo = value;
	}

    /**
     * Setter exibicao
     * @param {string} value
     */
	public set exibicao(value: string) {
		this._exibicao = value;
	}

    /**
     * Setter edicao
     * @param {string} value
     */
	public set edicao(value: boolean) {
		this._edicao = value;
	}

}
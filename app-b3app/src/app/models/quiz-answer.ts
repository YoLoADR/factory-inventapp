export class Answer {
    uid: string;
    answer: string;
    weight: number;
    result: number;
    correct: boolean;
    showFalse: boolean = false;
    selected: boolean = false;
}
export class trainingAnswer {
    uid: string;
    answer: string;
    weight: number;
    result: number;
    correct: boolean;
}
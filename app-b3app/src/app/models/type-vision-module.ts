export enum TypeVisionModule {
    GLOBAL_VISION = 0, // visão global 
    DIVIDED_BY_GROUPS = 1, // visão dividida por grupo
    GROUP_VISION = 2,  // visão por grupos 
    GROUP_ACCESS_PERMISSION = 3 //acessp limitado por grupo 
}
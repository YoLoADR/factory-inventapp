import { FeedNewsComment } from "./feed-news-comment";

export class FeedPost {
    description: string;
    uid: string;
    timestamp: number;
    urlPhoto: string;
    likes: any;
    userId: string;
    fixed: boolean = false;
    visible: boolean = false;
    type: string;
    comments: Array<FeedNewsComment>;
    date: string;
}
export class GalleryFolder {
    uid: string;
    name: string;
    orderGallery: string;
    createdAt: number;
    constructor(name: string) {
        this.uid = null;
        this.name = name;
        this.orderGallery = 'recent';
        this.createdAt = null;
    }
}
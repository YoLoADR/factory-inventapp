import { Session } from "../models/ceu-session";

export class CeuLocation {
    private _uid: string;
    private _urlPhoto: string;
    private _name: string;
    private _nameFile: string;
    private _description: string;
    private _sessions : Array<Session>

    constructor() {
        this._uid = null;
        this._urlPhoto = null;
        this._name = null;
        this._nameFile = null;
        this._description = null;
        this._sessions = [];
     }

    /**
     * Getter uid
     * @return {string}
     */
	public get uid(): string {
		return this._uid;
	}

    /**
     * Getter urlPhoto
     * @return {string}
     */
	public get urlPhoto(): string {
		return this._urlPhoto;
	}

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Getter nameFile
     * @return {string}
     */
	public get nameFile(): string {
		return this._nameFile;
	}

    /**
     * Getter description
     * @return {string}
     */
	public get description(): string {
		return this._description;
	}

     /**
     * Getter sesions
     * @return {Array<Session>}
     */
	public get sessions(): Array<Session> {
		return this._sessions;
	}

    /**
     * Setter uid
     * @param {string} value
     */
	public set uid(value: string) {
		this._uid = value;
	}

    /**
     * Setter urlPhoto
     * @param {string} value
     */
	public set urlPhoto(value: string) {
		this._urlPhoto = value;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

    /**
     * Setter nameFile
     * @param {string} value
     */
	public set nameFile(value: string) {
		this._nameFile = value;
	}

    /**
     * Setter description
     * @param {string} value
     */
	public set description(value: string) {
		this._description = value;
    }
    
      /**
     * Setter sessions
     * @param {Array<Session>} value
     */
	public set sessions(value: Array<Session>) {
		this._sessions = value;
    }
    

    

}
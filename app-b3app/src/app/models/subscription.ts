import { SubscriptionQuestions } from "./subscription-questions";

export class Subscription {
    title: string;
    type: string;
    uid: string;
    visibility: string;
    icon: string;
    change_answer: boolean;
    max_responses: number;
    module_id: string;
    references: string;
}
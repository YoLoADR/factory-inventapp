export enum TypeVisionDocument {
    GLOBAL_VISION = 0, // visão global 
    GROUP_VISION = 1,  // visão por grupos do attendee
}
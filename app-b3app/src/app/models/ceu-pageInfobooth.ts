import { CeuModule } from "../models/ceu-module";

export class CeuPageInfobooth extends CeuModule {
    private _pageContent: string; // html, imagem, video inserido pelo usuário.
    private _uidInfobooth: string; //id do infobooth da página

    constructor(uid: string, name: string, icon: string, pageContent: string) {
        super(uid, name, icon, null, null, null, null, null);
        this._pageContent = pageContent;
    }

    /**
     * Getter pageContent
     * @return {string}
     */
    public get pageContent(): string {
        return this._pageContent;
    }

    /**
     * Setter pageContent
     * @param {string} value
     */
    public set pageContent(value: string) {
        this._pageContent = value;
    }


    /**
     * Getter uidInfobooth
     * @return {string}
     */
    public get uidInfobooth(): string {
        return this._uidInfobooth;
    }

    /**
     * Setter uidInfobooth
     * @param {string} value
     */
    public set uidInfobooth(value: string) {
        this._uidInfobooth = value;
    }

}
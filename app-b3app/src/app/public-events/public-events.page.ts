import { Component, OnInit } from '@angular/core';
import { DaoEventsService } from '../providers/db/dao-events.service';
import { MenuController, Events, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { CeuEvent } from '../models/ceu-event';
import { LuxonService } from '../providers/luxon/luxon.service';

@Component({
    selector: 'app-public-events',
    templateUrl: './public-events.page.html',
    styleUrls: ['./public-events.page.scss'],
})
export class PublicEventsPage implements OnInit {
    events;
    searchOpen = false;
    terms;
    loader: boolean = true;

    constructor(
        private daoEvents: DaoEventsService,
        private luxon: LuxonService,
        private router: Router,
        private menuCtrl: MenuController,
        public global: GlobalService,
        private eventsA: Events,
        private navCtrl: NavController
    ) {
        this.menuCtrl.enable(false);

        this.eventsA.subscribe('reloadContainerEvents', () => {
            this.loadEvents();
        });
    }

    ngOnInit() {
        this.loader = true;
        this.loadEvents();
    }

    loadEvents() {
        this.daoEvents.getPublicEvents((events: Array<CeuEvent>) => {
            // makes the treatment of the startDate of the event.
            for (const event of events) {
                if (typeof event.startDate == 'number') {
                    const sDate = this.luxon.convertTimestampToDate(event.startDate)
                    event.startDate = this.luxon.convertDateToFormatDDMM(sDate)
                }
                if (event.banner == '' || event.banner == null || event.banner == undefined) {
                    event.banner = {
                        uid: '',
                        url: ''
                    }
                }
            }

            this.events = events;
            this.loader = false;
        });
    }

    openEvent(event) {
        this.loader = true;
        this.menuCtrl.enable(true);
        localStorage.setItem('eventId', event.uid);
        this.global.previousPage = 'container';

        this.global.loadService(() => {
            // this.router.navigate([`${event.homePage}`])
            this.navCtrl.navigateRoot([`${event.homePage}`])
                .then((_) => {
                    this.eventsA.publish('updateScreen');
                });
        });
    }

}

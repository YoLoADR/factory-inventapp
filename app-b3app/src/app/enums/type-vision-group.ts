export enum TypeVisionGroup {
    GLOBAL_VISION = 0, // visão global 
    DIVIDED_BY_GROUPS = 1, // visão dividida por grupod
    GROUP_VISION = 2,  // visão por grupos do attendee
    GROUP_ACCESS_PERMISSION = 3 //acessp limitado por grupo 
}
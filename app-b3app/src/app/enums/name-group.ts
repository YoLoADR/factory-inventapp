export enum NameGroup {
    DeDE = 'DE-DE',
    EnUS = 'EN-US',
    EsES = 'ES-ES',
    FrFR = 'FR-FR',
    PtBR = 'PT-BR'
}
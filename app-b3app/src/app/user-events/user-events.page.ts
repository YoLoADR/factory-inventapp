import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { DaoEventsService } from '../providers/db/dao-events.service';
import { AuthService } from '../providers/authentication/auth.service';
import { Events, MenuController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { LuxonService } from '../providers/luxon/luxon.service';

@Component({
    selector: 'app-user-events',
    templateUrl: './user-events.page.html',
    styleUrls: ['./user-events.page.scss'],
    providers: [DaoEventsService]
})
export class UserEventsPage implements OnInit {
    events;
    searchOpen = false;
    container: any;
    userId: string = null;
    loader: boolean = true;
    allow: boolean = true;

    constructor(
        public global: GlobalService,
        private daoEvents: DaoEventsService,
        private auth: AuthService,
        private route: ActivatedRoute,
        private eventsA: Events,
        private menuCtrl: MenuController,
        private luxon: LuxonService,
        private navCtrl: NavController
    ) {
    }

    ngOnInit() {
        this.loader = true;
        this.route.params.subscribe((params) => {
            this.userId = params.userId;
            this.menuCtrl.enable(false);
            this.loadEvents();
        })
    }

    loadEvents() {
        this.daoEvents.getUserEvents(this.userId, (data) => {
            // makes the treatment of the startDate of the event.
            for (const event of data) {
                if (typeof event.startDate == 'number') {
                    const sDate = this.luxon.convertTimestampToDate(event.startDate)
                    event.startDate = this.luxon.convertDateToFormatDDMM(sDate)
                }
                if (event.banner == '' || event.banner == null || event.banner == undefined) {
                    event.banner = {
                        uid: '',
                        url: ''
                    }
                }
            }
            this.events = data;
            this.loader = false;
        });
    }

    logOut() {
        this.auth.logout();
    }

    openEvent(event) {
        this.loader = true;
        localStorage.setItem('eventId', event.uid);
        localStorage.setItem('homePage', event.homePage);
        this.global.eventHomePage = event.homePage;
        this.global.previousPage = 'container';

        this.global.loadService((_) => {
            if (this.allow) {
                this.allow = false;
                if (event.required_edit_profile == false) {
                    // this.router.navigate([event.homePage])
                    this.navCtrl.navigateRoot([event.homePage])
                        .then((_) => {
                            if (_ == true) {

                                this.eventsA.publish('updateScreen');
                                // this.menuCtrl.enable(true);
                                this.loader = false;
                            }
                        });
                } else if (event.required_edit_profile == true && this.global.userEditProfile == true) {
                    // this.router.navigate([event.homePage])
                    this.navCtrl.navigateRoot([event.homePage])
                        .then((_) => {
                            if (_ == true) {

                                this.eventsA.publish('updateScreen');
                                // this.menuCtrl.enable(true);
                                this.loader = false;
                            }
                        });
                } else if (event.required_edit_profile == true && this.global.userEditProfile == false) {
                    // this.router.navigate([`/event/${event.uid}/edit-profile/${this.global.userModuleId}/${this.global.userType}/${this.global.userId}`])
                    this.navCtrl.navigateRoot([`/event/${event.uid}/edit-profile/${this.global.userModuleId}/${this.global.userType}/${this.global.userId}`])
                        .then((_) => {
                            if (_ == true) {

                                this.eventsA.publish('updateScreen');
                                // this.menuCtrl.enable(true);
                                this.loader = false;
                            }
                        });
                }
            }
        });
    }
}

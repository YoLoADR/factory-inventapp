import { Component, OnInit } from '@angular/core';
import { NavParams, ActionSheetController, PopoverController, ModalController } from '@ionic/angular';
import { DaoFeedNewsService } from 'src/app/providers/db/dao-feed-news.service';
import { Post } from 'src/app/models/ceu-post';
import { TranslateService } from '@ngx-translate/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { GlobalService } from 'src/app/shared/services';

@Component({
    selector: 'app-feed-more-options',
    templateUrl: './feed-more-options.component.html',
    styleUrls: ['./feed-more-options.component.scss']
})
export class FeedMoreOptionsComponent implements OnInit {
    post: Post = null;
    posts = []
    eventId: string = null;
    moduleId: string = null
    isModal: boolean = false;
    allowDelete: boolean = false;
    isSocialSharing: boolean = false;

    constructor(
        private params: NavParams,
        private dbFeed: DaoFeedNewsService,
        private translateService: TranslateService,
        private actionSheetController: ActionSheetController,
        private socialSharing: SocialSharing,
        private popoverCtrl: PopoverController,
        private modalCtrl: ModalController,
        public global: GlobalService
    ) {
        this.post = this.params.get('post');
        this.posts = this.params.get('posts');

        if (this.post.creator.uid == this.global.userId || this.global.userType <= 1) {
            this.allowDelete = true;
        }
        this.eventId = this.params.get('eventId');
        this.moduleId = this.params.get('moduleId');
        this.isModal = this.params.get('isModal');

        this.getModule()
    }

    ngOnInit() {
    }

    // Opens the social sharing action sheet
    async shareInSocialMedia() {
        const actionSheet = await this.actionSheetController.create({
            header: this.translateService.instant('global.texts.social_medias'),
            buttons: [
                // {
                //   text: 'E-mail',
                //   icon: 'mail',
                //   handler: () => {
                //     this.socialSharing.shareViaEmail(this.posts.description, "Post", )
                //   }
                // },
                {
                    text: 'Facebook',
                    icon: 'logo-facebook',
                    handler: () => {
                        this.socialSharing.shareViaFacebook(this.post.description, null, this.post.img.url).then(() => {

                        }).catch((err) => {

                        })
                    }
                },
                {
                    text: 'Twitter',
                    icon: 'logo-twitter',
                    handler: () => {
                        this.socialSharing.shareViaTwitter(this.post.description, null, this.post.img.url).then(() => {

                        }).catch((err) => {

                        })
                    }
                },
                {
                    text: 'Whatsapp',
                    icon: 'logo-whatsapp',
                    handler: async () => {
                        this.socialSharing.shareViaWhatsApp(this.post.description, null, this.post.img.url).then(() => {

                        }).catch((err) => {

                        })
                    }
                },
                {
                    text: this.translateService.instant('global.buttons.cancel'),
                    icon: 'close',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }

                }]
        })

        this.popoverCtrl.dismiss();
        await actionSheet.present();
    }

    getModule() {
        this.dbFeed.moduleNewsFeed(this.moduleId, (module) => {
            this.isSocialSharing = module.social_sharing
        })
    }


    deletePost() {
        // remove post from post array
        const pos = this.posts.map(function (e) { return e.uid; }).indexOf(this.post.uid);

        if (pos > -1) {
            this.posts.splice(pos, 1)
        }


        this.dbFeed.deletePost(this.post, this.eventId);
        this.popoverCtrl.dismiss();
        if (this.isModal) this.modalCtrl.dismiss();
    }

    ionViewDidLeave() {
        this.allowDelete = false;
    }

}

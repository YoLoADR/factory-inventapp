import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, Events } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/providers/authentication/auth.service';
import { TypeUser } from 'src/app/models/type-user';
import { Router } from '@angular/router';
import { DaoGeralService } from 'src/app/providers/db/dao-geral.service';
import { GlobalService } from 'src/app/shared/services';
import { TermsNPrivacyComponent } from '../terms-n-privacy/terms-n-privacy.component';

@Component({
    selector: 'app-public-login',
    templateUrl: './public-login.component.html',
    styleUrls: ['./public-login.component.scss']
})
export class PublicLoginComponent implements OnInit {
    eventId: string = null;
    logoUrl: string = null;
    insertPasswordView: boolean = false;
    activeLoader: boolean = false;
    menuColor: string = null;
    menuTxtColor: string = null;
    email: string = null;
    password: any = null;
    user;
    userType: number = null;
    clientId: string = null;
    msgEmail;
    msgPassword;
    activeLoader2: boolean = false;
    createPass: string = null;
    confirmPass: string = null;
    userUid: string = null;
    userTempId: string = null;
    codeNumber: string;
    codeNumberTyped: string = null;
    count: number;
    interval: any;
    seconds: number = 180;
    timeRemaining;
    msgTermRequired: string = null;
    msgTimeRemaining: string = null;
    msgCode: string = null;
    normalView: boolean = true;
    firstAccessView: boolean = false;
    createPassView: boolean = false;
    msgEmailAlreadyUse: string = null;
    msgCreateAccount: string = null;
    emailDisabled: boolean = false;
    termCheck: boolean;
    type: string = null;
    event: any = null;
    constructor(
        private navParams: NavParams,
        private modalCtrl: ModalController,
        private events: Events,
        private translateService: TranslateService,
        private auth: AuthService,
        private router: Router,
        private dbGeral: DaoGeralService,
        public global: GlobalService
    ) {
        this.eventId = this.navParams.get('eventId');
        this.logoUrl = this.navParams.get('logoUrl');
        this.menuColor = this.navParams.get('menuColor');
        this.menuTxtColor = this.navParams.get('menuTxtColor');
    }

    ngOnInit() {
        this.event = this.global.event;
    }

    verifyEmail() {
        if (this.email !== null) {
            this.activeLoader = true;
            this.auth.verifyEmailDb(this.email, (user) => {
                if (user['code'] !== 404) {
                    this.user = user['result'];
                    this.userType = user['result']['type'];
                    this.emailDisabled = true;
                    // verify if type is client or employee to get client id and display correct events later
                    if (this.userType == TypeUser.CLIENT) {
                        this.clientId = user['result']['uid'];
                    } else if (this.userType == TypeUser.EMPLOYEE) {
                        this.clientId = user['result']['clientId'];
                    }
                    // case e-mail exists and is first access, display verify code view;
                    if (user['result'] !== 'email-not-found') {
                        if (user['message'] == 'success') {
                            if (user['result']['firstAccess'] == false) {
                                this.insertPasswordView = true;
                                // this.passwordInput.nativeElement.focus();
                                this.activeLoader = false;
                            } else {
                                // case first access, send code number and create account
                                this.normalView = false;
                                this.firstAccessView = true;
                                this.createCodeNumber();
                                this.activeLoader = false;
                            }
                        }
                    } else {
                        this.msgEmail = this.translateService.instant('pages.login.email_not_found');
                        this.emailDisabled = false;
                        this.activeLoader = false;
                    }
                } else {
                    this.msgEmail = this.translateService.instant('pages.login.email_not_found');
                    this.activeLoader = false;
                }
            });
        } else {
            this.msgEmail = this.translateService.instant('pages.login.blank_email');
        }

    }

    createCodeNumber() {
        // call function to create code, insert in user database and send e-mail to user;
        this.auth.sendCodeNumberToEmail(this.user, (data) => {

            if (data !== false) {
                this.codeNumber = data;
                this.userTempId = this.user['uid'];

                this.count = this.seconds;
                this.interval = setInterval(() => {
                    this.count--;

                    var date = new Date(null);
                    date.setSeconds(this.count); // specify value for SECONDS here
                    this.timeRemaining = date.toISOString().substr(11, 8);

                    if (this.count == 0) {
                        clearInterval(this.interval);
                        this.msgTimeRemaining = this.translateService.instant('pages.login.resend_code_number');
                    }
                }, 1000);
                this.activeLoader2 = false;
            } else {
                this.msgCode = this.translateService.instant('pages.login.error_sendemail_code');
            }
        })
    }

    /* in case of first access, the user needs to verify your identity 
    by code number sended to e-mail and create your account */
    verifyCodeNumber() {
        if (this.codeNumberTyped !== null) {
            this.activeLoader = true;
            this.auth.verifyCodeNumber(this.codeNumberTyped, this.userTempId, (data) => {
                if (data['result'] == true) {
                    clearInterval(this.interval);
                    this.firstAccessView = false;
                    this.createPassView = true;
                    this.activeLoader = false;
                } else if (data['result'] == false) {
                    this.msgCode = this.translateService.instant('pages.login.invalid_code');
                    this.activeLoader = false;
                } else {
                    // not ok
                    this.msgCode = this.translateService.instant('pages.login.error_sendemail_code');
                    this.activeLoader = false;
                }
            });
        } else {
            this.msgCode = 'O código não pode estar vazio'
        }
    }

    /* to create account, the user enter your password two times, check both 
  is equal and add to Firebase Authentication and update in Firestore */
    createAccount() {
        this.msgPassword = null;
        if (this.createPass.length >= 6) {
            if (this.createPass == this.confirmPass) {
                if (this.termCheck) {
                    this.activeLoader = true;
                    this.auth.createAccount(this.email, this.createPass, this.userTempId, this.userType).subscribe((success) => {
                        if (success['message'] == 'error') {
                            if (success['code'] == "auth/email-already-in-use") {
                                this.activeLoader = false;
                                this.msgEmailAlreadyUse = this.translateService.instant('pages.login.email_already_use');
                            } else {
                                this.activeLoader = false;
                                this.msgCreateAccount = this.translateService.instant('pages.login.pass_error');
                            }
                        } else if (success['message'] == 'success') {
                            // call API sending old uid and new uid; 
                            this.password = this.createPass;
                            this.makeLogin();
                        }
                    }, (error) => {
                        this.activeLoader = false;
                        this.msgCreateAccount = this.translateService.instant('pages.login.pass_error');
                    })
                } else {
                    this.activeLoader = false;
                    this.msgPassword = this.translateService.instant('pages.login.term_check_required');
                }
            } else {
                this.activeLoader = false;
                this.msgPassword = this.translateService.instant('pages.login.password_not_match');
            }
        } else {
            this.activeLoader = false;
            this.msgPassword = this.translateService.instant('pages.login.invalid_pass');
        }
    }

    makeLogin() {
        this.msgPassword = null;
        this.activeLoader = true;
        this.email = this.email.toLowerCase();
        if (this.password !== null && this.password !== undefined) {
            if (this.termCheck) {
                this.auth.login(this.email, this.password, (data) => {
                    if (data.code == 'auth/wrong-password') {
                        this.activeLoader = false;
                        this.msgPassword = this.translateService.instant('pages.login.incorrect_pass');
                    } else if (data.user['uid'] !== null && data.user['uid'] !== undefined) {
                        localStorage.setItem('userIdentifier', data.user['uid']);
                        if (this.eventId !== null && this.eventId !== '' && this.eventId !== undefined) {
                            localStorage.setItem('eventId', this.eventId);
                            let index = this.user['events'].map(function (e) { return e; }).indexOf(this.eventId);
                            if (index > -1) {
                                this.events.publish('allowReloadModules');
                                this.global.loadService((data) => {
                                    this.events.publish('updateScreen');
                                    this.close();
                                });
                            } else {
                                this.dbGeral.addUserToEvent(this.user, this.eventId, (status) => {
                                    // if (status == true) {
                                    this.events.publish('allowReloadModules');
                                    this.global.loadService((data) => {
                                        this.events.publish('updateScreen');
                                        this.close();
                                    });
                                })
                            }
                        } else {
                            if (this.userType == TypeUser.SUPERGOD || this.userType == TypeUser.GOD) {
                                this.events.publish('allowReloadModules');
                                this.global.loadService((data) => {
                                    this.events.publish('updateScreen');
                                    this.close();
                                    this.router.navigate(['/admin-events']);
                                });
                            } else if (this.userType == TypeUser.CLIENT || this.userType == TypeUser.EMPLOYEE) {
                                this.events.publish('allowReloadModules');
                                this.global.loadService((data) => {
                                    this.events.publish('updateScreen');
                                    this.close();
                                    this.router.navigate(['/c-events', this.clientId]);
                                });
                            } else if (this.userType == TypeUser.SPEAKER || this.userType == TypeUser.ATTENDEE) {
                                this.events.publish('allowReloadModules');
                                this.global.loadService((data) => {
                                    this.events.publish('updateScreen');
                                    this.close();
                                    this.router.navigate(['/user-events', data.user['uid']]);
                                });
                            }
                        }
                    } else if (data.code == 'auth/too-many-requests') {
                        this.activeLoader == false;
                        this.msgPassword = this.translateService.instant('pages.login.many_pass_errors');
                    } else if (data.code == 'auth/argument-error') {
                        this.activeLoader == false;
                        this.msgPassword = this.translateService.instant('pages.login.invalid_pass');
                    } else {
                        this.activeLoader == false;
                        this.msgPassword = this.translateService.instant('pages.login.invalid_pass');
                    }
                });
            } else {
                this.activeLoader = false;
                this.msgPassword = this.translateService.instant('pages.login.term_check_required');
            }
        } else {
            this.activeLoader = false;
            this.msgPassword = this.translateService.instant('pages.login.incorrect_pass');
        }
    }

    recoveryPassword() {
        if (this.email !== null && this.email !== undefined && this.email !== null) {
            this.auth.recoveryPassword(this.email, (status) => {
                if (status == true) {
                } else {

                }
            });
        } else {
            // blank email error
        }
    }

    async openTermsOfUseAndPrivacy(type) {
        const modal = await this.modalCtrl.create({
            component: TermsNPrivacyComponent,
            componentProps: {
                eventId: this.eventId,
                type: type
            }
        });
        return await modal.present();
    }

    close() {
        this.activeLoader == false;
        this.modalCtrl.dismiss();
        this.eventId = null;
        this.type = null;
    }

    signInWithFacebook() {
        alert('Funcionalidade em desenvolvimento! ;)')
    }
    signInWithTwitter() {
        alert('Funcionalidade em desenvolvimento! ;)')
    }
    signInWithGoogle() {
        alert('Funcionalidade em desenvolvimento! ;)')
    }
    signInWithMicrosoft() {
        alert('Funcionalidade em desenvolvimento! ;)')
    }
    signInWithYahoo() {
        alert('Funcionalidade em desenvolvimento! ;)')
    }
}

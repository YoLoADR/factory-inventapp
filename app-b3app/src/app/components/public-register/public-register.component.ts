import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, Events, NavController } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { TypeUser } from 'src/app/models/type-user';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/providers/authentication/auth.service';
import { RegexProvider } from 'src/app/providers/regex/regex.service';
import { TermsNPrivacyComponent } from '../terms-n-privacy/terms-n-privacy.component';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ViewController } from '@ionic/core';

@Component({
    selector: 'app-public-register',
    templateUrl: './public-register.component.html',
    styleUrls: ['./public-register.component.scss'],
    providers: [RegexProvider]
})
export class PublicRegisterComponent implements OnInit {
    activeLoader: boolean = false;
    logoUrl: string = null;
    menuColor: string = null;
    menuTxtColor: string = null;
    email: string = null;
    password: any = null;
    name: string = null;
    userType: number = 5;
    clientId: string = null;
    msgEmail;
    msgPassword;
    msgName;
    msgGeral: any = null;
    eventId: string = null;
    termCheck: boolean;
    constructor(
        private navParams: NavParams,
        private modalCtrl: ModalController,
        private events: Events,
        private auth: AuthService,
        public global: GlobalService,
        private router: Router,
        private translateService: TranslateService,
        private regex: RegexProvider,
        // private navCtrl: NavController,
        private splashScreen: SplashScreen
    ) {
        this.eventId = this.navParams.get('eventId');
        this.logoUrl = this.navParams.get('logoUrl');
        this.menuColor = this.navParams.get('menuColor');
        this.menuTxtColor = this.navParams.get('menuTxtColor');
    }

    ngOnInit() {
    }

    createAccount() {
        this.msgEmail = null;
        this.msgName = null;
        this.msgPassword = null;
        if (this.name !== null) {
            if (this.regex.validateEmail(this.email)) {
                if (this.email !== null) {
                    if (this.password !== null) {
                        if (this.password.length >= 6) {
                            if (this.termCheck) {
                                this.activeLoader = true;
                                this.auth.createUserPublicEvent(this.eventId, this.name, this.email, this.password, this.global.event.language, (data) => {
                                    if (data.status) {
                                        localStorage.setItem('userIdentifier', data.uid);
                                        if (this.eventId !== null && this.eventId !== '' && this.eventId !== undefined) {
                                            localStorage.setItem('eventId', this.eventId);
                                            this.events.publish('allowReloadModules');
                                            this.global.loadService((data) => {
                                                this.events.publish('updateScreen');
                                                this.close();
                                            });
                                        } else {
                                            if (this.userType == TypeUser.SUPERGOD || this.userType == TypeUser.GOD) {
                                                this.events.publish('allowReloadModules');
                                                this.global.loadService((data) => {
                                                    this.events.publish('updateScreen');
                                                    this.close();
                                                    this.router.navigate(['/admin-events']);
                                                });
                                            } else if (this.userType == TypeUser.CLIENT || this.userType == TypeUser.EMPLOYEE) {
                                                this.events.publish('allowReloadModules');
                                                this.global.loadService((data) => {
                                                    this.events.publish('updateScreen');
                                                    this.close();
                                                    this.router.navigate(['/c-events', this.clientId]);
                                                });
                                            } else if (this.userType == TypeUser.SPEAKER || this.userType == TypeUser.ATTENDEE) {
                                                this.events.publish('allowReloadModules');
                                                this.global.loadService((data) => {
                                                    this.events.publish('updateScreen');
                                                    this.close();
                                                    this.router.navigate(['/user-events', data.uid]);
                                                });
                                            }
                                        }
                                        this.activeLoader = false;
                                    } else if (data.status == 'account-already-exist') {
                                        this.activeLoader = false;
                                        this.msgGeral = this.translateService.instant('pages.login.account_already_exist_with_email')
                                    } else {
                                        this.activeLoader = false;
                                        this.msgGeral = this.translateService.instant('pages.login.geral_error_register');
                                    }
                                })
                            } else {
                                this.activeLoader = false;
                                this.msgGeral = this.translateService.instant('pages.login.term_check_required');
                            }
                        } else {
                            this.activeLoader = false;
                            this.msgGeral = this.translateService.instant('pages.login.invalid_pass');
                        }
                    } else {
                        this.msgPassword = this.translateService.instant('pages.login.invalid_pass');
                    }
                } else {
                    this.msgEmail = this.translateService.instant('pages.login.invalid_email');
                }
            } else {
                this.msgName = this.translateService.instant('pages.login.invalid_email');
            }
        } else {
            this.msgName = this.translateService.instant('pages.login.invalid_name');
        }
    }

    async openTermsOfUseAndPrivacy(type) {
        const modal = await this.modalCtrl.create({
            component: TermsNPrivacyComponent,
            componentProps: {
                eventId: this.eventId,
                type: type
            }
        });
        return await modal.present();
    }

    firstTime: boolean = true;
    close() {
        if (this.firstTime) {
            this.firstTime = false;
            this.splashScreen.show();
            this.modalCtrl.dismiss('cancel');
            window.location.reload();
            this.splashScreen.hide();
        }
        // this.modalCtrl.getTop().then((a) => {
        //   if (a !== undefined) this.modalCtrl.dismiss('cancel');
        // })
        //   .catch((e) => {
        //     console.log(e);
        //   })
    }

}

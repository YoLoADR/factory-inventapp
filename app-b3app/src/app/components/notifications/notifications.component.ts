import { Component, OnInit } from '@angular/core';
import { ModalController, Events, NavParams } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { TypeUser } from 'src/app/models/type-user';
import { TranslateService } from '@ngx-translate/core';
import { NotificationDateService } from 'src/app/providers/date/notification-date.service';
import { DaoGeralService } from 'src/app/providers/db/dao-geral.service';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    eventId: string = null;

    viewAllNotifications: boolean = true;
    viewNotificationDetail: boolean = false;
    notifications: Array<any> = [];
    notificationDetail: any = null;

    refNotifications: any = null;
    notificationsCollection: AngularFirestoreCollection<any> = null;
    dateNow;
    languageUser: string = null;
    userType: number;
    constructor(
        public global: GlobalService,
        private modalCtrl: ModalController,
        private afs: AngularFirestore,
        private events: Events,
        private navParams: NavParams,
        private translateService: TranslateService,
        private notificationDate: NotificationDateService,
        private daoGeral: DaoGeralService
    ) {
        this.eventId = this.navParams.get('eventId');
        this.loadColors();
        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
        if (this.global.language == null) {
            this.languageUser = this.global.event.language;
        } else {
            this.languageUser = this.global.language;
        }
    }

    ngOnInit() {
        this.startAllNotifications();
    }

    startAllNotifications() {
        this.dateNow = this.notificationDate.getTimeStampFromDateNow(new Date(), this.global.event.timezone);
        this.notificationsCollection = this.afs.collection('events').doc(this.eventId).collection('notifications', ref => ref.orderBy('delivery_date', 'desc').where('delivery_date', '<=', this.dateNow));
        this.refNotifications = this.notificationsCollection.valueChanges().subscribe((data: any) => {

            if (data.length <= this.notifications.length || data.length >= this.notifications.length) {
                this.notifications = [];
                if (this.global.user !== null) {
                    if (this.global.user.type >= TypeUser.SUPERGOD && this.global.user.type <= TypeUser.EMPLOYEE) {
                        data.forEach(notification => {
                            if (notification.delivery_date > this.global.userNotificationTime) {
                                notification.header = this.translateService.instant('pages.notifications.not_read');
                            } else if (notification.delivery_date <= this.global.userNotificationTime) {
                                notification.header = this.translateService.instant('pages.notifications.read');
                            }
                            notification.userLastAccess = this.global.userNotificationTime;
                            // notification.delivery_date = this.date.convertTimestampToDateWithoutZone(notification.delivery_date);
                            this.notifications.push(this.instantiateNotification(notification));
                        });
                    } else {
                        data.forEach(notification => {
                            if (notification.send_to == 'all') {
                                if (notification.delivery_date > this.global.userNotificationTime) {
                                    notification.header = this.translateService.instant('pages.notifications.not_read');
                                } else if (notification.delivery_date <= this.global.userNotificationTime) {
                                    notification.header = this.translateService.instant('pages.notifications.read');
                                }
                                notification.userLastAccess = this.global.userNotificationTime;
                                // notification.delivery_date = this.date.convertTimestampToDateWithoutZone(notification.delivery_date);
                                this.notifications.push(this.instantiateNotification(notification));
                            } else {
                                /** verify group and verifiy if user have allowed view group
                                 * case have, display notification, else not display
                                */
                                for (let index in notification.groups_ids) {
                                    const pos = this.global.groupsAttendees.map(function (e) { return e.uid; }).indexOf(notification.groups_ids[index]);
                                    if (pos >= 0) {
                                        if (notification.delivery_date > this.global.userNotificationTime) {
                                            notification.header = this.translateService.instant('pages.notifications.not_read');
                                        } else if (notification.delivery_date <= this.global.userNotificationTime) {
                                            notification.header = this.translateService.instant('pages.notifications.read');
                                        }
                                        notification.userLastAccess = this.global.userNotificationTime;
                                        // notification.delivery_date = this.date.convertTimestampToDateWithoutZone(notification.delivery_date);
                                        this.notifications.push(this.instantiateNotification(notification));
                                        break;
                                    }
                                }
                            }
                        });
                    }
                } else {
                    let noUserNotificationDate = localStorage.getItem('notificationTime');
                    data.forEach(notification => {
                        if (notification.send_to == 'all') {
                            if (notification.delivery_date > noUserNotificationDate || (noUserNotificationDate == null || noUserNotificationDate == undefined)) {
                                notification.header = this.translateService.instant('pages.notifications.not_read');
                            } else if (notification.delivery_date <= noUserNotificationDate) {
                                notification.header = this.translateService.instant('pages.notifications.read');
                            }
                            notification.userLastAccess = noUserNotificationDate;
                            // notification.delivery_date = this.date.convertTimestampToDateWithoutZone(notification.delivery_date);
                            this.notifications.push(this.instantiateNotification(notification));
                        }
                    });
                }
            }
        });
    }

    instantiateNotification(notification) {
        notification['principal_title'] = "";
        notification['principal_msg'] = "";

        switch (this.languageUser) {
            case 'pt_BR':
                notification['principal_title'] = notification.headings['pt'];
                break;
            case 'en_US':
                notification['principal_title'] = notification.headings['en'];
                break;
            case 'es_ES':
                notification['principal_title'] = notification.headings['es'];
                break;
            case 'fr_FR':
                notification['principal_title'] = notification.headings['fr'];
                break;
            case 'de_DE':
                notification['principal_title'] = notification.headings['de'];
                break;
        }

        if (notification['principal_title'] == "") {
            notification['principal_title'] = this.getNotificationTitle(notification.headings);
        }

        switch (this.languageUser) {
            case 'pt_BR':
                notification['principal_msg'] = notification.contents['pt'];
                break;
            case 'en_US':
                notification['principal_msg'] = notification.contents['en'];
                break;
            case 'es_ES':
                notification['principal_msg'] = notification.contents['es'];
                break;
            case 'fr_FR':
                notification['principal_msg'] = notification.contents['fr'];
                break;
            case 'de_DE':
                notification['principal_msg'] = notification.contents['de'];
                break;
        }


        if (notification['principal_msg'] == "") {
            notification['principal_msg'] = this.getNotificationMsg(notification.contents);
        }
        return notification;
    }

    getNotificationTitle(headings) {
        for (let aux in headings) {
            if (headings[aux] !== '') {
                return headings[aux];
            }
        }
    }

    getNotificationMsg(msgs) {
        for (let aux in msgs) {
            if (msgs[aux] !== '') {
                return msgs[aux];
            }
        }
    }

    notificationHeader(notification, index, notifications) {
        if (index == 0) {
            return notification.header;
        } else if (index != 0 && notification.header != notifications[index - 1].header) {
            return notification.header;
        }
    }

    notificationOpenDetail(notification) {
        this.notificationDetail = notification;
        this.viewAllNotifications = false;
        this.viewNotificationDetail = true;
    }

    backAllNotifications() {
        this.viewNotificationDetail = false;
        this.viewAllNotifications = true;
        this.notificationDetail = null;
    }

    close() {
        this.unsubscribe();
        this.modalCtrl.dismiss();
    }

    unsubscribe() {
        this.refNotifications.unsubscribe();
        if (this.global.user !== null && this.global.userId !== null) this.daoGeral.updateUserGeneral({ notification_last_time: this.dateNow }, this.global.userId, this.eventId, this.global.user.type);
        if (this.global.user == null) localStorage.setItem('notificationTime', this.dateNow);
        this.global.updateNotificationBadge(0, 'notify');
        this.events.publish('clearNotificationBadge');
        this.events.publish('updateChatBadge');
        this.events.publish('clearBadgePropertie');
        this.backAllNotifications();
    }

    ionViewWillLeave() {
        this.unsubscribe();
    }
}

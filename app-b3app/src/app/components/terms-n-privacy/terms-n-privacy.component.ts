import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, NavParams, Events } from '@ionic/angular';
import { DaoEventsService } from 'src/app/providers/db/dao-events.service';
import { GlobalService } from 'src/app/shared/services';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-terms-n-privacy',
    templateUrl: './terms-n-privacy.component.html',
    styleUrls: ['./terms-n-privacy.component.scss']
})
export class TermsNPrivacyComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    term;
    privacy;
    eventId: string;
    showAll: boolean = true;
    showTerms: boolean = false;
    showPrivacy: boolean = false;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;
    type: string = null;
    constructor(
        private navParams: NavParams,
        private modalCtrl: ModalController,
        private dbEvents: DaoEventsService,
        public global: GlobalService,
        private events: Events
    ) {
        this.eventId = this.navParams.get('eventId');
        if (this.navParams.get('type')) {
            this.type = this.navParams.get('type');
            if (this.navParams.get('type') == 'termsOfUse') {
                this.showAll = false;
                this.showPrivacy = false;
                this.showTerms = true;
            } else {
                this.showAll = false;
                this.showTerms = false;
                this.showPrivacy = true;
            }
        }
        this.loadColors();
        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }


    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        this.subscriptions.push(this.dbEvents.getEvent(this.eventId).subscribe((event) => {
            this.term = event['terms_of_use'];
            this.privacy = event['privacy'];
        }));
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    close() {
        this.modalCtrl.dismiss();
        this.type = null;
        this.eventId = null;
    }

}

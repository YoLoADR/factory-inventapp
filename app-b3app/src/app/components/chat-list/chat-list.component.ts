import { Component, OnInit, NgZone } from '@angular/core';
import { Events, ModalController, NavParams } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { GroupDiscussionsService } from 'src/app/shared/services/group-discussions/group-discussions.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-chat-list',
    templateUrl: './chat-list.component.html',
    styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {
    eventId: string = null;
    userId: string = null;
    userPrimaryId: string = null;
    userSecondaryId: string = null;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;

    attendees: Array<any> = [];
    allAttendees: Array<any> = [];
    loader: boolean = true;
    searchOpen: boolean = false;
    constructor(
        private events: Events,
        public global: GlobalService,
        private modalCtrl: ModalController,
        private router: Router,
        private navParams: NavParams,
        private dbAttendees: DaoAttendeesService,
        private gdService: GroupDiscussionsService,
        private zone: NgZone
    ) {
        this.eventId = this.navParams.get('eventId');
        this.userId = this.navParams.get('userId');
        this.loadColors();
        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    ngOnInit() {
        this.getAvailablesAttendees();
    }

    getAvailablesAttendees() {
        this.dbAttendees.getAvailableAttendeesChat(
            this.eventId,
            (attendees) => {
                this.zone.run(() => {
                    this.attendees = [];
                    this.attendees = attendees;
                    this.allAttendees = attendees;
                    this.loader = false;
                });
            }
        );
    }

    attendeeHeader(attendee, i, attendees) {
        if (i == 0) {
            return attendee.letter;
        } else if (i != 0 && attendee.letter != attendees[i - 1].letter) {
            return attendee.letter;
        }
    }

    searchBar(ev) {
        if (ev.target.value.length >= 1) {
            let value = ev.target.value.toLowerCase();
            this.attendees = [];
            this.allAttendees.filter((item) => {
                if (item.name.toLowerCase().includes(value)) {
                    this.attendees.push(item);
                }
            });
        } else {
            this.attendees = this.allAttendees;
        }
    }

    async close(uid?: string) {
        if (uid) {
            try {
                const chatId = await this.gdService.getOrCreateChat(
                    this.eventId,
                    this.global.userId,
                    uid
                );
                console.log(chatId);
                this.router.navigate([`/event/${this.eventId}/chat/${chatId}`]);
            } catch (e) {
                console.log(e);
            }
        }

        this.modalCtrl.dismiss();
    }
}

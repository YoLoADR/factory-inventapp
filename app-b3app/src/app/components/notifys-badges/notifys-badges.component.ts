import { Component, OnInit, NgZone, Input, OnDestroy } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { Events, ModalController } from '@ionic/angular';
import { NotificationDateService } from 'src/app/providers/date/notification-date.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { DaoModulesService } from 'src/app/providers/db/dao-modules.service';
import { TypeUser } from 'src/app/models/type-user';
import { NotificationsComponent } from '../notifications/notifications.component';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-notifys-badges',
    templateUrl: './notifys-badges.component.html',
    styleUrls: ['./notifys-badges.component.scss']
})
export class NotifysBadgesComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    @Input() eventId: string = null;
    allowReloadNotifications: boolean = true;
    allowChat: boolean = false;
    refNotifications: any = null;
    notificationsCollection: AngularFirestoreCollection<any> = null;
    notificationBadge: number = 0;
    refChat: any = null;
    chatCollection: AngularFirestoreCollection<any> = null;
    chatBadge: number = 0;
    refMsgsChat: any = null;
    msgChatCollection: AngularFirestoreCollection<any> = null;
    interval: any;
    intervalUserType: any;
    menu_text_color: string = null;
    constructor(
        public global: GlobalService,
        private zone: NgZone,
        private notificationDate: NotificationDateService,
        private afs: AngularFirestore,
        private daoModules: DaoModulesService,
        private modalCtrl: ModalController,
        public events: Events
    ) {

    }

    ngOnInit() {
        this.global.loadService(() => { });
        this.events.subscribe('updateChatBadge', () => {
            this.zone.run(() => {
                let badge = this.global.notificationBadge;
                badge--;
                this.global.updateNotificationBadge(badge, 'chat');
            });
        });

        this.events.subscribe('clearBadgePropertie', () => {
            this.zone.run(() => { this.notificationBadge = 0; });
        })

        this.events.subscribe('reloadNotification', () => {
            if (this.allowReloadNotifications) {
                this.allowReloadNotifications = false;
                this.notificationsUpdate();
                setTimeout(() => {
                    this.allowReloadNotifications = true;
                }, 5000);
            }
        });

        this.interval = setInterval(() => {
            if (this.global.userModuleId !== null && this.global.userModuleId !== undefined) {
                clearInterval(this.interval);
                this.notificationsUpdate();
                this.subscriptions.push(this.daoModules.getModule(this.global.userModuleId).subscribe((moduleRes) => {
                    // this.intervalUserType = setTimeout(() => {
                    if (this.global.user.type) {
                        clearInterval(this.intervalUserType);
                        if (this.global.user.type == TypeUser.ATTENDEE) {
                            this.zone.run(() => {
                                this.allowChat = moduleRes['allow_chat'];
                                if (this.allowChat == true) {
                                    this.chatUpdate();
                                }
                            })
                        }
                    }
                    // }, 1500);
                }));
            }
        }, 1000);
        this.menu_text_color = this.global.eventColors.menu_text_color;
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    dateNow;
    notificationsUpdate() {
        this.dateNow = this.notificationDate.getTimeStampFromDateNow(new Date(), this.global.event.timezone);
        this.notificationsCollection = this.afs.collection('events').doc(this.eventId).collection('notifications', ref => ref.orderBy('delivery_date', 'desc').where('delivery_date', '<=', this.dateNow));
        this.refNotifications = this.notificationsCollection.valueChanges().subscribe((data: any) => {
            this.notificationBadge = 0;
            this.getBadgeValue(data).then((response: number) => {
                this.zone.run(() => {
                    this.notificationBadge = response;
                });
            })
            if (this.notificationBadge >= 1) {
                this.global.updateNotificationBadge(1, 'notify');
            }

        });
    }

    getBadgeValue(data) {
        return new Promise((resolve) => {
            let auxBadge = [];
            let cont = 0;
            data.forEach(async notification => {
                if (this.global.user !== null) {
                    if (notification.send_to == 'all') {
                        if (this.global.userNotificationTime !== undefined) {
                            if (notification.delivery_date > this.global.userNotificationTime) {
                                auxBadge.push(notification);
                            }
                        } else {
                            auxBadge.push(notification);
                        }
                        cont++;
                    } else {
                        await this.global.loadGroups(this.global.userId, this.global.userType, this.global.eventId);
                        for (let index in notification.groups_ids) {
                            const pos = this.global.groupsAttendees.map(function (e) { return e.uid; }).indexOf(notification.groups_ids[index]);
                            if (pos >= 0) {
                                if (this.global.userNotificationTime !== undefined) {
                                    if (notification.delivery_date > this.global.userNotificationTime) {
                                        auxBadge.push(notification);
                                    }
                                } else {
                                    auxBadge.push(notification);
                                }
                                cont++;
                            } else {
                                cont++;
                            }
                        }
                    }
                } else {
                    let noUserNotificationDate = localStorage.getItem('notificationTime');
                    if (notification.send_to == 'all') {
                        if (noUserNotificationDate !== undefined) {
                            if (notification.delivery_date > noUserNotificationDate) {
                                auxBadge.push(notification);
                            }
                        } else {
                            auxBadge.push(notification);
                        }
                        cont++;
                    } else {
                        cont++;
                    }
                }
                if (cont == data.length - 1) {
                    resolve(auxBadge.length);
                }
            });
        })
    }

    chatUpdate() {
        this.chatCollection = this.afs.collection('events').doc(this.eventId).collection('chats', ref => ref.where(`members.${this.global.userId}.uid`, '==', this.global.userId));
        this.refChat = this.chatCollection.valueChanges().subscribe((data: any) => {
            this.zone.run(() => {
                let auxBadgeChat = [];
                data.forEach(element => {
                    let last_access = element['last_access'][this.global.userId];
                    let uid = element['uid'];
                    this.msgChatCollection = this.afs.collection('events').doc(this.eventId).collection('chats').doc(uid).collection('messages', ref => ref.orderBy('send_at', 'desc'));
                    this.refMsgsChat = this.msgChatCollection.valueChanges().subscribe((dataT: any) => {
                        dataT.forEach(elMsg => {
                            let msg = elMsg;
                            if (msg.send_at > last_access) {
                                if (msg.from_user !== this.global.userId) {
                                    auxBadgeChat.push(element);
                                }
                            }
                        });
                        this.chatBadge = this.removeChatBadgeDouble(auxBadgeChat);
                        if (this.chatBadge >= 1 && this.global.chatBadge < 1) {
                            this.global.updateNotificationBadge(1, 'chat');
                        }
                    });
                });
            });
        });
    }

    removeChatBadgeDouble(array) {
        let badgeArray = array.filter(function (el, i) {
            return array.indexOf(el) == i;
        });

        return badgeArray.length;
    }

    async openNotifications() {
        const modal = await this.modalCtrl.create({
            component: NotificationsComponent,
            componentProps: {
                'eventId': this.eventId
            }
        });
        return await modal.present();
    }

}
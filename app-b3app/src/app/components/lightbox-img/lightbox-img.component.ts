import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import  { FileService } from 'src/app/providers/file/file-service';

@Component({
  selector: 'app-lightbox-img',
  templateUrl: './lightbox-img.component.html',
  styleUrls: ['./lightbox-img.component.scss']
})
export class LightboxImgComponent implements OnInit {

  @ViewChild('slider', { read: ElementRef, static: false }) slider: ElementRef;
  img: any;
  type = '';
  name = '';
  allowDownload = false;
  zoomer: boolean = false;
  downloading = false;
  sliderOpts = {
    zoom: {
      maxRatio: 5
    }
  };

  constructor(
    private navParams: NavParams,
    private modalController: ModalController,
    private fileService: FileService
  ) { }

  ngOnInit() {
    this.img = this.navParams.get('img');
    this.allowDownload = this.navParams.get('allowDownload');
    this.type = (this.navParams.get('type') || 'jpg');
    this.name = (this.navParams.get('name') || '');
  }

  zoom(zoomIn: boolean) {
    this.zoomer = zoomIn;
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }

  async download(){
    if (this.downloading){
      return;
    }
    
    try {
      this.downloading = true;

      await this.fileService.download(this.img, this.type, {
        directory: 'app_pictures',
        name: this.name
      });

      this.downloading = false;
    }
    catch (e){
      alert('Error: ' + JSON.stringify(e));
      console.log(e);
      this.downloading = false;
    }
  }

  close() {
    this.modalController.dismiss();
  }

}

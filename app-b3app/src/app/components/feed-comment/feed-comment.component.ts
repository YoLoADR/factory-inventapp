import { Component, OnInit } from '@angular/core';
import { NavParams, Events, ModalController, PopoverController } from '@ionic/angular';
import { GlobalService } from 'src/app/shared/services';
import { DaoFeedNewsService } from 'src/app/providers/db/dao-feed-news.service';
import { Post } from '../../models/ceu-post'
import { DateTime } from 'luxon';
import { FeedMoreOptionsComponent } from '../feed-more-options/feed-more-options.component';
import { LightboxImgComponent } from '../lightbox-img/lightbox-img.component';

@Component({
    selector: 'app-feed-comment',
    templateUrl: './feed-comment.component.html',
    styleUrls: ['./feed-comment.component.scss']
})


export class FeedCommentComponent implements OnInit {
    eventId: string;
    moduleId: string;
    postId: string;
    userLoggedId: string

    isComment: boolean
    isSocial_sharing: boolean

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;

    post = null
    comments = []
    textArea = ""

    constructor(
        private navParams: NavParams,
        private events: Events,
        public global: GlobalService,
        private modalCtrl: ModalController,
        private dbFeed: DaoFeedNewsService,
        private popoverController: PopoverController
    ) {
        this.eventId = this.navParams.get('eventId');
        this.moduleId = this.navParams.get('moduleId');
        this.postId = this.navParams.get('postId');
        this.userLoggedId = this.navParams.get('userLoggedId')
        this.isComment = this.navParams.get('isComment')

        this.loadColors();

        this.events.subscribe('loadColors', () => {
            this.loadColors();
        });
    }

    loadColors() {
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bg_content_color = this.global.eventColors.bg_content_color;
        this.bg_general_color = this.global.eventColors.bg_general_color;
    }

    async ngOnInit() {
        this.getPost()
        this.getModule()
    }

    getPost() {
        this.dbFeed.getPost(this.moduleId, this.postId, (post) => {
            if (typeof post !== 'undefined') {
                this.post = post
                // comments
                const aux = []

                for (let uid in this.post.comments) {
                    aux.push(this.post.comments[uid])
                }

                // sorting by date
                this.comments = aux.sort(function (a, b) {
                    return a.date < b.date ? -1 : a.date > b.date ? 1 : 0;
                });
            }
        })
    }

    getModule() {
        this.dbFeed.moduleNewsFeed(this.moduleId, (module) => {
            this.isComment = module.comment
            this.isSocial_sharing = module.social_sharing
        })
    }

    // verifies if the user liked the post
    verifiesLikedPost(post) {
        return typeof post.likes[this.global.userId] !== 'undefined'
    }

    // account the number of likes in the post.
    contLikes(likes) {
        let cont = 0

        for (let i in likes) {
            cont++
        }

        return cont
    }

    // add the like in post
    addLike(post) {
        if (this.global.userLogged()) {
            post.likes[this.userLoggedId] = true
            this.dbFeed.updatePost(post, this.eventId);
        }
    }

    // remove the like in post
    removeLike(post) {
        if (this.global.userLogged()) {
            delete post.likes[this.userLoggedId]
            this.dbFeed.updatePost(post, this.eventId);
        }
    }

    deletePost(post) {
        this.dbFeed.deletePost(post, this.eventId);
        this.close()
    }

    createComment() {
        if (this.global.userLogged()) {
            if (this.textArea.length > 0) {
                const commentId = this.dbFeed.createIdPost()

                this.post.comments[commentId] = {
                    uid: commentId,
                    creator: {
                        uid: this.global.userId,
                        name: this.global.displayName,
                        img: this.global.photoUrl
                    },
                    text: this.textArea,
                    date: DateTime.local().valueOf() / 1000
                }

                this.dbFeed.updatePost(this.post, this.eventId);
                this.textArea = ""
            }
        }
    }

    deleteComment(comment) {
        delete this.post.comments[comment.uid]
        this.dbFeed.updatePost(this.post, this.eventId);
    }

    close() {
        this.modalCtrl.dismiss();
    }

    openImage(url: string) {
        this.modalCtrl.create({
            component: LightboxImgComponent,
            componentProps: {
                img: url
            }
        }).then(modal => {
            modal.present();
        });
    }

    async moreOptions(post: Post) {
        const popover = await this.popoverController.create({
            component: FeedMoreOptionsComponent,
            componentProps: {
                post: post,
                eventId: this.eventId,
                isModal: true
            },
            translucent: true,
            animated: true
        });
        return await popover.present();
    }

}

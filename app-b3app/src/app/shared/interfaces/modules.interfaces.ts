export interface INameModule {
    DeDE: string;
    EnUS: string;
    EsES: string;
    FrFR: string;
    PtBR: string;
}

export interface IModule {
    uid: string;
    name: INameModule;
    eventId: string;
    icon: string;
    iconFamily: string;
    viewBackOffice: string;
    viewApp: string;
    habilitedBackOffice: boolean;
    habilitedApp: boolean;
    order: number;
    remove: boolean;
    type: number;
    total_access: number;
}
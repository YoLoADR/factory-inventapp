import { Survey } from "src/app/models/survey";
import { AskQuestion } from "src/app/models/ask-question";
import { Quiz } from "src/app/models/quiz";
import { sessionFeedback } from "src/app/models/sessionFeedback";
import { GroupDiscussion } from "src/app/models/group-discussion";

export interface IInteractivityQuestions {
    questionModule: any,
    questions: AskQuestion[]
}

export interface IInteractivitySurveys {
    surveyModule: any,
    surveys: Survey[]
}

export interface IInteractivityQuizs {
    quizModule: any,
    quizs: Quiz[]
}

export interface IInteractivitySpeakers {
    speakers: any[]
}

export interface IInteractivityDocuments {
    documents: any[]
}

export interface IInteractivityFeedbacks {
    feedbacks: sessionFeedback[],
    feedbackModule: any,
}

export interface IInteractivityDiscussionsGroups {
    discussionsGroups: GroupDiscussion[]
}

export interface IInteractivityState {
    questionsInteractivity: IInteractivityQuestions,
    surveysInteractivity: IInteractivitySurveys,
    quizsInteractivity: IInteractivityQuizs,
    speakersInteractivity: IInteractivitySpeakers,
    documentsInteractivity: IInteractivityDocuments,
    feedbacksInteractivity: IInteractivityFeedbacks,
    discussionsGroupsInteractivity: IInteractivityDiscussionsGroups
}
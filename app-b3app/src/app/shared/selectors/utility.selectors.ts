import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IUtilityState } from '../interfaces/utility.interfaces';

export const getUtilityState = createFeatureSelector<IUtilityState>('utility');

export const getConnectionStatus = createSelector(getUtilityState, (state: IUtilityState) => state.haveNetworkConnection);
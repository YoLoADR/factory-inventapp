import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IInteractivityState } from '../interfaces/interactivity.interfaces';

export const getInteractivityState = createFeatureSelector<IInteractivityState>('interactivity');

export const getQuestions = createSelector(getInteractivityState, (state: IInteractivityState) => state.questionsInteractivity);

export const getSurveys = createSelector(getInteractivityState, (state: IInteractivityState) => state.surveysInteractivity);

export const getQuizs = createSelector(getInteractivityState, (state: IInteractivityState) => state.quizsInteractivity);

export const getSpeakers = createSelector(getInteractivityState, (state: IInteractivityState) => state.speakersInteractivity);

export const getDocuments = createSelector(getInteractivityState, (state: IInteractivityState) => state.documentsInteractivity);

export const getFeedbacks = createSelector(getInteractivityState, (state: IInteractivityState) => state.feedbacksInteractivity);

export const getDiscussionsGroups = createSelector(getInteractivityState, (state: IInteractivityState) => state.discussionsGroupsInteractivity);
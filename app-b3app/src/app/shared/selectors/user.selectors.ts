import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IUserState } from '../interfaces/user.interfaces';

export const getUserState = createFeatureSelector<IUserState>('user');

export const getUserData = createSelector(getUserState, (state: IUserState) => state.userData);
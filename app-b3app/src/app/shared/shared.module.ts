import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchFilterPipe } from '../pipes/search-filter/search-filter';
import { SafeHtmlPipe } from '../pipes/safe-html/safe-html';
import { SortListPipe } from '../pipes/sort-list/sort-list';
import { GetNameSession } from '../pipes/get-name-session.pipe';
import { PathComponent } from '../models/path/path-components';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DaoEventsService } from '../providers/db/dao-events.service';
import { RegexProvider } from '../providers/regex/regex.service';
import { FormatDatePipe } from '../pipes/format-date/format-date.pipe';
import { GetNameModule } from '../pipes/get-name-module.pipe';

@NgModule({
    declarations: [
        SearchFilterPipe,
        SafeHtmlPipe,
        SortListPipe,
        GetNameSession,
        PathComponent.notifys_component,
        FormatDatePipe,
        GetNameModule
    ],
    imports: [
        IonicModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        CommonModule,
        TranslateModule.forChild()
    ],
    exports: [
        SearchFilterPipe,
        SafeHtmlPipe,
        SortListPipe,
        GetNameSession,
        PathComponent.notifys_component,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        FormatDatePipe,
        GetNameModule
    ],
    providers: [InAppBrowser, DaoEventsService, RegexProvider]
})
export class SharedModule {}

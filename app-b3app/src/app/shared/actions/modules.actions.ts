import { Action } from '@ngrx/store';
import { IModule } from '../interfaces/modules.interfaces';

export enum ModulesActionsTypes {
    UpdateModules = "[Modules Service] Update list of modules"
}

export class UpdateModules implements Action {
    readonly type = ModulesActionsTypes.UpdateModules;

    constructor(public payload: IModule[]) { }
}

export type ModulesActionsUnion = UpdateModules;
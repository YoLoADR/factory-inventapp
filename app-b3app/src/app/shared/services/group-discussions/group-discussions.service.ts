import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GroupDiscussion } from 'src/app/models/group-discussion';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { first, map, switchMap } from 'rxjs/operators';
import { User } from 'firebase';
import firebase from 'firebase';
import { Participant } from 'src/app/models/participant';
import { GlobalService } from '../global/global.service';

@Injectable({
    providedIn: 'root'
})
export class GroupDiscussionsService {
    groupDiscussionsRef;

    constructor(
        private afs: AngularFirestore,
        private aAuth: AngularFireAuth,
        private SGlobal: GlobalService
    ) {
        this.groupDiscussionsRef = this.afs.collection<GroupDiscussion>(
            'groupDiscussions'
        );
    }

    /**
     * Get discussions groups for specified session
     * @param eventId 
     * @param sessionId 
     */
    getGroupsForEventForSession(eventId: string, sessionId: string): Observable<GroupDiscussion[]> {
        return (
            this.afs.collection<GroupDiscussion>('groupDiscussions', (ref) =>
                ref
                    .where("disabled", "==", false)
                    .where("eventId", "==", eventId))
                .valueChanges()
                .pipe(
                    map((groups) => {
                        return (groups.filter((grp) => {
                            return ((grp.link_sessions == 'all' || grp.sessions.includes(sessionId)) ? true : false);
                        }))
                    })
                )
        )
    }

    groupDiscussions(
        eventId: string,
        moduleId: string
    ): Observable<GroupDiscussion[]> {
        return this.aAuth.user.pipe(
            switchMap((u: firebase.User) =>
                this.afs
                    .collection<GroupDiscussion>('groupDiscussions', (ref) =>
                        ref
                            .where('eventId', '==', eventId)
                            .where('moduleId', '==', moduleId)
                            .where('disabled', '==', false)
                    )
                    .valueChanges({ idField: 'id' })
                    .pipe(
                        map((gps: GroupDiscussion[]) => {
                            return gps.filter((gp: GroupDiscussion) => {
                                const includes = gp.participants
                                    .map((p) => p.uid)
                                    .includes(u.uid);

                                return gp.visibility || includes;
                            });
                        })
                    )
            )
        );
    }

    getUserId(): Promise<string> {
        return this.aAuth.user
            .pipe(
                first(),
                map((u: User) => u.uid)
            )
            .toPromise();
    }

    userId(): Observable<string> {
        return this.aAuth.user.pipe(map((u: User) => u.uid));
    }

    currentUser(): Observable<any> {
        return this.aAuth.user.pipe(
            switchMap((u: User) =>
                this.afs.collection('users').doc(u.uid).valueChanges()
            )
        );
    }

    images(eventId: string, groupId: string) {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(groupId)
            .collection('messages', (ref) =>
                ref.where('message_picture', '>', '')
            )
            .valueChanges();
    }

    messagesAfterLastAccess(
        eventId: string,
        groupId: string,
        timestamp: number
    ) {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(groupId)
            .collection('messages', (ref) =>
                ref.where('send_at', '>', timestamp ? timestamp : 0)
            )
            .valueChanges();
    }

    badgeNotifications(eventId: string, groupId: string): Observable<number> {
        return this.userId().pipe(
            switchMap((u) => {
                return this.chat(eventId, groupId).pipe(
                    switchMap((c) => {
                        return this.messagesAfterLastAccess(
                            eventId,
                            groupId,
                            c.last_access[u]
                        ).pipe(map((m) => m.length));
                    })
                );
            })
        );
    }

    async getOrCreateChat(
        eventId: string,
        userId1: string,
        userId2: string
    ): Promise<string> {
        const chatId = `${userId1}-${userId2}`;
        const doc: any = await this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(`${userId1}-${userId2}`)
            .valueChanges()
            .pipe(first<string>())
            .toPromise();

        if (doc) {
            return chatId;
        }

        await this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .set({
                uid: chatId,
                members: {
                    [userId1]: {
                        uid: userId1
                    },
                    [userId2]: {
                        uid: userId2
                    }
                },
                disabled: false,
                visibility: true
            });

        return chatId;
    }

    currentParticipant(): Observable<Participant> {
        return this.aAuth.user.pipe(
            switchMap((u: User) =>
                this.afs.collection('users').doc(u.uid).valueChanges()
            ),
            map((u: any) => {
                return new Participant(
                    u.uid,
                    u.name,
                    u.email,
                    u.emailRecovery,
                    false,
                    u.photoUrl
                );
            })
        );
    }

    groupDiscussion(groupId: string): Observable<GroupDiscussion> {
        return this.groupDiscussionsRef.doc(groupId).valueChanges();
    }

    module(moduleId: string): Observable<any> {
        return this.afs.collection('modules').doc(moduleId).valueChanges();
    }

    event(eventId: string): Observable<any> {
        return this.afs.collection('events').doc(eventId).valueChanges();
    }

    chat(eventId: string, chatId: string): Observable<any> {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .valueChanges();
    }

    messages(eventId: string, chatId: string) {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .collection('messages', (ref) => ref.orderBy('send_at'))
            .valueChanges();
    }

    chats(eventId: string): Observable<any> {
        return this.aAuth.user.pipe(
            switchMap((u: User) =>
                this.afs
                    .collection('events')
                    .doc(eventId)
                    .collection('chats', ref => ref.where(`members.${this.SGlobal.userId}.uid`, '==', this.SGlobal.userId))
                    .valueChanges()
                    .pipe(
                        map((c) => {
                            return (c.filter((c) => {
                                const includes = Object.keys(
                                    c.members || {}
                                ).includes(u.uid);

                                return (
                                    !!c.lastMessage && !c.disabled &&
                                    (c.visibility || includes)
                                );
                            }))
                        }
                        )
                    )
            )
        );
    }

    getChatMembers(eventId: string, chatId: string): Promise<string[]> {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .valueChanges()
            .pipe(
                first(),
                map((c: any) => Object.keys(c.members || {}))
            )
            .toPromise();
    }

    async updateLastAccess(eventId: string, chatId: string, timestamp: number) {
        const uid = await this.getUserId();

        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .set(
                {
                    last_access: {
                        [uid]: timestamp
                    }
                },
                { merge: true }
            );
    }

    async addMessage(
        eventId: string,
        chatId: string,
        message: any,
        group: boolean
    ) {
        const batch = this.afs.firestore.batch();
        const messageId = this.afs.createId();
        const gdRef = this.groupDiscussionsRef.doc(chatId).ref;
        const chatRef = this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId).ref;
        const messageRef = this.afs
            .collection('events')
            .doc(eventId)
            .collection('chats')
            .doc(chatId)
            .collection('messages')
            .doc(messageId).ref;

        batch.set(messageRef, { ...message });
        batch.update(chatRef, { lastMessage: { ...message } });

        if (group) {
            const activeParticipants = firebase.firestore.FieldValue.arrayUnion(
                message.from_user
            );

            batch.update(gdRef, {
                activeParticipants: activeParticipants,
                lastMessage: { ...message }
            });
        }

        return batch.commit();
    }

    async updateMutedParticipants(groupId: string, uid: string) {
        const gdRef = this.groupDiscussionsRef.doc(groupId).ref;

        return this.afs.firestore.runTransaction(async (t) => {
            const sfDoc = await t.get<GroupDiscussion>(gdRef);
            const muted = sfDoc.data().mutedParticipants || [];

            return t.update(gdRef, {
                mutedParticipants: muted.includes(uid)
                    ? muted.filter((u) => u !== uid)
                    : [...muted, uid]
            });
        });
    }

    async updateLastMessage(
        content: string,
        sender: any,
        eventId: string,
        chatId: string,
        type: string
    ): Promise<void> {
        const gdRef = this.groupDiscussionsRef.doc(chatId);
        const activeParticipants = firebase.firestore.FieldValue.arrayUnion(
            sender.uid
        );

        let newMessage = {
            sender: { ...sender },
            eventId,
            chatId,
            content,
            type,
            sentAt: firebase.firestore.FieldValue.serverTimestamp()
        };

        gdRef.update({
            activeParticipants: activeParticipants,
            lastMessage: { ...newMessage }
        });
    }
}

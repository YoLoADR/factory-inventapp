import { TestBed } from '@angular/core/testing';

import { GroupDiscussionsService } from './group-discussions.service';

describe('GroupDiscussionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupDiscussionsService = TestBed.get(GroupDiscussionsService);
    expect(service).toBeTruthy();
  });
});

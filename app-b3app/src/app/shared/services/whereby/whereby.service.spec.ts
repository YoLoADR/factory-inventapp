import { TestBed } from '@angular/core/testing';

import { WherebyService } from './whereby.service';

describe('WherebyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WherebyService = TestBed.get(WherebyService);
    expect(service).toBeTruthy();
  });
});

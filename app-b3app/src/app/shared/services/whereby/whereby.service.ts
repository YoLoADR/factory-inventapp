import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CeuVisio } from 'src/app/models/ceu-visio';
import { DateTime } from 'luxon';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Platform } from '@ionic/angular';
import { environment } from '../../../../environments/environment';
import { first, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GroupDiscussionsService } from '../group-discussions/group-discussions.service';

@Injectable({
    providedIn: 'root'
})
export class WherebyService {
    headers: HttpHeaders;
    baseApiUrl: string = environment.platform.apiBaseUrl;

    constructor(
        private afs: AngularFirestore,
        private iab: InAppBrowser,
        private svc: SafariViewController,
        private gdService: GroupDiscussionsService,
        private http: HttpClient,
        private platform: Platform
    ) {
        this.headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
    }

    createMeetingOnWhereby(data: any): Promise<HttpResponse<any>> {
        const url = `${this.baseApiUrl}dbWherebyCreateMeeting`;

        return this.http
            .post(url, data, {
                headers: this.headers,
                observe: 'response'
            })
            .toPromise();
    }

    isVisioEnabled(eventId: string, groupId?: string): Observable<boolean> {
        if (groupId) {
            return this.gdService
                .groupDiscussion(groupId)
                .pipe(map((gd) => gd.visio));
        }

        return this.afs
            .collection('events')
            .doc(eventId)
            .valueChanges()
            .pipe(map((e: any) => {
                return (e.allow_visio && e.allow_visio_for_2)
            }));
    }

    getVisio(eventId: string, chatId: string): Promise<string> {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('visios')
            .doc(chatId)
            .valueChanges()
            .pipe(
                first(),
                map((v: CeuVisio) => {
                    const currentTime = DateTime.local().toISO();

                    return ((v && v.endDate > currentTime) ? v.url : null);
                })
            )
            .toPromise();
    }

    async setVisio(eventId: string, visio: any): Promise<void> {
        return this.afs
            .collection('events')
            .doc(eventId)
            .collection('visios')
            .doc(visio.uid)
            .set(visio);
    }

    visioAvailableOnMobile(): boolean {
        return (this.isIOS() || this.isAndroid());
    }

    isIOS() {
        return this.platform.is('ios');
    }

    isAndroid() {
        return this.platform.is('android');
    }

    visioAvailableOnSF(): Promise<boolean> {
        return this.svc.isAvailable();
    }

    openInAppBrowserVisio(url: string): Observable<any> {
        const browser = this.iab.create(url, '_system');

        return browser.on('loadstart');
    }

    openSFVisio(url: string): Observable<any> {
        return this.svc.show({
            url,
            hidden: true,
            animated: true,
            transition: 'curl',
            enterReaderModeIfAvailable: false
        });
    }

    openDesktopVisio(url: string): void {
        window.open(url, '_system');
    }

    analyticsNewAccessToRoom(eventId: string, userId: string) {
        return this.afs
            .collection('analytics')
            .doc(eventId)
            .collection('total-visio-access')
            .add({
                userId: userId,
                date: DateTime.local().toISO()
            });
    }

    analyticsNewRoom(eventId: string, userIds: string[]) {
        return this.afs
            .collection('analytics')
            .doc(eventId)
            .collection('total-visio')
            .add({
                userIds: userIds,
                date: DateTime.local().toISO()
            });
    }
}

import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { ConnectionService } from 'ng-connection-service';
import { IUtilityState } from '../../interfaces/utility.interfaces';
import { Store } from '@ngrx/store';
import { HaveNetworkConnection } from '../../actions/utility.actions';

@Injectable({
    providedIn: 'root'
})
export class UtilityService {

    constructor(
        private toastCtrl: ToastController,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private connection: ConnectionService,
        private store: Store<IUtilityState>
    ) { }

    /**
     * Present a toast
     * @param message 
     * @param duration 
     * @param position 
     * @param closeButton 
     * @param color 
     */
    async presentToast(message, duration, position, closeButton, color) {
        let toast = await this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position,
            showCloseButton: closeButton,
            color: color
        });

        toast.present();
    }

    /**
     * Present an alert
     * @param title 
     * @param message 
     * @param buttons 
     * @param inputs 
     */
    async presentAlert(title, message, buttons = [], inputs = []) {
        let alert = await this.alertCtrl.create({
            header: title,
            message: message,
            buttons: buttons,
            inputs: inputs
        });

        return (alert);
    }

    /**
     * Present loading
     * @param message 
     * @param duration 
     * @param spinner 
     */
    async presentLoading(message, duration, spinner) {
        let loader = await this.loadingCtrl.create({
            message: message,
            duration: duration,
            spinner: spinner,
            cssClass: 'spinner-css-class'
        });

        return (loader);
    }

    /**
     * Convert a lang format
     * @param lang 
     */
    convertLangFormat(lang) {
        let formatedLang;
        switch (lang) {
            case 'pt_BR': {
                formatedLang = 'PtBR'
                break;
            }
            case 'en_US': {
                formatedLang = 'EnUS';
                break;
            }
            case 'es_ES': {
                formatedLang = 'EsES';
                break;
            }
            case 'fr_FR': {
                formatedLang = 'FrFR';
                break;
            }
            case 'de_DE': {
                formatedLang = 'DeDE';
                break;
            }
        }
        return (formatedLang);
    }

    /**
     * Check if index exist in array
     * @param array 
     * @param item 
     */
    checkIndexExists(array, item) {
        return array.map(function (e) { return e.uid; }).indexOf(item.uid);
    }

    /**
     * Check network connection
     */
    manageNetworkStatus() {
        let check = window.navigator.onLine;

        // Initial check
        if (check) {
            this.store.dispatch(new HaveNetworkConnection("ONLINE"));
        } else {
            this.store.dispatch(new HaveNetworkConnection("OFFLINE"));
        }

        this.connection.monitor().subscribe((isConnected) => {
            if (isConnected) {
                this.store.dispatch(new HaveNetworkConnection("ONLINE"));
            } else {
                this.store.dispatch(new HaveNetworkConnection("OFFLINE"));
            }
        });
    }
}

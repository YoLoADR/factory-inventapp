import { Injectable } from '@angular/core';
import { Platform, Events, AlertController, ToastController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { TypeUser } from 'src/app/models/type-user';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import 'rxjs/add/operator/catch';
import { Container } from 'src/app/models/container';
import { TypeLogin } from 'src/app/enums/type-login';
import { AuthService } from 'src/app/providers/authentication/auth.service';
import { DaoGeralService } from 'src/app/providers/db/dao-geral.service';
import { DaoModulesService } from 'src/app/providers/db/dao-modules.service';
import { NameGroup } from 'src/app/enums/name-group';
import { Subject, Subscription, ReplaySubject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { GetUser } from '../../actions/user.actions';
import { take } from 'rxjs/operators';

@Injectable()
export class GlobalService {
    subscriptionUser: Subscription;
    public userAuthStatus = null;
    public previousPage: string = null;
    public displayName: string = null;
    public email: string = null;
    public emailRecovery: string = null;
    public company: string = null;
    public title: string = null;
    public photoUrl: string = null;
    public userType: number;
    public userId: string = null;
    public clientId: string = null;
    public userEditProfile: boolean;
    public eventId: string = null;
    public userModuleId: string = null;
    public language: string = environment.platform.defaultLanguage;
    public userPlatform: string = null;
    public userNotificationTime: number = null;
    public userTotalAccess: number = null;
    public notificationBadge: number = 0;
    public chatBadge: number = 0;

    public modulesMenu: any
    public modulesEvent: any
    public groupsAttendees: any = [];
    public eventColors: any = {
        menu_color: '',
        menu_text_color: '',
        title_color: '',
        text_content_color: '',
        link_color: '',
        bg_content_color: '',
        bg_general_color: ''
    }
    public eventHomePage: string = null;
    public event: any = null;
    public eventTimezone: string = null;
    public user: any = null;
    public userLoaded: boolean = false;
    public userEvents: Array<string> = null;
    public isBrowser: boolean = false;
    public allow_language: boolean = null; //multi language 
    public allow_broadcast: boolean = null;
    public container: Container = null;
    public cantGoBackFromProfile: boolean = false;
    public hamburgerActivated: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        private auth: AuthService,
        private daoGeral: DaoGeralService,
        private platform: Platform,
        private daoModules: DaoModulesService,
        private aFirestore: AngularFirestore,
        private events: Events,
        private alertCtrl: AlertController,
        private translateService: TranslateService,
        private toastController: ToastController,
        private store: Store<AppState>
    ) {
        this.getReferences();
    }

    async loadService(onResolve) {
        this.getReferences();
        this.auth.getUser().subscribe((userFromUser) => {
            this.getActiveEvent();
            if (userFromUser) {
                this.userType = userFromUser['type'];
                this.userEvents = userFromUser['events'];
                this.userId = userFromUser['uid'];
                this.userType = userFromUser['type']
                if (this.eventId) {
                    if (this.subscriptionUser && !this.subscriptionUser.closed) {
                        this.subscriptionUser.unsubscribe();
                    }
                    this.subscriptionUser = this.daoGeral.loadUser(this.userId, this.userType, this.eventId).subscribe((user) => {
                        if (user !== undefined && user !== null) {
                            this.userLoaded = true;
                            this.user = user;

                            if (user['name']) { this.displayName = user['name']; }
                            if (user['email']) { this.email = user['email']; }
                            if (user['emailRecovery']) { this.emailRecovery = user['emailRecovery']; }
                            if (user['photoUrl']) { this.photoUrl = user['photoUrl']; } else { this.photoUrl = '' }
                            if (user['moduleId']) { this.userModuleId = user['moduleId']; }
                            if (user['company']) { this.company = user['company']; }
                            if (user['title']) { this.title = user['title']; }
                            if (user['language']) { this.language = user['language']; }
                            if (user['edited_profile']) { this.userEditProfile = user['edited_profile']; }
                            if (user['notification_last_time']) { this.userNotificationTime = user['notification_last_time']; }
                            if (user['type']) { this.userType = user['type']; }
                            if (user['total_access']) { this.userTotalAccess = user['total_access']; }
                            if (user['eventId']) { this.loadGroups(this.userId, this.userType, user['eventId']); }
                            this.events.publish('globalUserLoaded');
                            if (this.userNotificationTime !== null) { this.events.publish('reloadNotification'); }
                            if (this.userType == TypeUser.EMPLOYEE) this.clientId = user['clientId'];

                            this.store.dispatch(new GetUser(user));
                            onResolve(true);
                        } else {
                            onResolve(false);
                        }
                    });
                } else {
                    onResolve(false);
                }
            } else {
                onResolve(false);
            }
        }, (error) => {
            onResolve((false));
        })
    }

    getReferences() {
        this.userId = localStorage.getItem('userIdentifier');
        this.eventId = localStorage.getItem('eventId');
        this.eventHomePage = localStorage.getItem('homePage');
        this.eventTimezone = localStorage.getItem('timezone');
    }

    async presentAlertController(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 2000
        });
        toast.present();
    }

    updateNotificationBadge(number: number, badgeUpdate: string) {
        if (badgeUpdate == 'chat') {
            this.chatBadge = number;
        } else if (badgeUpdate == 'notify') {
            this.notificationBadge = number;
        }

        if (this.notificationBadge < 0) {
            this.notificationBadge = 0;
        }

        if (this.chatBadge < 0) {
            this.chatBadge = 0;
        }
        this.notificationBadge = this.notificationBadge + this.chatBadge;
        if (this.notificationBadge >= 3) { this.notificationBadge = 2; }
        this.events.publish('menuBadge');
    }

    resetService() {
        this.userId = null;
        this.userType = null;
        this.displayName = null;
        this.email = null;
        this.event = null;
        this.emailRecovery = null;
        this.photoUrl = null;
        this.userModuleId = null;
        this.company = null;
        this.title = null;
        this.language = null;
        this.eventId = null;
        this.userPlatform = null;
        this.allow_language = null

        this.groupsAttendees = []
        this.modulesEvent = []
        this.modulesMenu = []
    }

    getActiveEvent() {
        return new Promise((resolve, reject) => {
            this.eventId = localStorage.getItem('eventId');
            this.eventHomePage = localStorage.getItem('homePage');
            this.eventTimezone = localStorage.getItem('timezone');
            if (this.eventId !== null) {
                this.getEvent(this.eventId, (result) => {
                    resolve(true);
                });
            } else {
                resolve(false);
            }
        })
    }

    getPlatform() {
        let aux = this.platform.platforms().join();
        if (aux.includes("desktop")) {
            this.userPlatform = 'web';
        } else if (aux.includes("hybrid")) {
            this.userPlatform = 'web';
        } else if (aux.includes("android")) {
            this.userPlatform = 'android';
        } else if (aux.includes("ios")) {
            this.userPlatform = 'ios';
        }
    }

    getEvent(eventId, onResolve) {
        // return new Promise((resolve) => {
        let refEvent: any = null;
        let eventDoc: AngularFirestoreDocument<any> = null;
        if (eventId) {
            eventDoc = this.aFirestore.collection('events').doc(eventId);
            refEvent = eventDoc.valueChanges().subscribe((data: any) => {
                if (data !== undefined && data !== null) {
                    this.event = data;
                    this.eventColors = data.colors;
                    localStorage.setItem('homePage', data.homePage);
                    this.eventHomePage = data.homePage;
                    localStorage.setItem('timezone', data.timezone);
                    this.eventTimezone = data.timezone;
                    this.allow_language = data.allow_language

                    if (this.event.allowProfileQR == undefined || this.event.allowProfileQR == null) {
                        this.event.allowProfileQR = true;
                    }

                    if (this.event.allow_broadcast != undefined && this.event.allow_broadcast != null) {
                        this.allow_broadcast = this.event.allow_broadcast;
                    } else {
                        this.allow_broadcast = false;
                    }

                    if (this.event.publicOptions == undefined) {
                        this.event.publicOptions = {
                            google_btn: false,
                            facebook_btn: false,
                            twitter_btn: false,
                            microsoft_btn: false,
                            yahoo_btn: false
                        }
                    } else {
                        this.event.publicOptions = {
                            google_btn: this.event.publicOptions.google_btn ? this.event.publicOptions.google_btn : false,
                            facebook_btn: this.event.publicOptions.facebook_btn ? this.event.publicOptions.facebook_btn : false,
                            twitter_btn: this.event.publicOptions.twitter_btn ? this.event.publicOptions.twitter_btn : false,
                            microsoft_btn: this.event.publicOptions.microsoft_btn ? this.event.publicOptions.microsoft_btn : false,
                            yahoo_btn: this.event.publicOptions.yahoo_btn ? this.event.publicOptions.yahoo_btn : false
                        }
                    }

                    if (!this.userLoaded) {
                        this.language = data.language;
                    }

                    this.events.publish('allowLanguage')
                    this.events.publish('loadColors');

                    if (this.eventColors !== null) onResolve(true);
                } else {
                    onResolve(false);
                }

                // if (this.eventColors !== null) resolve(true);
            }, e => { console.log(e); })
            // })
        }
    }

    // loads the attendeees groups
    loadGroups(attendeeId: string, typeUser: number, eventId: string) {
        return new Promise((resolve) => {
            if (attendeeId && typeUser && eventId) {
                let db = this.aFirestore.firestore;
                let ref;

                if (typeUser == TypeUser.SPEAKER) {
                    ref = db.collection('events').doc(eventId).collection('speakers').doc(attendeeId);
                } else if (typeUser == TypeUser.ATTENDEE) {
                    ref = db.collection('events').doc(eventId).collection('attendees').doc(attendeeId);
                }

                ref.onSnapshot((snapshot) => {
                    this.groupsAttendees = []
                    const attendee = snapshot.data()
                    if (typeof attendee.groups !== 'undefined') {
                        const groups = attendee.groups

                        for (let index in groups) {
                            this.groupsAttendees.push(groups[index])
                        }
                    }

                    resolve(this.groupsAttendees)
                }, (e) => { console.log(e) });
            } else {
                this.groupsAttendees = [];
                resolve(this.groupsAttendees);
            }
        })
    }

    loadUserId() {
        return new Promise((resolve) => {
            this.auth.current().then((userAuth) => {
                if (userAuth != null) {
                    this.userId = userAuth['uid'];
                    resolve(this.userId)
                } else {
                    resolve(null)
                }
            })
                .catch((e) => {
                    console.log(e);
                })
        })
    }

    // loads the type of user.
    loadUserType() {
        return new Promise((resolve: any) => {
            this.auth.current().then((userAuth) => {
                if (userAuth != null) {
                    this.auth.claimsUser(async (claims) => {
                        this.userType = claims['result']['type']
                        resolve(this.userType)
                    })
                } else {
                    resolve(null)
                }
            })
                .catch((e) => {
                    console.log(e);
                })
        })
    }

    loadModulesEvent() {
        return new Promise((resolve) => {
            this.daoModules.getModulesEvent(this.eventId, (modules) => {
                this.modulesEvent = []
                this.modulesEvent = modules
                resolve(this.modulesEvent)
            })
        })
    }

    userLogged() {
        let logged = false;
        if (this.userId !== null) {
            logged = true;
        } else {
            logged = false;
            this.needLoginToUse();
        }
        return logged;
    }

    async needLoginToUse() {
        const alert = await this.alertCtrl.create({
            // header: 'Confirm!',
            message: this.translateService.instant('global.texts.need_login_to_use'),
            buttons: [
                {
                    text: this.translateService.instant('global.buttons.ok'),
                    handler: (blah) => {
                        console.log();
                    }
                }
            ]
        });

        await alert.present();
    }

    public isAppOrBrowser() {
        this.isBrowser =
            document.URL.indexOf('http://localhost:8100') === 0 ||
            document.URL.indexOf('ionic') === 0 ||
            document.URL.indexOf('https://localhost') === 0 ||
            document.URL.indexOf('https://app') === 0 ||
            document.URL.indexOf('http://app') === 0 ||
            document.URL.indexOf('app') === 0 ||
            document.URL.indexOf('event') === 0 ||
            document.URL.indexOf('webapp') === 0 ||
            document.URL.indexOf('web') === 0 ||
            document.URL.indexOf('fair') === 0 ||
            document.URL.indexOf('market') === 0 ||
            document.URL.indexOf('com') === 0 ||
            document.URL.indexOf('br') === 0 ||
            document.URL.indexOf('net') === 0 ||
            document.URL.indexOf('symposium') === 0 ||
            document.URL.indexOf('ch') === 0 ||
            document.URL.indexOf('fr') === 0 ||
            document.URL.indexOf('firebaseapp') === 0 ||
            document.URL.indexOf('web.app') === 0 ||
            document.URL.indexOf(environment.platform.appBaseUrl) === 0;
    }

    // remove o grupo do usuário ao trocar o idioma.
    public removeGroup(lang) {
        switch (lang) {
            case 'de_DE':
                lang = NameGroup.DeDE
                break;
            case 'en_US':
                lang = NameGroup.EnUS
                break;
            case 'es_ES':
                lang = NameGroup.EsES
                break;
            case 'fr_FR':
                lang = NameGroup.FrFR
                break;
            default:
                lang = NameGroup.PtBR
                break;
        }


        const index = this.groupsAttendees.map(function (e) { return e.identifier; }).indexOf(lang);

        if (index > -1) {
            this.groupsAttendees.splice(index, 1)
        }
    }

    getContainerData() {
        this.container = {
            uid: null,
            clientId: null,
            appName: environment.platform.appName,
            loginPage: {
                type: TypeLogin.WITH_EMAIL_CONFIRM,
                eventId: null,
            },
            loginOptions: {
                fbLoginBtn: false,
                gLoginBtn: false,
                registeBtn: false,
                shortcodeBtn: true,
            },
            logo: '../../assets/images/logo.png',
            logoSize: 'invent-logo',
            color: null,
        };

        if (environment.platform.containerId !== '') {
            this.daoGeral.getContainer(environment.platform.containerId, (container: Container) => {
                this.container = container;
                console.log('container global: ', this.container);
            });
        }
    }

}

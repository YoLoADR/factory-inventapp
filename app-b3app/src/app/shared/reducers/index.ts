import {
    ActionReducerMap,
    MetaReducer
} from '@ngrx/store';
import { environment } from 'src/environments/environment';

// Interactivity
import { IInteractivityState } from '../interfaces/interactivity.interfaces';
import { interactivityReducer } from '../reducers/interactivity.reducers';

// Schedules
import { ISchedulesState } from '../interfaces/schedules.interfaces';
import { schedulesReducer } from './schedules.reducers';

// User
import { IUserState } from '../interfaces/user.interfaces';
import { userReducer } from './user.reducers';

// Utility
import { IUtilityState } from '../interfaces/utility.interfaces';
import { utilityReducer } from './utility.reducers';

export interface AppState {
    interactivity: IInteractivityState,
    schedules: ISchedulesState,
    user: IUserState,
    utility: IUtilityState
}

export const reducers: ActionReducerMap<AppState> = {
    interactivity: interactivityReducer,
    schedules: schedulesReducer,
    user: userReducer,
    utility: utilityReducer
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
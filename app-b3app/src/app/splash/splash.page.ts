import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController, NavController, Events } from '@ionic/angular';
import { DaoEventsService } from '../providers/db/dao-events.service';
import { CeuEvent } from '../models/ceu-event';
import { GlobalService } from 'src/app/shared/services';

@Component({
    selector: 'app-splash',
    templateUrl: './splash.page.html',
    styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {
    public shortcode: string = null;
    public event: CeuEvent;

    constructor(
        private route: ActivatedRoute,
        private menuCtrl: MenuController,
        private daoEvent: DaoEventsService,
        private navCtrl: NavController,
        private events: Events,
        public global: GlobalService
    ) {
        this.menuCtrl.enable(false);
        this.route.params.subscribe((params) => {
            this.shortcode = params.shortcode;
            // this.shortcode = this.route.snapshot.params['shortcode'];
        })
    }

    ngOnInit() {
        this.daoEvent.getEventByShortcode(this.shortcode, (event: CeuEvent) => {
            if (event !== null) {
                this.event = event;
                localStorage.setItem('usedLanguage', event.language);
                localStorage.setItem('eventId', event.uid);
                localStorage.setItem('homePage', event.homePage);
                this.global.eventHomePage = event.homePage;
                setTimeout(() => {
                    if (event.visibility == false) {
                        this.navCtrl.navigateRoot([`login/${event.uid}`]);
                        // this.router.navigate([`login/${event.uid}`]);
                    } else {
                        // open event in home page
                        localStorage.setItem('eventId', this.event.uid);
                        this.events.publish('updateScreen');
                        this.events.publish('allowReloadModules');
                        this.navCtrl.navigateRoot([this.event.homePage]);
                        // this.router.navigate([`event/${event.uid}`]);
                    }
                }, 1500);
            } else {
                this.navCtrl.navigateRoot(['/']);
                // this.router.navigate(['/']);
            }
        });
    }

}

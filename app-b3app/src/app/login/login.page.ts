import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { Container } from '../models/container';
import { DaoGeralService } from '../providers/db/dao-geral.service';
import { TypeLogin } from '../enums/type-login';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    loginLogoSize: string = environment.platform.loginLogoClass;
    containerId: string = environment.platform.containerId;
    loader: boolean = true;
    container: Container = {
        uid: null,
        clientId: null,
        appName: environment.platform.appName,
        loginPage: {
            type: TypeLogin.WITH_EMAIL_CONFIRM,
            eventId: null,
        },
        loginOptions: {
            fbLoginBtn: false,
            gLoginBtn: false,
            registeBtn: false,
            shortcodeBtn: true,
        },
        logo: '../../assets/images/logo.png',
        logoSize: 'invent-logo',
        color: null,
    };
    constructor(
        private menu: MenuController,
        private daoGeral: DaoGeralService,
        private navCtrl: NavController
    ) {
        this.menu.enable(false);
    }

    ngOnInit() {
        // case containerId is blank, just end loading
        if (this.containerId == '') {
            this.loader = false;
        } else {
            // case containerId is filled, get container data
            this.daoGeral.getContainer(this.containerId, (container: Container) => {
                if (container && container.loginPage && container.loginPage.type == TypeLogin.PUBLIC_EVENT_HOME) {
                    this.container = container;
                    this.daoGeral.getEvent(container.loginPage.eventId).subscribe((event) => {
                        this.navCtrl.navigateRoot([`/${event.shortcode}`]);
                    }, (error) => {
                        this.loader = false;
                    });
                } else if (container && container.loginPage) {
                    this.container = container;
                    this.loader = false;
                } else {
                    this.loader = false;
                }
            });
        }
    }

}

import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-select-login',
  templateUrl: './select-login.page.html',
  styleUrls: ['./select-login.page.scss'],
})
export class SelectLoginPage implements OnInit {

  constructor(private menuCtrl: MenuController) { }

  ngOnInit() {
    this.menuCtrl.enable(false);
  }

}

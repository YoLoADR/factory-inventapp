import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import {
    AngularFirestoreModule,
    FirestoreSettingsToken
} from '@angular/fire/firestore';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { environment } from '../environments/environment';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Globalization } from '@ionic-native/globalization/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

import { AppComponent } from './app.component';
import { PathComponent } from './models/path/path-components';
import { SharedModule } from './shared/shared.module';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { NotificationDateService } from './providers/date/notification-date.service';
import { DaoModulesService } from './providers/db/dao-modules.service';
import { DateService } from './providers/date/date.service';
import { DaoGeralService } from './providers/db/dao-geral.service';
import { LuxonService } from './providers/luxon/luxon.service';
import { ConnectionService } from 'ng-connection-service';

import { AppRoutingModule } from './app-routing.module';
import { AvatarModule } from 'ngx-avatar';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera/ngx';
import { BambuserService } from './providers/bambuser/bambuser.service';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

// Services imports
import {
    GlobalService,
    UtilityService,
    WherebyService
} from './shared/services';

// Ngrx part
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './shared/reducers';
import { GroupDiscussionsService } from './shared/services/group-discussions/group-discussions.service';

import { ChartsModule } from 'ng2-charts';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        PathComponent.start,
        PathComponent.select_login,
        PathComponent.login,
        PathComponent.loginp,
        PathComponent.admin_events,
        PathComponent.client_events,
        PathComponent.user_events,
        PathComponent.public_events,
        PathComponent.terms_of_use,
        PathComponent.privacy_terms,
        PathComponent.notifications,
        PathComponent.feed_comments,
        PathComponent.chat_list,
        PathComponent.terms_n_privacy,
        PathComponent.public_login,
        PathComponent.public_register,
        PathComponent.feed_more_options,
        PathComponent.lightbox_img,
        PathComponent.login_with_code_confirm,
        PathComponent.login_without_code_confirm,
        PathComponent.login_with_shortcode
    ],
    entryComponents: [
        PathComponent.terms_of_use,
        PathComponent.privacy_terms,
        PathComponent.notifications,
        PathComponent.feed_comments,
        PathComponent.chat_list,
        PathComponent.terms_n_privacy,
        PathComponent.public_login,
        PathComponent.public_register,
        PathComponent.feed_more_options,
        PathComponent.lightbox_img,
        PathComponent.login_with_code_confirm,
        PathComponent.login_without_code_confirm,
        PathComponent.login_with_shortcode
    ],
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        AngularFireFunctionsModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        BrowserModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),

        AppRoutingModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        SharedModule,
        AvatarModule,
        SharedModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true
            }
        }),
        StoreDevtoolsModule.instrument(),
        ChartsModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        GlobalService,
        { provide: FirestoreSettingsToken, useValue: {} },
        Camera,
        InAppBrowser,
        File,
        FileOpener,
        FileTransfer,
        SafariViewController,
        ScreenOrientation,
        DaoModulesService,
        DateService,
        NotificationDateService,
        DaoGeralService,
        LuxonService,
        OneSignal,
        Globalization,
        ConnectionService,
        Keyboard,
        BambuserService,
        GlobalService,
        UtilityService,
        WherebyService,
        GroupDiscussionsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}

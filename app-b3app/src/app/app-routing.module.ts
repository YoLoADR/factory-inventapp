import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PathComponent } from './models/path/path-components';
import { NavController } from '@ionic/angular';

const routes: Routes = [
    // { path: '', component: PathComponent.select_login },
    { path: '', component: PathComponent.login },
    { path: 'login', component: PathComponent.login },
    { path: 'login/:eventId', component: PathComponent.login },
    // SUPERGOD/GOD Events
    { path: 'admin-events', component: PathComponent.admin_events },
    // CLIENT/EMPLOYEE Events
    { path: 'c-events/:clientId', component: PathComponent.client_events },
    { path: 'user-events/:userId', component: PathComponent.user_events },
    // { path: 'public', component: PathComponent.public_events },
    {
        path: 'event/:eventId',
        loadChildren: './modules/modules.module#ModulesModule'
    },
    { path: ':shortcode', component: PathComponent.start },
    { path: 'plogin', component: PathComponent.loginp },
    {
        path: 'documents-module',
        loadChildren: './content/pages/documents-module/documents-module.module#DocumentsModulePageModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule {
    constructor(private navCtrl: NavController) {
        if (
            localStorage.getItem('homePage') &&
            localStorage.getItem('userToken') &&
            localStorage.getItem('userIdentifier')
        ) {
            this.navCtrl.navigateRoot([localStorage.getItem('homePage')]);
        } else if (
            localStorage.getItem('homePage') &&
            localStorage.getItem('eventId') &&
            localStorage.getItem('eventVisible') !== null
        ) {
            let visible = localStorage.getItem('eventVisible') ? true : false;
            if (visible) {
                this.navCtrl.navigateRoot([localStorage.getItem('homePage')]);
            }
        }
    }
}

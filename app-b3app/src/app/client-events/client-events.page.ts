import { Component, OnInit, NgZone } from '@angular/core';
import { DaoEventsService } from '../providers/db/dao-events.service';
import { Events, MenuController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/shared/services';
import { AuthService } from '../providers/authentication/auth.service';
import { LuxonService } from '../providers/luxon/luxon.service';

@Component({
    selector: 'app-client-events',
    templateUrl: './client-events.page.html',
    styleUrls: ['./client-events.page.scss'],
    providers: [DaoEventsService]
})
export class ClientEventsPage implements OnInit {
    events;
    searchOpen = false;
    container: any;
    clientId: string = null;
    loader: boolean = true;

    constructor(
        private daoEvents: DaoEventsService,
        private route: ActivatedRoute,
        public global: GlobalService,
        private eventsA: Events,
        private menuCtrl: MenuController,
        private auth: AuthService,
        private luxon: LuxonService,
        private navCtrl: NavController,
        private zone: NgZone
    ) {
        this.clientId = this.route.snapshot.params['clientId'];
        this.menuCtrl.enable(false);
    }

    ngOnInit() {
        this.loadEvents();
    }

    loadEvents() {
        this.daoEvents.getClientEvents(this.clientId, (data) => {
            // makes the treatment of the startDate of the event.
            for (const event of data['result']) {
                if (typeof event.startDate == 'number') {
                    const sDate = this.luxon.convertTimestampToDate(event.startDate)
                    event.startDate = this.luxon.convertDateToFormatDDMM(sDate)
                }
                if (event.banner == '' || event.banner == null || event.banner == undefined) {
                    event.banner = {
                        uid: '',
                        url: ''
                    }
                }
            }

            this.events = data['result'];
            this.loader = false;
        });
    }

    openEvent(event) {
        this.loader = true;
        this.menuCtrl.enable(true);
        this.global.previousPage = 'container';
        localStorage.setItem('eventId', event.uid);
        localStorage.setItem('homePage', event.homePage);
        this.global.eventHomePage = event.homePage;
        this.global.loadService(() => {
            this.navCtrl.navigateRoot([`${event.homePage}`])
                .then((_) => {
                    this.eventsA.publish('updateScreen');
                    this.loader = false;
                });
        });
    }

    logOut() {
        this.auth.logout();
    }
}

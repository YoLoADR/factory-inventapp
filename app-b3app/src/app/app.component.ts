import { Component, NgZone, Inject, Type } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import {
    Platform,
    Events,
    ModalController,
    MenuController,
    AlertController,
    PickerController,
    NavController
} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { environment } from '../environments/environment';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { DaoModulesService } from './providers/db/dao-modules.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AuthService } from './providers/authentication/auth.service';
import { TypeUser } from './models/type-user';
import { TypeVisionGroup } from './enums/type-vision-group';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { DaoGeralService } from './providers/db/dao-geral.service';
import { PathComponent } from './models/path/path-components';
import {
    AngularFirestore,
    AngularFirestoreCollection
} from '@angular/fire/firestore';
import { NotificationDateService } from './providers/date/notification-date.service';
import { DaoAnalyticsService } from './providers/db/dao-analytics.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { LoginpPage } from './loginp/loginp.page';
// import { ConnectionService } from 'ng-connection-service';
import { TypeModule } from './enums/type-module';
import {
    InAppBrowser,
    InAppBrowserOptions
} from '@ionic-native/in-app-browser/ngx';
import 'rxjs/add/operator/catch';
import { HttpHeaders } from '@angular/common/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Store } from '@ngrx/store';
import { AppState } from './shared/reducers';
import { getConnectionStatus } from './shared/selectors/utility.selectors';
import {
    take,
    mergeMap,
    switchMap,
    tap,
    catchError,
    first
} from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { DaoEventsService } from './providers/db/dao-events.service';
import { getUserData } from './shared/selectors/user.selectors';
import { Title } from '@angular/platform-browser';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { ModulesModule } from './modules/modules.module';
import { TypeVisionModule } from './models/type-vision-module';
import { DaoScheduleService } from './providers/db/dao-schedule.service';
import { DaoSessionFeedbackService } from './providers/db/dao-session-feedback.service';
import { GetScheduleSessions } from './shared/actions/schedules.actions';
import { DaoSpeakersService } from './providers/db/dao-speakers.service';
import { DaoAttendeesService } from './providers/db/dao-attendees.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    authenticated: boolean = false;
    options: InAppBrowserOptions = {};
    showSplashScreenWeb: boolean = false;
    public requestOptions;
    public headers;
    allow_language: boolean = null; //multi language
    displayName: string = '';
    eventId: string = null;
    userId: any = null;
    typeUser: number = null;
    moduleId: string = null;
    photoUrl: string = null;
    // menuModules$: Observable<any[]>;
    menuModules: any[] = [];
    interval: any;
    groupsUser: any;
    allowChat: boolean = false;
    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    contAccess: number = 0;
    refNotifications: any = null;
    notificationsCollection: AngularFirestoreCollection<any> = null;
    notificationBadge: number = 0;
    refChat: any = null;
    chatCollection: AngularFirestoreCollection<any> = null;
    chatBadge: number = 0;
    refMsgsChat: any = null;
    msgChatCollection: AngularFirestoreCollection<any> = null;
    bannerUrl: string = null;
    notificationPlayerIds = null;
    allowPublicRegister: boolean = false;
    allowReloadModules: boolean = true;
    onlyCallOne: boolean = true;

    connectionStatus: string = 'PENDING';
    networkInit: boolean = false;

    resumeAppInterval: any;
    appLoadedBeforeResume: boolean = false;
    totalEvents: number = 0;

    isMobile: boolean = false;
    hamburgerActivated: boolean = false;

    constructor(
        @Inject(DOCUMENT) private _document: HTMLDocument,
        private route: ActivatedRoute,
        public globalization: Globalization,
        public alertCtrl: AlertController,
        private safariViewController: SafariViewController,
        private auth: AuthService,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translateService: TranslateService,
        public global: GlobalService,
        private daoModules: DaoModulesService,
        private router: Router,
        public events: Events,
        private zone: NgZone,
        private oneSignal: OneSignal,
        private daoGeral: DaoGeralService,
        private modalCtrl: ModalController,
        private afs: AngularFirestore,
        private notificationDate: NotificationDateService,
        private dbAnalytics: DaoAnalyticsService,
        private dbGeral: DaoGeralService,
        private luxon: NotificationDateService,
        private menuCtrl: MenuController,
        private pickerCtrl: PickerController,
        private navCtrl: NavController,
        // private connection: ConnectionService,
        private theInAppBrowser: InAppBrowser,
        private screenOrientation: ScreenOrientation,
        private store: Store<AppState>,
        public SUtility: UtilityService,
        private daoEvent: DaoEventsService,
        private title: Title,
        private dbSchedule: DaoScheduleService,
        private daoFeedback: DaoSessionFeedbackService,
        private dbSpeakers: DaoSpeakersService,
        private dbAttendees: DaoAttendeesService
    ) {
        this.showSplashscreen();

        this.setLanguageToUse();
        // initialize app, padrão do ionic para iniciar a plataforma
        this.initializeApp();

        // evento que observa se deve recarregar alguma função, nesse caso a load general
        this.events.subscribe('updateScreen', () => {
            // this.allowReloadModules = true;
            this.showSplashscreen();

            this.loadGeneral();
            if (
                this.router.url.includes('start') ||
                this.router.url.includes('login') ||
                this.router.url.includes('select-login') ||
                this.router.url.includes('admin-events') ||
                this.router.url.includes('c-events') ||
                this.router.url.includes('user-events') ||
                this.router.url.includes('plogin')
            ) {
                this.hideSplashScreen();
            } else {
                this.hideSplashScreen();
            }
        });
        this.events.subscribe('allowReloadModules', () => {
            this.allowReloadModules = true;
        });
        this.events.subscribe('reloadNotification', () => {
            if (this.eventId) this.startNotificationsValues();
        });
        // evento que observa o multi-idioma
        this.events.subscribe('allowLanguage', () => {
            this.allow_language = this.global.allow_language;
        });

        // evento que observa se deve atualizar as badges (chat,notificação)
        this.events.subscribe('updateChatBadge', () => {
            // let badge = this.global.notificationBadge;
            // badge--;
            // this.global.updateNotificationBadge(badge, 'chat');
            let badge = this.global.notificationBadge - 1;
            this.global.notificationBadge = badge;
            this.events.publish('menuBadge');
        });

        // evento que observa se deve atualizar o idioma do aplicativo
        this.events.subscribe('languageUpdate', (language) => {
            this.setLanguageUse(language);
        });

        this.events.subscribe('clearNotificationBadge', () => {
            this.global.updateNotificationBadge(0, 'notify');
        });

        // Subscription on connection status
        this.store.select(getConnectionStatus).subscribe((networkStatus) => {
            if (
                this.connectionStatus == 'OFFLINE' &&
                networkStatus == 'ONLINE' &&
                this.networkInit
            ) {
                this.events.publish('updateScreen');
            }

            if (
                (this.connectionStatus == 'PENDING' &&
                    networkStatus == 'OFFLINE') ||
                this.connectionStatus === 'OFFLINE'
            ) {
                this.connectionStatus = networkStatus;
                if (networkStatus == 'ONLINE') {
                    setTimeout(() => {
                        this.connectionStatus = 'PENDING';
                    }, 5000);
                }
            }

            if (!this.networkInit) {
                this.networkInit = true;
            }
        });
    }

    // connectionStatusNet() {
    //     this.connection.monitor().subscribe((isConnected) => {
    //         if (isConnected) {
    //             this.hideSplashScreen()
    //             this.connectionStatus = true;
    //             this.events.publish('updateScreen');
    //             setTimeout(() => {
    //                 this.connectionStatus = null;
    //             }, 5000);
    //         } else {
    //             this.hideSplashScreen()
    //             this.connectionStatus = false;
    //             setTimeout(() => {
    //                 this.connectionStatus = null;
    //             }, 5000);
    //         }
    //     });
    // }

    setLanguageToUse() {
        let storageLang = localStorage.getItem('usedLanguage');
        if (storageLang !== null) {
            this.setLanguageUse(storageLang);
        } else {
            // se não tem uma língua no storage, obtém o idioma preferencial do aparelho
            if (this.platform.is('cordova')) {
                this.globalization
                    .getPreferredLanguage()
                    .then((lang) => {
                        this.setLanguageUse(this.checkLanguage(lang.value));
                    })
                    .catch((e) => {
                        this.checkLanguage(
                            environment.platform.defaultLanguage
                        );
                    });
            } else {
                this.setLanguageUse(environment.platform.defaultLanguage);
            }
        }
    }

    setIntervalAppResume: any;
    isCordova: boolean = false;

    initializeApp() {
        this.platform.ready().then(() => {
            if (this.platform.is('ios') || this.platform.is('android')) {
                this.screenOrientation.lock(
                    this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY
                );
                this.isMobile = true;
            }
            this.global.isAppOrBrowser();
            this.global.getPlatform();
            this.setLanguageToUse();
            this.loadGeneral();
            if (this.platform.is('mobile')) {
                this.isCordova = true;
                this.statusBar.styleDefault();
                this.setupPush();
            }
            this.platform.pause.subscribe(() => {
                this.global.userLoaded = false;
            });
            this.platform.resume.subscribe(() => {
                this.events.publish('updateScreen');
            });

            this.SUtility.manageNetworkStatus();
            // this.connectionStatusNet();

            this.wakeUpFunctions();
        });
    }

    // configurações iniciais da notificação
    setupPush() {
        // if (!this.global.isBrowser) {
        // Init onesignal with app id and sender id (android)
        this.oneSignal.startInit(
            environment.platform.oneSignalAppId,
            environment.firebase.messagingSenderId
        );
        // if app opened, not display default alert notification
        this.oneSignal.inFocusDisplaying(
            this.oneSignal.OSInFocusDisplayOption.None
        );

        // Notifcation was received in general
        this.oneSignal.handleNotificationReceived().subscribe((data) => {
            let msg = data.payload.body;
            let title = data.payload.title;
            // let additionalData = data.payload.additionalData;
            this.notificationAlert(title, msg);
        });

        // Notification was really clicked/opened
        this.oneSignal.handleNotificationOpened().subscribe((data) => {
            let additionalData = data.notification.payload.additionalData;
            // this.notificationAlert('Notification opened', 'You already read this before', additionalData.task);
        });

        this.oneSignal.endInit();
        // }
    }

    async notificationAlert(title, msg) {
        const alert = await this.alertCtrl.create({
            header: title,
            subHeader: msg,
            buttons: [
                {
                    text: 'OK',
                    handler: () => { }
                }
            ]
        });
        alert.present();
    }

    dateNow;
    groupIntervalIf: any;
    groupIntervalElse: any;
    loadGeneral() {
        this.auth.isAuthenticated().subscribe((authState) => {
            this.authenticated = authState;
            this.eventId = localStorage.getItem('eventId');

            // CHECK HAVE EVENT ID AND LOAD MODULES OR REDIRECT TO LOGIN
            if (this.eventId) {
                this.global.getEvent(this.eventId, async (status) => {
                    if (status && this.global.event && this.global.event.title) {
                        this.title.setTitle(this.global.event.title);
                        this._document.getElementById('appFavicon').setAttribute('href', this.global.event.webApp.touchIcon.url);
                        this._document.getElementById('appAppleFavicon').setAttribute('href', this.global.event.webApp.touchIcon.url);
                    }
                    if (this.global.event && (this.global.event.visibility || (!this.global.event.visibility && this.authenticated))) {
                        this.loadModules();
                        this.setReferences();
                    } else if (!this.global.event || (!this.global.event.visibility && !this.authenticated)) {
                        this.auth.removeStorageReferences();
                        this.navCtrl.navigateRoot(['/'])
                            .then(() => {
                                this.hideSplashScreen()
                            });
                    }
                });
            } else if (this.authenticated && !this.eventId) {
                this.auth.getUser().pipe(
                    take(1)
                ).subscribe((user) => {
                    if (user) {
                        this.global.loadService((_) => {
                            this.hideSplashScreen();
                            if (
                                this.global.userEvents &&
                                this.global.userEvents.length > 1
                            ) {
                                this.navCtrl.navigateRoot([
                                    '/user-events',
                                    user.uid
                                ]);
                            } else if (
                                this.global.userEvents &&
                                this.global.userEvents.length == 1
                            ) {
                                this.redirectUserToEvent(
                                    this.global.userEvents[0]
                                );
                            }
                        });
                    } else {
                        this.auth.logout();
                    }
                });
            } else {
                // CASE DO NOT HAVE EVENT ID, CLEAR STORAGE REFERENCES AND REDIRECT TO LOGIN
                this.auth.removeStorageReferences();
                this.navCtrl.navigateRoot(['/']).then(() => {
                    this.hideSplashScreen();
                });
            }
        });
    }

    updateAccessAnalytics() {
        if (this.typeUser != TypeUser.ATTENDEE || this.contAccess !== 0) return;

        this.dbAnalytics.userAccess(
            this.eventId,
            this.global.event.timezone
        );
        const totalAccess = this.global.userTotalAccess ? this.global.userTotalAccess + 1 : 1;

        this.dbGeral.updateUserGeneral(
            {
                total_access: totalAccess,
                last_access: this.luxon.getTimeStampFromDateNow(
                    new Date(),
                    this.global.event.timezone
                )
            },
            this.userId,
            this.eventId,
            this.typeUser
        );
        this.contAccess = 1;
    }

    redirectUserToEvent(eventId: string) {
        localStorage.setItem('eventId', eventId);
        this.global.previousPage = 'container';

        this.daoEvent.getEvent(eventId).subscribe((event) => {

            if (event) {
                this.updateAccessAnalytics();
            }

            localStorage.setItem('homePage', event.homePage);
            this.global.eventHomePage = event.homePage;
            this.global.loadService((_) => {
                if (event.required_edit_profile == false) {
                    this.navCtrl.navigateRoot([event.homePage]).then((_) => {
                        if (_ == true) {
                            this.events.publish('updateScreen');
                        }
                    });
                } else if (
                    event.required_edit_profile == true &&
                    this.global.userEditProfile == true
                ) {
                    this.navCtrl.navigateRoot([event.homePage]).then((_) => {
                        if (_ == true) {
                            this.events.publish('updateScreen');
                        }
                    });
                } else if (
                    event.required_edit_profile == true &&
                    (this.global.userEditProfile == false ||
                        this.global.userEditProfile == null)
                ) {
                    this.global.cantGoBackFromProfile = true;
                    this.navCtrl
                        .navigateRoot([
                            `/event/${eventId}/edit-profile/${this.global.userModuleId}/${this.global.userType}/${this.global.userId}`
                        ])
                        .then((_) => {
                            if (_ == true) {
                                this.events.publish('updateScreen');
                            }
                        });
                }
            });
        });
    }

    setReferences() {
        this.global.loadService(async (status) => {
            if (status) {
                // obtém as informações do usuário, via global service
                this.typeUser = this.global.userType;
                this.photoUrl = this.global.photoUrl;
                this.displayName = this.global.displayName;
                this.userId = this.global.userId;
                this.eventId = localStorage.getItem('eventId');
                this.moduleId = this.global.userModuleId;
                // após logado, utiliza o idioma preferido do usuário, definido na conta
                if (
                    this.global.language !== null &&
                    this.global.language !== undefined &&
                    this.global.language !== ''
                ) {
                    this.setLanguageUse(this.global.language);
                }

                if (
                    this.userId == null &&
                    !localStorage.getItem('loggedLang')
                ) {
                    localStorage.setItem('loggedLang', 'false');
                }

                if (this.userId !== null && this.eventId !== null) {
                    // obtem o device id do dispositivo para notificações
                    if (this.isCordova) {
                        this.oneSignal.getIds().then((ids) => {
                            this.daoGeral.updateUserPushNotification(
                                ids,
                                this.userId,
                                this.eventId,
                                this.typeUser
                            );
                        });
                    }

                    this.daoModules
                        .getModule(this.moduleId)
                        .subscribe((moduleRes) => {
                            if (this.typeUser == TypeUser.ATTENDEE) {
                                this.allowChat = moduleRes['allow_chat'];
                                if (this.allowChat) {
                                    this.startChatValues();
                                }
                            }
                        });

                    // Update analytics access
                    this.updateAccessAnalytics();

                    // start notification badges
                    this.startNotificationsValues();
                    // start event values
                    this.startEventValues();
                    // redirect user
                    this.redirectUser();
                    // start user groups
                    if (this.typeUser >= TypeUser.SPEAKER)
                        this.global.loadGroups(
                            this.userId,
                            this.typeUser,
                            this.eventId
                        );

                    this.observeNotifications();
                    this.global.loadModulesEvent();

                    if (this.typeUser <= 3 || this.typeUser == null) {
                        this.totalEvents = 0;
                    } else {
                        this.totalEvents = this.global.userEvents.length;
                    }
                    this.hideSplashScreen();
                }
            } else {
                if (this.eventId !== null) {
                    if (this.global.event.visibility) {
                        this.loadModules();
                        this.startEventValues();
                        this.hideSplashScreen();
                    } else {
                        this.eventId = null;
                        this.auth.removeStorageReferences();
                        this.router.navigateByUrl('/').then(() => {
                            this.hideSplashScreen();
                        });
                    }
                } else {
                    this.hideSplashScreen();
                }
            }
        });
    }

    /**
     * Init badges
     */
    startNotificationsValues() {
        this.notificationBadge = 0;
        this.dateNow = this.notificationDate.getTimeStampFromDateNow(
            new Date(),
            this.global.event.timezone
        );
        this.notificationsCollection = this.afs
            .collection('events')
            .doc(this.eventId)
            .collection('notifications', (ref) =>
                ref
                    .orderBy('delivery_date', 'desc')
                    .where('delivery_date', '<=', this.dateNow)
            );
        this.refNotifications = this.notificationsCollection
            .valueChanges()
            .subscribe((data: any) => {
                this.getBadgeValue(data).then((response: number) => {
                    this.zone.run(() => {
                        this.notificationBadge = response;
                    });
                });
                if (
                    this.notificationBadge >= 1 &&
                    this.global.notificationBadge <= 1
                ) {
                    this.global.updateNotificationBadge(1, 'notify');
                }
            });
    }

    /**
     * Init badges chat
     */
    startChatValues() {
        if (
            this.typeUser >= TypeUser.SPEAKER &&
            this.typeUser <= TypeUser.ATTENDEE
        ) {
            this.chatCollection = this.afs
                .collection('events')
                .doc(this.eventId)
                .collection('chats', (ref) =>
                    ref.where(
                        `members.${this.global.userId}.uid`,
                        '==',
                        this.global.userId
                    )
                );
            this.refChat = this.chatCollection
                .valueChanges()
                .subscribe((data: any) => {
                    let auxBadgeChat = [];
                    data.forEach((element) => {
                        let last_access = element['last_access']
                            ? element['last_access'][this.userId]
                            : null;
                        let uid = element['uid'];
                        this.msgChatCollection = this.afs
                            .collection('events')
                            .doc(this.eventId)
                            .collection('chats')
                            .doc(uid)
                            .collection('messages', (ref) =>
                                ref.orderBy('send_at', 'desc')
                            );
                        this.refMsgsChat = this.msgChatCollection
                            .valueChanges()
                            .subscribe((dataT: any) => {
                                dataT.forEach((elMsg) => {
                                    let msg = elMsg;
                                    if (msg.send_at > last_access) {
                                        if (msg.from_user !== this.userId) {
                                            auxBadgeChat.push(element);
                                        }
                                    }
                                });
                                this.chatBadge = this.removeChatBadgeDouble(
                                    auxBadgeChat
                                );
                                if (
                                    this.chatBadge >= 1 &&
                                    this.global.chatBadge < 1
                                ) {
                                    this.global.updateNotificationBadge(
                                        1,
                                        'chat'
                                    );
                                }
                            });
                    });
                });
        }
    }

    async startEventValues() {
        if (
            this.global.event !== null &&
            !this.global.event.visibility &&
            this.userId == null
        ) {
            this.callLogin();
        }

        if (this.global.event.visibility && this.userId == null) {
            localStorage.setItem('eventVisible', 'true');
            if (
                localStorage.getItem('loggedLang') &&
                localStorage.getItem('loggedLang') == 'false'
            ) {
                this.setLanguageUse(this.global.event.language);
                this.navCtrl.navigateRoot([this.global.eventHomePage]);
                this.hideSplashScreen();
            }

            this.global.language = localStorage.getItem('usedLanguage');
        }
        // carrega as cores do evento
        if (
            this.global.language == '' ||
            this.global.language == null ||
            this.global.language == undefined
        ) {
            this.global.language = environment.platform.defaultLanguage;
        }
        localStorage.setItem('usedLanguage', this.global.language);
        this.translateService.setDefaultLang(this.global.language);
        this.translateService.use(this.global.language);
        this.menu_color = this.global.eventColors.menu_color;
        this.menu_text_color = this.global.eventColors.menu_text_color;
        this.title_color = this.global.eventColors.title_color;
        this.text_content_color = this.global.eventColors.text_content_color;
        this.link_color = this.global.eventColors.link_color;
        this.bannerUrl = this.global.event.banner.url;
        this.allow_language = this.global.allow_language;

        if (this.typeUser >= TypeUser.SPEAKER)
            await this.global
                .loadGroups(this.userId, this.typeUser, this.eventId)
                .then((a) => { });

        if (this.global.event['default_attendee_module'] !== '') {
            this.allowPublicRegister = true;
        } else {
            this.allowPublicRegister = false;
        }
    }

    redirectUser() {
        // se o usuário estiver logado e a página que ele está fizer parte do login, redireciona para inicial do evento
        if (
            this.router.url == '/' ||
            this.router.url == '/start' ||
            this.router.url == '/login' ||
            this.router.url == '/select-login'
        ) {
            this.allow_language = this.global.event.allow_language;
            /**
             *  caso o evento não seja obrigatório editar o perfil no primeiro acesso
             *  redireciona para home do evento
             */
            if (this.global.event.required_edit_profile == false) {
                this.navCtrl
                    .navigateRoot([this.global.event.homePage])
                    .then((_) => {
                        if (_ == true) {
                            this.hideSplashScreen();
                        }
                    });
                /**
                 * caso o evento seja obrigatório editar o perfil no primeiro acesso e o usuário já tenha editado
                 * redireciona para a home do evento
                 */
            } else if (
                this.global.event.required_edit_profile == true &&
                this.global.userEditProfile == true
            ) {
                this.navCtrl
                    .navigateRoot([this.global.event.homePage])
                    .then((_) => {
                        if (_ == true) {
                            this.hideSplashScreen();
                        }
                    });
                /**
                 * caso o evento seja obrigatório editar o perfil no primeiro acesso e o usuário ainda não editou
                 * redireciona para a página de editar perfil
                 */
            } else if (
                this.global.event.required_edit_profile == true &&
                this.global.userEditProfile == false
            ) {
                this.global.cantGoBackFromProfile = true;
                this.navCtrl
                    .navigateRoot([
                        `/event/${this.global.event.uid}/edit-profile/${this.global.userModuleId}/${this.global.userType}/${this.global.userId}`
                    ])
                    .then((_) => {
                        // fecha a splash screen
                        if (_ == true) {
                            this.hideSplashScreen();
                        }
                    });
            }
        } else {
            // fecha a splash screen
            this.hideSplashScreen();
        }
    }

    getBadgeValue(data) {
        return new Promise((resolve) => {
            let auxBadge = [];
            let cont = 0;
            data.forEach(async (notification) => {
                if (this.global.user !== null) {
                    if (notification.send_to == 'all') {
                        if (this.global.userNotificationTime !== undefined) {
                            if (
                                notification.delivery_date >
                                this.global.userNotificationTime
                            ) {
                                auxBadge.push(notification);
                            }
                        } else {
                            auxBadge.push(notification);
                        }
                        cont++;
                    } else {
                        await this.global.loadGroups(
                            this.global.userId,
                            this.global.userType,
                            this.global.eventId
                        );
                        for (let index in notification.groups_ids) {
                            const pos = this.global.groupsAttendees
                                .map(function (e) {
                                    return e.uid;
                                })
                                .indexOf(notification.groups_ids[index]);
                            if (pos >= 0) {
                                if (
                                    this.global.userNotificationTime !==
                                    undefined
                                ) {
                                    if (
                                        notification.delivery_date >
                                        this.global.userNotificationTime
                                    ) {
                                        auxBadge.push(notification);
                                    }
                                } else {
                                    auxBadge.push(notification);
                                }
                                cont++;
                            } else {
                                cont++;
                            }
                        }
                    }
                } else {
                    let noUserNotificationDate = localStorage.getItem(
                        'notificationTime'
                    );
                    if (notification.send_to == 'all') {
                        if (noUserNotificationDate !== undefined) {
                            if (
                                notification.delivery_date >
                                noUserNotificationDate
                            ) {
                                auxBadge.push(notification);
                            }
                        } else {
                            auxBadge.push(notification);
                        }
                        cont++;
                    } else {
                        cont++;
                    }
                }
                if (cont == data.length) {
                    resolve(auxBadge.length);
                }
            });
        });
    }

    // loads the modules in the event menu
    async loadModules() {
        // check if user is logged in. seeking the userId in the global service.
        if (typeof this.typeUser === 'undefined' || this.typeUser === null) {
            this.userId = await this.global.loadUserId();
        }
        // if (this.allowReloadModules == true) {
        //     this.allowReloadModules = false;
        this.daoModules
            .getMenuModules(this.eventId)
            .pipe(
                switchMap((modules) => {
                    let menuModules: any[] = [];

                    // Logged
                    if (this.userId) {
                        return this.store.select(getUserData).pipe(
                            mergeMap(async (userData) => {
                                if (userData) {
                                    let menuModules: any[] = [];
                                    for (let module of modules) {
                                        // if the type of vision is limited by groups
                                        if (
                                            this.global.userType >=
                                            TypeUser.SUPERGOD &&
                                            this.global.userType <=
                                            TypeUser.EMPLOYEE
                                        ) {
                                            menuModules.push(module);
                                        } else if (
                                            module.typeVision ===
                                            TypeVisionGroup.GROUP_ACCESS_PERMISSION
                                        ) {
                                            if (
                                                this.global.groupsAttendees ==
                                                undefined
                                            ) {
                                                await this.global.loadGroups(
                                                    this.userId,
                                                    this.global.userType,
                                                    this.eventId
                                                );
                                            }
                                            for (let index in module.access_groups) {
                                                // checks whether the group belongs to the user groups
                                                const pos = this.global.groupsAttendees
                                                    .map(function (e) {
                                                        return e.uid;
                                                    })
                                                    .indexOf(
                                                        module.access_groups[
                                                            index
                                                        ].uid
                                                    );

                                                if (pos >= 0) {
                                                    menuModules.push(module);
                                                    break;
                                                }
                                            }
                                        } else {
                                            menuModules.push(module);
                                        }
                                    }
                                    return of(menuModules).toPromise();
                                } else {
                                    return of([]).toPromise();
                                }
                            })
                        );
                    }
                    // not logged in
                    else {
                        for (let module of modules) {
                            if (
                                module.typeVision !==
                                TypeVisionGroup.GROUP_ACCESS_PERMISSION
                            ) {
                                menuModules.push(module);
                            }
                        }

                        return of(menuModules);
                    }
                })
            )
            .subscribe((menuModules) => {
                this.menuModules = menuModules;
            });
        // }
    }

    basicModuleRouting(module) {
        this.router.navigate([module.viewApp]);
    }

    routingToSessionDirectly(moduleId, sessionId, sessionFeedbackModule) {
        let navigationExtras: NavigationExtras = {
            state: {
                sessionFeedbackModule: sessionFeedbackModule
            }
        }

        this.router.navigate([`/event/${this.eventId}/schedule-detail/${moduleId}/${sessionId}`], navigationExtras);
    }

    routingToSpeakerDirectly(moduleId, speaker) {
        let navigationExtras: NavigationExtras = {
            state: {
                user: speaker
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${moduleId}/4/${speaker.uid}`], navigationExtras);
    }

    routingToAttendeeDirectly(moduleId, attendee) {
        let navigationExtras: NavigationExtras = {
            state: {
                user: attendee
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${moduleId}/${attendee.type}/${attendee.uid}`], navigationExtras);
    }

    /**
     * Check number of sesions/speaker/orators
     * @param module 
     */
    async checkNumberOfSub(module) {
        if (module.type == TypeModule.SCHEDULE) {
            this.dbSchedule.closeRefGetSessionsGroupsVision();

            this.daoFeedback.getFeedbackModule(this.global.eventId, async (modules) => {
                let sessionFeedbackModule = null;
                if (modules.length >= 1) {
                    sessionFeedbackModule = modules[0].uid;
                }

                // verifies the type of vision of the module. global vision
                if (module.typeVision === TypeVisionModule.GLOBAL_VISION) {
                    this.dbSchedule.getAllSessionsVisionGlobal(module.uid).pipe(take(1)).subscribe((sessions) => {
                        if (sessions.length == 1) {
                            this.routingToSessionDirectly(module.uid, sessions[0].uid, sessionFeedbackModule);
                        } else {
                            this.basicModuleRouting(module);
                        }
                    })
                }

                // group vision
                if (module.typeVision === TypeVisionModule.GROUP_VISION) {
                    let groups = (this.global.groupsAttendees) ? this.global.groupsAttendees : await this.global.loadGroups(this.global.userId, this.global.userType, this.eventId);
                    this.dbSchedule.getSessionsGroupsVision(groups, module.uid, (data) => {
                        if (data['sessions'].length == 1) {
                            this.routingToSessionDirectly(module.uid, data['sessions'][0].uid, sessionFeedbackModule);
                        } else {
                            this.basicModuleRouting(module);
                        }
                    })
                }

                //limited access by groups
                if (module.typeVision === TypeVisionModule.GROUP_ACCESS_PERMISSION) {
                    this.dbSchedule.getAllSessionsLimitedAccessByGroup(module.uid).pipe(take(1)).subscribe((sessions) => {
                        if (sessions.length == 1) {
                            this.routingToSessionDirectly(module.uid, sessions[0].uid, sessionFeedbackModule);
                        } else {
                            this.basicModuleRouting(module);
                        }
                    })
                }
            })
        } else if (module.type == TypeModule.SPEAKER) {
            // verifies the type of vision of the module. global vision
            if (module.typeVision === TypeVisionModule.GLOBAL_VISION) {
                this.dbSpeakers.getSpeakersAllGlobalVision(module.uid).pipe(take(1)).subscribe((speakers) => {
                    if (speakers.length == 1) {
                        this.routingToSpeakerDirectly(module.uid, speakers[0]);
                    } else {
                        this.basicModuleRouting(module);
                    }
                })
            }

            // group vision
            if (module.typeVision === TypeVisionModule.GROUP_VISION) {
                let groups = (this.global.groupsAttendees) ? this.global.groupsAttendees : await this.global.loadGroups(this.global.userId, this.global.userType, this.eventId);
                this.dbSpeakers.getSpeakersGroupsVision(groups, 'asc', module.uid, (speakers) => {
                    if (speakers.length == 1) {
                        this.routingToSpeakerDirectly(module.uid, speakers[0]);
                    } else {
                        this.basicModuleRouting(module);
                    }
                })
            }

            //limited access by groups
            if (module.typeVision === TypeVisionModule.GROUP_ACCESS_PERMISSION) {
                this.dbSpeakers.getSpeakersAllLimitedAccessByGroup(module.uid).pipe(take(1)).subscribe((speakers) => {
                    if (speakers.length == 1) {
                        this.routingToSpeakerDirectly(module.uid, speakers[0]);
                    } else {
                        this.basicModuleRouting(module);
                    }
                })
            }
        } else if (module.type == TypeModule.ATTENDEE) {
            // verifies the type of vision of the module. global vision
            if (module.typeVision === TypeVisionModule.GLOBAL_VISION) {
                this.dbAttendees.getAttendeesAllGlobalVision(module.uid).pipe(take(1)).subscribe((attendees) => {
                    if (attendees.length == 1) {
                        this.routingToAttendeeDirectly(module.uid, attendees[0]);
                    } else {
                        this.basicModuleRouting(module);
                    }
                })
            }

            // group vision
            if (module.typeVision === TypeVisionModule.GROUP_VISION) {
                let groups = (this.global.groupsAttendees) ? this.global.groupsAttendees : await this.global.loadGroups(this.global.userId, this.global.userType, this.eventId);
                this.dbAttendees.getAttendeesGroupsVision(groups, 'asc', module.uid, (attendees) => {
                    if (attendees.length == 1) {
                        this.routingToAttendeeDirectly(module.uid, attendees[0]);
                    } else {
                        this.basicModuleRouting(module);
                    }
                })
            }

            //limited access by groups
            if (module.typeVision === TypeVisionModule.GROUP_ACCESS_PERMISSION) {
                this.dbAttendees.getAttendeesAllLimitedAccessByGroup(module.uid).pipe(take(1)).subscribe((attendees) => {
                    if (attendees.length == 1) {
                        this.routingToAttendeeDirectly(module.uid, attendees[0]);
                    } else {
                        this.basicModuleRouting(module);
                    }
                })
            }
        } else {
            this.basicModuleRouting(module);
        }
    }

    async openModule(module) {
        if (module.type !== TypeModule.EXTERNAL_LINK) {
            this.checkNumberOfSub(module);
        } else {
            const available = await this.safariViewController.isAvailable();

            if (module.open_link_behavior === 'sf' && available) {
                this.safariViewController
                    .show({
                        url: module.link,
                        hidden: false,
                        animated: false,
                        transition: 'curl',
                        enterReaderModeIfAvailable: true
                    })
                    .subscribe(
                        (result: any) => {
                            if (result.event === 'opened')
                                console.log('Opened');
                            else if (result.event === 'loaded')
                                console.log('Loaded');
                            else if (result.event === 'closed')
                                console.log('Closed');
                        },
                        (error: any) => console.error(error)
                    );
            } else {
                if (!this.global.isBrowser) {
                    let target = '_system';
                    this.theInAppBrowser.create(
                        module.link,
                        target,
                        this.options
                    );
                } else {
                    window.open(module.link, '_system');
                }
            }
        }
    }

    openLink(url: string) {
        if (!this.global.isBrowser) {
            let target = '_system';
            this.theInAppBrowser.create(url, target, this.options);
        } else {
            window.open(url, '_system');
        }
    }

    async callLogin() {
        if (this.onlyCallOne) {
            if (
                this.eventId == null ||
                this.eventId == undefined ||
                this.eventId == ''
            ) {
                if (this.route.snapshot.children.length >= 1) {
                    this.eventId = this.route.snapshot.children[0].params[
                        'eventId'
                    ];
                    localStorage.setItem('eventId', this.eventId);
                    this.global.getActiveEvent();
                }
            }
            this.onlyCallOne = false;
            this.auth.removeStorageReferences();

            const modal = await this.modalCtrl.create({
                component: LoginpPage,
                componentProps: {
                    redirectUrl: this.router.url,
                    eventId: this.eventId
                },
                backdropDismiss: false
            });
            return await modal.present();
        }
    }

    // remove as conversas duplicadas do chat, para o número da badge ficar correto
    removeChatBadgeDouble(array) {
        let badgeArray = array.filter(function (el, i) {
            return array.indexOf(el) == i;
        });

        return badgeArray.length;
    }

    // ao clicar na foto de perfil no menu, vai para o perfil do usuário
    personalPage() {
        if (
            this.typeUser == TypeUser.ATTENDEE ||
            this.typeUser == TypeUser.SPEAKER
        ) {
            this.router.navigate([
                `/event/${this.eventId}/personal-page/${this.moduleId}/${this.typeUser}/${this.userId}`
            ]);
        }
    }

    logout() {
        this.auth.logout();
        this.global.resetService();
        this.eventId = null;
        this.userId = null;
        this.typeUser = null;
    }

    // volta para a lista de eventos do usuário
    async confirmRedirectToContainer() {
        const alert = await this.alertCtrl.create({
            header: this.translateService.instant('global.alerts.list_events'),
            message: this.translateService.instant(
                'global.alerts.go_to_container_confirm'
            ),
            buttons: [
                {
                    text: this.translateService.instant(
                        'global.buttons.cancel'
                    ),
                    handler: () => { }
                },
                {
                    text: this.translateService.instant('global.buttons.go'),
                    cssClass: 'red-text',
                    handler: (blah) => {
                        this.redirectToContainer();
                    }
                }
            ]
        });

        await alert.present();
    }
    // volta para o login
    async confirmBackToLogin() {
        const alert = await this.alertCtrl.create({
            header: this.translateService.instant('global.alerts.exit_event'),
            message: this.translateService.instant(
                'global.alerts.confirm_exit_event'
            ),
            buttons: [
                {
                    text: this.translateService.instant(
                        'global.buttons.cancel'
                    ),
                    handler: () => { }
                },
                {
                    text: this.translateService.instant('global.buttons.exit'),
                    cssClass: 'red-text',
                    handler: (_) => {
                        this.showSplashscreen();
                        this.auth.removeStorageReferences();
                        this.navCtrl.navigateRoot(['/']).then(() => {
                            window.location.reload();
                            this.hideSplashScreen();
                        });
                    }
                }
            ]
        });

        await alert.present();
    }

    /**
     * Showing splashscreen dependent on platform
     */
    showSplashscreen() {
        if (this.platform.is('mobile')) {
            this.splashScreen.show();
        } else {
            this.showSplashScreenWeb = true;
            this.splashScreen.hide();
        }
    }

    /**
     * Hiding splashscreen dependent on platform
     */
    hideSplashScreen() {
        if (this.platform.is('mobile')) {
            this.splashScreen.hide();
        } else {
            this.showSplashScreenWeb = false;
        }
    }

    redirectToContainer() {
        this.allowReloadModules = true;
        localStorage.removeItem('eventId');
        // localStorage.removeItem('usedLanguage');
        localStorage.removeItem('timezone');
        localStorage.removeItem('homePage');

        this.events.publish('reloadContainerEvents');
        if (this.userId == null) {
            // if (!this.global.isBrowser)
            this.showSplashscreen();
            // this.router.navigate
            this.navCtrl.navigateRoot(['/public']).then((_) => {
                window.location.reload();
                // if (!this.global.isBrowser)
                this.hideSplashScreen();
            });
        } else {
            if (this.typeUser <= TypeUser.GOD) {
                // this.router.navigate
                this.navCtrl.navigateRoot(['/admin-events']);
                this.menuCtrl.close();
            } else if (this.typeUser == TypeUser.EMPLOYEE) {
                // this.router.navigate
                this.navCtrl.navigateRoot(['/c-events', this.global.clientId]);
                this.menuCtrl.close();
            } else if (this.typeUser == TypeUser.CLIENT) {
                // this.router.navigate
                this.navCtrl.navigateRoot(['/c-events', this.userId]);
                this.menuCtrl.close();
            } else if (
                this.typeUser >= TypeUser.SPEAKER &&
                this.typeUser <= TypeUser.ATTENDEE
            ) {
                // this.router.navigate
                this.navCtrl.navigateRoot(['/user-events', this.userId]);
                this.menuCtrl.close();
            }
        }
    }

    // checa a linguagem do usuário e retorna a string do idioma no formato necessário
    checkLanguage(language: string) {
        if (language == 'pt-BR') {
            return 'pt_BR';
        } else if (language == 'en-US') {
            return 'en_US';
        } else if (language == 'fr-FR') {
            return 'fr_FR';
        } else if (language == 'es-ES') {
            return 'es_ES';
        } else if (language == 'de-DE') {
            return 'de_DE';
        } else {
            return environment.platform.defaultLanguage;
        }
    }

    // seta a linguagem para uso do app
    setLanguageUse(language: string) {
        if (language == '' || language == null || language == undefined) {
            language = environment.platform.defaultLanguage;
        }
        this.translateService.setDefaultLang(language);
        this.translateService.use(language);
        this.global.language = language;
        localStorage.setItem('usedLanguage', language);
    }

    // abre o modal para exibir notificações
    async openNotifications() {
        const modal = await this.modalCtrl.create({
            component: PathComponent.notifications,
            componentProps: {
                eventId: this.eventId
            }
        });
        return await modal.present();
    }

    // abre o modal de termos de uso
    async openLegalTerms() {
        const modal = await this.modalCtrl.create({
            component: PathComponent.terms_n_privacy,
            componentProps: {
                eventId: this.eventId
            }
        });
        return await modal.present();
    }

    // login evento público
    async openPublicLogin() {
        const modal = await this.modalCtrl.create({
            component: PathComponent.public_login,
            componentProps: {
                eventId: this.eventId,
                logoUrl: this.global.event.logo.url,
                menuColor: this.menu_color,
                menuTxtColor: this.menu_text_color
            }
        });
        return await modal.present();
    }

    // registro evento público
    async openPublicRegister() {
        const modal = await this.modalCtrl.create({
            component: PathComponent.public_register,
            componentProps: {
                eventId: this.eventId,
                logoUrl: this.global.event.logo.url,
                menuColor: this.menu_color,
                menuTxtColor: this.menu_text_color
            }
        });
        return await modal.present();
    }

    // alterar idioma do app, baseado nos idiomas disponíveis no evento
    async changeLanguage() {
        const picker = await this.pickerCtrl.create({
            columns: this.getLanguageColumns(),
            buttons: [
                {
                    text: this.translateService.instant(
                        'global.buttons.cancel'
                    ),
                    role: 'cancel'
                },
                {
                    text: this.translateService.instant('global.buttons.ok'),
                    handler: (v) => {
                        let lang = v['lang']['value'];
                        if (this.userId) {
                            this.dbGeral.updateUserGeneral(
                                { language: lang },
                                this.userId,
                                this.eventId,
                                this.typeUser
                            );
                        } else {
                            localStorage.setItem('loggedLang', 'true');
                        }
                        this.events.publish('languageUpdate', lang);
                    }
                }
            ]
        });
        // picker.columns[0].selectedIndex = this.selectedLangIndex();
        await picker.present();
    }

    // obtém as colunas da seleção de idioma
    getLanguageColumns() {
        let columns = [];
        columns.push({
            name: 'lang',
            options: this.getColumnOptions()
        });
        return columns;
    }

    // retorna as colunas de seleção de idioma, com base nos idiomas disponíveis no evento
    getColumnOptions() {
        let options = [];

        if (this.global.event !== null) {
            if (
                this.global.event.description['de_DE'] !== null &&
                this.global.event.description['de_DE'] !== undefined
            ) {
                options.push({
                    text: this.translateService.instant('global.texts.de_DE'),
                    value: 'de_DE'
                });
            }

            if (
                this.global.event.description['es_ES'] !== null &&
                this.global.event.description['es_ES'] !== undefined
            ) {
                options.push({
                    text: this.translateService.instant('global.texts.es_ES'),
                    value: 'es_ES'
                });
            }

            if (
                this.global.event.description['fr_FR'] !== null &&
                this.global.event.description['fr_FR'] !== undefined
            ) {
                options.push({
                    text: this.translateService.instant('global.texts.fr_FR'),
                    value: 'fr_FR'
                });
            }

            if (
                this.global.event.description['en_US'] !== null &&
                this.global.event.description['en_US'] !== undefined
            ) {
                options.push({
                    text: this.translateService.instant('global.texts.en_US'),
                    value: 'en_US'
                });
            }

            if (
                this.global.event.description['pt_BR'] !== null &&
                this.global.event.description['pt_BR'] !== undefined
            ) {
                options.push({
                    text: this.translateService.instant('global.texts.pt_BR'),
                    value: 'pt_BR'
                });
            }
        }
        return options;
    }

    notificationsCollectionObserver: AngularFirestoreCollection<any> = null;
    refNotificationsObserver: any = null;
    observeNotifications() {
        this.notificationsCollectionObserver = this.afs
            .collection('events')
            .doc(this.eventId)
            .collection('notifications');
        this.refNotificationsObserver = this.notificationsCollectionObserver
            .valueChanges()
            .subscribe((data: any) => {
                this.events.publish('reloadNotification');
            });
    }

    wakeUpFunctions() {
        this.auth.verifyEmailDb('test@test.com', () => { });
        this.auth.verifyCodeNumber('123456', '12345678901234567890', () => { });
        this.auth.createAccount(null, null, null, null).subscribe(() => { });
        let body = { eventId: null };
        this.requestOptions = { headers: this.headers, body: {} };
        this.headers = new HttpHeaders();
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
    }

    /**
     * Event for hamburger click
     */
    hamburgerClicked() {
        this.global.hamburgerActivated.next(this.hamburgerActivated);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DocumentsModulePage } from './documents-module.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { InteractivityModule } from '../../reusable_components/interactivity/interactivity.module';

const routes: Routes = [
    {
        path: '',
        component: DocumentsModulePage
    }
];

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        SharedModule,
        InteractivityModule
    ],
    declarations: [
        DocumentsModulePage
    ]
})
export class DocumentsModulePageModule { }

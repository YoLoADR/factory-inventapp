import { Component, OnInit, OnDestroy } from '@angular/core';
import { DaoDocumentsService } from 'src/app/providers/db/dao-document.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/app/providers/authentication/auth.service';
import { TypeVisionDocument } from 'src/app/models/type-vision-document';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { StorageOfflineService } from 'src/app/providers/storage-offline/storage-offline.service';
import { NetworkService } from 'src/app/providers/network/network.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-documents-module',
    templateUrl: './documents-module.page.html',
    styleUrls: ['./documents-module.page.scss'],
})
export class DocumentsModulePage implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];

    loader: boolean = false;
    allowButtonsHeader: boolean = false;
    backBtn: boolean = false;
    oneFolder: boolean = false;

    userLanguage: string;

    eventId: string;
    moduleId: string;

    docModule: any;

    folders: any[] = [];
    documents: any[] = [];

    order: any = "desc";

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private SAuth: AuthService,
        private SDocuments: DaoDocumentsService,
        public SGlobal: GlobalService,
        private SOfflineStorage: StorageOfflineService,
        private SNetwork: NetworkService,
        private SUtility: UtilityService
    ) { }

    ngOnInit() {
        this.loader = true;
        if (this.SGlobal.previousPage == 'container') {
            this.backBtn = false;
        } else {
            this.backBtn = true;
        }

        if (this.router.url == this.SGlobal.eventHomePage) {
            this.allowButtonsHeader = true;
        }
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;

        this.route.params.pipe(take(1)).subscribe((params) => {
            this.eventId = params.eventId;
            this.moduleId = params.moduleId;
            this.getFolders();
        })
    }

    /**
     * Getting all folders of module
     */
    async getFolders() {
        // Online
        if (this.SNetwork.status()) {
            // Getting document module data
            this.subscriptions.push(this.SDocuments.getModule(this.moduleId).subscribe((docModule) => {
                this.docModule = docModule;

                if (this.docModule['typeOrder'] == 'recent') {
                    this.order = 'desc';
                } else if (this.docModule['typeOrder'] == 'oldest') {
                    this.order = 'asc';
                } else if (this.docModule['typeOrder'] == 'custom') {
                    this.order = 'custom';
                }

                // Getting all folders of module
                this.subscriptions.push(this.SDocuments.getFolders(this.moduleId, this.order).subscribe(async (foldersReturned) => {
                    let folders = [];
                    let userAuth = await this.SAuth.current();
                    for (const folder of foldersReturned) {
                        if (folder.typeVision === TypeVisionDocument.GLOBAL_VISION) {
                            folders.push(folder);
                        } else {
                            if (userAuth) {
                                let groupsAttendees = typeof this.SGlobal.groupsAttendees !== 'undefined' && this.SGlobal.groupsAttendees !== null ? this.SGlobal.groupsAttendees : [];
                                for (const group of groupsAttendees) {
                                    const groupId = group.uid
                                    let index = folders.map(function (e) { return e.uid; }).indexOf(folder.uid);

                                    if (folder.groups[groupId] && index <= -1) {
                                        folders.push(folder);
                                    }
                                }
                            }
                        }
                    }
                    // stores data on storage,
                    this.SOfflineStorage.setObject(`folders-module:${this.moduleId}`, { module: this.docModule, folders: folders });

                    if (folders.length == 1) {
                        this.oneFolder = true;
                        this.onlyOneFolder(folders[0]);
                    } else {
                        this.oneFolder = false;
                        this.folders = folders;
                        this.loader = false;
                    }
                }))
            }));
        } else {
            // Offline
            try {
                let result = await this.SOfflineStorage.getObject(`folders-module:${this.moduleId}`);
                if (result != null) {
                    this.docModule = result.module;
                    this.folders = result.folders;
                }
                this.loader = false;
            } catch (error) {
                this.docModule = null;
                this.folders = [];
                this.loader = false;
            }
        }
    }

    /**
     * On case of only one folder
     * @param folders 
     */
    onlyOneFolder(folder: any) {
        // Getting all documents of folder if only one
        this.subscriptions.push(this.SDocuments.getDocuments(this.moduleId, folder.uid, folder.orderDocuments, `name.${this.userLanguage}`).subscribe((documents) => {
            this.documents = documents;
            this.loader = false;
        }));
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}

import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { Survey } from 'src/app/models/survey';
import { Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { DaoSurveyService } from 'src/app/providers/db/dao-survey.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-survey-list',
    templateUrl: './survey-list.component.html',
    styleUrls: ['./survey-list.component.scss'],
})
export class SurveyListComponent implements OnInit, OnDestroy, OnChanges {
    subscriptions: Subscription[] = [];

    showListSurveys: boolean = true;
    backToList: boolean = false;

    @Input() eventId: string;
    @Input() moduleId: string;
    @Input() sessionId: string;
    @Input() userId: string;

    @Input() surveys: Survey[] = [];
    @Input() surveyModule: any;

    unansweredSurveys: Survey[] = [];
    answeredSurveys: Survey[] = [];

    surveySelectedId: string;

    title_color: string = null;
    text_content_color: string = null;
    bg_content_color: string = null;
    menu_color: string = null;

    userLanguage: string;

    loader: boolean = false;

    constructor(
        private SGlobal: GlobalService,
        private router: Router,
        private SUtility: UtilityService,
        private daoSurvey: DaoSurveyService
    ) { }

    ngOnInit() {
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        this.loadColors();
        this.loadSurveys();
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    ngOnChanges() {
        this.loadSurveys();
    }

    /**
     * Loading surveys
     */
    loadSurveys() {
        this.loader = true;
        this.unansweredSurveys = [];
        this.answeredSurveys = [];

        for (let i = 0; i < this.surveys.length; i++) {
            let survey = _.cloneDeep(this.surveys[i]);
            this.daoSurvey.getQuestions(this.surveyModule.uid, survey.uid).subscribe((questions) => {
                survey.questions = questions;

                let indexAnswered = this.checkAnsweredSurveys(survey.uid);
                if (indexAnswered >= 0) {
                    let index = this.SUtility.checkIndexExists(this.answeredSurveys, survey);

                    // if the object already exists in the array
                    if (index >= 0) {
                        //if visibility is on
                        if (survey.visibility) {
                            //update object
                            this.answeredSurveys[index] = survey;
                        } else {
                            //if visibility is off removes object from array
                            for (let i = 0; i <= this.answeredSurveys.length; i++) {
                                if (this.answeredSurveys[i].uid == survey.uid) {
                                    this.answeredSurveys.splice(i, 1);
                                }
                            }
                        }
                    } else { // if the survey does not already exist in the array and has active visibility, push
                        if (survey.visibility) {
                            this.answeredSurveys.push(survey);
                        }
                    }
                } else {
                    let index = this.SUtility.checkIndexExists(this.unansweredSurveys, survey);

                    // if the object already exists in the array
                    if (index >= 0) {
                        //if visibility is on
                        if (survey.visibility) {
                            //update object
                            this.unansweredSurveys[index] = survey;
                        } else { //if visibility is off removes object from array
                            for (let i = 0; i <= this.unansweredSurveys.length; i++) {
                                if (this.unansweredSurveys[i].uid == survey.uid) {
                                    this.unansweredSurveys.splice(i, 1);
                                }
                            }
                        }
                    } else { // if the survey does not already exist in the array and has active visibility, push
                        if (survey.visibility) {
                            this.unansweredSurveys.push(survey);
                        }
                    }
                }

                if (i == this.surveys.length - 1) {
                    this.answeredSurveys.sort((a, b) => {
                        return (a.title < b.title ? -1 : a.title > b.title ? 1 : 0);
                    });

                    this.unansweredSurveys.sort((a, b) => {
                        return (a.title < b.title ? -1 : a.title > b.title ? 1 : 0);
                    });

                    if (this.surveys.length > 1) {
                        this.loader = false;
                    } else {
                        if (this.unansweredSurveys.length > 0) {
                            this.openUnansweredSurvey(0);
                        } else {
                            this.loader = false;
                        }
                    }
                }
            });
        }
    }

    /**
     * Check answered surveys
     * @param surveyId 
     */
    checkAnsweredSurveys(surveyId) {
        if (!this.SGlobal.userLoaded) {
            return -1;
        }

        if (this.SGlobal.user.answeredSurveys) {
            return this.SGlobal.user.answeredSurveys.map((e) => { return e; }).indexOf(surveyId);
        } else {
            return -1;
        }
    }

    /**
     * Open unanswered survey
     * @param index 
     */
    openUnansweredSurvey(index) {
        if (this.sessionId) {
            if (this.surveys.length > 1) {
                this.backToList = true;
            } else {
                this.backToList = false;
            }
            this.surveySelectedId = this.unansweredSurveys[index].uid;
            this.loader = true;
            this.showListSurveys = false;

            // let navigationExtras: NavigationExtras = {
            //     state: {
            //         previousPage: 'surveyListSession',
            //         sessionId: this.sessionId,
            //         scheduleModuleId: this.surveyModule.uid,
            //     }
            // };
            // this.router.navigate([`/event/${this.eventId}/survey/${this.surveyModule.uid}/${this.unansweredSurveys[index].uid}`], navigationExtras);

        } else {
            let navigationExtras: NavigationExtras = {
                state: {
                    previousPage: 'surveyList'
                }
            };
            this.router.navigate([`/event/${this.eventId}/survey/${this.surveyModule.uid}/${this.unansweredSurveys[index].uid}`], navigationExtras);
        }
    }

    /**
     * Open answered survey
     * @param index 
     */
    openAnsweredSurvey(index) {
        if (this.sessionId) {
            if (this.surveys.length > 1 || this.answeredSurveys.length == 1) {
                this.backToList = true;
            } else {
                this.backToList = false;
            }
            this.surveySelectedId = this.answeredSurveys[index].uid;
            this.loader = true;
            this.showListSurveys = false;
            // let navigationExtras: NavigationExtras = {
            //     state: {
            //         previousPage: 'surveyListSession',
            //         sessionId: this.sessionId,
            //         scheduleModuleId: this.surveyModule.uid,
            //     }
            // };
            // this.router.navigate([`/event/${this.eventId}/survey/${this.surveyModule.uid}/${this.answeredSurveys[index].uid}`], navigationExtras);
        } else if (this.answeredSurveys[index].change_answer || this.answeredSurveys[index].view_answered) {
            let navigationExtras: NavigationExtras = {
                state: {
                    previousPage: 'surveyList'
                }
            };

            this.router.navigate([`/event/${this.eventId}/survey/${this.surveyModule.uid}/${this.answeredSurveys[index].uid}`], navigationExtras);
        }
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
    }

    /**
     * Stop loader
     */
    stopLoader() {
        this.loader = false;
    }

    /**
     * Go back here from child component
     */
    goBackToList() {
        this.showListSurveys = true;
    }
}

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { GlobalService } from 'src/app/shared/services';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { NavigationExtras, Router } from '@angular/router';

@Component({
    selector: 'app-speakers',
    templateUrl: './speakers.component.html',
    styleUrls: ['./speakers.component.scss'],
})
export class SpeakersComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];

    @Input() eventId: string;
    @Input() showTitle: boolean = false;
    @Input() speakers: any[] = [];
    @Input() title: string = "";

    title_color: string = null;
    bg_content_color: string = null;

    constructor(
        private SGlobal: GlobalService,
        private router: Router
    ) { }

    ngOnInit() {
        this.loadColors();
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Go to speaker profile
     * @param speaker 
     */
    profileDetails(speaker) {
        const moduleSpeaker = speaker.moduleId;
        const type = speaker.type;
        const uid = speaker.uid;

        let navigationExtras: NavigationExtras = {
            state: {
                user: speaker,
            }
        };
        this.router.navigate([`/event/${this.eventId}/profile/${moduleSpeaker}/${type}/${uid}`], navigationExtras);
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.title_color = this.SGlobal.eventColors.title_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
    }
}

import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { UtilityService, GlobalService } from 'src/app/shared/services';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { sessionFeedback } from 'src/app/models/sessionFeedback';
import { DaoAttendeesService } from 'src/app/providers/db/dao-attendees.service';
import { DaoSessionFeedbackService } from 'src/app/providers/db/dao-session-feedback.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-feedbacks',
    templateUrl: './feedbacks.component.html',
    styleUrls: ['./feedbacks.component.scss'],
})
export class FeedbacksComponent implements OnInit, OnDestroy, OnChanges {
    subscriptions: Subscription[] = [];

    @Input() eventId: string;
    @Input() userId: string;
    @Input() sessionId: string;

    @Input() feedbacks: sessionFeedback[] = [];
    @Input() feedbackModule: any = null;
    result: any[] = [];
    loadingSend: boolean[] = [];

    title_color: string = null;
    text_content_color: string = null;
    menu_color: string = null;
    menu_text_color: string = null;

    buttonColorYes = [];
    buttonColorNo = [];
    setResult: boolean = false;

    loader: HTMLIonLoadingElement;

    userLanguage: string;

    constructor(
        private translate: TranslateService,
        private SGlobal: GlobalService,
        private SUtility: UtilityService,
        private daoAttendee: DaoAttendeesService,
        private daoFeedback: DaoSessionFeedbackService
    ) { }

    ngOnInit() {
        this.loadColors();
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        this.setResultFeedback();
    }

    ngOnChanges() {
        this.setResultFeedback();
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Setting result feedback
     */
    setResultFeedback() {
        this.feedbacks = _.cloneDeep(this.feedbacks);
        this.result = []; // matriz com respostas.
        this.buttonColorNo = [];
        this.buttonColorYes = [];

        //Assigns null to all fields initially
        for (let i = 0; i < this.feedbacks.length; i++) {
            var totalQuestions = Object.keys(this.feedbacks[i].questions).length;
            this.result[i] = new Array(totalQuestions);
            this.buttonColorNo[i] = new Array(totalQuestions).fill(null);
            this.buttonColorYes[i] = new Array(totalQuestions).fill(null);

            for (let j = 0; j < totalQuestions; j++) {
                this.buttonColorYes[i][j] = new Array(totalQuestions).fill("#f4f4f4");
                this.buttonColorNo[i][j] = new Array(totalQuestions).fill("#f4f4f4");

                if (this.feedbacks[i].questions[j].type === "multipleSelect") {
                    let totalAnswers = this.feedbacks[i].questions[j].answers.length;
                    this.result[i][j] = new Array(totalAnswers).fill(false);
                } else {
                    this.result[i][j] = null;
                }
            }

            if (i == this.feedbacks.length - 1) {
                this.setResult = true;

                if (this.SGlobal.userLoaded) {
                    this.getPreviousAnswers();
                }
            }
        }
    }

    /**
     * Get prevous answer feedback
     */
    getPreviousAnswers() {
        // checks if there are any previous answers from the user to this search, and if it does, assign the values
        for (let i = 0; i < this.feedbacks.length; i++) {
            this.daoFeedback.getResponseUsers(
                this.feedbackModule,
                this.sessionId,
                this.feedbacks[i].uid,
                this.userId,
                (list) => {
                    if (Object.keys(list).length >= 1) {
                        if (
                            this.feedbacks[i].change_answer == false &&
                            this.feedbacks[i].questions.length <= Object.keys(list).length
                        ) {
                            this.feedbacks[i].response = true;
                        }

                        var totalQuestions = Object.keys(this.feedbacks[i].questions)
                            .length;
                        for (let j = 0; j < totalQuestions; j++) {
                            var typeQuestion = this.feedbacks[i].questions[j].type;
                            var questionId = this.feedbacks[i].questions[j].uid;

                            if (
                                typeQuestion == "evaluation" ||
                                typeQuestion == "dissertative"
                            ) {
                                if (
                                    list[questionId] &&
                                    list[questionId].answer !== null &&
                                    list[questionId].answer !== undefined
                                ) {
                                    this.feedbacks[i].questions[j].answered = true;
                                    this.result[i][j] = list[questionId].answer;
                                }
                            } else if (typeQuestion == "oneSelect") {
                                for (let uid of list[questionId].answer) {
                                    this.feedbacks[i].questions[j].answered = true;
                                    this.result[i][j] = uid;
                                }
                            } else if (typeQuestion == "multipleSelect") {
                                var answers = this.feedbacks[i].questions[j].answers;
                                var answersSize = Object.keys(answers).length;

                                for (let uid of list[questionId].answer) {
                                    this.feedbacks[i].questions[j].answered = true;

                                    for (let k = 0; k < answersSize; k++) {
                                        if (uid == answers[k].uid) {
                                            setTimeout(() => {
                                                this.result[i][j][k] = true;
                                            }, 300);
                                        }
                                    }
                                }
                            } else if (typeQuestion == "yesOrNo") {
                                if (list[questionId].answer) {
                                    this.feedbacks[i].questions[j].answered = true;

                                    let answerUser = list[questionId].answer;

                                    if (answerUser == "yes") {
                                        this.result[i][j] = "yes";
                                        this.buttonColorYes[i][j] = "#26B950";
                                        this.buttonColorNo[i][j] = "#f4f4f4";
                                    } else if (answerUser == "no") {
                                        this.result[i][j] = "no";
                                        this.buttonColorNo[i][j] = "#ff0000";
                                        this.buttonColorYes[i][j] = "#f4f4f4";
                                    }
                                }
                            }
                        }
                    }
                }
            );
        }
    }

    /**
     * Set evaluation
     * @param $event 
     * @param indexFeedback 
     * @param indexQuestion 
     */
    setEvaluation($event: { newValue: number }, indexFeedback, indexQuestion) {
        this.result[indexFeedback][indexQuestion] = $event.newValue;
    }

    /**
     * Send a feedback
     * @param indice 
     */
    async sendFeedback(indice) {
        this.loadingSend[indice] = true;
        let totalPoints = 0;
        let timestamp = (Date.now() / 1000) | 0;

        let totalQuestions = Object.keys(this.feedbacks[indice].questions).length;
        let contQuestion = 0;
        for (let question of this.feedbacks[indice].questions) {
            // for (let i = 0; i < totalQuestions; i++) {
            let typeQuestion = question.type;
            let feedbackId = this.feedbacks[indice].uid;
            let questionId = question.uid;
            let answer = this.result[indice][contQuestion];

            if (typeQuestion == "oneSelect") {
                if (answer != null && answer != undefined && answer != "") {
                    let arrayAnswer = [answer];
                    question.answered = true;
                    await this.createResultQuestion(
                        feedbackId,
                        questionId,
                        typeQuestion,
                        arrayAnswer,
                        timestamp
                    )
                        .then((status) => {
                            if (status) {
                                let weight;

                                for (let aux of this.feedbacks[indice].questions[contQuestion]
                                    .answers) {
                                    if (aux.uid == this.result[indice][contQuestion]) {
                                        weight = aux.weight;
                                    }
                                }

                                totalPoints = totalPoints + weight;
                            }

                            if (contQuestion == totalQuestions - 1) {
                                this.loadingSend[indice] = false;
                                this.addUserPoints(totalPoints);
                            }

                            contQuestion++;
                        })
                        .catch((e) => { });
                } else {
                    if (contQuestion == totalQuestions - 1) {
                        this.loadingSend[indice] = false;
                        this.addUserPoints(totalPoints);
                    }

                    contQuestion++;
                }
            } else if (typeQuestion == "multipleSelect") {
                let allAnswers = question.answers;
                let totalAnswers = question.answers.length;
                let answersSelected = [];
                let answersselectedWeight = [];

                for (let j = 0; j < totalAnswers; j++) {
                    if (answer[j] == true) {
                        answersSelected.push(allAnswers[j].uid);

                        if (
                            allAnswers[j].weight !== null &&
                            allAnswers[j].weight !== undefined &&
                            allAnswers[j].weight !== 0
                        ) {
                            let weight = allAnswers[j].weight;
                            answersselectedWeight.push(weight);
                        }
                    }
                }

                if (answersSelected.length > 0) {
                    question.answered = true;

                    await this.createResultQuestion(
                        feedbackId,
                        questionId,
                        typeQuestion,
                        answersSelected,
                        timestamp
                    )
                        .then((status) => {
                            if (status) {
                                for (let weight of answersselectedWeight) {
                                    totalPoints = totalPoints + weight;
                                }
                            }

                            if (contQuestion == totalQuestions - 1) {
                                this.loadingSend[indice] = false;
                                this.addUserPoints(totalPoints);
                            }

                            contQuestion++;
                        })
                        .catch((e) => { });
                } else {
                    if (contQuestion == totalQuestions - 1) {
                        this.loadingSend[indice] = false;
                        this.addUserPoints(totalPoints);
                    }

                    contQuestion++;
                }
            } else {
                if (answer != null && answer != undefined && answer != "") {
                    question.answered = true;
                    await this.createResultQuestion(
                        feedbackId,
                        questionId,
                        typeQuestion,
                        answer,
                        timestamp
                    )
                        .then((status) => {
                            if (status) {
                                totalPoints =
                                    totalPoints +
                                    this.feedbacks[indice].questions[contQuestion].points;
                            }

                            if (contQuestion == totalQuestions - 1) {
                                this.loadingSend[indice] = false;
                                this.addUserPoints(totalPoints);
                            }

                            contQuestion++;
                        })
                        .catch((e) => { });
                } else {
                    if (contQuestion == totalQuestions - 1) {
                        this.loadingSend[indice] = false;
                        this.addUserPoints(totalPoints);
                    }

                    contQuestion++;
                }
            }
        }
    }

    /**
     * Yes or no answered
     * @param answered 
     * @param indiceFeedback 
     * @param indiceQuestion 
     */
    answeredYesOrNo(answered, indiceFeedback, indiceQuestion) {
        if (answered == "yes") {
            this.result[indiceFeedback][indiceQuestion] = "yes"; // atribui yes como resultado da questão
            this.buttonColorYes[indiceFeedback][indiceQuestion] = "#26B950"; // marca o button "Yes" como selecionado
            this.buttonColorNo[indiceFeedback][indiceQuestion] = "#f4f4f4"; // desmarca a seleção do button "No" caso ele tenha sido selecionado posteriormente
        } else if (answered == "no") {
            this.result[indiceFeedback][indiceQuestion] = "no"; // atribui no como resultado da questão
            this.buttonColorNo[indiceFeedback][indiceQuestion] = "#ff0000"; // marca o button "No" como selecionado
            this.buttonColorYes[indiceFeedback][indiceQuestion] = "#f4f4f4"; // desmarca a seleção do button "Yes" caso ele tenha sido selecionado posteriormente
        }
    }

    /**
     * Create a result question
     * @param feedbackId 
     * @param questionId 
     * @param typeQuestion 
     * @param answersSelected 
     * @param timestamp 
     */
    createResultQuestion(
        feedbackId,
        questionId,
        typeQuestion,
        answersSelected,
        timestamp
    ) {
        return new Promise((resolve) => {
            this.daoFeedback.createResult(
                this.feedbackModule,
                this.sessionId,
                this.userId,
                feedbackId,
                questionId,
                typeQuestion,
                answersSelected,
                timestamp,
                (status) => {
                    resolve(status);
                }
            );
        });
    }

    /**
     * Adding user points
     * @param points 
     */
    addUserPoints(points) {
        if (points > 0 && this.SGlobal.userLoaded) {
            let userModuleId = this.SGlobal.userModuleId;

            this.daoAttendee.addPoints(
                this.eventId,
                userModuleId,
                this.userId,
                points,
                (result) => {
                    this.presentAlertConfirm();
                }
            );
        } else {
            this.presentAlertConfirm();
        }
    }

    /**
     * Present a confirmation alert
     */
    async presentAlertConfirm() {
        const alert = await this.SUtility.presentAlert(this.translate.instant("global.alerts.thankyou_answer"), this.translate.instant("global.alerts.answer_saved_successfully"), [
            {
                text: this.translate.instant("global.buttons.ok"),
                handler: () => { },
            },
        ], [])

        await alert.present();
    }

    /**
     * Present toast
     * @param answered 
     * @param change_answer 
     */
    async presentToast(answered, change_answer) {
        if (answered && !change_answer) {
            await this.SUtility.presentToast(this.translate.instant('global.alerts.not_change_answer'), 2000, 'bottom', false, 'blue-default');
        }
    }

    /**
     * Load colors;
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
    }
}

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AskQuestion } from 'src/app/models/ask-question';
import { DaoAskQuestionService } from 'src/app/providers/db/dao-ask-queston.service';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { Platform } from '@ionic/angular';

@Component({
    selector: 'app-ask-question',
    templateUrl: './ask-question.component.html',
    styleUrls: ['./ask-question.component.scss'],
})
export class AskQuestionComponent implements OnInit, OnDestroy {
    subscriptions: Array<Subscription> = [];
    isMobile: boolean = false;
    @Input() eventId: string;
    @Input() moduleId: string;
    @Input() itemId: string;
    @Input() sessionId: string;
    @Input() session: any;
    @Input() userId: string;
    @Input() moduleType: string;

    @Input() questionModule: any;
    @Input() questions: Array<AskQuestion> = [];

    txtQuestion: string = null;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;

    constructor(
        private daoAskQuestion: DaoAskQuestionService,
        private SGlobal: GlobalService,
        private translateService: TranslateService,
        private dbAnalytics: DaoAnalyticsService,
        private SUtility: UtilityService,
        private platform: Platform
    ) { }

    /**
     * Init component
     */
    ngOnInit() {
        this.loadColors();
        if (this.platform.is('ios') || this.platform.is('android')) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }

    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Create a question
     */
    async createQuestion() {
        let question = new AskQuestion();
        question.userId = this.userId;
        question.question = this.txtQuestion;
        question.createdAt = Date.now() / 1000 | 0;

        if (this.SGlobal.userLoaded && this.moduleType == "item") {
            (!this.questionModule.moderate) ? question.visibility = true : question.visibility = false;

            await this.daoAskQuestion.createQuestionGeneral(this.moduleId, this.itemId, question);
            this.presentAlertConfirm();
            this.txtQuestion = null;
        } else if (this.moduleType == "session") {
            (!this.session.askModerate) ? question.visibility = true : question.visibility = false;

            await this.daoAskQuestion.createQuestion(this.eventId, this.moduleId, this.sessionId, question);
            this.presentAlertConfirm();
            this.txtQuestion = null;
        }
    }

    /**
     * Add vote to a question
     * @param question 
     */
    questionAddVote(question) {
        if (this.moduleType == 'item') {
            this.daoAskQuestion.addOrRemoveVoteGeneral(this.moduleId, this.itemId, this.userId, question.uid).subscribe((data) => {
                if (data) {
                    question.userLiked = true;
                } else {
                    question.userLiked = false;
                }
            })
        } else if (this.moduleType == 'session') {
            this.daoAskQuestion.addOrRemoveVote(this.eventId, this.moduleId, this.sessionId, this.userId, question.uid).subscribe((data) => {
                if (data) {
                    question.userLiked = true;
                } else {
                    question.userLiked = false;
                }
            })
        }
    }

    /**
     * Present a confirmation alert
     */
    async presentAlertConfirm() {
        let alert = await this.SUtility.presentAlert(this.translateService.instant('global.alerts.thankyour_asking'), this.translateService.instant('global.alerts.asking_saved_successfully'), [
            {
                text: this.translateService.instant('global.buttons.ok'),
                handler: () => { }
            }
        ], []);
        alert.present();
    }

    /**
     * On component leave
     */
    ionViewWillLeave() {
        this.dbAnalytics.moduleAccess(this.eventId, this.moduleId);
    }

    /**
     * Load colors;
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
        this.link_color = this.SGlobal.eventColors.link_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
        this.bg_general_color = this.SGlobal.eventColors.bg_general_color;
    }
}

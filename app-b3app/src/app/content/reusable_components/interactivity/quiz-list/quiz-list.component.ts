import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { Quiz } from 'src/app/models/quiz';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { DaoQuizService } from 'src/app/providers/db/dao-quiz.service';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-quiz-list',
    templateUrl: './quiz-list.component.html',
    styleUrls: ['./quiz-list.component.scss'],
})
export class QuizListComponent implements OnInit, OnDestroy, OnChanges {
    subscriptions: Subscription[] = [];

    showListQuizs: boolean = true;
    backToList: boolean = false;

    @Input() eventId: string;
    @Input() moduleId: string;
    @Input() sessionId: string;
    @Input() userId: string;

    @Input() quizs: Quiz[] = [];
    @Input() quizModule: any;

    unansweredQuizs: Quiz[] = [];
    answeredQuizs: Quiz[] = [];

    quizSelectedId: string;

    title_color: string = null;
    text_content_color: string = null;
    bg_content_color: string = null;
    menu_color: string = null;

    loader: boolean = false;

    userLanguage: string;

    constructor(
        private SGlobal: GlobalService,
        private SUtility: UtilityService,
        private daoQuiz: DaoQuizService,
        private router: Router
    ) { }

    ngOnInit() {
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        this.loadColors();
        this.loadQuizs();
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    ngOnChanges() {
        this.loadQuizs();
    }

    /*
    * Loading colors
    */
    loadColors() {
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
        this.menu_color = this.SGlobal.eventColors.menu_color;
    }

    /**
     * Loading quizs
     */
    loadQuizs() {
        this.loader = true;
        this.unansweredQuizs = [];
        this.answeredQuizs = [];
        for (let i = 0; i < this.quizs.length; i++) {
            let quiz = _.cloneDeep(this.quizs[i]);
            this.daoQuiz.getQuestions(this.quizModule.uid, quiz.uid).subscribe((questions) => {
                quiz.questions = questions;

                let indexAnswered = this.checkAnsweredQuizs(quiz.uid);
                if (indexAnswered >= 0) {
                    let index = this.SUtility.checkIndexExists(this.answeredQuizs, quiz);

                    // if the object already exists in the array
                    if (index >= 0) {
                        //if visibility is on
                        if (quiz.visibility) {
                            //update object
                            this.answeredQuizs[index] = quiz;
                        } else {
                            //if visibility is off removes object from array
                            for (let i = 0; i <= this.answeredQuizs.length; i++) {
                                if (this.answeredQuizs[i].uid == quiz.uid) {
                                    this.answeredQuizs.splice(i, 1);
                                }
                            }
                        }
                    } else { // if the quiz does not already exist in the array and has active visibility, push
                        if (quiz.visibility) {
                            this.answeredQuizs.push(quiz);
                        }
                    }
                } else {
                    let index = this.SUtility.checkIndexExists(this.unansweredQuizs, quiz);

                    // if the object already exists in the array
                    if (index >= 0) {
                        //if visibility is on
                        if (quiz.visibility) {
                            //update object
                            this.unansweredQuizs[index] = quiz;
                        } else { //if visibility is off removes object from array
                            for (let i = 0; i <= this.unansweredQuizs.length; i++) {
                                if (this.unansweredQuizs[i].uid == quiz.uid) {
                                    this.unansweredQuizs.splice(i, 1);
                                }
                            }
                        }
                    } else { // if the quiz does not already exist in the array and has active visibility, push
                        if (quiz.visibility) {
                            this.unansweredQuizs.push(quiz);
                        }
                    }
                }

                if (i == this.quizs.length - 1) {
                    this.answeredQuizs.sort((a, b) => {
                        return (a.title[this.userLanguage] < b.title[this.userLanguage] ? -1 : a.title[this.userLanguage] > b.title[this.userLanguage] ? 1 : 0);
                    });

                    this.unansweredQuizs.sort((a, b) => {
                        return (a.title[this.userLanguage] < b.title[this.userLanguage] ? -1 : a.title[this.userLanguage] > b.title[this.userLanguage] ? 1 : 0);
                    });

                    if (this.quizs.length > 1) {
                        this.loader = false;
                    } else if (this.quizs.length == 1) {
                        if (this.unansweredQuizs.length > 0) {
                            this.openUnansweredQuiz(0);
                        } else {
                            this.loader = false;
                        }
                    }
                }
            });
        }
    }

    /**
     * Check if quiz already answered
     * @param quizId 
     */
    checkAnsweredQuizs(quizId) {
        if (this.userId === null || this.userId === undefined) {
            return (false);
        }

        if (this.SGlobal.user.answeredQuizes) {
            return (this.SGlobal.user.answeredQuizes.map((e) => { return e; }).indexOf(quizId));
        } else {
            return (-1);
        }
    }

    /**
     * Open unanswered quiz
     * @param index 
     */
    openUnansweredQuiz(index) {
        if (this.sessionId) {
            if (this.quizs.length > 1) {
                this.backToList = true;
            } else {
                this.backToList = false;
            }
            this.quizSelectedId = this.unansweredQuizs[index].uid;
            this.loader = true;
            this.showListQuizs = false;
            // let navigationExtras: NavigationExtras = {
            //     state: {
            //         previousPage: 'scheduleDetail',
            //         sessionId: this.sessionId,
            //         scheduleModuleId: this.quizModule.uid
            //     }
            // };

            // this.router.navigate([`/event/${this.eventId}/quiz/${this.quizModule.uid}/${this.unansweredQuizs[index].uid}`], navigationExtras);
        } else {
            let navigationExtras: NavigationExtras = {
                state: {
                    previousPage: 'quizList',
                }
            };

            this.router.navigate([`/event/${this.eventId}/quiz/${this.quizModule.uid}/${this.unansweredQuizs[index].uid}`], navigationExtras);
        }
    }

    /**
     * Open answered quiz
     * @param index 
     */
    openAnsweredQuiz(index) {
        if ((this.answeredQuizs[index].change_answer || this.answeredQuizs[index].view_answered) && this.sessionId) {
            if (this.quizs.length > 1 || this.answeredQuizs.length == 1) {
                this.backToList = true;
            } else {
                this.backToList = false;
            }
            this.quizSelectedId = this.answeredQuizs[index].uid;
            this.loader = true;
            this.showListQuizs = false;
            // let navigationExtras: NavigationExtras = {
            //     state: {
            //         previousPage: 'scheduleDetail',
            //         sessionId: this.sessionId,
            //         scheduleModuleId: this.quizModule.uid
            //     }
            // };

            // this.router.navigate([`/event/${this.eventId}/quiz/${this.quizModule.uid}/${this.answeredQuizs[index].uid}`], navigationExtras);
        } else if (this.answeredQuizs[index].change_answer || this.answeredQuizs[index].view_answered) {
            let navigationExtras: NavigationExtras = {
                state: {
                    previousPage: 'quizList',
                }
            };

            this.router.navigate([`/event/${this.eventId}/quiz/${this.quizModule.uid}/${this.answeredQuizs[index].uid}`], navigationExtras);
        }
    }

    /**
     * Stop loader
     */
    stopLoader() {
        this.loader = false;
    }

    /**
     * Go back here from child component
     */
    goBackToList() {
        this.showListQuizs = true;
        this.loadQuizs();
    }
}

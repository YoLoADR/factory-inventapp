import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { AskQuestionComponent } from './ask-question/ask-question.component';
import { InteractivitySegmentsComponent } from './interactivity-segments/interactivity-segments.component';
import { SurveyListComponent } from './survey-list/survey-list.component';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { AvatarModule } from 'ngx-avatar';
import { DocumentsComponent } from './documents/documents.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { RatingModule } from 'ng-starrating';
import { ComponentsModulesModule } from 'src/app/modules/components-modules.module';

@NgModule({
    declarations: [
        AskQuestionComponent,
        DocumentsComponent,
        FeedbacksComponent,
        InteractivitySegmentsComponent,
        QuizListComponent,
        SpeakersComponent,
        SurveyListComponent
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        TranslateModule.forChild(),
        AvatarModule,
        SharedModule,
        RatingModule,
        ComponentsModulesModule
    ],
    exports: [
        AskQuestionComponent,
        DocumentsComponent,
        FeedbacksComponent,
        InteractivitySegmentsComponent,
        QuizListComponent,
        SpeakersComponent,
        SurveyListComponent
    ]
})
export class InteractivityModule { }

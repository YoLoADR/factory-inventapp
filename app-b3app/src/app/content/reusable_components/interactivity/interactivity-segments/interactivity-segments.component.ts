import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/shared/reducers';
import { Subscription } from 'rxjs';
import { getInteractivityState } from 'src/app/shared/selectors/interactivity.selectors';
import * as _ from 'lodash';
import { GetQuestions, GetQuizs, GetSurveys, GetSpeakers, GetDocuments, GetFeedbacks } from 'src/app/shared/actions/interactivity.actions';
import { Platform } from '@ionic/angular';
import { Quiz } from 'src/app/models/quiz';
import { Survey } from 'src/app/models/survey';
import { AskQuestion } from 'src/app/models/ask-question';
import { sessionFeedback } from 'src/app/models/sessionFeedback';
import { UtilityService, GlobalService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-interactivity-segments',
    templateUrl: './interactivity-segments.component.html',
    styleUrls: ['./interactivity-segments.component.scss'],
})
export class InteractivitySegmentsComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    type: string;
    isMobile: boolean = false;

    // Global
    @Input() eventId: string;
    @Input() moduleId: string;
    @Input() sessionId: string;
    @Input() itemId: string;
    @Input() userId: string;
    @Input() moduleType: string;
    @Input() session: any;
    @Input() allowDiscussionsGroups: boolean = false;

    event: any;

    // Details
    showSpeakers: boolean = false;
    speakers: any[] = [];

    // Documents
    showDocuments: boolean = false;
    documents: any[] = [];

    // Feedback
    showFeedbacks: boolean = false;
    feedbacks: sessionFeedback[] = [];
    feedbackModule: any = null;
    result: any[] = [];
    loadingSend: boolean[] = [];

    // Questions
    showQuestion: boolean = false;
    questions: AskQuestion[] = [];
    questionModule: any = null;

    // Survey
    showSurvey: boolean = false;
    surveys: Survey[] = [];
    surveyModule: any = null;

    // Quiz
    showQuiz: boolean = false;
    quizs: Quiz[] = [];
    quizModule: any = null;

    // Training
    @Input() training: any;

    countInit: number = 0;

    userLanguage: string;

    constructor(
        private store: Store<AppState>,
        private platform: Platform,
        private SGlobal: GlobalService,
        private SUtility: UtilityService
    ) { }

    ngOnInit() {
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        if (this.platform.is('mobile')) {
            this.isMobile = true;
        } else {
            this.isMobile = false;
        }

        this.subscriptions.push(this.store.select(getInteractivityState).subscribe((interactivityState) => {
            if (interactivityState) {
                if (interactivityState.questionsInteractivity) {
                    if (interactivityState.questionsInteractivity.questions && interactivityState.questionsInteractivity.questionModule && this.session && this.session.askQuestion) {
                        this.showQuestion = true;
                        this.questions = _.cloneDeep(interactivityState.questionsInteractivity.questions);
                        this.questionModule = _.cloneDeep(interactivityState.questionsInteractivity.questionModule);
                    } else {
                        if (this.showQuestion) {
                            this.initSegment();
                        }
                        this.showQuestion = false;
                        this.questions = [];
                        this.questionModule = null;
                    }
                }
                if (interactivityState.surveysInteractivity) {
                    if (interactivityState.surveysInteractivity.surveys && interactivityState.surveysInteractivity.surveyModule && interactivityState.surveysInteractivity.surveys.length > 0 && interactivityState.surveysInteractivity.surveyModule) {
                        this.showSurvey = true;
                        this.surveys = _.cloneDeep(interactivityState.surveysInteractivity.surveys);
                        this.surveyModule = _.cloneDeep(interactivityState.surveysInteractivity.surveyModule);
                    } else {
                        if (this.showSurvey) {
                            this.initSegment();
                        }
                        this.showSurvey = false;
                        this.surveys = [];
                        this.surveyModule = null;
                    }
                }

                if (interactivityState.quizsInteractivity) {
                    if (interactivityState.quizsInteractivity.quizs && interactivityState.quizsInteractivity.quizModule && interactivityState.quizsInteractivity.quizs.length > 0 && interactivityState.quizsInteractivity.quizModule) {
                        this.showQuiz = true;
                        this.quizs = _.cloneDeep(interactivityState.quizsInteractivity.quizs);
                        this.quizModule = _.cloneDeep(interactivityState.quizsInteractivity.quizModule);
                    } else {
                        if (this.showQuiz) {
                            this.initSegment();
                        }
                        this.showQuiz = false;
                        this.quizs = [];
                        this.quizModule = null;
                    }
                }

                if (interactivityState.speakersInteractivity) {
                    if (interactivityState.speakersInteractivity.speakers && interactivityState.speakersInteractivity.speakers.length > 0) {
                        this.showSpeakers = true;
                        this.speakers = _.cloneDeep(interactivityState.speakersInteractivity.speakers);
                    } else {
                        if (this.showSpeakers) {
                            this.initSegment();
                        }
                        this.showSpeakers = false;
                        this.speakers = [];
                    }
                }

                if (interactivityState.documentsInteractivity) {
                    if (interactivityState.documentsInteractivity.documents && interactivityState.documentsInteractivity.documents.length > 0) {
                        this.showDocuments = true;
                        this.documents = _.cloneDeep(interactivityState.documentsInteractivity.documents);
                    } else {
                        if (this.showDocuments) {
                            this.initSegment();
                        }
                        this.showDocuments = false;
                        this.documents = [];
                    }
                }

                if (interactivityState.feedbacksInteractivity) {
                    if (interactivityState.feedbacksInteractivity.feedbacks && interactivityState.feedbacksInteractivity.feedbacks.length > 0) {
                        this.showFeedbacks = true;
                        this.feedbacks = _.cloneDeep(interactivityState.feedbacksInteractivity.feedbacks);
                        this.feedbackModule = _.cloneDeep(interactivityState.feedbacksInteractivity.feedbackModule);
                    } else {
                        if (this.showFeedbacks) {
                            this.initSegment();
                        }
                        this.showFeedbacks = false;
                        this.feedbacks = [];
                        this.feedbackModule = null;
                    }
                }

                this.countInit++;
                if (this.countInit == 7) {
                    this.initSegment();
                }
            }
        }))
    }

    /**
     * Initialise segment
     */
    initSegment() {
        if ((this.showSpeakers || this.showDocuments || this.showFeedbacks) && this.isMobile) {
            this.type = "details";
            return;
        }
        if (this.showQuestion) {
            this.type = "question";
            return;
        }
        if (this.showSurvey) {
            this.type = "survey";
            return;
        }
        if (this.showQuiz) {
            this.type = "quiz";
            return;
        }
    }

    /**
     * Reset interactivity store
     */
    resetInteractivity() {
        this.store.dispatch(new GetQuestions({
            questionModule: null,
            questions: []
        }))

        this.store.dispatch(new GetSurveys({
            surveyModule: null,
            surveys: []
        }))

        this.store.dispatch(new GetQuizs({
            quizModule: null,
            quizs: []
        }))

        this.store.dispatch(new GetSpeakers({
            speakers: []
        }))

        this.store.dispatch(new GetDocuments({
            documents: []
        }))

        this.store.dispatch(new GetFeedbacks({
            feedbacks: [],
            feedbackModule: null
        }))
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.resetInteractivity();
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}

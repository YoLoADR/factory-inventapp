import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { GlobalService, UtilityService } from 'src/app/shared/services';
import { Subscription } from 'rxjs';
import * as _ from 'lodash';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { DaoAnalyticsService } from 'src/app/providers/db/dao-analytics.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss'],
})
export class DocumentsComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];

    @Input() eventId: string;
    @Input() showTitle: boolean = false;

    doc: any;
    options: InAppBrowserOptions = {};

    @Input() documents: any[] = [];

    title_color: string = null;
    bg_content_color: string = null;

    loader: HTMLIonLoadingElement;

    userLanguage: string;

    constructor(
        private SGlobal: GlobalService,
        private fileOpener: FileOpener,
        private theInAppBrowser: InAppBrowser,
        private dbAnalytics: DaoAnalyticsService,
        private transfer: FileTransfer,
        private file: File,
        private platform: Platform,
        private SUtility: UtilityService,
        private translate: TranslateService
    ) { }

    ngOnInit() {
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;
        this.loadColors();
    }

    /**
     * Unsubscribe all subscriptions
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
    * Open a document
    * @param document 
    */
    openDocument(document) {
        this.doc = document;
        let platforms = this.platform.platforms().join();
        if (platforms.includes("hybrid") && platforms.includes("cordova")) {
            if (platforms.includes("android")) {
                if (
                    document.type == "pdf" ||
                    document.type == "docx" ||
                    document.type == "doc" ||
                    document.type == "docm" ||
                    document.type == "txt" ||
                    document.type == "rtf" ||
                    document.type == "csv" ||
                    document.type == "pptx" ||
                    document.type == "ppt" ||
                    document.type == "thmx" ||
                    document.type == "ppsx" ||
                    document.type == "pps" ||
                    document.type == "ppsm" ||
                    document.type == "xlsx" ||
                    document.type == "xls" ||
                    document.type == "xlsm" ||
                    document.type == "xlt" ||
                    document.type == "xlsb" ||
                    document.type == "xltx" ||
                    document.type == "xltm" ||
                    document.type == "mp4" ||
                    document.type == "wmv" ||
                    document.type == "3gp" ||
                    document.type == "avi" ||
                    document.type == "mp3" ||
                    document.type == "wav" ||
                    document.type == "jpg" ||
                    document.type == "jpeg" ||
                    document.type == "png" ||
                    document.type == "gif" ||
                    document.type == "bmp"
                ) {
                    this.presentLoading();
                    this.openInAndroid()
                        .then((res: any) => {
                            let fileExtn = this.doc.type;
                            let fileMIMEType = this.getMIMEtype(fileExtn);
                            this.fileOpener
                                .open(res, fileMIMEType)
                                .then(() => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 50);
                                })
                                .catch((e) => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 50);
                                });
                        })
                        .catch(async (e) => {
                            if (
                                document.type == "png" ||
                                document.type == "jpg" ||
                                document.type == "jpeg" ||
                                document.type == "gif" ||
                                document.type == "bmp" ||
                                document.type == "js" ||
                                document.type == "html" ||
                                document.type == "css" ||
                                document.type == "htm" ||
                                document.type == "txt"
                            ) {
                                window.open(document.url, "_system", "location=yes");
                            } else {
                                let urlFinal = encodeURIComponent(document.url);
                                window.open(
                                    "https://docs.google.com/viewer?url=" + urlFinal,
                                    "_system",
                                    "location=yes"
                                );
                            }
                        });
                } else {
                    let urlFinal = encodeURIComponent(this.doc.url);
                    window.open(
                        "https://docs.google.com/viewer?url=" + urlFinal,
                        "_system",
                        "location=yes"
                    );
                }
            } else if (platforms.includes("ios")) {
                if (
                    document.type == "pdf" ||
                    document.type == "docx" ||
                    document.type == "doc" ||
                    document.type == "docm" ||
                    document.type == "txt" ||
                    document.type == "rtf" ||
                    document.type == "csv" ||
                    document.type == "pptx" ||
                    document.type == "ppt" ||
                    document.type == "ppsx" ||
                    document.type == "pps" ||
                    document.type == "ppsm" ||
                    document.type == "xlsx" ||
                    document.type == "xls" ||
                    document.type == "xlsm" ||
                    document.type == "xlt" ||
                    document.type == "xlsb" ||
                    document.type == "xltx" ||
                    document.type == "xltm" ||
                    document.type == "mp4" ||
                    document.type == "wmv" ||
                    document.type == "3gp" ||
                    document.type == "avi" ||
                    document.type == "mp3" ||
                    document.type == "wav" ||
                    document.type == "jpg" ||
                    document.type == "jpeg" ||
                    document.type == "png" ||
                    document.type == "gif" ||
                    document.type == "bmp"
                ) {
                    this.presentLoading();
                    this.openInIos()
                        .then((res: any) => {
                            let fileExtn = this.doc.type;
                            let fileMIMEType = this.getMIMEtype(fileExtn);
                            this.fileOpener
                                .open(res, fileMIMEType)
                                .then(() => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 200);
                                })
                                .catch((e) => {
                                    setTimeout(() => {
                                        this.closeLoading();
                                    }, 200);
                                    this.presentAlertConfirm();
                                });
                        })
                        .catch((e) => {
                            setTimeout(() => {
                                this.closeLoading();
                            }, 50);
                            if (
                                document.type == "png" ||
                                document.type == "jpg" ||
                                document.type == "jpeg" ||
                                document.type == "gif" ||
                                document.type == "bmp" ||
                                document.type == "js" ||
                                document.type == "html" ||
                                document.type == "css" ||
                                document.type == "htm" ||
                                document.type == "txt"
                            ) {
                                // window.open(document.url, '_system', 'location=yes');
                                if (!this.SGlobal.isBrowser) {
                                    this.theInAppBrowser.create(
                                        document.url,
                                        "_system",
                                        this.options
                                    );
                                } else {
                                    window.open(document.url);
                                }
                            } else {
                                if (!this.SGlobal.isBrowser) {
                                    let urlFinal = encodeURIComponent(document.url);
                                    this.theInAppBrowser.create(
                                        "https://docs.google.com/viewer?url=" + urlFinal,
                                        "_system",
                                        this.options
                                    );
                                } else {
                                    window.open(document.url);
                                }
                            }
                        });
                } else {
                    if (
                        document.type == "png" ||
                        document.type == "jpg" ||
                        document.type == "jpeg" ||
                        document.type == "gif" ||
                        document.type == "bmp" ||
                        document.type == "js" ||
                        document.type == "html" ||
                        document.type == "css" ||
                        document.type == "htm" ||
                        document.type == "txt"
                    ) {
                        if (!this.SGlobal.isBrowser) {
                            window.open(document.url, "_system", "location=yes");
                        } else {
                            window.open(document.ur);
                        }
                    } else {
                        if (!this.SGlobal.isBrowser) {
                            let urlFinal = encodeURIComponent(document.url);
                            window.open(
                                "https://docs.google.com/viewer?url=" + urlFinal,
                                "_system",
                                "location=yes"
                            );
                        } else {
                            window.open(document.url);
                        }
                    }
                }
            } else {
                this.openInweb(document);
            }
        } else {
            this.openInweb(document);
        }
        this.dbAnalytics.documentAccess(
            this.eventId,
            document.uid,
            this.SGlobal.event.timezone
        );
    }


    /**
     * Open document in web
     * @param document 
     */
    openInweb(document) {
        // if (
        //     document.type == "png" ||
        //     document.type == "jpg" ||
        //     document.type == "jpeg" ||
        //     document.type == "gif" ||
        //     document.type == "bmp" ||
        //     document.type == "js" ||
        //     document.type == "html" ||
        //     document.type == "css" ||
        //     document.type == "htm" ||
        //     document.type == "txt"
        // ) {
        //     window.open(document.url, "_system", "location=yes");
        // } else {
        //     window.open(document.url, "_system", "location=yes");
        // }
        window.open(document.url, "_system", "location=yes");
    }

    /**
     * Open document in ios
     */
    openInIos() {
        return new Promise((resolve, reject) => {
            // ********* IMPORTANT: IOS NEED TO CHANGE EXTERNALROOTDIRECTORY TO OTHER PATH ****** //
            const fileTransfer: FileTransferObject = this.transfer.create();
            fileTransfer
                .download(this.doc.url, this.file.dataDirectory + this.doc.name[this.userLanguage])
                .then((entry) => {
                    resolve(entry.toURL());
                })
                .catch((err) => {
                    reject();
                });
        });
    }

    /**
     * Open document in android
     */
    openInAndroid() {
        return new Promise((resolve, reject) => {
            this.file
                .createDir(this.file.externalRootDirectory, "_downloads", false)
                .then((response) => {
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    fileTransfer
                        .download(this.doc.url, this.file.externalRootDirectory + '/_downloads/' + this.doc.name[this.userLanguage])
                        .then((entry) => {
                            resolve(entry.toURL());
                        })
                        .catch((err) => {
                            reject();
                        });
                })
                .catch((err) => {
                    const fileTransfer: FileTransferObject = this.transfer.create();
                    fileTransfer
                        .download(this.doc.url, this.file.externalRootDirectory + '/_downloads/' + this.doc.name[this.userLanguage])
                        .then((entry) => {
                            resolve(entry.toURL());
                        })
                        .catch((err) => {
                            reject();
                        });
                });
        });
    }

    /**
     * Get mime type of a file
     * @param extn 
     */
    getMIMEtype(extn) {
        let ext = extn.toLowerCase();
        let MIMETypes = {
            // texts
            txt: "text/plain",
            rtf: "application/rtf",
            csv: "text/csv",
            docx:
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            doc: "application/msword",
            docm: "application/vnd.ms-word.document.macroEnabled.12",
            // pdf
            pdf: "application/pdf",
            // images
            jpg: "image/jpeg",
            bmp: "image/bmp",
            png: "image/png",
            gif: "image/gif",
            // sheets
            xls: "application/vnd.ms-excel",
            xlsx: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            xlsm: "application/vnd.ms-excel.sheet.macroEnabled.12",
            xlt: "application/vnd.ms-excel",
            xlsb: "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
            xltx:
                "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
            xltm: "application/vnd.ms-excel.template.macroEnabled.12",
            // presentations (ppt)
            ppt: "application/vnd.ms-powerpoint",
            pptx:
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            ppsx:
                "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            pps: "application/vnd.ms-powerpoint",
            ppsm: "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
        };
        return MIMETypes[ext];
    }

    /**
     * Present loading
     */
    async presentLoading() {
        this.loader = await this.SUtility.presentLoading("", 0, "crescent");
        await this.loader.present();
    }

    /**
     * Close loading
     */
    async closeLoading() {
        await this.loader.dismiss();
    }

    /**
     * Present a confirmation alert
     */
    async presentAlertConfirm() {
        const alert = await this.SUtility.presentAlert(this.translate.instant("global.alerts.thankyou_answer"), this.translate.instant("global.alerts.answer_saved_successfully"), [
            {
                text: this.translate.instant("global.buttons.ok"),
                handler: () => { },
            },
        ], [])

        await alert.present();
    }

    /**
     * Loading colors
     */
    loadColors() {
        this.title_color = this.SGlobal.eventColors.title_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
    }
}

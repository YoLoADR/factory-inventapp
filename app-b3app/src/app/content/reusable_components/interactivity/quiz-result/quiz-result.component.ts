import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets, ChartColor } from 'chart.js';
import * as ChartDataLabels from 'chartjs-plugin-datalabels';
import { Label, Color } from 'ng2-charts';
import { UtilityService, GlobalService } from 'src/app/shared/services';
import { environment } from 'src/environments/environment';
import { Quiz } from 'src/app/models/quiz';
import { DaoQuizService } from 'src/app/providers/db/dao-quiz.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-quiz-result',
    templateUrl: './quiz-result.component.html',
    styleUrls: ['./quiz-result.component.scss'],
})
export class QuizResultComponent implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    loaded: boolean = false;

    init: boolean = false;

    @Input() eventId: string;
    @Input() moduleId: string;

    barChartOptions: ChartOptions = {
        responsive: true,
        legend: {
            display: true
        },
        scales: {
            xAxes: [{
                ticks: {
                    min: 0,
                    max: 100,
                    callback: (value) => {
                        return ("");
                        // return (value + "%");
                    }
                    // display: false
                },
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        },
        tooltips: {
            enabled: false
            // callbacks: {
            //     label: (tooltipItems, data) => {
            //         return (tooltipItems.value + '%');
            //     }
            // }
        },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    let sum = 0;
                    let dataArr: any = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        sum += data;
                    });
                    let percentage = (value * 100 / sum).toFixed(2) + "%";

                    if (percentage == '0.00%') {
                        percentage = null;
                    }

                    return (percentage);
                },
                color: '#fff',
                font: {
                    size: 14,
                    weight: 800
                }
            }
        }
    };

    barChartLabels: Label[] = [];
    barChartType: ChartType = 'horizontalBar';
    barChartColors: Color[] = [];
    barChartLegend = false;
    barChartPlugins = [ChartDataLabels];

    barChartData: ChartDataSets[] = [{
        backgroundColor: [],
        borderColor: [],
        hoverBackgroundColor: [],
        hoverBorderColor: [],
        data: []
    }];

    backedColorsArray: string[] = [
        'rgba(0, 113, 186, 1)',
        'rgba(109, 199, 221, 1)',
        'rgba(73, 60, 144, 1)',
        'rgba(150, 54, 139, 1)',
        'rgba(234, 82, 132, 1)',
        'rgba(231, 52, 88, 1)',
        'rgba(243, 147, 37, 1)',
        'rgba(253, 196, 31, 1)',
        'rgba(149, 193, 31, 1)',
        'rgba(0, 179, 187, 1)'
    ];

    colorsArray: string[] = [
        'rgba(0, 113, 186, 1)',
        'rgba(109, 199, 221, 1)',
        'rgba(73, 60, 144, 1)',
        'rgba(150, 54, 139, 1)',
        'rgba(234, 82, 132, 1)',
        'rgba(231, 52, 88, 1)',
        'rgba(243, 147, 37, 1)',
        'rgba(253, 196, 31, 1)',
        'rgba(149, 193, 31, 1)',
        'rgba(0, 179, 187, 1)'
    ];

    @Input() question: any;
    @Input() quiz: Quiz;

    userLanguage: string;

    menu_color: string = null;
    menu_text_color: string = null;
    title_color: string = null;
    text_content_color: string = null;
    link_color: string = null;
    bg_content_color: string = null;
    bg_general_color: string = null;

    constructor(
        private SQuizs: DaoQuizService,
        private SGlobal: GlobalService,
        private SUtility: UtilityService
    ) { }

    ngOnInit() {
        this.loaded = false;
        this.init = false;

        this.loadColors();
        this.userLanguage = (this.SUtility.convertLangFormat(this.SGlobal.language)) ? this.SUtility.convertLangFormat(this.SGlobal.language) : environment.platform.defaultLanguage;

        this.buildChartLabel();

        this.subscriptions.push(
            this.SQuizs.getResultsOfAnswer(this.moduleId, this.question.uid, this.quiz.uid).subscribe((results) => {
                this.buildChartDataSet(results);
            })
        )
    }

    /**
     * Unsubscribe subscriptions on destroy
     */
    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    /**
     * Load colors;
     */
    loadColors() {
        this.menu_color = this.SGlobal.eventColors.menu_color;
        this.menu_text_color = this.SGlobal.eventColors.menu_text_color;
        this.title_color = this.SGlobal.eventColors.title_color;
        this.text_content_color = this.SGlobal.eventColors.text_content_color;
        this.link_color = this.SGlobal.eventColors.link_color;
        this.bg_content_color = this.SGlobal.eventColors.bg_content_color;
        this.bg_general_color = this.SGlobal.eventColors.bg_general_color;
    }

    /**
     * Build chart label (answer name)
     */
    buildChartLabel() {
        this.barChartLabels = this.question.answers.map((answer) => {
            let label = answer.answer[this.userLanguage];

            if (label.length > 15) {
                label = label.substring(0, 14) + "...";
            }
            return (label);
        })
    }

    /**
     * Build chart options
     * @param colors 
     */
    buildChartOptions(colors: string[]) {
        let scalesForVerticalOrPie = {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                ticks: {
                    min: 0,
                    max: 100,
                    callback: (value) => {
                        return ("");
                        // return (value + "%");
                    }
                    // display: false
                },
                gridLines: {
                    display: false
                }
            }]
        };

        // Set type, basic style and colors labels
        if (this.question && this.question.graphic) {
            if (this.question.graphic == 'ColumnChart') {
                this.barChartType = "bar";
                this.barChartOptions.scales = scalesForVerticalOrPie;
                // this.barChartOptions.scales.xAxes[0].ticks = {
                //     fontColor: colors
                // };
            } else if (this.question.graphic == 'PieChart') {
                this.barChartType = "pie";
                this.barChartOptions.scales = scalesForVerticalOrPie;
                // this.barChartOptions.scales.xAxes[0].ticks = {
                //     fontColor: colors
                // };
            } else {
                this.barChartType = "horizontalBar";
                // this.barChartOptions.scales.yAxes[0].ticks = {
                //     fontColor: colors
                // };
            }
        } else {
            this.barChartType = "horizontalBar";
            // this.barChartOptions.scales.yAxes[0].ticks = {
            //     fontColor: colors
            // };
        }

        // Set bar colors
        this.barChartData[0].backgroundColor = colors;
        this.barChartData[0].borderColor = colors;
        this.barChartData[0].hoverBackgroundColor = colors;
        this.barChartData[0].hoverBorderColor = colors;
    }

    /**
     * Generate colors
     */
    generateArrayColorsDefault() {
        if (this.colorsArray.length == 0) {
            this.colorsArray = this.backedColorsArray;
        }

        let random = this.randomInt(0, this.colorsArray.length - 1);
        let color = this.colorsArray[random];

        // Remove color from array
        this.colorsArray.splice(random, 1);

        return (color);
    }

    /**
     * Get random int
     * @param min 
     * @param max 
     */
    randomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return (Math.floor(Math.random() * (max - min + 1)) + min);
    }

    /**
     * Build chart data set
     * @param results 
     */
    buildChartDataSet(results) {
        // let resultsForAnswer: ChartDataSets[] = []
        let colors: string[] = [];
        let datas: any[] = [];

        for (let i = 0; i < this.question.answers.length; i++) {
            let answerCount = 0;
            let answer = this.question.answers[i];
            colors.push(this.generateArrayColorsDefault());

            for (let iR = 0; iR < results.length; iR++) {
                let result = results[iR];
                let check = result.answer.includes(answer.uid);
                if (check) {
                    answerCount++;
                }
            }

            let percentage = 0;

            if (answerCount != 0) {
                percentage = Math.round(answerCount / results.length * 100);
            }

            datas.push(percentage);
        }

        if (!this.init) {
            this.buildChartOptions(colors);
        }

        this.barChartData[0].data = datas;

        this.loaded = true;
        this.init = true;
    }
}

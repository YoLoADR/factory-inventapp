import {
    Component,
    OnInit,
    Input,
    OnChanges,
    OnDestroy,
    Output,
    EventEmitter,
    Inject
} from '@angular/core';
import { Platform } from '@ionic/angular';
import { WherebyService, GlobalService } from 'src/app/shared/services';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-visio-conference',
    templateUrl: './visio-conference.component.html',
    styleUrls: ['./visio-conference.component.scss']
})
export class VisioConferenceComponent implements OnInit, OnChanges, OnDestroy {
    plat: string;
    subscriptions: Subscription[] = [];
    @Input() eventId: string;
    @Input() joined: boolean = false;

    @Input() urlVisio: string;
    @Input() allowChat: boolean = true;
    @Input() allowScreenshare: boolean = true;
    @Input() allowLeave: boolean = true;
    @Input() btnColor: string;
    @Input() comp: string = "chat";

    @Output() closeVisioEvent: EventEmitter<boolean> = new EventEmitter();

    urlIframe: SafeResourceUrl;

    fullscreenMode: boolean = false;

    width: string = "100%";
    height: string = "600px";
    miniPlayer: boolean = false;
    moveMiniPlayer: boolean = false;
    moveSubscription: Subscription;
    menuActivated: boolean = true; // width = 269 on activation 80 on closed
    noTransform: boolean = false;

    constructor(
        @Inject(DOCUMENT) private document: any,
        private global: GlobalService,
        private platform: Platform,
        private SWhereby: WherebyService,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        if (this.platform.is('ios') || this.platform.is('android')) {
            this.plat = 'mobile';
        } else {
            this.plat = 'desktop';
        }

        if (this.urlVisio) {
            this.buildUrl();
        }

        this.subscriptions.push(this.global.hamburgerActivated.subscribe((activated) => {
            this.menuActivated = activated;
        }))
    }

    /**
     * On changes
     */
    ngOnChanges() {
        if (this.urlVisio) {
            this.buildUrl();
        }
    }

    ngOnDestroy() {
        this.subscriptions.map((s) => s.unsubscribe());
    }

    /**
     * Build url for iframe
     */
    buildUrl() {
        // this.urlIframe = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlVisio + "?embed&iframeSource=invent-app&chat=on");
        this.urlIframe = this.sanitizer.bypassSecurityTrustResourceUrl(
            this.urlVisio +
            '?embed&iframeSource=invent-app' +
            (this.allowChat ? '&chat=on' : '&chat=off') +
            (this.allowScreenshare
                ? '&screenshare=on'
                : '&screenshare=off') +
            '&leaveButton=off'
        );
    }

    /**
     * Launch visio analytics
     */
    analyticsVisio() {
        this.SWhereby.analyticsNewAccessToRoom(
            this.eventId,
            this.global.userId
        );
    }

    /**
     * Full screen mode visio
     */
    fullScreenVisio() {
        let elem = document.getElementById('backgroundIframe') as any;
        if (this.fullscreenMode) {
            if (this.document.exitFullscreen) {
                this.document.exitFullscreen();
            } else if (this.document.mozCancelFullScreen) {
                /* Firefox */
                this.document.mozCancelFullScreen();
            } else if (this.document.webkitExitFullscreen) {
                /* Chrome, Safari and Opera */
                this.document.webkitExitFullscreen();
            } else if (this.document.msExitFullscreen) {
                /* IE/Edge */
                this.document.msExitFullscreen();
            }
            this.height = "600px";
        } else {
            this.deactivateMiniPlayer();

            let methodToBeInvoked = elem.requestFullscreen || elem.webkitRequestFullScreen || elem['mozRequestFullscreen'] || elem['msRequestFullscreen'];
            if (methodToBeInvoked) {
                methodToBeInvoked.call(elem);
            }
            this.height = "100%";
        }
        this.fullscreenMode = !this.fullscreenMode;
    }

    /**
     * Leave visio
     */
    leaveVisio() {
        if (this.fullscreenMode) {
            this.fullScreenVisio();
        }

        if (this.miniPlayer) {
            this.deactivateMiniPlayer();
        }

        this.closeVisioEvent.emit(true);
        this.joined = false;
    }

    /**
     * Join visio
     */
    async openVisio() {
        if (this.SWhereby.visioAvailableOnMobile()) {
            const fullUrl = this.urlVisio + "?embed&iframeSource=invent-app" + ((this.allowChat) ? "&chat=on" : "&chat=off") + ((this.allowScreenshare) ? "&screenshare=on" : "&screenshare=off") + "&leaveButton=" + (this.platform.is('ios') ? 'on' : 'off') + "&skipMediaPermissionPrompt"

            if (
                this.SWhereby.visioAvailableOnSF() &&
                this.SWhereby.isAndroid()
            ) {
                this.subscriptions.push(
                    this.SWhereby.openSFVisio(fullUrl).subscribe()
                );
            } else {
                this.subscriptions.push(
                    this.SWhereby.openInAppBrowserVisio(fullUrl).subscribe()
                );
            }
        }
        // this.SWhereby.openVisioFor2(null, this.urlVisio + "?embed&iframeSource=invent-app" + ((this.allowChat) ? "&chat=on" : "&chat=off") + ((this.allowScreenshare) ? "&screenshare=on" : "&screenshare=off") + "&leaveButton=" + (this.platform.is('ios') ? 'on' : 'off') + "&skipMediaPermissionPrompt");
    }

    /**
     * Activate or deactivate miniplayer
     */
    miniPlayerActivation() {
        (this.miniPlayer) ? this.deactivateMiniPlayer() : this.activateMiniPlayer();
    }

    /**
     * ActivateMiniPlayer
     */
    activateMiniPlayer() {
        // Activate mini player
        this.miniPlayer = true;
        this.width = "350px";
        this.height = "240px";
        this.noTransform = false;
    }

    /**
     * Desactivate miniplayer
     */
    deactivateMiniPlayer() {
        // Deactivate mini player
        this.miniPlayer = false;
        this.width = "100%";
        this.height = "600px";
        this.noTransform = true;
    }

    /**
     * Move mini player
     * @param move 
     * @param evt 
     */
    moveMiniPlayerActivation(move, evt) {
        evt.preventDefault();
        if (move) {
            this.moveMiniPlayer = true;
        } else {
            this.moveMiniPlayer = false;
        }
    }
}

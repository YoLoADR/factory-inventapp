import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisioConferenceComponent } from './visio-conference.component';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AngularDraggableModule } from 'angular2-draggable';

@NgModule({
    declarations: [
        VisioConferenceComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        TranslateModule.forChild(),
        AngularDraggableModule
    ],
    exports: [
        VisioConferenceComponent
    ]
})
export class VisioConferenceModule { }

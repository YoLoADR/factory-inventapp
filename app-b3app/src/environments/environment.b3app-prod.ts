// staging environment
export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyClL8A0hChJwmFPCZoHrpE56xobP5Dkgc4",
    authDomain: "b3app-prod.firebaseapp.com",
    databaseURL: "https://b3app-prod.firebaseio.com",
    projectId: "b3app-prod",
    hosting: "app-b3",
    storageBucket: "b3app-prod.appspot.com",
    messagingSenderId: "663376630834",
  },
  onesignal: {
    onesignal_appid: "a8d79900-d281-4b89-bd09-27c6454a6a75",
    notification_api_id: "YjA2YzY4YTgtYzFhYi00ZTg4LTk3NTctZThjYjhhN2ZkOTY2",
    notification_app_id: "a8d79900-d281-4b89-bd09-27c6454a6a75",
  },
  platform: {
    appName: "B3App",
    apiBaseUrl: "https://us-central1-b3app-prod.cloudfunctions.net/",
    oneSignalAppId: "a8d79900-d281-4b89-bd09-27c6454a6a75",
    defaultLanguage: "en_US",
    defaultTimezone: "Europe/Paris",
    appBaseUrl: "https://app.b3app.com",
    loginLogo: "logo.png",
    loginLogoClass: "invent-logo", // options is 'banner-logo' or 'invent-logo'
    showProfileText: false,
    publicEvent: false,
    eventId: "", // case public event
    containerId: "defaultContainer", // case app is event manager with multiple events
    isClientApp: false, // case client app, set true to remove public events button
    bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
    bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
  },
};

// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyD4CDW53IBQhrqkIFHAGZr6mC_FnDPae7E",
    authDomain: "b3app-master.firebaseapp.com",
    databaseURL: "https://b3app-master.firebaseio.com",
    projectId: "b3app-master",
    hosting: "app-b3app",
    storageBucket: "b3app-master.appspot.com",
    messagingSenderId: "882162877679",
  },
  onesignal: {
    onesignal_appid: "4632b112-ea3d-4866-918a-343650b2d24a",
    notification_api_id: "ZGFiY2YxYTEtYzU2ZC00ZWM1LWE2NjItY2E2OWFjN2M3NmJl",
    notification_app_id: "f7b1610c-09b2-4588-af3d-f2a07a60534b",
  },
  platform: {
    appName: "B3App",
    apiBaseUrl: "https://us-central1-b3app-master.cloudfunctions.net/",
    oneSignalAppId: "4632b112-ea3d-4866-918a-343650b2d24a",
    defaultLanguage: "en_US",
    defaultTimezone: "Europe/Paris",
    appBaseUrl: "https://app-staging.b3app.com",
    loginLogo: "logo.png",
    loginLogoClass: "invent-logo", // options is 'banner-logo' or 'invent-logo'
    showProfileText: false,
    publicEvent: false,
    eventId: "", // case public event
    containerId: "defaultContainer", // case app is event manager with multiple events
    isClientApp: false, // case client app, set true to remove public events button
    bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
    bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
  },
};

// production ceuapp environment
export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBOY3g2pteaC9dOQ4k5MhN9dGGpFtmh5Bw",
    authDomain: "ceuapp-prod.firebaseapp.com",
    databaseURL: "https://ceuapp-prod.firebaseio.com",
    projectId: "ceuapp-prod",
    hosting: "app-ceuprod",
    storageBucket: "ceuapp-prod.appspot.com",
    messagingSenderId: "1040918180944",
    appId: "1:1040918180944:web:f2fec8651a17bb0c",
  },
  onesignal: {
    onesignal_appid: "a3c19564-340b-46e8-8a41-468ef87de9a0",
    notification_api_id: "MDFlZTk0MjAtMjRhOS00MWEwLWIxNDgtZjUzMWE1ZjNiOTQw",
    notification_app_id: "a3c19564-340b-46e8-8a41-468ef87de9a0",
  },
  platform: {
    appName: "CéuApp",
    apiBaseUrl: "https://us-central1-ceuapp-prod.cloudfunctions.net/",
    oneSignalAppId: "a3c19564-340b-46e8-8a41-468ef87de9a0",
    defaultLanguage: "pt_BR",
    defaultTimezone: "America/Sao_Paulo",
    appBaseUrl: "https://app.ceuapp.com",
    loginLogo: "/ceu/logo.png",
    loginLogoClass: "invent-logo", // options is 'banner-logo' or 'invent-logo'
    showProfileText: false,
    publicEvent: false,
    eventId: "", // case public event
    containerId: "defaultContainer", // case app is event manager with multiple events
    isClientApp: false, // case client app, set true to remove public events button
    bambuserApplicationId: "3u9sc8akbtohsISc6XbDnw",
    bambuserApiKey: "EHPZTFHfSbTuoxA3CDnc2a",
  },
};

// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

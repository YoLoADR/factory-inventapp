document.title = 'B3App';
let eventId;
let interval = setInterval(() => {
    if (localStorage.getItem('eventId')) {
        eventId = localStorage.getItem('eventId');
        getEventData(eventId);
        clearInterval(interval);
    }
}, 2500);

function getEventData(eventId) {
    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url: 'https://us-central1-b3app-develop.cloudfunctions.net/dbEventGetEventWebappDetails?eventId=' + eventId,
            success: function (data) {
                // document.title = data.title;
                // add ios touchscreen event icon
                // $("LINK[rel='apple-touch-icon']").remove();
                // $('head').append('<link rel="apple-touch-icon" href="' + data.touchIcon + '" type="image/png" />');
                // change default favicon to event favicon
                // $("LINK[rel='icon']").remove();
                // $('head').append('<link rel="icon" type="image/png" href="' + data.touchIcon + '" />');
                // add ios touchstartup event splashscreen
                // $("LINK[rel='apple-touch-startup-image']").remove();
                // $('head').append('<link href="' + data.splashScreen + '" rel="apple-touch-startup-image" />');
            }
        });
    });
}
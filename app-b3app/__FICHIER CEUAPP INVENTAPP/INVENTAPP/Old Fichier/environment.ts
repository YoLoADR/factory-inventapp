// production invent-app environment
export const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyA3NQSX1-436v5WByKK3hS9dNue44CH75U",
        authDomain: "invent-app-prod.firebaseapp.com",
        databaseURL: "https://invent-app-prod.firebaseio.com",
        projectId: "invent-app-prod",
        storageBucket: "invent-app-prod.appspot.com",
        messagingSenderId: "394492317094",
    },
    platform: {
        appName: 'Invent App',
        apiBaseUrl: 'https://us-central1-invent-app-prod.cloudfunctions.net/',
        oneSignalAppId: 'fb407285-3d6d-467a-8be7-4ddf7a373873',
        defaultLanguage: 'en_US',
        appBaseUrl: 'https://app.invent-app.com',
        loginLogo: 'logo.png',
        loginLogoClass: 'invent-logo', // options is 'banner-logo' or 'invent-logo'
        showProfileText: false,
        publicEvent: false,
        eventId: '', // case public event
        containerId: 'defaultContainer', // case app is event manager with multiple events
        isClientApp: false // case client app, set true to remove public events button
    }
};

  // import 'zone.js/dist/zone-error';  // Included with Angular CLI.
